package com.pts.invetech.pojo;

public class NewLetterDetails {
	
	public String j;
	public String date;
	public String url;
	public String text;

	public NewLetterDetails(String j, String date, String url, String text) {
		// TODO Auto-generated constructor stub
		this.j = j;
		this.date = date;
		this.url = url;
		this.text = text;
	}

	public String getJ() {
		return j;
	}

	public void setJ(String j) {
		this.j = j;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	@Override
	public String toString() {
		return "NewLetterDetails[j=" + j + ", url=" + url
				+ ", date=" + date + "]";
	}


}
