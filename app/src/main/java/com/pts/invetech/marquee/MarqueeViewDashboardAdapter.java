package com.pts.invetech.marquee;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pts.invetech.R;

/**
 * Created by Ashish on 26-09-2016.
 */

public class MarqueeViewDashboardAdapter extends RecyclerView.Adapter<MarqueeViewDashboardAdapter.ViewHolder> {

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView secondView;
        public ImageView firstView;

        public ViewHolder(View v) {
            super(v);
            firstView =  (ImageView) v.findViewById(R.id.first);
            secondView =  (TextView) v.findViewById(R.id.second);
        }
    }


    private String[] desc_array;
    private Integer[] imageid_array;
    private boolean isData=true;


    public MarqueeViewDashboardAdapter(String[] desc,
                                       Integer[] imageid){

        desc_array=desc;
        imageid_array=imageid;

        if(desc_array== null){
            isData=false;
        }
    }

    @Override
    public MarqueeViewDashboardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tmp_file_dashboard, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MarqueeViewDashboardAdapter.ViewHolder holder, int position) {

        if(isData) {
            position = position % desc_array.length;

            holder.firstView.setImageResource(imageid_array[position]);
            holder.secondView.setText(desc_array[position]);
        }
    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }
}