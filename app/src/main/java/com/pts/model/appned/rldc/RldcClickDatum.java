
package com.pts.model.appned.rldc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RldcClickDatum {

    @SerializedName("buyer")
    @Expose
    private String buyer;
    @SerializedName("seller")
    @Expose
    private String seller;
    @SerializedName("approval_no")
    @Expose
    private String approvalNo;
    @SerializedName("trader")
    @Expose
    private String trader;
    @SerializedName("maxrevision")
    @Expose
    private String maxrevision;
    @SerializedName("data_id")
    @Expose
    private String dataId;
    @SerializedName("totalqtm")
    @Expose
    private String totalqtm;
    @SerializedName("track")
    @Expose
    private Integer track;

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getApprovalNo() {
        return approvalNo;
    }

    public void setApprovalNo(String approvalNo) {
        this.approvalNo = approvalNo;
    }

    public String getTrader() {
        return trader;
    }

    public void setTrader(String trader) {
        this.trader = trader;
    }

    public String getMaxrevision() {
        return maxrevision;
    }

    public void setMaxrevision(String maxrevision) {
        this.maxrevision = maxrevision;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getTotalqtm() {
        return totalqtm;
    }

    public void setTotalqtm(String totalqtm) {
        this.totalqtm = totalqtm;
    }

    public Integer getTrack() {
        return track;
    }

    public void setTrack(Integer track) {
        this.track = track;
    }

}
