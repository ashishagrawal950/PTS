package com.pts.invetech.customlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

import java.util.ArrayList;

/**
 * Created by Ashish on 19-09-2016.
 */
public class LandedCostApplicableChargesCustomList extends ArrayAdapter<String> {

    private Activity context;
    private ArrayList<String> applicable_charges_name_array;
    private ArrayList<String> applicable_charges_array;

    public LandedCostApplicableChargesCustomList(Activity context, ArrayList<String> applicable_charges_name_array, ArrayList<String> applicable_charges_array) {
        // TODO Auto-generated constructor stub
        super(context, R.layout.list_new_bid, applicable_charges_name_array);
        this.context = context;
        this.applicable_charges_name_array = applicable_charges_name_array;
        this.applicable_charges_array = applicable_charges_array;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_item_landedcost, null, true);
        TextView txt1 = (TextView) rowView.findViewById(R.id.textView1);
        TextView txt2 = (TextView) rowView.findViewById(R.id.textView2);
        txt1.setText(applicable_charges_name_array.get(position));
        txt2.setText(applicable_charges_array.get(position));
        return rowView;
    }
}