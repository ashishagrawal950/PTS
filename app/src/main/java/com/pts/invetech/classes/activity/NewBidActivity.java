package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.AddBidCancelAllAPIResponse;
import com.pts.invetech.apiresponse.AddBidClientSubmitAPIResponse;
import com.pts.invetech.apiresponse.AddBidDeleteAPIResponse;
import com.pts.invetech.apiresponse.AddBidEscertSaveAPIResponse;
import com.pts.invetech.apiresponse.AddBidRECDeleteAPIResponse;
import com.pts.invetech.apiresponse.AddBidRECGetallAPIResponse;
import com.pts.invetech.apiresponse.AddBidRECSUBMITGetallAPIResponse;
import com.pts.invetech.apiresponse.AddBidRECSaveAPIResponse;
import com.pts.invetech.apiresponse.AddBidSubmitAPIResponse;
import com.pts.invetech.apiresponse.AddBidUpdateAPIResponse;
import com.pts.invetech.apiresponse.DeletetamBidAPIResponse;
import com.pts.invetech.apiresponse.GetTamBidDetailsAPIResponse;
import com.pts.invetech.apiresponse.NoBidGetAllDataAPIResponse;
import com.pts.invetech.apiresponse.SaveTamBidAPIResponse;
import com.pts.invetech.apiresponse.SubmitTamBidDetailsAPIResponse;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.customlist.AddBidCustomList;
import com.pts.invetech.customlist.AddBidEscertsCustomList;
import com.pts.invetech.customlist.AddBidRECCustomList;
import com.pts.invetech.customlist.TamGetCustomList;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.pojo.AddBidGetAllDetails;
import com.pts.invetech.pojo.AddBidGetAllDetailsRec;
import com.pts.invetech.pojo.AddBidRecDelate;
import com.pts.invetech.pojo.AddBidSubmit;
import com.pts.invetech.pojo.AddBidSubmitEscerts;
import com.pts.invetech.pojo.DeletetamBid;
import com.pts.invetech.pojo.Login;
import com.pts.invetech.pojo.NewBidCancelAllClientBid;
import com.pts.invetech.pojo.NewBidDeleteClientBid;
import com.pts.invetech.pojo.NewBidUpdateClientBid;
import com.pts.invetech.pojo.NewSaveClientBid;
import com.pts.invetech.pojo.SaveTamBid;
import com.pts.invetech.pojo.TamGetAllDetails;
import com.pts.invetech.utils.Constant;
import com.pts.invetech.utils.JSONUtils;
import com.pts.invetech.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;


public class NewBidActivity extends Activity implements OnItemSelectedListener {
    private Button popupButtoncancel;
    private Context context;
    private SQLiteDatabase db;
    private String dbacess;
    private Dialog prgDialog;
    private LinearLayout mopenLayout, llnewbidsecondheading,
            lladdbidcancel, llsavecancel, lladdbidsave, lladdbidsubmit, lladdbidform, lllistheading;
    static TextView etxt_fromdate, etxt_fromdate_set, btaddbid, clientusename, tvbidtype;
    private NonScrollListView addbidlist;
    private Button btsaveclientbid, btcancelclientbid, btcancelbid, btsubmitbid;
    private Animation animbounce;
    private int j;
    private RadioGroup rgbiddigntype, rgbiddigndate, lltype;
    private Button btgobidsave;
    private CheckBox cbclienttype;
    private RadioButton biddingtype, biddingdate, lliexbuttontype;
    private String biddingtypeSend, biddingdateSend;
    static String as = "", place;
    private String gettype = "", getclienttype = "";
    private EditText etslotfrom, etslotto, etprice, etbid, etnoofbloack;
    private String clientslotfrom, clientslotto, clientprice, clientbid,
            clientnoofblock;
    private String submitstatus;
    private HashMap<Integer, AddBidGetAllDetails> myHashmap;
    private AddBidGetAllDetails mypojo;
    private String[] serialnumberadapter, idadapter, dateadapter, priceadapter, bidadapter,
            blockfromadapter, blocktoadapter, statusadapter;
    private String iddelete, datedelete, idupdate, priceupdate, bidupadte, blockfromupadte, blocktoupadte;
    private String companyName;

    private String solormax, solormin, nonsolormax, nonsolormin;
    /*For the Save*/
    private RadioGroup popupcbclienttype;
    private RadioButton popupbiddingtype;
    static TextView popupetxt_fromdate;
    private EditText popupetprice, popupetbid, popupetnoofbloack;
    static String popupas, popupcbclienttypesendBUY;
    private String popupclientprice,
            popupclientbid, popupclientnoofblock;
    private Button popupButtonSave;
    static String popupplace;
    private String status;
    /* For Update */
    static String updateas, updategetclienttype;
    private RadioGroup updatecbclienttype;
    private RadioButton updatebiddingtype;
    static TextView updateetxt_fromdate;
    private TextView updateetslotfromfirst, updateetslotfromsecond, updateetslotfromthird, updateetslottofirst, updateetslottosecond, updateetslottothird;
    private EditText updateetprice, updateetbid,
            updateetnoofbloack;
    private String updateclientslotfromfirst, updateclientslotfromsecond, updateclientslottofirst, updateclientslottosecond, updateclientprice,
            updateclientbid, updateclientnoofblock;
    private String part1, part2, partone, parttwo;
    private RadioButton updateRBclienttypeBUY, updateRBclienttypeSELL;

    /*For Cancel All*/
    private String cancelallid = "", cancelalldate = "";
    private String cancelalldateSend;
    private ArrayList<String> serialnumber = null;
    private ArrayList<String> addbidid = null;
    private ArrayList<String> addbiddate = null;
    private ArrayList<String> addbidprice = null;
    private ArrayList<String> addbidbid = null;
    private ArrayList<String> addbidblockfrom = null;
    private ArrayList<String> addbidblockto = null;
    private ArrayList<String> addbidstatus = null;
    private JSONArray biddetail;
    private TextView popupetslotfromfirst, popupetslotfromsecond, popupetslotfromthird, popupetslottofirst, popupetslottosecond, popupetslottothird;
    private LinearLayout popupetslotfromcolon, popupllslotfromsecond, popupllslotfromthird, popupetslottocolon, popupllslottosecond, popupllslottothird;
    private String popupclientslotfromfirst, popupclientetslotfromsecond, popupclientetslotfromthird, popupclientslottofirst, popupclientetslottosecond, popupclientetslottothird;
    private LinearLayout tvcross;
    private ArrayAdapter<String> adapter;
    private String exchangetype;


    //// REC PLACE BID
    private RadioGroup rgrectype, rgrecbiddigntype, rgrectypesolor, updatergrectypesolor;
    private RadioButton rbtyperec1, rbtyperec2, buyrb1, sellrb2, solarrb1, nonsolarrb2, updatesolarrb1, updatenonsolarrb2;
    private TextView recetxt_fromdate_set;
    private Spinner recetxt_fromdate;
    private ArrayAdapter<String> adapter_date;
    private Button btgobidrecsave;
    private RadioButton rgrectypebutton, rgrecbiddigntypebutton, rgrectypesolorbutton;
    private String rgrectypebuttongettypeSend = "", rgrecbiddigntypebuttonSend = "", rgrectypesolorbuttonSend = "", recplacedbiddatesend;
    private HashMap<Integer, AddBidGetAllDetailsRec> myHashmaprec;
    private AddBidGetAllDetailsRec mypojorec;
    private String[] recseriala, serialnumberadapterrec, idadapterrec, clientidadapterrec, dateadapterrec, typeadapterrec, rectypeadapterrec, priceadapterrec, noofrecadapterrec,
            ordernatureadapterrec, statusadapterrec, tstatusadapterrec, extraadapterrec, entrybyadapterrec, timestampadapterrec;
    private String iddeleterec, clientiddeleterec, datedeleterec, typedeleterec, rectypedeleterec, pricedeleterec, noofrecdeleterec, ordernaturedeleterec,
            statusdeleterec, tstatusdeleterec, extradeleterec, entrybydeleterec, timestampdeleterec;
    private LinearLayout llsavecancelrec, lladdbidsaverec, lladdbidcancelrec, lladdbidsubmitrec, llreclistheading;
    private Button btaddbidrec;
    private NonScrollListView addbidreclist;
    private EditText popupetpricerec, popupetnoofbloackrec;
    private LinearLayout tvcrossrec;
    private Button popupButtonSaverec, popupButtoncancelrec, btcancelbidrec, btsubmitbidrec;
    private String popupclientpricerec, popupclientnoofblockrec;
    private EditText updateetpricerec, updateetnoofbloackrec;
    private String updateclientpricerec, updateclientnoofblockrec;
    private String ideditrec, clientideditrec, dateeditrec, typeeditrec, rectypeeditrec, priceeditrec, noofreceditrec,
            ordernatureeditrec, statuseditrec, tstatuseditrec, extraeditrec, entrybyeditrec, timestampeditrec;

    private ArrayList<String> recdate_array = new ArrayList<String>();
    private ArrayList<String> recprintdate_array = new ArrayList<String>();

    private ArrayList<String> serialnumberplacerec = new ArrayList<String>();
    private ArrayList<String> addbididrec = new ArrayList<String>();
    private ArrayList<String> addbidclientidrec = new ArrayList<String>();
    private ArrayList<String> addbiddaterec = new ArrayList<String>();
    private ArrayList<String> addbidtyperec = new ArrayList<String>();
    private ArrayList<String> addbidrectyperec = new ArrayList<String>();
    private ArrayList<String> addbidpricerec = new ArrayList<String>();
    private ArrayList<String> addbidnoofrec = new ArrayList<String>();
    private ArrayList<String> addbidordernaturerec = new ArrayList<String>();
    private ArrayList<String> addbidstatusrec = new ArrayList<String>();
    private ArrayList<String> addbidtstatusrec = new ArrayList<String>();
    private ArrayList<String> addbidextrarec = new ArrayList<String>();
    private ArrayList<String> addbidentrybyrec = new ArrayList<String>();
    private ArrayList<String> addbidtimestamprec = new ArrayList<String>();
    private String cancelallidrec = "";
    private TextView tvpopupmin, tvpopupmax;
    private TextView tvupdatemin, tvupdatemax;

    ////
    private String comeFrom;

    ////// TAM BID
    private Spinner tam_sp_selectproduct;
    private RadioGroup ll_radiogrp_platform;
    private RadioButton rb1_paltform, rb2_paltform;
    static TextView tam_tv_delivery;
    private Button btn_go_tam, btn_tam_newbutton;
    static String producttypesend = "", rb1_paltformproducttypeSend = "", tam_date_Send = "";
    private LinearLayout ll_tam_second_heading, ll_tam_new_cancel_submit, ll_TAMlist_heading;
    private NonScrollListView add_tam_list;

    private ArrayList<String> selectproductArray = new ArrayList<String>();

    private RadioGroup tampopupcbclienttype;
    private TextView tampopupetslotfromfirst, tampopupetslotfromsecond, tampopupetslotfromthird, tampopupetslottofirst,
            tampopupetslottosecond, tampopupetslottothird;
    private LinearLayout tampopupetslotfromcolon, tampopupllslotfromsecond, tampopupllslotfromthird, tampopupetslottocolon,
            tampopupllslottosecond, tampopupllslottothird, tamtvcross;
    private EditText tampopupetprice, tampopupetmaxprice, tampopupetbid;

    private RadioButton tampopupbiddingtype;
    private LinearLayout ll_tam_cancelbutton, ll_tam_submitbutton;
    private Button btn_tam_cancelbutton, btn_tam_submitbutton;


    private String tampopupcbclienttypesendBUY, tampopupclientslotfromfirst, tampopupclientetslotfromsecond, tampopupclientetslotfromthird,
            tampopupclientslottofirst, tampopupclientetslottosecond, tampopupclientetslottothird, tampopupclientprice, tampopupclientmaxprice, tampopupclientbid;


    private ArrayList<String> tamgetserialnumber = new ArrayList<String>();
    private ArrayList<String> tamgetid = new ArrayList<String>();
    private ArrayList<String> tamgetdateshow = new ArrayList<String>();
    private ArrayList<String> tamgettype = new ArrayList<String>();
    private ArrayList<String> tamgetproducttype = new ArrayList<String>();
    private ArrayList<String> tamgetfromtime = new ArrayList<String>();
    private ArrayList<String> tamgettotime = new ArrayList<String>();
    private ArrayList<String> tamgetminprice = new ArrayList<String>();
    private ArrayList<String> tamgetmaxprice = new ArrayList<String>();
    private ArrayList<String> tamgetordernature = new ArrayList<String>();
    private ArrayList<String> tamgetbidquantum = new ArrayList<String>();
    private ArrayList<String> tamgetbidcode = new ArrayList<String>();
    private ArrayList<String> tamgetstatus = new ArrayList<String>();

    private String idtamdelete, typetamdelete, producttypetamdelete, dateshowtamdelete;
    private String tamcancelallid;

    // TAM Edit
    private RadioGroup updatetampopupcbclienttype;
    private RadioButton updatetamclienttypeBUY, updatetamclienttypeSELL;
    private TextView updatetampopupetslotfromfirst, updatetampopupetslotfromsecond, updatetampopupetslotfromthird, updatetampopupetslottofirst, updatetampopupetslottosecond, updatetampopupetslottothird;
    private EditText updatetampopupetprice, updatetampopupetmaxprice, updatetampopupetbid;
    private RadioButton updatetambiddingtype;
    private String updatetamgetid, updatetamgetdateshow, updatetamgettype, updatetamgetproducttype,
            updatetemgetclienttype, updatetemclientslotfromfirst, updatetemclientslotfromsecond,
            updatetemclientslottofirst, updatetemclientslottosecond, updatetemclientprice,
            updatetemclientmaxprice, updatetemclientbid;

    // Escerts
    private Spinner escertsetxt_fromdate;
    private TextView escertsetxt_fromdate_set;
    private ArrayList<String> escertsdate_array = new ArrayList<String>();
    private ArrayList<String> escertsprintdate_array = new ArrayList<String>();
    private ArrayAdapter<String> escerts_adapter_date;
    private RadioButton escertsbuyrb1, escertssellrb2, rbtypeescerts1, rbtypeescerts2;
    private RadioGroup rgescertsbiddigntype, rgescertstype;
    private Button btgobidescertssave;
    private RadioButton rgescerttypebutton, rgescertbiddigntypebutton;
    private String rgescerttypebuttongettypeSend = "", rgescertbiddigntypebuttonSend = "", escertplacedbiddatesend = "";

    private ArrayList<String> serialnumberplaceescert = new ArrayList<String>();
    private ArrayList<String> addbididescert = new ArrayList<String>();
    private ArrayList<String> addbidclientidescert = new ArrayList<String>();
    private ArrayList<String> addbiddateescert = new ArrayList<String>();
    private ArrayList<String> addbidtypeescert = new ArrayList<String>();
    private ArrayList<String> addbidpriceescert = new ArrayList<String>();
    private ArrayList<String> addbidnoofescert = new ArrayList<String>();
    private ArrayList<String> addbidordernatureescert = new ArrayList<String>();
    private ArrayList<String> addbidstatusescert = new ArrayList<String>();
    private ArrayList<String> addbidtstatusescert = new ArrayList<String>();
    private ArrayList<String> addbidextraescert = new ArrayList<String>();
    private ArrayList<String> addbidentrybyescert = new ArrayList<String>();
    private ArrayList<String> addbidtimestampescert = new ArrayList<String>();

    private String[] escertseriala, serialnumberadapterescert, idadapterescert, clientidadapterescert,
            dateadapterescert, typeadapterescert, priceadapterescert, noofrecadapterescert,
            ordernatureadapterescert, statusadapterescert, tstatusadapterescert, extraadapterescert, entrybyadapterescert,
            timestampadapterescert;

    private LinearLayout lladdbidsaveescert, lladdbidcancelescert, lladdbidsubmitescert, llescertlistheading;
    private NonScrollListView addbidescertlist;
    private HashMap<Integer, AddBidGetAllDetailsRec> myHashmapescert;
    private AddBidGetAllDetailsRec mypojoescert;
    private Button btaddbidescert;
    private EditText popupetpriceescerts, popupetnoofcertificateescerts;
    private LinearLayout tvcrossescerts;
    private Button popupButtonSaveescerts, popupButtoncancelescerts;
    private String popupclientpriceescerts, popupclientnoofblockescerts;
    private EditText updateetpriceescert, updateetnoofcertificateescert;
    private TextView tvupdateminescert, tvupdatemaxescert;
    private String ideditescert, clientideditescert, dateeditescert, typeeditescert, priceeditescert, noofcertificateeditescert, ordernatureeditescert, statuseditescert, tstatuseditescert, extraeditescert, entrybyeditescert, timestampeditescert;
    private String updateclientpriceescert, updateclientnoofcertificateescert;
    private String iddeleteescert, clientiddeleteescert, datedeleteescert, typedeleteescert, pricedeleteescert, noofrecdeleteescert, ordernaturedeleteescert, statusdeleteescert, tstatusdeleteescert, extradeleteescert, entrybydeleteescert, timestampdeleteescert;
    private Button btcancelbidescert, btsubmitbidescert;
    private String rgescertstypebuttongettypeSend, rgescertsbiddigntypebuttonSend;
    private String cancelallidescert = "";
    private String domain;
    private PopupWindow pwindo;
    private PopupWindow pwindotam;
    private PopupWindow pwindorec;
    private PopupWindow pwindoescerts;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_newbid);
        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM baby", null);
        if (c.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("access_key: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
        }


        Cursor recpriceTablec = db.rawQuery("SELECT * FROM recpriceTable", null);
        if (recpriceTablec.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        StringBuffer menuTablecbuffer = new StringBuffer();
        while (recpriceTablec.moveToNext()) {
            menuTablecbuffer.append("menuTablecbuffer: " + recpriceTablec.getString(0) + "\n");
            String recprice = recpriceTablec.getString(0);
            //Toast.makeText(this, recprice, Toast.LENGTH_LONG).show();
            try {
                JSONObject mainObject = new JSONObject(recprice);
                if (mainObject.has("recprice")) {
                    JSONObject recpriceobject = mainObject.getJSONObject("recprice");
                    JSONObject solor_details = recpriceobject.getJSONObject("solor");
                    solormax = solor_details.getString("max");
                    solormin = solor_details.getString("min");
                    JSONObject nonsolor_details = recpriceobject.getJSONObject("nonsolor");
                    nonsolormax = nonsolor_details.getString("max");
                    nonsolormin = nonsolor_details.getString("min");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");

        Intent in = getIntent();
        companyName = in.getStringExtra("companyName");
        exchangetype = in.getStringExtra("name");
        comeFrom = in.getStringExtra("from");

        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        prgDialog.setCancelable(false);

        myHashmap = new HashMap<Integer, AddBidGetAllDetails>();
        myHashmaprec = new HashMap<Integer, AddBidGetAllDetailsRec>();
        myHashmapescert = new HashMap<Integer, AddBidGetAllDetailsRec>();
        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);

        /* FOR GET DATA API CALL */
        //checkboxtype = (CheckBox) findViewById(R.id.checkboxtype);
        lltype = (RadioGroup) findViewById(R.id.lltype);
        RadioButton rb1type = (RadioButton) findViewById(R.id.rb1type);
        RadioButton rb2type = (RadioButton) findViewById(R.id.rb2type);
        if (exchangetype.equalsIgnoreCase("IEX")) {
            lltype.check(rb1type.getId());
        } else if (exchangetype.equalsIgnoreCase("PXIL")) {
            lltype.check(rb2type.getId());
        } else {

        }

        rgbiddigntype = (RadioGroup) findViewById(R.id.rgbiddigntype);
        rgbiddigndate = (RadioGroup) findViewById(R.id.rgbiddigndate);
        etxt_fromdate = (TextView) findViewById(R.id.etxt_fromdate); // invisible
        etxt_fromdate_set = (TextView) findViewById(R.id.etxt_fromdate_set); // invisible
        btgobidsave = (Button) findViewById(R.id.btgobidsave);

        llnewbidsecondheading = (LinearLayout) findViewById(R.id.llnewbidsecondheading);// invisible
        tvbidtype = (TextView) findViewById(R.id.tvbidtype);
        clientusename = (TextView) findViewById(R.id.clientusename);
        clientusename.setText(companyName);
        llsavecancel = (LinearLayout) findViewById(R.id.llsavecancel); // invisible
        lladdbidsave = (LinearLayout) findViewById(R.id.lladdbidsave); // Gone
        lladdbidsubmit = (LinearLayout) findViewById(R.id.lladdbidsubmit); // Gone
        lladdbidcancel = (LinearLayout) findViewById(R.id.lladdbidcancel); // Gone
        btaddbid = (TextView) findViewById(R.id.btaddbid);
        cbclienttype = (CheckBox) findViewById(R.id.cbclienttype);
        etslotfrom = (EditText) findViewById(R.id.etslotfrom);
        etslotto = (EditText) findViewById(R.id.etslotto);
        etprice = (EditText) findViewById(R.id.etprice);
        etbid = (EditText) findViewById(R.id.etbid);
        etnoofbloack = (EditText) findViewById(R.id.etnoofbloack);
        btsaveclientbid = (Button) findViewById(R.id.btsaveclientbid);
        btcancelclientbid = (Button) findViewById(R.id.btcancelclientbid);

        btcancelbid = (Button) findViewById(R.id.btcancelbid);
        btsubmitbid = (Button) findViewById(R.id.btsubmitbid);
        Animation mAnimation = new AlphaAnimation(1, 0);
        mAnimation.setDuration(200);
        mAnimation.setInterpolator(new LinearInterpolator());
        mAnimation.setRepeatCount(Animation.INFINITE);
        mAnimation.setRepeatMode(Animation.REVERSE);
        btsubmitbid.startAnimation(mAnimation);

        lladdbidform = (LinearLayout) findViewById(R.id.lladdbidform); // Gone
        lllistheading = (LinearLayout) findViewById(R.id.lllistheading); // Gone
        addbidlist = (NonScrollListView) findViewById(R.id.addbidlist); // Gone

        /////// copy bid
        Button btncopybid = (Button) findViewById(R.id.btncopybid);

        /////// Previous bid
        Button btnbid_summery = (Button) findViewById(R.id.btnbid_summery);

        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);

        //////////////////  open tabbbing

        final Animation myAnimation = AnimationUtils.loadAnimation(this, R.anim.blink);
        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView_DAM);
        final ScrollView Tam_activity = (ScrollView) findViewById(R.id.Tam_activity);
        final ScrollView recplacebidtabscroolview = (ScrollView) findViewById(R.id.recplacebidtabscroolview);
        final ScrollView escertstabscroolview = (ScrollView) findViewById(R.id.escertstabscroolview);

        final Button dam = (Button) findViewById(R.id.tab_DAM);
        final Button damTwo = (Button) findViewById(R.id.tab_DAM2);
        final Button tam = (Button) findViewById(R.id.tab_TAM);
        final Button tamTwo = (Button) findViewById(R.id.tab_TAM2);
        final Button rec = (Button) findViewById(R.id.tab_REC);
        final Button recTwo = (Button) findViewById(R.id.tab_REC2);

        final Button escerts = (Button) findViewById(R.id.tab_Escerts);
        final Button escertsTwo = (Button) findViewById(R.id.tab_Escerts2);


        damTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dam.setVisibility(View.VISIBLE);
                damTwo.setVisibility(View.GONE);
                tam.setVisibility(View.GONE);
                tamTwo.setVisibility(View.VISIBLE);
                rec.setVisibility(View.GONE);
                recTwo.setVisibility(View.VISIBLE);
                escerts.setVisibility(View.GONE);
                escertsTwo.setVisibility(View.VISIBLE);

                dam.startAnimation(myAnimation);

                scrollView.setVisibility(View.VISIBLE);
                Tam_activity.setVisibility(View.GONE);
                recplacebidtabscroolview.setVisibility(View.GONE);
                escertstabscroolview.setVisibility(View.GONE);
            }
        });

        tamTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dam.setVisibility(View.GONE);
                damTwo.setVisibility(View.VISIBLE);
                tam.setVisibility(View.VISIBLE);
                tamTwo.setVisibility(View.GONE);
                rec.setVisibility(View.GONE);
                recTwo.setVisibility(View.VISIBLE);
                escerts.setVisibility(View.GONE);
                escertsTwo.setVisibility(View.VISIBLE);

                tam.startAnimation(myAnimation);

                scrollView.setVisibility(View.GONE);
                Tam_activity.setVisibility(View.VISIBLE);
                recplacebidtabscroolview.setVisibility(View.GONE);
                escertstabscroolview.setVisibility(View.GONE);
            }
        });

        recTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dam.setVisibility(View.GONE);
                damTwo.setVisibility(View.VISIBLE);
                tam.setVisibility(View.GONE);
                tamTwo.setVisibility(View.VISIBLE);
                rec.setVisibility(View.VISIBLE);
                recTwo.setVisibility(View.GONE);
                escerts.setVisibility(View.GONE);
                escertsTwo.setVisibility(View.VISIBLE);

                rec.startAnimation(myAnimation);

                scrollView.setVisibility(View.GONE);
                Tam_activity.setVisibility(View.GONE);
                recplacebidtabscroolview.setVisibility(View.VISIBLE);
                escertstabscroolview.setVisibility(View.GONE);
                new HttpAsyncTaskRECgetallrectradingdate().execute(domain + "/mobile/pxs_app/service/rec/getallrectradingdate.php");

            }
        });

        escertsTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dam.setVisibility(View.GONE);
                damTwo.setVisibility(View.VISIBLE);
                tam.setVisibility(View.GONE);
                tamTwo.setVisibility(View.VISIBLE);
                rec.setVisibility(View.GONE);
                recTwo.setVisibility(View.VISIBLE);
                escerts.setVisibility(View.VISIBLE);
                escertsTwo.setVisibility(View.GONE);

                escerts.startAnimation(myAnimation);

                scrollView.setVisibility(View.GONE);
                Tam_activity.setVisibility(View.GONE);
                recplacebidtabscroolview.setVisibility(View.GONE);
                escertstabscroolview.setVisibility(View.VISIBLE);
                new HttpAsyncTaskEscertsgetallrectradingdate().execute(domain + "/mobile/pxs_app/service/escert/getallescerttradingdate.php");

            }
        });


        Animation animationToRight = new TranslateAnimation(-200, 200, 0, 0);
        animationToRight.setDuration(3000);
        /*animationToRight.setRepeatMode(Animation.RESTART);*/
        animationToRight.setRepeatCount(Animation.INFINITE);


        // TextView tam_tv=(TextView) findViewById(R.id.textView_tam);
        // tam_tv.startAnimation(animationToRight);


        // TAM PLACE BID IDS
        tam_sp_selectproduct = (Spinner) findViewById(R.id.tam_sp_selectproduct);
        selectproductArray.add("ITD");
        selectproductArray.add("DSE");
        selectproductArray.add("DAILY");
        selectproductArray.add("WEEKLY");
        // Creating adapter for spinner
        ArrayAdapter<String> madapter = new ArrayAdapter<String>(NewBidActivity.this, R.layout.spinner_item_type, selectproductArray);
        // Drop down layout style - list view with radio button
        madapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        tam_sp_selectproduct.setAdapter(madapter);
        tam_sp_selectproduct.setOnItemSelectedListener(this);

        ll_radiogrp_platform = (RadioGroup) findViewById(R.id.ll_radiogrp_platform);
        rb1_paltform = (RadioButton) findViewById(R.id.rb1_paltform);
        rb2_paltform = (RadioButton) findViewById(R.id.rb2_paltform);
        tam_tv_delivery = (TextView) findViewById(R.id.tam_tv_delivery);
        btn_go_tam = (Button) findViewById(R.id.btn_go_tam);

        btn_tam_newbutton = (Button) findViewById(R.id.btn_tam_newbutton);
        ll_tam_cancelbutton = (LinearLayout) findViewById(R.id.ll_tam_cancelbutton);
        ll_tam_submitbutton = (LinearLayout) findViewById(R.id.ll_tam_submitbutton);
        btn_tam_cancelbutton = (Button) findViewById(R.id.btn_tam_cancelbutton);
        btn_tam_submitbutton = (Button) findViewById(R.id.btn_tam_submitbutton);
        btn_tam_submitbutton.startAnimation(mAnimation);

        ll_tam_second_heading = (LinearLayout) findViewById(R.id.ll_tam_second_heading);
        ll_tam_new_cancel_submit = (LinearLayout) findViewById(R.id.ll_tam_new_cancel_submit);
        ll_TAMlist_heading = (LinearLayout) findViewById(R.id.ll_TAMlist_heading);
        add_tam_list = (NonScrollListView) findViewById(R.id.add_tam_list);

        // REC PLACE BID IDS
        rgrectype = (RadioGroup) findViewById(R.id.rgrectype);
        rbtyperec1 = (RadioButton) findViewById(R.id.rbtyperec1);
        rbtyperec2 = (RadioButton) findViewById(R.id.rbtyperec2);
        rgrecbiddigntype = (RadioGroup) findViewById(R.id.rgrecbiddigntype);
        buyrb1 = (RadioButton) findViewById(R.id.buyrb1);
        sellrb2 = (RadioButton) findViewById(R.id.sellrb2);
        recetxt_fromdate = (Spinner) findViewById(R.id.recetxt_fromdate);
        recetxt_fromdate.setOnItemSelectedListener(this);
        recetxt_fromdate_set = (TextView) findViewById(R.id.recetxt_fromdate_set);


        btgobidrecsave = (Button) findViewById(R.id.btgobidrecsave);
        llsavecancelrec = (LinearLayout) findViewById(R.id.llsavecancelrec);
        lladdbidsaverec = (LinearLayout) findViewById(R.id.lladdbidsaverec);
        lladdbidcancelrec = (LinearLayout) findViewById(R.id.lladdbidcancelrec);
        btcancelbidrec = (Button) findViewById(R.id.btcancelbidrec);
        lladdbidsubmitrec = (LinearLayout) findViewById(R.id.lladdbidsubmitrec);
        btsubmitbidrec = (Button) findViewById(R.id.btsubmitbidrec);

        Animation mAnimationsub = new AlphaAnimation(1, 0);
        mAnimationsub.setDuration(200);
        mAnimationsub.setInterpolator(new LinearInterpolator());
        mAnimationsub.setRepeatCount(Animation.INFINITE);
        mAnimationsub.setRepeatMode(Animation.REVERSE);
        btsubmitbidrec.startAnimation(mAnimationsub);

        llreclistheading = (LinearLayout) findViewById(R.id.llreclistheading);
        addbidreclist = (NonScrollListView) findViewById(R.id.addbidreclist);
        btaddbidrec = (Button) findViewById(R.id.btaddbidrec);

        //// Escerts BID IDS
        escertsetxt_fromdate = (Spinner) findViewById(R.id.escertsetxt_fromdate);
        escertsetxt_fromdate.setOnItemSelectedListener(this);
        escertsetxt_fromdate_set = (TextView) findViewById(R.id.escertsetxt_fromdate_set);
        rgescertstype = (RadioGroup) findViewById(R.id.rgescertstype);
        rgescertsbiddigntype = (RadioGroup) findViewById(R.id.rgescertsbiddigntype);
        escertsbuyrb1 = (RadioButton) findViewById(R.id.escertsbuyrb1);
        escertssellrb2 = (RadioButton) findViewById(R.id.escertssellrb2);
        rbtypeescerts1 = (RadioButton) findViewById(R.id.rbtypeescerts1);
        rbtypeescerts2 = (RadioButton) findViewById(R.id.rbtypeescerts2);
        btgobidescertssave = (Button) findViewById(R.id.btgobidescertssave);

        lladdbidsaveescert = (LinearLayout) findViewById(R.id.lladdbidsaveescert);
        lladdbidcancelescert = (LinearLayout) findViewById(R.id.lladdbidcancelescert);
        lladdbidsubmitescert = (LinearLayout) findViewById(R.id.lladdbidsubmitescert);
        llescertlistheading = (LinearLayout) findViewById(R.id.llescertlistheading);
        addbidescertlist = (NonScrollListView) findViewById(R.id.addbidescertlist);
        btaddbidescert = (Button) findViewById(R.id.btaddbidescert);
        btcancelbidescert = (Button) findViewById(R.id.btcancelbidescert);
        btsubmitbidescert = (Button) findViewById(R.id.btsubmitbidescert);
        btsubmitbidescert.startAnimation(mAnimationsub);

        btgobidescertssave.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                int selectedlltypeId = rgescertstype.getCheckedRadioButtonId();
                rgescerttypebutton = (RadioButton) findViewById(selectedlltypeId);
                if (rgescerttypebutton != null) {
                    rgescerttypebuttongettypeSend = (String) rgescerttypebutton.getText();
                }

                int selectedbiddingtypeId = rgescertsbiddigntype.getCheckedRadioButtonId();
                rgescertbiddigntypebutton = (RadioButton) findViewById(selectedbiddingtypeId);
                if (rgescertbiddigntypebutton != null) {
                    rgescertbiddigntypebuttonSend = (String) rgescertbiddigntypebutton.getText();
                    //tvbidtype.setText(biddingtypeSend+" Details");
                }

                if ((rgescerttypebuttongettypeSend.length() != 0) && (rgescertbiddigntypebuttonSend.length() != 0) && (!escertplacedbiddatesend.equalsIgnoreCase("Select Trading Date"))) {
                    ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                    if (!cd.isConnectingToInternet()) {
                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        new HttpAsyncTaskNewBidgetescertbiddetail().execute(domain + "/mobile/pxs_app/service/escert/newbid/getescertbiddetail.php");
                    }
                } else {
                    Toast.makeText(NewBidActivity.this, "Please fill all the details.", Toast.LENGTH_LONG).show();
                }
            }
        });

        btaddbidescert.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                initiatePopupWindowESCERTS();
            }
        });

        btsubmitbidescert.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewBidActivity.this);
                alertDialogBuilder
                        .setMessage("Are you sure, you want to submit bid?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                int selectedlltypeId = rgescertstype.getCheckedRadioButtonId();
                                RadioButton rgescertstypebutton = (RadioButton) findViewById(selectedlltypeId);
                                if (rgescertstypebutton != null) {
                                    rgescertstypebuttongettypeSend = (String) rgescertstypebutton.getText();
                                }

                                int selectedbiddingtypeId = rgescertsbiddigntype.getCheckedRadioButtonId();
                                RadioButton rgescertsbiddigntypebutton = (RadioButton) findViewById(selectedbiddingtypeId);
                                if (rgescertsbiddigntypebutton != null) {
                                    rgescertsbiddigntypebuttonSend = (String) rgescertsbiddigntypebutton.getText();
                                    //tvbidtype.setText(biddingtypeSend+" Details");
                                }

                                if ((rgescertstypebuttongettypeSend.length() != 0) && (rgescertsbiddigntypebuttonSend != null) && (!escertplacedbiddatesend.equalsIgnoreCase("Select Trading Date"))) {
                                    ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                    if (!cd.isConnectingToInternet()) {
                                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                    } else {
                                        new HttpAsyncTaskNewBidgetescertsubmitrecbid().execute(domain + "/mobile/pxs_app/service/escert/newbid/submitesccertbid.php");
                                    }
                                } else {
                                    Toast.makeText(NewBidActivity.this, "Please fill all the details.", Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });


        btcancelbidescert.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewBidActivity.this);
                alertDialogBuilder
                        .setMessage("Are you sure, you want to cancel bid?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (idadapterescert != null) {
                                    if (idadapterescert.length == 1) {
                                        cancelallidescert = idadapterescert[0];
                                    } else {
                                        for (int i = 0; i < idadapterescert.length; i++) {
                                            cancelallidescert = cancelallidescert + idadapterescert[i] + ",";
                                        }
                                    }
                                    cancelallidescert = cancelallidescert.trim();
                                    int selectedlltypeId = rgescertstype.getCheckedRadioButtonId();
                                    rgescerttypebutton = (RadioButton) findViewById(selectedlltypeId);
                                    if (rgescerttypebutton != null) {
                                        rgescerttypebuttongettypeSend = (String) rgescerttypebutton.getText();
                                    }

                                    int selectedbiddingtypeId = rgescertsbiddigntype.getCheckedRadioButtonId();
                                    rgescertbiddigntypebutton = (RadioButton) findViewById(selectedbiddingtypeId);
                                    if (rgescertbiddigntypebutton != null) {
                                        rgescertbiddigntypebuttonSend = (String) rgescertbiddigntypebutton.getText();
                                    }

                                    ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                    if (!cd.isConnectingToInternet()) {
                                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                    } else {
                                        new HttpAsyncTaskNewSaveClientBidCancelALLSubmitDataEscert().execute(domain + "/mobile/pxs_app/service/escert/newbid/deletebid.php");
                                    }
                                } else {
                                    Toast.makeText(NewBidActivity.this, "Please add the bid first.", Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });


        btgobidrecsave.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                int selectedlltypeId = rgrectype.getCheckedRadioButtonId();
                rgrectypebutton = (RadioButton) findViewById(selectedlltypeId);
                if (rgrectypebutton != null) {
                    rgrectypebuttongettypeSend = (String) rgrectypebutton.getText();
                }

                int selectedbiddingtypeId = rgrecbiddigntype.getCheckedRadioButtonId();
                rgrecbiddigntypebutton = (RadioButton) findViewById(selectedbiddingtypeId);
                if (rgrecbiddigntypebutton != null) {
                    rgrecbiddigntypebuttonSend = (String) rgrecbiddigntypebutton.getText();
                    //tvbidtype.setText(biddingtypeSend+" Details");
                }

                if ((rgrectypebuttongettypeSend.length() != 0) && (rgrecbiddigntypebuttonSend.length() != 0) && (!recplacedbiddatesend.equalsIgnoreCase("Select Trading Date"))) {
                    ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                    if (!cd.isConnectingToInternet()) {
                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        //new HttpAsyncTaskNewBidGetBidDetail().execute("https://www.mittalpower.com/mobile/pxs_app/service/newbid/getbiddetail.php");
                        new HttpAsyncTaskNewBidgetrecbiddetail().execute(domain + "/mobile/pxs_app/service/rec/newbid/getrecbiddetail.php");
                    }
                } else {
                    Toast.makeText(NewBidActivity.this, "Please fill all the details.", Toast.LENGTH_LONG).show();
                }
            }
        });


        btaddbidrec.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                rgrectypesolorbuttonSend = "";
                initiatePopupWindowREC();
            }
        });

        btcancelbidrec.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewBidActivity.this);
                // set dialog message
                alertDialogBuilder
                        .setMessage("Are you sure, you want to cancel bid?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, close
                                // current activity
                                //Toast.makeText(getApplicationContext(), idadapterrec[+id], Toast.LENGTH_LONG).show();
                                if (idadapterrec != null) {
                                    if (idadapterrec.length == 1) {
                                        cancelallidrec = idadapterrec[0];
                                    } else {
                                        for (int i = 0; i < idadapterrec.length; i++) {
                                            cancelallidrec = cancelallidrec + idadapterrec[i] + ",";
                                        }
                                    }
                                    cancelallidrec = cancelallidrec.trim();
                                    //cancelallidrec = cancelallidrec.substring(0, cancelallid.length() - 1);
                                    //System.out.println(cancelallid);
                                    //Toast.makeText(getActivity(), cancelallid.toString(), Toast.LENGTH_LONG).show();
                                    int selectedlltypeId = rgrectype.getCheckedRadioButtonId();
                                    rgrectypebutton = (RadioButton) findViewById(selectedlltypeId);
                                    if (rgrectypebutton != null) {
                                        rgrectypebuttongettypeSend = (String) rgrectypebutton.getText();
                                    }

                                    int selectedbiddingtypeId = rgrecbiddigntype.getCheckedRadioButtonId();
                                    rgrecbiddigntypebutton = (RadioButton) findViewById(selectedbiddingtypeId);
                                    if (rgrecbiddigntypebutton != null) {
                                        rgrecbiddigntypebuttonSend = (String) rgrecbiddigntypebutton.getText();
                                        //tvbidtype.setText(biddingtypeSend+" Details");
                                    }

                                    ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                    if (!cd.isConnectingToInternet()) {
                                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                    } else {
                                        new HttpAsyncTaskNewSaveClientBidCancelALLSubmitDataREC().execute(domain + "/mobile/pxs_app/service/rec/newbid/deletebid.php");
                                    }
                                } else {
                                    Toast.makeText(NewBidActivity.this, "Please add the bid first.", Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });


        btsubmitbidrec.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewBidActivity.this);

                // set title
                //alertDialogBuilder.setTitle("Submit Bid:");
                // set dialog message
                alertDialogBuilder
                        .setMessage("Are you sure, you want to submit bid?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                int selectedlltypeId = rgrectype.getCheckedRadioButtonId();
                                rgrectypebutton = (RadioButton) findViewById(selectedlltypeId);
                                if (rgrectypebutton != null) {
                                    rgrectypebuttongettypeSend = (String) rgrectypebutton.getText();
                                }

                                int selectedbiddingtypeId = rgrecbiddigntype.getCheckedRadioButtonId();
                                rgrecbiddigntypebutton = (RadioButton) findViewById(selectedbiddingtypeId);
                                if (rgrecbiddigntypebutton != null) {
                                    rgrecbiddigntypebuttonSend = (String) rgrecbiddigntypebutton.getText();
                                    //tvbidtype.setText(biddingtypeSend+" Details");
                                }

                                if ((rgrectypebuttongettypeSend.length() != 0) && (rgrecbiddigntypebuttonSend != null) && (!recplacedbiddatesend.equalsIgnoreCase("Select Trading Date"))) {
                                    ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                    if (!cd.isConnectingToInternet()) {
                                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                    } else {
                                        //new HttpAsyncTaskNewBidGetBidDetail().execute("https://www.mittalpower.com/mobile/pxs_app/service/newbid/getbiddetail.php");
                                        new HttpAsyncTaskNewBidgetrecsubmitrecbid().execute(domain + "/mobile/pxs_app/service/rec/newbid/submitrecbid.php");
                                    }
                                } else {
                                    Toast.makeText(NewBidActivity.this, "Please fill all the details.", Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });

        ////////close tabbing

        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });


        rgbiddigndate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                biddingdate = (RadioButton) findViewById(checkedId);
                biddingdateSend = (String) biddingdate.getText();
                if (biddingdateSend.equalsIgnoreCase("Future Date")) {
                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.DATE, +1);
                    System.out.println("Current time => " + c.getTime());
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = df.format(c.getTime());
                    // formattedDate have current date/time
                    String[] parts = formattedDate.split(" ");
                    String part1 = parts[0]; // 004
                    String part2 = parts[1]; // 004
                    String[] datesparts = part1.split("-");
                    String datepart1 = datesparts[0];
                    String datepart2 = datesparts[1];
                    String datepart3 = datesparts[2];

                    etxt_fromdate.setText(datepart3 + "-" + datepart2 + "-" + datepart1);
                    etxt_fromdate_set.setText(datepart1 + "-" + datepart2 + "-" + datepart3);
                    etxt_fromdate.setVisibility(View.VISIBLE);
                    as = etxt_fromdate_set.getText().toString();
                }
                if (biddingdateSend.equalsIgnoreCase("Today")) {
                    etxt_fromdate.setText("");
                    etxt_fromdate.setVisibility(View.VISIBLE);
                    Calendar c = Calendar.getInstance();
                    System.out.println("Current time => " + c.getTime());
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = df.format(c.getTime());
                    // formattedDate have current date/time
                    String[] parts = formattedDate.split(" ");
                    String part1 = parts[0]; // 004
                    String[] datesparts = part1.split("-");
                    String datepart1 = datesparts[0];
                    String datepart2 = datesparts[1];
                    String datepart3 = datesparts[2];
                    etxt_fromdate.setText(datepart3 + "-" + datepart2 + "-" + datepart1);
                    etxt_fromdate_set.setText(datepart1 + "-" + datepart2 + "-" + datepart3);
                    as = etxt_fromdate_set.getText().toString();
                }
            }
        });

        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                System.out.println("Current time => " + c.getTime());
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String formattedDate = df.format(c.getTime());
                // formattedDate have current date/time
                String[] parts = formattedDate.split(" ");
                String part1 = parts[0]; // 004
                String[] datesparts = part1.split("-");
                String datepart1 = datesparts[0];
                String datepart2 = datesparts[1];
                String datepart3 = datesparts[2];
                String checktodaydate = datepart1 + "-" + datepart2 + "-" + datepart3;
                if (comeFrom.equalsIgnoreCase("MainActivity")) {
                    as = etxt_fromdate_set.getText().toString();
                    if (as.length() == 0) {
                        Intent in = new Intent(NewBidActivity.this, MainActivityAfterLogin.class);
                        startActivity(in);
                        overridePendingTransition(R.anim.animation, R.anim.animation2);
                        finish();
                    } else if (as.equalsIgnoreCase(checktodaydate)) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                NewBidActivity.this);
                        alertDialogBuilder
                                .setMessage("Are you sure you want to go back without submit bid?");
                        alertDialogBuilder.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        Intent in = new Intent(NewBidActivity.this, MainActivityAfterLogin.class);
                                        startActivity(in);
                                        overridePendingTransition(R.anim.animation, R.anim.animation2);
                                        finish();
                                    }
                                });
                        alertDialogBuilder.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        // finish();
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else if (as.equalsIgnoreCase(checktodaydate) && addbidstatus.contains("FALSE")) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                NewBidActivity.this);
                        alertDialogBuilder
                                .setMessage("Are you sure you want to go back without submit bid?");
                        alertDialogBuilder.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        Intent in = new Intent(NewBidActivity.this, MainActivityAfterLogin.class);
                                        startActivity(in);
                                        overridePendingTransition(R.anim.animation, R.anim.animation2);
                                        finish();
                                    }
                                });

                        alertDialogBuilder.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        // finish();
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else {
                        Intent in = new Intent(NewBidActivity.this, MainActivityAfterLogin.class);
                        startActivity(in);
                        overridePendingTransition(R.anim.animation, R.anim.animation2);
                        finish();
                    }
                } else if (comeFrom.equalsIgnoreCase("HomeActivity")) {
                    Intent in = new Intent(NewBidActivity.this, HomeActivity.class);
                    in.putExtra("companyName", companyName);
                    startActivity(in);
                    overridePendingTransition(R.anim.animation, R.anim.animation2);
                    finish();
                } else {
                    Intent in = new Intent(NewBidActivity.this, MainActivityAfterLogin.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.animation, R.anim.animation2);
                    finish();
                }
            }
        });

        etxt_fromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        btaddbid.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                initiatePopupWindow();
            }
        });

        btcancelbid.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewBidActivity.this);
                // set title
                //alertDialogBuilder.setTitle("Cancel Bid:");
                // set dialog message
                alertDialogBuilder
                        .setMessage("Are you sure, you want to cancel bid?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, close
                                // current activity
                                if (idadapter != null) {
                                    for (int i = 0; i < idadapter.length; i++) {
                                        cancelallid = cancelallid + idadapter[i] + ",";
                                    }
                                    cancelallid = cancelallid.trim();
                                    cancelallid = cancelallid.substring(0, cancelallid.length() - 1);
                                    //System.out.println(cancelallid);
                                    //Toast.makeText(getActivity(), cancelallid.toString(), Toast.LENGTH_LONG).show();
                                    for (int i = 0; i < dateadapter.length; i++) {
                                        cancelalldate = cancelalldate + dateadapter[i] + ",";
                                    }
                                    cancelalldate = cancelalldate.trim();
                                    cancelalldate = cancelalldate.substring(0, cancelalldate.length() - 1);
                                    String[] parts = cancelalldate.split(",");
                                    cancelalldateSend = parts[0]; // 004
                                    //String part2 = parts[1]; // 034556
                                    System.out.println(cancelalldate);
                                    //Toast.makeText(getActivity(), cancelalldate, Toast.LENGTH_LONG).show();
                                    ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                    if (!cd.isConnectingToInternet()) {
                                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                    } else {
                                        new HttpAsyncTaskNewSaveClientBidCancelALLSubmitData().execute(domain + "/mobile/pxs_app/service/newbid/deletebid.php");
                                    }
                                } else {
                                    Toast.makeText(NewBidActivity.this, "Please add the bid first.", Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });

        btgobidsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
				/*if (checkboxtype.isChecked() ) {
					gettype = (String) checkboxtype.getText();
				}*/

                int selectedlltypeId = lltype.getCheckedRadioButtonId();
                lliexbuttontype = (RadioButton) findViewById(selectedlltypeId);
                if (lliexbuttontype != null) {
                    gettype = (String) lliexbuttontype.getText();
                }

                int selectedbiddingtypeId = rgbiddigntype.getCheckedRadioButtonId();
                biddingtype = (RadioButton) findViewById(selectedbiddingtypeId);
                if (biddingtype != null) {
                    biddingtypeSend = (String) biddingtype.getText();
                    tvbidtype.setText(biddingtypeSend + " Details");
                }

                if ((gettype.length() != 0) && (biddingtype != null) && (biddingdateSend != null)) {
					/*if(biddingtypeSend.equalsIgnoreCase("Block Bid")){
						biddingtypeSend = "BLOCK";
					}
					else {
						biddingtypeSend = "BLOCK";
					}*/
                    ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                    if (!cd.isConnectingToInternet()) {
                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        new HttpAsyncTaskNewBidGetBidDetail().execute(domain + "/mobile/pxs_app/service/newbid/getbiddetail.php");
                    }
                } else {
                    Toast.makeText(NewBidActivity.this, "Please fill all the details.", Toast.LENGTH_LONG).show();
                }
                //gettype = "";
            }
        });

        btsaveclientbid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                int selectedbiddingtypeId = rgbiddigntype
                        .getCheckedRadioButtonId();
                biddingtype = (RadioButton) findViewById(selectedbiddingtypeId);
                biddingtypeSend = (String) biddingtype.getText();
				/*if (checkboxtype.isChecked()) {
					gettype = (String) checkboxtype.getText();
				}*/
                int selectedlltypeId = lltype.getCheckedRadioButtonId();
                lliexbuttontype = (RadioButton) findViewById(selectedlltypeId);
                if (lliexbuttontype != null) {
                    gettype = (String) lliexbuttontype.getText();
                }
                if (cbclienttype.isChecked()) {
                    getclienttype = (String) cbclienttype.getText();
                }
                clientslotfrom = etslotfrom.getText().toString();
                clientslotto = etslotto.getText().toString();
                clientprice = etprice.getText().toString();
                clientbid = etbid.getText().toString();
                clientnoofblock = etnoofbloack.getText().toString();
                ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                if (!cd.isConnectingToInternet()) {
                    Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    new HttpAsyncTaskNewSaveClientBidSubmitData().execute(domain + "/mobile/pxs_app/service/newbid/saveclientbid.php");
                    lladdbidform.setVisibility(View.GONE);
                    biddingtype.setChecked(false);
                    etslotfrom.setText(" ");
                    etslotto.setText(" ");
                    etprice.setText(" ");
                    etbid.setText(" ");
                    etnoofbloack.setText(" ");
                }
            }
        });


        btsubmitbid.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewBidActivity.this);
                // set title
                //alertDialogBuilder.setTitle("Submit Bid:");
                // set dialog message
                alertDialogBuilder
                        .setMessage("Are you sure, you want to submit bid?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                int selectedlltypeId = lltype.getCheckedRadioButtonId();
                                lliexbuttontype = (RadioButton) findViewById(selectedlltypeId);
                                if (lliexbuttontype != null) {
                                    gettype = (String) lliexbuttontype.getText();
                                }

                                int selectedbiddingtypeId = rgbiddigntype.getCheckedRadioButtonId();
                                biddingtype = (RadioButton) findViewById(selectedbiddingtypeId);
                                if (biddingtype != null) {
                                    biddingtypeSend = (String) biddingtype.getText();
                                }

                                if ((gettype != null) && (biddingtype != null) && (biddingdateSend != null) && (as != null)) {
                                    ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                    if (!cd.isConnectingToInternet()) {
                                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                    } else {
                                        new HttpAsyncTaskNewBidSubmitClientBidSubmitData().execute(domain + "/mobile/pxs_app/service/newbid/submitbid.php");
                                    }
                                } else {
                                    Toast.makeText(NewBidActivity.this, "Please fill all the details.", Toast.LENGTH_LONG).show();
                                }

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });

        btcancelclientbid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Animation animSideup = AnimationUtils.loadAnimation(
                        NewBidActivity.this, R.anim.slide_up);
                lladdbidform.setVisibility(View.GONE);
                lladdbidform.startAnimation(animSideup);
                biddingtype.setChecked(false);
                etslotfrom.setText(" ");
                etslotto.setText(" ");
                etprice.setText(" ");
                etbid.setText(" ");
                etnoofbloack.setText(" ");
            }
        });
        // openPopupNewBid = (LinearLayout) rootView.findViewById(R.id.openPopupNewBid);
        // logoutpopup();
        btncopybid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Uri.parse("android.resource://" + NewBidActivity.this.getPackageName() + "your_sound_file_name.mp3");
                Intent in = new Intent(NewBidActivity.this, CopyBidActivity.class);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();

            }
        });

        btnbid_summery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Uri.parse("android.resource://" + NewBidActivity.this.getPackageName() + "your_sound_file_name.mp3");
                Intent in = new Intent(NewBidActivity.this, Previous_Bid_Activity.class);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();

            }
        });


        // TAM CONTROL

        tam_tv_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new TamSelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        btn_go_tam.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                int selectedlltypeId = ll_radiogrp_platform.getCheckedRadioButtonId();
                rb1_paltform = (RadioButton) findViewById(selectedlltypeId);
                if (rb1_paltform != null) {
                    rb1_paltformproducttypeSend = (String) rb1_paltform.getText();
                }

                if ((producttypesend.length() != 0) && (rb1_paltformproducttypeSend.length() != 0) && (tam_date_Send.length() != 0)) {
                    ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                    if (!cd.isConnectingToInternet()) {
                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        new HttpAsyncTaskgettambiddetails().execute(domain + Constant.GET_TAM_BIDDETAIL);
                    }
                } else {
                    Toast.makeText(NewBidActivity.this, "Please fill all the details.", Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_tam_newbutton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                initiatePopupWindowForTam();
            }
        });


        btn_tam_cancelbutton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewBidActivity.this);
                // set dialog message
                alertDialogBuilder
                        .setMessage("Are you sure, you want to cancel bid?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (tamgetid != null) {
                                    if (tamgetid.size() == 1) {
                                        tamcancelallid = tamgetid.get(0);
                                    } else {
                                        for (int i = 0; i < tamgetid.size(); i++) {
                                            tamcancelallid = tamcancelallid + tamgetid.get(i) + ",";
                                        }
                                    }
                                    tamcancelallid = tamcancelallid.trim();

                                    ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                    if (!cd.isConnectingToInternet()) {
                                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                    } else {
                                        new HttpAsyncTaskalldeletetambid()
                                                .execute(domain + Constant.DELETE_TAM_BID);

                                    }
                                } else {
                                    Toast.makeText(NewBidActivity.this, "Please add the bid first.", Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });


        btn_tam_submitbutton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewBidActivity.this);
                alertDialogBuilder
                        .setMessage("Are you sure, you want to submit bid?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                if (!cd.isConnectingToInternet()) {
                                    Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                } else {
                                    new HttpAsyncTasksubmittambid().execute(domain + Constant.SUBMIT_TAM_BID);
                                }

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });

    }

    private void initiatePopupWindow() {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.screen_popup,
                    (ViewGroup) findViewById(R.id.popup_element));
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            pwindo = new PopupWindow(layout, (width - 20), (height - 90), true);
            pwindo.showAtLocation(layout, Gravity.BOTTOM, 0, 0);


            //popupcheckboxtype = (CheckBox) layout.findViewById(R.id.popupcheckboxtype);
            //popuprgbiddigntype = (RadioGroup) layout.findViewById(R.id.popuprgbiddigntype);
            //popuprgbiddigndate = (RadioGroup) layout.findViewById(R.id.popuprgbiddigndate);
            //popupetxt_fromdate = (TextView) layout.findViewById(R.id.popupetxt_fromdate); // invisible

            popupcbclienttype = (RadioGroup) layout.findViewById(R.id.popupcbclienttype);
            popupetslotfromfirst = (TextView) layout.findViewById(R.id.popupetslotfromfirst);
            popupetslotfromcolon = (LinearLayout) layout.findViewById(R.id.popupetslotfromcolon);
            popupllslotfromsecond = (LinearLayout) layout.findViewById(R.id.popupllslotfromsecond);
            popupetslotfromsecond = (TextView) layout.findViewById(R.id.popupetslotfromsecond);
            popupllslotfromthird = (LinearLayout) layout.findViewById(R.id.popupllslotfromthird);
            popupetslotfromthird = (TextView) layout.findViewById(R.id.popupetslotfromthird);

            popupetslottofirst = (TextView) layout.findViewById(R.id.popupetslottofirst);
            popupetslottocolon = (LinearLayout) layout.findViewById(R.id.popupetslottocolon);
            popupllslottosecond = (LinearLayout) layout.findViewById(R.id.popupllslottosecond);
            popupetslottosecond = (TextView) layout.findViewById(R.id.popupetslottosecond);
            popupllslottothird = (LinearLayout) layout.findViewById(R.id.popupllslottothird);
            popupetslottothird = (TextView) layout.findViewById(R.id.popupetslottothird);


            popupetprice = (EditText) layout.findViewById(R.id.popupetprice);
            popupetbid = (EditText) layout.findViewById(R.id.popupetbid);
            popupetnoofbloack = (EditText) layout.findViewById(R.id.popupetnoofbloack);
            tvcross = (LinearLayout) layout.findViewById(R.id.tvcross);

            popupetslotfromfirst.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                    final String[] items = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
                            "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                    //set the title for alert dialog
                    builder.setTitle("Choose Select From HH:");
                    //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            // setting the button text to the selected itenm from the list
                            popupetslotfromfirst.setText(items[item]);
                            if (items[item].equalsIgnoreCase("24")) {
                                popupetslotfromcolon.setVisibility(View.VISIBLE);
                                popupllslotfromsecond.setVisibility(View.GONE);
                                popupllslotfromthird.setVisibility(View.VISIBLE);
                            } else {
                                popupetslotfromcolon.setVisibility(View.VISIBLE);
                                popupllslotfromsecond.setVisibility(View.VISIBLE);
                                popupllslotfromthird.setVisibility(View.GONE);
                            }
                        }
                    });
                    //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                    builder.setCancelable(false)
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //When clicked on CANCEL button the dalog will be dismissed
                                    dialog.dismiss();
                                }
                            });
                    // Creating alert dialog
                    AlertDialog alert = builder.create();
                    //Showing alert dialog
                    alert.show();
                }
            });

            popupetslotfromsecond.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                    final String[] items = {"00", "15", "30", "45"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                    //set the title for alert dialog
                    builder.setTitle("Choose Select From MM: ");
                    //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            // setting the button text to the selected itenm from the list
                            popupetslotfromsecond.setText(items[item]);

                        }
                    });
                    //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                    builder.setCancelable(false)
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //When clicked on CANCEL button the dalog will be dismissed
                                    dialog.dismiss();
                                }
                            });
                    // Creating alert dialog
                    AlertDialog alert = builder.create();
                    //Showing alert dialog
                    alert.show();
                }
            });

            popupetslottofirst.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                    final String[] items = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
                            "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                    //set the title for alert dialog
                    builder.setTitle("Choose Select To HH: ");
                    //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            // setting the button text to the selected itenm from the list
                            popupetslottofirst.setText(items[item]);
                            if (items[item].equalsIgnoreCase("24")) {
                                popupetslottocolon.setVisibility(View.VISIBLE);
                                popupllslottosecond.setVisibility(View.GONE);
                                popupllslottothird.setVisibility(View.VISIBLE);
                            } else {
                                popupetslottocolon.setVisibility(View.VISIBLE);
                                popupllslottosecond.setVisibility(View.VISIBLE);
                                popupllslottothird.setVisibility(View.GONE);
                            }
                        }
                    });
                    //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                    builder.setCancelable(false)
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //When clicked on CANCEL button the dalog will be dismissed
                                    dialog.dismiss();
                                }
                            });
                    // Creating alert dialog
                    AlertDialog alert = builder.create();
                    //Showing alert dialog
                    alert.show();
                }
            });

            popupetslottosecond.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                    final String[] items = {"00", "15", "30", "45"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                    //set the title for alert dialog
                    builder.setTitle("Choose Select to MM: ");
                    //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            // setting the button text to the selected itenm from the list
                            popupetslottosecond.setText(items[item]);
                        }
                    });
                    //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                    builder.setCancelable(false)
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //When clicked on CANCEL button the dalog will be dismissed
                                    dialog.dismiss();
                                }
                            });
                    // Creating alert dialog
                    AlertDialog alert = builder.create();
                    //Showing alert dialog
                    alert.show();
                }
            });

			/*popuprgbiddigndate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
				public void onCheckedChanged(RadioGroup group,int checkedId) {
					popupbiddingdate = (RadioButton) layout.findViewById(checkedId);
					popupbiddingdateSend = (String) popupbiddingdate.getText();
					if (popupbiddingdateSend.equalsIgnoreCase("Future Date")) {
						popupetxt_fromdate.setText("");
						popupetxt_fromdate.setVisibility(View.VISIBLE);
					}
					if (popupbiddingdateSend.equalsIgnoreCase("Today Date")) {
						popupetxt_fromdate.setText("");
						popupetxt_fromdate.setVisibility(View.VISIBLE);
						Calendar c = Calendar.getInstance();
						System.out.println("Current time => "+ c.getTime());
						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String formattedDate = df.format(c.getTime());
						String[] parts = formattedDate.split(" ");
						String part1 = parts[0]; // 004
						popupetxt_fromdate.setText(part1);
						popupas = popupetxt_fromdate.getText().toString();
					}
				}
			});*/

			/*popupetxt_fromdate
			.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(
						View arg0) {
					DialogFragment newFragment = new SelectPopupDateFragment();
					newFragment
							.show(getFragmentManager(),
									"DatePicker");
				}
			});*/


            popupButtonSave = (Button) layout.findViewById(R.id.popupButtonSave);
            popupButtonSave.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    popupcbclienttypesendBUY = "";
                    popupclientslotfromfirst = popupetslotfromfirst.getText().toString();
                    popupclientetslotfromsecond = popupetslotfromsecond.getText().toString();
                    popupclientetslotfromthird = popupetslotfromthird.getText().toString();

                    popupclientslottofirst = popupetslottofirst.getText().toString();
                    popupclientetslottosecond = popupetslottosecond.getText().toString();
                    popupclientetslottothird = popupetslotfromthird.getText().toString();

                    popupclientprice = popupetprice.getText().toString().trim();
                    popupclientbid = popupetbid.getText().toString().trim();
                    popupclientnoofblock = popupetnoofbloack.getText().toString().trim();
					/*if (popupcheckboxtype.isChecked()) {
						popupgettypeIEX = (String) popupcheckboxtype.getText();
						//Toast.makeText(getActivity(), popupgettypeIEX, Toast.LENGTH_LONG).show();
					}
					else{
						Toast.makeText(getActivity(), "Please select the type.", Toast.LENGTH_LONG).show();
					}*/
                    //Toast.makeText(getActivity(), "popupclientetslotfromsecond"+popupclientetslotfromsecond, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(), "popupclientetslottosecond"+popupclientetslottosecond, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(), "popupclientprice"+popupclientprice, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(), "popupclientbid"+popupclientbid, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(), "popupclientnoofblock"+popupclientnoofblock, Toast.LENGTH_LONG).show();
                    int selectedpopupbiddingtypeId = popupcbclienttype.getCheckedRadioButtonId();
                    popupbiddingtype = (RadioButton) layout.findViewById(selectedpopupbiddingtypeId);
                    if (popupbiddingtype != null) {
                        if (popupbiddingtype.isChecked()) {
                            popupcbclienttypesendBUY = (String) popupbiddingtype.getText();
                            //Toast.makeText(getActivity(), "popupgetclienttype"+popupgetclienttype, Toast.LENGTH_LONG).show();
                        }
                    }

                    //String match = "^(1?[0-9]|2[0-3]):[0-5][0-9]$";


                    int price = 0;
                    int numberofblock = 0;
                    try {
                        price = NumberFormat.getInstance().parse(popupclientprice).intValue();
                        numberofblock = NumberFormat.getInstance().parse(popupclientnoofblock).intValue();
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    if (popupcbclienttypesendBUY.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please Check the type.", Toast.LENGTH_LONG).show();
                    } else if (popupclientslotfromfirst.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Slot from HH.", Toast.LENGTH_LONG).show();
                    }
					/*else if(popupclientetslotfromsecond.equalsIgnoreCase("MM")){
						Toast.makeText(getActivity(), "Please fill the Slot from MM.", Toast.LENGTH_LONG).show();
					}*/
                    else if (popupclientslottofirst.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Slot to HH.", Toast.LENGTH_LONG).show();
                    }
					/*else if(popupclientetslottosecond.equalsIgnoreCase("MM")){
						Toast.makeText(getActivity(), "Please fill the Slot to MM.", Toast.LENGTH_LONG).show();
					}*/
                    else if (popupclientprice.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Price.", Toast.LENGTH_LONG).show();
                    } else if (popupclientbid.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Bid.", Toast.LENGTH_LONG).show();
                    } else if (popupclientnoofblock.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Number of Block.", Toast.LENGTH_LONG).show();
                    } else if (price > 20000) {
                        Toast.makeText(NewBidActivity.this, "Price should be less than 20000.", Toast.LENGTH_LONG).show();
                    } else if (numberofblock > 24) {
                        Toast.makeText(NewBidActivity.this, "Number of Block should be less than 24.", Toast.LENGTH_LONG).show();
                    } else {
                        ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                        if (!cd.isConnectingToInternet()) {
                            Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                        } else {
                            new HttpAsyncTaskNewSaveClientBidSubmitData().execute(domain + "/mobile/pxs_app/service/newbid/saveclientbid.php");
                            //pwindo.dismiss();
                        }
                    }

					/*if ((popupcbclienttypesendBUY != null) && (popupclientslotfrom.length() != 0) && (popupclientslotto.length() != 0)
							&& (popupclientprice.length() != 0) && (popupclientbid.length() != 0) && (popupclientnoofblock.length() != 0) ){
						new HttpAsyncTaskNewSaveClientBidSubmitData().execute("http://www.mittalpower.com/mobile/pxs_app/service/newbid/saveclientbid.php");
						pwindo.dismiss();
					}
					else{
						Toast.makeText(getActivity(), "Please fill all the details.", Toast.LENGTH_LONG).show();
					}*/

                }

            });
            popupButtoncancel = (Button) layout
                    .findViewById(R.id.popupButtoncancel);
            popupButtoncancel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });

            tvcross.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initiatePopupWindowForTam() {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.screen_popup_tam,
                    (ViewGroup) findViewById(R.id.popup_element_tam));
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            pwindotam = new PopupWindow(layout, (width - 20), (height - 90), true);
            pwindotam.showAtLocation(layout, Gravity.BOTTOM, 0, 0);


            tampopupcbclienttype = (RadioGroup) layout.findViewById(R.id.tampopupcbclienttype);
            tampopupetslotfromfirst = (TextView) layout.findViewById(R.id.tampopupetslotfromfirst);
            tampopupetslotfromcolon = (LinearLayout) layout.findViewById(R.id.tampopupetslotfromcolon);
            tampopupllslotfromsecond = (LinearLayout) layout.findViewById(R.id.tampopupllslotfromsecond);
            tampopupetslotfromsecond = (TextView) layout.findViewById(R.id.tampopupetslotfromsecond);
            tampopupllslotfromthird = (LinearLayout) layout.findViewById(R.id.tampopupllslotfromthird);
            tampopupetslotfromthird = (TextView) layout.findViewById(R.id.tampopupetslotfromthird);

            tampopupetslottofirst = (TextView) layout.findViewById(R.id.tampopupetslottofirst);
            tampopupetslottocolon = (LinearLayout) layout.findViewById(R.id.tampopupetslottocolon);
            tampopupllslottosecond = (LinearLayout) layout.findViewById(R.id.tampopupllslottosecond);
            tampopupetslottosecond = (TextView) layout.findViewById(R.id.tampopupetslottosecond);
            tampopupllslottothird = (LinearLayout) layout.findViewById(R.id.tampopupllslottothird);
            tampopupetslottothird = (TextView) layout.findViewById(R.id.tampopupetslottothird);


            tampopupetprice = (EditText) layout.findViewById(R.id.tampopupetprice);
            tampopupetmaxprice = (EditText) layout.findViewById(R.id.tampopupetmaxprice);
            tampopupetbid = (EditText) layout.findViewById(R.id.tampopupetbid);
            tamtvcross = (LinearLayout) layout.findViewById(R.id.tamtvcross);

            tampopupetslotfromfirst.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                    final String[] items = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
                            "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                    //set the title for alert dialog
                    builder.setTitle("Choose Select From HH:");
                    //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            // setting the button text to the selected itenm from the list
                            tampopupetslotfromfirst.setText(items[item]);
                            if (items[item].equalsIgnoreCase("24")) {
                                tampopupetslotfromcolon.setVisibility(View.VISIBLE);
                                tampopupllslotfromsecond.setVisibility(View.GONE);
                                tampopupllslotfromthird.setVisibility(View.VISIBLE);
                            } else {
                                tampopupetslotfromcolon.setVisibility(View.VISIBLE);
                                tampopupllslotfromsecond.setVisibility(View.VISIBLE);
                                tampopupllslotfromthird.setVisibility(View.GONE);
                            }
                        }
                    });
                    //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                    builder.setCancelable(false)
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //When clicked on CANCEL button the dalog will be dismissed
                                    dialog.dismiss();
                                }
                            });
                    // Creating alert dialog
                    AlertDialog alert = builder.create();
                    //Showing alert dialog
                    alert.show();
                }
            });

            tampopupetslotfromsecond.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                    final String[] items = {"00", "15", "30", "45"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                    //set the title for alert dialog
                    builder.setTitle("Choose Select From MM: ");
                    //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            // setting the button text to the selected itenm from the list
                            tampopupetslotfromsecond.setText(items[item]);

                        }
                    });
                    //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                    builder.setCancelable(false)
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //When clicked on CANCEL button the dalog will be dismissed
                                    dialog.dismiss();
                                }
                            });
                    // Creating alert dialog
                    AlertDialog alert = builder.create();
                    //Showing alert dialog
                    alert.show();
                }
            });

            tampopupetslottofirst.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                    final String[] items = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
                            "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                    //set the title for alert dialog
                    builder.setTitle("Choose Select To HH: ");
                    //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            // setting the button text to the selected itenm from the list
                            tampopupetslottofirst.setText(items[item]);
                            if (items[item].equalsIgnoreCase("24")) {
                                tampopupetslottocolon.setVisibility(View.VISIBLE);
                                tampopupllslottosecond.setVisibility(View.GONE);
                                tampopupllslottothird.setVisibility(View.VISIBLE);
                            } else {
                                tampopupetslottocolon.setVisibility(View.VISIBLE);
                                tampopupllslottosecond.setVisibility(View.VISIBLE);
                                tampopupllslottothird.setVisibility(View.GONE);
                            }
                        }
                    });
                    //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                    builder.setCancelable(false)
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //When clicked on CANCEL button the dalog will be dismissed
                                    dialog.dismiss();
                                }
                            });
                    // Creating alert dialog
                    AlertDialog alert = builder.create();
                    //Showing alert dialog
                    alert.show();
                }
            });

            tampopupetslottosecond.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                    final String[] items = {"00", "15", "30", "45"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                    //set the title for alert dialog
                    builder.setTitle("Choose Select to MM: ");
                    //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            // setting the button text to the selected itenm from the list
                            tampopupetslottosecond.setText(items[item]);
                        }
                    });
                    //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                    builder.setCancelable(false)
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    //When clicked on CANCEL button the dalog will be dismissed
                                    dialog.dismiss();
                                }
                            });
                    // Creating alert dialog
                    AlertDialog alert = builder.create();
                    //Showing alert dialog
                    alert.show();
                }
            });

            Button tampopupButtonSave = (Button) layout.findViewById(R.id.tampopupButtonSave);
            tampopupButtonSave.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    tampopupcbclienttypesendBUY = "";
                    tampopupclientslotfromfirst = tampopupetslotfromfirst.getText().toString();
                    tampopupclientetslotfromsecond = tampopupetslotfromsecond.getText().toString();
                    tampopupclientetslotfromthird = tampopupetslotfromthird.getText().toString();

                    tampopupclientslottofirst = tampopupetslottofirst.getText().toString();
                    tampopupclientetslottosecond = tampopupetslottosecond.getText().toString();
                    tampopupclientetslottothird = tampopupetslotfromthird.getText().toString();

                    tampopupclientprice = tampopupetprice.getText().toString().trim();
                    tampopupclientmaxprice = tampopupetmaxprice.getText().toString().trim();
                    tampopupclientbid = tampopupetbid.getText().toString().trim();
					/*if (popupcheckboxtype.isChecked()) {
						popupgettypeIEX = (String) popupcheckboxtype.getText();
						//Toast.makeText(getActivity(), popupgettypeIEX, Toast.LENGTH_LONG).show();
					}
					else{
						Toast.makeText(getActivity(), "Please select the type.", Toast.LENGTH_LONG).show();
					}*/
                    //Toast.makeText(getActivity(), "popupclientetslotfromsecond"+popupclientetslotfromsecond, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(), "popupclientetslottosecond"+popupclientetslottosecond, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(), "popupclientprice"+popupclientprice, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(), "popupclientbid"+popupclientbid, Toast.LENGTH_LONG).show();
                    //Toast.makeText(getActivity(), "popupclientnoofblock"+popupclientnoofblock, Toast.LENGTH_LONG).show();
                    int selectedpopupbiddingtypeId = tampopupcbclienttype.getCheckedRadioButtonId();
                    tampopupbiddingtype = (RadioButton) layout.findViewById(selectedpopupbiddingtypeId);
                    if (tampopupbiddingtype != null) {
                        if (tampopupbiddingtype.isChecked()) {
                            tampopupcbclienttypesendBUY = (String) tampopupbiddingtype.getText();
                            //Toast.makeText(getActivity(), "popupgetclienttype"+popupgetclienttype, Toast.LENGTH_LONG).show();
                        }
                    }

                    //String match = "^(1?[0-9]|2[0-3]):[0-5][0-9]$";


                    int price = 0;
                    try {
                        price = NumberFormat.getInstance().parse(tampopupclientprice).intValue();
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    int maxprice = 0;
                    try {
                        maxprice = NumberFormat.getInstance().parse(tampopupclientmaxprice).intValue();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (tampopupcbclienttypesendBUY.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please Check the type.", Toast.LENGTH_LONG).show();
                    } else if (tampopupclientslotfromfirst.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Slot from HH.", Toast.LENGTH_LONG).show();
                    }
					/*else if(popupclientetslotfromsecond.equalsIgnoreCase("MM")){
						Toast.makeText(getActivity(), "Please fill the Slot from MM.", Toast.LENGTH_LONG).show();
					}*/
                    else if (tampopupclientslottofirst.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Slot to HH.", Toast.LENGTH_LONG).show();
                    }
					/*else if(popupclientetslottosecond.equalsIgnoreCase("MM")){
						Toast.makeText(getActivity(), "Please fill the Slot to MM.", Toast.LENGTH_LONG).show();
					}*/
                    else if (tampopupclientprice.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Min Price.", Toast.LENGTH_LONG).show();
                    } else if (tampopupclientmaxprice.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Max Price.", Toast.LENGTH_LONG).show();
                    } else if (tampopupclientbid.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Bid.", Toast.LENGTH_LONG).show();
                    } else if (price > 20000) {
                        Toast.makeText(NewBidActivity.this, "Price should be less than 20000.", Toast.LENGTH_LONG).show();
                    } else if (maxprice > 40000) {
                        Toast.makeText(NewBidActivity.this, "Price should be less than 20000.", Toast.LENGTH_LONG).show();
                    } else {
                        ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                        if (!cd.isConnectingToInternet()) {
                            Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                        } else {
                            new HttpAsyncTasksavetambid().execute(domain + Constant.SAVE_TAM_BID);
                            //pwindo.dismiss();
                        }
                    }
                }
            });
            Button tampopupButtoncancel = (Button) layout
                    .findViewById(R.id.tampopupButtoncancel);
            tampopupButtoncancel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindotam.dismiss();
                }
            });

            tamtvcross.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindotam.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initiatePopupWindowREC() {

        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.screen_popuprec,
                    (ViewGroup) findViewById(R.id.popup_element_rec));
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            pwindorec = new PopupWindow(layout, (width - 20), (height - 90), true);
            pwindorec.showAtLocation(layout, Gravity.BOTTOM, 0, 0);

            rgrectypesolor = (RadioGroup) layout.findViewById(R.id.rgrectypesolor);
            solarrb1 = (RadioButton) layout.findViewById(R.id.solarrb1);
            nonsolarrb2 = (RadioButton) layout.findViewById(R.id.nonsolarrb2);
            popupetpricerec = (EditText) layout.findViewById(R.id.popupetpricerec);
            tvpopupmin = (TextView) layout.findViewById(R.id.tvpopupmin);
            tvpopupmax = (TextView) layout.findViewById(R.id.tvpopupmax);

            popupetnoofbloackrec = (EditText) layout.findViewById(R.id.popupetnoofbloackrec);
            tvcrossrec = (LinearLayout) layout.findViewById(R.id.tvcrossrec);

            rgrectypesolor.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // checkedId is the RadioButton selected
                    View radioButton = rgrectypesolor.findViewById(checkedId);
                    int index = rgrectypesolor.indexOfChild(radioButton);

                    // Add logic here

                    switch (index) {
                        case 1: // first button
                            tvpopupmin.setVisibility(View.VISIBLE);
                            tvpopupmax.setVisibility(View.VISIBLE);
                            tvpopupmin.setText("Min: " + solormin);
                            tvpopupmax.setText("Max: " + solormax);
                            //Toast.makeText(getApplicationContext(), "Selected button number " + index, 500).show();
                            break;
                        case 2: // secondbutton
                            tvpopupmin.setVisibility(View.VISIBLE);
                            tvpopupmax.setVisibility(View.VISIBLE);
                            tvpopupmin.setText("Min: " + nonsolormin);
                            tvpopupmax.setText("Max: " + nonsolormax);
                            //Toast.makeText(getApplicationContext(), "Selected button number " + index, 500).show();
                            break;
                    }
                }
            });


            popupButtonSaverec = (Button) layout.findViewById(R.id.popupButtonSaverec);
            popupButtonSaverec.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    int selectedrgrectypesolorId = rgrectypesolor.getCheckedRadioButtonId();
                    rgrectypesolorbutton = (RadioButton) layout.findViewById(selectedrgrectypesolorId);
                    if (rgrectypesolorbutton != null) {
                        rgrectypesolorbuttonSend = (String) rgrectypesolorbutton.getText();

                    }
                    popupclientpricerec = popupetpricerec.getText().toString().trim();
                    popupclientnoofblockrec = popupetnoofbloackrec.getText().toString().trim();

                    int price = 0;
                    int numberofblock = 0;
                    try {
                        price = NumberFormat.getInstance().parse(popupclientpricerec).intValue();
                        numberofblock = NumberFormat.getInstance().parse(popupclientnoofblockrec).intValue();
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    if (rgrectypesolorbuttonSend.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill REC Type.", Toast.LENGTH_LONG).show();
                    } else if (popupclientpricerec.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Price.", Toast.LENGTH_LONG).show();
                    } else if (popupclientnoofblockrec.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Number of Rec.", Toast.LENGTH_LONG).show();
                    }


					/*else if(rgrectypesolorbuttonSend.equalsIgnoreCase("SOLAR")){
						if(price > 5800 || price < 3500){
						Toast.makeText(NewBidActivity.this, "Price should be less than 3500 and greater than 5800.", Toast.LENGTH_LONG).show();
						}else{

						}
					}

					else if(rgrectypesolorbuttonSend.equalsIgnoreCase("NONSOLAR")){
						if(price > 3300 || price < 1500){
						Toast.makeText(NewBidActivity.this, "Price should be less than 1500 and greater than 3300.", Toast.LENGTH_LONG).show();
						}
						else{

						}
					}*/

                    else if (numberofblock > 1000000 || numberofblock < 0) {
                        Toast.makeText(NewBidActivity.this, "Number of Rec should be greater than 0 and less than 1000000.", Toast.LENGTH_LONG).show();
                    } else {
                        ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                        if (!cd.isConnectingToInternet()) {
                            Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                        } else {
                            //new HttpAsyncTaskNewSaveClientBidSubmitData().execute("https://www.mittalpower.com/mobile/pxs_app/service/newbid/saveclientbid.php");
                            //pwindo.dismiss();
                            if (rgrectypesolorbuttonSend.equalsIgnoreCase("SOLAR")) {
                                if (price > Integer.parseInt(solormax) || price < Integer.parseInt(solormin)) {
                                    Toast.makeText(NewBidActivity.this, "Price should be greater than or equal to" + solormin + "and less than or equal to" + solormax, Toast.LENGTH_LONG).show();
                                } else {
                                    new HttpAsyncTaskNewBidRecSave().execute(domain + "/mobile/pxs_app/service/rec/newbid/savebid.php");
                                }
                            } else if (rgrectypesolorbuttonSend.equalsIgnoreCase("NONSOLAR")) {
                                if (price > Integer.parseInt(nonsolormax) || price < Integer.parseInt(nonsolormin)) {
                                    Toast.makeText(NewBidActivity.this, "Price should be greater than or equal to" + nonsolormin + " and less than or equal to" + nonsolormax, Toast.LENGTH_LONG).show();
                                } else {
                                    new HttpAsyncTaskNewBidRecSave().execute(domain + "/mobile/pxs_app/service/rec/newbid/savebid.php");
                                }
                            } else {
                                //new HttpAsyncTaskNewBidRecSave().execute("http://www.mittalpower.com/mobile/pxs_app/service/rec/newbid/savebid.php");
                            }
                        }
                    }
                }

            });
            popupButtoncancelrec = (Button) layout
                    .findViewById(R.id.popupButtoncancelrec);
            popupButtoncancelrec.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindorec.dismiss();
                }
            });

            tvcrossrec.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindorec.dismiss();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void initiatePopupWindowESCERTS() {
        try {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.screen_popupescerts,
                    (ViewGroup) findViewById(R.id.popup_element_escerts));
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;
            pwindoescerts = new PopupWindow(layout, (width - 20), (height - 90), true);
            pwindoescerts.showAtLocation(layout, Gravity.BOTTOM, 0, 0);

            popupetpriceescerts = (EditText) layout.findViewById(R.id.popupetpriceescerts);
            popupetnoofcertificateescerts = (EditText) layout.findViewById(R.id.popupetnoofcertificateescerts);
            tvcrossescerts = (LinearLayout) layout.findViewById(R.id.tvcrossescerts);

            popupButtonSaveescerts = (Button) layout.findViewById(R.id.popupButtonSaveescerts);
            popupButtonSaveescerts.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    popupclientpriceescerts = popupetpriceescerts.getText().toString().trim();
                    popupclientnoofblockescerts = popupetnoofcertificateescerts.getText().toString().trim();

                    int escertsprice = 0;
                    int escertsnumberofcertificate = 0;
                    try {
                        escertsprice = NumberFormat.getInstance().parse(popupclientpriceescerts).intValue();
                        escertsnumberofcertificate = NumberFormat.getInstance().parse(popupclientnoofblockescerts).intValue();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (popupetpriceescerts.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Price.", Toast.LENGTH_LONG).show();
                    } else if (popupetnoofcertificateescerts.length() == 0) {
                        Toast.makeText(NewBidActivity.this, "Please fill the Number of Certificate.", Toast.LENGTH_LONG).show();
                    } else if (escertsprice > 10000 || escertsprice < 0) {
                        Toast.makeText(NewBidActivity.this, "Price should be greater than 0 and less than 10000.", Toast.LENGTH_LONG).show();
                    } else if (escertsnumberofcertificate > 1000000 || escertsnumberofcertificate < 0) {
                        Toast.makeText(NewBidActivity.this, "Number of Certificate should be greater than 0 and less than 1000000.", Toast.LENGTH_LONG).show();
                    } else {
                        ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                        if (!cd.isConnectingToInternet()) {
                            Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                        } else {
                            new HttpAsyncTaskNewBidEscertSave().execute(domain + "/mobile/pxs_app/service/escert/newbid/savebid.php");
                        }
                    }
                }

            });
            popupButtoncancelescerts = (Button) layout.findViewById(R.id.popupButtoncancelescerts);
            popupButtoncancelescerts.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindoescerts.dismiss();
                }
            });
            tvcrossescerts.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindoescerts.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinrevision = (Spinner) parent;
        Spinner spintam = (Spinner) parent;
        Spinner spinescerts = (Spinner) parent;
        if (spinrevision.getId() == R.id.recetxt_fromdate) {
            recplacedbiddatesend = recdate_array.get(position);
        }
        if (spintam.getId() == R.id.tam_sp_selectproduct) {
            producttypesend = selectproductArray.get(position);
        }
        if (spinescerts.getId() == R.id.escertsetxt_fromdate) {
            escertplacedbiddatesend = escertsdate_array.get(position);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }

    @Override
    public void onBackPressed() {
        if (comeFrom.equalsIgnoreCase("MainActivity")) {
            Intent in = new Intent(NewBidActivity.this, MainActivityAfterLogin.class);
            startActivity(in);
            overridePendingTransition(R.anim.animation, R.anim.animation2);
            finish();
        } else if (comeFrom.equalsIgnoreCase("HomeActivity")) {
            Intent in = new Intent(NewBidActivity.this, HomeActivity.class);
            in.putExtra("companyName", companyName);
            startActivity(in);
            overridePendingTransition(R.anim.animation, R.anim.animation2);
            finish();
        } else {
            Intent in = new Intent(NewBidActivity.this, MainActivityAfterLogin.class);
            startActivity(in);
            overridePendingTransition(R.anim.animation, R.anim.animation2);
            finish();
        }
        return;
    }

    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            // fromDateEtxt.setText(month + "/" + day + "/" + year);
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                etxt_fromdate.setText(dayplace + "-" + monthplace + "-" + year);
                etxt_fromdate_set.setText(year + "-" + monthplace + "-" + dayplace);
                as = etxt_fromdate_set.getText().toString();
                // Toast.makeText(getActivity(), "DownloadResult" +
                // as,Toast.LENGTH_LONG).show();
            } else if (month <= 9) {
                String monthplace = "0" + month;
                etxt_fromdate.setText(day + "-" + monthplace + "-" + year);
                etxt_fromdate_set.setText(year + "-" + monthplace + "-" + day);
                as = etxt_fromdate_set.getText().toString();
            } else if (day < 10) {
                String dayplace = "0" + day;
                etxt_fromdate.setText(dayplace + "-" + month + "-" + year);
                etxt_fromdate_set.setText(year + "-" + month + "-" + dayplace);
                as = etxt_fromdate_set.getText().toString();
            } else {
                etxt_fromdate.setText(day + "-" + month + "-" + year);
                etxt_fromdate_set.setText(year + "-" + month + "-" + day);
                as = etxt_fromdate_set.getText().toString();
            }
        }
    }

    public static class SelectUpdateDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            // fromDateEtxt.setText(month + "/" + day + "/" + year);
            if (month % 10 == 0) {
                updateetxt_fromdate.setText(year + "-" + month + "-" + day);
                updateas = updateetxt_fromdate.getText().toString();
                // Toast.makeText(getActivity(), "DownloadResult" +
                // as,Toast.LENGTH_LONG).show();
            } else {
                place = "0" + month;
                updateetxt_fromdate.setText(year + "-" + place + "-" + day);
                updateas = updateetxt_fromdate.getText().toString();
            }
        }
    }

    public static class SelectPopupDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            // fromDateEtxt.setText(month + "/" + day + "/" + year);
            if (month % 10 == 0) {
                popupetxt_fromdate.setText(year + "-" + month + "-" + day);
                popupas = popupetxt_fromdate.getText().toString();
            } else {
                popupplace = "0" + month;
                popupetxt_fromdate.setText(year + "-" + popupplace + "-" + day);
                popupas = popupetxt_fromdate.getText().toString();

            }
        }
    }

    public static class TamSelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            // fromDateEtxt.setText(month + "/" + day + "/" + year);
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                tam_tv_delivery.setText(dayplace + "-" + monthplace + "-" + year);
                tam_date_Send = year + "-" + monthplace + "-" + dayplace;
            } else if (month <= 9) {
                String monthplace = "0" + month;
                tam_tv_delivery.setText(day + "-" + monthplace + "-" + year);
                tam_date_Send = year + "-" + monthplace + "-" + day;
            } else if (day < 10) {
                String dayplace = "0" + day;
                tam_tv_delivery.setText(dayplace + "-" + month + "-" + year);
                tam_date_Send = year + "-" + month + "-" + dayplace;
            } else {
                tam_tv_delivery.setText(day + "-" + month + "-" + year);
                tam_date_Send = year + "-" + month + "-" + day;

            }
        }
    }

    private class HttpAsyncTaskNewBidGetBidDetail extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            if (biddingtypeSend.equalsIgnoreCase("Block Bid") || biddingtypeSend.equalsIgnoreCase("BLOCK")) {
                biddingtypeSend = "BLOCK";
            } else if (biddingtypeSend.equalsIgnoreCase("Single Bid") || biddingtypeSend.equalsIgnoreCase("SINGLE")) {
                biddingtypeSend = "SINGLE";
            } else {

            }
            AddBidSubmit addBidSubmit = new AddBidSubmit();
            addBidSubmit.setBidtype(biddingtypeSend);
            addBidSubmit.setType(gettype);
            addBidSubmit.setBiddate(as);
            addBidSubmit.setAddbidsaveaccesskey(dbacess);
            return AddBidSubmitAPIResponse.POST(urls[0], addBidSubmit);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            // Toast.makeText(getActivity(), "AddBidSave" +
            // result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {

                    JSONArray jsonarray = new JSONArray(result);
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject json_data = jsonarray.getJSONObject(i);
                        String clientid = json_data.getString("clientid");
                        submitstatus = json_data.getString("submitstatus");
                        biddetail = json_data.getJSONArray("biddetail");
                        if (biddetail.isNull(0)) {
                            lladdbidcancel.setVisibility(View.GONE); // GONE
                            lladdbidsubmit.setVisibility(View.GONE); // GONE
                            llnewbidsecondheading.setVisibility(View.GONE); // Visible
                            lladdbidsave.setVisibility(View.GONE);
                            lllistheading.setVisibility(View.GONE);
                            addbidlist.setVisibility(View.GONE); // Visible
                            //Toast.makeText(NewBidActivity.this, "Data is empty.",Toast.LENGTH_LONG).show();

                        } else {
                            for (j = 0; j < biddetail.length(); j++) {
                                JSONObject obj = biddetail.getJSONObject(j);
                                String id = obj.getString("id");
                                String date = obj.getString("date");
                                String price = obj.getString("price");
                                String bid = obj.getString("bid");
                                String blockfrom = obj.getString("blockfrom");
                                String blockto = obj.getString("blockto");
                                status = obj.getString("status");
                                String k = String.valueOf(j + 1);
                                mypojo = new AddBidGetAllDetails(k, id, date, price, bid, blockfrom, blockto, status);
                                myHashmap.put(j, mypojo);
                                serialnumber = new ArrayList<String>();
                                addbidid = new ArrayList<String>();
                                addbiddate = new ArrayList<String>();
                                addbidprice = new ArrayList<String>();
                                addbidbid = new ArrayList<String>();
                                addbidblockfrom = new ArrayList<String>();
                                addbidblockto = new ArrayList<String>();
                                addbidstatus = new ArrayList<String>();
                                Collection<AddBidGetAllDetails> c = myHashmap
                                        .values();
                                Iterator<AddBidGetAllDetails> itr = c.iterator();
                                while (itr.hasNext()) {
                                    AddBidGetAllDetails value = (AddBidGetAllDetails) itr.next();
                                    serialnumber.add(value.getK());
                                    addbidid.add(value.getId());
                                    addbiddate.add(value.getDate());
                                    addbidprice.add(value.getPrice());
                                    addbidbid.add(value.getBid());
                                    addbidblockfrom.add(value.getBlockfrom());
                                    addbidblockto.add(value.getBlockto());
                                    addbidstatus.add(value.getStatus());
                                }
                            }
                            serialnumberadapter = serialnumber.toArray(new String[serialnumber.size()]);
                            Integer[] intarray = new Integer[serialnumberadapter.length];
                            int j = 0;
                            for (String str : serialnumberadapter) {
                                intarray[j] = Integer.parseInt(str.trim());//Exception in this line
                                j++;
                            }
                            Arrays.sort(intarray);
                            String[] a = Arrays.toString(intarray).split("[\\[\\]]")[1].split(", ");
                            System.out.println(Arrays.toString(a));
                            idadapter = addbidid.toArray(new String[addbidid.size()]);
                            dateadapter = addbiddate.toArray(new String[addbiddate.size()]);
                            priceadapter = addbidprice.toArray(new String[addbidprice.size()]);
                            bidadapter = addbidbid.toArray(new String[addbidbid.size()]);
                            blockfromadapter = addbidblockfrom.toArray(new String[addbidblockfrom.size()]);
                            blocktoadapter = addbidblockto.toArray(new String[addbidblockto.size()]);
                            statusadapter = addbidstatus.toArray(new String[addbidstatus.size()]);

                            AddBidCustomList adapter = new AddBidCustomList(
                                    NewBidActivity.this, a, blockfromadapter,
                                    blocktoadapter, priceadapter, dateadapter,
                                    bidadapter);
                            addbidlist.setAdapter(adapter);
                            myHashmap.clear();
//                        Toast.makeText(getActivity(), statusadapter.length, Toast.LENGTH_LONG).show();
                            for (int k = 0; k < statusadapter.length; k++) {
                                System.out.println(statusadapter[k]);
                                if (statusadapter[k].equalsIgnoreCase("FALSE")) {
                                    lladdbidcancel.setVisibility(View.GONE);
                                    lladdbidsubmit.setVisibility(View.VISIBLE);
                                    lllistheading.setVisibility(View.VISIBLE); // Visible
                                    addbidlist.setVisibility(View.VISIBLE); // Visible
                                    llnewbidsecondheading.setVisibility(View.VISIBLE); // Visible
                                    lladdbidsave.setVisibility(View.VISIBLE); // Visible
                                    break;
                                } else {
                                    lladdbidcancel.setVisibility(View.VISIBLE);
                                    lladdbidsubmit.setVisibility(View.GONE);
                                    lllistheading.setVisibility(View.VISIBLE); // Visible
                                    addbidlist.setVisibility(View.VISIBLE); // Visible
                                    llnewbidsecondheading.setVisibility(View.VISIBLE); // Visible
                                    lladdbidsave.setVisibility(View.VISIBLE); // Visible
                                }
                            }


                            addbidlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    // Toast.makeText(getActivity(),"You Clicked at " +idadapter[+position],Toast.LENGTH_SHORT).show();

                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewBidActivity.this);
                                    alertDialogBuilder.setMessage("Are you sure you want to Delete or Edit? Please Select one.");
                                    alertDialogBuilder.setPositiveButton("Edit",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface arg0, int arg1) {
                                                    // Toast.makeText(getActivity(),"You Clicked at " + text1[+position],Toast.LENGTH_SHORT).show();
                                                    idupdate = idadapter[+position];
                                                    priceupdate = priceadapter[+position];
                                                    bidupadte = bidadapter[+position];
                                                    blockfromupadte = blockfromadapter[+position];
                                                    blocktoupadte = blocktoadapter[+position];
                                                    //Toast.makeText(getActivity(),"You Clicked at "+ idupdate,Toast.LENGTH_SHORT).show();
                                                    final Dialog dialog = new Dialog(NewBidActivity.this);
                                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                                                    dialog.setContentView(R.layout.newbid_custom_editpopup);
                                                    //dialog.setTitle("Edit Your bid...");
                                                    // set the custom dialog  components - text, image and button
                                                    /*
                                                     * TextView text = (TextView) dialog.findViewById(R.id.text);
                                                     * text.setText("Android custom dialog example!");
                                                     *  ImageView image =  (ImageView) dialog.findViewById(R.id.image);
                                                     * image.setImageResource(R.drawable.ic_launcher);
                                                     */
                                                    //updatecheckboxtype = (CheckBox) dialog.findViewById(R.id.updatecheckboxtype);
                                                    //updatergbiddigntype = (RadioGroup) dialog.findViewById(R.id.updatergbiddigntype);
                                                    //updatergbiddigndate = (RadioGroup) dialog.findViewById(R.id.updatergbiddigndate);
                                                    //updateetxt_fromdate = (TextView) dialog.findViewById(R.id.updateetxt_fromdate); // invisible
                                                    updatecbclienttype = (RadioGroup) dialog.findViewById(R.id.updatecbclienttype);
                                                    updateRBclienttypeBUY = (RadioButton) dialog.findViewById(R.id.updateRBclienttypeBUY);
                                                    updateRBclienttypeSELL = (RadioButton) dialog.findViewById(R.id.updateRBclienttypeSELL);
                                                    updateetslotfromfirst = (TextView) dialog.findViewById(R.id.updateetslotfromfirst);
                                                    updateetslotfromsecond = (TextView) dialog.findViewById(R.id.updateetslotfromsecond);
                                                    updateetslotfromthird = (TextView) dialog.findViewById(R.id.updateetslotfromthird);

                                                    updateetslottofirst = (TextView) dialog.findViewById(R.id.updateetslottofirst);
                                                    updateetslottosecond = (TextView) dialog.findViewById(R.id.updateetslottosecond);
                                                    updateetslottothird = (TextView) dialog.findViewById(R.id.updateetslottothird);

                                                    updateetprice = (EditText) dialog.findViewById(R.id.updateetprice);
                                                    updateetbid = (EditText) dialog.findViewById(R.id.updateetbid);
                                                    updateetnoofbloack = (EditText) dialog.findViewById(R.id.updateetnoofbloack);

                                                    Double abc = Double.parseDouble(bidupadte);
                                                    if (abc > 0) {
                                                        updateRBclienttypeBUY.setChecked(true);
                                                        updateetbid.setText(bidupadte);
                                                    } else {
                                                        updateRBclienttypeSELL.setChecked(true);
                                                        bidupadte = bidupadte.replace("-", "");
                                                        updateetbid.setText(bidupadte);
                                                    }


                                                    String[] parts = blockfromupadte.split(":");
                                                    part1 = parts[0]; // 004
                                                    part2 = parts[1]; // 004
                                                    updateetslotfromfirst.setText(part1);
                                                    updateetslotfromsecond.setText(part2);
                                                    String[] partsto = blocktoupadte.split(":");
                                                    partone = partsto[0]; // 004
                                                    parttwo = partsto[1]; // 004
                                                    updateetslottofirst.setText(partone);
                                                    updateetslottosecond.setText(parttwo);
                                                    updateetprice.setText(priceupdate);
                                                    updateetnoofbloack.setText("1");


                                                    updateetslotfromfirst.setOnClickListener(new OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                                                            final String[] items = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
                                                                    "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"};
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                                                            //set the title for alert dialog
                                                            builder.setTitle("Choose Select From HH:");
                                                            //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                                                            builder.setItems(items, new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int item) {
                                                                    // setting the button text to the selected itenm from the list
                                                                    updateetslotfromfirst.setText(items[item]);
									                            	/*if(items[item].equalsIgnoreCase("24")){
									                            		popupetslotfromcolon.setVisibility(View.VISIBLE);
									                            		popupllslotfromsecond.setVisibility(View.GONE);
									                            		popupllslotfromthird.setVisibility(View.VISIBLE);
									                            	}
									                            	else{
									                            		popupetslotfromcolon.setVisibility(View.VISIBLE);
									                            		popupllslotfromsecond.setVisibility(View.VISIBLE);
									                            		popupllslotfromthird.setVisibility(View.GONE);
									                            	}*/
                                                                }
                                                            });
                                                            //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                                                            builder.setCancelable(false)
                                                                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(DialogInterface dialog, int id) {
                                                                            //When clicked on CANCEL button the dalog will be dismissed
                                                                            dialog.dismiss();
                                                                        }
                                                                    });
                                                            // Creating alert dialog
                                                            AlertDialog alert = builder.create();
                                                            //Showing alert dialog
                                                            alert.show();
                                                        }
                                                    });


                                                    updateetslotfromsecond.setOnClickListener(new OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                                                            final String[] items = {"00", "15", "30", "45"};
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                                                            //set the title for alert dialog
                                                            builder.setTitle("Choose Select From MM: ");
                                                            //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                                                            builder.setItems(items, new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int item) {
                                                                    // setting the button text to the selected itenm from the list
                                                                    updateetslotfromsecond.setText(items[item]);

                                                                }
                                                            });
                                                            //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                                                            builder.setCancelable(false)
                                                                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(DialogInterface dialog, int id) {
                                                                            //When clicked on CANCEL button the dalog will be dismissed
                                                                            dialog.dismiss();
                                                                        }
                                                                    });
                                                            // Creating alert dialog
                                                            AlertDialog alert = builder.create();
                                                            //Showing alert dialog
                                                            alert.show();
                                                        }
                                                    });


                                                    updateetslottofirst.setOnClickListener(new OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                                                            final String[] items = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
                                                                    "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"};
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                                                            //set the title for alert dialog
                                                            builder.setTitle("Choose Select From HH:");
                                                            //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                                                            builder.setItems(items, new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int item) {
                                                                    // setting the button text to the selected itenm from the list
                                                                    updateetslottofirst.setText(items[item]);
									                            	/*if(items[item].equalsIgnoreCase("24")){
									                            		popupetslotfromcolon.setVisibility(View.VISIBLE);
									                            		popupllslotfromsecond.setVisibility(View.GONE);
									                            		popupllslotfromthird.setVisibility(View.VISIBLE);
									                            	}
									                            	else{
									                            		popupetslotfromcolon.setVisibility(View.VISIBLE);
									                            		popupllslotfromsecond.setVisibility(View.VISIBLE);
									                            		popupllslotfromthird.setVisibility(View.GONE);
									                            	}*/
                                                                }
                                                            });
                                                            //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                                                            builder.setCancelable(false)
                                                                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(DialogInterface dialog, int id) {
                                                                            //When clicked on CANCEL button the dalog will be dismissed
                                                                            dialog.dismiss();
                                                                        }
                                                                    });
                                                            // Creating alert dialog
                                                            AlertDialog alert = builder.create();
                                                            //Showing alert dialog
                                                            alert.show();
                                                        }
                                                    });


                                                    updateetslottosecond.setOnClickListener(new OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                                                            final String[] items = {"00", "15", "30", "45"};
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                                                            //set the title for alert dialog
                                                            builder.setTitle("Choose Select From MM: ");
                                                            //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                                                            builder.setItems(items, new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int item) {
                                                                    // setting the button text to the selected itenm from the list
                                                                    updateetslottosecond.setText(items[item]);

                                                                }
                                                            });
                                                            //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                                                            builder.setCancelable(false)
                                                                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(DialogInterface dialog, int id) {
                                                                            //When clicked on CANCEL button the dalog will be dismissed
                                                                            dialog.dismiss();
                                                                        }
                                                                    });
                                                            // Creating alert dialog
                                                            AlertDialog alert = builder.create();
                                                            //Showing alert dialog
                                                            alert.show();
                                                        }
                                                    });


                                                    Button dialogButtonOK = (Button) dialog.findViewById(R.id.dialogButtonOK);
                                                    Button dialogButtoncancel = (Button) dialog.findViewById(R.id.dialogButtoncancel);


												/*updatergbiddigndate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
															public void onCheckedChanged(RadioGroup group,int checkedId) {
																// checkedId is
																// the
																// RadioButton
																// selected
																updatebiddingdate = (RadioButton) dialog.findViewById(checkedId);
																updatebiddingdateSend = (String) updatebiddingdate.getText();
																if (updatebiddingdateSend.equalsIgnoreCase("Future Date")) {
																	updateetxt_fromdate.setText("");
																	updateetxt_fromdate.setVisibility(View.VISIBLE);
																}
																if (updatebiddingdateSend.equalsIgnoreCase("Today Date")) {
																	updateetxt_fromdate.setText("");
																	updateetxt_fromdate.setVisibility(View.VISIBLE);
																	Calendar c = Calendar.getInstance();
																	System.out.println("Current time => "+ c.getTime());
																	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
																	String formattedDate = df.format(c.getTime());
																	// formattedDate
																	// have
																	// current
																	// date/time
																	String[] parts = formattedDate.split(" ");
																	String part1 = parts[0]; // 004
																	updateetxt_fromdate.setText(part1);
																	// tvtradingdate
																	// =
																	// (TextView)
																	// rootView.findViewById(R.id.tvtradingdate);
																	updateas = updateetxt_fromdate.getText().toString();
																}
															}
														});
*/
												/*updateetxt_fromdate.setOnClickListener(new View.OnClickListener() {
															@Override
															public void onClick(View arg0) {
																DialogFragment newFragment = new SelectUpdateDateFragment();
																newFragment.show(getFragmentManager(),"DatePicker");
															}
														});*/

                                                    // if button is clicked, close
                                                    // the custom dialog
                                                    dialogButtonOK.setOnClickListener(new OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            //Toast.makeText(getActivity(),"You Clicked at "+ idupdate,Toast.LENGTH_SHORT).show();
                                                            // dialog.dismiss();
																/*if (updatecheckboxtype.isChecked()) {
																	updategettype = (String) updatecheckboxtype.getText();
																}*/

                                                            int selectedupdatebiddingtypeId = updatecbclienttype.getCheckedRadioButtonId();
                                                            updatebiddingtype = (RadioButton) dialog.findViewById(selectedupdatebiddingtypeId);
                                                            if (updatebiddingtype != null) {
                                                                updategetclienttype = (String) updatebiddingtype.getText();
                                                            }


																/*if (updatecbclienttype.isChecked()) {
																	updategetclienttype = (String) updatecbclienttype.getText();
																}*/
                                                            //	updateclientslotfromfirst, updateclientslotfromsecond, updateclientslottofirst, updateclientslottosecond

                                                            updateclientslotfromfirst = updateetslotfromfirst.getText().toString().trim();
                                                            updateclientslotfromsecond = updateetslotfromsecond.getText().toString().trim();

                                                            updateclientslottofirst = updateetslottofirst.getText().toString().trim();
                                                            updateclientslottosecond = updateetslottosecond.getText().toString().trim();

                                                            updateclientprice = updateetprice.getText().toString().trim();
                                                            updateclientbid = updateetbid.getText().toString().trim();
                                                            updateclientnoofblock = updateetnoofbloack.getText().toString().trim();
                                                            int editprice = 0;
                                                            int editnumberofblock = 0;
                                                            try {
                                                                editprice = NumberFormat.getInstance().parse(updateclientprice).intValue();
                                                                editnumberofblock = NumberFormat.getInstance().parse(updateclientnoofblock).intValue();
                                                            } catch (ParseException e) {
                                                                // TODO Auto-generated catch block
                                                                e.printStackTrace();
                                                            }

																/*if(updategetclienttype == null){
																	Toast.makeText(getActivity(), "Please Check the type.", Toast.LENGTH_LONG).show();
																}*/
																/*else if(updateclientslotfrom.length() != 0){
																	Toast.makeText(getActivity(), "Please fill the Slot from.", Toast.LENGTH_LONG).show();
																}
																else if(updateclientslotto.length() != 0){
																	Toast.makeText(getActivity(), "Please fill the Slot to.", Toast.LENGTH_LONG).show();
																}*/
																/*else if(updateclientprice.length() != 0){
																	Toast.makeText(getActivity(), "Please fill the Price.", Toast.LENGTH_LONG).show();
																}
																else if(updateclientbid.length() != 0){
																	Toast.makeText(getActivity(), "Please fill the Bid.", Toast.LENGTH_LONG).show();
																}
																else if(updateclientnoofblock.length() != 0){
																	Toast.makeText(getActivity(), "Please fill the Number of Block.", Toast.LENGTH_LONG).show();
																}*/
																/*else if(!(updateclientslotfrom.matches("^[0-2][0-3]:[0-5][0-9]$"))){
																	Toast.makeText(getActivity(), "Please select the slot from in HH:MM formate.", Toast.LENGTH_LONG).show();
																}
																else if(!(updateclientslotto.matches("^[0-2][0-3]:[0-5][0-9]$"))){
																	Toast.makeText(getActivity(), "Please select the slot from in HH:MM formate.", Toast.LENGTH_LONG).show();
																}*/
                                                            if (updateclientprice.length() == 0) {
                                                                Toast.makeText(NewBidActivity.this, "Please fill the Price.", Toast.LENGTH_LONG).show();
                                                            } else if (updateclientbid.length() == 0) {
                                                                Toast.makeText(NewBidActivity.this, "Please fill the Bid.", Toast.LENGTH_LONG).show();
                                                            } else if (updateclientnoofblock.length() == 0) {
                                                                Toast.makeText(NewBidActivity.this, "Please fill the Number of Block.", Toast.LENGTH_LONG).show();
                                                            } else if (editprice > 20000) {
                                                                Toast.makeText(NewBidActivity.this, "Price should be less than 20000.", Toast.LENGTH_LONG).show();
                                                            } else if (editnumberofblock > 24) {
                                                                Toast.makeText(NewBidActivity.this, "Number of Block should be less than 24.", Toast.LENGTH_LONG).show();
                                                            } else {
                                                                ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                                                if (!cd.isConnectingToInternet()) {
                                                                    Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                                                } else {
                                                                    new HttpAsyncTaskNewBidUpdateClientBid().execute(domain + "/mobile/pxs_app/service/newbid/updateclientbid.php");
                                                                    dialog.dismiss();
                                                                }
                                                            }
																/*if ((updategetclienttype != null) && (updateclientslotfrom.length() != 0) && (updateclientslotto.length() != 0) && (updateclientprice.length() != 0) &&
																		(updateclientbid.length() != 0) && (updateclientnoofblock.length() != 0)) {
																	new HttpAsyncTaskNewBidUpdateClientBid().execute("http://www.mittalpower.com/mobile/pxs_app/service/newbid/updateclientbid.php");
																	dialog.dismiss();
																}
																else {
																	Toast.makeText(getActivity(),"Plaese fill the all details.", Toast.LENGTH_LONG).show();
																}*/


                                                        }
                                                    });

                                                    dialogButtoncancel.setOnClickListener(new OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            dialog.dismiss();
                                                        }
                                                    });
                                                    dialog.show();
                                                }
                                            });


                                    alertDialogBuilder.setNeutralButton("Delete",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface arg0,
                                                        int arg1) {
                                                    // Toast.makeText(getActivity(),"You Clicked at "
                                                    // + text1[+position],
                                                    // Toast.LENGTH_SHORT).show();
                                                    iddelete = idadapter[+position];
                                                    datedelete = dateadapter[+position];
                                                    ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                                    if (!cd.isConnectingToInternet()) {
                                                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                                    } else {
                                                        new HttpAsyncTaskNewSaveClientBidDeleteSubmitData()
                                                                .execute(domain + "/mobile/pxs_app/service/newbid/deletebid.php");
                                                    }
                                                }
                                            });

                                    alertDialogBuilder.setNegativeButton("Cancel",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    // finish();
                                                }
                                            });
                                    AlertDialog alertDialog = alertDialogBuilder
                                            .create();
                                    alertDialog.show();
                                }
                            });
                        }
                    }
				/*if(status.equalsIgnoreCase("FALSE")){
					lladdbidcancel.setVisibility(View.VISIBLE);
				}
				else{
					lladdbidcancel.setVisibility(View.GONE);
				}*/

                    if (submitstatus.equalsIgnoreCase("TRUE")) {
                        llnewbidsecondheading.setVisibility(View.VISIBLE); // Visible
                        lladdbidsave.setVisibility(View.VISIBLE); // Visible
                        //	lladdbidcancel.setVisibility(View.GONE); // Visible
                        //	lladdbidsubmit.setVisibility(View.VISIBLE); // GONE
                        // lladdbidform.setVisibility(View.VISIBLE); // Visible
                        //	lllistheading.setVisibility(View.VISIBLE); // Visible
                        //	addbidlist.setVisibility(View.VISIBLE); // Visible
                    } else {
                        llnewbidsecondheading.setVisibility(View.VISIBLE); // GONE
                        lladdbidsave.setVisibility(View.VISIBLE); // Visible
                        //lladdbidcancel.setVisibility(View.VISIBLE); // GONE
                        //	lladdbidsubmit.setVisibility(View.GONE); // GONE
                        lladdbidform.setVisibility(View.GONE); // GONE
                        //	lllistheading.setVisibility(View.VISIBLE); // GONE
                        //	addbidlist.setVisibility(View.VISIBLE); // GONE
                    }
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class HttpAsyncTaskNewSaveClientBidCancelALLSubmitData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            if (biddingtypeSend.equalsIgnoreCase("Block Bid") || biddingtypeSend.equalsIgnoreCase("BLOCK")) {
                biddingtypeSend = "BLOCK";
            } else if (biddingtypeSend.equalsIgnoreCase("Single Bid") || biddingtypeSend.equalsIgnoreCase("SINGLE")) {
                biddingtypeSend = "SINGLE";
            } else {

            }
            NewBidCancelAllClientBid newBidCancelAllClientBid = new NewBidCancelAllClientBid();
            newBidCancelAllClientBid.setId(cancelallid);
            newBidCancelAllClientBid.setBidtype(biddingtypeSend);
            newBidCancelAllClientBid.setType(gettype);
            newBidCancelAllClientBid.setBiddate(cancelalldateSend);
            newBidCancelAllClientBid.setAddbiddaleteaccesskey(dbacess);
            // newSaveClientBid.setAddbidclientsavesaveaccesskey("ggwxdamvghpmuzodcplnjcbebnnkbzcigzfjarfgzuttthwwwijgkllmyzwzyyifynoyftfeoyyifvecdnjozubxtxxswfxutmsygxduwcccxgebuoqtjrrcoovlutfn");
            // login.setAccess_key("tvjspkknnsishkmfskfeqgmklbsmmzmgvvykgjytbgmjryojjuozbbkmcczpcmvxiuiodgifnuoetdocxdcyenlgplwsxrpfmxupecvsxjxrmlukowjtjvzzgwrdogia");
            // login.setAccess_key(DeviceRegister.access_key);
            return AddBidCancelAllAPIResponse.POST(urls[0], newBidCancelAllClientBid);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            // Toast.makeText(getActivity(), "ClientBidDelete" +
            // result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    String value = mainObject.getString("value");
                    String message = mainObject.getString("message");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(NewBidActivity.this, message, Toast.LENGTH_LONG)
                                .show();
                    } else {
                        Toast.makeText(NewBidActivity.this, message, Toast.LENGTH_LONG)
                                .show();
                    }

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskNewBidGetBidDetail().execute(domain + "/mobile/pxs_app/service/newbid/getbiddetail.php");
        }
    }

    private class HttpAsyncTaskNewBidSubmitClientBidSubmitData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            if (biddingtypeSend.equalsIgnoreCase("Block Bid") || biddingtypeSend.equalsIgnoreCase("BLOCK")) {
                biddingtypeSend = "BLOCK";
            } else if (biddingtypeSend.equalsIgnoreCase("Single Bid") || biddingtypeSend.equalsIgnoreCase("SINGLE")) {
                biddingtypeSend = "SINGLE";
            } else {

            }
            AddBidSubmit addBidSubmit = new AddBidSubmit();
            addBidSubmit.setBidtype(biddingtypeSend);
            addBidSubmit.setType(gettype);
            addBidSubmit.setBiddate(as);
            addBidSubmit.setAddbidsaveaccesskey(dbacess);
            // newSaveClientBid.setAddbidclientsavesaveaccesskey("ggwxdamvghpmuzodcplnjcbebnnkbzcigzfjarfgzuttthwwwijgkllmyzwzyyifynoyftfeoyyifvecdnjozubxtxxswfxutmsygxduwcccxgebuoqtjrrcoovlutfn");
            // login.setAccess_key("tvjspkknnsishkmfskfeqgmklbsmmzmgvvykgjytbgmjryojjuozbbkmcczpcmvxiuiodgifnuoetdocxdcyenlgplwsxrpfmxupecvsxjxrmlukowjtjvzzgwrdogia");
            // login.setAccess_key(DeviceRegister.access_key);
            return AddBidSubmitAPIResponse.POST(urls[0], addBidSubmit);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            // Toast.makeText(getActivity(), "ClientBidDelete" +
            // result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    String value = mainObject.getString("value");
                    String message = mainObject.getString("message");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(NewBidActivity.this, message, Toast.LENGTH_LONG)
                                .show();
                    } else {
                        Toast.makeText(NewBidActivity.this, message, Toast.LENGTH_LONG)
                                .show();
                    }

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
            if (!cd.isConnectingToInternet()) {
                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
            } else {
                new HttpAsyncTaskNewBidGetBidDetail().execute(domain + "/mobile/pxs_app/service/newbid/getbiddetail.php");
            }
        }
    }

    private class HttpAsyncTaskNewSaveClientBidDeleteSubmitData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            if (biddingtypeSend.equalsIgnoreCase("Block Bid") || biddingtypeSend.equalsIgnoreCase("BLOCK")) {
                biddingtypeSend = "BLOCK";
            } else if (biddingtypeSend.equalsIgnoreCase("Single Bid") || biddingtypeSend.equalsIgnoreCase("SINGLE")) {
                biddingtypeSend = "SINGLE";
            } else {

            }
            NewBidDeleteClientBid newBidDeleteClientBid = new NewBidDeleteClientBid();
            newBidDeleteClientBid.setId(iddelete);
            newBidDeleteClientBid.setBidtype(biddingtypeSend);
            newBidDeleteClientBid.setType(gettype);
            newBidDeleteClientBid.setBiddate(datedelete);
            newBidDeleteClientBid.setAddbiddaleteaccesskey(dbacess);
            return AddBidDeleteAPIResponse.POST(urls[0], newBidDeleteClientBid);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            // Toast.makeText(getActivity(), "ClientBidDelete" +
            // result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    String value = mainObject.getString("value");
                    String message = mainObject.getString("message");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(NewBidActivity.this, message, Toast.LENGTH_LONG)
                                .show();
                    } else {
                        Toast.makeText(NewBidActivity.this, message, Toast.LENGTH_LONG)
                                .show();
                    }

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
            if (!cd.isConnectingToInternet()) {
                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
            } else {
                new HttpAsyncTaskNewBidGetBidDetail().execute(domain + "/mobile/pxs_app/service/newbid/getbiddetail.php");
            }
        }
    }

    private class HttpAsyncTaskNewBidUpdateClientBid extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            if (biddingtypeSend.equalsIgnoreCase("Block Bid") || biddingtypeSend.equalsIgnoreCase("BLOCK")) {
                biddingtypeSend = "BLOCK";
            } else if (biddingtypeSend.equalsIgnoreCase("Single Bid") || biddingtypeSend.equalsIgnoreCase("SINGLE")) {
                biddingtypeSend = "SINGLE";
            } else {

            }
            NewBidUpdateClientBid newBidUpdateClientBid = new NewBidUpdateClientBid();
            newBidUpdateClientBid.setId(idupdate);
            newBidUpdateClientBid.setBidtype(biddingtypeSend);
            newBidUpdateClientBid.setBtype(updategetclienttype);
            newBidUpdateClientBid.setBlockfrom(updateclientslotfromfirst + ":" + updateclientslotfromsecond);
            newBidUpdateClientBid.setBlockto(updateclientslottofirst + ":" + updateclientslottosecond);
            newBidUpdateClientBid.setPrice(updateclientprice);
            newBidUpdateClientBid.setBid(updateclientbid);
            newBidUpdateClientBid.setNoofblock(updateclientnoofblock);
            newBidUpdateClientBid.setType(gettype);
            newBidUpdateClientBid.setBiddate(as);
            newBidUpdateClientBid.setAddbidupdateaccesskey(dbacess);
            // newSaveClientBid.setAddbidclientsavesaveaccesskey("ggwxdamvghpmuzodcplnjcbebnnkbzcigzfjarfgzuttthwwwijgkllmyzwzyyifynoyftfeoyyifvecdnjozubxtxxswfxutmsygxduwcccxgebuoqtjrrcoovlutfn");
            // login.setAccess_key("tvjspkknnsishkmfskfeqgmklbsmmzmgvvykgjytbgmjryojjuozbbkmcczpcmvxiuiodgifnuoetdocxdcyenlgplwsxrpfmxupecvsxjxrmlukowjtjvzzgwrdogia");
            // login.setAccess_key(DeviceRegister.access_key);
            return AddBidUpdateAPIResponse.POST(urls[0], newBidUpdateClientBid);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            //Toast.makeText(getActivity(), "ClientBidUpadte" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    String value = mainObject.getString("value");
                    String message = mainObject.getString("message");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(NewBidActivity.this, message, Toast.LENGTH_LONG)
                                .show();
                    } else {
                        Toast.makeText(NewBidActivity.this, message, Toast.LENGTH_LONG)
                                .show();
                    }

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
            if (!cd.isConnectingToInternet()) {
                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
            } else {

                new HttpAsyncTaskNewBidGetBidDetail().execute(domain + "/mobile/pxs_app/service/newbid/getbiddetail.php");
            }
        }
    }

    private class HttpAsyncTaskNewSaveClientBidSubmitData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            if (biddingtypeSend.equalsIgnoreCase("Block Bid") || biddingtypeSend.equalsIgnoreCase("BLOCK")) {
                biddingtypeSend = "BLOCK";
            } else if (biddingtypeSend.equalsIgnoreCase("Single Bid") || biddingtypeSend.equalsIgnoreCase("SINGLE")) {
                biddingtypeSend = "SINGLE";
            } else {

            }
            NewSaveClientBid newSaveClientBid = new NewSaveClientBid();
            newSaveClientBid.setBidtype(biddingtypeSend);
            newSaveClientBid.setBtype(popupcbclienttypesendBUY);
            if (popupclientetslotfromthird.equalsIgnoreCase("00") && popupclientetslotfromsecond.equalsIgnoreCase("00")) {
                newSaveClientBid.setBlockfrom(popupclientslotfromfirst + ":" + popupclientetslotfromthird);
            } else {
                newSaveClientBid.setBlockfrom(popupclientslotfromfirst + ":" + popupclientetslotfromsecond);
            }
            if (popupclientetslottothird.equalsIgnoreCase("00") && popupclientetslottosecond.equalsIgnoreCase("00")) {
                newSaveClientBid.setBlockto(popupclientslottofirst + ":" + popupclientetslottothird);
            } else {
                newSaveClientBid.setBlockto(popupclientslottofirst + ":" + popupclientetslottosecond);
            }
            newSaveClientBid.setPrice(popupclientprice);
            newSaveClientBid.setBid(popupclientbid);
            newSaveClientBid.setNoofblock(popupclientnoofblock);
            newSaveClientBid.setType(gettype);
            newSaveClientBid.setDate(as);
            newSaveClientBid.setAddbidclientsavesaveaccesskey(dbacess);
            return AddBidClientSubmitAPIResponse.POST(urls[0], newSaveClientBid);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            // Toast.makeText(getActivity(), "ClientBidSave" +
            // result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    String value = mainObject.getString("value");
                    String message = mainObject.getString("message");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(NewBidActivity.this, message, Toast.LENGTH_LONG)
                                .show();
                        pwindo.dismiss();
                    } else {
                        Toast.makeText(NewBidActivity.this, message, Toast.LENGTH_LONG).show();

                    }

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
            if (!cd.isConnectingToInternet()) {
                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
            } else {
                new HttpAsyncTaskNewBidGetBidDetail().execute(domain + "/mobile/pxs_app/service/newbid/getbiddetail.php");
            }
        }
    }

    // REC NO BID API CALL
    private class HttpAsyncTaskRECgetallrectradingdate extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Login login = new Login();
            login.setAccess_key(dbacess);
            return NoBidGetAllDataAPIResponse.POST(urls[0], login);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                //Toast.makeText(getActivity(), "NoBidGetAllData" + result,Toast.LENGTH_LONG).show();
                try {
                    JSONArray jsonarray = new JSONArray(result);
                    recdate_array.add("Select Trading Date");
                    recprintdate_array.add("Select Trading Date");
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonObject = jsonarray.getJSONObject(i);
                        if (jsonObject.has("date")) {
                            String date = jsonObject.getString("date");
                            recdate_array.add(date);
                        }
                        if (jsonObject.has("printDate")) {
                            String printDate = jsonObject.getString("printDate");
                            recprintdate_array.add(printDate);
                        }
                    }

                    adapter_date = new ArrayAdapter<String>(
                            NewBidActivity.this,
                            R.layout.spinner_item,
                            recprintdate_array);
                    adapter_date
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    recetxt_fromdate.setAdapter(adapter_date);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Invalid JSON: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

        }

    }

    ///// REC PLACE BID
    private class HttpAsyncTaskNewBidgetrecbiddetail extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AddBidSubmit addBidSubmit = new AddBidSubmit();
            addBidSubmit.setType(rgrectypebuttongettypeSend);
            addBidSubmit.setBidtype(rgrecbiddigntypebuttonSend);
            addBidSubmit.setBiddate(recplacedbiddatesend);
            addBidSubmit.setAddbidsaveaccesskey(dbacess);
            return AddBidRECGetallAPIResponse.POST(urls[0], addBidSubmit);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        //llsavecancelrec
        //lladdbidsaverec
        //lladdbidcancelrec
        //lladdbidsubmitrec
        //llreclistheading
        //addbidreclist
        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            // Toast.makeText(getActivity(), "AddBidSave" +
            // result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONArray jsonarray = new JSONArray(result);
                    serialnumberplacerec.clear();
                    addbididrec.clear();
                    addbidclientidrec.clear();
                    addbiddaterec.clear();
                    addbidtyperec.clear();
                    addbidrectyperec.clear();
                    addbidpricerec.clear();
                    addbidnoofrec.clear();
                    addbidordernaturerec.clear();
                    addbidstatusrec.clear();
                    addbidtstatusrec.clear();
                    addbidextrarec.clear();
                    addbidentrybyrec.clear();
                    addbidtimestamprec.clear();
                    if (jsonarray.length() == 0) {
                        //Toast.makeText(NewBidActivity.this, "NO Data found!", Toast.LENGTH_LONG).show();
                        lladdbidsaverec.setVisibility(View.VISIBLE); // Visible
                        lladdbidcancelrec.setVisibility(View.GONE);
                        lladdbidsubmitrec.setVisibility(View.GONE);
                        llreclistheading.setVisibility(View.GONE); // Visible
                        addbidreclist.setVisibility(View.GONE); // Visible
                    } else {
                        lladdbidsaverec.setVisibility(View.VISIBLE); // Visible
                        lladdbidform.setVisibility(View.GONE); // GONE
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject obj = jsonarray.getJSONObject(i);
                            String id = obj.getString("id");
                            String clientid = obj.getString("clientid");
                            String date = obj.getString("date");
                            String type = obj.getString("type");
                            String rectype = obj.getString("rectype");
                            String price = obj.getString("price");
                            String noofrec = obj.getString("noofrec");
                            String ordernature = obj.getString("ordernature");
                            String status = obj.getString("status");
                            String tstatus = obj.getString("tstatus");
                            String extra = obj.getString("extra");
                            String entryby = obj.getString("entryby");
                            String timestamp = obj.getString("timestamp");
                            String k = String.valueOf(i + 1);
                            mypojorec = new AddBidGetAllDetailsRec(k, id, clientid, date, type, rectype, price, noofrec, ordernature, status, tstatus, extra, entryby, timestamp);
                            //mypojo = new AddBidGetAllDetails(k, id, date, price, rectype, ordernature, tstatus, status);
                            myHashmaprec.put(i, mypojorec);

                        }
                        Collection<AddBidGetAllDetailsRec> c = myHashmaprec.values();
                        Iterator<AddBidGetAllDetailsRec> itr = c.iterator();
                        while (itr.hasNext()) {
                            AddBidGetAllDetailsRec value = (AddBidGetAllDetailsRec) itr.next();
                            serialnumberplacerec.add(value.getK());
                            addbididrec.add(value.getId());
                            addbidclientidrec.add(value.getClientid());
                            addbiddaterec.add(value.getDate());
                            addbidtyperec.add(value.getType());
                            addbidrectyperec.add(value.getRectype());
                            addbidpricerec.add(value.getPrice());
                            addbidnoofrec.add(value.getNoofrec());
                            addbidordernaturerec.add(value.getOrdernature());
                            addbidstatusrec.add(value.getStatus());
                            addbidtstatusrec.add(value.getTstatus());
                            addbidextrarec.add(value.getExtra());
                            addbidentrybyrec.add(value.getEntryby());
                            addbidtimestamprec.add(value.getTimestamp());
                        }

                        serialnumberadapterrec = serialnumberplacerec.toArray(new String[serialnumberplacerec.size()]);
                        Integer[] intarray = new Integer[serialnumberadapterrec.length];
                        int j = 0;
                        for (String str : serialnumberadapterrec) {
                            intarray[j] = Integer.parseInt(str.trim());//Exception in this line
                            j++;
                        }
                        Arrays.sort(intarray);
                        recseriala = Arrays.toString(intarray).split("[\\[\\]]")[1].split(", ");
                        System.out.println(Arrays.toString(recseriala));
                        idadapterrec = addbididrec.toArray(new String[addbididrec.size()]);
                        clientidadapterrec = addbidclientidrec.toArray(new String[addbidclientidrec.size()]);
                        dateadapterrec = addbiddaterec.toArray(new String[addbiddaterec.size()]);
                        typeadapterrec = addbidtyperec.toArray(new String[addbidtyperec.size()]);
                        rectypeadapterrec = addbidrectyperec.toArray(new String[addbidrectyperec.size()]);
                        priceadapterrec = addbidpricerec.toArray(new String[addbidpricerec.size()]);
                        noofrecadapterrec = addbidnoofrec.toArray(new String[addbidnoofrec.size()]);
                        ordernatureadapterrec = addbidordernaturerec.toArray(new String[addbidordernaturerec.size()]);
                        statusadapterrec = addbidstatusrec.toArray(new String[addbidstatusrec.size()]);
                        tstatusadapterrec = addbidtstatusrec.toArray(new String[addbidtstatusrec.size()]);
                        extraadapterrec = addbidextrarec.toArray(new String[addbidextrarec.size()]);
                        entrybyadapterrec = addbidentrybyrec.toArray(new String[addbidentrybyrec.size()]);
                        timestampadapterrec = addbidtimestamprec.toArray(new String[addbidtimestamprec.size()]);

                        AddBidRECCustomList adapter = new AddBidRECCustomList(
                                NewBidActivity.this, recseriala, rectypeadapterrec,
                                typeadapterrec, priceadapterrec, dateadapterrec,
                                noofrecadapterrec);
                        addbidreclist.setAdapter(adapter);
                        myHashmaprec.clear();
                        llreclistheading.setVisibility(View.VISIBLE); // Visible
                        addbidreclist.setVisibility(View.VISIBLE); // Visible

                        for (int k = 0; k < statusadapterrec.length; k++) {
                            System.out.println(statusadapterrec[k]);
                            if (statusadapterrec[k].equalsIgnoreCase("FALSE")) {
                                lladdbidcancelrec.setVisibility(View.GONE);
                                lladdbidsubmitrec.setVisibility(View.VISIBLE);
                                llreclistheading.setVisibility(View.VISIBLE); // Visible
                                addbidreclist.setVisibility(View.VISIBLE); // Visible
                                lladdbidsaverec.setVisibility(View.VISIBLE); // Visible
                                break;
                            } else {
                                lladdbidcancelrec.setVisibility(View.VISIBLE);
                                lladdbidsubmitrec.setVisibility(View.GONE);
                                llreclistheading.setVisibility(View.VISIBLE); // Visible
                                addbidreclist.setVisibility(View.VISIBLE); // Visible
                                lladdbidsaverec.setVisibility(View.VISIBLE); // Visible
                            }
                        }

                        addbidreclist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent,
                                                    View view, final int position, long id) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewBidActivity.this);
                                alertDialogBuilder.setMessage("Are you sure you want to Delete or Edit? Please Select one.");
                                alertDialogBuilder.setPositiveButton("Edit",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                ideditrec = idadapterrec[+position];
                                                clientideditrec = clientidadapterrec[+position];
                                                dateeditrec = dateadapterrec[+position];
                                                typeeditrec = typeadapterrec[+position];
                                                rectypeeditrec = rectypeadapterrec[+position];
                                                priceeditrec = priceadapterrec[+position];
                                                noofreceditrec = noofrecadapterrec[+position];
                                                ordernatureeditrec = ordernatureadapterrec[+position];
                                                statuseditrec = statusadapterrec[+position];
                                                tstatuseditrec = tstatusadapterrec[+position];
                                                extraeditrec = extraadapterrec[+position];
                                                entrybyeditrec = entrybyadapterrec[+position];
                                                timestampeditrec = timestampadapterrec[+position];
                                                //Intent in = new Intent(NewBidActivity.this, RecSaveNewBidActivity.class);
                                                //in.putExtra("gettype", typeedit);
                                                //in.putExtra("biddingtypeSend", biddingtypeSend);
                                                //in.putExtra("biddingtypesolorSend", biddingtypesolorSend);
                                                //in.putExtra("as", dateedit);
                                                //in.putExtra("datesendtext", datesendtext);
                                                //startActivity(in);
                                                //overridePendingTransition(R.anim.animation, R.anim.animation2);


                                                // Toast.makeText(getActivity(),"You Clicked at " + text1[+position],Toast.LENGTH_SHORT).show();

                                                //Toast.makeText(getActivity(),"You Clicked at "+ idupdate,Toast.LENGTH_SHORT).show();
                                                final Dialog dialog = new Dialog(NewBidActivity.this);
                                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                                                dialog.setContentView(R.layout.newbid_custom_editpopuprec);
                                                //dialog.setTitle("Edit Your bid...");
                                                // set the custom dialog  components - text, image and button
                                                /*
                                                 * TextView text = (TextView) dialog.findViewById(R.id.text);
                                                 * text.setText("Android custom dialog example!");
                                                 *  ImageView image =  (ImageView) dialog.findViewById(R.id.image);
                                                 * image.setImageResource(R.drawable.ic_launcher);
                                                 */


                                                updatergrectypesolor = (RadioGroup) dialog.findViewById(R.id.updatergrectypesolor);
                                                updatesolarrb1 = (RadioButton) dialog.findViewById(R.id.updatesolarrb1);
                                                updatenonsolarrb2 = (RadioButton) dialog.findViewById(R.id.updatenonsolarrb2);
                                                updateetpricerec = (EditText) dialog.findViewById(R.id.updateetpricerec);
                                                updateetnoofbloackrec = (EditText) dialog.findViewById(R.id.updateetnoofbloackrec);
                                                tvupdatemin = (TextView) dialog.findViewById(R.id.tvupdatemin);
                                                tvupdatemax = (TextView) dialog.findViewById(R.id.tvupdatemax);


                                                updateetpricerec.setText(priceeditrec);
                                                updateetnoofbloackrec.setText(noofreceditrec);
                                                if (rectypeeditrec.equalsIgnoreCase("SOLAR")) {
                                                    updatesolarrb1.setChecked(true);
                                                    tvupdatemin.setText("Min: " + solormin);
                                                    tvupdatemax.setText("Max: " + solormax);
                                                } else if (rectypeeditrec.equalsIgnoreCase("NONSOLAR")) {
                                                    updatenonsolarrb2.setChecked(true);
                                                    tvupdatemin.setText("Min: " + nonsolormin);
                                                    tvupdatemax.setText("Max: " + nonsolormax);
                                                }


                                                updatergrectypesolor.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                                                    @Override
                                                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                        // checkedId is the RadioButton selected
                                                        View radioButton = updatergrectypesolor.findViewById(checkedId);
                                                        int index = updatergrectypesolor.indexOfChild(radioButton);

                                                        // Add logic here

                                                        switch (index) {
                                                            case 1: // first button
                                                                tvupdatemin.setText("Min: " + solormin);
                                                                tvupdatemax.setText("Max: " + solormax);
                                                                //Toast.makeText(getApplicationContext(), "Selected button number " + index, 500).show();
                                                                break;
                                                            case 2: // secondbutton
                                                                tvupdatemin.setText("Min: " + nonsolormin);
                                                                tvupdatemax.setText("Max: " + nonsolormax);
                                                                //Toast.makeText(getApplicationContext(), "Selected button number " + index, 500).show();
                                                                break;
                                                        }
                                                    }
                                                });


                                                Button dialogButtonOKrec = (Button) dialog.findViewById(R.id.dialogButtonOKrec);
                                                Button dialogButtoncancelrec = (Button) dialog.findViewById(R.id.dialogButtoncancelrec);

                                                // if button is clicked, close
                                                // the custom dialog
                                                dialogButtonOKrec.setOnClickListener(new OnClickListener() {
                                                    @Override

                                                    public void onClick(View v) {
                                                        int selectedrgrectypesolorId = updatergrectypesolor.getCheckedRadioButtonId();
                                                        rgrectypesolorbutton = (RadioButton) dialog.findViewById(selectedrgrectypesolorId);
                                                        if (rgrectypesolorbutton != null) {
                                                            rgrectypesolorbuttonSend = (String) rgrectypesolorbutton.getText();
                                                        }
                                                        updateclientpricerec = updateetpricerec.getText().toString().trim();
                                                        updateclientnoofblockrec = updateetnoofbloackrec.getText().toString().trim();
                                                        int editprice = 0;
                                                        int editnumberofblock = 0;
                                                        try {
                                                            editprice = NumberFormat.getInstance().parse(updateclientpricerec).intValue();
                                                            editnumberofblock = NumberFormat.getInstance().parse(updateclientnoofblockrec).intValue();
                                                        } catch (ParseException e) {
                                                            // TODO Auto-generated catch block
                                                            e.printStackTrace();
                                                        }

                                                        if (updateclientpricerec.length() == 0) {
                                                            Toast.makeText(NewBidActivity.this, "Please fill the Price.", Toast.LENGTH_LONG).show();
                                                        } else if (updateclientnoofblockrec.length() == 0) {
                                                            Toast.makeText(NewBidActivity.this, "Please fill the Number of Rec.", Toast.LENGTH_LONG).show();
                                                        } else if (editnumberofblock > 1000000 || editnumberofblock < 0) {
                                                            Toast.makeText(NewBidActivity.this, "Number of Rec should be greater than 0 and less than 1000000.", Toast.LENGTH_LONG).show();
                                                        } else {
                                                            ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                                            if (!cd.isConnectingToInternet()) {
                                                                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                                            } else {

                                                                if (rgrectypesolorbuttonSend.equalsIgnoreCase("SOLAR")) {
                                                                    if (editprice > Integer.parseInt(solormax) || editprice < Integer.parseInt(solormin)) {
                                                                        Toast.makeText(NewBidActivity.this, "Price should be greater than or equal to" + solormin + "and less than or equal to " + solormax, Toast.LENGTH_LONG).show();
                                                                    } else {
                                                                        new HttpAsyncTaskNewBidRecUpdate().execute(domain + "/mobile/pxs_app/service/rec/newbid/savebid.php");
                                                                        dialog.dismiss();
                                                                    }
                                                                } else if (rgrectypesolorbuttonSend.equalsIgnoreCase("NONSOLAR")) {
                                                                    if (editprice > Integer.parseInt(nonsolormax) || editprice < Integer.parseInt(nonsolormin)) {
                                                                        Toast.makeText(NewBidActivity.this, "Price should be greater than or equal to" + nonsolormin + "and less than or equal to " + nonsolormax, Toast.LENGTH_LONG).show();
                                                                    } else {
                                                                        new HttpAsyncTaskNewBidRecUpdate().execute(domain + "/mobile/pxs_app/service/rec/newbid/savebid.php");
                                                                        dialog.dismiss();
                                                                    }
                                                                } else {
                                                                    //new HttpAsyncTaskNewBidRecSave().execute("http://www.mittalpower.com/mobile/pxs_app/service/rec/newbid/savebid.php");
                                                                }


                                                                //new HttpAsyncTaskNewBidRecUpdate().execute("http://www.mittalpower.com/mobile/pxs_app/service/rec/newbid/savebid.php");

                                                            }
                                                        }
                                                    }
                                                });

                                                dialogButtoncancelrec.setOnClickListener(new OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                                dialog.show();
                                            }
                                        });

                                alertDialogBuilder.setNeutralButton("Delete",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface arg0,
                                                    int arg1) {
                                                iddeleterec = idadapterrec[+position];
                                                clientiddeleterec = clientidadapterrec[+position];
                                                datedeleterec = dateadapterrec[+position];
                                                typedeleterec = typeadapterrec[+position];
                                                rectypedeleterec = rectypeadapterrec[+position];
                                                pricedeleterec = priceadapterrec[+position];
                                                noofrecdeleterec = noofrecadapterrec[+position];
                                                ordernaturedeleterec = ordernatureadapterrec[+position];
                                                statusdeleterec = statusadapterrec[+position];
                                                tstatusdeleterec = tstatusadapterrec[+position];
                                                extradeleterec = extraadapterrec[+position];
                                                entrybydeleterec = entrybyadapterrec[+position];
                                                timestampdeleterec = timestampadapterrec[+position];
                                                new HttpAsyncTaskNewBidRecDelete().execute(domain + "/mobile/pxs_app/service/rec/newbid/deletebid.php");

                                            }
                                        });

                                alertDialogBuilder.setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                // finish();
                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder
                                        .create();
                                alertDialog.show();
                            }
                        });

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class HttpAsyncTaskNewBidRecSave extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AddBidSubmit addBidSubmit = new AddBidSubmit();
            addBidSubmit.setType(rgrectypebuttongettypeSend);
            addBidSubmit.setBidtype(rgrecbiddigntypebuttonSend);
            addBidSubmit.setRectype(rgrectypesolorbuttonSend);
            addBidSubmit.setBiddate(recplacedbiddatesend);
            addBidSubmit.setAddbidsaveaccesskey(dbacess);
            addBidSubmit.setId("null");
            addBidSubmit.setPrice(popupclientpricerec);
            addBidSubmit.setNoofrec(popupclientnoofblockrec);
            return AddBidRECSaveAPIResponse.POST(urls[0], addBidSubmit);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            // Toast.makeText(getActivity(), "AddBidSave" +
            // result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {

                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("msg");
                        String value = jSONObject.getString("value");

                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                            pwindorec.dismiss();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskNewBidgetrecbiddetail().execute(domain + "/mobile/pxs_app/service/rec/newbid/getrecbiddetail.php");
        }
    }

    private class HttpAsyncTaskNewBidRecDelete extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AddBidRecDelate addBidRecDelate = new AddBidRecDelate();
            addBidRecDelate.setId(iddeleterec);
            addBidRecDelate.setType(typedeleterec);
            addBidRecDelate.setRectype(rectypedeleterec);
            addBidRecDelate.setOrdernature(ordernaturedeleterec);
            addBidRecDelate.setDate(datedeleterec);
            addBidRecDelate.setAccess_key(dbacess);
            return AddBidRECDeleteAPIResponse.POST(urls[0], addBidRecDelate);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get REC Delete BID:", result);
            prgDialog.hide();
            // Toast.makeText(getActivity(), "AddBidSave" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {

                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("msg");
                        String value = jSONObject.getString("value");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskNewBidgetrecbiddetail().execute(domain + "/mobile/pxs_app/service/rec/newbid/getrecbiddetail.php");
        }
    }

    private class HttpAsyncTaskNewBidRecUpdate extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AddBidSubmit addBidSubmit = new AddBidSubmit();
            addBidSubmit.setType(typeeditrec);
            addBidSubmit.setBidtype(ordernatureeditrec);
            addBidSubmit.setRectype(rgrectypesolorbuttonSend);
            addBidSubmit.setBiddate(dateeditrec);
            addBidSubmit.setAddbidsaveaccesskey(dbacess);
            addBidSubmit.setId(ideditrec);
            addBidSubmit.setPrice(updateclientpricerec);
            addBidSubmit.setNoofrec(updateclientnoofblockrec);
            return AddBidRECSaveAPIResponse.POST(urls[0], addBidSubmit);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            // Toast.makeText(getActivity(), "AddBidSave" +
            // result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {

                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("msg");
                        String value = jSONObject.getString("value");

                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskNewBidgetrecbiddetail().execute(domain + "/mobile/pxs_app/service/rec/newbid/getrecbiddetail.php");
        }
    }

    private class HttpAsyncTaskNewBidgetrecsubmitrecbid extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AddBidSubmit addBidSubmit = new AddBidSubmit();
            addBidSubmit.setType(rgrectypebuttongettypeSend);
            addBidSubmit.setBidtype(rgrecbiddigntypebuttonSend);
            addBidSubmit.setRectype(rectypeadapterrec[0]);
            addBidSubmit.setBiddate(recplacedbiddatesend);
            addBidSubmit.setAddbidsaveaccesskey(dbacess);
            return AddBidRECSUBMITGetallAPIResponse.POST(urls[0], addBidSubmit);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            // Toast.makeText(getActivity(), "AddBidSave" +
            // result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {

                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {

                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("msg");
                        String value = jSONObject.getString("value");

                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

            new HttpAsyncTaskNewBidgetrecbiddetail().execute(domain + "/mobile/pxs_app/service/rec/newbid/getrecbiddetail.php");

        }
    }

    private class HttpAsyncTaskNewSaveClientBidCancelALLSubmitDataREC extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AddBidRecDelate addBidRecDelate = new AddBidRecDelate();
            addBidRecDelate.setId(cancelallidrec);
            addBidRecDelate.setType(rgrectypebuttongettypeSend);
            addBidRecDelate.setRectype(rgrectypesolorbuttonSend);
            addBidRecDelate.setOrdernature(rgrecbiddigntypebuttonSend);
            addBidRecDelate.setDate(recplacedbiddatesend);
            addBidRecDelate.setAccess_key(dbacess);
            return AddBidRECDeleteAPIResponse.POST(urls[0], addBidRecDelate);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get REC Delete BID:", result);
            prgDialog.hide();
            // Toast.makeText(getActivity(), "AddBidSave" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {

                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("msg");
                        String value = jSONObject.getString("value");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (JSONException e) {
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskNewBidgetrecbiddetail().execute(domain + "/mobile/pxs_app/service/rec/newbid/getrecbiddetail.php");
        }
    }

    // TAM API CALL
    private class HttpAsyncTaskgettambiddetails extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            TamGetAllDetails tamGetAllDetails = new TamGetAllDetails();
            tamGetAllDetails.setProducttype(producttypesend);
            tamGetAllDetails.setType(rb1_paltformproducttypeSend);
            tamGetAllDetails.setDate(tam_date_Send);
            tamGetAllDetails.setAccess_key(dbacess);
            return GetTamBidDetailsAPIResponse.POST(urls[0], tamGetAllDetails);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONArray jsonarray = new JSONArray(result);
                    if (jsonarray.isNull(0)) {
                        Toast.makeText(NewBidActivity.this, "Data is empty.", Toast.LENGTH_LONG).show();
						/*tvnobidnull.setBackgroundResource(R.drawable.sad);
						tvnobidnulltext.setText("You haven't placed no bid yet.");*/
                        //tvnobidnull.setVisibility(View.VISIBLE);
                        ll_tam_second_heading.setVisibility(View.VISIBLE);
                        ll_tam_new_cancel_submit.setVisibility(View.VISIBLE);
                        ll_tam_cancelbutton.setVisibility(View.GONE);
                        ll_tam_submitbutton.setVisibility(View.GONE);
                        ll_TAMlist_heading.setVisibility(View.GONE);
                        add_tam_list.setVisibility(View.GONE);

                    } else {
						/*nobidlistheading.setVisibility(View.VISIBLE);
						list.setVisibility(View.VISIBLE);
						tvnobidnull.setVisibility(View.GONE);
						tvnobidnulltext.setVisibility(View.GONE);*/
                        ll_tam_second_heading.setVisibility(View.VISIBLE);
                        ll_tam_new_cancel_submit.setVisibility(View.VISIBLE);
                        ll_TAMlist_heading.setVisibility(View.VISIBLE);
                        add_tam_list.setVisibility(View.VISIBLE);

                        tamgetserialnumber.clear();
                        tamgetid.clear();
                        tamgetdateshow.clear();
                        tamgettype.clear();
                        tamgetproducttype.clear();
                        tamgetfromtime.clear();
                        tamgettotime.clear();
                        tamgetminprice.clear();
                        tamgetmaxprice.clear();
                        tamgetordernature.clear();
                        tamgetbidquantum.clear();
                        tamgetbidcode.clear();
                        tamgetstatus.clear();
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject obj = jsonarray.getJSONObject(i);
                            String id = obj.getString("id");
                            String clientid = obj.getString("clientid");
                            String date = obj.getString("date");
                            String[] parts = date.split("-");
                            String datepartone = parts[0];
                            String dateparttwo = parts[1];
                            String datepartthree = parts[2];
                            String dateshow = (datepartthree + "-" + dateparttwo + "-" + datepartone);

                            String type = obj.getString("type");
                            String producttype = obj.getString("producttype");
                            String fromtime = obj.getString("fromtime");
                            String totime = obj.getString("totime");
                            String minprice = obj.getString("minprice");
                            String maxprice = obj.getString("maxprice");
                            String ordernature = obj.getString("ordernature");
                            String bidquantum = obj.getString("bidquantum");
                            String bidcode = obj.getString("bidcode");
                            String status = obj.getString("status");
                            int counting = (i + 1);
                            String j = String.valueOf(counting);
                            tamgetserialnumber.add(j);
                            tamgetid.add(id);
                            tamgetdateshow.add(dateshow);
                            tamgettype.add(type);
                            tamgetproducttype.add(producttype);
                            tamgetfromtime.add(fromtime);
                            tamgettotime.add(totime);
                            tamgetminprice.add(minprice);
                            tamgetmaxprice.add(maxprice);
                            tamgetordernature.add(ordernature);
                            tamgetbidquantum.add(bidquantum);
                            tamgetbidcode.add(bidcode);
                            tamgetstatus.add(status);
                        }

                        TamGetCustomList adapter = new TamGetCustomList(NewBidActivity.this, tamgetserialnumber, tamgetfromtime, tamgettotime, tamgetbidquantum);
                        add_tam_list.setAdapter(adapter);
                        Utility.setListViewHeightBasedOnChildren(add_tam_list);

                        add_tam_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent,
                                                    View view, final int position, long id) {
                                // Toast.makeText(getActivity(),"You Clicked at " +idadapter[+position],Toast.LENGTH_SHORT).show();
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewBidActivity.this);
                                alertDialogBuilder.setMessage("Are you sure you want to Delete or Edit? Please Select one.");
                                alertDialogBuilder.setPositiveButton("Edit",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
												/*idtamdelete = tamgetid.get(position);
												typetamdelete = tamgettype.get(position);
												producttypetamdelete = tamgetproducttype.get(position);
												dateshowtamdelete = tamgetdateshow.get(position);*/
                                                updatetamgetid = tamgetid.get(position);
                                                String[] dateparts = tamgetdateshow.get(position).split("-");
                                                String datepartone = dateparts[0];
                                                String dateparttwo = dateparts[1];
                                                String datepartthree = dateparts[2];
                                                updatetamgetdateshow = (datepartthree + "-" + dateparttwo + "-" + datepartone);
                                                updatetamgettype = tamgettype.get(position);
                                                updatetamgetproducttype = tamgetproducttype.get(position);
                                                tamgetfromtime.get(position);
                                                tamgettotime.get(position);
                                                tamgetminprice.get(position);
                                                tamgetmaxprice.get(position);
                                                tamgetordernature.get(position);
                                                tamgetbidquantum.get(position);
                                                tamgetbidcode.get(position);

                                                final Dialog dialog = new Dialog(NewBidActivity.this);
                                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                                                dialog.setContentView(R.layout.newbid_custom_editpopuptam);

                                                updatetampopupcbclienttype = (RadioGroup) dialog.findViewById(R.id.updatetampopupcbclienttype);
                                                updatetamclienttypeBUY = (RadioButton) dialog.findViewById(R.id.updatetamclienttypeBUY);
                                                updatetamclienttypeSELL = (RadioButton) dialog.findViewById(R.id.updatetamclienttypeSELL);
                                                updatetampopupetslotfromfirst = (TextView) dialog.findViewById(R.id.updatetampopupetslotfromfirst);
                                                updatetampopupetslotfromsecond = (TextView) dialog.findViewById(R.id.updatetampopupetslotfromsecond);
                                                updatetampopupetslotfromthird = (TextView) dialog.findViewById(R.id.updatetampopupetslotfromthird);
                                                updatetampopupetslottofirst = (TextView) dialog.findViewById(R.id.updatetampopupetslottofirst);
                                                updatetampopupetslottosecond = (TextView) dialog.findViewById(R.id.updatetampopupetslottosecond);
                                                updatetampopupetslottothird = (TextView) dialog.findViewById(R.id.updatetampopupetslottothird);
                                                updatetampopupetprice = (EditText) dialog.findViewById(R.id.updatetampopupetprice);
                                                updatetampopupetmaxprice = (EditText) dialog.findViewById(R.id.updatetampopupetmaxprice);
                                                updatetampopupetbid = (EditText) dialog.findViewById(R.id.updatetampopupetbid);


                                                Double abc = Double.parseDouble(tamgetbidquantum.get(position));
                                                if (abc > 0) {
                                                    updatetamclienttypeBUY.setChecked(true);
                                                    updatetampopupetbid.setText(tamgetbidquantum.get(position));
                                                } else {
                                                    updateRBclienttypeSELL.setChecked(true);
                                                    String tambidupadte = tamgetbidquantum.get(position).replace("-", "");
                                                    updatetampopupetbid.setText(tambidupadte);
                                                }

                                                String[] parts = tamgetfromtime.get(position).split(":");
                                                String part1 = parts[0]; // 004
                                                String part2 = parts[1]; // 004
                                                updatetampopupetslotfromfirst.setText(part1);
                                                updatetampopupetslotfromsecond.setText(part2);
                                                String[] partsto = tamgettotime.get(position).split(":");
                                                String partone = partsto[0]; // 004
                                                String parttwo = partsto[1]; // 004
                                                updatetampopupetslottofirst.setText(partone);
                                                updatetampopupetslottosecond.setText(parttwo);
                                                updatetampopupetprice.setText(tamgetminprice.get(position));
                                                updatetampopupetmaxprice.setText(tamgetmaxprice.get(position));

                                                updatetampopupetslotfromfirst.setOnClickListener(new OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                                                        final String[] items = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
                                                                "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"};
                                                        AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                                                        //set the title for alert dialog
                                                        builder.setTitle("Choose Select From HH:");
                                                        //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                                                        builder.setItems(items, new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int item) {
                                                                // setting the button text to the selected itenm from the list
                                                                updatetampopupetslotfromfirst.setText(items[item]);

                                                            }
                                                        });
                                                        //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                                                        builder.setCancelable(false)
                                                                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int id) {
                                                                        //When clicked on CANCEL button the dalog will be dismissed
                                                                        dialog.dismiss();
                                                                    }
                                                                });
                                                        // Creating alert dialog
                                                        AlertDialog alert = builder.create();
                                                        //Showing alert dialog
                                                        alert.show();
                                                    }
                                                });


                                                updatetampopupetslotfromsecond.setOnClickListener(new OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                                                        final String[] items = {"00", "15", "30", "45"};
                                                        AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                                                        //set the title for alert dialog
                                                        builder.setTitle("Choose Select From MM: ");
                                                        //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                                                        builder.setItems(items, new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int item) {
                                                                // setting the button text to the selected itenm from the list
                                                                updatetampopupetslotfromsecond.setText(items[item]);

                                                            }
                                                        });
                                                        //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                                                        builder.setCancelable(false)
                                                                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int id) {
                                                                        //When clicked on CANCEL button the dalog will be dismissed
                                                                        dialog.dismiss();
                                                                    }
                                                                });
                                                        // Creating alert dialog
                                                        AlertDialog alert = builder.create();
                                                        //Showing alert dialog
                                                        alert.show();
                                                    }
                                                });

                                                updatetampopupetslottofirst.setOnClickListener(new OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                                                        final String[] items = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
                                                                "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"};
                                                        AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                                                        //set the title for alert dialog
                                                        builder.setTitle("Choose Select From HH:");
                                                        //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                                                        builder.setItems(items, new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int item) {
                                                                // setting the button text to the selected itenm from the list
                                                                updatetampopupetslottofirst.setText(items[item]);

                                                            }
                                                        });
                                                        //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                                                        builder.setCancelable(false)
                                                                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int id) {
                                                                        //When clicked on CANCEL button the dalog will be dismissed
                                                                        dialog.dismiss();
                                                                    }
                                                                });
                                                        // Creating alert dialog
                                                        AlertDialog alert = builder.create();
                                                        //Showing alert dialog
                                                        alert.show();
                                                    }
                                                });

                                                updatetampopupetslottosecond.setOnClickListener(new OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        //List of items to be show in  alert Dialog are stored in array of strings/char sequences
                                                        final String[] items = {"00", "15", "30", "45"};
                                                        AlertDialog.Builder builder = new AlertDialog.Builder(NewBidActivity.this);
                                                        //set the title for alert dialog
                                                        builder.setTitle("Choose Select From MM: ");
                                                        //set items to alert dialog. i.e. our array , which will be shown as list view in alert dialog
                                                        builder.setItems(items, new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int item) {
                                                                // setting the button text to the selected itenm from the list
                                                                updatetampopupetslottosecond.setText(items[item]);

                                                            }
                                                        });
                                                        //Creating CANCEL button in alert dialog, to dismiss the dialog box when nothing is selected
                                                        builder.setCancelable(false)
                                                                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int id) {
                                                                        //When clicked on CANCEL button the dalog will be dismissed
                                                                        dialog.dismiss();
                                                                    }
                                                                });
                                                        // Creating alert dialog
                                                        AlertDialog alert = builder.create();
                                                        //Showing alert dialog
                                                        alert.show();
                                                    }
                                                });
                                                Button upadtetampopupButtonSave = (Button) dialog.findViewById(R.id.upadtetampopupButtonSave);
                                                Button updatetampopupButtoncancel = (Button) dialog.findViewById(R.id.updatetampopupButtoncancel);
                                                LinearLayout updatetamtvcross = (LinearLayout) dialog.findViewById(R.id.updatetamtvcross);

                                                // if button is clicked, close
                                                // the custom dialog
                                                upadtetampopupButtonSave.setOnClickListener(new OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {

                                                        int selectedupdatebiddingtypeId = updatetampopupcbclienttype.getCheckedRadioButtonId();
                                                        updatetambiddingtype = (RadioButton) dialog.findViewById(selectedupdatebiddingtypeId);
                                                        if (updatetambiddingtype != null) {
                                                            updatetemgetclienttype = (String) updatetambiddingtype.getText();
                                                        }

                                                        updatetemclientslotfromfirst = updatetampopupetslotfromfirst.getText().toString().trim();
                                                        updatetemclientslotfromsecond = updatetampopupetslotfromsecond.getText().toString().trim();

                                                        updatetemclientslottofirst = updatetampopupetslottofirst.getText().toString().trim();
                                                        updatetemclientslottosecond = updatetampopupetslottosecond.getText().toString().trim();

                                                        updatetemclientprice = updatetampopupetprice.getText().toString().trim();
                                                        updatetemclientmaxprice = updatetampopupetmaxprice.getText().toString().trim();
                                                        updatetemclientbid = updatetampopupetbid.getText().toString().trim();

                                                        int editprice = 0;
                                                        int editmaxprice = 0;
                                                        try {
                                                            editprice = NumberFormat.getInstance().parse(updatetemclientprice).intValue();
                                                            editmaxprice = NumberFormat.getInstance().parse(updatetemclientmaxprice).intValue();
                                                        } catch (ParseException e) {
                                                            // TODO Auto-generated catch block
                                                            e.printStackTrace();
                                                        }


                                                        if (updatetemclientprice.length() == 0) {
                                                            Toast.makeText(NewBidActivity.this, "Please fill the Price.", Toast.LENGTH_LONG).show();
                                                        }
                                                        if (updatetemclientmaxprice.length() == 0) {
                                                            Toast.makeText(NewBidActivity.this, "Please fill the Price.", Toast.LENGTH_LONG).show();
                                                        } else if (updatetemclientbid.length() == 0) {
                                                            Toast.makeText(NewBidActivity.this, "Please fill the Bid.", Toast.LENGTH_LONG).show();
                                                        } else if (editprice > 20000) {
                                                            Toast.makeText(NewBidActivity.this, "Price should be less than 20000.", Toast.LENGTH_LONG).show();
                                                        } else if (editmaxprice > 40000) {
                                                            Toast.makeText(NewBidActivity.this, "Price should be less than 40000.", Toast.LENGTH_LONG).show();
                                                        } else {
                                                            ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                                            if (!cd.isConnectingToInternet()) {
                                                                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                                            } else {
                                                                new HttpAsyncTaskEdittambid().execute(domain + Constant.SAVE_TAM_BID);
                                                                dialog.dismiss();
                                                            }
                                                        }


                                                    }
                                                });

                                                updatetampopupButtoncancel.setOnClickListener(new OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        dialog.dismiss();
                                                    }
                                                });

                                                updatetamtvcross.setOnClickListener(new OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        dialog.dismiss();
                                                    }
                                                });

                                                dialog.show();

                                            }
                                        });

                                alertDialogBuilder.setNeutralButton("Delete",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                //Toast.makeText(NewBidActivity.this,"You Clicked at "+ tamgetid.get(position), Toast.LENGTH_SHORT).show();
                                                idtamdelete = tamgetid.get(position);
                                                typetamdelete = tamgettype.get(position);
                                                producttypetamdelete = tamgetproducttype.get(position);
                                                dateshowtamdelete = tamgetdateshow.get(position);

                                                ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                                if (!cd.isConnectingToInternet()) {
                                                    Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                                } else {
													/*new HttpAsyncTaskdeletetambid()
															.execute("https://" + Constant.BASE_URL + "/mobile/pxs_app/service/newbid/deletebid.php"); */
                                                    new HttpAsyncTaskdeletetambid().execute(domain + Constant.DELETE_TAM_BID);
                                                }
                                            }
                                        });

                                alertDialogBuilder.setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                // finish();
                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder
                                        .create();
                                alertDialog.show();
                            }
                        });

                        for (int k = 0; k < tamgetstatus.size(); k++) {
                            System.out.println(tamgetstatus.get(k));
                            if (tamgetstatus.get(k).equalsIgnoreCase("FALSE")) {
								/*lladdbidcancelrec.setVisibility(View.GONE);
								lladdbidsubmitrec.setVisibility(View.VISIBLE);
								llreclistheading.setVisibility(View.VISIBLE); // Visible
								addbidreclist.setVisibility(View.VISIBLE); // Visible
								lladdbidsaverec.setVisibility(View.VISIBLE); // Visible*/
                                ll_tam_cancelbutton.setVisibility(View.GONE);
                                ll_tam_submitbutton.setVisibility(View.VISIBLE);
                                break;
                            } else {
								/*lladdbidcancelrec.setVisibility(View.VISIBLE);
								lladdbidsubmitrec.setVisibility(View.GONE);
								llreclistheading.setVisibility(View.VISIBLE); // Visible
								addbidreclist.setVisibility(View.VISIBLE); // Visible
								lladdbidsaverec.setVisibility(View.VISIBLE); // Visible*/
                                ll_tam_cancelbutton.setVisibility(View.VISIBLE);
                                ll_tam_submitbutton.setVisibility(View.GONE);
                            }
                        }

                    }

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class HttpAsyncTasksavetambid extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            SaveTamBid saveTamBid = new SaveTamBid();
            saveTamBid.setId("null");
            saveTamBid.setType(rb1_paltformproducttypeSend);
            saveTamBid.setProducttype(producttypesend);
            saveTamBid.setOrdernature(tampopupcbclienttypesendBUY);
            saveTamBid.setDate(tam_date_Send);
            saveTamBid.setFromtime(tampopupclientslotfromfirst + ":" + tampopupclientetslotfromsecond);
            saveTamBid.setTotime(tampopupclientslottofirst + ":" + tampopupclientetslottosecond);
            saveTamBid.setBidquantum(tampopupclientbid);
            saveTamBid.setMinprice(tampopupclientprice);
            saveTamBid.setMaxprice(tampopupclientmaxprice);
            saveTamBid.setAccess_key(dbacess);
            return SaveTamBidAPIResponse.POST(urls[0], saveTamBid);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //Toast.makeText(NewBidActivity.this, "GET TAM Detail" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {

                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("message");
                        String value = jSONObject.getString("value");

                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                            pwindotam.dismiss();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }

                    }
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskgettambiddetails().execute(domain + Constant.GET_TAM_BIDDETAIL);
        }
    }

    private class HttpAsyncTasksubmittambid extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            TamGetAllDetails tamGetAllDetails = new TamGetAllDetails();
            tamGetAllDetails.setProducttype(producttypesend);
            tamGetAllDetails.setType(rb1_paltformproducttypeSend);
            tamGetAllDetails.setDate(tam_date_Send);
            tamGetAllDetails.setAccess_key(dbacess);
            return SubmitTamBidDetailsAPIResponse.POST(urls[0], tamGetAllDetails);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //Toast.makeText(NewBidActivity.this, "GET TAM Detail" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {

                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("message");
                        String value = jSONObject.getString("value");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskgettambiddetails().execute(domain + Constant.GET_TAM_BIDDETAIL);
        }
    }

    private class HttpAsyncTaskdeletetambid extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            DeletetamBid deletetamBid = new DeletetamBid();
            deletetamBid.setId(idtamdelete);
            deletetamBid.setType(typetamdelete);
            deletetamBid.setProducttype(producttypetamdelete);
            deletetamBid.setDate(dateshowtamdelete);
            deletetamBid.setAccess_key(dbacess);
            return DeletetamBidAPIResponse.POST(urls[0], deletetamBid);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //Toast.makeText(NewBidActivity.this, "GET TAM Detail" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONArray jsonarray = new JSONArray(result);
                    //	for (int i = 0; i < jsonarray.length(); i++)
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

            new HttpAsyncTaskgettambiddetails().execute(domain + Constant.GET_TAM_BIDDETAIL);
        }

    }

    private class HttpAsyncTaskalldeletetambid extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            DeletetamBid deletetamBid = new DeletetamBid();
            deletetamBid.setId(tamcancelallid);
            deletetamBid.setType(rb1_paltformproducttypeSend);
            deletetamBid.setProducttype(producttypesend);
            deletetamBid.setDate(tam_date_Send);
            deletetamBid.setAccess_key(dbacess);
            return DeletetamBidAPIResponse.POST(urls[0], deletetamBid);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //Toast.makeText(NewBidActivity.this, "GET TAM Detail" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONArray jsonarray = new JSONArray(result);
                    //	for (int i = 0; i < jsonarray.length(); i++)
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

            new HttpAsyncTaskgettambiddetails().execute(domain + Constant.GET_TAM_BIDDETAIL);
        }

    }

    private class HttpAsyncTaskEdittambid extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            SaveTamBid saveTamBid = new SaveTamBid();
            saveTamBid.setId(updatetamgetid);
            saveTamBid.setType(updatetamgettype);
            saveTamBid.setProducttype(updatetamgetproducttype);
            saveTamBid.setOrdernature(updatetemgetclienttype);
            saveTamBid.setDate(updatetamgetdateshow);
            saveTamBid.setFromtime(updatetemclientslotfromfirst + ":" + updatetemclientslotfromsecond);
            saveTamBid.setTotime(updatetemclientslottofirst + ":" + updatetemclientslottosecond);
            saveTamBid.setBidquantum(updatetemclientbid);
            saveTamBid.setMinprice(updatetemclientprice);
            saveTamBid.setMaxprice(updatetemclientmaxprice);
            saveTamBid.setAccess_key(dbacess);
            return SaveTamBidAPIResponse.POST(urls[0], saveTamBid);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //Toast.makeText(NewBidActivity.this, "GET TAM Detail" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {

                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("message");
                        String value = jSONObject.getString("value");

                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                            //pwindotam.dismiss();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }

                    }
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskgettambiddetails().execute(domain + Constant.GET_TAM_BIDDETAIL);
        }
    }

    /// Escerts API Call
    private class HttpAsyncTaskEscertsgetallrectradingdate extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Login login = new Login();
            login.setAccess_key(dbacess);
            return NoBidGetAllDataAPIResponse.POST(urls[0], login);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONArray jsonarray = new JSONArray(result);
                    escertsdate_array.add("Select Trading Date");
                    escertsprintdate_array.add("Select Trading Date");
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonObject = jsonarray.getJSONObject(i);
                        if (jsonObject.has("date")) {
                            String date = jsonObject.getString("date");
                            escertsdate_array.add(date);
                        }
                        if (jsonObject.has("printDate")) {
                            String printDate = jsonObject.getString("printDate");
                            escertsprintdate_array.add(printDate);
                        }
                    }
                    escerts_adapter_date = new ArrayAdapter<String>(
                            NewBidActivity.this,
                            R.layout.spinner_item,
                            escertsprintdate_array);
                    escerts_adapter_date
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    escertsetxt_fromdate.setAdapter(escerts_adapter_date);

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    if (result != null) {
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Invalid JSON: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

        }

    }

    private class HttpAsyncTaskNewBidgetescertbiddetail extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AddBidSubmit addBidSubmit = new AddBidSubmit();
            addBidSubmit.setType(rgescerttypebuttongettypeSend);
            addBidSubmit.setBidtype(rgescertbiddigntypebuttonSend);
            addBidSubmit.setBiddate(escertplacedbiddatesend);
            addBidSubmit.setAddbidsaveaccesskey(dbacess);
            return AddBidRECGetallAPIResponse.POST(urls[0], addBidSubmit);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONArray jsonarray = new JSONArray(result);
                    serialnumberplaceescert.clear();
                    addbididescert.clear();
                    addbidclientidescert.clear();
                    addbiddateescert.clear();
                    addbidtypeescert.clear();
                    addbidpriceescert.clear();
                    addbidnoofescert.clear();
                    addbidordernatureescert.clear();
                    addbidstatusescert.clear();
                    addbidtstatusescert.clear();
                    addbidextraescert.clear();
                    addbidentrybyescert.clear();
                    addbidtimestampescert.clear();
                    if (jsonarray.length() == 0) {
                        //Toast.makeText(NewBidActivity.this, "NO Data found!", Toast.LENGTH_LONG).show();
                        lladdbidsaveescert.setVisibility(View.VISIBLE); // Visible
                        lladdbidcancelescert.setVisibility(View.GONE);
                        lladdbidsubmitescert.setVisibility(View.GONE);
                        llescertlistheading.setVisibility(View.GONE);
                        addbidescertlist.setVisibility(View.GONE);
                    } else {
                        lladdbidsaveescert.setVisibility(View.VISIBLE); // Visible
                        lladdbidform.setVisibility(View.GONE); // GONE
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject obj = jsonarray.getJSONObject(i);
                            String id = obj.getString("id");
                            String clientid = obj.getString("clientid");
                            String date = obj.getString("date");
                            String type = obj.getString("type");
                            String price = obj.getString("price");
                            String noofcertificates = obj.getString("noofcertificates");
                            String ordernature = obj.getString("ordernature");
                            String status = obj.getString("status");
                            String tstatus = obj.getString("tstatus");
                            String extra = obj.getString("extra");
                            String entryby = obj.getString("entryby");
                            String timestamp = obj.getString("timestamp");
                            String k = String.valueOf(i + 1);
                            mypojoescert = new AddBidGetAllDetailsRec(k, id, clientid, date, type, price, noofcertificates, ordernature, status, tstatus, extra, entryby, timestamp);
                            //mypojo = new AddBidGetAllDetails(k, id, date, price, rectype, ordernature, tstatus, status);
                            myHashmapescert.put(i, mypojoescert);

                        }
                        Collection<AddBidGetAllDetailsRec> c = myHashmapescert.values();
                        Iterator<AddBidGetAllDetailsRec> itr = c.iterator();
                        while (itr.hasNext()) {
                            AddBidGetAllDetailsRec value = (AddBidGetAllDetailsRec) itr.next();
                            serialnumberplaceescert.add(value.getK());
                            addbididescert.add(value.getId());
                            addbidclientidescert.add(value.getClientid());
                            addbiddateescert.add(value.getDate());
                            addbidtypeescert.add(value.getType());
                            addbidpriceescert.add(value.getPrice());
                            addbidnoofescert.add(value.getNoofcertificates());
                            addbidordernatureescert.add(value.getOrdernature());
                            addbidstatusescert.add(value.getStatus());
                            addbidtstatusescert.add(value.getTstatus());
                            addbidextraescert.add(value.getExtra());
                            addbidentrybyescert.add(value.getEntryby());
                            addbidtimestampescert.add(value.getTimestamp());
                        }

                        serialnumberadapterescert = serialnumberplaceescert.toArray(new String[serialnumberplaceescert.size()]);
                        Integer[] intarray = new Integer[serialnumberadapterescert.length];
                        int j = 0;
                        for (String str : serialnumberadapterescert) {
                            intarray[j] = Integer.parseInt(str.trim());//Exception in this line
                            j++;
                        }
                        Arrays.sort(intarray);
                        escertseriala = Arrays.toString(intarray).split("[\\[\\]]")[1].split(", ");
                        System.out.println(Arrays.toString(escertseriala));
                        idadapterescert = addbididescert.toArray(new String[addbididescert.size()]);
                        clientidadapterescert = addbidclientidescert.toArray(new String[addbidclientidescert.size()]);
                        dateadapterescert = addbiddateescert.toArray(new String[addbiddateescert.size()]);
                        typeadapterescert = addbidtypeescert.toArray(new String[addbidtypeescert.size()]);
                        priceadapterescert = addbidpriceescert.toArray(new String[addbidpriceescert.size()]);
                        noofrecadapterescert = addbidnoofescert.toArray(new String[addbidnoofescert.size()]);
                        ordernatureadapterescert = addbidordernatureescert.toArray(new String[addbidordernatureescert.size()]);
                        statusadapterescert = addbidstatusescert.toArray(new String[addbidstatusescert.size()]);
                        tstatusadapterescert = addbidtstatusescert.toArray(new String[addbidtstatusescert.size()]);
                        extraadapterescert = addbidextraescert.toArray(new String[addbidextraescert.size()]);
                        entrybyadapterescert = addbidentrybyescert.toArray(new String[addbidentrybyescert.size()]);
                        timestampadapterescert = addbidtimestampescert.toArray(new String[addbidtimestampescert.size()]);

                        AddBidEscertsCustomList adapter = new AddBidEscertsCustomList(
                                NewBidActivity.this, escertseriala,
                                typeadapterescert, priceadapterescert, dateadapterescert,
                                noofrecadapterescert);
                        addbidescertlist.setAdapter(adapter);
                        myHashmapescert.clear();
                        llescertlistheading.setVisibility(View.VISIBLE); // Visible
                        addbidescertlist.setVisibility(View.VISIBLE); // Visible

                        for (int k = 0; k < statusadapterescert.length; k++) {
                            System.out.println(statusadapterescert[k]);
                            if (statusadapterescert[k].equalsIgnoreCase("FALSE")) {
                                lladdbidcancelescert.setVisibility(View.GONE);
                                lladdbidsubmitescert.setVisibility(View.VISIBLE);
                                llescertlistheading.setVisibility(View.VISIBLE); // Visible
                                addbidescertlist.setVisibility(View.VISIBLE); // Visible
                                lladdbidsaveescert.setVisibility(View.VISIBLE); // Visible
                                break;
                            } else {
                                lladdbidcancelescert.setVisibility(View.VISIBLE); // Visible
                                lladdbidsubmitescert.setVisibility(View.GONE);
                                llescertlistheading.setVisibility(View.VISIBLE); // Visible
                                addbidescertlist.setVisibility(View.VISIBLE); // Visible
                                lladdbidsaveescert.setVisibility(View.VISIBLE); // Visible
                            }
                        }

                        addbidescertlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent,
                                                    View view, final int position, long id) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewBidActivity.this);
                                alertDialogBuilder.setMessage("Are you sure you want to Delete or Edit? Please Select one.");
                                alertDialogBuilder.setPositiveButton("Edit",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                ideditescert = idadapterescert[+position];
                                                clientideditescert = clientidadapterescert[+position];
                                                dateeditescert = dateadapterescert[+position];
                                                typeeditescert = typeadapterescert[+position];
                                                priceeditescert = priceadapterescert[+position];
                                                noofcertificateeditescert = noofrecadapterescert[+position];
                                                ordernatureeditescert = ordernatureadapterescert[+position];
                                                statuseditescert = statusadapterescert[+position];
                                                tstatuseditescert = tstatusadapterescert[+position];
                                                extraeditescert = extraadapterescert[+position];
                                                entrybyeditescert = entrybyadapterescert[+position];
                                                timestampeditescert = timestampadapterescert[+position];

                                                final Dialog dialog = new Dialog(NewBidActivity.this);
                                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
                                                dialog.setContentView(R.layout.newbid_custom_editpopupescert);


                                                updateetpriceescert = (EditText) dialog.findViewById(R.id.updateetpriceescert);
                                                updateetnoofcertificateescert = (EditText) dialog.findViewById(R.id.updateetnoofcertificateescert);
                                                tvupdateminescert = (TextView) dialog.findViewById(R.id.tvupdateminescert);
                                                tvupdatemaxescert = (TextView) dialog.findViewById(R.id.tvupdatemaxescert);

                                                updateetpriceescert.setText(priceeditescert);
                                                updateetnoofcertificateescert.setText(noofcertificateeditescert);

                                                Button dialogButtonOKescert = (Button) dialog.findViewById(R.id.dialogButtonOKescert);
                                                Button dialogButtoncancelescert = (Button) dialog.findViewById(R.id.dialogButtoncancelescert);

                                                // if button is clicked, close
                                                // the custom dialog
                                                dialogButtonOKescert.setOnClickListener(new OnClickListener() {
                                                    @Override

                                                    public void onClick(View v) {

                                                        updateclientpriceescert = updateetpriceescert.getText().toString().trim();
                                                        updateclientnoofcertificateescert = updateetnoofcertificateescert.getText().toString().trim();
                                                        int editprice = 0;
                                                        int editnumberofcertificate = 0;
                                                        try {
                                                            editprice = NumberFormat.getInstance().parse(updateclientpriceescert).intValue();
                                                            editnumberofcertificate = NumberFormat.getInstance().parse(updateclientnoofcertificateescert).intValue();
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }

                                                        if (updateclientpriceescert.length() == 0) {
                                                            Toast.makeText(NewBidActivity.this, "Please fill the Price.", Toast.LENGTH_LONG).show();
                                                        } else if (updateclientnoofcertificateescert.length() == 0) {
                                                            Toast.makeText(NewBidActivity.this, "Please fill the Number of Certificate.", Toast.LENGTH_LONG).show();
                                                        } else if (editprice > 1000000 || editprice < 0) {
                                                            Toast.makeText(NewBidActivity.this, "Please fill the Price.", Toast.LENGTH_LONG).show();
                                                        } else if (editnumberofcertificate > 1000000 || editnumberofcertificate < 0) {
                                                            Toast.makeText(NewBidActivity.this, "Number of Rec should be greater than 0 and less than 1000000.", Toast.LENGTH_LONG).show();
                                                        } else {
                                                            ConnectionDetector cd = new ConnectionDetector(NewBidActivity.this);
                                                            if (!cd.isConnectingToInternet()) {
                                                                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                                            } else {
                                                                new HttpAsyncTaskNewBidEscertUpdate().execute(domain + "/mobile/pxs_app/service/escert/newbid/savebid.php");
                                                                dialog.dismiss();
                                                            }
                                                        }
                                                    }
                                                });

                                                dialogButtoncancelescert.setOnClickListener(new OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                                dialog.show();
                                            }
                                        });

                                alertDialogBuilder.setNeutralButton("Delete",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface arg0,
                                                    int arg1) {
                                                iddeleteescert = idadapterescert[+position];
                                                clientiddeleteescert = clientidadapterescert[+position];
                                                datedeleteescert = dateadapterescert[+position];
                                                typedeleteescert = typeadapterescert[+position];
                                                pricedeleteescert = priceadapterescert[+position];
                                                noofrecdeleteescert = noofrecadapterescert[+position];
                                                ordernaturedeleteescert = ordernatureadapterescert[+position];
                                                statusdeleteescert = statusadapterescert[+position];
                                                tstatusdeleteescert = tstatusadapterescert[+position];
                                                extradeleteescert = extraadapterescert[+position];
                                                entrybydeleteescert = entrybyadapterescert[+position];
                                                timestampdeleteescert = timestampadapterescert[+position];
                                                new HttpAsyncTaskNewBidEscertDelete().execute(domain + "/mobile/pxs_app/service/escert/newbid/deletebid.php");

                                            }
                                        });

                                alertDialogBuilder.setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                // finish();
                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder
                                        .create();
                                alertDialog.show();
                            }
                        });

                    }
                } catch (JSONException e) {
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class HttpAsyncTaskNewBidEscertSave extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AddBidSubmitEscerts addBidSubmitEscerts = new AddBidSubmitEscerts();
            addBidSubmitEscerts.setDate(escertplacedbiddatesend);
            addBidSubmitEscerts.setType(rgescerttypebuttongettypeSend);
            addBidSubmitEscerts.setPrice(popupclientpriceescerts);
            addBidSubmitEscerts.setNoofcertificates(popupclientnoofblockescerts);
            addBidSubmitEscerts.setOrdernature(rgescertbiddigntypebuttonSend);
            addBidSubmitEscerts.setAccess_key(dbacess);
            addBidSubmitEscerts.setId("null");
            return AddBidEscertSaveAPIResponse.POST(urls[0], addBidSubmitEscerts);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {
                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("message");
                        String value = jSONObject.getString("value");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                            pwindoescerts.dismiss();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (JSONException e) {
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskNewBidgetescertbiddetail().execute(domain + "/mobile/pxs_app/service/escert/newbid/getescertbiddetail.php");
        }
    }

    private class HttpAsyncTaskNewBidEscertUpdate extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AddBidSubmitEscerts addBidSubmitEscerts = new AddBidSubmitEscerts();
            addBidSubmitEscerts.setDate(dateeditescert);
            addBidSubmitEscerts.setType(typeeditescert);
            addBidSubmitEscerts.setPrice(updateclientpriceescert);
            addBidSubmitEscerts.setNoofcertificates(updateclientnoofcertificateescert);
            addBidSubmitEscerts.setOrdernature(ordernatureeditescert);
            addBidSubmitEscerts.setId(ideditescert);
            addBidSubmitEscerts.setAccess_key(dbacess);
            return AddBidEscertSaveAPIResponse.POST(urls[0], addBidSubmitEscerts);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {

                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("message");
                        String value = jSONObject.getString("value");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (JSONException e) {
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskNewBidgetescertbiddetail().execute(domain + "/mobile/pxs_app/service/escert/newbid/getescertbiddetail.php");
        }
    }

    private class HttpAsyncTaskNewBidEscertDelete extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AddBidRecDelate addBidRecDelate = new AddBidRecDelate();
            addBidRecDelate.setId(iddeleteescert);
            addBidRecDelate.setType(typedeleteescert);
            addBidRecDelate.setOrdernature(ordernaturedeleteescert);
            addBidRecDelate.setDate(datedeleteescert);
            addBidRecDelate.setAccess_key(dbacess);
            return AddBidRECDeleteAPIResponse.POST(urls[0], addBidRecDelate);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {

                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("message");
                        String value = jSONObject.getString("value");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (JSONException e) {
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskNewBidgetescertbiddetail().execute(domain + "/mobile/pxs_app/service/escert/newbid/getescertbiddetail.php");
        }
    }

    private class HttpAsyncTaskNewBidgetescertsubmitrecbid extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AddBidSubmitEscerts addBidSubmitEscerts = new AddBidSubmitEscerts();
            addBidSubmitEscerts.setType(rgescertstypebuttongettypeSend);
            addBidSubmitEscerts.setDate(escertplacedbiddatesend);
            addBidSubmitEscerts.setOrdernature(rgescertsbiddigntypebuttonSend);
            //addBidSubmitEscerts.setId(escertplacedbiddatesend);
            //addBidSubmitEscerts.setNoofcertificates();
            addBidSubmitEscerts.setAccess_key(dbacess);
            return AddBidEscertSaveAPIResponse.POST(urls[0], addBidSubmitEscerts);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {

                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {

                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("message");
                        String value = jSONObject.getString("value");

                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskNewBidgetescertbiddetail().execute(domain + "/mobile/pxs_app/service/escert/newbid/getescertbiddetail.php");
        }
    }

    private class HttpAsyncTaskNewSaveClientBidCancelALLSubmitDataEscert extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AddBidRecDelate addBidRecDelate = new AddBidRecDelate();
            addBidRecDelate.setId(cancelallidescert);
            addBidRecDelate.setType(rgescerttypebuttongettypeSend);
            addBidRecDelate.setOrdernature(rgescertbiddigntypebuttonSend);
            addBidRecDelate.setDate(escertplacedbiddatesend);
            addBidRecDelate.setAccess_key(dbacess);
            return AddBidRECDeleteAPIResponse.POST(urls[0], addBidRecDelate);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get REC Delete BID:", result);
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject jSONObject = new JSONObject(result);
                    if (jSONObject.length() == 0) {
                    } else {
                        String status = jSONObject.getString("status");
                        String msg = jSONObject.getString("message");
                        String value = jSONObject.getString("value");
                        if (status.equalsIgnoreCase("SUCCESS")) {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (JSONException e) {
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskNewBidgetescertbiddetail().execute(domain + "/mobile/pxs_app/service/escert/newbid/getescertbiddetail.php");
        }
    }
}
