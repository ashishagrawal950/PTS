package com.pts.model.realtimeschedule;

import java.util.ArrayList;

/**
 * Created by Vaibhav on 10-08-2016.
 */
public class RealTimeIntermed {

    private String name;
    private ArrayList<RealTimeInnerData> innerDatas=new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<RealTimeInnerData> getInnerDatas() {
        return innerDatas;
    }

    public void setInnerDatas(ArrayList<RealTimeInnerData> innerDatas) {
        this.innerDatas = innerDatas;
    }

    public void addInnerData(RealTimeInnerData data){
        innerDatas.add(data);
    }
}