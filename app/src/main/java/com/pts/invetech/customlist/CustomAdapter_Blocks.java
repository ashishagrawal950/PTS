package com.pts.invetech.customlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.model.blockqtm.BlockQtmApproval;

import java.util.List;

/**
 * Created by Ashish Karn on 02-08-2016.
 */
public class CustomAdapter_Blocks extends ArrayAdapter<BlockQtmApproval> {

    private List<BlockQtmApproval> cardList;
    private LayoutInflater inflater;
    private int jugaad=0;

    class CardViewHolder {
        TextView tvBlock1;
        TextView tvBlock2;

        TextView tvValue1;
        TextView tvValue2;
    }

    public CustomAdapter_Blocks(Context context,  List<BlockQtmApproval> list) {
        super(context, R.layout.realtimescheduling_list_item);
        inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        cardList=list;
        jugaad=cardList.size()/2;
    }

    @Override
    public void add(BlockQtmApproval object) {
        cardList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return cardList.size()/2;
    }

    @Override
    public BlockQtmApproval getItem(int index) {
        return cardList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CardViewHolder viewHolder;

        if (row == null) {
            row = inflater.inflate(R.layout.list_item_blocks, parent, false);
            viewHolder = new CardViewHolder();


            viewHolder.tvBlock1 = (TextView) row.findViewById(R.id.qtmblock1);
            viewHolder.tvBlock2 = (TextView) row.findViewById(R.id.qtmblock2);

            viewHolder.tvValue1 = (TextView) row.findViewById(R.id.qtmvalue1);
            viewHolder.tvValue2 = (TextView) row.findViewById(R.id.qtmvalue2);

            row.setTag(viewHolder);
        } else {
            viewHolder = (CardViewHolder)row.getTag();
        }

        BlockQtmApproval card1 = getItem(position);
        BlockQtmApproval card2 = getItem((position+jugaad));

        viewHolder.tvBlock1.setText(card1.getBlock());
        viewHolder.tvBlock2.setText(card2.getBlock());

        viewHolder.tvValue1.setText(card1.getQtm());
        viewHolder.tvValue2.setText(card2.getQtm());

        return row;
    }

    public void updateData(List<BlockQtmApproval> list){
        cardList=list;
        jugaad=cardList.size()/2;
        notifyDataSetChanged();
    }
}