package com.pts.invetech.module.rtclubbed_detail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.module.rtclubbed_detail.model.First;

import java.util.List;

/**
 * Created by vaibhav on 29/3/18.
 */

public class ClubBlockDetailAdapter extends BaseAdapter {

    private Context mContext;
    private List<First> fData;

    ClubBlockDetailAdapter(Context _context, List<First> _fData){
        mContext=_context;
        fData=_fData;
    }


    @Override
    public int getCount() {
        return fData.size();
    }

    @Override
    public First getItem(int position) {
        return fData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return fData.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ClubBlockViewHolder viewHolder;
        if(convertView==null){
            convertView=LayoutInflater.from(mContext).inflate(R.layout.row_rtclub_dtl,null);
            viewHolder=new ClubBlockViewHolder(convertView);

            convertView.setTag(viewHolder);
        }else{
            viewHolder= (ClubBlockViewHolder) convertView.getTag();
        }

        viewHolder.cb_dtl_timeslot.setText(getItem(position).getSlot());

        viewHolder.cb_dtl_prev.setText(getItem(position).getPrevRev());
        viewHolder.cb_dtl_cur.setText(getItem(position).getCurrentRev());

        return convertView;
    }


    private class ClubBlockViewHolder{

        TextView cb_dtl_timeslot,cb_dtl_prev,cb_dtl_cur;

        ClubBlockViewHolder(View view){
            cb_dtl_timeslot= (TextView) view.findViewById(R.id.cb_dtl_timeslot);
            cb_dtl_prev= (TextView) view.findViewById(R.id.cb_dtl_prev);
            cb_dtl_cur = (TextView) view.findViewById(R.id.cb_dtl_cur);
        }

    }
}
