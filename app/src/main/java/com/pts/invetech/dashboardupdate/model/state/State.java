
package com.pts.invetech.dashboardupdate.model.state;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class State implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("state_name")
    @Expose
    private String stateName;

    @SerializedName("state_code")
    @Expose
    private String state_code;
    @SerializedName("region")
    @Expose
    private String region;

    @SerializedName("iexregion")
    @Expose
    private String iexregion;


    public State(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }


    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getIexregion() {
        return iexregion;
    }

    public void setIexregion(String iexregion) {
        this.iexregion = iexregion;
    }

    protected State(Parcel in) {
        id = in.readString();
        stateName = in.readString();
        state_code = in.readString();
        region = in.readString();
        iexregion = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(stateName);
        dest.writeString(state_code);
        dest.writeString(region);
        dest.writeString(iexregion);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<State> CREATOR = new Parcelable.Creator<State>() {
        @Override
        public State createFromParcel(Parcel in) {
            return new State(in);
        }

        @Override
        public State[] newArray(int size) {
            return new State[size];
        }
    };

    public String toString() {

        return stateName;
    }

}
