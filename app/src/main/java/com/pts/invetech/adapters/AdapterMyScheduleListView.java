package com.pts.invetech.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.myscheduling.GetSchedulingData;

import java.util.List;

/**
 * Created by Ashish on 24-03-2018.
 */

public class AdapterMyScheduleListView extends BaseAdapter {

    private Context mContext;
    private List<GetSchedulingData> scheduleList;

    public AdapterMyScheduleListView(Context _context,
                                     List<GetSchedulingData> _scheduleList){

        scheduleList=_scheduleList;
        mContext=_context;
    }


    @Override
    public int getCount() {
        return scheduleList.size();
    }

    @Override
    public GetSchedulingData getItem(int i) {
        return scheduleList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return scheduleList.get(i).hashCode();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        AMSViewViewHolder viewHolder;
        if(view==null){
            view=LayoutInflater.from(mContext)
                    .inflate(R.layout.listitem_listview_myschedulelist,
                            null);

            viewHolder=new AMSViewViewHolder(view);
            view.setTag(viewHolder);
        }else{
            viewHolder= (AMSViewViewHolder) view.getTag();
        }

        GetSchedulingData tmp=getItem(i);


        int serno = i+1;
        viewHolder.tv1.setText(String.valueOf(serno));

        if(tmp.getRegion()!=null){
            viewHolder.tv2.setText(tmp.getRegion());
        }else{
            viewHolder.tv2.setText("NA");
        }


        if(tmp.getBuyer()!=null){
            viewHolder.tv4.setText(tmp.getBuyer());
        }else{
            viewHolder.tv4.setText("NA");
        }

        if(tmp.getSeller()!=null){
            viewHolder.tv5.setText(tmp.getSeller());
        }else{
            viewHolder.tv5.setText("NA");
        }

        if(tmp.getAppNo()!=null){
            viewHolder.tv6.setText(tmp.getAppNo());
        }else{
            viewHolder.tv6.setText("NA");
        }

        if(tmp.getQtm()!=null){
            viewHolder.tv7.setText(tmp.getQtm());
        }else{
            viewHolder.tv7.setText("NA");
        }

        if(tmp.getCurrentRev()!=null){
            viewHolder.tv3.setText(tmp.getCurrentRev());
        }else{
            viewHolder.tv3.setText("-");
        }

        if(tmp.getDifferenceValue().equalsIgnoreCase("View")){
            viewHolder.diff_val.setText("");
            viewHolder.diff_val.setAnimation(null);
        }else{
            viewHolder.diff_val.setText(tmp.getDifferenceValue());
            viewHolder.diff_val.setAnimation(AnimationUtils.loadAnimation(mContext,
                    R.anim.blink));
        }

        return view;
    }

    class AMSViewViewHolder{

        TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7,diff_val;
        AMSViewViewHolder(View view){
            tv1= (TextView) view.findViewById(R.id.txt1);
            tv2= (TextView) view.findViewById(R.id.txt2);
            tv3= (TextView) view.findViewById(R.id.txt3);
            tv4= (TextView) view.findViewById(R.id.txt4);
            tv5= (TextView) view.findViewById(R.id.txt5);
            tv6= (TextView) view.findViewById(R.id.txt6);
            tv7= (TextView) view.findViewById(R.id.txt7);
            diff_val= (TextView) view.findViewById(R.id.diff_val);
        }

    }
}


