package com.pts.model.statewise;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish on 24-07-2017.
 */

public class StateWiseRealTime {
    private String status;
    private String message;
    private ArrayList<StateRealTimeValue> realList;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<StateRealTimeValue> getRealList() {
        return realList;
    }

    public void setRealList(ArrayList<StateRealTimeValue> realList) {
        this.realList = realList;
    }





}
