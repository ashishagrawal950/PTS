package com.pts.invetech.dashboardupdate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.pts.invetech.R;
import com.pts.invetech.dashboardupdate.model.news.SubNews;

import java.util.List;

/**
 * Created by Ashish Karn on 13-10-2016.
 */

public class DashBoardNewsAdapter extends BaseAdapter {


    private List<SubNews> list;
    private Context mContext;

    public DashBoardNewsAdapter(Context _con, List<SubNews> _news) {
        mContext = _con;
        list = _news;
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public SubNews getItem(int i) {
        return list.get(i % list.size());
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater inf = LayoutInflater.from(mContext);
            view = inf.inflate(R.layout.new_listitem_newsfeed, null);

            ViewHolder holder = new ViewHolder();
            holder.header = (TextView) view.findViewById(R.id.nheader);
            holder.source = (TextView) view.findViewById(R.id.nsource);
            holder.type = (TextView) view.findViewById(R.id.ntype);
            holder.time = (TextView) view.findViewById(R.id.tvtime);
            holder.date = (TextView) view.findViewById(R.id.tvdate);
            view.setTag(holder);
        }

        SubNews data = getItem(i);
        ViewHolder holder= (ViewHolder) view.getTag();

        holder.header.setText(data.getTitle());
        holder.source.setText(data.getSource());
        holder.type.setText(data.getType());
        holder.time.setText(data.getTime());
        holder.date.setText(data.getPubdate());

        return view;
    }

    class ViewHolder {

        TextView header;
        TextView source;
        TextView type;
        TextView time;
        TextView date;
    }
}