package com.pts.invetech.classes.fragment;

/**
 * Created by vaibhav on 12/1/17.
 */

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.R;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.dashboardupdate.views.DashBoardUpdate;
import com.pts.invetech.pojo.FeedbackData;
import com.pts.invetech.utils.AppDeviceUtils;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.AppStringUtils;

import org.json.JSONException;
import org.json.JSONObject;


public class FeedBackFragment extends Fragment implements View.OnClickListener {

    private final String URL_FEEDBACK = "https://www.mittalpower.com/mobile/pxs_app/service/submitfeedback.php";
    private EditText mName, mMail, mNumber, mData;
    private LinearLayout openLayout;
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private String loginconfig, domain;
    private View rootView;
    private boolean isFirst = true;
    private NetworkCallBack callBack = new NetworkCallBack() {

        @Override
        public void onResultObject(Object data, int id) {
            if (data == null) {
                AppLogger.showToastLong(getActivity(), "Some Error occurred while sending mail.");
                return;
            }

            FeedbackData feedback = (FeedbackData) data;

            AppLogger.showToastLong(getActivity(), feedback.getMessage());
            mName.setText("");
            mMail.setText("");
            mNumber.setText("");
            mData.setText("");
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_feedback, container, false);
        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NewFeedMainActivity) getActivity()).open();
            }
        });

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        Intent in = getActivity().getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");

        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        lltoplogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        mName = (EditText) rootView.findViewById(R.id.feedback_name);
        mMail = (EditText) rootView.findViewById(R.id.feedback_mail);
        mNumber = (EditText) rootView.findViewById(R.id.feedback_number);
        mData = (EditText) rootView.findViewById(R.id.feedback_data);

        Button send = (Button) rootView.findViewById(R.id.feedback_send);
        send.setOnClickListener(this);
        if(!isFirst){
            domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        }
        return rootView;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.feedback_send) {
            verifyFeedback();
        }
    }

    private void verifyFeedback() {
        String name = mName.getText().toString();
        String email = mMail.getText().toString();
        String number = mNumber.getText().toString();
        String data = mData.getText().toString();

        if (AppStringUtils.isTextEmpty(name)) {
            AppLogger.showToastLong(getActivity(), getString(R.string.error_empty_name));
            return;
        }

        if (!AppStringUtils.isValidPhoneNumber(number)) {
            AppLogger.showToastLong(getActivity(), getString(R.string.error_empty_number));
            return;
        }

        if (!AppStringUtils.isEmailValid(email)) {
            AppLogger.showToastLong(getActivity(), getString(R.string.error_empty_mail));
            return;
        }

        if (AppStringUtils.isTextLess(data)) {
            AppLogger.showToastLong(getActivity(), getString(R.string.error_empty_data));
            return;
        }

        try {
            sendFeedback(name, email, number, data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendFeedback(String name, String email, String number, String data) throws JSONException {
        JSONObject object = new JSONObject();

        object.put("device_ip", AppDeviceUtils.getTelephonyId(getActivity()));
        object.put("name", name);
        object.put("mobile", number);
        object.put("email", email);
        object.put("feedback", data);
        if(isFirst){
            new NetworkHandlerModel(getActivity(), callBack, FeedbackData.class, 1).execute(URL_FEEDBACK, object.toString());
        }else{
            new NetworkHandlerModel(getActivity(), callBack, FeedbackData.class, 1).execute(domain + "/mobile/pxs_app/service/submitfeedback.php", object.toString());
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashBoardUpdate dashBoardUpdate = new DashBoardUpdate();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, dashBoardUpdate);
                        fragmentTransaction.commit();
                    }
                    return true;
                }
                return false;
            }
        });
    }
}