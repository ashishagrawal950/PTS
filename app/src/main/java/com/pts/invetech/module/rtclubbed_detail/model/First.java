package com.pts.invetech.module.rtclubbed_detail.model;

/**
 * Created by vaibhav on 29/3/18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class First {

    @SerializedName("slot")
    @Expose
    private String slot;
    @SerializedName("prevRev")
    @Expose
    private String prevRev;
    @SerializedName("currentRev")
    @Expose
    private String currentRev;
    @SerializedName("difference")
    @Expose
    private Double difference;

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public String getPrevRev() {
        return prevRev;
    }

    public void setPrevRev(String prevRev) {
        this.prevRev = prevRev;
    }

    public String getCurrentRev() {
        return currentRev;
    }

    public void setCurrentRev(String currentRev) {
        this.currentRev = currentRev;
    }

    public Double getDifference() {
        return difference;
    }

    public void setDifference(Double difference) {
        this.difference = difference;
    }

}
