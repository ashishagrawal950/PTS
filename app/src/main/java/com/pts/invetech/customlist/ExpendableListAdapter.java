package com.pts.invetech.customlist;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.invetech.NotificationOne;
import com.pts.invetech.R;
import com.pts.invetech.pojo.MonthData;
import com.pts.invetech.pojo.MonthlyAtcPojo;
import com.pts.invetech.utils.AppLogger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by Ashish Karn on 03-10-2016.
 */

public class ExpendableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<MonthlyAtcPojo> mData;
    private int siz = 5;
    String urlfileSend;
    String extation="", extetaionagain;
    private int numMessagesOne = 0;
    private NotificationManager myNotificationManager;
    private int notificationIdOne = 111;

    public ExpendableListAdapter(Context context, List<MonthlyAtcPojo> pdata) {
        mData = pdata;
        this.context = context;
    }

    @Override
    public MonthData getChild(int groupPosition, int childPosititon) {
        return mData.get(groupPosition).getMonthData().get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        MonthlyAtcPojo pjo = getGroup(groupPosition);
        final MonthData childMonth = getChild(groupPosition, childPosition);


        if (!pjo.isExpandable() && childPosition == 4) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.viewmore, null);
            convertView.setTag(groupPosition);
            convertView.setOnClickListener(listner);

            return convertView;
        } else {

            if (convertView == null || convertView instanceof TextView) {
                LayoutInflater infalInflater = (LayoutInflater) this.context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_item_monthly_atc, null);
            }

            TextView txtListChild = (TextView) convertView
                    .findViewById(R.id.lblListItem);

            txtListChild.setText(childMonth.getFileName());

            TextView txtListChild1 = (TextView) convertView
                    .findViewById(R.id.atcdate);
            txtListChild1.setText(childMonth.getTime());

            TextView txtListChild2 = (TextView) convertView
                    .findViewById(R.id.atctime);
            txtListChild2.setText(childMonth.getDate());

            ImageView dView= (ImageView) convertView.findViewById(R.id.iv_downloadfile);
            dView.setTag(childMonth.getFileAddress());
            dView.setOnClickListener(downLoadListner);



            return convertView;
        }

    }

    private View.OnClickListener downLoadListner=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            urlfileSend= (String) view.getTag();
           // AppLogger.showToastShort(context, urlfileSend);
            //urlfileSend = childMonth.getFileAddress();
            new DownloadFileAsync().execute(urlfileSend);

        }
    };

    @Override
    public int getChildrenCount(int groupPosition) {
        MonthlyAtcPojo pojo = mData.get(groupPosition);

        if (pojo.isExpandable()) {
            return mData.get(groupPosition).getMonthData().size();
        } else {
            return Math.min(mData.get(groupPosition).getMonthData().size(), siz);
        }
    }

    @Override
    public MonthlyAtcPojo getGroup(int groupPosition) {
        return mData.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return mData.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        MonthlyAtcPojo pojo = getGroup(groupPosition);

        String headerTitle = pojo.getMonth();

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_monthly_atc, null);
            (convertView.findViewById(R.id.arrdown)).setOnClickListener(parentListner);

        }

        ExpandableListView expListView = (ExpandableListView) parent;
        expListView.expandGroup(groupPosition);

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setTextColor(Color.BLACK);
        lblListHeader.setText(headerTitle);

        ImageView imgVu = (ImageView) convertView.findViewById(R.id.arrdown);
        imgVu.setTag(groupPosition);

        if (pojo.isExpandable()) {
            imgVu.setVisibility(View.VISIBLE);
        } else {
            imgVu.setVisibility(View.GONE);
        }


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void updateList(List<MonthlyAtcPojo> monthlyAtcList) {
        this.mData = monthlyAtcList;
        notifyDataSetChanged();
        notifyDataSetInvalidated();
    }

    private void setExpandable(int position, boolean value) {
        mData.get(position).setExpandable(value);

        notifyDataSetChanged();
    }

    private View.OnClickListener listner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = (int) v.getTag();
            setExpandable(id, true);
        }
    };

    private View.OnClickListener parentListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = (int) v.getTag();
            setExpandable(id, false);
        }
    };


    /**
     * Background Async Task to download file
     * */
    class DownloadFileAsync extends AsyncTask<String, String, String> {
        /**
         * Before starting background thread Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            String myUrl;
            final int BUFFER_SIZE = 4096;
            try {
                myUrl=f_url[0];
                String to = myUrl.replaceAll("(?<!http:|https:)//", "/");
                String finalstring =  to.replaceAll(" ","%20");
                //  String str="https://docs.google.com/viewer?url="+finalstring;
                URL url = new URL(finalstring);
                URLConnection conection = url.openConnection();
                conection.connect();
                extation = "";
                String disposition = conection.getHeaderField("Content-Disposition");
                String contentType = conection.getContentType();
                int contentLength = conection.getContentLength();
                if (disposition != null) {
                    int index = disposition.indexOf("filename=");
                    if (index > 0) {
                        extation = disposition.substring(index + 10,
                                disposition.length() - 1);
                    }
                } else {
                    extation = myUrl.substring(myUrl.lastIndexOf("/") + 1,
                            myUrl.length());
                }
                extetaionagain = extation.replaceAll("%","_");
                InputStream inputStream = conection.getInputStream();
                String saveFilePath = Environment
                        .getExternalStorageDirectory().toString()+"/"+extetaionagain;
                // opens an output stream to save into file
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);
                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
                outputStream.close();
                inputStream.close();
            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            Notification.Builder  mBuilder = new Notification.Builder(context);
            //String[] parts =  categoryurl.split("/");
            //String filename = parts[parts.length-1];
            mBuilder.setContentTitle("PTS");
            if(extetaionagain.endsWith(".pdf")){
                mBuilder.setContentText("Monthly ATC: "+extetaionagain + " file download.(File is in PDF format.)");
            }
            if(extetaionagain.endsWith(".PDF")){
                mBuilder.setContentText("Monthly ATC: "+extetaionagain + " file download.(File is in PDF format.)");
            }
            else if(extetaionagain.endsWith(".xlsx")){
                mBuilder.setContentText("Monthly ATC: "+extetaionagain + " file download.(File is in xlsx format.)");
            }
            else if(extetaionagain.endsWith(".xls")){
                mBuilder.setContentText("Monthly ATC: "+extetaionagain + " file download.(File is in xls format.)");
            }
            mBuilder.setTicker("Downloading");
            mBuilder.setSmallIcon(R.drawable.logopts);

            // Increase notification number every time a new notification arrives
            mBuilder.setNumber(++numMessagesOne);
            mBuilder.setAutoCancel(true);
            // Creates an explicit intent for an Activity in your app
            Intent myIntent=new Intent();
            myIntent.setAction(Intent.ACTION_VIEW);
            if(extetaionagain.endsWith(".pdf")){
                File file =new File(Environment.getExternalStorageDirectory().getPath()+"/"+extetaionagain);
                if(file.exists()){
                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                }else{

                }
            }
            if(extetaionagain.endsWith(".PDF")){
                File file =new File(Environment.getExternalStorageDirectory().getPath()+"/"+extetaionagain);
                if(file.exists()){
                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                }else{

                }
            }
            else if(extetaionagain.endsWith(".xlsx")){
                File file =new File(Environment.getExternalStorageDirectory().getPath()+"/"+extetaionagain);
                if(file.exists()){
                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                }else{

                }
            }
            else if(extetaionagain.endsWith(".xls")){
                File file =new File(Environment.getExternalStorageDirectory().getPath()+"/"+extetaionagain);
                if(file.exists()){
                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                }else{

                }
            }
            //This ensures that navigating backward from the Activity leads out of the app to Home page
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            // Adds the back stack for the Intent
            stackBuilder.addParentStack(NotificationOne.class);

            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(myIntent);
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(
                            0,
                            PendingIntent.FLAG_ONE_SHOT //can only be used once
                    );
            // start the activity when the user clicks the notification text
            mBuilder.setContentIntent(resultPendingIntent);
            Notification noti = mBuilder.build();
            myNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            noti.defaults |= Notification.DEFAULT_VIBRATE;
            noti.defaults |= Notification.DEFAULT_SOUND;
            noti.flags |= Notification.FLAG_SHOW_LIGHTS;
            noti.ledARGB = 0xff00ff00;
            noti.ledOnMS = 300;
            noti.ledOffMS = 1000;
            noti.flags |= Notification.FLAG_AUTO_CANCEL;
            // pass the Notification object to the system
            myNotificationManager.notify(notificationIdOne, mBuilder.build());
        }

    }
}