package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.pts.invetech.R;
import com.pts.invetech.customlist.LandedCostApplicableChargesCustomList;
import com.pts.invetech.customlist.LandedCostApplicablelossustomList;
import com.pts.invetech.customlist.LandedCostcalculationustomList;
import com.pts.invetech.customscrollview.widget.NonScrollListView;

import java.util.ArrayList;

public class ApplicableChargesActivity extends Activity {

    private ArrayList<String> applicable_charges_nameone_array;
    private ArrayList<String> applicable_charges_valueone_reasonarray;
    private ArrayList<String> applicable_losses_name_array;
    private ArrayList<String> applicable_losses_value_reasonarray;
    private ArrayList<String> calculation_nametwo_array;
    private ArrayList<String> calculation_valuetwo_reasonarray;
    private NonScrollListView lv_charges, lv_losses, lv_calculation;
    private LinearLayout openLayout;
    private LinearLayout llapplicablecharges, llapplicablelosses, llcalculation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_applicable_charges);

        openLayout = (LinearLayout) findViewById(R.id.openLayout);

        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent in = getIntent();
        applicable_charges_nameone_array = in.getStringArrayListExtra("applicable_charges_nameone_array");
        applicable_charges_valueone_reasonarray = in.getStringArrayListExtra("applicable_charges_valueone_reasonarray");
        applicable_losses_name_array = in.getStringArrayListExtra("applicable_losses_name_array");
        applicable_losses_value_reasonarray = in.getStringArrayListExtra("applicable_losses_value_reasonarray");
        calculation_nametwo_array = in.getStringArrayListExtra("calculation_nametwo_array");
        calculation_valuetwo_reasonarray = in.getStringArrayListExtra("calculation_valuetwo_reasonarray");


        llapplicablecharges = (LinearLayout) findViewById(R.id.llapplicablecharges);
        llapplicablelosses = (LinearLayout) findViewById(R.id.llapplicablelosses);
        llcalculation = (LinearLayout) findViewById(R.id.llcalculation);

        lv_charges = (NonScrollListView) findViewById(R.id.lv_charges);
        lv_losses = (NonScrollListView) findViewById(R.id.lv_losses);
        lv_calculation = (NonScrollListView) findViewById(R.id.lv_calculation);


        LandedCostApplicableChargesCustomList adaptercharges = new LandedCostApplicableChargesCustomList(ApplicableChargesActivity.this,
                applicable_charges_nameone_array, applicable_charges_valueone_reasonarray);
        lv_charges.setAdapter(adaptercharges);

        LandedCostApplicablelossustomList adapterlosses = new LandedCostApplicablelossustomList(ApplicableChargesActivity.this,
                applicable_losses_name_array, applicable_losses_value_reasonarray);
        lv_losses.setAdapter(adapterlosses);

        LandedCostcalculationustomList adaptercalculation = new LandedCostcalculationustomList(ApplicableChargesActivity.this,
                calculation_nametwo_array, calculation_valuetwo_reasonarray);
        lv_calculation.setAdapter(adaptercalculation);


        //llapplicablecharges,llapplicablelosses,llcalculation;

        llapplicablecharges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //overridePendingTransition(R.anim.bottom_in, R.anim.top_out);
            }
        });

        llapplicablelosses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        llcalculation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        /*AnimatedExpandableListView listView = (AnimatedExpandableListView) findViewById(R.id.listView);
        ExpandLandedCostAdapter adapter = new ExpandLandedCostAdapter(this, calculation_nametwo_array, calculation_valuetwo_reasonarray);
        listView.setAdapter(adapter);
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> cricket = new ArrayList<String>();
        cricket.add("India");
        cricket.add("Pakistan");
        cricket.add("Australia");
        cricket.add("England");
        cricket.add("South Africa");

        List<String> football = new ArrayList<String>();
        football.add("Brazil");
        football.add("Spain");
        football.add("Germany");
        football.add("Netherlands");
        football.add("Italy");

        List<String> basketball = new ArrayList<String>();
        basketball.add("United States");
        basketball.add("Spain");
        basketball.add("Argentina");
        basketball.add("France");
        basketball.add("Russia");

        expandableListDetail.put("CRICKET TEAMS", cricket);
        expandableListDetail.put("FOOTBALL TEAMS", football);
        expandableListDetail.put("BASKETBALL TEAMS", basketball);
*/
    }


}
