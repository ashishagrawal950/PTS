package com.pts.invetech.customlist;

import java.util.ArrayList;

import com.pts.invetech.R;
import com.pts.invetech.classes.activity.NewsLetterActivity;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NewsLetterCustomList extends ArrayAdapter<String> {

	private  Activity context;
	private  ArrayList<String> text1;
	private  ArrayList<String> text2;
	private  ArrayList<String> text3;
	private  ArrayList<String> text4;
	private static LayoutInflater inflater = null;
	private int lastPosition = -1;

	/*public NewsLetterCustomList(Activity context, String[] text1, String[] text2, String[] text3
			, String[] text4) {
		super(context, R.layout.newletter_list, text1);
		this.context = context;
		this.text1 = text1;
		this.text2 = text2;
		this.text3 = text3;
		this.text4 = text4;
	}*/

	
	
//	NoBidCustomList adapter = new NoBidCustomList(getActivity(), nobidid,nobidclientid, nobiddate);

	public NewsLetterCustomList(NewsLetterActivity context,
								ArrayList<String> text1, ArrayList<String> text2,
								ArrayList<String> text3, ArrayList<String> text4) {
		// TODO Auto-generated constructor stub
		super(context, R.layout.newletter_list, text1);
		this.context = context;
		this.text1 = text1;
		this.text2 = text2;
		this.text3 = text3;
		this.text4 = text4;
	}



	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.newletter_list, null, true);
		TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
		TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
		TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
		txt1.setText(text1.get(position));
		txt2.setText(text4.get(position));
		Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		rowView.startAnimation(animation);
		lastPosition = position;
		//txt3.setText("Delete");
		// imageView.setImageResource(imageId[position]);
		return rowView;
	}
}