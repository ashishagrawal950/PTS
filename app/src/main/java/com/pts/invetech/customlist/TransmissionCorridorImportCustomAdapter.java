package com.pts.invetech.customlist;

import java.util.ArrayList;

import com.pts.invetech.R;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class TransmissionCorridorImportCustomAdapter  extends ArrayAdapter<String> {
	private final Activity context;
	static ArrayList<String> onearrayList = new ArrayList<String>();
	static ArrayList<String> twoarrayList = new ArrayList<String>();
	static ArrayList<String> threearrayList = new ArrayList<String>();
	static ArrayList<String> fourarrayList = new ArrayList<String>();
	static ArrayList<String> fivearrayList = new ArrayList<String>();
	static ArrayList<String> sixarrayList = new ArrayList<String>();
	
	private int lastPosition = -1;
	
	public TransmissionCorridorImportCustomAdapter(Activity context, ArrayList<ArrayList<String>> arr) {
		super(context, R.layout.list_item_corridor);
		
		this.onearrayList.clear();
		this.twoarrayList.clear();
		this.threearrayList.clear();
		this.fourarrayList.clear();
        this.fivearrayList.clear();
        this.sixarrayList.clear();
        convertArray(arr);
		this.context = context;
	}
	
	private void convertArray(ArrayList<ArrayList<String>> arr) {
		
		for(int index=0;index<arr.size();index++){
			ArrayList<String> str=arr.get(index);
			
			onearrayList.add(str.get(0));
			twoarrayList.add(str.get(1));
			threearrayList.add(str.get(2));
			fourarrayList.add(str.get(3));
			fivearrayList.add(str.get(4));
			sixarrayList.add(str.get(5));
		}
	}

	@Override
	 public View getView(int position, View view, ViewGroup parent) {
		
	  LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	  View rowView = inflater.inflate(R.layout.list_item_corridor, null, true);
	  TextView txt1 = (TextView) rowView.findViewById(R.id.tv_one);
	  TextView txt2 = (TextView) rowView.findViewById(R.id.tv_two);
	  TextView txt3 = (TextView) rowView.findViewById(R.id.tv_three);
	  TextView txt4 = (TextView) rowView.findViewById(R.id.tv_four);
	  TextView txt5 = (TextView) rowView.findViewById(R.id.tv_five);
	  
	  txt1.setText(onearrayList.get(position));
	  txt2.setText(twoarrayList.get(position));
	  txt3.setText(threearrayList.get(position));
	  txt4.setText(fourarrayList.get(position));
	  
	  if(sixarrayList.get(position).equalsIgnoreCase("green")){
	  txt5.setText(fivearrayList.get(position));
	  txt5.setBackgroundColor(Color.parseColor("#00ac00"));
	  txt5.setTextColor(Color.WHITE);
      }
	  else if(sixarrayList.get(position).equalsIgnoreCase("red")){
	  txt5.setText(fivearrayList.get(position));
	  //txt5.setBackgroundColor(Color.RED);
	  txt5.setBackgroundColor(Color.parseColor("#ff0900"));
	  txt5.setTextColor(Color.WHITE);
	  }
	  else if(sixarrayList.get(position).equalsIgnoreCase("yellow")){
	  txt5.setText(fivearrayList.get(position));
	  txt5.setBackgroundColor(Color.parseColor("#bcb501"));
	  txt5.setTextColor(Color.WHITE);
      }else{
      txt5.setText(fivearrayList.get(position));  
      }
	 
	  /*Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
	  rowView.startAnimation(animation);
	  lastPosition = position;*/

	  return rowView;
	 }
	
	@Override
	public int getCount() {
		return onearrayList.size();
	}
	
	@Override
	public String getItem(int position) {
		return onearrayList.get(position);
	}

	
	}

