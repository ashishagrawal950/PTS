package com.pts.invetech.pojo;

public class NoBidGetAllDetails {
    public String j;
	public String id;
	public String clientid;
	public String date;
	public String dateshow;

	public NoBidGetAllDetails(String j, String id, String clientid, String date, String dateshow) {
		// TODO Auto-generated constructor stub
		this.j = j;
		this.id = id;
		this.clientid = clientid;
		this.date = date;
		this.dateshow = dateshow;
	}

	

	


	public String getJ() {
		return j;
	}






	public void setJ(String j) {
		this.j = j;
	}






	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientid() {
		return clientid;
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	

	public String getDateshow() {
		return dateshow;
	}






	public void setDateshow(String dateshow) {
		this.dateshow = dateshow;
	}






	@Override
	public String toString() {
		return "NoBidGetAllDetails[id=" + id + ", clientid=" + clientid
				+ ", date=" + date + "]";
	}

}
