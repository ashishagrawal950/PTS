package com.pts.invetech.pojo;

/**
 * Created by Ashish on 17-10-2017.
 */

public class AddBidSubmitEscerts {

    private String date;
    private String type;
    private String price;
    private String noofcertificates;
    private String ordernature;
    private String id;
    private String access_key;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNoofcertificates() {
        return noofcertificates;
    }

    public void setNoofcertificates(String noofcertificates) {
        this.noofcertificates = noofcertificates;
    }

    public String getOrdernature() {
        return ordernature;
    }

    public void setOrdernature(String ordernature) {
        this.ordernature = ordernature;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }
}
