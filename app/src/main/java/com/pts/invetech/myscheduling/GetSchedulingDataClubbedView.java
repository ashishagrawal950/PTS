package com.pts.invetech.myscheduling;

/**
 * Created by Ashish on 25-03-2018.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class GetSchedulingDataClubbedView {

    @SerializedName("clubbed")
    @Expose
    private List<Clubbed> clubbed = null;
    @SerializedName("unclubbed")
    @Expose
    private List<Unclubbed> unclubbed = null;

    public List<Clubbed> getClubbed() {
        return clubbed;
    }

    public void setClubbed(List<Clubbed> clubbed) {
        this.clubbed = clubbed;
    }

    public List<Unclubbed> getUnclubbed() {
        return unclubbed;
    }

    public void setUnclubbed(List<Unclubbed> unclubbed) {
        this.unclubbed = unclubbed;
    }
}
