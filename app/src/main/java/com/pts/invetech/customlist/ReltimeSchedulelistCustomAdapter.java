package com.pts.invetech.customlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

import java.util.ArrayList;

/**
 * Created by Ashish on 10-08-2017.
 */

public class ReltimeSchedulelistCustomAdapter extends ArrayAdapter<String> {
    private final Activity context;
    static ArrayList<String> key_element = new ArrayList<String>();


    public ReltimeSchedulelistCustomAdapter(Activity context, ArrayList<String> key_element) {
        super(context, R.layout.menu_custom_list, key_element);
        this.key_element = key_element;
        this.context = context;

    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View rowView = inflater.inflate(R.layout.menu_custom_list, null, true);
        TextView txt1 = (TextView) rowView.findViewById(R.id.textView1);

        txt1.setText(key_element.get(position));

        return rowView;
    }


}
