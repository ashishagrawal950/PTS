package com.pts.invetech.pojo;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class LastYearMarketPrice {
	private String  state_id;
	private String type;
	private String device_id;
	private ArrayList<String> datearray;

	public String getState_id() {
		return state_id;
	}
	public void setState_id(String state_id) {
		this.state_id = state_id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public ArrayList<String> getDatearray() {
		return datearray;
	}
	public JSONArray getTotalArray(){
		JSONArray array=new JSONArray();
		for(String str:datearray){
			array.put(str);
		}
		return  array;
	}
	public void setDatearray(ArrayList<String> datearray) {
		this.datearray = datearray;
	}
}
