package com.pts.invetech.classes.afterlogin;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.cardview.widget.CardView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.pts.api.ApiParamConvertor;
import com.pts.convertor.ClubbDataConvertor;
import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseFragment;
import com.pts.invetech.R;
import com.pts.invetech.adapters.AdapterMyscheduleClubList;
import com.pts.invetech.module.rtclubbed_detail.ClubBlockDetailFragment;
import com.pts.invetech.myscheduling.GetSchedulingDataClubbedView;
import com.pts.invetech.utils.AppDeviceUtils;
import com.pts.model.myscheduling.ClubViewData;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Ashish Karn on 20-03-2018.
 */

public class MyScheduleClubviewFragmentAfterLogin extends AppBaseFragment implements AdapterView.OnItemSelectedListener {

    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private String deviceid;
    private Dialog prgDialog;
    private String[] viewtypespn = {"BOTH", "TERMS", "REGIONS",};
    private String[] tersmspn = {"STOA", "MTOA", "LTA", "INTRA", "ALL"};
    private String[] regionspn = {"ER", "WR", "NR", "SR", "NER", "ALL"};
    private ListView listView;
    private SQLiteDatabase db;
    private String dbacess, domain;
    private TextView btn_setup;
    private CardView crd_setUpAccount;
    private Button btn_filter, btn_cancel;
    private Spinner spn_viewtype, spn_regions, spn_terms;
    private String viewtypeSend = "BOTH", regionSend = "", termsSend = "";
    private LinearLayout llterms, llregions;
    /*
     * Api Calling
     */
    private NetworkCallBack networkCallBack = new NetworkCallBack() {

        @Override
        public void onResultObject(Object data, int id) {
            if (data == null) {
                return;
            }
            switch (id) {
                case 1:
                    GetSchedulingDataClubbedView scheduleList = (GetSchedulingDataClubbedView) data;
                    ArrayList<ClubViewData> listData = ClubbDataConvertor.getTotalData(scheduleList);

                    AdapterMyscheduleClubList adapter = new AdapterMyscheduleClubList(getActivity(),
                            listData);
                    listView.setAdapter(adapter);
                    break;
            }
        }
    };
    /*
     * List View Item Selected
     */
    private AdapterView.OnItemClickListener itemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            AdapterMyscheduleClubList dd = ((AdapterMyscheduleClubList) parent.getAdapter());
            ClubViewData clubData = dd.getItem(position);

            openDetailPage(clubData);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = getActivity().openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM baby", null);
        if (c.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("access_key: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
//        domain = getArguments().getString("domain");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_myscheduleclubview, container, false);
        LinearLayout openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivityAfterLogin) getActivity()).open();
            }
        });

        btn_setup = (TextView) rootView.findViewById(R.id.btn_setup);
        crd_setUpAccount = (CardView) rootView.findViewById(R.id.crd_setUpAccount);
        btn_filter = (Button) rootView.findViewById(R.id.btn_filter);
        btn_cancel = (Button) rootView.findViewById(R.id.btn_cancel);
        spn_viewtype = (Spinner) rootView.findViewById(R.id.spn_viewtype);
        spn_regions = (Spinner) rootView.findViewById(R.id.spn_regions);
        spn_terms = (Spinner) rootView.findViewById(R.id.spn_terms);
        spn_viewtype.setOnItemSelectedListener(this);
        spn_regions.setOnItemSelectedListener(this);
        spn_terms.setOnItemSelectedListener(this);
        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        lltoplogin.setVisibility(View.GONE);
        lltoplogout.setVisibility(View.VISIBLE);
        tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
        tvlogouttext.setText("Home");
        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(in);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
            }
        });

        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);

        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        prgDialog.setCancelable(false);

        deviceid = AppDeviceUtils.getTelephonyId(getActivity());
        btn_setup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_setup.setVisibility(View.GONE);
                crd_setUpAccount.setVisibility(View.VISIBLE);
            }
        });

        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_setup.setVisibility(View.VISIBLE);
                crd_setUpAccount.setVisibility(View.GONE);
                try {
                    apiGetSaveSchedule("wazmrogqbjqhbkeddvelefdttxpeesabtznkoubqdrxfcbigwnrasvtlsjqxcqyv",
                            viewtypeSend, regionSend, termsSend);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_setup.setVisibility(View.VISIBLE);
                crd_setUpAccount.setVisibility(View.GONE);
            }
        });

        /*LIST ITEMS AND ADAPTER*/
        listView = (ListView) rootView.findViewById(R.id.lv_myshedulingClub);
        listView.setOnItemClickListener(itemClick);

        llterms = (LinearLayout) rootView.findViewById(R.id.llterms);
        llregions = (LinearLayout) rootView.findViewById(R.id.llregions);

        selectViewTypeSpinner();
        selectRegionSpinner();
        selectTermsSpinner();

        try {
            apiGetSaveSchedule("wazmrogqbjqhbkeddvelefdttxpeesabtznkoubqdrxfcbigwnrasvtlsjqxcqyv",
                    "BOTH", "", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rootView;
    }

    private void selectViewTypeSpinner() {
        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item,
                viewtypespn);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_viewtype.setAdapter(aa);
    }

    private void selectRegionSpinner() {
        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item,
                regionspn);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_regions.setAdapter(aa);
    }

    private void selectTermsSpinner() {
        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item,
                tersmspn);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_terms.setAdapter(aa);
    }

    private void apiGetSaveSchedule(String access_key, String viewtype,
                                    String region, String terms) throws JSONException {
        String URL_SAVEFILTER = domain + "/mobile/pxs_app/service/schedule_track/myscheduleclubbedview/getschedulingdata.php";

//        String param= ApiParamConvertor.getSchedulingData("wazmrogqbjqhbkeddvelefdttxpeesabtznkoubqdrxfcbigwnrasvtlsjqxcqyv",
        String param = ApiParamConvertor.getSchedulingData(access_key, viewtype, region, terms);

        new NetworkHandlerModel(getActivity(), networkCallBack,
                GetSchedulingDataClubbedView.class, 1).execute(URL_SAVEFILTER, param);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner viewtypesp = (Spinner) parent;
        Spinner regionsp = (Spinner) parent;
        Spinner termssp = (Spinner) parent;


        if (viewtypesp.getId() == R.id.spn_viewtype) {
            viewtypeSend = viewtypespn[position];
            regionSend = "";
            termsSend = "";
            if (viewtypeSend.equalsIgnoreCase("BOTH")) {
                llregions.setVisibility(View.GONE);
                llterms.setVisibility(View.GONE);
            } else if (viewtypeSend.equalsIgnoreCase("TERMS")) {
                llregions.setVisibility(View.GONE);
                llterms.setVisibility(View.VISIBLE);
            } else if (viewtypeSend.equalsIgnoreCase("REGIONS")) {
                llregions.setVisibility(View.VISIBLE);
                llterms.setVisibility(View.GONE);
            }
        }
        if (regionsp.getId() == R.id.spn_regions) {
            if (viewtypeSend.equalsIgnoreCase("BOTH")) {
                regionSend = "";
            } else {
                regionSend = regionspn[position];
            }
        }
        if (termssp.getId() == R.id.spn_terms) {
            if (viewtypeSend.equalsIgnoreCase("BOTH")) {
                termsSend = "";
            } else {
                termsSend = tersmspn[position];
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP
                        && keyCode == KeyEvent.KEYCODE_BACK) {
                    Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                    return true;
                }
                return false;
            }
        });
    }

    private void openDetailPage(ClubViewData clubData) {

        Bundle bundle = new Bundle();
        bundle.putString("id", clubData.getId());
        bundle.putString("prevId", clubData.getPrevId());

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ClubBlockDetailFragment clubBlockDetail = new ClubBlockDetailFragment();
        clubBlockDetail.setArguments(bundle);

        ft.replace(R.id.content_frame, clubBlockDetail);
        ft.commit();
    }

    /*
     *
     */


}