package com.pts.model;

/**
 * Created by Ashish on 26-07-2017.
 */

public class YearandMonthvalue {
    private String data;
    private String val;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
