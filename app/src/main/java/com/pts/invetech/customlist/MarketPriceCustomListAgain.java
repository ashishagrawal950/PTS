package com.pts.invetech.customlist;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

public class MarketPriceCustomListAgain extends ArrayAdapter<String> {

	private  Activity context2;
	private  ArrayList<String> iexArr1;
	private  ArrayList<String> iexArr2;
	private  ArrayList<String> iexArr3;
	private  ArrayList<String> iexArr4;
	private  ArrayList<String> pxilArr2;
	private  ArrayList<String> pxilArr3;
	private  ArrayList<String> pxilArr4;

	public MarketPriceCustomListAgain(Activity context2,
			ArrayList<String> iexArr1, ArrayList<String> iexArr2,
			ArrayList<String> iexArr3, ArrayList<String> iexArr4,
			ArrayList<String> pxilArr2, ArrayList<String> pxilArr3,
			ArrayList<String> pxilArr4) {
				super(context2, R.layout.marketprice_single_bid, iexArr1);
				this.context2 = context2;
				this.iexArr1 = iexArr1;
				this.iexArr2 = iexArr2;
				this.iexArr3 = iexArr3;
				this.iexArr4 = iexArr4;
				this.pxilArr2 = pxilArr2;
				this.pxilArr3 = pxilArr3;
				this.pxilArr4 = pxilArr4;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context2.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.marketprice_single_bid, null, true);
		TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
		TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
		TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
		TextView txt4 = (TextView) rowView.findViewById(R.id.txt4);
		TextView txt5 = (TextView) rowView.findViewById(R.id.txt5);
		TextView txt6 = (TextView) rowView.findViewById(R.id.txt6);
		TextView txt7 = (TextView) rowView.findViewById(R.id.txt7);
		txt1.setText(iexArr1.get(position));
		txt2.setText(String.valueOf(iexArr2.get(position)));
		txt3.setText(String.valueOf(iexArr3.get(position)));
		txt4.setText(String.valueOf(iexArr4.get(position)));
		txt5.setText(String.valueOf(pxilArr2.get(position)));
		txt6.setText(String.valueOf(pxilArr3.get(position)));
		txt7.setText(String.valueOf(pxilArr4.get(position)));
		return rowView;
	}
}
