package com.pts.invetech.classes.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseActivity;
import com.pts.invetech.R;
import com.pts.invetech.customlist.CustomAdapter_Blocks;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Constant;
import com.pts.model.blockqtm.BlockQtmApproval;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RealtimeSchedulingDetailBlockActivity extends AppBaseActivity {

    private final String URL_CURRENT = "https://www.mittalpower.com/mobile/pxs_app/service/scheduling/getallblockqtmofapproval.php";
    private CustomAdapter_Blocks customAdapter_blocks;
    private ArrayList<BlockQtmApproval> qtmApproval;
    private SQLiteDatabase db;
    private String device_id, domain;
    private LinearLayout openLayout;
    private boolean isFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_block);
        db = openOrCreateDatabase("deviceDBSecond",
                Context.MODE_PRIVATE, null);
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
        if(!isFirst){
            domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
        }
        openLayout = (LinearLayout) findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        try {
            init();

        } catch (Exception e) {
            AppLogger.showMsgWithoutTag(e.getMessage());
            e.printStackTrace();
        }
    }

    private void parseArray(String data) throws Exception {
        qtmApproval = new ArrayList<>();

        JSONArray array = new JSONArray(data);

        for (int index = 0; index < array.length(); index++) {
            JSONObject obj = array.getJSONObject(index);

            BlockQtmApproval temp = new BlockQtmApproval();
            temp.setBlock(obj.getString("block"));
            temp.setQtm(obj.getString("qtm"));

            qtmApproval.add(temp);
        }
    }

    private void init() throws JSONException {
        String dataid = null;
        String maxRev = "";
        String approv = "";

        Intent intent = getIntent();

        if (intent != null) {
            dataid = intent.getStringExtra(Constant.VALUE_DATAID);
            approv = intent.getStringExtra(Constant.DATA_APPROVAL);
            maxRev = intent.getStringExtra(Constant.DATA_REV);

            JSONObject object = new JSONObject();
            object.put(Constant.VALUE_DATAID, dataid);
            object.put(Constant.VALUE_DEVICEID, device_id);
            if(!isFirst)
                new NetworkManagerActivity(this, object, domain + "/mobile/pxs_app/service/scheduling/getallblockqtmofapproval.php").execute();
            else
                new NetworkManagerActivity(this, object, URL_CURRENT).execute();
        } else {
//            AppLogger.showToastShort(this, "Some Error Occurred");
        }

        setAnimation(approv, maxRev);
    }

    public void createList() {
        ListView listView = (ListView) findViewById(R.id.lv_block);
        listView.addHeaderView(new View(this));
        listView.addFooterView(new View(this));

        customAdapter_blocks = new CustomAdapter_Blocks(getApplicationContext(), qtmApproval);

        listView.setAdapter(customAdapter_blocks);
    }

    public void setAnimation(String approv, String maxRev) {

        TextView tv_mainstrip = (TextView) findViewById(R.id.tv_mainstrip);

        String str = "RealTime Scheduling of Approval No- " + approv + " (R" + maxRev + ")";
        tv_mainstrip.setText(str);

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.realtime_scheduling_slide_left_to_right);
        tv_mainstrip.startAnimation(animation);
    }


    @Override
    public void updateStringResult(String data) {
        if (data == null || data.isEmpty()) {
//            AppLogger.showToastShort(this, "Some Error Occurred");
            return;
        }

        try {
            parseArray(data);
            createList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}