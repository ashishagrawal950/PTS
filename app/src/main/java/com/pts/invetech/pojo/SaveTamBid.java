package com.pts.invetech.pojo;

/**
 * Created by Ashish on 06-01-2017.
 */
public class SaveTamBid {

    public String id;
    public String type;
    public String producttype;
    public String ordernature;
    public String date;
    public String fromtime;
    public String totime;
    public String bidquantum;
    public String minprice;
    public String maxprice;
    public String access_key;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProducttype() {
        return producttype;
    }

    public void setProducttype(String producttype) {
        this.producttype = producttype;
    }

    public String getOrdernature() {
        return ordernature;
    }

    public void setOrdernature(String ordernature) {
        this.ordernature = ordernature;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromtime() {
        return fromtime;
    }

    public void setFromtime(String fromtime) {
        this.fromtime = fromtime;
    }

    public String getTotime() {
        return totime;
    }

    public void setTotime(String totime) {
        this.totime = totime;
    }

    public String getBidquantum() {
        return bidquantum;
    }

    public void setBidquantum(String bidquantum) {
        this.bidquantum = bidquantum;
    }

    public String getMinprice() {
        return minprice;
    }

    public void setMinprice(String minprice) {
        this.minprice = minprice;
    }

    public String getMaxprice() {
        return maxprice;
    }

    public void setMaxprice(String maxprice) {
        this.maxprice = maxprice;
    }

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }
}
