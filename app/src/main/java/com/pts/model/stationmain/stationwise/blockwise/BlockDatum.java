
package com.pts.model.stationmain.stationwise.blockwise;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BlockDatum {

    @SerializedName("fromtime")
    @Expose
    private String fromtime;
    @SerializedName("totime")
    @Expose
    private String totime;
    @SerializedName("available")
    @Expose
    private Double available;
    @SerializedName("disable")
    @Expose
    private String disable;

    public String getFromtime() {
        return fromtime;
    }

    public void setFromtime(String fromtime) {
        this.fromtime = fromtime;
    }

    public String getTotime() {
        return totime;
    }

    public void setTotime(String totime) {
        this.totime = totime;
    }

    public Double getAvailable() {
        return available;
    }

    public void setAvailable(Double available) {
        this.available = available;
    }

    public String getDisable() {
        return disable;
    }

    public void setDisable(String disable) {
        this.disable = disable;
    }

}
