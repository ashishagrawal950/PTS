package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pts.invetech.R;

import java.util.ArrayList;
import java.util.List;

public class NewsFeedActivityWebView extends Activity {

    private Context context;
    private LinearLayout mopenLayout;
    private String client_id;
    private SQLiteDatabase db;
    private String link_key_element;
    private String loginconfig;
    private Dialog prgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_newsfeedwebview);
        db = openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        Cursor clientid = db.rawQuery("SELECT * FROM clientid", null);
        if (clientid.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        StringBuffer clientidbuffer = new StringBuffer();
        while (clientid.moveToNext()) {
            clientidbuffer.append("clientid: " + clientid.getString(0) + "\n");
            client_id = clientid.getString(0);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
        }

        Intent in = getIntent();
        link_key_element = in.getStringExtra("link_key_element");
        loginconfig = in.getStringExtra("LOGINCONFIG");


        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(NewsFeedActivityWebView.this, NewFeedMainActivity.class);
                in.putExtra("LOGINCONFIG", loginconfig);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });
        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });
        ImageView ivshare = (ImageView) findViewById(R.id.ivshare);

        if (link_key_element.length() == 0) {
            Toast.makeText(NewsFeedActivityWebView.this, "No Data Found", Toast.LENGTH_LONG).show();
            ivshare.setVisibility(View.INVISIBLE);
        } else {
            ivshare.setVisibility(View.VISIBLE);
            WebView myWebView = (WebView) findViewById(R.id.webView);
            myWebView.setClickable(true);
            myWebView.setFocusableInTouchMode(true);
            myWebView.getSettings().setJavaScriptEnabled(true);
            myWebView.loadUrl(link_key_element);
            WebClientClass webViewClient = new WebClientClass();
            myWebView.setWebViewClient(webViewClient);
            WebChromeClient webChromeClient = new WebChromeClient();
            myWebView.setWebChromeClient(webChromeClient);

        }
        ivshare.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Resources resources = getResources();

                Intent emailIntent = new Intent();
                emailIntent.setAction(Intent.ACTION_SEND);
                // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
                emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_native)));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
                emailIntent.setType("message/rfc822");

                PackageManager pm = getPackageManager();
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");

                Intent openInChooser = Intent.createChooser(emailIntent, resources.getString(R.string.share_chooser_text));
                List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
                List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
                for (int i = 0; i < resInfo.size(); i++) {
                    // Extract the label, append it, and repackage it in a LabeledIntent
                    ResolveInfo ri = resInfo.get(i);
                    String packageName = ri.activityInfo.packageName;
                    if (packageName.contains("android.email")) {
                        emailIntent.setPackage(packageName);
                    } else if (packageName.contains("whatsapp") || packageName.contains("gm")) {
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        if (packageName.contains("whatsapp")) {
                            intent.putExtra(Intent.EXTRA_TEXT, link_key_element);
                            //	startActivity(Intent.createChooser(intent, ""));

                        } else if (packageName.contains("android.gm")) {
                            intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_gmail)));
                            intent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
                            intent.setType("message/rfc822");
                            intent.putExtra(Intent.EXTRA_SUBJECT, "News: PTS");
                            intent.putExtra(Intent.EXTRA_TEXT, link_key_element);
                        }
                        intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
                    }
                }

                // convert intentList to array
                LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);

                openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
                startActivity(openInChooser);
            }

        });
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(NewsFeedActivityWebView.this, NewFeedMainActivity.class);
        in.putExtra("LOGINCONFIG", loginconfig);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    public class WebClientClass extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
		   /*pd = new ProgressDialog(NewsFeedActivityWebView.this);
		   pd.setTitle("Please wait");
		   pd.setMessage("Loading..");
		   pd.show();*/
            prgDialog = new Dialog(NewsFeedActivityWebView.this);
            prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            prgDialog.setContentView(R.layout.progessdialog);
            ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
            image.startAnimation(animation);
            prgDialog.show();
			  /*prgDialog.setCancelable(false);
			  new CountDownTimer(10000, 1000) {
				  public void onTick(long millisUntilFinished) {
					  // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
				  }
				  public void onFinish() {
					  prgDialog.dismiss();
				  }
			  }.start();*/

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            prgDialog.dismiss();
        }
    }


}
