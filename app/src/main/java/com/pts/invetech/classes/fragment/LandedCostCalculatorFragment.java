package com.pts.invetech.classes.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.LandedcostAPIResponse;
import com.pts.invetech.apiresponse.LandedcostgetstatediscoAPIResponse;
import com.pts.invetech.classes.activity.ApplicableChargesActivity;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.dashboardupdate.views.DashBoardUpdate;
import com.pts.invetech.pojo.Landedcost;
import com.pts.invetech.utils.Constant;
import com.pts.invetech.utils.JSONUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;

public class LandedCostCalculatorFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    static ArrayList<String> state_idarray = new ArrayList<String>();
    static ArrayList<String> discom_idarray = new ArrayList<String>();
    static ArrayList<String> codearray = new ArrayList<String>();
    static ArrayList<String> voltage_array = new ArrayList<String>();
    static ArrayList<String> applicable_losses_name_array = new ArrayList<String>();
    static ArrayList<String> applicable_losses_value_reasonarray = new ArrayList<String>();
    static ArrayList<String> applicable_charges_nameone_array = new ArrayList<String>();
    static ArrayList<String> applicable_charges_valueone_reasonarray = new ArrayList<String>();
    static ArrayList<String> calculation_nametwo_array = new ArrayList<String>();
    static ArrayList<String> calculation_valuetwo_reasonarray = new ArrayList<String>();
    private LinearLayout openLayout;
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private String loginconfig;
    private RadioGroup lltype;
    private RadioButton rgypebutton;
    private String client_type_Send = "", quantum_Send = "", no_of_hours_Send = "", price_Send = "";
    private View rootView;
    private Dialog prgDialog;
    private String state_id, discom_id, code;
    private ArrayAdapter<String> adapter_state;
    private Spinner spin_state, spin_voltage;
    private String discom_id_send = "", voltage_send = "";
    private JSONArray mainarray;
    private ArrayAdapter<String> adapter_voltage;
    private EditText edtquantity, edttotalhourstraded, edtiexprice;
    private String deviceid;
    private TextView tvlanded_cost;
    private LinearLayout landed_cost_layout;
    private Animation RightSwipe;
    private String landed_cost, name, value, nameone, valueone, nametwo, valuetwo;
    private SQLiteDatabase db;
    private String dbacess, domain;
    private boolean isFirst = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_landedcostcalculator, container,
                false);
        db = getActivity().openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        Cursor c = db.rawQuery("SELECT * FROM baby", null);
        if (c.getCount() == 0) {
            // showMessage("Error", "No records found");
            dbacess = "";
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("access_key: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
        }
        if(!isFirst){
            domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        }
        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NewFeedMainActivity) getActivity()).open();
            }
        });

		/*prgDialog = new ProgressDialog(getActivity());
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        TelephonyManager tManagerr = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        deviceid = tManagerr.getDeviceId();

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        Intent in = getActivity().getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");

        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        lltoplogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),
                        MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        // Spinner element
        spin_state = (Spinner) rootView.findViewById(R.id.spin_state);
        spin_voltage = (Spinner) rootView.findViewById(R.id.spin_voltage);

        // Spinner click listener
        spin_state.setOnItemSelectedListener(this);
        spin_voltage.setOnItemSelectedListener(this);

        landed_cost_layout = (LinearLayout) rootView.findViewById(R.id.landed_cost_layout);
        //Animation
        RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_right_to_left);
        final Animation blink = AnimationUtils.loadAnimation(getActivity(), R.anim.landedcost_blink);


        ImageView img = (ImageView) rootView.findViewById(R.id.img_blink_arrow);
        img.setAnimation(blink);

        lltype = (RadioGroup) rootView.findViewById(R.id.lltype);

        edtquantity = (EditText) rootView.findViewById(R.id.edtquantity);
        edttotalhourstraded = (EditText) rootView.findViewById(R.id.edttotalhourstraded);
        edtiexprice = (EditText) rootView.findViewById(R.id.edtiexprice);

        tvlanded_cost = (TextView) rootView.findViewById(R.id.tvlanded_cost);

        //Calculate button
        Button btn_calculate = (Button) rootView.findViewById(R.id.btn_calculate);
        btn_calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedlltypeId = lltype.getCheckedRadioButtonId();
                rgypebutton = (RadioButton) rootView.findViewById(selectedlltypeId);
                if (rgypebutton != null) {
                    client_type_Send = (String) rgypebutton.getText();
                }
                //discom_id_send = spin_state.getSelectedItem().toString();
                voltage_send = spin_voltage.getSelectedItem().toString();
                quantum_Send = edtquantity.getText().toString();
                no_of_hours_Send = edttotalhourstraded.getText().toString();
                price_Send = edtiexprice.getText().toString();

                if (voltage_send.equalsIgnoreCase("Voltage")) {
                    Toast.makeText(getActivity(), "There are not found Voltage in the Selected State, Please select another state.", Toast.LENGTH_LONG).show();
                } else {
				/*	if ((client_type_Send.length() != 0) && (discom_id_send.length() != 0) &&
							(voltage_send.length() != 0) && (quantum_Send.length() != 0) &&
							(no_of_hours_Send.length() != 0) && (price_Send.length() != 0)) {*/
                    ConnectionDetector cd = new ConnectionDetector(getActivity());
                    if (client_type_Send.length() == 0) {
                        Toast.makeText(getActivity(), "Please Select Type of Entity.", Toast.LENGTH_LONG).show();
                    } else if (discom_id_send.length() == 0) {
                        Toast.makeText(getActivity(), "Please Select State.", Toast.LENGTH_LONG).show();
                    } else if (voltage_send.length() == 0) {
                        Toast.makeText(getActivity(), "Please Voltage.", Toast.LENGTH_LONG).show();
                    } else if (quantum_Send.length() == 0) {
                        Toast.makeText(getActivity(), "Please fill all the details.", Toast.LENGTH_LONG).show();
                    } else if (no_of_hours_Send.length() == 0) {
                        Toast.makeText(getActivity(), "Please fill all the details.", Toast.LENGTH_LONG).show();
                    } else if (price_Send.length() == 0) {
                        Toast.makeText(getActivity(), "Please fill all the details.", Toast.LENGTH_LONG).show();
                    } else if (!cd.isConnectingToInternet()) {
                        Toast.makeText(getActivity(), "Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        if(isFirst){
                            new HttpAsyncTasklandedcost_calc().execute("https://" + Constant.BASE_URL + "/mobile/pxs_app/service/landed_cost/landedcost_calc.php");
                        }else{
                            new HttpAsyncTasklandedcost_calc().execute(domain + "/mobile/pxs_app/service/landed_cost/landedcost_calc.php");
                        }

                    }
                }
            }
        });

        Button btnclear = (Button) rootView.findViewById(R.id.btnclear);
        btnclear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lltype.clearCheck();
                edtquantity.setText("");
                edttotalhourstraded.setText("");
                edtiexprice.setText("");
                landed_cost_layout.setVisibility(View.INVISIBLE);
                client_type_Send = "";
                quantum_Send = "";
                no_of_hours_Send = "";
                price_Send = "";
            }
        });


        landed_cost_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), ApplicableChargesActivity.class);
                i.putStringArrayListExtra("applicable_charges_nameone_array", applicable_charges_nameone_array);
                i.putStringArrayListExtra("applicable_charges_valueone_reasonarray", applicable_charges_valueone_reasonarray);
                i.putStringArrayListExtra("applicable_losses_name_array", applicable_losses_name_array);
                i.putStringArrayListExtra("applicable_losses_value_reasonarray", applicable_losses_value_reasonarray);
                i.putStringArrayListExtra("calculation_nametwo_array", calculation_nametwo_array);
                i.putStringArrayListExtra("calculation_valuetwo_reasonarray", calculation_valuetwo_reasonarray);
                startActivity(i);
				/*lltype.clearCheck();
				edtquantity.setText("");
				edttotalhourstraded.setText("");
				edtiexprice.setText("");
				landed_cost_layout.setVisibility(View.INVISIBLE);
				client_type_Send = "";
				quantum_Send = "";
				no_of_hours_Send = "";
				price_Send = "";*/
            }
        });
        //landedcost_calc(result);

        ConnectionDetector cd = new ConnectionDetector(getActivity());
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getActivity(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            //new HttpAsyncTaskNewBidGetBidDetail().execute("https://www.mittalpower.com/mobile/pxs_app/service/newbid/getbiddetail.php");
            if(isFirst){
                new HttpAsyncTaskgetstatediscom().execute("https://" + Constant.BASE_URL + "/mobile/pxs_app/service/landed_cost/getstatediscom.php");
            }
            else{
                new HttpAsyncTaskgetstatediscom().execute(domain + "/mobile/pxs_app/service/landed_cost/getstatediscom.php");
            }

        }


        return rootView;
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {

        Spinner statetypeid = (Spinner) parent;
        Spinner bidtypeid = (Spinner) parent;

        if (statetypeid.getId() == R.id.spin_state) {
            //Toast.makeText(getActivity(), discom_idarray.get(position), Toast.LENGTH_LONG).show();
            discom_id_send = discom_idarray.get(position);
            try {
                //mainarray = new JSONArray(result);
                for (int i = 0; i < mainarray.length(); i++) {
                    JSONObject mainarraydetail = mainarray.getJSONObject(i);
                    if (mainarraydetail.has("state_id")) {
                        state_id = mainarraydetail.getString("state_id");
                    }
                    if (mainarraydetail.has("discom_id")) {
                        discom_id = mainarraydetail.getString("discom_id");
                    }
                    if (mainarraydetail.has("code")) {
                        code = mainarraydetail.getString("code");
                    }

                    if (discom_idarray.get(position).equalsIgnoreCase(discom_id)) {
                        voltage_array.clear();
                        JSONArray voltage = mainarraydetail.getJSONArray("voltage");
                        if (voltage.length() == 0) {
                            //Toast.makeText(getActivity(),"No Data",Toast.LENGTH_LONG).show();
                            voltage_array.add("Voltage");
                        } else {
                            for (int j = 0; j < voltage.length(); j++) {
                                String voltagedata = voltage.getString(j);
                                voltage_array.add(voltagedata);
                            }
                        }
                    }

                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            adapter_voltage = new ArrayAdapter<String>(
                    getActivity(),
                    R.layout.spinner_item_voltage,
                    voltage_array);
            adapter_voltage
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spin_voltage.setAdapter(adapter_voltage);
        }
        if (bidtypeid.getId() == R.id.spin_voltage) {
            //voltage_send = voltage_array.get(position);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

        // TODO Auto-generated method stub

    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashBoardUpdate dashBoardUpdate = new DashBoardUpdate();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, dashBoardUpdate);
                        fragmentTransaction.commit();
                    }
                    return true;
                }
                return false;
            }
        });
    }


    ///// Landed Cost
    //{"device_id":"867935024394966","client_type":" BUYER","discom_id":9,"voltage_level":11,"quantum":4,"no_of_hours":24,"price":3}

    private class HttpAsyncTaskgetstatediscom extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Landedcost landedcost = new Landedcost();
            landedcost.setDevice_id(deviceid);
            return LandedcostgetstatediscoAPIResponse.POST(urls[0], landedcost);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //Toast.makeText(getActivity(), "Landed Cost" +result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    state_idarray.clear();
                    discom_idarray.clear();
                    codearray.clear();
                    voltage_array.clear();
                    mainarray = new JSONArray(result);
                    for (int i = 0; i < mainarray.length(); i++) {
                        JSONObject mainarraydetail = mainarray.getJSONObject(i);
                        if (mainarraydetail.has("state_id")) {
                            state_id = mainarraydetail.getString("state_id");
                        }
                        if (mainarraydetail.has("discom_id")) {
                            discom_id = mainarraydetail.getString("discom_id");
                        }
                        if (mainarraydetail.has("code")) {
                            code = mainarraydetail.getString("code");
                        }

                        JSONArray voltage = mainarraydetail.getJSONArray("voltage");
                        if (voltage.length() == 0) {
                            //Toast.makeText(getActivity(),"No Data",Toast.LENGTH_LONG).show();
                        } else {
                            for (int j = 0; j < voltage.length(); j++) {
                                String voltagedata = voltage.getString(j);
                                voltage_array.add(voltagedata);
                            }
                        }
                        state_idarray.add(state_id);
                        discom_idarray.add(discom_id);
                        codearray.add(code);
                    }
                    adapter_state = new ArrayAdapter<String>(
                            getActivity(),
                            R.layout.spinner_item_voltage,
                            codearray);
                    adapter_state
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spin_state.setAdapter(adapter_state);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }


        }
    }

    private class HttpAsyncTasklandedcost_calc extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            if (client_type_Send.equalsIgnoreCase("Buy")) {
                client_type_Send = "BUYER";
            } else if (client_type_Send.equalsIgnoreCase("Sell")) {
                client_type_Send = "SELLER";
            }
            Landedcost landedcost = new Landedcost();
            landedcost.setDevice_id(deviceid);
            landedcost.setClient_type(client_type_Send);
            landedcost.setDiscom_id(discom_id_send);
            landedcost.setVoltage_level(voltage_send);
            landedcost.setQuantum(quantum_Send);
            landedcost.setNo_of_hours(no_of_hours_Send);
            landedcost.setPrice(price_Send);
            landedcost.setDbacess(dbacess);
            return LandedcostAPIResponse.POST(urls[0], landedcost);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //Toast.makeText(getActivity(), "Landed Cost" +result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    applicable_losses_name_array.clear();
                    applicable_losses_value_reasonarray.clear();
                    applicable_charges_nameone_array.clear();
                    applicable_charges_valueone_reasonarray.clear();
                    calculation_nametwo_array.clear();
                    calculation_valuetwo_reasonarray.clear();
                    //JSONArray jsonarray = new JSONArray(result);
                    JSONObject mainobject = new JSONObject(result);
                    if (mainobject.has("landed_cost")) {
                        landed_cost = mainobject.getString("landed_cost");
                    }
                    JSONArray applicable_losses = mainobject.getJSONArray("applicable_losses");
                    for (int i = 0; i < applicable_losses.length(); i++) {
                        JSONObject applicable_lossesdata = applicable_losses.getJSONObject(i);
                        if (applicable_lossesdata.has("name")) {
                            name = applicable_lossesdata.getString("name");
                        }
                        if (applicable_lossesdata.has("value")) {
                            value = applicable_lossesdata.getString("value");
                        }
                        applicable_losses_name_array.add(name);
                        applicable_losses_value_reasonarray.add(value);

                    }
                    JSONArray applicable_charges = mainobject.getJSONArray("applicable_charges");
                    for (int i = 0; i < applicable_charges.length(); i++) {
                        JSONObject applicable_chargesdata = applicable_charges.getJSONObject(i);
                        if (applicable_chargesdata.has("name")) {
                            nameone = applicable_chargesdata.getString("name");
                        }
                        if (applicable_chargesdata.has("value")) {
                            valueone = applicable_chargesdata.getString("value");
                        }
                        applicable_charges_nameone_array.add(nameone);
                        applicable_charges_valueone_reasonarray.add(valueone);
                    }
                    JSONArray calculation = mainobject.getJSONArray("calculation");
                    for (int i = 0; i < calculation.length(); i++) {
                        JSONObject calculationdata = calculation.getJSONObject(i);
                        if (calculationdata.has("name")) {
                            nametwo = calculationdata.getString("name");
                        }
                        if (calculationdata.has("value")) {
                            valuetwo = calculationdata.getString("value");
                        }
                        calculation_nametwo_array.add(nametwo);
                        calculation_valuetwo_reasonarray.add(valuetwo);
                    }
                    tvlanded_cost.setText(landed_cost);
                    landed_cost_layout.setVisibility(View.VISIBLE);
                    landed_cost_layout.startAnimation(RightSwipe);

                } catch (JSONException e) {
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }
}