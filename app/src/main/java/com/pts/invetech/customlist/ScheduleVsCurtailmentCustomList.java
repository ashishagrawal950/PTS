package com.pts.invetech.customlist;
import java.util.ArrayList;
import java.util.Random;

import com.pts.invetech.R;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class ScheduleVsCurtailmentCustomList extends ArrayAdapter<String> {
	private final Activity context;
	static ArrayList<String> blockarrayList = new ArrayList<String>();
	static ArrayList<String> actualarrayList = new ArrayList<String>();
	static ArrayList<String> curtailmentarrayList = new ArrayList<String>();
	static ArrayList<String> revisedarrayList = new ArrayList<String>();
	private int lastPosition = -1;
	
	
	public ScheduleVsCurtailmentCustomList(Activity context, ArrayList<String> blockarrayList,
			ArrayList<String> actualarrayList
			   , ArrayList<String> curtailmentarrayList
			   , ArrayList<String> revisedarrayList) {
		super(context, R.layout.list_schedulevscurtailment, blockarrayList);
		this.blockarrayList = blockarrayList;
		this.actualarrayList = actualarrayList;
		this.curtailmentarrayList = curtailmentarrayList;
		this.revisedarrayList = revisedarrayList;
		this.context = context;
		
	}
	
	@Override
	 public View getView(int position, View view, ViewGroup parent) {
	  LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	  View rowView = inflater.inflate(R.layout.list_schedulevscurtailment, null, true);
	  TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
	  TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
	  TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
	  TextView txt4 = (TextView) rowView.findViewById(R.id.txt4);
	  
	  txt1.setText(blockarrayList.get(position));
	  txt2.setText(actualarrayList.get(position));
	  if(curtailmentarrayList.get(position).equalsIgnoreCase("0.0")){
	       txt3.setText("-");
	       txt3.setBackgroundResource(R.drawable.rounded_corner_iex);
	  }else{
		  txt3.setText(curtailmentarrayList.get(position));
		  txt3.setBackgroundResource(R.drawable.rounded_corner_max);
	  }
	  txt4.setText(revisedarrayList.get(position));
	  
	  Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
	  rowView.startAnimation(animation);
	  lastPosition = position;

	//  txtTitle.setImageResource(RechargeAmmout[position]);
	  return rowView;
	 }

	
	}

