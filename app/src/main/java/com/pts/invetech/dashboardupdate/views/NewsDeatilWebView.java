package com.pts.invetech.dashboardupdate.views;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.pts.invetech.R;
import com.pts.invetech.utils.AppLogger;

public class NewsDeatilWebView extends AppCompatActivity {

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailwebview);

        Intent intent = getIntent();

        if (intent != null && intent.hasExtra("weburl")) {
            String url = intent.getStringExtra("weburl");
            setWebView(url);
        } else {
            AppLogger.showToastLong(this, "No Url Found.");
        }
    }

    private void setWebView(String url) {
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        WebView myWebView = (WebView) findViewById(R.id.webview);
        myWebView.setWebViewClient(new AppWebClient());
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.loadUrl(url);
    }


    public class AppWebClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            progressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
//            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

            progressBar.setVisibility(View.GONE);
        }
    }
}
