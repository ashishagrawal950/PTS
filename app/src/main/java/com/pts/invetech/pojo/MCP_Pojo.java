package com.pts.invetech.pojo;

/**
 * Created by Mustajeeb on 4/18/2016.
 */
public class MCP_Pojo {
    private String date;
    private String text;


    public MCP_Pojo(String line1, String line2) {
        this.date = line1;
        this.text = line2;

    }

    public String getLine1() {return date;}

    public String getLine2() {return text;}


}