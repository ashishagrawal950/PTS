package com.pts.invetech.customlist;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.myscheduling.CurrentFlowModel;
import com.pts.model.realtimedetail.Datum;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish Karn on 02-03-2017.
 */

public class AdapterMyScheduling extends BaseAdapter {

    private LayoutInflater inflater;
    private List<CurrentFlowModel> flowList;
    private String data_id_forcolor;

    public AdapterMyScheduling(Context _context, List<CurrentFlowModel> _list, String data_id){
        inflater=LayoutInflater.from(_context);
        flowList=_list;
        data_id_forcolor = data_id;
    }

    @Override
    public int getCount() {
        return flowList.size();
    }

    @Override
    public CurrentFlowModel getItem(int i) {
        return flowList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public List<CurrentFlowModel> getData(){
        return flowList;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if(view==null){
            view=inflater.inflate(R.layout.listitem_myscheduling, null);
        }

        LinearLayout llmyscheduling = (LinearLayout) view.findViewById(R.id.llmyscheduling);
        TextView sellerNo = (TextView) view.findViewById(R.id.tv_seller);
        TextView appNo = (TextView) view.findViewById(R.id.tv_appno);
        TextView importType = (TextView) view.findViewById(R.id.tv_importtype);
        TextView buyer = (TextView) view.findViewById(R.id.tv_buyer);
        TextView counterNo = (TextView) view.findViewById(R.id.tv_number);
        TextView tv_qtm = (TextView) view.findViewById(R.id.tv_qtm);

        CurrentFlowModel currentFlow=getItem(position);

        if(currentFlow.getId().equalsIgnoreCase(data_id_forcolor)){

            counterNo.setText("" + (position + 1));
            sellerNo.setText(currentFlow.getSeller());
            appNo.setText(currentFlow.getAppNo());
            importType.setText(currentFlow.getImporttype());
            buyer.setText(currentFlow.getBuyer());
            tv_qtm.setText(currentFlow.getQtm());
            llmyscheduling.setBackgroundColor(Color.GREEN);
            /*counterNo.setBackgroundColor(Color.GREEN);
            sellerNo.setBackgroundColor(Color.GREEN);
            appNo.setBackgroundColor(Color.GREEN);
            importType.setBackgroundColor(Color.GREEN);
            buyer.setBackgroundColor(Color.GREEN);
            counterNo.setBackgroundColor(Color.GREEN);*/
        }
        else {
            counterNo.setText("" + (position + 1));
            sellerNo.setText(currentFlow.getSeller());
            appNo.setText(currentFlow.getAppNo());
            importType.setText(currentFlow.getImporttype());
            buyer.setText(currentFlow.getBuyer());
            tv_qtm.setText(currentFlow.getQtm());
        }

        return view;
    }
}


