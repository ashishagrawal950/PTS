package com.pts.invetech.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang.builder.ToStringBuilder;

public class NotificationData {

    @SerializedName("MCP")
    @Expose
    private String mCP;
    @SerializedName("NEWSLETTER")
    @Expose
    private String nEWSLETTER;
    @SerializedName("menuhome")
    @Expose
    private String menuhome;
    @SerializedName("menumarketprice")
    @Expose
    private String menumarketprice;
    @SerializedName("menudownload")
    @Expose
    private String menudownload;
    @SerializedName("menunewbid")
    @Expose
    private String menunewbid;
    @SerializedName("menureport")
    @Expose
    private String menureport;
    @SerializedName("menunobid")
    @Expose
    private String menunobid;
    @SerializedName("recplacenewbid")
    @Expose
    private String recplacenewbid;
    @SerializedName("recnobid")
    @Expose
    private String recnobid;
    @SerializedName("precurtailment")
    @Expose
    private String precurtailment;
    @SerializedName("NLDCTOIEX")
    @Expose
    private String nLDCTOIEX;
    @SerializedName("NOTAFFECTED")
    @Expose
    private String nOTAFFECTED;
    @SerializedName("REALTIME")
    @Expose
    private String rEALTIME;
    @SerializedName("RESTORE")
    @Expose
    private String rESTORE;
    @SerializedName("newsfeedactivity")
    @Expose
    private String newsfeedactivity;

    /**
     *
     * @return
     * The mCP
     */
    public String getMCP() {
        return mCP;
    }

    /**
     *
     * @param mCP
     * The MCP
     */
    public void setMCP(String mCP) {
        this.mCP = mCP;
    }

    /**
     *
     * @return
     * The nEWSLETTER
     */
    public String getNEWSLETTER() {
        return nEWSLETTER;
    }

    /**
     *
     * @param nEWSLETTER
     * The NEWSLETTER
     */
    public void setNEWSLETTER(String nEWSLETTER) {
        this.nEWSLETTER = nEWSLETTER;
    }

    /**
     *
     * @return
     * The menuhome
     */
    public String getMenuhome() {
        return menuhome;
    }

    /**
     *
     * @param menuhome
     * The menuhome
     */
    public void setMenuhome(String menuhome) {
        this.menuhome = menuhome;
    }

    /**
     *
     * @return
     * The menumarketprice
     */
    public String getMenumarketprice() {
        return menumarketprice;
    }

    /**
     *
     * @param menumarketprice
     * The menumarketprice
     */
    public void setMenumarketprice(String menumarketprice) {
        this.menumarketprice = menumarketprice;
    }

    /**
     *
     * @return
     * The menudownload
     */
    public String getMenudownload() {
        return menudownload;
    }

    /**
     *
     * @param menudownload
     * The menudownload
     */
    public void setMenudownload(String menudownload) {
        this.menudownload = menudownload;
    }

    /**
     *
     * @return
     * The menunewbid
     */
    public String getMenunewbid() {
        return menunewbid;
    }

    /**
     *
     * @param menunewbid
     * The menunewbid
     */
    public void setMenunewbid(String menunewbid) {
        this.menunewbid = menunewbid;
    }

    /**
     *
     * @return
     * The menureport
     */
    public String getMenureport() {
        return menureport;
    }

    /**
     *
     * @param menureport
     * The menureport
     */
    public void setMenureport(String menureport) {
        this.menureport = menureport;
    }

    /**
     *
     * @return
     * The menunobid
     */
    public String getMenunobid() {
        return menunobid;
    }

    /**
     *
     * @param menunobid
     * The menunobid
     */
    public void setMenunobid(String menunobid) {
        this.menunobid = menunobid;
    }

    /**
     *
     * @return
     * The recplacenewbid
     */
    public String getRecplacenewbid() {
        return recplacenewbid;
    }

    /**
     *
     * @param recplacenewbid
     * The recplacenewbid
     */
    public void setRecplacenewbid(String recplacenewbid) {
        this.recplacenewbid = recplacenewbid;
    }

    /**
     *
     * @return
     * The recnobid
     */
    public String getRecnobid() {
        return recnobid;
    }

    /**
     *
     * @param recnobid
     * The recnobid
     */
    public void setRecnobid(String recnobid) {
        this.recnobid = recnobid;
    }

    /**
     *
     * @return
     * The precurtailment
     */
    public String getPrecurtailment() {
        return precurtailment;
    }

    /**
     *
     * @param precurtailment
     * The precurtailment
     */
    public void setPrecurtailment(String precurtailment) {
        this.precurtailment = precurtailment;
    }

    /**
     *
     * @return
     * The nLDCTOIEX
     */
    public String getNLDCTOIEX() {
        return nLDCTOIEX;
    }

    /**
     *
     * @param nLDCTOIEX
     * The NLDCTOIEX
     */
    public void setNLDCTOIEX(String nLDCTOIEX) {
        this.nLDCTOIEX = nLDCTOIEX;
    }

    /**
     *
     * @return
     * The nOTAFFECTED
     */
    public String getNOTAFFECTED() {
        return nOTAFFECTED;
    }

    /**
     *
     * @param nOTAFFECTED
     * The NOTAFFECTED
     */
    public void setNOTAFFECTED(String nOTAFFECTED) {
        this.nOTAFFECTED = nOTAFFECTED;
    }

    /**
     *
     * @return
     * The rEALTIME
     */
    public String getREALTIME() {
        return rEALTIME;
    }

    /**
     *
     * @param rEALTIME
     * The REALTIME
     */
    public void setREALTIME(String rEALTIME) {
        this.rEALTIME = rEALTIME;
    }

    /**
     *
     * @return
     * The rESTORE
     */
    public String getRESTORE() {
        return rESTORE;
    }

    /**
     *
     * @param rESTORE
     * The RESTORE
     */
    public void setRESTORE(String rESTORE) {
        this.rESTORE = rESTORE;
    }

    /**
     *
     * @return
     * The newsfeedactivity
     */
    public String getNewsfeedactivity() {
        return newsfeedactivity;
    }

    /**
     *
     * @param newsfeedactivity
     * The newsfeedactivity
     */
    public void setNewsfeedactivity(String newsfeedactivity) {
        this.newsfeedactivity = newsfeedactivity;
    }

    @Override
    public String toString(){
        return ToStringBuilder.reflectionToString(this);
    }

}