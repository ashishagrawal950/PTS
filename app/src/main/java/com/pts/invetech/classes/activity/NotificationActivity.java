package com.pts.invetech.classes.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingRegistrar;
import com.pts.badge.ShortcutBadger;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.AlertDialogManager;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.GCMIntentService;
import com.pts.invetech.R;
import com.pts.invetech.ServerUtilities;
import com.pts.invetech.apiresponse.NotificationAPIResponse;
import com.pts.invetech.apiresponse.NotificationLoadMoreAPIResponse;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.customlist.NotificationCustomList;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.pojo.Notification;
import com.pts.invetech.utils.JSONUtils;
import com.pts.invetech.utils.WakeLocker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.pts.invetech.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.pts.invetech.CommonUtilities.EXTRA_MESSAGE;
import static com.pts.invetech.CommonUtilities.SENDER_ID;

import androidx.annotation.NonNull;

@SuppressLint("SimpleDateFormat")
public class NotificationActivity extends Activity {
    public static String name;
    public static String email;
    public static String deviceid;
    private Context context;
    private LinearLayout openPopupNotification;
    private Point p;
    private SQLiteDatabase db;
    //	TextView goMessage;
    private NonScrollListView notificationlist;
    private AsyncTask<Void, Void, Void> mRegisterTask;
    // Alert dialog manager
    private AlertDialogManager alert = new AlertDialogManager();
    // Connection detector
    private ConnectionDetector cd;
    private TextView lblMessage, tvtradingdate, tvnotificationnull, tvnotificationnulltext;
    private String newMessage;
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(NotificationActivity.this);

            /**
             * Take appropriate action on this message
             * depending upon your app requirement
             * For now i am just displaying it on the screen
             * */

            // Showing received message
            //lblMessage.append("Ashish Agrawal:" + newMessage + "\n");
            if (newMessage.equalsIgnoreCase("Your device registred with GCM") || newMessage.equalsIgnoreCase("Trying (attempt 1/5) to register device on Demo Server.") ||
                    newMessage.equalsIgnoreCase("From Demo Server: successfully added device!") || newMessage.equalsIgnoreCase("null")) {

            } else {
                //db.execSQL("INSERT INTO pushnotification VALUES('"+newMessage+"', '" + datedb + "', '" + timedb + "')");
            }
            //Toast.makeText(NotificationActivity.this, "New Message: " + newMessage, Toast.LENGTH_LONG).show();
            //callNotification();
            // Releasing wake lock
            WakeLocker.release();
        }
    };
    private String icontypefrom, dbacess, displaydate, displaytime;
    private LinearLayout mopenLayout;
    private HomeActivity homeActivity;
    private String datedb, timedb, as = "";
    private Button btgoNotification;
    private Dialog prgDialog;
    private String dbacessone;
    private LinearLayout tvloadmore;
    private ArrayList<String> id_array = new ArrayList<String>();
    private ArrayList<String> date_array = new ArrayList<String>();
    private ArrayList<String> message_array = new ArrayList<String>();
    private ArrayList<String> module_type_array = new ArrayList<String>();
    private ArrayList<String> status_array = new ArrayList<String>();
    private ArrayList<String> datestamp_array = new ArrayList<String>();
    private ArrayList<String> timestamp_array = new ArrayList<String>();
    private ArrayList<Integer> icon_array = new ArrayList<Integer>();
    private String id, message, domain,regId;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_notication);

	 /*TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
	 deviceid = tManager.getDeviceId();*/
        //deviceid = Secure.getString(NotificationActivity.this.getContentResolver(),Secure.ANDROID_ID);


        clearSharedPref(this);
        ShortcutBadger.applyCount(NotificationActivity.this, 0);
	/*prgDialog = new ProgressDialog(NotificationActivity.this);
    prgDialog.setMessage("Please wait...");
    prgDialog.setCancelable(false)*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);

        Cursor cu = db.rawQuery("SELECT * FROM baby", null);
        if (cu.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (cu.moveToNext()) {
            buffer.append("access_key: " + cu.getString(0) + "\n");
            dbacessone = cu.getString(0);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
            //buffer.append("access_key: "+c.getString(0)+"\n");
            //Toast.makeText(getApplicationContext(), "access_key" + c.getColumnIndex("access_key"), Toast.LENGTH_LONG).show();
        }

        Cursor cde = db.rawQuery("SELECT * FROM device", null);
        if (cde.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        StringBuffer bufferd = new StringBuffer();
        while (cde.moveToNext()) {
            bufferd.append("uuid: " + cde.getString(0) + "\n");
            deviceid = cde.getString(0);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        String[] splitdateandtime = formattedDate.split(" ");
        datedb = splitdateandtime[0]; // 004
        timedb = splitdateandtime[1]; // 004


        cd = new ConnectionDetector(NotificationActivity.this);
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            //alert.showAlertDialog(NotificationActivity.this,"Internet Connection Error","Please connect to working Internet connection", false);
            // stop executing code by return
        }


        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(NotificationActivity.this);
        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
        GCMRegistrar.checkManifest(NotificationActivity.this);
        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        tvtradingdate = (TextView) findViewById(R.id.tvtradingdate);
        notificationlist = (NonScrollListView) findViewById(R.id.notificationlist);

        tvnotificationnull = (TextView) findViewById(R.id.tvnotificationnull);
        tvnotificationnulltext = (TextView) findViewById(R.id.tvnotificationnulltext);

        lblMessage = (TextView) findViewById(R.id.lblMessage);
        tvloadmore = (LinearLayout) findViewById(R.id.tvloadmore);


        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));

        // Get GCM registration id
        final String regId = GCMRegistrar.getRegistrationId(NotificationActivity.this);

//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(NotificationActivity.this, new OnSuccessListener<InstanceIdResult>(){
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                 regId = instanceIdResult.getToken();
//                Log.e("newToken", regId);
//
//            }
//        });
        // Check if regid already presents
        if (regId.equals("")) {
            // Registration is not present, register now with GCM
            GCMRegistrar.register(NotificationActivity.this, SENDER_ID);


        } else {
            // Device is already registered on GCM
            if (GCMRegistrar.isRegisteredOnServer(NotificationActivity.this)) {
                // Skips registration.
                //	Toast.makeText(getActivity(), "Already registered with GCM", Toast.LENGTH_LONG).show();
            } else {
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                final Context context = NotificationActivity.this;
                mRegisterTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        // Register on our server
                        // On server creates a new user
                        ServerUtilities.register(context, deviceid, regId);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }

                };
                mRegisterTask.execute(null, null, null);
            }
        }
        cd = new ConnectionDetector(NotificationActivity.this);
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            //alert.showAlertDialog(SplashScreenActivity.this,"Internet Connection Error","Please connect to working Internet connection", false);
            // stop executing code by return
            Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            new HttpAsyncTaskgetnotification().execute(domain + "/mobile/pxs_app/service/notification/getnotification.php");
        }

        //callNotification();

        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(NotificationActivity.this, MainActivityAfterLogin.class);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        tvtradingdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        tvloadmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                cd = new ConnectionDetector(NotificationActivity.this);
                // Check if Internet present
                if (!cd.isConnectingToInternet()) {
                    // Internet Connection is not present
                    //alert.showAlertDialog(SplashScreenActivity.this,"Internet Connection Error","Please connect to working Internet connection", false);
                    // stop executing code by return
                    Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    new HttpAsyncTaskgetnotificationloadmore().execute(domain + "/mobile/pxs_app/service/notification/getnotification.php");
                }
            }
        });
		/*btgoNotification = (Button) findViewById(R.id.btgoNotification);
		btgoNotification.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(tvtradingdate.length() == 0){
					Toast.makeText(NotificationActivity.this, "Please Select Date.", Toast.LENGTH_LONG).show();
				}
				else{
					Cursor c=db.rawQuery("SELECT * FROM pushnotification WHERE notificationdate like '"+as+"'", null);
					if(c.getCount()==0)
					{
						//tvnotificationnull, tvnotificationnulltext
						tvnotificationnull.setBackgroundResource(R.drawable.sad);
						tvnotificationnulltext.setText("No Notification found.");
						tvnotificationnull.setVisibility(View.VISIBLE);
						tvnotificationnulltext.setVisibility(View.VISIBLE);
						notificationlist.setVisibility(View.GONE);
						//Toast.makeText(NotificationActivity.this, "No Notification found", Toast.LENGTH_LONG).show();
					}
					else{
					tvnotificationnull.setVisibility(View.GONE);
					tvnotificationnulltext.setVisibility(View.GONE);
					notificationlist.setVisibility(View.VISIBLE);
					StringBuffer buffer=new StringBuffer();
					icontypearraysearch = new String[c.getCount()];
					arraysearch = new String[c.getCount()];
					arraydispalydatesearch = new String[c.getCount()];
					arraydispalytimesearch = new String[c.getCount()];
					elementiconarraysearch = new Integer[c.getCount()];
					int i = 0;
					while(c.moveToNext())
					{
						buffer.append("dbacess: "+c.getString(0)+"\n");
						//dbacesssearch = c.getString(0);
						String dbacessfromsearch = c.getString(0);

						String[] parts = dbacessfromsearch.split("/");
						icontypesearch = parts[0];
						dbacesssearch = parts[1];
						displaydate = c.getString(1);
						displaytime = c.getString(2);

						displaydatesearch = c.getString(1);
						displaytimesearch = c.getString(2);

						icontypearraysearch[i] = icontypesearch;
						arraysearch[i] = dbacesssearch;
						arraydispalydatesearch[i] = displaydatesearch;
						arraydispalytimesearch[i] = displaytimesearch;
					    i++;
						Log.d("dbacesssearch: ", dbacesssearch);
						Log.d("displaydateandtimesearch: ", displaydatesearch);
						Log.d("displaydateandtimesearch: ", displaytimesearch);
						//Toast.makeText(getActivity(), dbacess , Toast.LENGTH_LONG).show();
					}

					List<String> icontypearraysearchlist = Arrays.asList(icontypearraysearch);
					 Collections.reverse(icontypearraysearchlist);
					 icontypearraysearch = (String[]) icontypearraysearchlist.toArray();


					for(int j=0; j < icontypearraysearch.length; j++){
						 String elementicon = icontypearraysearch[j];
						 if(elementicon.equalsIgnoreCase("MCP"))
						 {
							 elementiconarraysearch[j] = R.drawable.againdashboardagain;
						 }
						 if(elementicon.equalsIgnoreCase("NEWSLETTER"))
						 {
							 elementiconarraysearch[j] = R.drawable.menureport;
						 }
						 if(elementicon.equalsIgnoreCase("menuhome"))
						 {
							 elementiconarraysearch[j] = R.drawable.againdashboardagain;
						 }
						 if(elementicon.equalsIgnoreCase("menumarketprice"))
						 {
							 elementiconarraysearch[j] = R.drawable.marketpriceagainagain;
						 }
						 if(elementicon.equalsIgnoreCase("menudownload"))
						 {
							 elementiconarraysearch[j] = R.drawable.menudownload;
						 }
						 if(elementicon.equalsIgnoreCase("menunewbid"))
						 {
							 elementiconarraysearch[j] = R.drawable.againnewbidagain;
						 }
						 if(elementicon.equalsIgnoreCase("menureport"))
						 {
							 elementiconarraysearch[j] = R.drawable.menureport;
						 }
						 if(elementicon.equalsIgnoreCase("menunobid"))
						 {
							 elementiconarraysearch[j] = R.drawable.againnobidagain;
						 }
					 }

					 List<String> list = Arrays.asList(arraysearch);
					 Collections.reverse(list);
					 arraysearch = (String[]) list.toArray();

					 List<String> displaydatelist = Arrays.asList(arraydispalydatesearch);
					 Collections.reverse(displaydatelist);
					 arraydispalydatesearch = (String[]) displaydatelist.toArray();

					 List<String> displaytimelist = Arrays.asList(arraydispalytimesearch);
					 Collections.reverse(displaytimelist);
					 arraydispalytimesearch = (String[]) displaytimelist.toArray();

					 notificationlist.setAdapter(new NotificationCustomList(NotificationActivity.this,elementiconarraysearch, arraysearch, arraydispalydatesearch, arraydispalytimesearch));
				}}
			}
		});*/
    }

    @Override
    public void onDestroy() {
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
            //	getAgetunregisterReceiver(mHandleMessageReceiver);
            unregisterReceiver(mHandleMessageReceiver);
            GCMRegistrar.onDestroy(NotificationActivity.this);
        } catch (Exception e) {
            Log.e("UnRegister Receiver Error", "> " + e.getMessage());
        }
        super.onDestroy();
    }
	
	/*public void callNotification(){
		 db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
		 db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
		 db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
		 db.execSQL("DROP TABLE IF EXISTS pushnotificationUnRead");
		 Cursor c = db.rawQuery("SELECT * FROM pushnotification", null);
	    //	goMessage.setText("All Notification:" + c.getCount());
			if(c.getCount()==0)
			{
				tvnotificationnull.setBackgroundResource(R.drawable.sad);
				tvnotificationnulltext.setText("No Notification found.");
				tvnotificationnull.setVisibility(View.VISIBLE);
				tvnotificationnulltext.setVisibility(View.VISIBLE);
				notificationlist.setVisibility(View.GONE);
				//showMessage("Error", "No Notification found");
				//Toast.makeText(NotificationActivity.this, "No Notification found", Toast.LENGTH_LONG).show();
			}else{
			tvnotificationnull.setVisibility(View.GONE);
			tvnotificationnulltext.setVisibility(View.GONE);
			notificationlist.setVisibility(View.VISIBLE);
			StringBuffer buffer=new StringBuffer();
			icontypearray = new String[c.getCount()];
			array = new String[c.getCount()];
			arraydispalydate = new String[c.getCount()];
			arraydispalytime = new String[c.getCount()];
			elementiconarray = new Integer[c.getCount()];
			int i = 0;
			while(c.moveToNext())
			{
				buffer.append("dbacess: "+c.getString(0)+"\n");
				String dbacessfrom = c.getString(0);
				
				String[] parts = dbacessfrom.split("/");
				icontypefrom = parts[0];
				dbacess = parts[1];
				displaydate = c.getString(1);
				displaytime = c.getString(2);
				//goMessage.setText("All Notification:" + dbacess);
				array[i] = dbacess;
				arraydispalydate[i] = displaydate;
				arraydispalytime[i] = displaytime;
				icontypearray[i] = icontypefrom;
				
			    i++;
			    Log.d("icontype: ", icontypefrom);
				Log.d("dbacess: ", dbacess);
				Log.d("displaydateandtime: ", displaydate);
				Log.d("displaydateandtime: ", displaytime);
				//Toast.makeText(getActivity(), dbacess , Toast.LENGTH_LONG).show();
			}
			
			 List<String> icontypearraylist = Arrays.asList(icontypearray);
			 Collections.reverse(icontypearraylist);
			 icontypearray = (String[]) icontypearraylist.toArray();
			 
			 for(int j=0; j < icontypearray.length; j++){
				 String elementicon = icontypearray[j];
				 if(elementicon.equalsIgnoreCase("MCP"))
				 {
					 elementiconarray[j] = R.drawable.againdashboardagain;
				 }
				 if(elementicon.equalsIgnoreCase("NEWSLETTER"))
				 {
					 elementiconarray[j] = R.drawable.menureport;
				 }
				 if(elementicon.equalsIgnoreCase("menuhome"))
				 {
					 elementiconarray[j] = R.drawable.againdashboardagain;
				 }
				 if(elementicon.equalsIgnoreCase("menumarketprice"))
				 {
					 elementiconarray[j] = R.drawable.marketpriceagainagain;
				 }
				 if(elementicon.equalsIgnoreCase("menudownload"))
				 {
					 elementiconarray[j] = R.drawable.menudownload;
				 }
				 if(elementicon.equalsIgnoreCase("menunewbid"))
				 {
					 elementiconarray[j] = R.drawable.againnewbidagain;
				 }
				 if(elementicon.equalsIgnoreCase("menureport"))
				 {
					 elementiconarray[j] = R.drawable.menureport;
				 }
				 if(elementicon.equalsIgnoreCase("menunobid"))
				 {
					 elementiconarray[j] = R.drawable.againnobidagain;
				 }
				 if(elementicon.equalsIgnoreCase("curtailment"))
				 {
					 elementiconarray[j] = R.drawable.againnobidagain;
				 }
			 }
			
			 List<String> list = Arrays.asList(array);
			 Collections.reverse(list);
			 array = (String[]) list.toArray();
			 
			 List<String> displaydatelist = Arrays.asList(arraydispalydate);
			 Collections.reverse(displaydatelist);
			 arraydispalydate = (String[]) displaydatelist.toArray();
			 
			 List<String> displaytimelist = Arrays.asList(arraydispalytime);
			 Collections.reverse(displaytimelist);
			 arraydispalytime = (String[]) displaytimelist.toArray();
		
		     NotificationCustomList adapter = new NotificationCustomList(NotificationActivity.this, elementiconarray, array, arraydispalydate, arraydispalytime);
		     notificationlist.setAdapter(adapter);
			}
	}*/

    public void showMessage(String title, String message) {
        Builder builder = new Builder(NotificationActivity.this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(NotificationActivity.this, MainActivityAfterLogin.class);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    private void clearSharedPref(Context context) {
        SharedPreferences settings = context.getSharedPreferences(GCMIntentService.SHAREDPREF_PTS_DATA,
                Context.MODE_PRIVATE);
        settings.edit().clear().commit();
    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm Date, DeviceRegister.access_key
     */
    private class HttpAsyncTaskgetnotification extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Notification notification = new Notification();
            notification.setAccess_key(dbacessone);
            return NotificationAPIResponse.POST(urls[0], notification);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            //  Toast.makeText(getActivity(), "NoBidSave" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    tvnotificationnull.setVisibility(View.GONE);
                    tvnotificationnulltext.setVisibility(View.GONE);
                    notificationlist.setVisibility(View.VISIBLE);
                    id_array.clear();
                    date_array.clear();
                    message_array.clear();
                    module_type_array.clear();
                    status_array.clear();
                    timestamp_array.clear();
                    JSONObject mainObject = new JSONObject(result);
                    JSONArray details = mainObject.getJSONArray("details");
                    if (details.length() == 0) {
                        tvnotificationnull.setBackgroundResource(R.drawable.sad);
                        tvnotificationnulltext.setText("No Notification found.");
                        tvnotificationnull.setVisibility(View.VISIBLE);
                        tvnotificationnulltext.setVisibility(View.VISIBLE);
                        notificationlist.setVisibility(View.GONE);
                    } else {
                        tvnotificationnull.setVisibility(View.GONE);
                        tvnotificationnulltext.setVisibility(View.GONE);
                        notificationlist.setVisibility(View.VISIBLE);
                        for (int i = 0; i < details.length(); i++) {
                            JSONObject details_details = details.getJSONObject(i);
                            id = details_details.getString("id");
                            String date = details_details.getString("date");
                            message = details_details.getString("message");
                            if (message.startsWith("{")) {
                                JSONObject jsonObj = new JSONObject(message);
                                message = jsonObj.getString("message");
                                String revision = jsonObj.getString("revision");
                                String dateone = jsonObj.getString("date");
                            } else {

                            }
                            String module_type = details_details.getString("module_type");
                            String status = details_details.getString("status");
                            String timestamp = details_details.getString("timestamp");
                            String[] part = timestamp.split(" ");
                            String datestampone = part[0];
                            String timestamptwo = part[1];

                            id_array.add(id);
                            date_array.add(date);
                            message_array.add(message);
                            module_type_array.add(module_type);
                            status_array.add(status);
                            datestamp_array.add(datestampone);
                            timestamp_array.add(timestamptwo);
                        }
                    }
                    String more = mainObject.getString("more");
                    if (more.equalsIgnoreCase("TRUE")) {
                        tvloadmore.setVisibility(View.VISIBLE);
                    } else if (more.equalsIgnoreCase("FALSE")) {
                        tvloadmore.setVisibility(View.GONE);
                    } else {
                        tvloadmore.setVisibility(View.GONE);
                    }
                    for (int k = 0; k < module_type_array.size(); k++) {
                        String elementicon = module_type_array.get(k);
                        if (elementicon.equalsIgnoreCase("MCP")) {
                            icon_array.add(R.drawable.newhomedashboard);
                        } else if (elementicon.equalsIgnoreCase("NEWSLETTER")) {
                            icon_array.add(R.drawable.newhomenewsletter);
                        } else if (elementicon.equalsIgnoreCase("menuhome")) {
                            icon_array.add(R.drawable.newhomedashboard);
                        } else if (elementicon.equalsIgnoreCase("menumarketprice")) {
                            icon_array.add(R.drawable.newhomemarketprice);
                        } else if (elementicon.equalsIgnoreCase("menudownload")) {
                            icon_array.add(R.drawable.newhomedownload);
                        } else if (elementicon.equalsIgnoreCase("menunewbid")) {
                            icon_array.add(R.drawable.newhomenewbid);
                        } else if (elementicon.equalsIgnoreCase("menubid")) {
                            icon_array.add(R.drawable.newhomenewbid);
                        } else if (elementicon.equalsIgnoreCase("menureport")) {
                            icon_array.add(R.drawable.newhomepayment_details);
                        } else if (elementicon.equalsIgnoreCase("menunobid")) {
                            icon_array.add(R.drawable.newhomenobid);
                        } else if (elementicon.equalsIgnoreCase("curtailment")) {
                            icon_array.add(R.drawable.newhomecut);
                        } else if (elementicon.equalsIgnoreCase("recplacenewbid")) {
                            icon_array.add(R.drawable.newbidagain_rec);
                        } else if (elementicon.equalsIgnoreCase("recnobid")) {
                            icon_array.add(R.drawable.nobid_rec);
                        } else {
                            icon_array.add(R.drawable.newhomedashboard);
                        }
                    }
                    NotificationCustomList adapter = new NotificationCustomList(NotificationActivity.this, icon_array, message_array, datestamp_array, timestamp_array, status_array);
                    notificationlist.setAdapter(adapter);
                } catch (JSONException e) {
                    //{"status":"ERR","value":"-","message":"HTTPS ERROR. Please contact to admin."}
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }

    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm Date, DeviceRegister.access_key
     */
    private class HttpAsyncTaskgetnotificationloadmore extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Notification notification = new Notification();
            notification.setAccess_key(dbacessone);
            notification.setId(id);
            return NotificationLoadMoreAPIResponse.POST(urls[0], notification);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            //  Toast.makeText(getActivity(), "NoBidSave" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    tvnotificationnull.setVisibility(View.GONE);
                    tvnotificationnulltext.setVisibility(View.GONE);
                    notificationlist.setVisibility(View.VISIBLE);
                    id_array.clear();
                    date_array.clear();
                    message_array.clear();
                    module_type_array.clear();
                    status_array.clear();
                    timestamp_array.clear();
                    JSONObject mainObject = new JSONObject(result);
                    JSONArray details = mainObject.getJSONArray("details");
                    if (details.length() == 0) {
                        tvnotificationnull.setBackgroundResource(R.drawable.sad);
                        tvnotificationnulltext.setText("No Notification found.");
                        tvnotificationnull.setVisibility(View.VISIBLE);
                        tvnotificationnulltext.setVisibility(View.VISIBLE);
                        notificationlist.setVisibility(View.GONE);
                    } else {
                        tvnotificationnull.setVisibility(View.GONE);
                        tvnotificationnulltext.setVisibility(View.GONE);
                        notificationlist.setVisibility(View.VISIBLE);
                        for (int i = 0; i < details.length(); i++) {
                            JSONObject details_details = details.getJSONObject(i);
                            id = details_details.getString("id");
                            String date = details_details.getString("date");
                            message = details_details.getString("message");
                            if (message.startsWith("{")) {
                                JSONObject jsonObj = new JSONObject(message);
                                message = jsonObj.getString("message");
                                String revision = jsonObj.getString("revision");
                                String dateone = jsonObj.getString("date");
                            } else {

                            }
                            String module_type = details_details.getString("module_type");
                            String status = details_details.getString("status");
                            String timestamp = details_details.getString("timestamp");
                            String[] part = timestamp.split(" ");
                            String datestampone = part[0];
                            String timestamptwo = part[1];

                            id_array.add(id);
                            date_array.add(date);
                            message_array.add(message);
                            module_type_array.add(module_type);
                            status_array.add(status);
                            datestamp_array.add(datestampone);
                            timestamp_array.add(timestamptwo);
                        }
                    }
                    String more = mainObject.getString("more");
                    if (more.equalsIgnoreCase("TRUE")) {
                        tvloadmore.setVisibility(View.VISIBLE);
                    } else if (more.equalsIgnoreCase("FALSE")) {
                        tvloadmore.setVisibility(View.GONE);
                    } else {
                        tvloadmore.setVisibility(View.GONE);
                    }
                    for (int k = 0; k < module_type_array.size(); k++) {
                        String elementicon = module_type_array.get(k);
                        if (elementicon.equalsIgnoreCase("MCP")) {
                            icon_array.add(R.drawable.newhomedashboard);
                        } else if (elementicon.equalsIgnoreCase("NEWSLETTER")) {
                            icon_array.add(R.drawable.newhomenewsletter);
                        } else if (elementicon.equalsIgnoreCase("menuhome")) {
                            icon_array.add(R.drawable.newhomedashboard);
                        } else if (elementicon.equalsIgnoreCase("menumarketprice")) {
                            icon_array.add(R.drawable.newhomemarketprice);
                        } else if (elementicon.equalsIgnoreCase("menudownload")) {
                            icon_array.add(R.drawable.newhomedownload);
                        } else if (elementicon.equalsIgnoreCase("menunewbid")) {
                            icon_array.add(R.drawable.newhomenewbid);
                        } else if (elementicon.equalsIgnoreCase("menubid")) {
                            icon_array.add(R.drawable.newhomenewbid);
                        } else if (elementicon.equalsIgnoreCase("menureport")) {
                            icon_array.add(R.drawable.newhomepayment_details);
                        } else if (elementicon.equalsIgnoreCase("menunobid")) {
                            icon_array.add(R.drawable.newhomenobid);
                        } else if (elementicon.equalsIgnoreCase("curtailment")) {
                            icon_array.add(R.drawable.newhomecut);
                        } else {
                            icon_array.add(R.drawable.newhomecut);
                        }
                    }
                    NotificationCustomList adapter = new NotificationCustomList(NotificationActivity.this, icon_array, message_array, datestamp_array, timestamp_array, status_array);
                    notificationlist.setAdapter(adapter);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }

    }

    public class SelectDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 10) && (day <= 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                tvtradingdate.setText(dayplace + "-" + monthplace + "-" + year);
                as = tvtradingdate.getText().toString();
                // Toast.makeText(getActivity(), "DownloadResult" +
                // as,Toast.LENGTH_LONG).show();
            } else if (month <= 10) {
                String monthplace = "0" + month;
                tvtradingdate.setText(day + "-" + monthplace + "-" + year);
                as = tvtradingdate.getText().toString();
            } else if (day < 10) {
                String dayplace = "0" + day;
                tvtradingdate.setText(dayplace + "-" + month + "-" + year);
                as = tvtradingdate.getText().toString();
            } else {
                tvtradingdate.setText(day + "-" + month + "-" + year);
                as = tvtradingdate.getText().toString();
                // Toast.makeText(getActivity(), "DownloadResult" +
                // as,Toast.LENGTH_LONG).show();
                // (month + "/" + day + "/" + year);
                // (year + "-" + month + "-" + day);
            }
        }
    }
}
