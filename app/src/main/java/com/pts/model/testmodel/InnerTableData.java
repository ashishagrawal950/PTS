package com.pts.model.testmodel;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

/**
 * Created by Ashish on 28-07-2017.
 */

public class InnerTableData implements Parcelable {

    private String year;
    private String demand;
    private String peak;

    public InnerTableData(){}

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDemand() {
        return demand;
    }

    public void setDemand(String demand) {
        this.demand = demand;
    }

    public String getPeak() {
        return peak;
    }

    public void setPeak(String peak) {
        this.peak = peak;
    }


    protected InnerTableData(Parcel in) {
        year = in.readString();
        demand = in.readString();
        peak = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(year);
        dest.writeString(demand);
        dest.writeString(peak);
    }

    @SuppressWarnings("unused")
    public static final Creator<InnerTableData> CREATOR = new Creator<InnerTableData>() {
        @Override
        public InnerTableData createFromParcel(Parcel in) {
            return new InnerTableData(in);
        }

        @Override
        public InnerTableData[] newArray(int size) {
            return new InnerTableData[size];
        }
    };
}
