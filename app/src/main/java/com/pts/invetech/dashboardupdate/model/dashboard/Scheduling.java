
package com.pts.invetech.dashboardupdate.model.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Scheduling {

    @SerializedName("value")
    @Expose
    private Float value;
    @SerializedName("name")
    @Expose
    private String name;

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
