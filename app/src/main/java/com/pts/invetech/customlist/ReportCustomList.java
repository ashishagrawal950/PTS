package com.pts.invetech.customlist;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

public class ReportCustomList extends ArrayAdapter<String> {

	private  Activity context;
	private  List<String> text1;
	private  List<String> text2;
	private  List<String> text3;
	private  List<String> text4;
	private  List<String> text5;
	private static LayoutInflater inflater = null;
	private int lastPosition = -1;

	public ReportCustomList(Activity context, List<String> text1, List<String> text2, List<String> text3, List<String> text4, List<String> text5) {
		super(context, R.layout.report_listview, text1);
		this.context = context;
		this.text1 = text1;
		this.text2 = text2;
		this.text3 = text3;
		this.text4 = text4;
		this.text5 = text5;
	}

	
	
//	NoBidCustomList adapter = new NoBidCustomList(getActivity(), nobidid,nobidclientid, nobiddate);

	



	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.report_listview, null, true);
		TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
		TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
		TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
		TextView txt4 = (TextView) rowView.findViewById(R.id.txt4);
		TextView txt5 = (TextView) rowView.findViewById(R.id.txt5);
		
		txt1.setText(text1.get(position));
		txt2.setText(text2.get(position));
		txt3.setText(text3.get(position));
		txt4.setText(text4.get(position));
		txt5.setText(text5.get(position));
		Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		rowView.startAnimation(animation);
		lastPosition = position;
		//txt3.setText("Delete");
		// imageView.setImageResource(imageId[position]);
		return rowView;
	}
}
