package com.pts.invetech.customlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.model.PowerDemandOuter;

import java.util.ArrayList;

/**
 * Created by Ashish on 27-07-2017.
 */

public class PowerDemandComparisionCustomList extends BaseAdapter {

    private Context context;
    ArrayList<PowerDemandOuter> powerDemandComparisiondata;

    public PowerDemandComparisionCustomList(Context context, ArrayList<PowerDemandOuter> powerDemandComparisiondata) {
        this.context = context;
        this.powerDemandComparisiondata = powerDemandComparisiondata;
    }


    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_powerdemand, null, true);
        TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
        TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
        TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
        //txt1.setText(text1.get(position));
        //txt2.setText(text2.get(position));
        return rowView;
    }
}

