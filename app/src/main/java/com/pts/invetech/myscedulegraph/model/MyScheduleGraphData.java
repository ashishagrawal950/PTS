package com.pts.invetech.myscedulegraph.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vaibhav on 24/5/17.
 */

public class MyScheduleGraphData {

    @SerializedName("current_rev")
    @Expose
    private String currentRev;
    @SerializedName("previous_rev")
    @Expose
    private Integer previousRev;
    @SerializedName("detail")
    @Expose
    private List<MyScheduleDetailData> detail = null;

    public String getCurrentRev() {
        return currentRev;
    }

    public void setCurrentRev(String currentRev) {
        this.currentRev = currentRev;
    }

    public Integer getPreviousRev() {
        return previousRev;
    }

    public void setPreviousRev(Integer previousRev) {
        this.previousRev = previousRev;
    }

    public List<MyScheduleDetailData> getDetail() {
        return detail;
    }

    public void setDetail(List<MyScheduleDetailData> detail) {
        this.detail = detail;
    }

}