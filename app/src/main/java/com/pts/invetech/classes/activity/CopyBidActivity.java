package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.CopyBidAPIResponse;
import com.pts.invetech.pojo.CopyBidDate;
import com.pts.invetech.utils.Constant;
import com.pts.invetech.utils.JSONUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CopyBidActivity extends Activity {

    static ArrayList<String> date_array = new ArrayList<String>();
    private Context context;
    private LinearLayout mopenLayout;
    private SQLiteDatabase db;
    static TextView tvcopydate, tvcopydatehidden, tvcopydatefrom, tvcopydatehiddenfrom, tvcopydateto, tvcopydatehiddento, output;
    static String copydate, copydatefrom, copydateto;
    private ProgressBar progressBar;
    private Button copybidButtonSave, copybidButtoncancel;
    private Integer count = 1;
    private Dialog prgDialog;
    private String dbacess, status, value, message;
    private String datesend,domain;
    private LinearLayout lldynamictextview;
    private ArrayList<String> datesave_array = new ArrayList<String>();
    private ArrayList<String> massage_array = new ArrayList<String>();
    private TextView tvoutputtext, tvoutputtextrespose, tvline, tvoutputsuccess;
    private ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_copybid);
        /*prgDialog = new ProgressDialog(this);
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false)*/

        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        db = openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS copybid(date VARCHAR, copybidmassage VARCHAR);");
        Cursor c = db.rawQuery("SELECT * FROM baby", null);
        if (c.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("access_key: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CopyBidActivity.this, NewBidActivity.class);
                in.putExtra("name", "");
                in.putExtra("from", "MainActivity");
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        tvcopydate = (TextView) findViewById(R.id.tvcopydate);
        tvcopydatehidden = (TextView) findViewById(R.id.tvcopydatehidden);
        tvcopydatefrom = (TextView) findViewById(R.id.tvcopydatefrom);
        tvcopydatehiddenfrom = (TextView) findViewById(R.id.tvcopydatehiddenfrom);
        tvcopydateto = (TextView) findViewById(R.id.tvcopydateto);
        tvcopydatehiddento = (TextView) findViewById(R.id.tvcopydatehiddento);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax(10);
        copybidButtonSave = (Button) findViewById(R.id.copybidButtonSave);
        copybidButtoncancel = (Button) findViewById(R.id.copybidButtoncancel);
        output = (TextView) findViewById(R.id.output);
        // tvoutputtext = (TextView) findViewById(R.id.tvoutputtext);
        // tvoutputtextrespose = (TextView) findViewById(R.id.tvoutputtextrespose);
        lldynamictextview = (LinearLayout) findViewById(R.id.lldynamictextview);
        tvline = (TextView) findViewById(R.id.tvline);
        tvoutputsuccess = (TextView) findViewById(R.id.tvoutputsuccess);
        tvcopydate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectDateFragmentCopyBid();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        tvcopydatefrom.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectDateFragmentcopydatefrom();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        tvcopydateto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectDateFragmentcopydateto();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        copybidButtonSave.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Toast.makeText(CopyBidActivity.this, "copydate" + copydate, Toast.LENGTH_LONG).show();
                //Toast.makeText(CopyBidActivity.this, "copydatefrom" + copydatefrom, Toast.LENGTH_LONG).show();
                //Toast.makeText(CopyBidActivity.this, "copydateto" + copydateto, Toast.LENGTH_LONG).show();
                if ((copydate == null) || (copydatefrom == null) || (copydateto == null)) {
                    Toast.makeText(getApplicationContext(), "Please Select All the Dates.", Toast.LENGTH_LONG).show();
                } else {
                    List<Date> dates = new ArrayList<Date>();
                    try {
                        String str_date = copydatefrom;
                        String end_date = copydateto;
                        DateFormat formatter;
                        formatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date startDate = (Date) formatter.parse(str_date);
                        Date endDate = (Date) formatter.parse(end_date);
                        long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
                        long endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
                        long curTime = startDate.getTime();
                        while (curTime <= endTime) {
                            dates.add(new Date(curTime));
                            curTime += interval;
                        }
                        for (int i = 0; i < dates.size(); i++) {
                            Date lDate = (Date) dates.get(i);
                            String ds = formatter.format(lDate);
                            System.out.println(" Date is ..." + ds);
                            date_array.add(ds);
                        }
                    } catch (ParseException e) {
                        //Handle exception here, most of the time you will just log it.
                        e.printStackTrace();
                    }

                    if (date_array.size() == 0) {
                        Toast.makeText(getApplicationContext(), "To date should be greater than or equal to From Date.", Toast.LENGTH_LONG).show();
                    } else {
                        breakLoop:
                        for (int i = 0; i <= date_array.size(); i++) {
                            //String mmstr = Integer.toString(mm);
                            cd = new ConnectionDetector(CopyBidActivity.this);
                            if (!cd.isConnectingToInternet()) {
                                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                            } else {
                                lldynamictextview.removeAllViews();
                                datesend = date_array.get(i);
                                date_array.remove(i);
                                new HttpAsyncCopyBidTask().execute(domain + "/mobile/pxs_app/service/newbid/copybid.php");
                                break breakLoop;
                            }
                        }
                        //getfindDate();
                    }
                }
            }
        });


        copybidButtoncancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(CopyBidActivity.this, NewBidActivity.class);
                in.putExtra("name", "");
                startActivity(in);
                finish();
            }
        });

    }

    public void getfindDate() {
        breakLoop:
        for (int i = 0; i <= date_array.size(); i++) {
            //String mmstr = Integer.toString(mm);
            if (date_array.size() == 0) {
						/*Cursor c = db.rawQuery("SELECT * FROM copybid", null);
					    //	goMessage.setText("All Notification:" + c.getCount());
							if(c.getCount()==0)
							{
							}else{
							StringBuffer buffer=new StringBuffer();
							int k = 0;
							while(c.moveToNext())
							{
								String date = c.getString(0);
								message = c.getString(1);
								datesave_array.add(date);
								massage_array.add(message);
							    k++;
							}
						final int N = datesave_array.size(); // total number of textviews to add
						final TextView[] myTextViews = new TextView[N]; // create an empty array;
						for (int j = 0; j < N; j++) {
						    // create a new textview
						    final TextView rowTextView = new TextView(CopyBidActivity.this);
						    // set some properties of rowTextView or something
						    rowTextView.setText(massage_array.get(j));
						    rowTextView.setTextColor(Color.WHITE);
						    rowTextView.setTextSize(14);
						    // add the textview to the linearlayout
						    lldynamictextview.addView(rowTextView);
						    // save a reference to the textview for later
						    myTextViews[i] = rowTextView;
						}
					}*/

            } else {
                cd = new ConnectionDetector(CopyBidActivity.this);
                if (!cd.isConnectingToInternet()) {
                    Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    datesend = date_array.get(i);
                    date_array.remove(i);
                    new HttpAsyncCopyBidTask().execute(domain + "/mobile/pxs_app/service/newbid/copybid.php");
                }
                break breakLoop;
            }
        }
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(CopyBidActivity.this, NewBidActivity.class);
        in.putExtra("name", "");
        in.putExtra("from", "MainActivity");
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    public static class SelectDateFragmentCopyBid extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                tvcopydate.setText(dayplace + "-" + monthplace + "-" + year);
                tvcopydatehidden.setText(year + "-" + monthplace + "-" + dayplace);
                copydate = tvcopydatehidden.getText().toString();
                //  Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
            } else if (month <= 9) {
                String monthplace = "0" + month;
                tvcopydate.setText(day + "-" + monthplace + "-" + year);
                tvcopydatehidden.setText(year + "-" + monthplace + "-" + day);
                copydate = tvcopydatehidden.getText().toString();
            } else if (day < 10) {
                String dayplace = "0" + day;
                tvcopydate.setText(dayplace + "-" + month + "-" + year);
                tvcopydatehidden.setText(year + "-" + month + "-" + dayplace);
                copydate = tvcopydatehidden.getText().toString();
            } else {
                tvcopydate.setText(day + "-" + month + "-" + year);
                tvcopydatehidden.setText(year + "-" + month + "-" + day);
                copydate = tvcopydatehidden.getText().toString();
            }
        }
    }

    public static class SelectDateFragmentcopydatefrom extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                tvcopydatefrom.setText(dayplace + "-" + monthplace + "-" + year);
                tvcopydatehiddenfrom.setText(year + "-" + monthplace + "-" + dayplace);
                copydatefrom = tvcopydatehiddenfrom.getText().toString();
                //  Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
            } else if (month <= 9) {
                String monthplace = "0" + month;
                tvcopydatefrom.setText(day + "-" + monthplace + "-" + year);
                tvcopydatehiddenfrom.setText(year + "-" + monthplace + "-" + day);
                copydatefrom = tvcopydatehiddenfrom.getText().toString();
            } else if (day < 10) {
                String dayplace = "0" + day;
                tvcopydatefrom.setText(dayplace + "-" + month + "-" + year);
                tvcopydatehiddenfrom.setText(year + "-" + month + "-" + dayplace);
                copydatefrom = tvcopydatehiddenfrom.getText().toString();
            } else {
                tvcopydatefrom.setText(day + "-" + month + "-" + year);
                tvcopydatehiddenfrom.setText(year + "-" + month + "-" + day);
                copydatefrom = tvcopydatehiddenfrom.getText().toString();
            }
        }
    }

    public static class SelectDateFragmentcopydateto extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                tvcopydateto.setText(dayplace + "-" + monthplace + "-" + year);
                tvcopydatehiddento.setText(year + "-" + monthplace + "-" + dayplace);
                copydateto = tvcopydatehiddento.getText().toString();
                //  Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
            } else if (month < 10) {
                String monthplace = "0" + month;
                tvcopydateto.setText(day + "-" + monthplace + "-" + year);
                tvcopydatehiddento.setText(year + "-" + monthplace + "-" + day);
                copydateto = tvcopydatehiddento.getText().toString();
            } else if (day < 10) {
                String dayplace = "0" + day;
                tvcopydateto.setText(dayplace + "-" + month + "-" + year);
                tvcopydatehiddenfrom.setText(year + "-" + month + "-" + dayplace);
                copydateto = tvcopydatehiddenfrom.getText().toString();
            } else {
                tvcopydateto.setText(day + "-" + month + "-" + year);
                tvcopydatehiddento.setText(year + "-" + month + "-" + day);
                copydateto = tvcopydatehiddento.getText().toString();
            }
        }
    }

    private class HttpAsyncCopyBidTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            CopyBidDate copyBidDate = new CopyBidDate();
            copyBidDate.setCopydate(copydate);
            copyBidDate.setTodate(datesend);
            copyBidDate.setAccess_key(dbacess);
            return CopyBidAPIResponse.POST(urls[0], copyBidDate);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Copy Bid Result:", result);
            //Toast.makeText(getBaseContext(), "result" + result, Toast.LENGTH_LONG).show();
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {

                    JSONObject mainObject = new JSONObject(result);
                    if (mainObject.has("status")) {
                        status = mainObject.getString("status");
                    }
                    if (mainObject.has("value")) {
                        value = mainObject.getString("value");
                    }
                    if (mainObject.has("message")) {
                        message = mainObject.getString("message");
                    }

					/*if(status.equalsIgnoreCase("SUCCESS") && date_array.size()==0){
						Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
						Intent in = new Intent(CopyBidActivity.this, NewBidActivity.class);
						in.putExtra("name", "");
						startActivity(in);
						finish();
						db.execSQL("INSERT INTO device copybid('"+datesend+"', '"+message+"')");

						//tvoutputtext.setText(datesend+" "+message);
					}
					else*/
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        db.execSQL("DROP TABLE IF EXISTS copybid");
                        db.execSQL("CREATE TABLE IF NOT EXISTS copybid(date VARCHAR, copybidmassage VARCHAR);");
                        lldynamictextview.setVisibility(View.VISIBLE);
                        //Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
                        db.execSQL("INSERT INTO copybid VALUES('" + datesend + "', '" + message + "')");
						/*
						tvoutputtext.setVisibility(View.VISIBLE);
						tvoutputtextrespose.setVisibility(View.VISIBLE);
						tvoutputtext.setText("Copy Bid From " +copydate + " to  ");
					    tvoutputtextrespose.setText(message);*/
                        tvline.setVisibility(View.VISIBLE);
                        datesave_array.clear();
                        massage_array.clear();
                        Cursor c = db.rawQuery("SELECT * FROM copybid", null);
							/*if(c.getCount()==0)
							{
							}else{*/
                        while (c.moveToNext()) {
                            String date = c.getString(0);
                            message = c.getString(1);
                            datesave_array.add(date);
                            massage_array.add(message);
                            Object[] st = massage_array.toArray();
                            for (Object s : st) {
                                if (massage_array.indexOf(s) != massage_array.lastIndexOf(s)) {
                                    massage_array.remove(massage_array.lastIndexOf(s));
                                }
                            }
                        }
                        final int N = massage_array.size(); // total number of textviews to add
                        final TextView[] myTextViews = new TextView[N]; // create an empty array;
                        for (int j = 0; j < N; j++) {
                            // create a new textview
                            final TextView rowTextView = new TextView(CopyBidActivity.this);
                            // set some properties of rowTextView or something
                            lldynamictextview.removeView(rowTextView);
                            rowTextView.setText(massage_array.get(j));
                            rowTextView.setTextColor(Color.GREEN);
                            rowTextView.setTextSize(14);
                            // add the textview to the linearlayout
                            lldynamictextview.addView(rowTextView);
                            // save a reference to the textview for later
                            // myTextViews[i] = rowTextView;
                        }
                        //}

					   /* if(date_array.size()==0){
					   // tvoutputtext.setVisibility(View.GONE);
					  //  tvoutputtextrespose.setVisibility(View.GONE);
					    lldynamictextview.setVisibility(View.GONE);
					    tvoutputsuccess.setVisibility(View.VISIBLE);
					    tvoutputsuccess.setText("Copying Command executed Successfully.");
					    }*/

                    } else {
                        //Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();

                        db.execSQL("DROP TABLE IF EXISTS copybid");
                        db.execSQL("CREATE TABLE IF NOT EXISTS copybid(date VARCHAR, copybidmassage VARCHAR);");
                        lldynamictextview.setVisibility(View.VISIBLE);
                        //Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
                        db.execSQL("INSERT INTO copybid VALUES('" + datesend + "', '" + message + "')");
						/*
						tvoutputtext.setVisibility(View.VISIBLE);
						tvoutputtextrespose.setVisibility(View.VISIBLE);
						tvoutputtext.setText("Copy Bid From " +copydate + " to  ");
					    tvoutputtextrespose.setText(message);*/
                        tvline.setVisibility(View.VISIBLE);
                        datesave_array.clear();
                        massage_array.clear();
                        Cursor c = db.rawQuery("SELECT * FROM copybid", null);
							/*if(c.getCount()==0)
							{
							}else{*/
                        while (c.moveToNext()) {
                            String date = c.getString(0);
                            message = c.getString(1);
                            datesave_array.add(date);
                            massage_array.add(message);
                            Object[] st = massage_array.toArray();
                            for (Object s : st) {
                                if (massage_array.indexOf(s) != massage_array.lastIndexOf(s)) {
                                    massage_array.remove(massage_array.lastIndexOf(s));
                                }
                            }
                        }
                        final int N = massage_array.size(); // total number of textviews to add
                        final TextView[] myTextViews = new TextView[N]; // create an empty array;
                        for (int j = 0; j < N; j++) {
                            // create a new textview
                            final TextView rowTextView = new TextView(CopyBidActivity.this);
                            // set some properties of rowTextView or something
                            lldynamictextview.removeView(rowTextView);
                            rowTextView.setText(massage_array.get(j));
                            rowTextView.setTextColor(Color.RED);
                            rowTextView.setTextSize(14);
                            // add the textview to the linearlayout
                            lldynamictextview.addView(rowTextView);
                            // save a reference to the textview for later
                            // myTextViews[i] = rowTextView;
                        }
                        //}

					    /*if(date_array.size()==0){
					    // tvoutputtext.setVisibility(View.GONE);
					    //  tvoutputtextrespose.setVisibility(View.GONE);
					    *//*lldynamictextview.setVisibility(View.VISIBLE);
					    tvoutputsuccess.setVisibility(View.GONE);
					    // tvoutputsuccess.setText("Copying Command executed Successfully.");*//*
					    lldynamictextview.setVisibility(View.GONE);
						tvoutputsuccess.setVisibility(View.VISIBLE);
						tvoutputsuccess.setText("Copying Command executed Successfully.");
						}*/
                    }
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contect to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            getfindDate();
        }
    }


}
