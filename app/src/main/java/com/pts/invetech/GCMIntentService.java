package com.pts.invetech;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import android.util.Log;
import android.view.View;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.android.gcm.GCMBaseIntentService;
import com.pts.badge.ShortcutBadger;
import com.pts.invetech.classes.activity.CartainmentNotificationModuleBlueActivity;
import com.pts.invetech.classes.activity.CartainmentNotificationModuleDarkRedActivity;
import com.pts.invetech.classes.activity.CartainmentNotificationModuleGreenActivity;
import com.pts.invetech.classes.activity.CartainmentNotificationModuleRedActivity;
import com.pts.invetech.classes.activity.CartainmentNotificationModuleYellowActivity;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.activity.NewsFeedActivityWebViewNotification;
import com.pts.invetech.classes.activity.NotificationActivity;
import com.pts.invetech.classes.activity.SplashScreenActivity;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.pts.invetech.CommonUtilities.SENDER_ID;
import static com.pts.invetech.CommonUtilities.displayMessage;

@SuppressLint("SimpleDateFormat")
public class GCMIntentService extends GCMBaseIntentService {
    static SQLiteDatabase db;
    String datedb, timedb;
    static String type;
    String message, messageicon, color;
    String messageone;
    private static final String TAG = "GCMIntentService";
    int notifCounter = 0;
    private String GROUP_KEY_BID = "menubid";
    private Context self;


    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    String formattedDate = df.format(c.getTime());
    String[] splitdateandtime = formattedDate.split(" ");

    {
        datedb = splitdateandtime[0]; // 004
        timedb = splitdateandtime[1]; // 004
    }

    public GCMIntentService() {
        super(SENDER_ID);
    }

    /**
     * Method called on device registered, NotificationActivity
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);

        AppLogger.showMsg("GCM ID -->", "" + registrationId);

        displayMessage(context, "Your device registred with GCM");
        Log.d("NAME", SplashScreenActivity.deviceid);
        ServerUtilities.register(context, SplashScreenActivity.deviceid, registrationId);
    }

    /**
     * Method called on device un registred
     */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        displayMessage(context, getString(R.string.gcm_unregistered));
        ServerUtilities.unregister(context, registrationId);
    }

    /**
     * Method called on Receiving a new message
     */
    @Override
    protected void onMessage(Context context, Intent intent) {
        self = this;
        Log.i(TAG, "Received message");
        String msg = intent.getExtras().getString("price");
        AppLogger.showMsg("notification msg", "" + msg);
        Bundle message1 = intent.getExtras();
        System.out.println("message1=" + message1);
        try {
            JSONObject jsonObj = new JSONObject(msg);
            type = jsonObj.getString("type");

            message = jsonObj.getString("msg");

            messageicon = type + "/" + message;
            if (messageicon.contains("'s")) {
                messageicon = messageicon.replace("'s", " ");
            } else if (messageicon.contains("'")) {
                messageicon = messageicon.replace("'", " ");
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        displayMessage(context, message);
        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        if (message.equalsIgnoreCase("Your device registred with GCM") || message.equalsIgnoreCase("Trying (attempt 1/5) to register device on Demo Server.") ||
                message.equalsIgnoreCase("From Demo Server: successfully added device!") || message.equalsIgnoreCase("null")) {
        } else {
            db.execSQL("DROP TABLE IF EXISTS pushnotification");
            db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
            db.execSQL("INSERT INTO pushnotification VALUES('" + messageicon + "', '" + datedb + "', '" + timedb + "')");

            if (type.equalsIgnoreCase("newsfeedactivity")) {
                db.execSQL("DROP TABLE IF EXISTS pushnotificationnewsfeed");
                db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationnewsfeed(newMessage TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
                db.execSQL("INSERT INTO pushnotificationnewsfeed VALUES('" + messageicon + "', '" + messageicon + "', '" + messageicon + "')");

                db.execSQL("DROP TABLE IF EXISTS newfeedsave");
                db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
            }

            db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
            db.execSQL("INSERT INTO pushnotificationUnRead VALUES('unread')");
        }
        // notifies user
        generateNotification(context, message);
        //Cursor cnot = db.rawQuery("SELECT * FROM pushnotificationUnRead", null);
    }

    /**
     * Method called on receiving a deleted message
     */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        displayMessage(context, message);
        // notifies user
        generateNotification(context, message);
    }

    /**
     * Method called on Error
     */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        displayMessage(context, getString(R.string.gcm_recoverable_error,
                errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private void generateNotification(Context context, String message) {
        int icon = R.drawable.logopts;
        long when = System.currentTimeMillis();
        String tmpStr = String.valueOf(when);
        String last4Str = tmpStr.substring(tmpStr.length() - 5);
        int notify_no = Integer.valueOf(last4Str);
        String title = context.getString(R.string.sliderheading);
        String messageone = "";
        String colorget = "";
        String GROUP_KEY_BID = "menubid";
        String GROUP_KEY_NEWS = "news";

        try {
            JSONObject jsonObj = new JSONObject(message);
            messageone = jsonObj.getString("message");
            colorget = jsonObj.getString("color");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Notification notification;
        if (type.equalsIgnoreCase("MCP")) {
            String tmp = saveInPref(context, message, NOTIFICTIONSPLITER.MENUBID);
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
                notificationIntent = new Intent(context, LoginInActivity.class);
            } else {
                notificationIntent = new Intent(context, NotificationActivity.class);
            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);
                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);

            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);

            }


        }

        if (type.equalsIgnoreCase("NEWSLETTER")) {
            String tmp = saveInPref(context, message, NOTIFICTIONSPLITER.MENUBID);
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
                notificationIntent = new Intent(context, LoginInActivity.class);
            } else {
                notificationIntent = new Intent(context, NotificationActivity.class);

            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            }


        } else if (type.equalsIgnoreCase("menuhome")) {
            String tmp = saveInPref(context, message, NOTIFICTIONSPLITER.MENUBID);
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
                notificationIntent = new Intent(context, LoginInActivity.class);
            } else {
                notificationIntent = new Intent(context, NotificationActivity.class);

            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            }

        } else if (type.equalsIgnoreCase("menumarketprice")) {
            String tmp = saveInPref(context, message, NOTIFICTIONSPLITER.MENUBID);
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
                notificationIntent = new Intent(context, LoginInActivity.class);
            } else {
                notificationIntent = new Intent(context, NotificationActivity.class);

            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            }

        } else if (type.equalsIgnoreCase("menudownload")) {
            String tmp = saveInPref(context, message, NOTIFICTIONSPLITER.MENUBID);
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
                notificationIntent = new Intent(context, LoginInActivity.class);
            } else {
                notificationIntent = new Intent(context, NotificationActivity.class);

            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            }

        } else if (type.equalsIgnoreCase("menunewbid")) {
            String tmp = saveInPref(context, message, NOTIFICTIONSPLITER.MENUBID);
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
                notificationIntent = new Intent(context, LoginInActivity.class);
            } else {
                notificationIntent = new Intent(context, NotificationActivity.class);

            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            }

        } else if (type.equalsIgnoreCase("menureport")) {
            String tmp = saveInPref(context, message, NOTIFICTIONSPLITER.MENUBID);
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
                notificationIntent = new Intent(context, LoginInActivity.class);
            } else {
                notificationIntent = new Intent(context, NotificationActivity.class);
            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            }

        } else if (type.equalsIgnoreCase("menunobid")) {
            String tmp = saveInPref(context, message, NOTIFICTIONSPLITER.MENUBID);

            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
                notificationIntent = new Intent(context, LoginInActivity.class);
            } else {

                notificationIntent = new Intent(context, NotificationActivity.class);
            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            }


        } else if (type.equalsIgnoreCase("recplacenewbid")) {
            String tmp = saveInPref(context, message, NOTIFICTIONSPLITER.MENUBID);
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
                notificationIntent = new Intent(context, LoginInActivity.class);
            } else {
                notificationIntent = new Intent(context, NotificationActivity.class);
            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            }

        } else if (type.equalsIgnoreCase("recnobid")) {
            String tmp = saveInPref(context, message, NOTIFICTIONSPLITER.MENUBID);
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
                notificationIntent = new Intent(context, LoginInActivity.class);
            } else {
                notificationIntent = new Intent(context, NotificationActivity.class);
            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            }

        } else if (colorget.equalsIgnoreCase("precurtailment")) {
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            notificationIntent = new Intent(context, CartainmentNotificationModuleBlueActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {

                PendingIntent intent = PendingIntent.getActivity(context, notify_no, notificationIntent,
                        PendingIntent.FLAG_IMMUTABLE);
                notification = new Notification.Builder(context)
                        .setContentTitle(title).setContentText(messageone).setSmallIcon(R.drawable.logopts)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(false)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                //notification.defaults |= Notification.DEFAULT_SOUND;
                notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
                notificationManager.notify(notify_no, notification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification = new Notification.Builder(context)
                        .setContentTitle(title).setContentText(messageone).setSmallIcon(R.drawable.logopts)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(false)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                //notification.defaults |= Notification.DEFAULT_SOUND;
                notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
                notificationManager.notify(notify_no, notification);
            }

        } else if (colorget.equalsIgnoreCase("NLDCTOIEX")) {
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            notificationIntent = new Intent(context, CartainmentNotificationModuleDarkRedActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);
                notification = new Notification.Builder(context)
                        .setContentTitle(title).setContentText(messageone).setSmallIcon(R.drawable.logopts)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(false)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                //notification.defaults |= Notification.DEFAULT_SOUND;
                notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
                notificationManager.notify(notify_no, notification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification = new Notification.Builder(context)
                        .setContentTitle(title).setContentText(messageone).setSmallIcon(R.drawable.logopts)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(false)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                //notification.defaults |= Notification.DEFAULT_SOUND;
                notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
                notificationManager.notify(notify_no, notification);
            }

        } else if (colorget.equalsIgnoreCase("NOTAFFECTED")) {
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            notificationIntent = new Intent(context, CartainmentNotificationModuleGreenActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);
                notification = new Notification.Builder(context)
                        .setContentTitle(title).setContentText(messageone).setSmallIcon(R.drawable.logopts)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(false)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                //notification.defaults |= Notification.DEFAULT_SOUND;
                notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
                notificationManager.notify(notify_no, notification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification = new Notification.Builder(context)
                        .setContentTitle(title).setContentText(messageone).setSmallIcon(R.drawable.logopts)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(false)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                //notification.defaults |= Notification.DEFAULT_SOUND;
                notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
                notificationManager.notify(notify_no, notification);
            }

        } else if (colorget.equalsIgnoreCase("REALTIME")) {
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            notificationIntent = new Intent(context, CartainmentNotificationModuleRedActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);
                notification = new Notification.Builder(context)
                        .setContentTitle(title).setContentText(messageone).setSmallIcon(R.drawable.logopts)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(false)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                //notification.defaults |= Notification.DEFAULT_SOUND;
                notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
                notificationManager.notify(notify_no, notification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification = new Notification.Builder(context)
                        .setContentTitle(title).setContentText(messageone).setSmallIcon(R.drawable.logopts)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(false)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                //notification.defaults |= Notification.DEFAULT_SOUND;
                notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
                notificationManager.notify(notify_no, notification);
            }

        } else if (colorget.equalsIgnoreCase("RESTORE")) {
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            notificationIntent = new Intent(context, CartainmentNotificationModuleYellowActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);
                notification = new Notification.Builder(context)
                        .setContentTitle(title).setContentText(messageone).setSmallIcon(R.drawable.logopts)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(false)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                //notification.defaults |= Notification.DEFAULT_SOUND;
                notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
                notificationManager.notify(notify_no, notification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                notification = new Notification.Builder(context)
                        .setContentTitle(title).setContentText(messageone).setSmallIcon(R.drawable.logopts)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(false)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;
                //notification.defaults |= Notification.DEFAULT_SOUND;
                notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
                notificationManager.notify(notify_no, notification);
            }

        } else if (type.equalsIgnoreCase("newsfeedactivity")) {
            String messagenewsfeed = "";
            String link = "";
            try {
                JSONObject jsonObj = new JSONObject(message);
                if (jsonObj.has("message")) {
                    messagenewsfeed = jsonObj.getString("message");
                }
                if (jsonObj.has("category")) {
                    String category = jsonObj.getString("category");
                }
                if (jsonObj.has("link")) {
                    link = jsonObj.getString("link");
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String tmp = saveInPref(context, messagenewsfeed, NOTIFICTIONSPLITER.NEWS);
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            notificationIntent = new Intent(context, NewsFeedActivityWebViewNotification.class);
            notificationIntent.putExtra("link", link);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);
            /*notification = new Notification.Builder(context)
            .setContentTitle(title).setContentText("News: "+messagenewsfeed).setSmallIcon(R.drawable.logopts)
			.setContentIntent(intent).setWhen(when).setAutoCancel(true)
			.build();*/
                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine("News: " + tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_NEWS)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                //	summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                //summaryNotification.flags
                //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
                notificationManager.notify(11, summaryNotification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            /*notification = new Notification.Builder(context)
            .setContentTitle(title).setContentText("News: "+messagenewsfeed).setSmallIcon(R.drawable.logopts)
			.setContentIntent(intent).setWhen(when).setAutoCancel(true)
			.build();*/
                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine("News: " + tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_NEWS)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                //	summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                //summaryNotification.flags
                //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
                notificationManager.notify(11, summaryNotification);
            }


        } else if (type.equalsIgnoreCase("Inter Country") ||
                type.equalsIgnoreCase("Inter Regional") ||
                type.equalsIgnoreCase("Intra Regional") ||
                type.equalsIgnoreCase("INTERCOUNTRY") ||
                type.equalsIgnoreCase("INTERREGIONAL") ||
                type.equalsIgnoreCase("INTRAREGIONAL") ||
                type.equalsIgnoreCase("intra-regional") ||
                type.equalsIgnoreCase("inter-country") ||
                type.equalsIgnoreCase("inter-regional")) {
            sendNNotif(context, type, when, notify_no);

        } else if (type.equalsIgnoreCase("scheduleTrack")) {
            drawMapsingle(message);
        } else {
            String tmp = saveInPref(context, message, NOTIFICTIONSPLITER.MENUBID);
            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent;
            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
                notificationIntent = new Intent(context, LoginInActivity.class);
            } else {
                notificationIntent = new Intent(context, NotificationActivity.class);

            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_IMMUTABLE);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            } else {
                PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

                String[] tmpSplit = tmp.split(":");

                for (int index = 0; index < tmpSplit.length; index++) {
                    istyle.addLine(tmpSplit[index]);
                }
                istyle.setBigContentTitle("PTS");
                if (notifCounter != 0) {
                    istyle.setSummaryText("+" + notifCounter + " more");
                }

                Notification summaryNotification = new NotificationCompat.Builder(context)
                        .setContentTitle("PTS")
                        .setContentText(tmpSplit[0])
                        .setSmallIcon(R.drawable.logopts)
                        .setStyle(istyle)
                        .setGroup(GROUP_KEY_BID)
                        .setGroupSummary(true)
                        .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                        .build();

                //summaryNotification.flags = Notification.FLAG_INSISTENT | Notification.FLAG_AUTO_CANCEL;
                summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
                summaryNotification.defaults |= Notification.DEFAULT_SOUND;
                // NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(11, summaryNotification);
            }

        }
    }




	/*
        Notification Class For general
	 */

    private void sendNNotif(Context context, String type, long when, int notify_no) {
        String tmp = saveInPref(context, message, NOTIFICTIONSPLITER.MENUBID);
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent;

        notificationIntent = new Intent(context, NewFeedMainActivity.class);
        notificationIntent.putExtra("LOGINCONFIG", "LOGIN");
        notificationIntent.putExtra(Constant.VALUE_DATAPASS, type);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                    notificationIntent, PendingIntent.FLAG_IMMUTABLE);

            NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

            String[] tmpSplit = tmp.split(":");

            for (int index = 0; index < tmpSplit.length; index++) {
                istyle.addLine(tmpSplit[index]);
            }
            istyle.setBigContentTitle("PTS");
            if (notifCounter != 0) {
                istyle.setSummaryText("+" + notifCounter + " more");
            }

            Notification summaryNotification = new NotificationCompat.Builder(context)
                    .setContentTitle("PTS")
                    .setContentText(tmpSplit[0])
                    .setSmallIcon(R.drawable.logopts)
                    .setStyle(istyle)
                    .setGroup(GROUP_KEY_BID)
                    .setGroupSummary(true)
                    .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                    .build();

            summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
            summaryNotification.defaults |= Notification.DEFAULT_SOUND;
            notificationManager.notify(11, summaryNotification);
        } else {
            PendingIntent intent = PendingIntent.getActivity(context, notify_no,
                    notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.InboxStyle istyle = new NotificationCompat.InboxStyle();

            String[] tmpSplit = tmp.split(":");

            for (int index = 0; index < tmpSplit.length; index++) {
                istyle.addLine(tmpSplit[index]);
            }
            istyle.setBigContentTitle("PTS");
            if (notifCounter != 0) {
                istyle.setSummaryText("+" + notifCounter + " more");
            }

            Notification summaryNotification = new NotificationCompat.Builder(context)
                    .setContentTitle("PTS")
                    .setContentText(tmpSplit[0])
                    .setSmallIcon(R.drawable.logopts)
                    .setStyle(istyle)
                    .setGroup(GROUP_KEY_BID)
                    .setGroupSummary(true)
                    .setContentIntent(intent).setWhen(when).setAutoCancel(true)
                    .build();

            summaryNotification.flags |= Notification.FLAG_AUTO_CANCEL;
            summaryNotification.defaults |= Notification.DEFAULT_SOUND;
            notificationManager.notify(11, summaryNotification);
        }

    }


    public static String SHAREDPREF_PTS_DATA = "pts_shared_pref";
    private String SHAREDPREF_PTS_MENUBID_COUNTER = "pts_shared_pref_menubid";
    private String SHAREDPREF_PTS_MENUBID_COUNTER_HEADLINE_FULL = "pts_shared_pref_menubid_headline";

    private String SHAREDPREF_PTS_NEWS_COUNTER = "pts_shared_pref_news";
    private String SHAREDPREF_PTS_NEWS_COUNTER_HEADLINE_FULL = "pts_shared_pref_news_headline";


    public String saveInPref(Context context, String msg, NOTIFICTIONSPLITER spliter) {
        notifCounter = 0;
        String counter = SHAREDPREF_PTS_MENUBID_COUNTER;
        String headerTitle = SHAREDPREF_PTS_MENUBID_COUNTER_HEADLINE_FULL;

        switch (spliter) {
            case MENUBID:
                counter = SHAREDPREF_PTS_MENUBID_COUNTER;
                headerTitle = SHAREDPREF_PTS_MENUBID_COUNTER_HEADLINE_FULL;
                break;
            case NEWS:
                counter = SHAREDPREF_PTS_NEWS_COUNTER;
                headerTitle = SHAREDPREF_PTS_NEWS_COUNTER_HEADLINE_FULL;
                break;
        }

        SharedPreferences sharedPref = context.getSharedPreferences(
                SHAREDPREF_PTS_DATA, Context.MODE_PRIVATE);

        notifCounter = sharedPref.getInt(counter, 0);
        notifCounter++;

        String msgTmp = sharedPref.getString(headerTitle, null);


        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(counter, notifCounter);
        editor.commit();

        if (notifCounter != 0) {
            //istyle.setSummaryText("+"+notifCounter+" more");
            ShortcutBadger.applyCount(context, notifCounter);
        }

        if (notifCounter <= 3) {
            msgTmp = writeStringToPref(context, msg, sharedPref, msgTmp, headerTitle);
            notifCounter = 0;
        } else {
            notifCounter = notifCounter - 3;
        }


        return msgTmp;
    }

    public String writeStringToPref(Context context, String msg, SharedPreferences pref, String tmpMsg, String headerCounter) {
        if (pref == null) {
            pref = context.getSharedPreferences(SHAREDPREF_PTS_DATA, Context.MODE_PRIVATE);
        }

        if (tmpMsg == null) {
            tmpMsg = msg;
        } else {
            tmpMsg = msg + ":" + tmpMsg;
        }
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(headerCounter, tmpMsg);
        editor.commit();

        return tmpMsg;
    }

    enum NOTIFICTIONSPLITER {
        MENUBID,
        NEWS;
    }

    // for graph single notification
    String messageforsinglegraph, jsontypeforgraph, app_noforgraph, data_idforgraph;
    ArrayList<String> block_noarraysingle = new ArrayList<String>();
    ArrayList<String> details_arraysingle = new ArrayList<String>();

    ArrayList<String> block_noarraydouble = new ArrayList<String>();
    ArrayList<String> last_qtmearraydouble = new ArrayList<String>();
    ArrayList<String> new_qtmarraydouble = new ArrayList<String>();

    private void drawMapsingle(String datapassed) {

        AppLogger.showMsg("JSON DATA-->" ,datapassed);

        try {

            JSONObject mainObject = new JSONObject(datapassed);
            //String type = mainObject.getString("type");
            //JSONObject msgmainObject = mainObject.getJSONObject("msg");
            messageforsinglegraph = mainObject.getString("message");
            data_idforgraph = mainObject.getString("data_id");

            JSONObject detailmainObject = mainObject.getJSONObject("detail");
            //String importtype = detailmainObject.getString("importtype");
            //String  date = detailmainObject.getString("date");
            app_noforgraph = detailmainObject.getString("app_no");
            //String  lastRev = detailmainObject.getString("lastRev");
            //String currentRev = detailmainObject.getString("currentRev");
            //String current_qtm = detailmainObject.getString("current_qtm");
            //String last_qtm = detailmainObject.getString("last_qtm");

            jsontypeforgraph = detailmainObject.getString("jsontype");

            details_arraysingle.clear();
            block_noarraysingle.clear();


            if (jsontypeforgraph.equalsIgnoreCase("single")) {
                JSONObject jSONObjectdetails = detailmainObject.getJSONObject("details");

//                Field[] ninthNumber = jSONObjectdetails.getClass().getDeclaredFields();
//
                for (int index = 0; index < jSONObjectdetails.length(); index++) {
                    if (jSONObjectdetails.has("block_" + (index + 1))) {
                        String tmm = jSONObjectdetails.getString("block_" + (index + 1));
                        String strI = String.valueOf(index + 1);
                        block_noarraysingle.add(strI);
                        details_arraysingle.add(tmm);
                    }
                }
//                AppLogger.showMsgWithoutTag(block_noarraysingle.size()+"     SIZES       "+details_arraysingle.size());
//
//                for(int index=0;index<ninthNumber.length;index++){
//                    AppLogger.showMsgWithoutTag(block_noarraysingle.get(index)+"     SIZES       "+details_arraysingle.get(index));
//                }

//                for(int inde=0;inde<strTmp.length;inde++){
//                    block_noarraydouble.add(String.valueOf(inde));
//                    if(jSONObjectdetails.has(strTmp[inde])){
//
//                    }
//                }

//
//
//                for (int i = 0; i < jSONObjectdetails.length(); i++) {
//                    String strI = String.valueOf(i);
//                    block_noarraysingle.add(strI);
//                }
//                Iterator iter = jSONObjectdetails.keys();
//                while (iter.hasNext()) {
//                    String key = (String) iter.next();
//                    String value = jSONObjectdetails.getString(key);
//                    details_arraysingle.add(value);
//                }
            } else {


                JSONArray details = detailmainObject.getJSONArray("details");
                block_noarraydouble.clear();
                last_qtmearraydouble.clear();
                new_qtmarraydouble.clear();
                for (int i = 0; i < details.length(); i++) {
                    String strI = String.valueOf(i);
                    JSONObject obligationdetail = details.getJSONObject(i);
                    String block_no = obligationdetail.getString("b");
                    String last_qtmfo = obligationdetail.getString("l");
                    String new_qtm = obligationdetail.getString("c");
                    block_noarraydouble.add(block_no);
                    last_qtmearraydouble.add(last_qtmfo);
                    new_qtmarraydouble.add(new_qtm);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            @Override
            public void run() {
                if (jsontypeforgraph.equalsIgnoreCase("single")) {
                    drawChartNavfornotificationsingle();
                } else {
                    drawChartNavfornotification();
                }
            }
        });
    }

    LineChart chartd;

    private void drawChartNavfornotificationsingle() {

        ArrayList<Entry> entriescurrent = new ArrayList<>();

        for (int min = 0; min < details_arraysingle.size(); min++) {
            entriescurrent.add(new Entry(Float.parseFloat(details_arraysingle.get(min)), min));
        }

        LineDataSet datasetcurrent = new LineDataSet(entriescurrent, "");
        datasetcurrent.setCircleColor(Color.GREEN);
        datasetcurrent.setColor(Color.GREEN);
        datasetcurrent.setValueTextColor(Color.WHITE);
        //datasetcurrent.setAxisDependency();
        datasetcurrent.setCircleSize(1f);
        datasetcurrent.setValueTextSize(4f);
        datasetcurrent.setLineWidth(1f);
        //datasetcurrent.

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(datasetcurrent);

        chartd = new LineChart(self);
        LineData data = new LineData(block_noarraysingle, dataSets);
        chartd.setData(data);


        chartd.measure(View.MeasureSpec.makeMeasureSpec(400, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(200, View.MeasureSpec.EXACTLY));
        chartd.layout(0, 0, chartd.getMeasuredWidth(), chartd.getMeasuredHeight());
        chartd.invalidate();


        chartd.setDescription("X(Block no)/Y(Qtm in Mw)");  // set the description
        chartd.setDescriptionTextSize(4f);

        XAxis xAxis = chartd.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(4f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        chartd.setDrawGridBackground(false);
        chartd.setViewPortOffsets(30, 2, 30, 0);

        chartd.getAxisLeft().setTextSize(4f);
        //chartd.getAxisLeft().setTypeface(openSansRegular);
        chartd.getAxisLeft().setTextColor(Color.BLACK);
        chartd.getAxisLeft().setStartAtZero(true);

        //chartd.getAxisRight().setEnabled(false);

        chartd.getAxisRight().setTextSize(4f);
        //chartd.getAxisLeft().setTypeface(openSansRegular);
        chartd.getAxisRight().setTextColor(Color.BLACK);
        chartd.getAxisRight().setStartAtZero(true);

        chartd.getLegend().setTextSize(4f);
        chartd.getLegend().setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        chartd.getLegend().setForm(Legend.LegendForm.SQUARE);
        /*.setTextSize(4f);
        //chartd.getAxisRight().setTypeface(openSansRegular);
        chartd.getAxisRight().setTextColor(Color.BLACK);
        chartd.getAxisRight().setStartAtZero(true);*/
        //chartd.setBackgroundColor(Color.parseColor(R.color.colorPrimary));
        Bitmap imgBitmap = chartd.getChartBitmap();
        showNotif(imgBitmap, true);
    }

    private void drawChartNavfornotification() {
        ArrayList<Entry> entriescurrent = new ArrayList<>();
        for (int min = 0; min < last_qtmearraydouble.size(); min++) {
            entriescurrent.add(new Entry(Float.parseFloat(last_qtmearraydouble.get(min)), min));
        }
        LineDataSet datasetcurrent = new LineDataSet(entriescurrent, "Last Qtm");
        datasetcurrent.setCircleColor(Color.GREEN);
        datasetcurrent.setColor(Color.GREEN);
        datasetcurrent.setValueTextColor(Color.WHITE);
        datasetcurrent.setCircleSize(1f);
        datasetcurrent.setValueTextSize(4f);
        datasetcurrent.setLineWidth(1f);
        //datasetcurrent.

        // creating list of entry
        ArrayList<Entry> entriespreviours = new ArrayList<>();
        for (int min = 0; min < new_qtmarraydouble.size(); min++) {
            entriespreviours.add(new Entry(Float.parseFloat(new_qtmarraydouble.get(min)), min));
        }
        LineDataSet datasetprevoursday = new LineDataSet(entriespreviours, "New Qtm");
        datasetprevoursday.setCircleColor(Color.BLUE);
        datasetprevoursday.setColor(Color.BLUE);
        datasetprevoursday.setValueTextColor(Color.WHITE);
        datasetprevoursday.setCircleSize(1f);
        datasetprevoursday.setValueTextSize(4f);
        datasetprevoursday.setLineWidth(1f);

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(datasetcurrent);
        dataSets.add(datasetprevoursday);

        chartd = new LineChart(self);
        LineData data = new LineData(block_noarraydouble, dataSets);
        chartd.setData(data);


        chartd.measure(View.MeasureSpec.makeMeasureSpec(400, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(200, View.MeasureSpec.EXACTLY));
        chartd.layout(0, 0, chartd.getMeasuredWidth(), chartd.getMeasuredHeight());
        chartd.invalidate();


        chartd.setDescription("Qtm in MW");  // set the description
        chartd.setDescriptionTextSize(4f);

        XAxis xAxis = chartd.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(4f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        chartd.setDrawGridBackground(false);


        chartd.getAxisLeft().setTextSize(4f);
        //chartd.getAxisLeft().setTypeface(openSansRegular);
        chartd.getAxisLeft().setTextColor(Color.BLACK);
        chartd.getAxisLeft().setStartAtZero(true);

        //chartd.getAxisRight().setEnabled(false);
        chartd.getAxisRight().setTextSize(4f);
        //chartd.getAxisLeft().setTypeface(openSansRegular);
        chartd.getAxisRight().setTextColor(Color.BLACK);
        chartd.getAxisRight().setStartAtZero(true);

        chartd.getLegend().setTextSize(4f);
        chartd.getLegend().setPosition(Legend.LegendPosition.ABOVE_CHART_LEFT);
        chartd.getLegend().setForm(Legend.LegendForm.SQUARE);

        //chartd.getLegend().setEnabled(false);

        chartd.setViewPortOffsets(30, 10, 30, 0);
        /*.setTextSize(4f);
        //chartd.getAxisRight().setTypeface(openSansRegular);
        chartd.getAxisRight().setTextColor(Color.BLACK);
        chartd.getAxisRight().setStartAtZero(true);*/

        Bitmap imgBitmap = chartd.getChartBitmap();
        showNotif(imgBitmap, false);
    }


    private void showNotif(Bitmap bitmap, boolean isSingle) {

        Uri soundUri = null;

        if (isSingle) {
            soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        } else {
            soundUri = Uri.parse("android.resource://" + self.getPackageName() + "/" + R.raw.dangeralar_eit3xqka);
        }


        NotificationCompat.Builder nb = new NotificationCompat.Builder(self);
        nb.setSmallIcon(R.drawable.logopts);
        nb.setContentTitle("PTS");
        nb.setContentText(messageforsinglegraph);
        nb.setTicker("Schedule Tracking");
        nb.setSound(soundUri);
        //nb.setLargeIcon(result).build();

        NotificationCompat.BigPictureStyle s = new NotificationCompat.BigPictureStyle().bigPicture(bitmap);
        s.setSummaryText(messageforsinglegraph);
        nb.setStyle(s);

        Intent resultIntent = new Intent(this, MainActivityAfterLogin.class);
        resultIntent.putExtra("LOGINCONFIG", "LOGIN");
        resultIntent.putExtra(Constant.VALUE_SHEDULETRACK, "scheduleTrack");
        resultIntent.putExtra(Constant.VALUE_APPNUMBERFROMGRAPH, data_idforgraph);
        TaskStackBuilder TSB = TaskStackBuilder.create(self);
        TSB.addParentStack(NewFeedMainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        TSB.addNextIntent(resultIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent resultPendingIntent =
                    TSB.getPendingIntent(
                            0,
                            PendingIntent.FLAG_IMMUTABLE
                    );

            nb.setContentIntent(resultPendingIntent);
            nb.setAutoCancel(true);
            NotificationManager mNotificationManager =
                    (NotificationManager) self.getSystemService(Context.NOTIFICATION_SERVICE);
            // mId allows you to update the notification later on.
            mNotificationManager.notify(11221, nb.build());
        } else {
            PendingIntent resultPendingIntent =
                    TSB.getPendingIntent(
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            nb.setContentIntent(resultPendingIntent);
            nb.setAutoCancel(true);
            NotificationManager mNotificationManager =
                    (NotificationManager) self.getSystemService(Context.NOTIFICATION_SERVICE);
            // mId allows you to update the notification later on.
            mNotificationManager.notify(11221, nb.build());
        }

    }

}