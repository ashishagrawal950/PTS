package com.pts.invetech.myscedulegraph.model;

/**
 * Created by vaibhav on 24/5/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyScheduleDetailData {

    @SerializedName("block")
    @Expose
    private Integer block;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("p_qtm")
    @Expose
    private String pQtm;
    @SerializedName("c_qtm")
    @Expose
    private String cQtm;

    public Integer getBlock() {
        return block;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPQtm() {
        return pQtm;
    }

    public void setPQtm(String pQtm) {
        this.pQtm = pQtm;
    }

    public String getCQtm() {
        return cQtm;
    }

    public void setCQtm(String cQtm) {
        this.cQtm = cQtm;
    }

}