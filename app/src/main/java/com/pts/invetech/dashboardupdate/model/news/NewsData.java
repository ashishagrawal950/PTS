
package com.pts.invetech.dashboardupdate.model.news;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NewsData implements Parcelable{

    @SerializedName("Power")
    @Expose
    private ArrayList<SubNews> power = null;
    @SerializedName("Coal")
    @Expose
    private ArrayList<SubNews> coal = null;
    @SerializedName("Renewable energy")
    @Expose
    private ArrayList<SubNews> renewableEnergy = null;
    @SerializedName("Nuclear")
    @Expose
    private ArrayList<SubNews> nuclear = null;

    public ArrayList<SubNews> getPower() {
        return power;
    }

    public void setPower(ArrayList<SubNews> power) {
        this.power = power;
    }

    public ArrayList<SubNews> getCoal() {
        return coal;
    }

    public void setCoal(ArrayList<SubNews> coal) {
        this.coal = coal;
    }

    public ArrayList<SubNews> getRenewableEnergy() {
        return renewableEnergy;
    }

    public void setRenewableEnergy(ArrayList<SubNews> renewableEnergy) {
        this.renewableEnergy = renewableEnergy;
    }

    public ArrayList<SubNews> getNuclear() {
        return nuclear;
    }

    public void setNuclear(ArrayList<SubNews> nuclear) {
        this.nuclear = nuclear;
    }

    public List<SubNews> getFullNews(){
        List<SubNews> fullList=new ArrayList<>();
        fullList.addAll(getPower());
        fullList.addAll(getCoal());
        fullList.addAll(getRenewableEnergy());
        fullList.addAll(getNuclear());

        return fullList;
    }


    protected NewsData(Parcel in) {
        if (in.readByte() == 0x01) {
            power = new ArrayList<SubNews>();
            in.readList(power, SubNews.class.getClassLoader());
        } else {
            power = null;
        }
        if (in.readByte() == 0x01) {
            coal = new ArrayList<SubNews>();
            in.readList(coal, SubNews.class.getClassLoader());
        } else {
            coal = null;
        }
        if (in.readByte() == 0x01) {
            renewableEnergy = new ArrayList<SubNews>();
            in.readList(renewableEnergy, SubNews.class.getClassLoader());
        } else {
            renewableEnergy = null;
        }
        if (in.readByte() == 0x01) {
            nuclear = new ArrayList<SubNews>();
            in.readList(nuclear, SubNews.class.getClassLoader());
        } else {
            nuclear = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (power == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(power);
        }
        if (coal == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(coal);
        }
        if (renewableEnergy == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(renewableEnergy);
        }
        if (nuclear == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(nuclear);
        }
    }

    @SuppressWarnings("unused")
    public static final Creator<NewsData> CREATOR = new Creator<NewsData>() {
        @Override
        public NewsData createFromParcel(Parcel in) {
            return new NewsData(in);
        }

        @Override
        public NewsData[] newArray(int size) {
            return new NewsData[size];
        }
    };

}
