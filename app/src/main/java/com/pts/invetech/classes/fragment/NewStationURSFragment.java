package com.pts.invetech.classes.fragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseFragment;
import com.pts.invetech.APICalling.NetworkManagerFragment;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.utils.AppLogger;
import com.pts.model.stationmain.StationUrsMain;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NewStationURSFragment extends AppBaseFragment implements AdapterView.OnItemSelectedListener {

    private static final String URL_STATIONURS = "https://www.mittalpower.com/mobile/pxs_app/service/newurs/getursdetailsstationwise.php";
    private String device_id;
    private String state_id_array_fromdb, state_name_name_fromdb, state_id_fromdb, region_name = "", from;
    private String available, typeSend, domain;
    private SQLiteDatabase db;
    private LinearLayout openLayout;
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext, buttonClick, tvnodatafound, tvnodata, tvdate;
    private ListView list_view_main;
    private Spinner dropdwn, spn_ntpc;
    private JSONObject networkObject;
    private ArrayAdapter<String> adapter_state;
    private ArrayList<String> select_array = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = getActivity().openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientid(clientid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationnewsfeed(newMessage TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS gpsloaction(latitude VARCHAR, longitude VARCHAR, state_id VARCHAR, state_serial VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS availableurs(region VARCHAR, available VARCHAR, select_yes VARCHAR, revision VARCHAR, region_id VARCHAR);");
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }

        Cursor cfrom = db.rawQuery("SELECT * FROM gpsloaction", null);
        if (cfrom.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        while (cfrom.moveToNext()) {
            state_id_array_fromdb = cfrom.getString(0);
            state_name_name_fromdb = cfrom.getString(1);
            state_id_fromdb = cfrom.getString(2);
            typeSend = "STATE";
        }

        Cursor ursfrom = db.rawQuery("SELECT * FROM availableurs", null);
        if (ursfrom.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        while (ursfrom.moveToNext()) {
            region_name = ursfrom.getString(0);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
        }
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        /*
         * {"device_id":867935024394966,"type":"REGION","region":"NRLDC","owner":"NTPCCOAL"}
         * */
        networkObject = new JSONObject();
        try {
            networkObject.put("device_id", device_id);
            networkObject.put("type", "REGION");
            networkObject.put("region", "NRLDC");
            networkObject.put("owner", "NTPCCOAL");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new NetworkManagerFragment(this, networkObject, domain + "/mobile/pxs_app/service/newurs/getursdetailsstationwise.php").execute();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_station_urs, container,
                false);
        LinearLayout openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivityAfterLogin) getActivity()).open();
            }
        });

        lltoplogin = rootView.findViewById(R.id.lltoplogin);
        lltoplogout = rootView.findViewById(R.id.lltoplogout);
        tvlogintext = rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = rootView.findViewById(R.id.tvlogouttext);
        lltoplogin.setVisibility(View.GONE);
        lltoplogout.setVisibility(View.VISIBLE);
        tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
        tvlogouttext.setText("Home");
        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(in);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);

            }
        });


        list_view_main = rootView.findViewById(R.id.list_view_main);
        tvnodatafound = rootView.findViewById(R.id.tvnodatafound);
        tvnodata = rootView.findViewById(R.id.tvnodata);
        dropdwn = rootView.findViewById(R.id.dropdwn);
        dropdwn.setOnItemSelectedListener(this);
        spn_ntpc = rootView.findViewById(R.id.spn_ntpc);
        spn_ntpc.setOnItemSelectedListener(this);
        tvdate = rootView.findViewById(R.id.tvdate);

        ConnectionDetector cd = new ConnectionDetector(getActivity());
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getActivity(), "Please Check your network.",
                    Toast.LENGTH_LONG).show();
        } else {
            if (region_name.length() == 0) {
                new NetworkManagerFragment(this, networkObject, domain + "/mobile/pxs_app/service/newurs/getursdetailsstationwise.php").execute();
            } else {
                from = "dropdown";
                new NetworkManagerFragment(this, networkObject, domain + "/mobile/pxs_app/service/newurs/getursdetailsstationwise.php").execute();
            }
        }
        new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
				/*ConnectionDetector cd = new ConnectionDetector(getActivity());
				if (!cd.isConnectingToInternet()) {
					Toast.makeText(getActivity(), "Please Check your network.",
							Toast.LENGTH_LONG).show();
				} else {*/
                if (isVisible()) {
                    Log.e("newsfeed", "in side counter");
                    region_name = dropdwn.getSelectedItem().toString();
                    from = "counter";
                    new NetworkManagerFragment(this, networkObject, domain + "/mobile/pxs_app/service/newurs/getursdetailsstationwise.php").execute();
                }
                Log.e("newsfeed", "out side counter");
                //}
            }
        }.start();
        setNtpcCoalSpinner(rootView);
        return rootView;
    }

    private void setNtpcCoalSpinner(View rootView) {
        List<String> categories = new ArrayList<String>();
        categories.add("Gas");
        categories.add("Coal");
        categories.add("Other");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_ntpc.setAdapter(dataAdapter);
    }

    @Override
    public void updateStringResult(String string) {
        super.updateStringResult(string);
        if (string == null) {
            AppLogger.showMsgWithoutTag("No Data Found or Some error occurred");
            return;
        }
        StationUrsMain stationUrsMain = new StationUrsMain();
        setData(stationUrsMain);


    }

    private void setData(StationUrsMain stationUrsMain) {

        if (from.equalsIgnoreCase("counter")) {

        } else if (from.equalsIgnoreCase("dropdown")) {

            List<String> categories = new ArrayList<String>();
            categories.add("ERLDC");
            categories.add("NRLDC");
            categories.add("SRLDC");
            categories.add("WRLDC");
            categories.add("NERLDC");

            adapter_state = new ArrayAdapter<String>(
                    getActivity(),
                    R.layout.spinner_item_state,
                    categories);
            adapter_state
                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dropdwn.setAdapter(adapter_state);
            select_array.indexOf("YES");
            dropdwn.setSelection((select_array.indexOf("YES")));
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
