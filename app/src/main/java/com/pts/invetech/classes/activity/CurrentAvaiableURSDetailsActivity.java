package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.URSdetailsGeneratorwiseAPIResponse;
import com.pts.invetech.classes.fragment.CurrentAvaiableURSFragment;
import com.pts.invetech.customlist.URSGeneratorWisenameCustomList;
import com.pts.invetech.pojo.AvaiableURS;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.JSONUtils;
import com.pts.invetech.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;

public class CurrentAvaiableURSDetailsActivity extends Activity {
    private SQLiteDatabase db;
    private Dialog prgDialog;
    private String station_name_Send, available_Send;
    private String available, generator_name, region, revision, fromtime, totime, disable;
    private ArrayList<String> serialarray = new ArrayList<String>();
    private ArrayList<String> availablearray = new ArrayList<String>();
    private ArrayList<String> fromtimearray = new ArrayList<String>();
    private ArrayList<String> totimearray = new ArrayList<String>();
    private ArrayList<String> disablearray = new ArrayList<String>();
    private TextView tvgenertorname, tvtotal_amount;
    private ListView lv_more;
    private String device_id;
    private LinearLayout openLayout;
    private ProgressBar pbprogressBar;
    private String from,domain;

    private CurrentAvaiableURSFragment currentAvaiableURSFragment = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_currentavaiableursdetail);

        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
		 /*prgDialog = new ProgressDialog(CurrentAvaiableURSDetailsActivity.this);
	     prgDialog.setMessage("Please wait...");
	     prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        openLayout = (LinearLayout) findViewById(R.id.openLayout);
        tvgenertorname = (TextView) findViewById(R.id.tvgenertorname);
        tvtotal_amount = (TextView) findViewById(R.id.tvtotal_amount);
        pbprogressBar = (ProgressBar) findViewById(R.id.pbprogressBar);


        Intent in = getIntent();
        station_name_Send = in.getStringExtra("station_name_Send");
        available_Send = in.getStringExtra("available_Send");
        tvgenertorname.setText(station_name_Send);
        tvtotal_amount.setText(available_Send);
        lv_more = (ListView) findViewById(R.id.lv_more);
        from = "start";

        ConnectionDetector cd = new ConnectionDetector(CurrentAvaiableURSDetailsActivity.this);
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(CurrentAvaiableURSDetailsActivity.this, "Please Check your network.",
                    Toast.LENGTH_LONG).show();
        } else {
            new HttpAsyncTaskgetursdata_generatorwise().execute(domain + "/mobile/pxs_app/service/getursdata_generatorwise.php");
        }


        openLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
		
		/*new CountDownTimer(120000, 1000) {
			public void onTick(long millisUntilFinished) {
				// mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
			}
			public void onFinish() {
				from = "counter";
					new HttpAsyncTaskgetursdata_generatorwise().execute("http://www.mittalpower.com/mobile/pxs_app/service/getursdata_generatorwise.php");		    		 
			}
		}.start();*/

    }

    private class HttpAsyncTaskgetursdata_generatorwise extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AvaiableURS avaiableURS = new AvaiableURS();
            avaiableURS.setName(station_name_Send);
            avaiableURS.setDevice_id(device_id);
            return URSdetailsGeneratorwiseAPIResponse.POST(urls[0], avaiableURS);
        }

        @Override
        protected void onPreExecute() {
            if (from.equalsIgnoreCase("counter")) {
                pbprogressBar.setVisibility(View.VISIBLE);
            } else if (from.equalsIgnoreCase("start")) {
                prgDialog.show();
            }
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            AppLogger.showMsg("Get Request For Demo Result:", result);
            if (from.equalsIgnoreCase("counter")) {
                pbprogressBar.setVisibility(View.INVISIBLE);
            } else if (from.equalsIgnoreCase("start")) {
                prgDialog.hide();
            }
            //Toast.makeText(getBaseContext(), "result" + result, Toast.LENGTH_LONG).show();
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    availablearray.clear();
                    fromtimearray.clear();
                    totimearray.clear();
                    disablearray.clear();
                    JSONObject mainObject = new JSONObject(result);
//				AppLogger.showMsg("result",result);
                    if (mainObject.has("generator_name")) {
                        generator_name = mainObject.getString("generator_name");
                    }
                    if (mainObject.has("region")) {
                        region = mainObject.getString("region");
                    }
                    if (mainObject.has("revision")) {
                        revision = mainObject.getString("revision");
                    }
                    int i;
                    JSONArray data = mainObject.getJSONArray("data");
                    for (i = 0; i < data.length(); i++) {
                        JSONObject datadata = data.getJSONObject(i);
                        if (datadata.has("available")) {
                            available = datadata.getString("available");
                        }
                        if (datadata.has("fromtime")) {
                            fromtime = datadata.getString("fromtime");
                        }
                        if (datadata.has("totime")) {
                            totime = datadata.getString("totime");
                        }
                        if (datadata.has("disable")) {
                            disable = datadata.getString("disable");
                        }
                        int counting = (i + 1);
                        String j = String.valueOf(counting);
                        serialarray.add(j);
                        availablearray.add(available);
                        fromtimearray.add(fromtime);
                        totimearray.add(totime);
                        disablearray.add(disable);
                    }
                    URSGeneratorWisenameCustomList adapter = new URSGeneratorWisenameCustomList(CurrentAvaiableURSDetailsActivity.this,
                            serialarray, fromtimearray, totimearray, availablearray, disablearray);
                    lv_more.setAdapter(adapter);
                    Utility.setListViewHeightBasedOnChildren(lv_more);
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(CurrentAvaiableURSDetailsActivity.this, errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(CurrentAvaiableURSDetailsActivity.this, "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(CurrentAvaiableURSDetailsActivity.this, "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(CurrentAvaiableURSDetailsActivity.this, "Invalid JSON: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }


}
