package com.pts.invetech.dashboardupdate.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pts.invetech.R;
import com.pts.invetech.classes.activity.NewsFeedActivityWebView;
import com.pts.invetech.dashboardupdate.adapters.SubNewsAdapter;
import com.pts.invetech.dashboardupdate.model.news.SubNews;
import com.pts.invetech.utils.AppLogger;

import java.util.List;

public class SubNewsFragment extends Fragment implements AdapterView.OnItemClickListener {

    private List<SubNews> coalNews;
    String loginconfig;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null){
            coalNews=getArguments().getParcelableArrayList("data");
        }

        Intent in = getActivity().getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");
       /* if(loginconfig.equalsIgnoreCase("LOGIN"))
        {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        }else {
            loginconfig.length();
            //String companyname = loginconfig.substring(0, 6);
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View rowView = inflater.inflate(R.layout.new_fragment_coal_news, container, false);
        setListView(rowView);
        return rowView;
    }

    private void setListView(View rowView) {
        if(coalNews==null){
            AppLogger.showToastShort(getActivity(),"No News To Show.");
            return;
        }
        ListView lView= (ListView) rowView.findViewById(R.id.newslist);
        SubNewsAdapter adapter=new SubNewsAdapter(getActivity(),coalNews);
        lView.setAdapter(adapter);
        lView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        SubNews data=coalNews.get(i);

        /*Intent intent=new Intent(getActivity(), NewsDeatilWebView.class);
        intent.putExtra("weburl",data.getLink());
        startActivity(intent);*/
        Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
        in.putExtra("link_key_element", data.getLink());
        in.putExtra("LOGINCONFIG",loginconfig);
        startActivity(in);
    }

}