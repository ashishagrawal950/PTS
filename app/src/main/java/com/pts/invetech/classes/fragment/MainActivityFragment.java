package com.pts.invetech.classes.fragment;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.R;
import com.pts.invetech.classes.activity.DownloadAgainActivity;
import com.pts.invetech.classes.activity.HomeActivity;
import com.pts.invetech.classes.activity.MarketPriceActivity;
import com.pts.invetech.classes.activity.NewBidActivity;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.activity.NewsLetterActivity;
import com.pts.invetech.classes.activity.NoBidActivity;
import com.pts.invetech.classes.activity.NotificationActivity;
import com.pts.invetech.classes.activity.ReportActivity;
import com.pts.invetech.classes.activity.ScheduleVsCurtailmentActivity;
import com.pts.invetech.classes.activity.SplashScreenActivity;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.customlist.CustomAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MainActivityFragment extends Fragment {
    static ArrayList<String> menu_jsonarray = new ArrayList<String>();
    static ArrayList<String> complete_attendancestaff_namehomearray = new ArrayList<String>();
    static ArrayList<Integer> complete_attendancestaff_imghomearraylist = new ArrayList<Integer>();
    // Context context;;
    private SQLiteDatabase db;
    private String name, pass, companyName, domain,newsletterBaseUrl;
    private Dialog prgDialog;
    private LinearLayout openLayout, openPopup;
    private TextView tvCompanyName, tvtimerview, tvnotificationicon,
            tvremainingtime;
    private Editor editor;
    private GridView gridView;
    private int heightGrid;
    private int countnot;
    private LinearLayout lltoplogout;
    private View rootView;

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_main);*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_main, container, false);
        SharedPreferences pref = getActivity().getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        editor = pref.edit();
		/*prgDialog = new ProgressDialog(getActivity());
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        openPopup = (LinearLayout) rootView.findViewById(R.id.openPopup);
        tvCompanyName = (TextView) rootView.findViewById(R.id.tvCompanyName);
        tvtimerview = (TextView) rootView.findViewById(R.id.tvtimerview);
        tvremainingtime = (TextView) rootView.findViewById(R.id.tvremainingtime);


        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        heightGrid = size.y;
        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivityAfterLogin) getActivity()).open();
            }
        });

        db = getActivity().openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
        if (loginc.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        StringBuffer logincbuffer = new StringBuffer();
        while (loginc.moveToNext()) {
            logincbuffer.append("loginc: " + loginc.getString(0) + "\n");
            name = loginc.getString(0);
            pass = loginc.getString(1);
            // Toast.makeText(getApplicationContext(), "name:" + name,
            // Toast.LENGTH_LONG).show();
        }
        newsletterBaseUrl = SharedPrefHandler.getDeviceString(getActivity(), "newsletterBaseUrl");
        domain = getArguments().getString("domain");
//        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        Cursor companynamec = db.rawQuery("SELECT * FROM companyname", null);
        if (companynamec.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        StringBuffer companynamecbuffer = new StringBuffer();
        while (companynamec.moveToNext()) {
            companynamecbuffer.append("companynamecbuffer: "
                    + companynamec.getString(0) + "\n");
            companyName = companynamec.getString(0);
            // Toast.makeText(getApplicationContext(), "companyName:" +
            // companyName, Toast.LENGTH_LONG).show();
        }
        tvCompanyName.setText(companyName);


        Cursor menuTablec = db.rawQuery("SELECT * FROM menuTable", null);
        if (menuTablec.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        StringBuffer menuTablecbuffer = new StringBuffer();
        menu_jsonarray.clear();
        while (menuTablec.moveToNext()) {
            menuTablecbuffer.append("menuTablecbuffer: " + menuTablec.getString(0) + "\n");
            String menucaption = menuTablec.getString(0);
            menu_jsonarray.add(menucaption);
        }
        menu_jsonarray.add("Curtailment");
        //menu_jsonarray.add("Support");
        if(!newsletterBaseUrl.equalsIgnoreCase("null")){
        menu_jsonarray.add("News Letter");}
        menu_jsonarray.add("News Update");
        //menu_jsonarray.add("Logout");

        complete_attendancestaff_namehomearray.clear();
        complete_attendancestaff_imghomearraylist.clear();

        if (menu_jsonarray.contains("Dashboard")) {
            complete_attendancestaff_namehomearray.add("Dashboard");
            complete_attendancestaff_imghomearraylist.add(R.drawable.newhomedashboard);
        }

        if (menu_jsonarray.contains("Place New Bid")) {
            complete_attendancestaff_namehomearray.add("Place New Bid");
            complete_attendancestaff_imghomearraylist.add(R.drawable.newhomenewbid);
        }

        if (menu_jsonarray.contains("No Bid")) {
            complete_attendancestaff_namehomearray.add("No Bid");
            complete_attendancestaff_imghomearraylist.add(R.drawable.newhomenobid);
        }

        if (menu_jsonarray.contains("Market Price")) {
            complete_attendancestaff_namehomearray.add("Market Price");
            complete_attendancestaff_imghomearraylist.add(R.drawable.newhomemarketprice);
        }

        if (menu_jsonarray.contains("Downloads")) {
            complete_attendancestaff_namehomearray.add("Bills & Reports");
            complete_attendancestaff_imghomearraylist.add(R.drawable.newhomedownload);
        }

        if (menu_jsonarray.contains("Payment Details")) {
            complete_attendancestaff_namehomearray.add("Account & Finance");
            complete_attendancestaff_imghomearraylist.add(R.drawable.newhomepayment_details);
        }
//        if (menu_jsonarray.contains("Notifications")) {
//            complete_attendancestaff_namehomearray.add("Notifications");
//            complete_attendancestaff_imghomearraylist.add(R.drawable.newhomenotification);
//        }

        if (menu_jsonarray.contains("Curtailment")) {
            complete_attendancestaff_namehomearray.add("Curtailment");
            complete_attendancestaff_imghomearraylist.add(R.drawable.newhomecut);
        }
		
		/*if(menu_jsonarray.contains("Support")){
			complete_attendancestaff_namehomearray.add("Support");
			complete_attendancestaff_imghomearraylist.add(R.drawable.newhomesupport);
		}*/

        if (menu_jsonarray.contains("News Letter")) {
            complete_attendancestaff_namehomearray.add("News Letter");
            complete_attendancestaff_imghomearraylist.add(R.drawable.newhomenewsletter);
        }

        if (menu_jsonarray.contains("News Update")) {
            complete_attendancestaff_namehomearray.add("News Update");
            complete_attendancestaff_imghomearraylist.add(R.drawable.newhomenews_update);
        }
		
		/*if(menu_jsonarray.contains("Logout")){
			complete_attendancestaff_namehomearray.add("Logout");
			complete_attendancestaff_imghomearraylist.add(R.drawable.newhomelogout);
		}*/

        gridView = (GridView) rootView.findViewById(R.id.gridView);
        gridView.setAdapter(new CustomAdapter(getActivity(), complete_attendancestaff_namehomearray, complete_attendancestaff_imghomearraylist));
        ViewGroup.LayoutParams layoutParams = gridView.getLayoutParams();
        layoutParams.height = heightGrid - 150; //this is in pixels
        gridView.setLayoutParams(layoutParams);
        gridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                // Toast.makeText(MainActivity.this, "" + complete_attendancestaff_namearray.get(position), Toast.LENGTH_SHORT).show();

                if (complete_attendancestaff_namehomearray.get(position).equalsIgnoreCase("Dashboard")) {
                    Intent in = new Intent(getActivity(), HomeActivity.class);
                    in.putExtra("companyName", companyName);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                }

                if (complete_attendancestaff_namehomearray.get(position).equalsIgnoreCase("Place New Bid")) {
                    Intent in = new Intent(getActivity(), NewBidActivity.class);
                    in.putExtra("name", "");
                    in.putExtra("from", "MainActivity");
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                }

                if (complete_attendancestaff_namehomearray.get(position).equalsIgnoreCase("No Bid")) {
                    Intent in = new Intent(getActivity(), NoBidActivity.class);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                }
                if (complete_attendancestaff_namehomearray.get(position).equalsIgnoreCase("Market Price")) {
                    Intent in = new Intent(getActivity(),
                            MarketPriceActivity.class);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                }
                if (complete_attendancestaff_namehomearray.get(position).equalsIgnoreCase("Bills & Reports")) {
                    Intent in = new Intent(getActivity(),
                            DownloadAgainActivity.class);
                    in.putExtra("lastdate", "");
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                }

                if (complete_attendancestaff_namehomearray.get(position).equalsIgnoreCase("Account & Finance")) {
                    Intent in = new Intent(getActivity(), ReportActivity.class);
                    in.putExtra("from", "MainActivity");
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                }

                if (complete_attendancestaff_namehomearray.get(position).equalsIgnoreCase("Notifications")) {
                    db = getActivity().openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                    db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
                    db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                    db.execSQL("DROP TABLE IF EXISTS pushnotificationUnRead");
                    db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                    Cursor c = db.rawQuery("SELECT * FROM pushnotification", null);
                    db.execSQL("DROP TABLE IF EXISTS notificationcount");
                    db.execSQL("CREATE TABLE IF NOT EXISTS notificationcount(notificationcount VARCHAR);");
                    Intent in = new Intent(getActivity(),
                            NotificationActivity.class);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                }

                if (complete_attendancestaff_namehomearray.get(position).equalsIgnoreCase("Curtailment")) {
                    Intent in = new Intent(getActivity(),
                            ScheduleVsCurtailmentActivity.class);
                    in.putExtra("revision", "revision");
                    in.putExtra("date", "date");
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                }
				
				/*if (complete_attendancestaff_namehomearray.get(position).equalsIgnoreCase("Support")) {
					Intent in = new Intent(MainActivity.this, SupportActivity.class);
					startActivity(in);
					overridePendingTransition(R.anim.animation, R.anim.animation2);
					finish();
				}*/

                if (complete_attendancestaff_namehomearray.get(position).equalsIgnoreCase("News Letter")) {

                    Intent in = new Intent(getActivity(),
                            NewsLetterActivity.class);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                }

                if (complete_attendancestaff_namehomearray.get(position).equalsIgnoreCase("News Update")) {
                    Intent in = new Intent(getActivity(),
                            NewFeedMainActivity.class);
                    in.putExtra("LOGINCONFIG", companyName);
                    in.putExtra("revision", "revision");
                    in.putExtra("date", "date");
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                }
				
				/*if (complete_attendancestaff_namehomearray.get(position).equalsIgnoreCase("Logout")) {
					db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
					// db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
					db.execSQL("DROP TABLE IF EXISTS baby");
					db.execSQL("DROP TABLE IF EXISTS logininfo");
					db.execSQL("DROP TABLE IF EXISTS companyname");
					db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
					db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
					db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
					Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
					startActivity(i);
					overridePendingTransition(R.anim.animation, R.anim.animation2);
					finish();
				}*/

            }
        });


        Cursor lastbidtimeTablec = db.rawQuery("SELECT * FROM lastbidtimeTable", null);
        if (lastbidtimeTablec.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        StringBuffer lastbidtimeTablecbuffer = new StringBuffer();
        while (lastbidtimeTablec.moveToNext()) {
            lastbidtimeTablecbuffer.append("lastbidtimeTablecbuffer: " + lastbidtimeTablec.getString(0) + "\n");
            String lastBidTime = lastbidtimeTablec.getString(0);

            String[] partsapi = lastBidTime.split(":");
            String datefromapi = partsapi[0];
            String timefromapi = partsapi[1]; // 004


            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());
            SimpleDateFormat df = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm");
            String formattedDate = df.format(c.getTime());
            // formattedDate have current date/time
            String[] parts = formattedDate.split(" ");
            String datefrom = parts[0];
            String timefrom = parts[1]; // 004

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            Date startDate = null, endDate = null;
            try {
                endDate = simpleDateFormat.parse(datefromapi + ":" + timefromapi);
                startDate = simpleDateFormat.parse(timefrom);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            long difference = endDate.getTime() - startDate.getTime();
            if (difference < 0) {
                Date dateMax = null, dateMin = null;
                try {
                    dateMax = simpleDateFormat.parse("24:00");
                    dateMin = simpleDateFormat.parse("00:00");
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                difference = (dateMax.getTime() - startDate.getTime()) + (endDate.getTime() - dateMin.getTime());
            }
            int days = (int) (difference / (1000 * 60 * 60 * 24));
            int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
            Log.i("log_tag", "Hours: " + hours + ", Mins: " + min);
            if (hours < 12) {

                tvremainingtime.setText("Bid Time Left for Today Trading: ");
                tvtimerview.setText(hours + ":"
                        + min + ":" + 00);
                final CounterClass timer = new CounterClass(
                        difference, 1000);
                timer.start();
            } else {
				/*String[] partssplit = datefrom.split("-");
				String dateyy = partssplit[0]; 
				String datemm = partssplit[1]; 
				String datedd = partssplit[2]; 
				int timedateyy = Integer.parseInt(dateyy);
				int timedatemm = Integer.parseInt(datemm);
				int timedatedd = Integer.parseInt(datedd);
				int timedateddbyone = timedatedd+1;*/
                Calendar ca = Calendar.getInstance();
                ca.add(Calendar.DATE, +1);
                System.out.println("Current time => " + ca.getTime());
                SimpleDateFormat dfa = new SimpleDateFormat(
                        "dd-MM-yyyy HH:mm:ss");
                String formattedDatea = dfa.format(ca.getTime());
                // formattedDate have current date/time
                String[] partsa = formattedDatea.split(" ");
                String part1 = partsa[0]; // 004
                String part2 = partsa[1]; // 004
                //String tomorrowdate = (timedateddbyone+"-"+datemm+"-"+timedateyy);
                tvremainingtime.setText("Bid Time Left for Tomorrow (" + part1 + ") Trading: ");
                //tvremainingtime.setText("Bid Time Left for Tomorrow Trading: ");
                tvtimerview.setText(hours + ":"
                        + min + ":" + 00);
                final CounterClass timer = new CounterClass(
                        difference, 1000);
                timer.start();
            }
        }
		
		/*openLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				backButtonHandler();
			}
		});*/


        tvnotificationicon = (TextView) rootView.findViewById(R.id.tvnotificationicon);
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
        Cursor cNot = db.rawQuery("SELECT * FROM pushnotificationUnRead", null);
        Cursor cnot = db.rawQuery("SELECT * FROM notificationcount", null);
        if (cnot.getCount() == 0) {
            // showMessage("Error", "No records found");
            countnot = 0;
        }
        StringBuffer cnotbuffer = new StringBuffer();
        while (cnot.moveToNext()) {
            logincbuffer.append("loginc: " + cnot.getString(0) + "\n");
            String countnotification = cnot.getString(0);
            countnot = Integer.parseInt(countnotification);
        }
        // goMessage.setText("All Notification:" + c.getCount());
        if (countnot == 0) {
            int coutning = (cNot.getCount() + countnot);
            if (coutning == 0) {
                openPopup.setVisibility(View.INVISIBLE);
            } else {
                String count = Integer.toString(coutning);
                openPopup.setVisibility(View.VISIBLE);
                tvnotificationicon.setVisibility(View.VISIBLE);
                tvnotificationicon.setText(count);
            }
        } else {
            int coutning = countnot;
            if (coutning == 0) {
                openPopup.setVisibility(View.INVISIBLE);
            } else {
                String count = Integer.toString(coutning);
                openPopup.setVisibility(View.VISIBLE);
                tvnotificationicon.setVisibility(View.VISIBLE);
                tvnotificationicon.setText(count);
            }
        }


        openPopup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = getActivity().openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS pushnotificationUnRead");
                db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                Cursor c = db.rawQuery("SELECT * FROM pushnotification", null);
                db.execSQL("DROP TABLE IF EXISTS notificationcount");
                db.execSQL("CREATE TABLE IF NOT EXISTS notificationcount(notificationcount VARCHAR);");
                Intent in = new Intent(getActivity(),
                        NotificationActivity.class);
                startActivity(in);
                getActivity().finish();
            }
        });


        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = getActivity().openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
                Intent i = new Intent(getActivity(), SplashScreenActivity.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                getActivity().finish();
            }
        });

        return rootView;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    public void showMessage(String title, String message) {
        Builder builder = new Builder(getActivity());
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    public void onBackPressed() {
        // Display alert message when back button has been pressed
        backButtonHandler();
        return;
    }

    public void backButtonHandler() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                getActivity());
        // Setting Dialog Title
        // alertDialog.setTitle("Leave application?");
        // Setting Dialog Message
        alertDialog
                .setMessage("Are you sure you want to leave the application?");
        // Setting Icon to Dialog
        // alertDialog.setIcon(R.drawable.ic_launcher);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        dialog.cancel();
                    }
                });
        // Showing Alert Message
        alertDialog.show();
    }

    public void logoutUser() {
        editor.clear();
        editor.apply();
        Log.e("result", "app trminate");
        Log.e("result 1", "app trminate");
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        logoutUser();
        super.onDestroy();
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressLint("NewApi")
    public class CounterClass extends CountDownTimer {

        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            tvtimerview.setText("finished.");
        }

        @SuppressLint("NewApi")
        @TargetApi(Build.VERSION_CODES.GINGERBREAD)
        @Override
        public void onTick(long millisUntilFinished) {

            long millis = millisUntilFinished;
            String hms = String.format(
                    "%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis)
                            - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
                            .toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis)
                            - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
                            .toMinutes(millis)));
            System.out.println(hms);

            tvtimerview.setText(hms);
        }
    }
}