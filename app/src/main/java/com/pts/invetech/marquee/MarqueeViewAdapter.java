package com.pts.invetech.marquee;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pts.invetech.R;

import java.util.ArrayList;

/**
 * Created by Vaibhav on 09-08-2016.
 */

public class MarqueeViewAdapter extends RecyclerView.Adapter<MarqueeViewAdapter.ViewHolder> {

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView firstView, secondView, thirdView;

        public ViewHolder(View v) {
            super(v);
            firstView = (TextView) v.findViewById(R.id.first);
            secondView = (TextView) v.findViewById(R.id.second);
            thirdView = (TextView) v.findViewById(R.id.tvthird);
        }
    }


    private ArrayList<String> region_name_array;
    private ArrayList<String> available_array;
    private ArrayList<String> revision_array;
    private boolean isData = true;

    public MarqueeViewAdapter(ArrayList<String> region,
                              ArrayList<String> avail, ArrayList<String> revision) {

        region_name_array = region;
        available_array = avail;
        revision_array = revision;

        if (region == null || region.isEmpty()) {
            isData = false;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tmp_file, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (isData) {
            position = position % region_name_array.size();
            holder.firstView.setText(region_name_array.get(position));
            holder.secondView.setText(available_array.get(position));
            holder.thirdView.setText(revision_array.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }
}