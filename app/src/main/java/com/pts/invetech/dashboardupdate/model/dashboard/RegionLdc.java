package com.pts.invetech.dashboardupdate.model.dashboard;

/**
 * Created by vaibhav on 12/1/17.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pts.invetech.utils.AppLogger;

public class RegionLdc implements Parcelable{


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("generation")
    @Expose
    private String generation;
    @SerializedName("demand")
    @Expose
    private String demand;
    @SerializedName("sch_drawal")
    @Expose
    private String schDrawal;
    @SerializedName("act_drawal")
    @Expose
    private String actDrawal;
    @SerializedName("deviation")
    @Expose
    private String deviation;
    @SerializedName("time")
    @Expose
    private String time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGeneration() {
        if(generation.isEmpty()){
            return "-";
        }
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public String getDemand() {
        return demand;
    }

    public void setDemand(String demand) {
        this.demand = demand;
    }

    public String getSchDrawal() {
        return schDrawal;
    }

    public void setSchDrawal(String schDrawal) {
        this.schDrawal = schDrawal;
    }

    public String getActDrawal() {
        return actDrawal;
    }

    public void setActDrawal(String actDrawal) {
        this.actDrawal = actDrawal;
    }

    public String getDeviation() {
        return deviation;
    }

    public void setDeviation(String deviation) {
        this.deviation = deviation;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    protected RegionLdc(Parcel in) {
        id = in.readString();
        date = in.readString();
        type = in.readString();
        state = in.readString();
        generation = in.readString();
        demand = in.readString();
        schDrawal = in.readString();
        actDrawal = in.readString();
        deviation = in.readString();
        time = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(date);
        dest.writeString(type);
        dest.writeString(state);
        dest.writeString(generation);
        dest.writeString(demand);
        dest.writeString(schDrawal);
        dest.writeString(actDrawal);
        dest.writeString(deviation);
        dest.writeString(time);
    }

    @SuppressWarnings("unused")
    public static final Creator<RegionLdc> CREATOR = new Creator<RegionLdc>() {
        @Override
        public RegionLdc createFromParcel(Parcel in) {
            return new RegionLdc(in);
        }

        @Override
        public RegionLdc[] newArray(int size) {
            return new RegionLdc[size];
        }
    };

}