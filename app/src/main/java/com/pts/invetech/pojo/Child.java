package com.pts.invetech.pojo;

/**
 * Created by Ashish on 03-11-2016.
 */

public class Child {

    private String name;
    private String fileAddress;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileAddress() {
        return fileAddress;
    }

    public void setFileAddress(String fileAddress) {
        this.fileAddress = fileAddress;
    }
}
