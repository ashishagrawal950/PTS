package com.pts.invetech.classes.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import com.google.gson.Gson;
import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.handler.NetworkIntentService;
import com.pts.handler.NotificationDataHandler;
import com.pts.handler.NotificationUiHandler;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseFragment;
import com.pts.invetech.NotificationSetting.SettingsToggle;
import com.pts.invetech.NotificationSetting.ToggleDataBridge;
import com.pts.invetech.R;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.dashboardupdate.views.DashBoardUpdate;
import com.pts.invetech.pojo.NotificationData;
import com.pts.invetech.utils.Constant;

import org.json.JSONException;
import org.json.JSONObject;

public class NotificationSettingFragment extends AppBaseFragment implements ToggleDataBridge {


    private LinearLayout openLayout;
    private SQLiteDatabase db;
    private LinearLayout lltoplogin, lltoplogout;
    private String loginconfig;
    private TextView tvlogintext, tvlogouttext;
    private View rootView;
    private NotificationData notificationData;
    private String deviceid, domain,url;
    private boolean isFirst = true;
    private NetworkCallBack objectCallBack = new NetworkCallBack() {

        @Override
        public void onResultObject(Object data, int id) {
            setDataObject(data, id);
        }
    };

    private static void sendData(Activity con, String url, String data) {
        Intent msgIntent = new Intent(con, NetworkIntentService.class);
        msgIntent.putExtra(Constant.INTENT_SERVICE_URL, url);
        msgIntent.putExtra(Constant.INTENT_SERVICE_DATA, data);
        con.startService(msgIntent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = getActivity().openOrCreateDatabase("deviceDBSecond",
                Context.MODE_PRIVATE, null);

        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientid(clientid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationnewsfeed(newMessage TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS gpsloaction(latitude VARCHAR, longitude VARCHAR, state_id VARCHAR, state_serial VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS availableurs(region VARCHAR, available VARCHAR, select_yes VARCHAR, revision VARCHAR, region_id VARCHAR);");
        TelephonyManager tManagerr = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        deviceid = tManagerr.getDeviceId();
        if(!isFirst){
            domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_notificationsetting, container, false);

        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NewFeedMainActivity) getActivity()).open();
            }
        });

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        Intent in = getActivity().getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");

        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        lltoplogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });


        setToggleBtns(rootView);
        try {
            networkData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rootView;
    }

    private void networkData() throws JSONException {
        String[] data = new String[2];
        JSONObject object = new JSONObject();
        object.put("device_id", deviceid);
        if(isFirst){
            data[0] = "https://www.mittalpower.com/mobile/pxs_app/service/notification/getnotificationsetting.php";
        }else{
            data[0] = domain + "/mobile/pxs_app/service/notification/getnotificationsetting.php";
        }
        data[1] = object.toString();
        new NetworkHandlerModel(getActivity(), objectCallBack, NotificationData.class, 1).execute(data);
    }

    private void setToggleBtns(View rootView) {

        SettingsToggle toggle = (SettingsToggle) rootView.findViewById(R.id.settingallnotif);
        toggle.setClickBridge(this);

        toggle = (SettingsToggle) rootView.findViewById(R.id.settingsbidnotif);
        toggle.setClickBridge(this);

        toggle = (SettingsToggle) rootView.findViewById(R.id.settingscurtailnotif);
        toggle.setClickBridge(this);

        toggle = (SettingsToggle) rootView.findViewById(R.id.settingsnewsnotif);
        toggle.setClickBridge(this);

        toggle = (SettingsToggle) rootView.findViewById(R.id.settingsnewsletter);
        toggle.setClickBridge(this);
    }

    @Override
    public void onToggleClick(View view, boolean click) {
        switch (view.getId()) {
            case R.id.settingallnotif:
                setSettingsAllNotification(click);
                break;
            case R.id.settingsbidnotif:
                setSettingBidNotification(click);
                break;
            case R.id.settingscurtailnotif:
                setSettingCurtainNotification(click);
                break;
            case R.id.settingsnewsnotif:
                setSettingNewsNotification(click);
                break;
            case R.id.settingsnewsletter:
                setSettingsNewsLetter(click);
                break;
        }
    }

    private void setSettingsAllNotification(boolean click) {
        if (click) {
            NotificationDataHandler.changeAllNotification(notificationData, "YES");
            allToggleOn();
        } else {
            NotificationDataHandler.changeAllNotification(notificationData, "NO");
            allToggleOff();
        }
        convert2Json();
    }

    private void setSettingsNewsLetter(boolean click) {
        if (click) {
            NotificationDataHandler.changeNewsLetter(notificationData, "YES");
        } else {
            NotificationDataHandler.changeNewsLetter(notificationData, "NO");
        }
        NotificationUiHandler.setUiNewsLetter(rootView, notificationData);
        convert2Json();
    }

    private void setSettingNewsNotification(boolean click) {
        if (click) {
            NotificationDataHandler.changeNewsNotification(notificationData, "YES");
        } else {
            NotificationDataHandler.changeNewsNotification(notificationData, "NO");
        }
        NotificationUiHandler.setUiNewsNotification(rootView, notificationData);
        convert2Json();
    }

    private void setSettingCurtainNotification(boolean click) {
        if (click) {
            NotificationDataHandler.changeCurtailment(notificationData, "YES");
        } else {
            NotificationDataHandler.changeCurtailment(notificationData, "NO");
        }
        NotificationUiHandler.setUiCurtailment(rootView, notificationData);
        convert2Json();
    }

    private void setSettingBidNotification(boolean click) {
        if (click) {
            NotificationDataHandler.changeBidNotification(notificationData, "YES");
        } else {
            NotificationDataHandler.changeBidNotification(notificationData, "NO");
        }
        NotificationUiHandler.setUiBidNotification(rootView, notificationData);
        convert2Json();
    }

    private void setDataObject(Object data, int id) {
        if (data == null) {
            return;
        }

        notificationData = (NotificationData) data;
        allToggleSet(notificationData);
    }

    private void allToggleSet(NotificationData data) {
        NotificationUiHandler.setAllNotification(rootView, data);
        NotificationUiHandler.setUiCurtailment(rootView, data);
        NotificationUiHandler.setUiBidNotification(rootView, data);
        NotificationUiHandler.setUiNewsNotification(rootView, data);
        NotificationUiHandler.setUiNewsLetter(rootView, data);
    }

    private void convert2Json() {

        try {
            Gson gson = new Gson();
            String jsonString = gson.toJson(notificationData);

            JSONObject request = new JSONObject(jsonString);

            JSONObject object = new JSONObject();
            object.put("device_id", deviceid);
            object.put("activity", request);
            if(isFirst){
                 url = "https://www.mittalpower.com/mobile/pxs_app/service/notification/updatenotificationsetting.php";
            }else{
                 url = domain + "/mobile/pxs_app/service/notification/updatenotificationsetting.php";
            }

            sendData(getActivity(), url, object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /*
        All Toggle Switch Methods
     */

    private void allToggleOff() {

        int[] idArr = {R.id.settingsbidnotif, R.id.settingscurtailnotif,
                R.id.settingsnewsnotif, R.id.settingsnewsletter};

        for (int index = 0; index < idArr.length; index++) {
            SettingsToggle toggle = (SettingsToggle) rootView.findViewById(idArr[index]);
            toggle.toggleOff();
        }
    }

    private void allToggleOn() {

        int[] idArr = {R.id.settingsbidnotif, R.id.settingscurtailnotif,
                R.id.settingsnewsnotif, R.id.settingsnewsletter};

        for (int index = 0; index < idArr.length; index++) {
            SettingsToggle toggle = (SettingsToggle) rootView.findViewById(idArr[index]);
            toggle.toggleOn();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashBoardUpdate dashBoardUpdate = new DashBoardUpdate();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content_frame, dashBoardUpdate)
                                .commit();
                    }
                    return true;
                }
                return false;
            }
        });
    }

}