package com.pts.model.blockqtm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BlockQtmApproval {

    @SerializedName("block")
    @Expose
    private String block;
    @SerializedName("qtm")
    @Expose
    private String qtm;

    /**
     *
     * @return
     * The block
     */
    public String getBlock() {
        return block;
    }

    /**
     *
     * @param block
     * The block
     */
    public void setBlock(String block) {
        this.block = block;
    }

    /**
     *
     * @return
     * The qtm
     */
    public String getQtm() {
        return qtm;
    }

    /**
     *
     * @param qtm
     * The qtm
     */
    public void setQtm(String qtm) {
        this.qtm = qtm;
    }

}