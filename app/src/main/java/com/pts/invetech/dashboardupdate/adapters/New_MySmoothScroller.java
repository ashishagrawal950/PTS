package com.pts.invetech.dashboardupdate.adapters;

import android.content.Context;
import android.graphics.PointF;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import android.util.DisplayMetrics;

/**
 * Created by Vaibhav on 09-08-2016.
 */
public class New_MySmoothScroller extends LinearSmoothScroller {

    private LinearLayoutManager mManager;
    private final float  MILLISECONDS_PER_INCH = 6000f;

    public New_MySmoothScroller(Context context, LinearLayoutManager manager){
        super(context);
        mManager=manager;
    }

    public New_MySmoothScroller(Context context, LinearLayoutManager manager,float milli){
        super(context);

        mManager=manager;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
        return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
    }

    @Override
    public PointF computeScrollVectorForPosition(int targetPosition) {
        return mManager.computeScrollVectorForPosition(targetPosition);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
