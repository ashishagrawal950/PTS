
package com.pts.invetech.dashboardupdate.model.news;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubNews implements Parcelable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("pubdate")
    @Expose
    private String pubdate;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("source")
    @Expose
    private String source;

    public SubNews(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPubdate() {
        return pubdate;
    }

    public void setPubdate(String pubdate) {
        this.pubdate = pubdate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    protected SubNews(Parcel in) {
        id = in.readString();
        title = in.readString();
        type = in.readString();
        link = in.readString();
        comment = in.readString();
        pubdate = in.readString();
        time = in.readString();
        source = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(type);
        dest.writeString(link);
        dest.writeString(comment);
        dest.writeString(pubdate);
        dest.writeString(time);
        dest.writeString(source);
    }

    @SuppressWarnings("unused")
    public static final Creator<SubNews> CREATOR = new Creator<SubNews>() {
        @Override
        public SubNews createFromParcel(Parcel in) {
            return new SubNews(in);
        }

        @Override
        public SubNews[] newArray(int size) {
            return new SubNews[size];
        }
    };

}
