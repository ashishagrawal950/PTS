
package com.pts.model.statewiseurs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StateWise {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("station")
    @Expose
    private Station station;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("revision")
    @Expose
    private Integer revision;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getRevision() {
        return revision;
    }

    public void setRevision(Integer revision) {
        this.revision = revision;
    }

}
