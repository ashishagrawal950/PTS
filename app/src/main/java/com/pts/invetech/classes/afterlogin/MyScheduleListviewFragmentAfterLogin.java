package com.pts.invetech.classes.afterlogin;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerList;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseFragment;
import com.pts.invetech.R;
import com.pts.invetech.adapters.AdapterMyScheduleListView;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.myscedulegraph.MySchedulingGraphActivity;
import com.pts.invetech.myscheduling.GetSchedulingData;
import com.pts.invetech.utils.AppDeviceUtils;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MyScheduleListviewFragmentAfterLogin extends AppBaseFragment {

    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private LinearLayout layout_one_myscheduling, layouttwo_myscheduling, listlayout, layout_region;
    private CardView cv_filter;
    private String deviceid;
    private Dialog prgDialog;
    private ListView list;
    private List<String> categories;
    private LinearLayout llregionandclientone, llregionandclienttwo, llregionandclientthree, llregionandclientfour, llregionandclientfive;

    private SQLiteDatabase db;
    private String dbacess, domain;
    private String data_id = "";

    private AdapterMyScheduleListView adapter;
    private NetworkCallBack networkCallBack = new NetworkCallBack() {

        @Override
        public void onResultObject(Object data, int id) {
            if (data == null) {
                return;
            }
            List<GetSchedulingData> scheduleList = (List<GetSchedulingData>) data;
            setFlowAdapter(scheduleList);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = getActivity().openOrCreateDatabase("deviceDBSecond",
                android.content.Context.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM baby", null);
        if (c.getCount() == 0) {

        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("access_key: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
//        domain = getArguments().getString("domain");
        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            data_id = extras.getString(Constant.VALUE_APPNUMBERFROMGRAPH);
        }

        if (dbacess == null) {
            Intent intent = new Intent(getActivity(), LoginInActivity.class);
            intent.putExtra("revision", "revision");
            intent.putExtra("date", "date");
            intent.putExtra(Constant.VALUE_FRMSCH, true);
            intent.putExtra(Constant.VALUE_APPNUMBERFROMGRAPH, data_id);
            startActivity(intent);

            getActivity().finish();
            return;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_myschedulelistview, container, false);
        LinearLayout openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivityAfterLogin) getActivity()).open();
            }
        });

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        lltoplogin.setVisibility(View.GONE);
        lltoplogout.setVisibility(View.VISIBLE);
        tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
        tvlogouttext.setText("Home");
        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(in);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);

            }
        });

        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);

        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        prgDialog.setCancelable(false);
        deviceid = AppDeviceUtils.getTelephonyId(getActivity());

        list = (ListView) rootView.findViewById(R.id.lv_mysheduleAfterLogin);
        listlayout = (LinearLayout) rootView.findViewById(R.id.listlayout);

        layout_one_myscheduling = (LinearLayout) rootView.findViewById(R.id.layout_one_myscheduling);
        layouttwo_myscheduling = (LinearLayout) rootView.findViewById(R.id.layouttwo_myscheduling);
        cv_filter = (CardView) rootView.findViewById(R.id.cv_filter);

        llregionandclientone = (LinearLayout) rootView.findViewById(R.id.llregionandclientone);
        llregionandclienttwo = (LinearLayout) rootView.findViewById(R.id.llregionandclienttwo);
        llregionandclientthree = (LinearLayout) rootView.findViewById(R.id.llregionandclienttthree);
        llregionandclientfour = (LinearLayout) rootView.findViewById(R.id.llregionandclientfour);
        llregionandclientfive = (LinearLayout) rootView.findViewById(R.id.llregionandclientfive);

        try {
            apiGetSaveSchedule();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rootView;
    }

    private void apiGetSaveSchedule() throws JSONException {
        String URL_GETSCHEDULINGDATA = domain + "/mobile/pxs_app/service/schedule_track/myscheduleoldview/getschedulingdata.php";
        JSONObject object = new JSONObject();
        object.put("access_key", "wazmrogqbjqhbkeddvelefdttxpeesabtznkoubqdrxfcbigwnrasvtlsjqxcqyv");

        TypeToken<List<GetSchedulingData>> token = new TypeToken<List<GetSchedulingData>>() {
        };

        new NetworkHandlerList(getActivity(), networkCallBack, token, 1)
                .execute(URL_GETSCHEDULINGDATA, object.toString());

        AppLogger.showMsgWithoutTag(URL_GETSCHEDULINGDATA + "  --  " + object.toString());
    }

    private void setFlowAdapter(List<GetSchedulingData> scheduleList) {
        adapter = new AdapterMyScheduleListView(getActivity(), scheduleList);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (adapter == null) {
                    return;
                }

                Intent intent = new Intent(getActivity(), MySchedulingGraphActivity.class);
                GetSchedulingData myScheduleDatum = adapter.getItem(position);
                intent.putExtra(Constant.VALUE_DATAID, myScheduleDatum.getId());
                intent.putExtra(Constant.DATA_APPROVAL, myScheduleDatum.getAppNo());
                intent.putExtra(Constant.DATA_REV, myScheduleDatum.getRevision());
                startActivity(intent);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP
                        && keyCode == KeyEvent.KEYCODE_BACK) {
                    Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                    return true;
                }
                return false;
            }
        });
    }


}