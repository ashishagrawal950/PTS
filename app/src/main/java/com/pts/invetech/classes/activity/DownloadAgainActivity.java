package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.core.app.NotificationCompat.Builder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.DownloadIEXCustomGrid;
import com.pts.invetech.R;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.apiresponse.DownloadAPIResponse;
import com.pts.invetech.pojo.Download;
import com.pts.invetech.utils.JSONUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;

public class DownloadAgainActivity extends Activity {

    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;
    static ArrayList<String> iex_namearray = new ArrayList<String>();
    static ArrayList<String> iex_namearray_sort = new ArrayList<String>();
    static ArrayList<Integer> iex_imagearray = new ArrayList<Integer>();
    static ArrayList<Integer> iex_imagearray_sort = new ArrayList<Integer>();
    private Context context;
    private Dialog prgDialog;
    private ProgressDialog prgDialog1;
    private LinearLayout mopenLayout, openPopupDownload, llObligation, llScheduling, llBill, llRatesheet, llProfitability, llObligationllScheduling,
            llBillllRatesheet;
    static TextView tvtradingdate, tvtradingdate_set, tvdownloadnull, tvdownloadnulltext, tviexdownload;
    static TextView tvobligation, tvscheduling, tvbill, tvratesheet, tvprofitability;
    private TextView etxt_fromdate;
    private String appurl, date, iexobligation, iexschedule, ratesheet, tradingdate, bill, profitabilty, pxilobligation, ipxilschedule, pxilprofitability, recbill;
    private String appurlagain, dateagain, iexobligationagain, iexscheduleagain, ratesheetagain, tradingdateagain, billagain, profitabiltyagain, recbillagain,
            pxilobligationagain, ipxilscheduleagain, pxilprofitabilityagain;
    private TextView btObligation, btScheduling, btBill, btRatesheet;
    static Button btgoDownload;
    private Animation animbounce;
    private String place;
    static String downloaddate;
    private NotificationManager mNotifyManager;
    private Builder mBuilder;
    private int id = 1;
    private SQLiteDatabase db;
    private String dbacess;
    private HomeActivity homeActivity;
    private GridView gridViewiex;
    private String extation,domain;
    private String lastdate;
    private boolean bResponse = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_downloadagain);
        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);

        Cursor cu = db.rawQuery("SELECT * FROM baby", null);
        if (cu.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (cu.moveToNext()) {
            buffer.append("access_key: " + cu.getString(0) + "\n");
            dbacess = cu.getString(0);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
		/* prgDialog = new ProgressDialog(DownloadAgainActivity.this);
	     prgDialog.setMessage("Please wait...");
	     prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        prgDialog1 = new ProgressDialog(DownloadAgainActivity.this);
        prgDialog1.setMessage("Downloading...");
        prgDialog1.setCancelable(false);

        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        tvtradingdate = (TextView) findViewById(R.id.tvtradingdate);
        tvtradingdate_set = (TextView) findViewById(R.id.tvtradingdate_set);
        tvdownloadnull = (TextView) findViewById(R.id.tvdownloadnull);
        tvdownloadnulltext = (TextView) findViewById(R.id.tvdownloadnulltext);
        llObligation = (LinearLayout) findViewById(R.id.llObligation);
        llScheduling = (LinearLayout) findViewById(R.id.llScheduling);
        llBill = (LinearLayout) findViewById(R.id.llBill);
        llRatesheet = (LinearLayout) findViewById(R.id.llRatesheet);
        llProfitability = (LinearLayout) findViewById(R.id.llProfitability);
        llObligationllScheduling = (LinearLayout) findViewById(R.id.llObligationllScheduling);
        llBillllRatesheet = (LinearLayout) findViewById(R.id.llBillllRatesheet);
        animbounce = AnimationUtils.loadAnimation(DownloadAgainActivity.this, R.anim.bounce);
        gridViewiex = (GridView) findViewById(R.id.gridViewiex);
        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });
        Intent in = getIntent();
        String lastdateSend = in.getStringExtra("lastdate");
        if (lastdateSend.length() == 0) {
            lastdate = "LASTDATE";
        } else {
            lastdate = lastdateSend;
        }


        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DownloadAgainActivity.this, MainActivityAfterLogin.class);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        tvtradingdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        ConnectionDetector cd = new ConnectionDetector(DownloadAgainActivity.this);
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            new HttpAsyncTask().execute(domain + "/mobile/pxs_app/html/downloads.php");
        }
        btgoDownload = (Button) findViewById(R.id.btgoDownload);
        btgoDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //as = tvtradingdate.getText().toString();
                //Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
                ConnectionDetector cd = new ConnectionDetector(DownloadAgainActivity.this);
                if (!cd.isConnectingToInternet()) {
                    Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    new HttpAsyncTaskGo()
                            .execute(domain + "/mobile/pxs_app/html/downloads.php");
                }
            }
        });
    }

    /**
     * Showing Dialog
     */
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                prgDialog1 = new ProgressDialog(DownloadAgainActivity.this);
                prgDialog1.setMessage("Downloading file. Please wait...");
                prgDialog1.setIndeterminate(false);
                prgDialog1.setMax(100);
                prgDialog1.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                prgDialog1.setCancelable(true);
                prgDialog1.show();
                return prgDialog1;
            default:
                return null;
        }
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(DownloadAgainActivity.this, MainActivityAfterLogin.class);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    public static class SelectDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                tvtradingdate.setText(dayplace + "-" + monthplace + "-" + year);
                tvtradingdate_set.setText(year + "-" + monthplace + "-" + dayplace);
                downloaddate = tvtradingdate_set.getText().toString();
                //  Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
            }
            else if (month <= 9) {
                String monthplace = "0" + month;
                tvtradingdate.setText(day + "-" + monthplace + "-" + year);
                tvtradingdate_set.setText(year + "-" + monthplace + "-" + day);
                downloaddate = tvtradingdate_set.getText().toString();
            }
            else if (day < 10) {
                String dayplace = "0" + day;
                tvtradingdate.setText(dayplace + "-" + month + "-" + year);
                tvtradingdate_set.setText(year + "-" + month + "-" + dayplace);
                downloaddate = tvtradingdate_set.getText().toString();
            }
            else{
                tvtradingdate.setText(day + "-" + month + "-" + year);
                tvtradingdate_set.setText(year + "-" + month + "-" + day);
                downloaddate = tvtradingdate_set.getText().toString();

            }

        }

    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm username, password
     */
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Download download = new Download();
            download.setDate(lastdate);
            download.setAccess_key(dbacess);
            return DownloadAPIResponse.POST(urls[0], download);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                //Toast.makeText(getActivity(), "DownloadResult" + result,Toast.LENGTH_LONG).show();
                try {
                    iex_namearray.clear();
                    iex_imagearray.clear();
                    iex_imagearray_sort.clear();
                    iex_namearray_sort.clear();
                    JSONObject mainObject = new JSONObject(result);
                    if (mainObject.has("url")) {
                        appurl = mainObject.getString("url");
                    }
                    if (mainObject.has("date")) {
                        date = mainObject.getString("date");
                        String[] parts = date.split("-");
                        String dd = parts[0];
                        String mm = parts[1];
                        String yy = parts[2];
                        tvtradingdate.setText(yy + "-" + mm + "-" + dd);
                        tvtradingdate_set.setText(date);
                        downloaddate = tvtradingdate_set.getText().toString();

                    }
                    if (mainObject.has("bill")) {
                        bill = mainObject.getString("bill");
                        if (bill.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("IEXBill");
                            iex_imagearray.add(R.drawable.downloadiexbill);
                        }
                    }
                    if (mainObject.has("iexobligation")) {
                        iexobligation = mainObject.getString("iexobligation");
                        if (iexobligation.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("IEXObligation");
                            iex_imagearray.add(R.drawable.downloadiexobligation);
                        }
                    }
                    if (mainObject.has("iexschedule")) {
                        iexschedule = mainObject.getString("iexschedule");
                        if (iexschedule.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("IEXSchedule");
                            iex_imagearray.add(R.drawable.downloadiexscheduling);
                        }
                    }
                    if (mainObject.has("iexprofitability")) {
                        profitabilty = mainObject.getString("iexprofitability");
                        if (profitabilty.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("IEXProfitability");
                            iex_imagearray.add(R.drawable.downloadiexprofitablity);
                        }
                    }

                    if (mainObject.has("recbill")) {
                        recbill = mainObject.getString("recbill");
                        if (recbill.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("PXILBill");
                            iex_imagearray.add(R.drawable.downlodpxilbill);
                        }
                    }
                    if (mainObject.has("pxilobligation")) {
                        pxilobligation = mainObject.getString("pxilobligation");
                        if (pxilobligation.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("PXILObligation");
                            iex_imagearray.add(R.drawable.downlodpxilobligation);
                        }
                    }
                    if (mainObject.has("ipxilschedule")) {
                        ipxilschedule = mainObject.getString("ipxilschedule");
                        if (ipxilschedule.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("PXILScheduling");
                            iex_imagearray.add(R.drawable.downloadpxilscheduling);
                        }
                    }

                    if (mainObject.has("pxilprofitability")) {
                        pxilprofitability = mainObject.getString("pxilprofitability");
                        if (pxilprofitability.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("PXILProfitablity");
                            iex_imagearray.add(R.drawable.downlodpxilprofitability);
                        }
                    }

                    if (mainObject.has("ratesheet")) {
                        ratesheet = mainObject.getString("ratesheet");
                        if (ratesheet.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("PXILRatesheet");
                            iex_imagearray.add(R.drawable.lldownloadratesheetbg);
                        }
                    }


                    if (iex_namearray.size() == 0) {
                        //	tvdownloadnull.setText("NO File Found");
                        tvdownloadnull.setBackgroundResource(R.drawable.sad);
                        tvdownloadnulltext.setText("No files found for download, Please select another date.");
                        tvdownloadnull.setVisibility(View.VISIBLE);
                        tvdownloadnulltext.setVisibility(View.VISIBLE);
                        gridViewiex.setVisibility(View.GONE);
                        //tviexdownload.setVisibility(View.GONE);
                    } else {
                        gridViewiex.setVisibility(View.VISIBLE);
                        //tviexdownload.setVisibility(View.VISIBLE);
                        tvdownloadnull.setVisibility(View.GONE);
                        tvdownloadnulltext.setVisibility(View.GONE);
					/*for(int j= 0; j <= iex_namearray.size(); j++){
						String getpostion = iex_namearray.get(j);
					}*/
                        if (iex_namearray.contains("PXILRatesheet")) {
                            iex_namearray_sort.add("PXILRatesheet");
                            iex_imagearray_sort.add(R.drawable.lldownloadratesheetbg);
                        }
                        if (iex_namearray.contains("IEXObligation")) {
                            iex_namearray_sort.add("IEXObligation");
                            iex_imagearray_sort.add(R.drawable.downloadiexobligation);
                        }
                        if (iex_namearray.contains("IEXProfitability")) {
                            iex_namearray_sort.add("IEXProfitability");
                            iex_imagearray_sort.add(R.drawable.downloadiexprofitablity);
                        }
                        if (iex_namearray.contains("IEXSchedule")) {
                            iex_namearray_sort.add("IEXSchedule");
                            iex_imagearray_sort.add(R.drawable.downloadiexscheduling);
                        }
                        if (iex_namearray.contains("IEXBill")) {
                            iex_namearray_sort.add("IEXBill");
                            iex_imagearray_sort.add(R.drawable.downloadiexbill);
                        }
                        if (iex_namearray.contains("PXILObligation")) {
                            iex_namearray_sort.add("PXILObligation");
                            iex_imagearray_sort.add(R.drawable.downlodpxilobligation);
                        }
                        if (iex_namearray.contains("PXILProfitablity")) {
                            iex_namearray_sort.add("PXILProfitablity");
                            iex_imagearray_sort.add(R.drawable.downlodpxilprofitability);
                        }
                        if (iex_namearray.contains("PXILScheduling")) {
                            iex_namearray_sort.add("PXILScheduling");
                            iex_imagearray_sort.add(R.drawable.downloadpxilscheduling);
                        }
                        if (iex_namearray.contains("PXILBill")) {
                            iex_namearray_sort.add("PXILBill");
                            iex_imagearray_sort.add(R.drawable.downlodpxilbill);
                        }
                        DownloadIEXCustomGrid adapterIEX = new DownloadIEXCustomGrid(DownloadAgainActivity.this, iex_namearray_sort,
                                iex_imagearray_sort);
                        gridViewiex.setAdapter(adapterIEX);
					/*gridViewiex.setOnTouchListener(new View.OnTouchListener(){
			            @Override
			            public boolean onTouch(View v, MotionEvent event) {
			                if(event.getAction() == MotionEvent.ACTION_MOVE){
			                    return true;
			                }
			                return false;
			            }
			        });*/
                    }

                    gridViewiex.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                            // Toast.makeText(MainActivity.this, "" + complete_attendancestaff_namearray.get(position), Toast.LENGTH_SHORT).show();
                            if (iex_namearray_sort.get(position).equalsIgnoreCase("PXILRatesheet")) {
                                new DownloadFileAsync().execute("https://" + appurl + ratesheet);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                // in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", ratesheet);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }
                            if (iex_namearray_sort.get(position).equalsIgnoreCase("IEXObligation")) {

                                new DownloadFileAsync().execute("http://" + appurl + iexobligation);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", iexobligation);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }

                            if (iex_namearray_sort.get(position).equalsIgnoreCase("IEXProfitability")) {
                                new DownloadFileAsync().execute("http://" + appurl + profitabilty);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", profitabilty);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }

                            if (iex_namearray_sort.get(position).equalsIgnoreCase("IEXSchedule")) {
                                new DownloadFileAsync().execute("http://" + appurl + iexschedule);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", iexschedule);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }

                            if (iex_namearray_sort.get(position).equalsIgnoreCase("IEXBill")) {
                                new DownloadFileAsync().execute("http://" + appurl + bill);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", bill);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }


                            if (iex_namearray_sort.get(position).equalsIgnoreCase("PXILObligation")) {
                                new DownloadFileAsync().execute("http://" + appurl + pxilobligation);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", pxilobligation);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }


                            if (iex_namearray_sort.get(position).equalsIgnoreCase("PXILProfitablity")) {
                                new DownloadFileAsync().execute("http://" + appurl + pxilprofitability);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", pxilprofitability);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }

                            if (iex_namearray_sort.get(position).equalsIgnoreCase("PXILScheduling")) {
                                new DownloadFileAsync().execute("http://" + appurl + ipxilschedule);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", ipxilschedule);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }


                            if (iex_namearray_sort.get(position).equalsIgnoreCase("PXILBill")) {

                                new DownloadFileAsync().execute("http://" + appurl + recbill);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", recbill);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }

                        }
                    });
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }


        }


    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm username, password
     */
    private class HttpAsyncTaskGo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Download download = new Download();
            download.setDate(downloaddate);
            download.setAccess_key(dbacess);
            return DownloadAPIResponse.POST(urls[0], download);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get DownloadAPIResponse Result:", result);
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                //Toast.makeText(getActivity(), "DownloadResult" + result,Toast.LENGTH_LONG).show();
                try {
                    iex_namearray.clear();
                    iex_imagearray.clear();
                    iex_imagearray_sort.clear();
                    iex_namearray_sort.clear();
                    JSONObject mainObject = new JSONObject(result);
                    if (mainObject.has("url")) {
                        appurl = mainObject.getString("url");
                    }
                    if (mainObject.has("date")) {
                        date = mainObject.getString("date");
                        String[] parts = date.split("-");
                        String dd = parts[0];
                        String mm = parts[1];
                        String yy = parts[2];
                        tvtradingdate.setText(yy + "-" + mm + "-" + dd);
                        tvtradingdate_set.setText(date);
                        downloaddate = tvtradingdate_set.getText().toString();

                    }
                    if (mainObject.has("bill")) {
                        bill = mainObject.getString("bill");
                        if (bill.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("IEXBill");
                            iex_imagearray.add(R.drawable.downloadiexbill);
                        }
                    }
                    if (mainObject.has("iexobligation")) {
                        iexobligation = mainObject.getString("iexobligation");
                        if (iexobligation.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("IEXObligation");
                            iex_imagearray.add(R.drawable.downloadiexobligation);
                        }
                    }
                    if (mainObject.has("iexschedule")) {
                        iexschedule = mainObject.getString("iexschedule");
                        if (iexschedule.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("IEXSchedule");
                            iex_imagearray.add(R.drawable.downloadiexscheduling);
                        }
                    }
                    if (mainObject.has("iexprofitability")) {
                        profitabilty = mainObject.getString("iexprofitability");
                        if (profitabilty.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("IEXProfitability");
                            iex_imagearray.add(R.drawable.downloadiexprofitablity);
                        }
                    }

                    if (mainObject.has("recbill")) {
                        recbill = mainObject.getString("recbill");
                        if (recbill.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("PXILBill");
                            iex_imagearray.add(R.drawable.downlodpxilbill);
                        }
                    }
                    if (mainObject.has("pxilobligation")) {
                        pxilobligation = mainObject.getString("pxilobligation");
                        if (pxilobligation.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("PXILObligation");
                            iex_imagearray.add(R.drawable.downlodpxilobligation);
                        }
                    }
                    if (mainObject.has("ipxilschedule")) {
                        ipxilschedule = mainObject.getString("ipxilschedule");
                        if (ipxilschedule.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("PXILScheduling");
                            iex_imagearray.add(R.drawable.downloadpxilscheduling);
                        }
                    }

                    if (mainObject.has("pxilprofitability")) {
                        pxilprofitability = mainObject.getString("pxilprofitability");
                        if (pxilprofitability.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("PXILProfitablity");
                            iex_imagearray.add(R.drawable.downlodpxilprofitability);
                        }
                    }

                    if (mainObject.has("ratesheet")) {
                        ratesheet = mainObject.getString("ratesheet");
                        if (ratesheet.equalsIgnoreCase("NA")) {
                        } else {
                            iex_namearray.add("PXILRatesheet");
                            iex_imagearray.add(R.drawable.lldownloadratesheetbg);
                        }
                    }


                    if (iex_namearray.size() == 0) {
                        //	tvdownloadnull.setText("NO File Found");
                        tvdownloadnull.setBackgroundResource(R.drawable.sad);
                        tvdownloadnulltext.setText("No files found for download, Please select another date.");
                        tvdownloadnull.setVisibility(View.VISIBLE);
                        tvdownloadnulltext.setVisibility(View.VISIBLE);
                        gridViewiex.setVisibility(View.GONE);
                        //tviexdownload.setVisibility(View.GONE);
                    } else {
                        gridViewiex.setVisibility(View.VISIBLE);
                        //tviexdownload.setVisibility(View.VISIBLE);
                        tvdownloadnull.setVisibility(View.GONE);
                        tvdownloadnulltext.setVisibility(View.GONE);
					/*for(int j= 0; j <= iex_namearray.size(); j++){
						String getpostion = iex_namearray.get(j);
					}*/
                        if (iex_namearray.contains("PXILRatesheet")) {
                            iex_namearray_sort.add("PXILRatesheet");
                            iex_imagearray_sort.add(R.drawable.lldownloadratesheetbg);
                        }
                        if (iex_namearray.contains("IEXObligation")) {
                            iex_namearray_sort.add("IEXObligation");
                            iex_imagearray_sort.add(R.drawable.downloadiexobligation);
                        }
                        if (iex_namearray.contains("IEXProfitability")) {
                            iex_namearray_sort.add("IEXProfitability");
                            iex_imagearray_sort.add(R.drawable.downloadiexprofitablity);
                        }
                        if (iex_namearray.contains("IEXSchedule")) {
                            iex_namearray_sort.add("IEXSchedule");
                            iex_imagearray_sort.add(R.drawable.downloadiexscheduling);
                        }
                        if (iex_namearray.contains("IEXBill")) {
                            iex_namearray_sort.add("IEXBill");
                            iex_imagearray_sort.add(R.drawable.downloadiexbill);
                        }
                        if (iex_namearray.contains("PXILObligation")) {
                            iex_namearray_sort.add("PXILObligation");
                            iex_imagearray_sort.add(R.drawable.downlodpxilobligation);
                        }
                        if (iex_namearray.contains("PXILProfitablity")) {
                            iex_namearray_sort.add("PXILProfitablity");
                            iex_imagearray_sort.add(R.drawable.downlodpxilprofitability);
                        }
                        if (iex_namearray.contains("PXILScheduling")) {
                            iex_namearray_sort.add("PXILScheduling");
                            iex_imagearray_sort.add(R.drawable.downloadpxilscheduling);
                        }
                        if (iex_namearray.contains("PXILBill")) {
                            iex_namearray_sort.add("PXILBill");
                            iex_imagearray_sort.add(R.drawable.downlodpxilbill);
                        }
                        DownloadIEXCustomGrid adapterIEX = new DownloadIEXCustomGrid(DownloadAgainActivity.this, iex_namearray_sort,
                                iex_imagearray_sort);
                        gridViewiex.setAdapter(adapterIEX);
					/*gridViewiex.setOnTouchListener(new View.OnTouchListener(){
			            @Override
			            public boolean onTouch(View v, MotionEvent event) {
			                if(event.getAction() == MotionEvent.ACTION_MOVE){
			                    return true;
			                }
			                return false;
			            }
			        });*/
                    }

                    gridViewiex.setOnItemClickListener(new OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                            // Toast.makeText(MainActivity.this, "" + complete_attendancestaff_namearray.get(position), Toast.LENGTH_SHORT).show();
                            if (iex_namearray_sort.get(position).equalsIgnoreCase("PXILRatesheet")) {
                                new DownloadFileAsync().execute("http://" + appurl + ratesheet);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                // in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", ratesheet);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }
                            if (iex_namearray_sort.get(position).equalsIgnoreCase("IEXObligation")) {
                                new DownloadFileAsync().execute("http://" + appurl + iexobligation);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", iexobligation);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }

                            if (iex_namearray_sort.get(position).equalsIgnoreCase("IEXProfitability")) {
                                new DownloadFileAsync().execute("http://" + appurl + profitabilty);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", profitabilty);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }

                            if (iex_namearray_sort.get(position).equalsIgnoreCase("IEXSchedule")) {
                                new DownloadFileAsync().execute("http://" + appurl + iexschedule);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                // in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", iexschedule);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }

                            if (iex_namearray_sort.get(position).equalsIgnoreCase("IEXBill")) {
                                new DownloadFileAsync().execute("http://" + appurl + bill);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                // in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", bill);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }


                            if (iex_namearray_sort.get(position).equalsIgnoreCase("PXILObligation")) {
                                new DownloadFileAsync().execute("http://" + appurl + pxilobligation);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", pxilobligation);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }


                            if (iex_namearray_sort.get(position).equalsIgnoreCase("PXILProfitablity")) {
                                new DownloadFileAsync().execute("http://" + appurl + pxilprofitability);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", pxilprofitability);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }

                            if (iex_namearray_sort.get(position).equalsIgnoreCase("PXILScheduling")) {
                                new DownloadFileAsync().execute("http://" + appurl + ipxilschedule);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", ipxilschedule);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }

                            if (iex_namearray_sort.get(position).equalsIgnoreCase("PXILBill")) {
                                new DownloadFileAsync().execute("http://" + appurl + recbill);
                                Intent in = new Intent(DownloadAgainActivity.this, DownloadFileActivity.class);
                                //in.putExtra("extation", extation);
                                in.putExtra("appurl", appurl);
                                in.putExtra("categoryurl", recbill);
                                in.putExtra("lastdate", downloaddate);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            }
                        }
                    });
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }

    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileAsync extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                String myUrl = f_url[0];

                URL url = new URL(myUrl);
                URLConnection conection = url.openConnection();
                conection.connect();

                String depo = conection.getHeaderField("Content-Disposition");
                String[] depoSplit = depo.split("filename=");
                extation = depoSplit[1].replace("filename=", "").replace("\"", "").trim();

                int lenghtOfFile = conection.getContentLength();
                final String contentLengthStr = conection.getHeaderField("content-length");
                String tyyup = conection.getContentType();


                //AppLogger.showError("typpepe",tyyup);

                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment
                        .getExternalStorageDirectory().toString() + "/" + extation);

                byte[] data = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            //dismissDialog(progress_bar_type);

        }

    }


}