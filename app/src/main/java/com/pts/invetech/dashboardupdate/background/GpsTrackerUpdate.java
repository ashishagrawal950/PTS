package com.pts.invetech.dashboardupdate.background;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;

import androidx.core.app.ActivityCompat;

import com.pts.invetech.utils.AppLogger;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by vaibhav on 9/1/17.
 */

public class GpsTrackerUpdate {

    public static Location getLastLocation(Context context) {

        LocationManager locationManager = (LocationManager) context
                .getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            Location loc=locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            AppLogger.showMsg("Location",loc.toString());
            Location loca1=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if(loc!=null){
                return loc;
            }else if(loca1!=null){
                return loca1;
            }else{
                AppLogger.showToastShort(context,"By default Delhi is selected, To change state please change state in List.");
            }
        }

        return null;
    }

}
