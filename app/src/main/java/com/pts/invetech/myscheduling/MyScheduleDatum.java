package com.pts.invetech.myscheduling;

/**
 * Created by vaibhav on 11/4/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyScheduleDatum {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("region")
    @Expose
    private String region;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

}