package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.NotificationOne;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.AccountSummaryAPIResponse;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.customlist.ReportCustomList;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.pojo.AccountSummary;
import com.pts.invetech.pojo.AccountSummaryDetails;
import com.pts.invetech.utils.JSONUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


public class ReportActivity extends Activity {

    static final int DATE_PICKER_ID = 1111;
    public int year;
    public int month;
    public int day;
    private Point p;
    private ArrayAdapter<String> adapter;
    private Context context;
    private LinearLayout mopenLayout, openPopupReport, reportlistheading;
    static TextView fromDateEtxt, toDateEtxt, etxt_fromdate_set, etxt_todate_set, tvcompanyName, tvtotalammount, tvtotalqty, tvtotalamtrecieve,
            tvlasttranaction;
    private SQLiteDatabase db;
    static String datefrom, dateto, dbacess;
    private Button btreportgo, btexporttoexcel;
    private Dialog prgDialog;
    private String totalammount, totalqty, totalamtrecieve;
    private NotificationManager myNotificationManager;
    private int numMessagesOne = 0;
    private int notificationIdOne = 111;
    private String path;
    private String zero, one, two, three, four, five, six, seven, eight;
    private AccountSummaryDetails mypojo;
    private HashMap<Integer, AccountSummaryDetails> myHashmap;
    private ArrayList<String> accountsummaryzero = new ArrayList<String>();
    private ArrayList<String> accountsummaryone = new ArrayList<String>();
    private ArrayList<String> accountsummarytwo = new ArrayList<String>();
    private ArrayList<String> accountsummarythree = new ArrayList<String>();
    private ArrayList<String> accountsummaryfour = new ArrayList<String>();
    private ArrayList<String> accountsummaryfive = new ArrayList<String>();
    private ArrayList<String> accountsummarysix = new ArrayList<String>();
    private ArrayList<String> accountsummaryseven = new ArrayList<String>();
    private ArrayList<String> accountsummaryeight = new ArrayList<String>();
    private String[] zeroadapter, oneadapter, twoadapter, threeadapter,
            fouradapter, fiveadapter, sixadapter, sevenadapter, eightadapter;
    private int i1;
    private String companyName;
    private String status, message;
    private NonScrollListView lvreportlist;
    private String part1;
    private HomeActivity homeActivity;
    private String comeFrom, domain;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_report);
        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);

        Cursor cu = db.rawQuery("SELECT * FROM baby", null);
        if (cu.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (cu.moveToNext()) {
            buffer.append("access_key: " + cu.getString(0) + "\n");
            dbacess = cu.getString(0);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
            //buffer.append("access_key: "+c.getString(0)+"\n");
            //Toast.makeText(getApplicationContext(), "access_key" + c.getColumnIndex("access_key"), Toast.LENGTH_LONG).show();
        }

        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");

        path = Environment.getExternalStorageDirectory()
                .getAbsolutePath();
	/* prgDialog = new ProgressDialog(ReportActivity.this);
     prgDialog.setMessage("Please wait...");
     prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        myHashmap = new HashMap<Integer, AccountSummaryDetails>();

        Intent in = getIntent();
        comeFrom = in.getStringExtra("from");


        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        fromDateEtxt = (TextView) findViewById(R.id.etxt_fromdate);
        toDateEtxt = (TextView) findViewById(R.id.etxt_todate);

        etxt_fromdate_set = (TextView) findViewById(R.id.etxt_fromdate_set);
        etxt_todate_set = (TextView) findViewById(R.id.etxt_todate_set);

        tvcompanyName = (TextView) findViewById(R.id.tvcompanyName);
        btreportgo = (Button) findViewById(R.id.btreportgo);
        btexporttoexcel = (Button) findViewById(R.id.btexporttoexcel);

        tvtotalammount = (TextView) findViewById(R.id.tvtotalammount);
        tvtotalqty = (TextView) findViewById(R.id.tvtotalqty);
        tvtotalamtrecieve = (TextView) findViewById(R.id.tvtotalamtrecieve);
        db = openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        Cursor companynamec = db.rawQuery("SELECT * FROM companyname", null);
        if (companynamec.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        StringBuffer companynamecbuffer = new StringBuffer();
        while (companynamec.moveToNext()) {
            companynamecbuffer.append("companynamecbuffer: "
                    + companynamec.getString(0) + "\n");
            companyName = companynamec.getString(0);
            // Toast.makeText(getApplicationContext(), "companyName:" +
            // companyName, Toast.LENGTH_LONG).show();
        }
        tvcompanyName.setText(companyName);
        reportlistheading = (LinearLayout) findViewById(R.id.reportlistheading);
        lvreportlist = (NonScrollListView) findViewById(R.id.lvreportlist);
        tvlasttranaction = (TextView) findViewById(R.id.tvlasttranaction);

        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -1);
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        // formattedDate have current date/time
        String[] parts = formattedDate.split(" ");
        part1 = parts[0]; // 004
        // http://www.mittalpower.com/mobile/pxs_app/html/accountsummary.php
        ConnectionDetector cd = new ConnectionDetector(ReportActivity.this);
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            new HttpAsyncTaskReport().execute(domain + "/mobile/pxs_app/html/accountsummary.php");
        }
        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comeFrom.equalsIgnoreCase("MainActivity")) {
                    Intent in = new Intent(ReportActivity.this, MainActivityAfterLogin.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.animation, R.anim.animation2);
                    finish();
                } else if (comeFrom.equalsIgnoreCase("HomeActivity")) {
                    Intent in = new Intent(ReportActivity.this, HomeActivity.class);
                    in.putExtra("companyName", companyName);
                    startActivity(in);
                    overridePendingTransition(R.anim.animation, R.anim.animation2);
                    finish();
                } else {
                    Intent in = new Intent(ReportActivity.this, MainActivityAfterLogin.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.animation, R.anim.animation2);
                    finish();
                }
            }
        });

        fromDateEtxt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectFromDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");

            }
        });

        toDateEtxt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectToDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");

            }
        });

        btreportgo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //	Toast.makeText(getActivity(), datefrom, Toast.LENGTH_LONG).show();
                //	Toast.makeText(getActivity(), dateto, Toast.LENGTH_LONG).show();
                //	Toast.makeText(getActivity(), dbacess, Toast.LENGTH_LONG).show();
                if ((datefrom != null) && (dateto != null)) {
                    ConnectionDetector cd = new ConnectionDetector(ReportActivity.this);
                    if (!cd.isConnectingToInternet()) {
                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        new HttpAsyncTask().execute(domain + "/mobile/pxs_app/html/accountsummary.php");
                    }
                } else {
                    Toast.makeText(ReportActivity.this, "Please fill the all details.", Toast.LENGTH_LONG).show();
                }
            }
        });

        btexporttoexcel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //	Toast.makeText(getActivity(), totalammount, Toast.LENGTH_LONG).show();
                //	Toast.makeText(getActivity(), totalqty, Toast.LENGTH_LONG).show();
                //	Toast.makeText(getActivity(), totalamtrecieve, Toast.LENGTH_LONG).show();
                createPDF();
                prgDialog.show();
                Notification.Builder mBuilder = new Notification.Builder(ReportActivity.this);

                mBuilder.setContentTitle("PTS");
                mBuilder.setContentText("Account Summary file download!!");
                mBuilder.setTicker("Generate");
                mBuilder.setSmallIcon(R.drawable.logopts);

                // Increase notification number every time a new notification arrives
                mBuilder.setNumber(++numMessagesOne);
                mBuilder.setAutoCancel(true);
                // Creates an explicit intent for an Activity in your app
                Intent myIntent = new Intent();
                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                File file = new File(path + "/" + "Account Summary.pdf");
                myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");

                //   Intent resultIntent = new Intent(getActivity(), NotificationOne.class);
                //   resultIntent.putExtra("notificationId", notificationIdOne);

                //This ensures that navigating backward from the Activity leads out of the app to Home page
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(ReportActivity.this);
                // Adds the back stack for the Intent
                stackBuilder.addParentStack(NotificationOne.class);

                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(myIntent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(
                                0,
                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                        );
                // start the activity when the user clicks the notification text
                mBuilder.setContentIntent(resultPendingIntent);
                Notification noti = mBuilder.build();
                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                noti.defaults |= Notification.DEFAULT_VIBRATE;
                noti.defaults |= Notification.DEFAULT_SOUND;
                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                noti.ledARGB = 0xff00ff00;
                noti.ledOnMS = 300;
                noti.ledOffMS = 1000;
                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                // pass the Notification object to the system
                myNotificationManager.notify(notificationIdOne, mBuilder.build());

                Toast.makeText(ReportActivity.this, "PDF Generated Successfully.", Toast.LENGTH_LONG).show();
                prgDialog.hide();
            }
        });

        //	openPopupReport = (LinearLayout) rootView.findViewById(R.id.openPopupReport);
        //	logoutpopup();
    }

    public void createPDF() {
        Document doc = new Document();
        Random r = new Random();
        i1 = r.nextInt(80 - 65) + 65;
        try {
		    /*path = Environment.getExternalStorageDirectory()
					.getAbsolutePath() + "/droidText";*/
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();
            Log.d("PDFCreator", "PDF Path: " + path);
            File file = new File(dir, "Account Summary.pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            PdfWriter.getInstance(doc, fOut);
            // open the document
            doc.open();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.group);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image myImg = Image.getInstance(stream.toByteArray());
            myImg.setAlignment(Image.RIGHT);
            // add image to document
            doc.add(myImg);

            Font largeBold = new Font(Font.TIMES_ROMAN, 22,
                    Font.UNDERLINE);
            Paragraph p4 = new Paragraph();
            p4.setFont(largeBold);
            p4.setAlignment(Paragraph.ALIGN_CENTER);
            //Font paraFont4 = new Font(Font.UNDERLINE);
            p4.add("Account Summary");
            // add paragraph to document
            doc.add(p4);

			/*PdfPTable mainTable = new PdfPTable(1);
	        PdfPCell cell;

	        for (int i = 0; i < zeroadapter.length; i++) {
	            cell = new PdfPCell(new Phrase(zeroadapter[i]));
	            PdfPTable table = new PdfPTable(1);
	            table.addCell(cell);
	            if (i+1 <= zeroadapter.length -1) {
	               cell = new PdfPCell(new Phrase(zeroadapter[i + 1]));
	               table.addCell(cell);
	            } else {
	                cell = new PdfPCell(new Phrase(""));
	                table.addCell(cell);
	            }
	            mainTable.addCell(table);
	        }
	        doc.add(mainTable);*/

            PdfPTable table = new PdfPTable(new float[]{2.5f, 3.3f, 4.3f, 4.5f, 5f, 4f, 5f, 5f, 3.3f});
            PdfPCell cellzero, cellone, celltwo, cellthree, cellfour, cellfive, cellsix, cellseven, celleight;
            table.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            table.setWidthPercentage(105f);
            table.setSpacingBefore(10);
            table.addCell("Date");
            table.addCell("INV NO.");
            table.addCell("Trade Date");
            table.addCell("Delivery Date");
            table.addCell("Units Name");
            table.addCell("Trade QTY in MWH");
            table.addCell("Amount Receivable");
            table.addCell("Amt Received");
            table.addCell("Balance");
            table.setHeaderRows(1);
            PdfPCell[] cells = table.getRow(0).getCells();
            for (int j = 0; j < cells.length; j++) {
                cells[j].setBackgroundColor(harmony.java.awt.Color.gray);
            }
            //	if(date.size()>0 && min.size()>0 && avg.size()>0 && max.size()>0)
            if (accountsummaryzero.size() > 0) {
                for (int i = 0; i < accountsummaryzero.size(); i++) {
                    cellzero = new PdfPCell(new Phrase(accountsummaryzero.get(i)));
                    table.addCell(cellzero);

                    cellone = new PdfPCell(new Phrase(accountsummaryone.get(i)));
                    table.addCell(cellone);

                    celltwo = new PdfPCell(new Phrase(accountsummarytwo.get(i)));
                    table.addCell(celltwo);

                    cellthree = new PdfPCell(new Phrase(accountsummarythree.get(i)));
                    table.addCell(cellthree);

                    cellfour = new PdfPCell(new Phrase(accountsummaryfour.get(i)));
                    table.addCell(cellfour);

                    cellfive = new PdfPCell(new Phrase(accountsummaryfive.get(i)));
                    table.addCell(cellfive);

                    cellsix = new PdfPCell(new Phrase(accountsummarysix.get(i)));
                    table.addCell(cellsix);

                    cellseven = new PdfPCell(new Phrase(accountsummaryseven.get(i)));
                    table.addCell(cellseven);

                    celleight = new PdfPCell(new Phrase(accountsummaryeight.get(i)));
                    table.addCell(celleight);

                    //table.addCell("Name:" + i);
                    //table.addCell("Age:" + i);
                    //table.addCell("Location:" + i);
                }
                doc.add(table);
            }

            PdfPTable tablefooter = new PdfPTable(new float[]{11.5f, 2.4f, 3,
                    3, 2});
            tablefooter.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            tablefooter.setWidthPercentage(105);
            tablefooter.addCell("Total");
            tablefooter.addCell(totalqty);
            tablefooter.addCell(totalammount);
            tablefooter.addCell(totalamtrecieve);
            tablefooter.addCell(" ");
            PdfPCell[] cells3 = tablefooter.getRow(0).getCells();
            for (int j = 0; j < cells3.length; j++) {
                cells3[j].setBackgroundColor(harmony.java.awt.Color.gray);
            }
            doc.add(tablefooter);

            /*
             * Paragraph p1 = new Paragraph("Total Ammount:  " + totalammount);
             * Font paraFont = new Font(Font.COURIER);
             * p1.setAlignment(Paragraph.ALIGN_LEFT); p1.setFont(paraFont); //
             * add paragraph to document doc.add(p1);
             */

            // set footer
            Phrase footerText = new Phrase("This is auto generated");
            HeaderFooter pdfFooter = new HeaderFooter(footerText, false);
            doc.setFooter(pdfFooter);

        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        } finally {
            doc.close();
        }
		/*Intent in = new Intent(ReportActivity.this, ReportPDFshowActivity.class);
		in.putExtra("accountsummary", "Account Summary.pdf");
		//in.putExtra("newsletterfilename", partsix);
		startActivity(in);
		overridePendingTransition(R.anim.animation, R.anim.animation2);
		finish();*/
        File file = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/" + "Account Summary.pdf");
		/*Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file),"application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		startActivity(intent);*/

        Intent intent;
        intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        try {
            //Intent intent = new Intent(Intent.ACTION_VIEW);
            //intent.setDataAndType(Uri.fromFile(file),"application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            // No application to view, ask to download one
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("No Application Found");
            builder.setMessage("Download one from Android Market?");
            builder.setPositiveButton("Yes, Please",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                            marketIntent
                                    .setData(Uri
                                            .parse("market://details?id=com.adobe.reader"));
                            startActivity(marketIntent);
                        }
                    });
            builder.setNegativeButton("No, Thanks", null);
            builder.create().show();
        }


    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        if (comeFrom.equalsIgnoreCase("MainActivity")) {
            Intent in = new Intent(ReportActivity.this, MainActivityAfterLogin.class);
            startActivity(in);
            overridePendingTransition(R.anim.animation, R.anim.animation2);
            finish();
        } else if (comeFrom.equalsIgnoreCase("HomeActivity")) {
            Intent in = new Intent(ReportActivity.this, HomeActivity.class);
            in.putExtra("companyName", companyName);
            startActivity(in);
            overridePendingTransition(R.anim.animation, R.anim.animation2);
            finish();
        } else {
            Intent in = new Intent(ReportActivity.this, MainActivityAfterLogin.class);
            startActivity(in);
            overridePendingTransition(R.anim.animation, R.anim.animation2);
            finish();
        }
        return;
    }

    public static class SelectFromDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                fromDateEtxt.setText(dayplace + "-" + monthplace + "-" + year);
                etxt_fromdate_set.setText(year + "-" + monthplace + "-" + dayplace);
                datefrom = etxt_fromdate_set.getText().toString();
                //  Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
            } else if (month <= 9) {
                String monthplace = "0" + month;
                fromDateEtxt.setText(day + "-" + monthplace + "-" + year);
                etxt_fromdate_set.setText(year + "-" + monthplace + "-" + day);
                datefrom = etxt_fromdate_set.getText().toString();
            } else if (day < 10) {
                //	String dayplace = "0" + day;
                fromDateEtxt.setText(day + "-" + month + "-" + year);
                etxt_fromdate_set.setText(year + "-" + month + "-" + day);
                datefrom = etxt_fromdate_set.getText().toString();
            } else {
                fromDateEtxt.setText(day + "-" + month + "-" + year);
                etxt_fromdate_set.setText(year + "-" + month + "-" + day);
                datefrom = etxt_fromdate_set.getText().toString();
            }

            //fromDateEtxt.setText(month + "/" + day + "/" + year);
        }

    }

    public static class SelectToDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                toDateEtxt.setText(dayplace + "-" + monthplace + "-" + year);
                etxt_todate_set.setText(year + "-" + monthplace + "-" + dayplace);
                dateto = etxt_todate_set.getText().toString();
                //  Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
            } else if (month <= 9) {
                String monthplace = "0" + month;
                toDateEtxt.setText(day + "-" + monthplace + "-" + year);
                etxt_todate_set.setText(year + "-" + monthplace + "-" + day);
                dateto = etxt_todate_set.getText().toString();
            } else if (day < 10) {
                //	String dayplace = "0" + day;
                toDateEtxt.setText(day + "-" + month + "-" + year);
                etxt_todate_set.setText(year + "-" + month + "-" + day);
                dateto = etxt_todate_set.getText().toString();
            } else {
                toDateEtxt.setText(day + "-" + month + "-" + year);
                etxt_todate_set.setText(year + "-" + month + "-" + day);
                dateto = etxt_todate_set.getText().toString();
            }
            //toDateEtxt.setText(month + "/" + day + "/" + year);
        }

    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm Date, DeviceRegister.access_key
     */
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AccountSummary accountSummary = new AccountSummary();
            accountSummary.setDatefrom(datefrom);
            accountSummary.setDateto(dateto);
            accountSummary.setLasttransaction("");
            accountSummary.setAccess_key(dbacess);
            return AccountSummaryAPIResponse.POST(urls[0], accountSummary);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Report Result:", result);
            prgDialog.hide();
            //  Toast.makeText(getActivity(), "Account Summary" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    //  	{"status":"ERR","value":"-","message":"Invalid Access Key"}

                    JSONObject jsonobject = new JSONObject(result);
                    if (jsonobject.has("status")) {
                        status = jsonobject.getString("status");
                    }
                    if (jsonobject.has("message")) {
                        message = jsonobject.getString("message");
                    }


                    if (jsonobject.has("totalammount")) {
                        totalammount = jsonobject.getString("totalammount");
                    }
                    if (jsonobject.has("totalqty")) {
                        totalqty = jsonobject.getString("totalqty");
                    }
                    if (jsonobject.has("totalamtrecieve")) {
                        totalamtrecieve = jsonobject.getString("totalamtrecieve");
                    }

                    JSONArray detail = jsonobject.getJSONArray("detail");
                    if (detail.isNull(0)) {
                        //Toast.makeText(getActivity(), "Data is empty.", Toast.LENGTH_LONG).show();
                        btexporttoexcel.setVisibility(View.GONE);
                        tvtotalqty.setBackgroundResource(R.drawable.sad);
                        tvtotalammount.setText("No Account Summary Found. Please select another dates.");
                        tvtotalammount.setVisibility(View.VISIBLE);
                        tvtotalqty.setVisibility(View.VISIBLE);
                        reportlistheading.setVisibility(View.GONE);
                        lvreportlist.setVisibility(View.GONE);
                        tvlasttranaction.setVisibility(View.GONE);
                    } else {
                        accountsummaryzero.clear();
                        accountsummaryone.clear();
                        accountsummarytwo.clear();
                        accountsummarythree.clear();
                        accountsummaryfour.clear();
                        accountsummaryfive.clear();
                        accountsummarysix.clear();
                        accountsummaryseven.clear();
                        accountsummaryeight.clear();
                        btexporttoexcel.setVisibility(View.VISIBLE);
                        lvreportlist.setVisibility(View.VISIBLE);
                        tvtotalammount.setVisibility(View.GONE);
                        reportlistheading.setVisibility(View.VISIBLE);
                        tvtotalqty.setVisibility(View.GONE);
                        tvlasttranaction.setVisibility(View.VISIBLE);
                        for (int j = 0; j < detail.length(); j++) {
                            JSONObject obj = detail.getJSONObject(j);

                            if (obj.has("0")) {
                                zero = obj.getString("0");

                            }
                            if (obj.has("1")) {
                                one = obj.getString("1");

                            }
                            if (obj.has("2")) {
                                two = obj.getString("2");

                            }
                            if (obj.has("3")) {
                                three = obj.getString("3");

                            }
                            if (obj.has("4")) {
                                four = obj.getString("4");

                            }
                            if (obj.has("5")) {
                                five = obj.getString("5");

                            }
                            if (obj.has("6")) {
                                six = obj.getString("6");

                            }
                            if (obj.has("7")) {
                                seven = obj.getString("7");

                            }
                            if (obj.has("8")) {
                                eight = obj.getString("8");

                            }
                            if (zero.length() != 0) {
                                accountsummaryzero.add(zero);
                                zero = "";
                            } else {
                                accountsummaryzero.add("-");
                                zero = "";
                            }

                            if (zero != null) {
                                accountsummaryone.add(one);
                                one = "";
                            } else {
                                accountsummaryone.add("-");
                                one = "";
                            }

                            if (two != null) {
                                accountsummarytwo.add(two);
                                two = "";
                            } else {
                                accountsummarytwo.add("-");
                                two = "";
                            }

                            if (three != null) {
                                accountsummarythree.add(three);
                                three = "";
                            } else {
                                accountsummarythree.add("-");
                                three = "";
                            }

                            if (four != null) {
                                accountsummaryfour.add(four);
                                four = "";
                            } else {
                                accountsummaryfour.add("-");
                                four = "";
                            }

                            if (five != null) {
                                accountsummaryfive.add(five);
                                five = "";
                            } else {
                                accountsummaryfive.add("-");
                                five = "";
                            }

                            if (six != null) {
                                accountsummarysix.add(six);
                                six = "";
                            } else {
                                accountsummarysix.add("-");
                                six = "";
                            }


                            if (seven != null) {
                                accountsummaryseven.add(seven);
                                seven = "";
                            } else {
                                accountsummaryseven.add("-");
                                seven = "";
                            }

                            if (eight != null) {
                                accountsummaryeight.add(eight);
                                eight = "";
                            } else {
                                accountsummaryeight.add("-");
                                eight = "";
                            }
						/*mypojo = new AccountSummaryDetails(zero, one, two, three, four, five, six, seven, eight);
						myHashmap.put(j, mypojo);
						accountsummaryzero = new ArrayList<String>();
						accountsummaryone = new ArrayList<String>();
						accountsummarytwo = new ArrayList<String>();
						accountsummarythree = new ArrayList<String>();
						accountsummaryfour = new ArrayList<String>();
						accountsummaryfive = new ArrayList<String>();
						accountsummarysix = new ArrayList<String>();
						accountsummaryseven = new ArrayList<String>();
						accountsummaryeight = new ArrayList<String>();

						Collection<AccountSummaryDetails> c = myHashmap.values();
						Iterator<AccountSummaryDetails> itr = c.iterator();
						while (itr.hasNext()) {
							AccountSummaryDetails value = (AccountSummaryDetails) itr.next();
							accountsummaryzero.add(value.getZero());
							accountsummaryone.add(value.getOne());
							accountsummarytwo.add(value.getTwo());
							accountsummarythree.add(value.getThree());
							accountsummaryfour.add(value.getFour());
							accountsummaryfive.add(value.getFive());
							accountsummarysix.add(value.getSix());
							accountsummaryseven.add(value.getSeven());
							accountsummaryeight.add(value.getEight());
						}*/
                        }

		    		/*zeroadapter = accountsummaryzero.toArray(new String[accountsummaryzero.size()]);
		    		oneadapter = accountsummaryone.toArray(new String[accountsummaryone.size()]);
		    		twoadapter = accountsummarytwo.toArray(new String[accountsummarytwo.size()]);
		    		threeadapter = accountsummarythree.toArray(new String[accountsummarythree.size()]);
		    		fouradapter = accountsummaryfour.toArray(new String[accountsummaryfour.size()]);
		    		fiveadapter = accountsummaryfive.toArray(new String[accountsummaryfive.size()]);
					sixadapter = accountsummarysix.toArray(new String[accountsummarysix.size()]);
					sevenadapter = accountsummaryseven.toArray(new String[accountsummaryseven.size()]);
					eightadapter = accountsummaryeight.toArray(new String[accountsummaryeight.size()]);*/


					/*List<String> listzero = Arrays.asList(zeroadapter);
					Collections.reverse(listzero);

					List<String> listone = Arrays.asList(oneadapter);
					Collections.reverse(listone);

					List<String> listtwo = Arrays.asList(twoadapter);
					Collections.reverse(listtwo);

					List<String> listthree = Arrays.asList(threeadapter);
					Collections.reverse(listthree);

					List<String> listfour = Arrays.asList(fouradapter);
					Collections.reverse(listfour);

					List<String> listfive = Arrays.asList(fiveadapter);
					Collections.reverse(listfive);


					List<String> listsix = Arrays.asList(sixadapter);
					Collections.reverse(listsix);


					List<String> listseven = Arrays.asList(sevenadapter);
					Collections.reverse(listseven);

					List<String> listeight = Arrays.asList(eightadapter);
					Collections.reverse(listeight);*/

                        //Collections.reverse(accountsummaryzero);
                        //Collections.reverse(accountsummaryfour);
                        //Collections.reverse(accountsummarysix);
                        //Collections.reverse(accountsummaryseven);
                        //Collections.reverse(accountsummaryeight);
                        List<String> accountsummaryzeroSend = null;
                        List<String> accountsummaryfourSend = null;
                        List<String> accountsummarysixSend = null;
                        List<String> accountsummarysevenSend = null;
                        List<String> accountsummaryeightSend = null;

                        if (accountsummaryfour.size() > 10) {
                            accountsummaryfourSend = accountsummaryfour.subList(0, 10);
                        }
                        if (accountsummarysix.size() > 10) {
                            accountsummarysixSend = accountsummarysix.subList(0, 10);
                        }
                        if (accountsummaryseven.size() > 10) {
                            accountsummarysevenSend = accountsummaryseven.subList(0, 10);
                        }
                        if (accountsummaryeight.size() > 10) {
                            accountsummaryeightSend = accountsummaryeight.subList(0, 10);
                        }
					/*if(accountsummaryzero.size() > 10){
						 accountsummaryzeroSend = accountsummaryzero.subList(0, 10);
						 ReportCustomList adapter = new ReportCustomList(ReportActivity.this, accountsummaryzeroSend, accountsummaryfourSend,accountsummarysixSend, accountsummarysevenSend, accountsummaryeightSend);
						 lvreportlist.setAdapter(adapter);
						}
					else{*/
                        ReportCustomList adapter = new ReportCustomList(ReportActivity.this, accountsummaryzero, accountsummaryfour, accountsummarysix, accountsummaryseven, accountsummaryeight);
                        lvreportlist.setAdapter(adapter);
                        /*}*/
                        //Utility.setListViewHeightBasedOnChildren(lvreportlist);
                        //myHashmap.clear();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        org.jsoup.nodes.Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                org.jsoup.nodes.Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm Date, DeviceRegister.access_key
     */
    private class HttpAsyncTaskReport extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AccountSummary accountSummary = new AccountSummary();
            accountSummary.setDatefrom("");
            accountSummary.setDateto("");
            accountSummary.setLasttransaction("LASTTRANSACTION");
            accountSummary.setAccess_key(dbacess);
            return AccountSummaryAPIResponse.POST(urls[0], accountSummary);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Report Result:", result);
            prgDialog.hide();
            //  Toast.makeText(getActivity(), "Account Summary" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    //  	{"status":"ERR","value":"-","message":"Invalid Access Key"}

                    JSONObject jsonobject = new JSONObject(result);
                    if (jsonobject.has("status")) {
                        status = jsonobject.getString("status");
                    }
                    if (jsonobject.has("message")) {
                        message = jsonobject.getString("message");
                    }

                    if (jsonobject.has("totalammount")) {
                        totalammount = jsonobject.getString("totalammount");
                    }
                    if (jsonobject.has("totalqty")) {
                        totalqty = jsonobject.getString("totalqty");
                    }
                    if (jsonobject.has("totalamtrecieve")) {
                        totalamtrecieve = jsonobject.getString("totalamtrecieve");
                    }

                    JSONArray detail = jsonobject.getJSONArray("detail");
                    if (detail.isNull(0)) {
                        //Toast.makeText(getActivity(), "Data is empty.", Toast.LENGTH_LONG).show();
                        btexporttoexcel.setVisibility(View.GONE);
                        tvtotalqty.setBackgroundResource(R.drawable.sad);
                        tvtotalammount.setText("No Account Summary Found. Please select another dates.");
                        tvtotalammount.setVisibility(View.VISIBLE);
                        tvtotalqty.setVisibility(View.VISIBLE);
                        reportlistheading.setVisibility(View.GONE);
                        lvreportlist.setVisibility(View.GONE);
                        tvlasttranaction.setVisibility(View.GONE);
                    } else {
                        accountsummaryzero.clear();
                        accountsummaryone.clear();
                        accountsummarytwo.clear();
                        accountsummarythree.clear();
                        accountsummaryfour.clear();
                        accountsummaryfive.clear();
                        accountsummarysix.clear();
                        accountsummaryseven.clear();
                        accountsummaryeight.clear();

                        btexporttoexcel.setVisibility(View.VISIBLE);
                        lvreportlist.setVisibility(View.VISIBLE);
                        tvtotalammount.setVisibility(View.GONE);
                        reportlistheading.setVisibility(View.VISIBLE);
                        tvtotalqty.setVisibility(View.GONE);
                        tvlasttranaction.setVisibility(View.VISIBLE);
                        for (int j = 0; j < detail.length(); j++) {
                            JSONObject obj = detail.getJSONObject(j);

                            if (obj.has("0")) {
                                zero = obj.getString("0");

                            }
                            if (obj.has("1")) {
                                one = obj.getString("1");

                            }
                            if (obj.has("2")) {
                                two = obj.getString("2");

                            }
                            if (obj.has("3")) {
                                three = obj.getString("3");

                            }
                            if (obj.has("4")) {
                                four = obj.getString("4");

                            }
                            if (obj.has("5")) {
                                five = obj.getString("5");

                            }
                            if (obj.has("6")) {
                                six = obj.getString("6");

                            }
                            if (obj.has("7")) {
                                seven = obj.getString("7");

                            }
                            if (obj.has("8")) {
                                eight = obj.getString("8");

                            }
                            if (zero.length() != 0) {
                                accountsummaryzero.add(zero);
                                zero = "";
                            } else {
                                accountsummaryzero.add("-");
                                zero = "";
                            }

                            if (zero != null) {
                                accountsummaryone.add(one);
                                one = "";
                            } else {
                                accountsummaryone.add("-");
                                one = "";
                            }

                            if (two != null) {
                                accountsummarytwo.add(two);
                                two = "";
                            } else {
                                accountsummarytwo.add("-");
                                two = "";
                            }

                            if (three != null) {
                                accountsummarythree.add(three);
                                three = "";
                            } else {
                                accountsummarythree.add("-");
                                three = "";
                            }

                            if (four != null) {
                                accountsummaryfour.add(four);
                                four = "";
                            } else {
                                accountsummaryfour.add("-");
                                four = "";
                            }

                            if (five != null) {
                                accountsummaryfive.add(five);
                                five = "";
                            } else {
                                accountsummaryfive.add("-");
                                five = "";
                            }

                            if (six != null) {
                                accountsummarysix.add(six);
                                six = "";
                            } else {
                                accountsummarysix.add("-");
                                six = "";
                            }


                            if (seven != null) {
                                accountsummaryseven.add(seven);
                                seven = "";
                            } else {
                                accountsummaryseven.add("-");
                                seven = "";
                            }

                            if (eight != null) {
                                accountsummaryeight.add(eight);
                                eight = "";
                            } else {
                                accountsummaryeight.add("-");
                                eight = "";
                            }
						/*mypojo = new AccountSummaryDetails(zero, one, two, three, four, five, six, seven, eight);
						myHashmap.put(j, mypojo);
						accountsummaryzero = new ArrayList<String>();
						accountsummaryone = new ArrayList<String>();
						accountsummarytwo = new ArrayList<String>();
						accountsummarythree = new ArrayList<String>();
						accountsummaryfour = new ArrayList<String>();
						accountsummaryfive = new ArrayList<String>();
						accountsummarysix = new ArrayList<String>();
						accountsummaryseven = new ArrayList<String>();
						accountsummaryeight = new ArrayList<String>();

						Collection<AccountSummaryDetails> c = myHashmap.values();
						Iterator<AccountSummaryDetails> itr = c.iterator();
						while (itr.hasNext()) {
							AccountSummaryDetails value = (AccountSummaryDetails) itr.next();
							accountsummaryzero.add(value.getZero());
							accountsummaryone.add(value.getOne());
							accountsummarytwo.add(value.getTwo());
							accountsummarythree.add(value.getThree());
							accountsummaryfour.add(value.getFour());
							accountsummaryfive.add(value.getFive());
							accountsummarysix.add(value.getSix());
							accountsummaryseven.add(value.getSeven());
							accountsummaryeight.add(value.getEight());
						}*/
                        }

		    		/*zeroadapter = accountsummaryzero.toArray(new String[accountsummaryzero.size()]);
		    		oneadapter = accountsummaryone.toArray(new String[accountsummaryone.size()]);
		    		twoadapter = accountsummarytwo.toArray(new String[accountsummarytwo.size()]);
		    		threeadapter = accountsummarythree.toArray(new String[accountsummarythree.size()]);
		    		fouradapter = accountsummaryfour.toArray(new String[accountsummaryfour.size()]);
		    		fiveadapter = accountsummaryfive.toArray(new String[accountsummaryfive.size()]);
					sixadapter = accountsummarysix.toArray(new String[accountsummarysix.size()]);
					sevenadapter = accountsummaryseven.toArray(new String[accountsummaryseven.size()]);
					eightadapter = accountsummaryeight.toArray(new String[accountsummaryeight.size()]);*/


					/*List<String> listzero = Arrays.asList(zeroadapter);
					Collections.reverse(listzero);

					List<String> listone = Arrays.asList(oneadapter);
					Collections.reverse(listone);

					List<String> listtwo = Arrays.asList(twoadapter);
					Collections.reverse(listtwo);

					List<String> listthree = Arrays.asList(threeadapter);
					Collections.reverse(listthree);

					List<String> listfour = Arrays.asList(fouradapter);
					Collections.reverse(listfour);

					List<String> listfive = Arrays.asList(fiveadapter);
					Collections.reverse(listfive);


					List<String> listsix = Arrays.asList(sixadapter);
					Collections.reverse(listsix);


					List<String> listseven = Arrays.asList(sevenadapter);
					Collections.reverse(listseven);

					List<String> listeight = Arrays.asList(eightadapter);
					Collections.reverse(listeight);*/

                        //Collections.reverse(accountsummaryzero);
                        //Collections.reverse(accountsummaryfour);
                        //Collections.reverse(accountsummarysix);
                        //Collections.reverse(accountsummaryseven);
                        //Collections.reverse(accountsummaryeight);
                        List<String> accountsummaryzeroSend = null;
                        List<String> accountsummaryfourSend = null;
                        List<String> accountsummarysixSend = null;
                        List<String> accountsummarysevenSend = null;
                        List<String> accountsummaryeightSend = null;

                        if (accountsummaryfour.size() > 10) {
                            accountsummaryfourSend = accountsummaryfour.subList(0, 10);
                        }
                        if (accountsummarysix.size() > 10) {
                            accountsummarysixSend = accountsummarysix.subList(0, 10);
                        }
                        if (accountsummaryseven.size() > 10) {
                            accountsummarysevenSend = accountsummaryseven.subList(0, 10);
                        }
                        if (accountsummaryeight.size() > 10) {
                            accountsummaryeightSend = accountsummaryeight.subList(0, 10);
                        }
					/*if(accountsummaryzero.size() > 10){
						 accountsummaryzeroSend = accountsummaryzero.subList(0, 10);
						 ReportCustomList adapter = new ReportCustomList(ReportActivity.this, accountsummaryzeroSend, accountsummaryfourSend,accountsummarysixSend, accountsummarysevenSend, accountsummaryeightSend);
						 lvreportlist.setAdapter(adapter);
						}
					else{*/
                        ReportCustomList adapter = new ReportCustomList(ReportActivity.this, accountsummaryzero, accountsummaryfour, accountsummarysix, accountsummaryseven, accountsummaryeight);
                        lvreportlist.setAdapter(adapter);
                        /*}*/
                        //Utility.setListViewHeightBasedOnChildren(lvreportlist);
                        //myHashmap.clear();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        org.jsoup.nodes.Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                org.jsoup.nodes.Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }

}
