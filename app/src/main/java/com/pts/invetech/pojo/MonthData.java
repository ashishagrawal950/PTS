package com.pts.invetech.pojo;

/**
 * Created by vaibhav on 5/11/16.
 */

public class MonthData{

    private String fileName;
    private String fileAddress;
    private String time;
    private String date;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileAddress() {
        return fileAddress;
    }

    public void setFileAddress(String fileAddress) {
        this.fileAddress = fileAddress;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String tm) {
        String[] splt=tm.split(" ");
        date=splt[0];
        time=splt[1];
    }

    public String getDate() {
        return date;
    }
}
