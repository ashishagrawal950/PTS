package com.pts.invetech;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class ServiceRequest extends AsyncTask<String, Integer, String> {
	OnAsyncRequestComplete caller;
	Context context;
	String method = "GET";
	List<NameValuePair> parameters = null;
	ProgressDialog pDialog = null;

	// Three Constructors
	public ServiceRequest(Activity a, String m, List<NameValuePair> p) {
		caller = (OnAsyncRequestComplete) a;
		context = a;
		method = m;
		parameters = p;
	}

	public ServiceRequest(Activity a, String m) {
		caller = (OnAsyncRequestComplete) a;
		context = a;
		method = m;
	}

	public ServiceRequest(Activity a) {
		caller = (OnAsyncRequestComplete) a;
		context = a;
	}

	// Interface to be implemented by calling activity
	public interface OnAsyncRequestComplete {
		public void asyncResponse(String response);
	}
	
	public interface OnAsyncRequestCompleteLogin {
		public void asyncResponseLogin(String response);
	}

	public String doInBackground(String... urls) {
		// get url pointing to entry point of API
		String address = urls[0].toString();
		if (method == "POST") {
			return post(address);
		}

		if (method == "GET") {
			return get(address);
		}

		return null;
	}

	public void onPreExecute() {
		pDialog = new ProgressDialog(context);
		pDialog.setMessage("Loading data.."); // typically you will define such
		// strings in a remote file.
		pDialog.show();
	}

	public void onProgressUpdate(Integer... progress) {
		// you can implement some progressBar and update it in this record
		// setProgressPercent(progress[0]);
	}

	public void onPostExecute(String response) {
		if (pDialog != null && pDialog.isShowing()) {
			pDialog.dismiss();
		}
		caller.asyncResponse(response);
	}

	protected void onCancelled(String response) {
		if (pDialog != null && pDialog.isShowing()) {
			pDialog.dismiss();
		}

		caller.asyncResponse(response);
	}

	@SuppressWarnings("deprecation")
	private String get(String address) {
		try {

			if (parameters != null) {

				String query = "";
				String EQ = "=";
				String AMP = "&";
				for (NameValuePair param : parameters) {
					query += param.getName() + EQ
							+ URLEncoder.encode(param.getValue()) + AMP;
				}

				if (query != "") {
					address += "?" + query;
				}
			}

			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(address);

			HttpResponse response = client.execute(get);
			return stringifyResponse(response);

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}

		return null;
	}

	private String post(String address) {
		try {
			JSONObject JSONObjectData = new JSONObject();
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(address);
			if (parameters != null) {
				String json = " ";
				//post.setEntity(new UrlEncodedFormEntity(parameters));
				for (NameValuePair nameValuePair : parameters) {
			        try {
			            JSONObjectData.put(nameValuePair.getName(), nameValuePair.getValue());
			        } catch (JSONException e) {

			        }
			    }
				json = JSONObjectData.toString();
				StringEntity se = new StringEntity("data="+json);
				post.setEntity(se);
				post.setHeader("Accept", "*/*");
				post.setHeader("Content-type", "application/x-www-form-urlencoded");
			}

			HttpResponse response = client.execute(post);
			return stringifyResponse(response);

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}

		return null;
	}
				
				
//	private String post(String address) {
//		try {
//			JSONObject JSONObjectData = new JSONObject();
//			String json = " ";
//			String response_data = null;
//			StringBuilder sb = new StringBuilder();
//			// HttpClient client = new DefaultHttpClient();
//			// HttpPost post = new HttpPost(address);
//
//			HttpURLConnection urlConnection = null;
//			URL url = new URL(address);
//			urlConnection = (HttpURLConnection) url.openConnection();
//			urlConnection.setDoOutput(true);
//			urlConnection.setRequestMethod("POST");
//			urlConnection.setUseCaches(false);
//			urlConnection.setConnectTimeout(10000);
//			urlConnection.setReadTimeout(10000);
//			urlConnection.setRequestProperty("Content-Type",
//					"application/x-www-form-urlencoded");
//			// urlConnection.setRequestProperty("Host",
//			// "android.schoolportal.gr");
//			urlConnection.connect();
//			if (parameters != null) {
//
//				// post.setEntity(new UrlEncodedFormEntity(parameters));
//				for (NameValuePair nameValuePair : parameters) {
//					try {
//						JSONObjectData.put(nameValuePair.getName(),
//								nameValuePair.getValue());
//					} catch (JSONException e) {
//
//					}
//				}
//				// json = JSONObjectData.toString();
//				// StringEntity se = new StringEntity("data="+json);
//				// post.setEntity(se);
//				// post.setHeader("Accept", "*/*");
//				// post.setHeader("Content-type",
//				// "application/x-www-form-urlencoded");*/
//
//				OutputStreamWriter out = new OutputStreamWriter(
//						urlConnection.getOutputStream());
//				out.write("data=" + JSONObjectData.toString());
//				out.close();
//
//			}
//
//			// HttpResponse response = client.execute(post);
//			int HttpResult = urlConnection.getResponseCode();
//			if (HttpResult == HttpURLConnection.HTTP_OK) {
//				BufferedReader br = new BufferedReader(new InputStreamReader(
//						urlConnection.getInputStream(), "utf-8"));
//				String line = null;
//				while ((line = br.readLine()) != null) {
//					sb.append(line + "\n");
//				}
//				br.close();
//
//				System.out.println("Ashish" + sb.toString());
//				response_data = sb.toString();
//
//			} else {
//				System.out.println(urlConnection.getResponseMessage());
//			}
//
//			return response_data;
//
//		} catch (ClientProtocolException e) {
//			// TODO Auto-generated catch block
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//		}
//
//		return null;
//	}

	private String stringifyResponse(HttpResponse response) {
		BufferedReader in;
		try {
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));

			StringBuffer sb = new StringBuffer("");
			String line = "";
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			in.close();

			return sb.toString();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
}
