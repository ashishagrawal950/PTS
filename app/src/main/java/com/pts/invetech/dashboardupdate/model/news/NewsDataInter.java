package com.pts.invetech.dashboardupdate.model.news;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by vaibhav on 20/1/17.
 */

public class NewsDataInter implements Parcelable {

    private ArrayList<SubNews> subNewsList = null;
    private String title;

    public NewsDataInter(){
        subNewsList =new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<SubNews> getSubNewsList() {
        return subNewsList;
    }

    public void setSubNewsList(ArrayList<SubNews> subNewsList) {
        this.subNewsList = subNewsList;
    }

    public void add(SubNews subnews){
        this.subNewsList.add(subnews);
    }

    public void clearList(){
        this.subNewsList.clear();
    }



    protected NewsDataInter(Parcel in) {
        if (in.readByte() == 0x01) {
            subNewsList = new ArrayList<SubNews>();
            in.readList(subNewsList, SubNews.class.getClassLoader());
        } else {
            subNewsList = null;
        }
        title = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (subNewsList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(subNewsList);
        }
        dest.writeString(title);
    }

    @SuppressWarnings("unused")
    public static final Creator<NewsDataInter> CREATOR = new Creator<NewsDataInter>() {
        @Override
        public NewsDataInter createFromParcel(Parcel in) {
            return new NewsDataInter(in);
        }

        @Override
        public NewsDataInter[] newArray(int size) {
            return new NewsDataInter[size];
        }
    };
}