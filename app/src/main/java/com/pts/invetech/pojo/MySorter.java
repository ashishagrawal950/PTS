package com.pts.invetech.pojo;

import com.pts.model.realtimedetail.Datum;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Vaibhav on 12-08-2016.
 */
public class MySorter {

    public static void sortA2ZBuyer(List<Datum> arrayList){

        Collections.sort(arrayList,new Comparator<Datum>() {
            @Override
            public int compare(Datum d1, Datum d2) {
                return d1.getBuyer().compareToIgnoreCase(d2.getBuyer());
            }
        });
    }

    public static void sortZ2ABuyer(List<Datum> arrayList){

        Collections.sort(arrayList,new Comparator<Datum>() {
            @Override
            public int compare(Datum d1, Datum d2) {
                return d2.getBuyer().compareToIgnoreCase(d1.getBuyer());
            }
        });
    }

    public static void sortA2ZSeller(List<Datum> arrayList){

        Collections.sort(arrayList,new Comparator<Datum>() {
            @Override
            public int compare(Datum d1, Datum d2) {
                return d1.getSeller().compareToIgnoreCase(d2.getSeller());
            }
        });
    }

    public static void sortZ2ASeller(List<Datum> arrayList){

        Collections.sort(arrayList,new Comparator<Datum>() {
            @Override
            public int compare(Datum d1, Datum d2) {
                return d2.getSeller().compareToIgnoreCase(d1.getSeller());
            }
        });
    }

    public static void sortA2ZTrader(List<Datum> arrayList){

        Collections.sort(arrayList,new Comparator<Datum>() {
            @Override
            public int compare(Datum d1, Datum d2) {
                return d1.getTrader().compareToIgnoreCase(d2.getTrader());
            }
        });
    }

    public static void sortZ2ATrader(List<Datum> arrayList){

        Collections.sort(arrayList,new Comparator<Datum>() {
            @Override
            public int compare(Datum d1, Datum d2) {
                return d2.getTrader().compareToIgnoreCase(d1.getTrader());
            }
        });
    }

    public static void sortA2ZApproval(List<Datum> arrayList){

        Collections.sort(arrayList,new Comparator<Datum>() {

            @Override
            public int compare(Datum d1, Datum d2) {
                return MySorter.compare(d1,d2);
            }
        });
    }

    public static void sortZ2AApproval(List<Datum> arrayList){

        Collections.sort(arrayList,new Comparator<Datum>() {

            @Override
            public int compare(Datum d1, Datum d2) {
                return MySorter.compare(d2,d1);
            }
        });
    }

    public static void sortDesendingOnQtm(List<Datum> arrayList){

        Collections.sort(arrayList,new Comparator<Datum>() {
            @Override
            public int compare(Datum d1, Datum d2) {
                float change1 = Float.parseFloat(d1.getTotalqtm());
                float change2 = Float.parseFloat(d2.getTotalqtm());
                if (change1 < change2) return 1;
                if (change1 > change2) return -1;
                return 0;
            }
        });
    }

    public static void sortAssendingOnQtm(List<Datum> arrayList){

        Collections.sort(arrayList,new Comparator<Datum>() {
            @Override
            public int compare(Datum d1, Datum d2) {
                float change1 = Float.parseFloat(d1.getTotalqtm());
                float change2 = Float.parseFloat(d2.getTotalqtm());
                if (change1 < change2) return -1;
                if (change1 > change2) return 1;
                return 0;
            }
        });
    }

    public static int compare(Datum d1, Datum d2) {
        String firstString = d1.getApprovalNo();
        String secondString = d2.getApprovalNo();

        if (secondString == null || firstString == null) {
            return 0;
        }

        int lengthFirstStr = firstString.length();
        int lengthSecondStr = secondString.length();

        int index1 = 0;
        int index2 = 0;

        while (index1 < lengthFirstStr && index2 < lengthSecondStr) {
            char ch1 = firstString.charAt(index1);
            char ch2 = secondString.charAt(index2);

            char[] space1 = new char[lengthFirstStr];
            char[] space2 = new char[lengthSecondStr];

            int loc1 = 0;
            int loc2 = 0;

            do {
                space1[loc1++] = ch1;
                index1++;

                if (index1 < lengthFirstStr) {
                    ch1 = firstString.charAt(index1);
                } else {
                    break;
                }
            } while (Character.isDigit(ch1) == Character.isDigit(space1[0]));

            do {
                space2[loc2++] = ch2;
                index2++;

                if (index2 < lengthSecondStr) {
                    ch2 = secondString.charAt(index2);
                } else {
                    break;
                }
            } while (Character.isDigit(ch2) == Character.isDigit(space2[0]));

            String str1 = new String(space1);
            String str2 = new String(space2);

            int result;

            if (Character.isDigit(space1[0]) && Character.isDigit(space2[0])) {
                Integer firstNumberToCompare = new Integer(
                        Integer.parseInt(str1.trim()));
                Integer secondNumberToCompare = new Integer(
                        Integer.parseInt(str2.trim()));
                result = firstNumberToCompare.compareTo(secondNumberToCompare);
            } else {
                result = str1.compareTo(str2);
            }

            if (result != 0) {
                return result;
            }
        }
        return lengthFirstStr - lengthSecondStr;
    }
}