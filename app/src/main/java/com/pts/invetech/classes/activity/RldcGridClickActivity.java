package com.pts.invetech.classes.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseActivity;
import com.pts.invetech.R;
import com.pts.invetech.customlist.RldcClickListAdapter;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Constant;
import com.pts.model.appned.rldc.RldcClickDatum;
import com.pts.model.appned.rldc.RldcClickModel;

import org.json.JSONException;
import org.json.JSONObject;

public class RldcGridClickActivity extends AppBaseActivity implements View.OnClickListener {

    private LinearLayout rldcclickback;
    private ListView rldcclick_list;
    private SQLiteDatabase db;
    private String device_id, domain;
    private TextView headertext;
    private RldcClickListAdapter rldcAdapter;
    private boolean isFirst = true;
    private String URL_HIT = "https://www.mittalpower.com/mobile/pxs_app/service/scheduling/stateandimportwiseschedule.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rldc_grid_click);

        fetchDeviceId();

        rldcclickback = (LinearLayout) findViewById(R.id.rldcclickback);
        rldcclickback.setOnClickListener(this);

        rldcclick_list = (ListView) findViewById(R.id.rldcclick_list);
        headertext = (TextView) findViewById(R.id.headertext);

        Intent intent = getIntent();
        if (intent != null) {
            String region = intent.getStringExtra("region");
            String stateid = intent.getStringExtra("stateid");
            String importtype = intent.getStringExtra("importtype");
            String revision = intent.getStringExtra("revision");
            headertext.setText(" " + revision);
            try {
                if (!isFirst){
                    apiCall(region, stateid, importtype);
                    searchFun();
                }else{
                    apiCall(region, stateid, importtype);
                    searchFun();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    private void searchFun() {
//        ((Button)findViewById(R.id.btnsearch)).setOnClickListener(btnClick);

        EditText txtVu = (EditText) findViewById(R.id.search_input_grid);
        txtVu.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count < before) {
                    rldcAdapter.resetData();
                }
                rldcAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void fetchDeviceId() {
        db = openOrCreateDatabase("deviceDBSecond",
                Context.MODE_PRIVATE, null);
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
//        if (cdevice.getCount() == 0) {
//            // showMessage("Error", "No records found");
//        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
        if(!isFirst){
            domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
        }
    }

    private void apiCall(String region, String stateId, String importType) throws JSONException {
        JSONObject object = new JSONObject();
        object.put("region", region);
        object.put("stateid", stateId);
        object.put("importtype", importType);
        object.put("device_id", device_id);
        if(!isFirst){
            new NetworkManagerActivityGson(this, object, domain + "/mobile/pxs_app/service/scheduling/stateandimportwiseschedule.php", RldcClickModel.class).execute();
        }else{
            new NetworkManagerActivityGson(this,object,URL_HIT,RldcClickModel.class).execute();
        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.rldcclickback:
                finish();
                break;
        }

    }

    @Override
    public void updateObjectResult(Object object) {
        if (object == null) {
            AppLogger.showToastShort(getBaseContext(), getString(R.string.error_generic));
            return;
        }

        RldcClickModel rldcModel = (RldcClickModel) object;
        updateList(rldcModel);
    }

    private void updateList(RldcClickModel rldcModel) {

        rldcAdapter = new RldcClickListAdapter(getApplicationContext(), rldcModel.getData());

        rldcclick_list.addHeaderView(new View(this));
        rldcclick_list.addFooterView(new View(this));
        rldcclick_list.setAdapter(rldcAdapter);
        rldcclick_list.setTextFilterEnabled(true);

        rldcclick_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(RldcGridClickActivity.this,
                        RealtimeSchedulingDetailBlockActivity.class);

                if (rldcAdapter == null) {
                    AppLogger.showToastLong(RldcGridClickActivity.this, getString(R.string.error_generic));
                    return;
                }

                RldcClickDatum datum = (RldcClickDatum) parent.getItemAtPosition(position);

                intent.putExtra(Constant.VALUE_DATAID, datum.getDataId());
                intent.putExtra(Constant.DATA_APPROVAL, datum.getApprovalNo());
                intent.putExtra(Constant.DATA_REV, datum.getMaxrevision());

                startActivity(intent);
            }
        });
    }
}
