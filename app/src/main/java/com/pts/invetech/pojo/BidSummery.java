package com.pts.invetech.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BidSummery {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("block")
    @Expose
    private Block block;
    @SerializedName("single")
    @Expose
    private Single single;
    @SerializedName("delivery_date")
    @Expose
    private String deliveryDate;
    @SerializedName("status")
    @Expose
    private String status;

    private ArrayList<List<InnerData>> refrenceHolder = new ArrayList<>();
    private ArrayList<String> refrenceNameHolder = new ArrayList<>();

    /**
     * @return The date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return The block
     */
    public Block getBlock() {
        return block;
    }

    /**
     * @param block The block
     */
    public void setBlock(Block block) {
        this.block = block;
    }

    /**
     * @return The single
     */
    public Single getSingle() {
        return single;
    }

    /**
     * @param single The single
     */
    public void setSingle(Single single) {
        this.single = single;
    }
    /**
     *
     * @return
     * The deliveryDate
     */
    public String getDeliveryDate() {
        return deliveryDate;
    }

    /**
     *
     * @param deliveryDate
     * The delivery_date
     */
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
    /**
     *
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }


    public List<InnerData> getRefrence(int pos) {
        return refrenceHolder.get(pos);
    }

    public String getHeaderName(int pos) {
        return refrenceNameHolder.get(pos);
    }

    public int incrementChildCount() {
        int num = 0;
        refrenceHolder.clear();
        refrenceNameHolder.clear();

        if (!getBlock().getPxil().isEmpty()) {
            List<InnerData> data = getBlock().getPxil();
            refrenceHolder.add(data);
            refrenceNameHolder.add("Block Bid || PXIL ");
            ++num;
        }
        if (!getBlock().getIex().isEmpty()) {
            List<InnerData> data = getBlock().getIex();
            refrenceHolder.add(data);
            refrenceNameHolder.add("Block Bid || IEX ");
            ++num;
        }
        if (!getSingle().getIex().isEmpty()) {
            List<InnerData> data = getSingle().getIex();
            refrenceHolder.add(data);
            refrenceNameHolder.add("Single Bid || IEX ");
            ++num;
        }
        if (!getSingle().getPxil().isEmpty()) {
            List<InnerData> data = getSingle().getPxil();
            refrenceHolder.add(data);
            refrenceNameHolder.add("Single Bid || PXIL ");
            ++num;
        }
        return num;
    }
}