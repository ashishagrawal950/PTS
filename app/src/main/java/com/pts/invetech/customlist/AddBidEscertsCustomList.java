package com.pts.invetech.customlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

/**
 * Created by Ashish on 18-10-2017.
 */

public class AddBidEscertsCustomList extends ArrayAdapter<String> {

    private Activity context;
    private String[] escertseriala;
    private String[] typeadapterescert;
    private String[] priceadapterescert;
    private String[] dateadapterescert;
    private String[] noofrecadapterescert;


    public AddBidEscertsCustomList(Activity context, String[] escertseriala, String[] typeadapterescert,
                               String[] priceadapterescert, String[] dateadapterescert,
                               String[] noofrecadapterescert) {
        super(context, R.layout.list_new_bid, escertseriala);
        this.context = context;
        this.escertseriala = escertseriala;
        this.typeadapterescert = typeadapterescert;
        this.priceadapterescert = priceadapterescert;
        this.dateadapterescert = dateadapterescert;
        this.noofrecadapterescert = noofrecadapterescert;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_new_bid, null, true);
        TextView txt5 = (TextView) rowView.findViewById(R.id.txt5);
        TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
        TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
        TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
        txt5.setText(escertseriala[position]);
        txt1.setText(typeadapterescert[position]);
        txt2.setText(priceadapterescert[position]);
        txt3.setText(noofrecadapterescert[position]);
        // imageView.setImageResource(imageId[position]);
        return rowView;
    }
}