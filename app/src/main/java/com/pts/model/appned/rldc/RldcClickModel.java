
package com.pts.model.appned.rldc;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RldcClickModel {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("rldc")
    @Expose
    private String rldc;
    @SerializedName("periphery")
    @Expose
    private String periphery;
    @SerializedName("data")
    @Expose
    private List<RldcClickDatum> data = null;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRldc() {
        return rldc;
    }

    public void setRldc(String rldc) {
        this.rldc = rldc;
    }

    public String getPeriphery() {
        return periphery;
    }

    public void setPeriphery(String periphery) {
        this.periphery = periphery;
    }

    public List<RldcClickDatum> getData() {
        return data;
    }

    public void setData(List<RldcClickDatum> data) {
        this.data = data;
    }

}
