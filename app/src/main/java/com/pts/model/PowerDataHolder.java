package com.pts.model;

import com.google.gson.Gson;
import com.pts.model.appned.AdapterStateModel;
import com.pts.model.testmodel.OuterTableData;

import java.util.ArrayList;

/**
 * Created by Ashish on 17-08-2017.
 */

public class PowerDataHolder {

    private ArrayList<PowerDemandOuter> powerDemandComparisiondata;
    private ArrayList<OuterTableData> outerTestList;
    private ArrayList<AdapterStateModel> stateDataList;
    private String frmDate=null;
    private String toDate=null;
    private int position=0;
    private ArrayList<String> idLists;

    public String getFrmDate() {
        return frmDate;
    }

    public void setFrmDate(String frmDate) {
        this.frmDate = frmDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    private String year=null;
    private String states=null;

    public ArrayList<PowerDemandOuter> getPowerDemandComparisiondata() {
        return powerDemandComparisiondata;
    }

    public void setPowerDemandComparisiondata(ArrayList<PowerDemandOuter> powerDemandComparisiondata) {
        this.powerDemandComparisiondata = powerDemandComparisiondata;
    }

    public ArrayList<OuterTableData> getOuterTestList() {
        return outerTestList;
    }

    public void setOuterTestList(ArrayList<OuterTableData> outerTestList) {
        this.outerTestList = outerTestList;
    }

    public ArrayList<AdapterStateModel> getStateDataList() {
        return stateDataList;
    }

    public void setStateDataList(ArrayList<AdapterStateModel> stateDataList) {
        this.stateDataList = stateDataList;
    }

    public String getDataInJson(){
        Gson gson = new Gson();
        String json = gson.toJson(this);

        return json;
    }

    public ArrayList<String> getIdLists() {
        return idLists;
    }

    public void setIdLists(ArrayList<String> idLists) {
        this.idLists = idLists;
    }
}
