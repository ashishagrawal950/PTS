
package com.pts.model.statewiseurs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coal {

    @SerializedName("station")
    @Expose
    private String station;
    @SerializedName("qtmvalue")
    @Expose
    private Double qtmvalue;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("title")
    @Expose
    private String title;

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public Double getQtmvalue() {
        return qtmvalue;
    }

    public void setQtmvalue(Double qtmvalue) {
        this.qtmvalue = qtmvalue;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
