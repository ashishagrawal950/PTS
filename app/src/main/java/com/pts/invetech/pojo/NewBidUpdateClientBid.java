package com.pts.invetech.pojo;

public class NewBidUpdateClientBid {
	private String id;
	private String bidtype;
	private String btype;
	private String blockfrom;
	private String blockto;
	private String price;
	private String bid;
	private String noofblock;
	private String type;
	private String biddate;
	private String addbidupdateaccesskey;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBidtype() {
		return bidtype;
	}
	public void setBidtype(String bidtype) {
		this.bidtype = bidtype;
	}
	public String getBtype() {
		return btype;
	}
	public void setBtype(String btype) {
		this.btype = btype;
	}
	public String getBlockfrom() {
		return blockfrom;
	}
	public void setBlockfrom(String blockfrom) {
		this.blockfrom = blockfrom;
	}
	public String getBlockto() {
		return blockto;
	}
	public void setBlockto(String blockto) {
		this.blockto = blockto;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getNoofblock() {
		return noofblock;
	}
	public void setNoofblock(String noofblock) {
		this.noofblock = noofblock;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBiddate() {
		return biddate;
	}
	public void setBiddate(String biddate) {
		this.biddate = biddate;
	}
	public String getAddbidupdateaccesskey() {
		return addbidupdateaccesskey;
	}
	public void setAddbidupdateaccesskey(String addbidupdateaccesskey) {
		this.addbidupdateaccesskey = addbidupdateaccesskey;
	}
	
	
	
	
}
