
package com.pts.model.stationmain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Other {

    @SerializedName("station_name")
    @Expose
    private String stationName;
    @SerializedName("urs_remaining")
    @Expose
    private String ursRemaining;
    @SerializedName("breakup_sum")
    @Expose
    private String breakupSum;
    @SerializedName("reschedule_to_beneficiary")
    @Expose
    private String rescheduleToBeneficiary;

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getUrsRemaining() {
        return ursRemaining;
    }

    public void setUrsRemaining(String ursRemaining) {
        this.ursRemaining = ursRemaining;
    }

    public String getBreakupSum() {
        return breakupSum;
    }

    public void setBreakupSum(String breakupSum) {
        this.breakupSum = breakupSum;
    }

    public String getRescheduleToBeneficiary() {
        return rescheduleToBeneficiary;
    }

    public void setRescheduleToBeneficiary(String rescheduleToBeneficiary) {
        this.rescheduleToBeneficiary = rescheduleToBeneficiary;
    }

}
