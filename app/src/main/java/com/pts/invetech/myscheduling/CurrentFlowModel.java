package com.pts.invetech.myscheduling;

/**
 * Created by vaibhav on 11/4/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentFlowModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("buyer")
    @Expose
    private String buyer;
    @SerializedName("seller")
    @Expose
    private String seller;
    @SerializedName("periphery")
    @Expose
    private String periphery;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("app_no")
    @Expose
    private String appNo;
    @SerializedName("revision")
    @Expose
    private String revision;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("importtype")
    @Expose
    private String importtype;
    @SerializedName("trader")
    @Expose
    private String trader;
    @SerializedName("qtm")
    @Expose
    private String qtm;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getPeriphery() {
        return periphery;
    }

    public void setPeriphery(String periphery) {
        this.periphery = periphery;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAppNo() {
        return appNo;
    }

    public void setAppNo(String appNo) {
        this.appNo = appNo;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImporttype() {
        return importtype;
    }

    public void setImporttype(String importtype) {
        this.importtype = importtype;
    }

    public String getTrader() {
        return trader;
    }

    public void setTrader(String trader) {
        this.trader = trader;
    }

    public String getQtm() {
        return qtm;
    }

    public void setQtm(String qtm) {
        this.qtm = qtm;
    }
}