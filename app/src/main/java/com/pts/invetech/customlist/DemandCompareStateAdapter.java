package com.pts.invetech.customlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.pts.invetech.R;
import com.pts.invetech.customlist.YearandMontnthCostumAdapter;
import com.pts.model.appned.AdapterStateModel;
import com.pts.model.appned.AdapterYearModel;
import com.pts.model.appned.YearMonthModel;

import java.util.ArrayList;

/**
 * Created by vaibhav on 1/8/17.
 */

public class DemandCompareStateAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<AdapterStateModel> dataList;

    public DemandCompareStateAdapter(Context _context,
                                     ArrayList<AdapterStateModel> _adapterList) {

        context = _context;
        dataList = _adapterList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public AdapterStateModel getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        StateViewHolder vHolder;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_dialog, null, true);

            vHolder = new StateViewHolder();
            vHolder.checkHolder = (CheckBox) view.findViewById(R.id.chk_year);
            vHolder.checkHolder.setOnCheckedChangeListener(checkListner);
            view.setTag(vHolder);

        } else {
            vHolder = (StateViewHolder) view.getTag();
        }

        AdapterStateModel aModel = getItem(position);

        vHolder.position = position;
        vHolder.checkHolder.setText(aModel.getName());
        vHolder.checkHolder.setChecked(aModel.isClick());

        return view;

    }

    private CompoundButton.OnCheckedChangeListener checkListner = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            StateViewHolder tmp = (StateViewHolder) buttonView.getTag();
            int pos = tmp.position;

            AdapterStateModel aModel = getItem(pos);
            aModel.setClick(isChecked);
        }
    };

    public YearMonthModel getAllYears() {

        YearMonthModel tmp = new YearMonthModel();

        StringBuilder buffer = new StringBuilder();

        for (int index = 0; index < dataList.size(); index++) {
            AdapterStateModel chkModel = dataList.get(index);
            if (chkModel.isClick()) {
                buffer.append(chkModel.getName() + ",");
                tmp.addMonth(chkModel.getName());
            }
        }

        tmp.setTitle(buffer.toString());

        return tmp;
    }

    public String getAllStateName() {

        StringBuilder buffer = new StringBuilder();

        for (int index = 0; index < dataList.size(); index++) {
            AdapterStateModel chkModel = dataList.get(index);
            if (chkModel.isClick()) {
                buffer.append(chkModel.getName() + ",");
            }
        }

        return buffer.toString();
    }

    public ArrayList<String> getStateIds(){
        ArrayList<String> tmp=new ArrayList<>();

        for (int index = 0; index < dataList.size(); index++) {
            AdapterStateModel chkModel = dataList.get(index);
            if (chkModel.isClick()) {
                tmp.add(chkModel.getId());
            }
        }

        return tmp;
    }

    static class StateViewHolder {
        CheckBox checkHolder;
        int position;
    }
}