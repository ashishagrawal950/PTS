package com.pts.invetech.pojo;

public class ObligationDetails {

	public String DATE;
	public Float IEX;
	public Float PXIL;

	public ObligationDetails(String DATE, Float IEX, Float PXIL) {
		this.DATE = DATE;
		this.IEX = IEX;
		this.PXIL = PXIL;

	}

	

	public Float getIEX() {
		return IEX;
	}



	public void setIEX(Float iEX) {
		IEX = iEX;
	}



	public Float getPXIL() {
		return PXIL;
	}



	public void setPXIL(Float pXIL) {
		PXIL = pXIL;
	}



	public String getDATE() {
		return DATE;
	}

	public void setDATE(String dATE) {
		DATE = dATE;
	}

	@Override
    public String toString() {
        return "ObligationDetails[DATE=" + DATE + ", IEX=" + IEX + ", PXIL=" + PXIL + "]";
    }
	
}
