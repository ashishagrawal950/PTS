package com.pts.model.appned;

import java.util.ArrayList;

/**
 * Created by vaibhav on 31/7/17.
 */

public class YearMonthModel {

    public String title;
    public ArrayList<String> yrMonthList=new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getYrMonthList() {
        return yrMonthList;
    }

    public void setYrMonthList(ArrayList<String> yrMonthList) {
        this.yrMonthList = yrMonthList;
    }

    public void addMonth(String data){
        if(yrMonthList==null){
            yrMonthList=new ArrayList<>();
        }

        yrMonthList.add(data);
    }
}
