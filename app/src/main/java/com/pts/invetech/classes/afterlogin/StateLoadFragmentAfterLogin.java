package com.pts.invetech.classes.afterlogin;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseFragment;
import com.pts.invetech.R;
import com.pts.invetech.dashboardupdate.model.dashboard.DashBoardData;
import com.pts.invetech.dashboardupdate.model.dashboard.RegionLdc;
import com.pts.invetech.dashboardupdate.model.dashboard.Stateload;
import com.pts.invetech.stateload.AdapterStateLoad;
import com.pts.invetech.stateload.StateButtonOrderHolder;
import com.pts.invetech.stateload.StateLoadSorter;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish on 25-01-2017.
 */
public class StateLoadFragmentAfterLogin extends AppBaseFragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private final String URL_DASHBOARD = "https://www.mittalpower.com/pts-client/api/index.php/mobile/getdashboarddata";
    private LinearLayout openLayout;
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private String loginconfig, domain;
    private Stateload stateLoaddata;
    private AdapterStateLoad customList;
    private ArrayList<StateButtonOrderHolder> orderHolder;
    private Spinner spinner_region;
    private LineChart chart;
    private TextView demandColor, schwlColor;
    private LineData lineData;
    private boolean[] gHolder = {true, true};
    private int[] colorArr = {Color.parseColor("#F59783"), Color.parseColor("#CBFF76")};
    private NetworkCallBack callBack = new NetworkCallBack() {

        @Override
        public void onResultObject(Object data, int id) {
            if (data == null) {
//                AppLogger.showToastLong(getActivity(), "Some Error Occurred in StateLoad Data");
                return;
            }

            DashBoardData dashBoardData = (DashBoardData) data;

            stateLoaddata = dashBoardData.getStateload();

            if (getView() != null) {
                setSpinner(getView());
            }
        }
    };
    private View.OnClickListener graphClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (lineData == null) {
                return;
            }
            if (view.getId() == R.id.state_demand) {
                updateArr(1);
            } else if (view.getId() == R.id.state_schwl) {
                updateArr(2);
            }

            chart.setData(getLineDataForGraph());
            chart.animateY(3000);

            demandColor.setBackgroundColor(getDemandColor());
            schwlColor.setBackgroundColor(getSchColor());
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        if (getArguments() != null) {
            stateLoaddata = getArguments().getParcelable(Constant.VALUE_STATELOAD);
        } else {
            int stateId = SharedPrefHandler.getIntData(getActivity().getApplicationContext(), getString(R.string.db_stateid));
            dashBoardApiCall(stateId, "IEX");
        }
//        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
    }

    private void dashBoardApiCall(int stateId, String type) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("state_id", stateId);
            obj.put("type", type);
            AppLogger.showError("tag", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new NetworkHandlerModel(getActivity(), callBack, DashBoardData.class, 1).
                execute(domain + "/pts-client/api/index.php/mobile/getdashboarddata", obj.toString());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_stateload, container, false);

        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivityAfterLogin) getActivity()).open();
            }
        });
        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        lltoplogin.setVisibility(View.GONE);
        lltoplogout.setVisibility(View.VISIBLE);
        tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
        tvlogouttext.setText("Home");

        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(in);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
            }
        });
        setHeader(rootView);
        setGraphClick(rootView);

        if (stateLoaddata != null) {
            setSpinner(rootView);
        }

        return rootView;
    }

    private void setGraphClick(View rootView) {
        LinearLayout demandLay = (LinearLayout) rootView.findViewById(R.id.state_demand);
        demandLay.setOnClickListener(graphClick);

        demandColor = (TextView) rootView.findViewById(R.id.clr_dmd);

        LinearLayout schwlLay = (LinearLayout) rootView.findViewById(R.id.state_schwl);
        schwlLay.setOnClickListener(graphClick);

        schwlColor = (TextView) rootView.findViewById(R.id.clr_sch);

        demandColor.setBackgroundColor(getDemandColor());
        schwlColor.setBackgroundColor(getSchColor());
    }

    private void setClickableBtns(View rootView) {
        orderHolder = new ArrayList<>();

        Button btn = (Button) rootView.findViewById(R.id.btn_state);
        btn.setOnClickListener(this);
        orderHolder.add(new StateButtonOrderHolder(btn));

        Button btn1 = (Button) rootView.findViewById(R.id.btn_generation);
        btn1.setOnClickListener(this);
        orderHolder.add(new StateButtonOrderHolder(btn1));

        Button btn2 = (Button) rootView.findViewById(R.id.btn_demand);
        btn2.setOnClickListener(this);
        orderHolder.add(new StateButtonOrderHolder(btn2));

        Button btn3 = (Button) rootView.findViewById(R.id.btn_schdrawal);
        btn3.setOnClickListener(this);
        orderHolder.add(new StateButtonOrderHolder(btn3));

        Button btn4 = (Button) rootView.findViewById(R.id.btn_actdrawal);
        btn4.setOnClickListener(this);
        orderHolder.add(new StateButtonOrderHolder(btn4));

        Button btn5 = (Button) rootView.findViewById(R.id.btn_deviation);
        btn5.setOnClickListener(this);
        orderHolder.add(new StateButtonOrderHolder(btn5));
    }

    private void setHeader(View rootView) {
        /*lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        Intent in = getActivity().getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");

        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        lltoplogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });*/
    }

    private void setListRegion(View rootView, List<RegionLdc> ldcList) {
        ListView list = (ListView) rootView.findViewById(R.id.lv_realtime_stateload);

        if (customList == null) {
            customList = new AdapterStateLoad(getActivity(), ldcList);
            list.setAdapter(customList);
        } else {
            customList.updateData(ldcList);
        }
    }

    private void setSpinner(View rootView) {

        spinner_region = (Spinner) rootView.findViewById(R.id.spin_stateload);
        spinner_region.setOnItemSelectedListener(this);
        List<String> category = new ArrayList<String>();
        category.add("NRLDC");
        category.add("WRLDC");

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, category);

        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_region.setAdapter(dataAdapter1);

        TextView txtDate = (TextView) rootView.findViewById(R.id.tv_updatetime);
        txtDate.setText("Updated at : " + stateLoaddata.getLdcDate());
    }

    private void setGraphData(View rootView, List<RegionLdc> ldcList) {
        chart = (LineChart) rootView.findViewById(R.id.graph_realtime_stateload);

        lineData = getLineRldc(ldcList, colorArr);

        chart.setDescription("");
        chart.getLegend().setEnabled(false);
        chart.setTouchEnabled(false);

        chart.setData(lineData);
        chart.animateY(3000);

    }

    private LineData getLineRldc(List<RegionLdc> ldcList, int[] ccc) {

        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<Entry> entry = new ArrayList<>();
        String[] xAxis = new String[ldcList.size()];

        ArrayList<LineDataSet> lines = new ArrayList<LineDataSet>();

        int index = 0;
        for (RegionLdc ldc : ldcList) {
            entries.add(new Entry(Float.valueOf(ldc.getDemand()), index));
            entry.add(new Entry(Float.valueOf(ldc.getSchDrawal()), index));
            xAxis[index] = ldc.getState();
            index++;
        }

        LineDataSet lDataSet1 = new LineDataSet(entries, "Demand");
        lDataSet1.setColor(ccc[0]);
        lDataSet1.setCircleColor(ccc[0]);
        lDataSet1.setCircleColorHole(ccc[0]);
        lDataSet1.setFillColor(ccc[0]);
        lDataSet1.setDrawFilled(true);
        lines.add(lDataSet1);

        LineDataSet lDataSet2 = new LineDataSet(entry, "Schedule Drawl");
        lDataSet2.setColor(ccc[1]);
        lDataSet2.setCircleColor(ccc[1]);
        lDataSet2.setCircleColorHole(ccc[1]);
        lDataSet2.setFillColor(ccc[1]);
        lDataSet2.setDrawFilled(true);
        lines.add(lDataSet2);

        return new LineData(xAxis, lines);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (getView() != null) {
            List<RegionLdc> ldcList;
            if (i == 1) {
                ldcList = stateLoaddata.getWRLDC();
            } else {
                ldcList = stateLoaddata.getNRLDC();
            }
            setGraphData(getView(), ldcList);
            setListRegion(getView(), ldcList);

//            resetTop(99);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP
                        && keyCode == KeyEvent.KEYCODE_BACK) {
                    Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                    return true;

                }
                return false;
            }
        });
    }

    /*
        Graph Click
     */

    @Override
    public void onClick(View view) {
        if (orderHolder == null) {
            return;
        }

        switch (view.getId()) {
            case R.id.btn_state:
                sortData(0);
                break;

            case R.id.btn_generation:
                sortData(1);
                break;

            case R.id.btn_demand:
                sortData(2);
                break;

            case R.id.btn_schdrawal:
                sortData(3);
                break;

            case R.id.btn_actdrawal:
                sortData(4);
                break;

            case R.id.btn_deviation:
                sortData(5);
                break;
        }
    }

    /*
        Sort Logic
     */
    private void sortData(int index) {
        StateButtonOrderHolder holder = orderHolder.get(index);

        holder.setIncDec(!holder.isIncDec());

        sortXYZ(holder.isIncDec(), index);

        customList.updateData(getListOnBasis());
        holder.getBtnId().setBackgroundResource(holder.isIncDec() ?
                R.drawable.on_click_backk : R.drawable.on_click_back);
        resetTop(index);
    }

    private List<RegionLdc> getListOnBasis() {
        int id = spinner_region.getSelectedItemPosition();
        if (id == 0) {
            return stateLoaddata.getNRLDC();
        }
        return stateLoaddata.getWRLDC();
    }

    private void sortXYZ(boolean incDec, int index) {
        switch (index) {
            case 0:
                StateLoadSorter.sortState(getListOnBasis(), incDec);
                break;
            case 1:
                StateLoadSorter.sortGeneration(getListOnBasis(), incDec);
                break;
            case 2:
                StateLoadSorter.sortDemand(getListOnBasis(), incDec);
                break;
            case 3:
                StateLoadSorter.sortSchDrawl(getListOnBasis(), incDec);
                break;
            case 4:
                StateLoadSorter.sortActualDrawl(getListOnBasis(), incDec);
                break;
            case 5:
                StateLoadSorter.sortDeviation(getListOnBasis(), incDec);
                break;
        }
    }

    private void resetTop(int num) {

        for (int index = 0; index < orderHolder.size(); index++) {
            if (index != num) {
                StateButtonOrderHolder holder = orderHolder.get(index);
                holder.setIncDec(false);
                holder.getBtnId().setBackgroundResource(R.drawable.on_click_back_init);
            }
        }
    }

    /*
        Color Set
     */

    public int getDemandColor() {
        if (gHolder[0]) {
            return colorArr[0];
        }
        return Color.GRAY;
    }

    public int getSchColor() {
        if (gHolder[1]) {
            return colorArr[1];
        }
        return Color.GRAY;
    }


    public void updateArr(int val) {

        if (!gHolder[0] && !gHolder[1]) {
            gHolder[0] = true;
            gHolder[1] = true;
            return;
        }

        if (val == 1) {
            if (!gHolder[1]) {
                gHolder[0] = true;
                gHolder[1] = true;
                return;
            }
            gHolder[0] = !gHolder[0];
            return;
        }

        if (val == 2) {
            if (!gHolder[0]) {
                gHolder[0] = true;
                gHolder[1] = true;
                return;
            }
            gHolder[1] = !gHolder[1];
            return;
        }
    }

    public LineData getLineDataForGraph() {


        if (gHolder[0] && gHolder[1]) {
            return lineData;
        }

        if (gHolder[0]) {
            return new LineData(lineData.getXVals(), lineData.getDataSetByIndex(0));
        }

        if (gHolder[1]) {
            return new LineData(lineData.getXVals(), lineData.getDataSetByIndex(1));
        }

        return null;
    }
}