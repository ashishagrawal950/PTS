package com.pts.invetech.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Single {

    @SerializedName("iex")
    @Expose
    private List<InnerData> iex = new ArrayList<InnerData>();
    @SerializedName("pxil")
    @Expose
    private List<InnerData> pxil = new ArrayList<InnerData>();

    /**
     * 
     * @return
     *     The iex
     */
    public List<InnerData> getIex() {
        return iex;
    }

    /**
     * 
     * @param iex
     *     The iex
     */
    public void setIex(List<InnerData> iex) {
        this.iex = iex;
    }

    /**
     * 
     * @return
     *     The pxil
     */
    public List<InnerData> getPxil() {
        return pxil;
    }

    /**
     * 
     * @param pxil
     *     The pxil
     */
    public void setPxil(List<InnerData> pxil) {
        this.pxil = pxil;
    }

}
