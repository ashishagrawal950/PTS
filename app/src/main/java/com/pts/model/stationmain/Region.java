
package com.pts.model.stationmain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Region {

    @SerializedName("ERLDC")
    @Expose
    private ERLDC eRLDC;
    @SerializedName("NRLDC")
    @Expose
    private NRLDC nRLDC;
    @SerializedName("SRLDC")
    @Expose
    private SRLDC sRLDC;
    @SerializedName("WRLDC")
    @Expose
    private WRLDC wRLDC;
    @SerializedName("NERLDC")
    @Expose
    private NERLDC nERLDC;

    public ERLDC getERLDC() {
        return eRLDC;
    }

    public void setERLDC(ERLDC eRLDC) {
        this.eRLDC = eRLDC;
    }

    public NRLDC getNRLDC() {
        return nRLDC;
    }

    public void setNRLDC(NRLDC nRLDC) {
        this.nRLDC = nRLDC;
    }

    public SRLDC getSRLDC() {
        return sRLDC;
    }

    public void setSRLDC(SRLDC sRLDC) {
        this.sRLDC = sRLDC;
    }

    public WRLDC getWRLDC() {
        return wRLDC;
    }

    public void setWRLDC(WRLDC wRLDC) {
        this.wRLDC = wRLDC;
    }

    public NERLDC getNERLDC() {
        return nERLDC;
    }

    public void setNERLDC(NERLDC nERLDC) {
        this.nERLDC = nERLDC;
    }

}
