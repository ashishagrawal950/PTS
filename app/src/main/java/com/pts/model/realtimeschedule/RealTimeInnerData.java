
package com.pts.model.realtimeschedule;


import java.util.ArrayList;

public class RealTimeInnerData {

    private String headerString;
    private String regionname;
    private String maxrevision;
    private String totalqtm;
    private String percentage;
    private ArrayList<String> revisionList=new ArrayList<>();

    public ArrayList<String> getRevisionList() {
        return revisionList;
    }

    public void setRevisionList(ArrayList<String> revisionList) {
        this.revisionList = revisionList;
    }

    public String getHeaderString() {
        return headerString;
    }

    public void setHeaderString(String headerString) {
        this.headerString = headerString;
    }

    public void addValueInRevision(String key){
        this.revisionList.add(key);
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getRegionname() {
        return regionname;
    }

    public void setRegionname(String regionname) {
        this.regionname = regionname;
    }

    public String getMaxrevision() {
        return maxrevision;
    }

    public void setMaxrevision(String maxrevision) {
        this.maxrevision = maxrevision;
    }

    public String getTotalqtm() {
        return totalqtm;
    }

    public void setTotalqtm(String totalqtm) {
        this.totalqtm = totalqtm;
    }

    public RealTimeInnerData(){}

    /*public RealTimeInnerData(Parcel in){
        String[] data = new String[3];

        in.readStringArray(data);
        this.regionname = data[0];
        this.name = data[1];
        this.grade = data[2];
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public RealTimeInnerData createFromParcel(Parcel in) {
            return new Student(in);
        }

        public RealTimeInnerData[] newArray(int size) {
            return new RealTimeInnerData[size];
        }
    };*/


}