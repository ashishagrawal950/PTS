
package com.pts.invetech.myscheduling;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Clubbed {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("data")
    @Expose
    private List<ClubbedDatum> data = null;
    @SerializedName("difference")
    @Expose
    private String difference;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ClubbedDatum> getData() {
        return data;
    }

    public void setData(List<ClubbedDatum> data) {
        this.data = data;
    }

    public String getDifference() {
        return difference;
    }

    public void setDifference(String difference) {
        this.difference = difference;
    }

}
