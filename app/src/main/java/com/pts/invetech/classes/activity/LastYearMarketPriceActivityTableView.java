package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.pts.invetech.R;
import com.pts.invetech.customlist.LastYearMarketPriceCustomList;
import com.pts.invetech.customscrollview.widget.NonScrollListView;

import java.util.ArrayList;

public class LastYearMarketPriceActivityTableView extends Activity {

    public boolean currentisTrue = true;
    public boolean lastyearisTrue = true;
    public boolean secondlastyearisTrue = true;
    public boolean isTrue = true;
    private LinearLayout mopenLayout, chart, lliex, llpxil, lltransaction, mopenPopup;
    private Context context;
    private SQLiteDatabase db;
    private LineChart lineChart;
    private String loginconfig;
    private ArrayList<String> currentyear = new ArrayList<String>();
    private ArrayList<String> lastyear = new ArrayList<String>();
    private ArrayList<String> secondlastyear = new ArrayList<String>();
    private ArrayList<String> blockarrayListfrom = new ArrayList<String>();
    private ArrayList<String> date_Array = new ArrayList<String>();
    private LinearLayout llgraphcurrent, llgraphlast, llgraphsecondlast;
    private TextView tvcurrenttext, tvcurrentline, tvlasttext, tvlastline, tvsecondlasttext, tvsecondlastline;
    private String revision_get, date_get;
    private ImageView tvback, tvbackagain;
    private TextView tvcurrenttable, tvlastyeartable, tvsecondlasttable, tvshowintabe, tvblock;
    private NonScrollListView marketpicelist;
    private LinearLayout lltableheading, llaaboveheading;
    private LinearLayout llgraphview, lltableview;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_lastyearmarketpricetableview);

        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        Intent in = getIntent();
        currentyear = in.getStringArrayListExtra("currentyear");
        lastyear = in.getStringArrayListExtra("lastyear");
        secondlastyear = in.getStringArrayListExtra("secondlastyear");
        date_Array = in.getStringArrayListExtra("date_Array");
        loginconfig = in.getStringExtra("LOGINCONFIG");

        blockarrayListfrom.clear();
        for (int i = 1; i <= 96; i++) {
            String numberAsString = Integer.toString(i);
            blockarrayListfrom.add(numberAsString);
        }

        tvshowintabe = (TextView) findViewById(R.id.tvshowintabe);
        //tvshowintabe.setPaintFlags(tvshowintabe.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        lltableheading = (LinearLayout) findViewById(R.id.lltableheading);

        tvcurrenttable = (TextView) findViewById(R.id.tvcurrenttable);
        tvcurrenttable.setText(date_Array.get(0));
        tvlastyeartable = (TextView) findViewById(R.id.tvlastyeartable);
        tvlastyeartable.setText(date_Array.get(1));
        tvsecondlasttable = (TextView) findViewById(R.id.tvsecondlasttable);
        tvsecondlasttable.setText(date_Array.get(2));

        llaaboveheading = (LinearLayout) findViewById(R.id.llaaboveheading);

        tvblock = (TextView) findViewById(R.id.tvblock);

        llgraphview = (LinearLayout) findViewById(R.id.llgraphview);
        lltableview = (LinearLayout) findViewById(R.id.lltableview);
        tvbackagain = (ImageView) findViewById(R.id.tvbackagain);

        marketpicelist = (NonScrollListView) findViewById(R.id.marketpicelist);
        LastYearMarketPriceCustomList adapter = new LastYearMarketPriceCustomList(
                LastYearMarketPriceActivityTableView.this, blockarrayListfrom, currentyear, lastyear, secondlastyear, secondlastyear);
        marketpicelist.setAdapter(adapter);


        //
        tvshowintabe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(LastYearMarketPriceActivityTableView.this, LastYearMarketPriceActivity.class);
                in.putStringArrayListExtra("currentyear", currentyear);
                in.putStringArrayListExtra("lastyear", lastyear);
                in.putStringArrayListExtra("secondlastyear", secondlastyear);
                in.putExtra("LOGINCONFIG", loginconfig);
                in.putStringArrayListExtra("date_Array", date_Array);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);

            }
        });


        tvbackagain.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent in = new Intent(LastYearMarketPriceActivityTableView.this, NewFeedMainActivity.class);
                in.putExtra("LOGINCONFIG", loginconfig);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });
    }


    public void showMessage(String title, String message) {
        Builder builder = new Builder(LastYearMarketPriceActivityTableView.this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }


    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(LastYearMarketPriceActivityTableView.this, NewFeedMainActivity.class);
        in.putExtra("LOGINCONFIG", loginconfig);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }


}
