package com.pts.invetech.customlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

import java.util.ArrayList;

public class StateUrsCustomList extends ArrayAdapter<String> {

    private Activity activity;
    private ArrayList<String> text1;
    private  ArrayList<String> text4;
    private  ArrayList<String> text3;

    public StateUrsCustomList(Activity activity,
                                        ArrayList<String> text1,
                                         ArrayList<String> text4,ArrayList<String> text3) {
        // TODO Auto-generated constructor stub
        super(activity, R.layout.list_item_stateurs, text1);
        this.activity = activity;
        this.text1 = text1;
        this.text4 = text4;
        this.text3 = text3;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_item_stateurs, null, true);
        TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
        TextView txt4 = (TextView) rowView.findViewById(R.id.txt4);
        TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
        txt1.setText(text1.get(position));
        txt3.setText(text3.get(position));
        txt4.setText(text4.get(position));
        //Animation animation = AnimationUtils.loadAnimation(activity, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        //rowView.startAnimation(animation);
        //lastPosition = position;
        //txt3.setText("Delete");
        // imageView.setImageResource(imageId[position]);
        return rowView;
    }
}


