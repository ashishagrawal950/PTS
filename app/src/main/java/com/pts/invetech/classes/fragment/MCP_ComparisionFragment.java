package com.pts.invetech.classes.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.LastYearMarketPriceAPIResponse;
import com.pts.invetech.apiresponse.LastYearMarketPriceWithDateAPIResponse;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.customlist.AdapterMCPComparision;
import com.pts.invetech.customlist.AdapterMCPComparisionThree;
import com.pts.invetech.customlist.AdapterMCPComparisionTwo;
import com.pts.invetech.customlist.StateAdapterMCPComparision;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.dashboardupdate.model.news.NewsDataUpdate;
import com.pts.invetech.dashboardupdate.model.state.State;
import com.pts.invetech.dashboardupdate.model.state.StateList;
import com.pts.invetech.dashboardupdate.views.DashBoardUpdate;
import com.pts.invetech.pojo.LastYearMarketPrice;
import com.pts.invetech.utils.AppDateUtils;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.DatePickerFragmentFour;
import com.pts.invetech.utils.DatePickerFragmentOne;
import com.pts.invetech.utils.DatePickerFragmentThree;
import com.pts.invetech.utils.DatePickerFragmentTwo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Ashish on 20-02-2017.
 */

public class MCP_ComparisionFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private final String URL_LASTYEAR = "https://www.mittalpower.com/mobile/pxs_app/service/lastyearmarketprice.php";
    private final String URL_STATELIST = "https://www.mittalpower.com/mobile/pxs_app/service/getstatenamefromgeo.php";
    private final int CONSTANT_STATE = 2;
    public boolean minisTrue = true;
    public boolean avgisTrue = true;
    public boolean maxisTrue = true;
    private Dialog prgDialog;
    private LinearLayout openLayout;
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private String loginconfig;
    private Calendar calendar;
    private int year, month, day;
    private LineChart newsfeed_graph;
    private LinearLayout lvlastmcpone, lvlastmcptwo, lvlastmcpthree, ll_expand_graph;
    private TextView tvcolorone, tvdateone, tvcolortwo, tvdatetwo, tvcolorthree, tvdatethree;
    private ImageView tvexpand;
    private NewsDataUpdate newsDataUpdate;
    private SQLiteDatabase db;
    private String state_id, device_id;
    private NonScrollListView mainListView;
    private View rootView;
    private RelativeLayout rlone, rltwo, rlfour;
    private LinearLayout rlthree, llnewsfeedlegent;
    private TextView tv_datepickerone, tv_datepickertwo, tv_datepickerthree, tv_datepickerfour, tvviewdata;
    private ImageView tv_crossone, tv_crosstwo, tv_crossthree, tv_crossfour, add_three, addtwo;
    private TextView tvheaderone, tvheadertwo, tvheaderthree, tvheaderfour;
    private LinearLayout llheaderfour, llheaderthree, llheadertwo;
    private TextView tvheaderthreeone, tvheaderthreetwo, tvheaderthreethree, tvheadertwoone, tvheadertwotwo;
    private String typesend;
    private List<String> typearray = new ArrayList<String>();
    private ArrayList<String> dataarraysend = new ArrayList<String>();
    private ArrayList<ArrayList<String>> dataArraya = new ArrayList<ArrayList<String>>();
    private ArrayList<String> date_Array = new ArrayList<String>();
    private ArrayList<String> showdate_Array = new ArrayList<String>();
    private ArrayList<String> min_Array = new ArrayList<String>();
    private ArrayList<String> max_Array = new ArrayList<String>();
    private ArrayList<String> avg_Array = new ArrayList<String>();
    private ArrayList<String> blockarrayListfrom = new ArrayList<String>();
    private ArrayList<String> lastyearmarketprice_Arrayone = new ArrayList<String>();
    private ArrayList<String> lastyearmarketprice_Arraytwo = new ArrayList<String>();
    private ArrayList<String> lastyearmarketprice_Arraythree = new ArrayList<String>();
    private ArrayList<String> lastyearmarketprice_Arrayfour = new ArrayList<String>();
    private List<State> sList;
    private int stateId;
    private StateList stateList;
    private String domain;
    private boolean isFirst = true;
    private NetworkCallBack callBack = new NetworkCallBack() {

        @Override
        public void onResultObject(Object data, int id) {
            if (data == null) {
//                AppLogger.showToastLong(getActivity(), "Some Error Occurred in id - " + id);
                return;
            }
            switch (id) {
                case CONSTANT_STATE:
                    stateList = (StateList) data;
                    if (getView() != null) {
                        setSpinnerRegion(getView(), stateList);
                        try {
                            SharedPrefHandler.saveStateList(getActivity(), stateList.getStates());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }

        @Override
        public void onResultString(String data, int id) {
            if (data == null) {
//                AppLogger.showToastLong(getActivity(), "Some Error Occurred in News");
                return;
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!isFirst){
            domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        }
        stateId = SharedPrefHandler.getIntData(getActivity().getApplicationContext(), getString(R.string.db_stateid));
        state_id = String.valueOf(stateId);
        AppLogger.showMsg("State ID  1", "" + stateId);
        if(isFirst){
            getStatesWoLoc(stateId, "IEX");
        }else{
            getStatesWoLoc(stateId, "IEX");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppLogger.showMsgWithoutTag("On Crete Monthly Atc");
        rootView = inflater.inflate(R.layout.activity_mcp_comparision, container, false);

        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NewFeedMainActivity) getActivity()).open();
            }
        });
        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        prgDialog.setCancelable(false);

        String deviceId = "device_id";
        db = getActivity().openOrCreateDatabase("deviceDBSecond",
                Context.MODE_PRIVATE, null);
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }


        //String statename = SharedPrefHandler.getStringData(getActivity(), getString(R.string.db_statename));

        lvlastmcpone = (LinearLayout) rootView.findViewById(R.id.lvlastmcpone);
        tvcolorone = (TextView) rootView.findViewById(R.id.tvcolorone);
        tvdateone = (TextView) rootView.findViewById(R.id.tvdateone);

        lvlastmcptwo = (LinearLayout) rootView.findViewById(R.id.lvlastmcptwo);
        tvcolortwo = (TextView) rootView.findViewById(R.id.tvcolortwo);
        tvdatetwo = (TextView) rootView.findViewById(R.id.tvdatetwo);


        lvlastmcpthree = (LinearLayout) rootView.findViewById(R.id.lvlastmcpthree);
        tvcolorthree = (TextView) rootView.findViewById(R.id.tvcolorthree);
        tvdatethree = (TextView) rootView.findViewById(R.id.tvdatethree);
        llnewsfeedlegent = (LinearLayout) rootView.findViewById(R.id.llnewsfeedlegent);


        ll_expand_graph = (LinearLayout) rootView.findViewById(R.id.ll_expand_graph);

        newsfeed_graph = (LineChart) rootView.findViewById(R.id.linechart);

        mainListView = (NonScrollListView) rootView.findViewById(R.id.lv_mcp);

        ConnectionDetector cd = new ConnectionDetector(getActivity());
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getActivity(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            if(isFirst){
                new HttpAsyncTasklastyearmarketprice()
                        .execute("https://www.mittalpower.com/mobile/pxs_app/service/lastyearmarketprice.php");
            }
            else{
                new HttpAsyncTasklastyearmarketprice()
                        .execute(domain + "/mobile/pxs_app/service/lastyearmarketprice.php");
            }
        }


        rlone = (RelativeLayout) rootView.findViewById(R.id.rlone);
        tv_datepickerone = (TextView) rootView.findViewById(R.id.tv_datepickerone);
        // tv_crossone=(TextView) rootView.findViewById(R.id.tv_crossone);
        rltwo = (RelativeLayout) rootView.findViewById(R.id.rltwo);
        tv_datepickertwo = (TextView) rootView.findViewById(R.id.tv_datepickertwo);
        // tv_crosstwo=(TextView) rootView.findViewById(R.id.tv_crosstwo);
        rlthree = (LinearLayout) rootView.findViewById(R.id.rlthree);
        tv_datepickerthree = (TextView) rootView.findViewById(R.id.tv_datepickerthree);
        tv_crossthree = (ImageView) rootView.findViewById(R.id.tv_crossthree);
        rlfour = (RelativeLayout) rootView.findViewById(R.id.rlfour);
        tv_datepickerfour = (TextView) rootView.findViewById(R.id.tv_datepickerfour);
        tv_crossfour = (ImageView) rootView.findViewById(R.id.tv_crossfour);
        tvviewdata = (TextView) rootView.findViewById(R.id.tvviewdata);

        add_three = (ImageView) rootView.findViewById(R.id.add_three);
        addtwo = (ImageView) rootView.findViewById(R.id.addtwo);


        llheaderfour = (LinearLayout) rootView.findViewById(R.id.llheaderfour);
        tvheaderone = (TextView) rootView.findViewById(R.id.tvheaderone);
        tvheadertwo = (TextView) rootView.findViewById(R.id.tvheadertwo);
        tvheaderthree = (TextView) rootView.findViewById(R.id.tvheaderthree);
        tvheaderfour = (TextView) rootView.findViewById(R.id.tvheaderfour);

        llheaderthree = (LinearLayout) rootView.findViewById(R.id.llheaderthree);
        tvheaderthreeone = (TextView) rootView.findViewById(R.id.tvheaderthreeone);
        tvheaderthreetwo = (TextView) rootView.findViewById(R.id.tvheaderthreetwo);
        tvheaderthreethree = (TextView) rootView.findViewById(R.id.tvheaderthreethree);

        llheadertwo = (LinearLayout) rootView.findViewById(R.id.llheadertwo);
        tvheadertwoone = (TextView) rootView.findViewById(R.id.tvheadertwoone);
        tvheadertwotwo = (TextView) rootView.findViewById(R.id.tvheadertwotwo);


        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        Intent in = getActivity().getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");

        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        lltoplogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });


        SpinnerIexPxl();
        SwitchGraph();


        tv_datepickerone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DatePickerFragmentOne newFragment = new DatePickerFragmentOne();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        tv_datepickertwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DatePickerFragmentTwo newFragment = new DatePickerFragmentTwo();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        tv_datepickerthree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DatePickerFragmentThree newFragment = new DatePickerFragmentThree();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        tv_datepickerfour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DatePickerFragmentFour newFragment = new DatePickerFragmentFour();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

       /* tv_crossone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                rlone.setVisibility(View.INVISIBLE);
            }
        });

        tv_crosstwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                rltwo.setVisibility(View.INVISIBLE);
            }
        });*/

        addtwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                rlthree.setVisibility(View.VISIBLE);
                addtwo.setVisibility(View.GONE);
                tv_datepickerthree.setVisibility(View.VISIBLE);
                tv_datepickerthree.setText("Add Date");
            }
        });

        tv_crossthree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                rlthree.setVisibility(View.INVISIBLE);
                addtwo.setVisibility(View.VISIBLE);
                tv_datepickerthree.setText("Add Date");
            }
        });

        add_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                rlfour.setVisibility(View.VISIBLE);
                tv_datepickerfour.setVisibility(View.VISIBLE);
                add_three.setVisibility(View.GONE);
                tv_crossthree.setVisibility(View.GONE);
                tv_datepickerfour.setText("Add Date");

            }
        });

        tv_crossfour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                rlfour.setVisibility(View.INVISIBLE);
                add_three.setVisibility(View.VISIBLE);
                tv_crossthree.setVisibility(View.VISIBLE);
                tv_datepickerfour.setText("Add Date");

            }
        });

        tvviewdata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                /*Toast.makeText(getActivity(), typesend, Toast.LENGTH_LONG).show();
                Toast.makeText(getActivity(), tv_datepickerone.getText().toString(), Toast.LENGTH_LONG).show();
                Toast.makeText(getActivity(), tv_datepickertwo.getText().toString(), Toast.LENGTH_LONG).show();
                Toast.makeText(getActivity(), tv_datepickerthree.getText().toString(), Toast.LENGTH_LONG).show();
                Toast.makeText(getActivity(), tv_datepickerfour.getText().toString(), Toast.LENGTH_LONG).show();*/
                dataarraysend.clear();
                if (!(tv_datepickerone.getText().toString().equalsIgnoreCase("Add Date"))) {
                    String one = AppDateUtils.getReverseDate(tv_datepickerone.getText().toString());
                    dataarraysend.add(one);
                }
                if (!(tv_datepickertwo.getText().toString().equalsIgnoreCase("Add Date"))) {
                    String two = AppDateUtils.getReverseDate(tv_datepickertwo.getText().toString());
                    dataarraysend.add(two);
                }
                if (!(tv_datepickerthree.getText().toString().equalsIgnoreCase("Add Date"))) {
                    String three = AppDateUtils.getReverseDate(tv_datepickerthree.getText().toString());
                    dataarraysend.add(three);
                }
                if (!(tv_datepickerfour.getText().toString().equalsIgnoreCase("Add Date"))) {
                    String four = AppDateUtils.getReverseDate(tv_datepickerfour.getText().toString());
                    dataarraysend.add(four);
                }

                ConnectionDetector cd = new ConnectionDetector(getActivity());
                if (!cd.isConnectingToInternet()) {
                    Toast.makeText(getActivity(), "Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    if(isFirst){
                        new HttpAsyncTasklastyearmarketpricewithdate().execute("https://www.mittalpower.com/mobile/pxs_app/service/lastyearmarketprice.php");
                    }else{
                        new HttpAsyncTasklastyearmarketpricewithdate().execute(domain + "/mobile/pxs_app/service/lastyearmarketprice.php");
                    }

                }
            }
        });

        lvlastmcpone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Toast.makeText(MarketPriceActivity.this, "Ashish", Toast.LENGTH_LONG).show();
                if (minisTrue == true) {
                    minisTrue = false;
                    String f = "false";
                    tvdateone.setTextColor(Color.GRAY);
                    tvcolorone.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(MarketPriceActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    minisTrue = true;
                    String t = "true";
                    tvdateone.setTextColor(Color.MAGENTA);
                    tvcolorone.setBackgroundColor(Color.MAGENTA);
                    //Toast.makeText(MarketPriceActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartNav();
            }
        });

        lvlastmcptwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (maxisTrue == true) {
                    maxisTrue = false;
                    String f = "false";
                    tvdatetwo.setTextColor(Color.GRAY);
                    tvcolortwo.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(MarketPriceActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    maxisTrue = true;
                    String t = "true";
                    tvdatetwo.setTextColor(Color.RED);
                    tvcolortwo.setBackgroundColor(Color.RED);
                    //Toast.makeText(MarketPriceActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartNav();
            }
        });

        lvlastmcpthree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (avgisTrue == true) {
                    avgisTrue = false;
                    String f = "false";
                    tvdatethree.setTextColor(Color.GRAY);
                    tvcolorthree.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(MarketPriceActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    avgisTrue = true;
                    String t = "true";
                    tvdatethree.setTextColor(Color.BLUE);
                    tvcolorthree.setBackgroundColor(Color.BLUE);
                    //Toast.makeText(MarketPriceActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartNav();
            }
        });

        return rootView;
    }

    private void SwitchGraph() {

        Switch switch_hide_graph = (Switch) rootView.findViewById(R.id.switch_hide_graph);
        switch_hide_graph.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    newsfeed_graph.setVisibility(View.GONE);
                    llnewsfeedlegent.setVisibility(View.GONE);
                } else {
                    newsfeed_graph.setVisibility(View.VISIBLE);
                    llnewsfeedlegent.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void SpinnerIexPxl() {
        Spinner spinner = (Spinner) rootView.findViewById(R.id.spnr_iex);
        spinner.setOnItemSelectedListener(this);
        typearray.add("IEX");
        typearray.add("PXIL");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_mcp, typearray);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spinner.setAdapter(dataAdapter);


    }

    private void getStatesWoLoc(int stateId, String type) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("state_id", stateId);
            obj.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(isFirst){
            new NetworkHandlerModel(getActivity(), callBack, StateList.class, CONSTANT_STATE).execute(URL_STATELIST, obj.toString());
        }else{
            new NetworkHandlerModel(getActivity(), callBack, StateList.class, CONSTANT_STATE).execute(domain + "/mobile/pxs_app/service/getstatenamefromgeo.php", obj.toString());
        }
    }

    private void setSpinnerRegion(View view, StateList data) {
        Spinner spnr_state = (Spinner) rootView.findViewById(R.id.spnr_state);
        StateAdapterMCPComparision dataAdapterstate = new StateAdapterMCPComparision(getActivity(), data);
        //dataAdapterstate.setDropDownViewResource(R.layout.spinner_text);
        spnr_state.setAdapter(dataAdapterstate);
        int currentStateID = SharedPrefHandler.getIntData(getActivity().getApplicationContext(), getString(R.string.db_stateid));
        spnr_state.setSelection(data.getStateIndexSelected(currentStateID));

        spnr_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                state_id = ((State) adapterView.getSelectedItem()).getId();
                //Toast.makeText(getActivity(), String.valueOf(state_id), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void drawChartNav() {
        ArrayList<String> labels = new ArrayList<String>();
        ArrayList<Entry> entriesmin = new ArrayList<>();
        ArrayList<Entry> entriesmax = new ArrayList<>();
        ArrayList<Entry> entriesavg = new ArrayList<>();

        for (int date = 0; date < date_Array.size(); date++) {
            labels.add(date_Array.get(date));
        }


        for (int min = 0; min < min_Array.size(); min++) {
            entriesmin.add(new Entry(Float.parseFloat(min_Array.get(min)), min));
        }
        LineDataSet datasetmin = new LineDataSet(entriesmin, "Min");
        datasetmin.setColor(Color.MAGENTA);
        datasetmin.setCircleColor(Color.MAGENTA);
        datasetmin.setValueTextColor(Color.MAGENTA);
        datasetmin.setDrawCubic(true);
        datasetmin.setValueTextSize(10f);
        datasetmin.setCircleSize(3f);

        for (int max = 0; max < max_Array.size(); max++) {
            entriesmax.add(new Entry(Float.parseFloat(max_Array.get(max)), max));
        }
        LineDataSet datasetmax = new LineDataSet(entriesmax, "Max");
        datasetmax.setColor(Color.parseColor("#01DADA"));
        datasetmax.setCircleColor(Color.parseColor("#01DADA"));
        datasetmax.setValueTextColor(Color.parseColor("#01DADA"));
        datasetmax.setDrawCubic(true);
        datasetmax.setValueTextSize(10f);
        datasetmax.setCircleSize(3f);

        for (int avg = 0; avg < avg_Array.size(); avg++) {
            entriesavg.add(new Entry(Float.parseFloat(avg_Array.get(avg)), avg));
        }
        LineDataSet datasetavg = new LineDataSet(entriesavg, "Avg");
        datasetavg.setColor(Color.parseColor("#000000"));
        datasetavg.setCircleColor(Color.parseColor("#000000"));
        datasetavg.setValueTextColor(Color.parseColor("#000000"));
        datasetavg.setCircleSize(3f);
        datasetavg.setValueTextSize(10f);
        //datasetavg.setValueTextSize(5f);

		/*ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
	        dataSets.add(datasetmin);
	        dataSets.add(datasetmax);
	        dataSets.add(datasetavg);// add the datasets
	        LineData data = new LineData(labels, dataSets);

	        newsfeed_graph.setData(data); // set the data and list of lables into
	        datasetavg.setDrawCubic(true);

	        newsfeed_graph.setData(data);
	        newsfeed_graph.animateY(2000); */
        // newsfeed_graph.setBackgroundColor(R.color.gray);
        // ((ILineDataSet) newsfeed_graph).getDashPathEffect();


        //tvcolorone,tvdateone,tvcolortwo, tvdatetwo,tvcolorthree,tvdatethree

        if (minisTrue == false && avgisTrue == false && maxisTrue == false) {
            //Toast.makeText(MarketPriceActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            tvdateone.setTextColor(Color.MAGENTA);
            tvcolorone.setBackgroundColor(Color.MAGENTA);
            tvdatetwo.setTextColor(Color.parseColor("#01DADA"));
            tvcolortwo.setBackgroundColor(Color.parseColor("#01DADA"));
            tvdatethree.setTextColor(Color.parseColor("#000000"));
            tvcolorthree.setBackgroundColor(Color.parseColor("#000000"));
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetavg);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            newsfeed_graph.setData(data); // set the data and list of lables into
            newsfeed_graph.animateY(3000);
            minisTrue = true;
            avgisTrue = true;
            maxisTrue = true;
        } else if (minisTrue == false && maxisTrue == false && avgisTrue == true) {
            //iexonlymaxdrawChart();
            LineData datamax = new LineData(labels, datasetavg);
            newsfeed_graph.setData(datamax); // set the data and list of lables
            newsfeed_graph.animateY(3000);
        } else if (minisTrue == false && maxisTrue == true && avgisTrue == false) {
            //iexonlyavgdrawChart();
            LineData dataavg = new LineData(labels, datasetmax);
            newsfeed_graph.setData(dataavg); // set the data and list of lables
            newsfeed_graph.animateY(3000);
        } else if (minisTrue == false && maxisTrue == true && avgisTrue == true) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetavg);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            newsfeed_graph.setData(data); // set the data and list of lables into
            newsfeed_graph.animateY(3000);
        } else if (minisTrue == true && maxisTrue == false && avgisTrue == false) {
            //iexonlymindrawChart();
            LineData datamin = new LineData(labels, datasetmin);
            newsfeed_graph.setData(datamin); // set the data and list of lables
            newsfeed_graph.animateY(3000);
        } else if (minisTrue == true && maxisTrue == false && avgisTrue == true) {
            //iexavgdrawChartMixMAx();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetavg);
            LineData data = new LineData(labels, dataSets);
            newsfeed_graph.setData(data); // set the data and list of lables into
            newsfeed_graph.animateY(3000);
        } else if (minisTrue == true && maxisTrue == true && avgisTrue == false) {
            //iexmaxdrawChartMinAvg();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            newsfeed_graph.setData(data); // set the data and list of lables into
            newsfeed_graph.animateY(3000);
        } else if (minisTrue == true && maxisTrue == true && avgisTrue == true) {
            //Toast.makeText(MarketPriceActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            tvdateone.setTextColor(Color.MAGENTA);
            tvcolorone.setBackgroundColor(Color.MAGENTA);
            tvdatetwo.setTextColor(Color.parseColor("#01DADA"));
            tvcolortwo.setBackgroundColor(Color.parseColor("#01DADA"));
            tvdatethree.setTextColor(Color.parseColor("#000000"));
            tvcolorthree.setBackgroundColor(Color.parseColor("#000000"));
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetavg);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            newsfeed_graph.setData(data); // set the data and list of lables into
            newsfeed_graph.animateY(3000);

        }

        newsfeed_graph.setDescription("Values in Rupees");
        newsfeed_graph.setDescriptionTextSize(2f);
        XAxis xAxis = newsfeed_graph.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(1f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        //newsfeed_graph.setTouchEnabled(false);
        newsfeed_graph.getLegend().setEnabled(false);

        newsfeed_graph.setBackgroundColor(Color.WHITE);

        YAxis axisRight = newsfeed_graph.getAxisRight();
        axisRight.setDrawLabels(true);

        YAxis axisLeft = newsfeed_graph.getAxisLeft();
        axisLeft.setDrawLabels(true);

    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashBoardUpdate dashBoardUpdate = new DashBoardUpdate();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, dashBoardUpdate);
                        fragmentTransaction.commit();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spintype = (Spinner) parent;
        Spinner spinstate = (Spinner) parent;

        if (spintype.getId() == R.id.spnr_iex) {
            typesend = typearray.get(position);
            //Toast.makeText(NewBidActivity.this, recplacedbiddatesend, Toast.LENGTH_LONG).show();
        }

        /*if (spinstate.getId() == R.id.spnr_state) {
            int state_Id_send = stateList.getStateIdSelected();
            //state_id = state_Id_send.getId();
            Toast.makeText(getActivity(), state_Id_send, Toast.LENGTH_LONG).show();
            //Toast.makeText(NewBidActivity.this, recplacedbiddatesend, Toast.LENGTH_LONG).show();
        }*/

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }

    private class HttpAsyncTasklastyearmarketprice extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            LastYearMarketPrice lastYearMarketPrice = new LastYearMarketPrice();
            lastYearMarketPrice.setType("IEX");
            lastYearMarketPrice.setDevice_id(device_id);
            lastYearMarketPrice.setState_id(state_id);
            return LastYearMarketPriceAPIResponse.POST(urls[0], lastYearMarketPrice);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            try {
                JSONArray jsonarray = new JSONArray(result);
                int countlen = jsonarray.length();
                if (countlen > 1) {
                } else {
                }
                int i;
                date_Array.clear();
                showdate_Array.clear();
                min_Array.clear();
                max_Array.clear();
                avg_Array.clear();
                blockarrayListfrom.clear();
                lastyearmarketprice_Arrayone.clear();
                lastyearmarketprice_Arraytwo.clear();
                lastyearmarketprice_Arraythree.clear();
                lastyearmarketprice_Arrayfour.clear();
                for (i = 0; i < jsonarray.length(); i++) {
                    JSONObject json_data = jsonarray.getJSONObject(i);
                    String date = json_data.getString("date");
                    String showdate = json_data.getString("showdate");
                    String min = json_data.getString("min");
                    String max = json_data.getString("max");
                    String avg = json_data.getString("avg");
                    date_Array.add(date);
                    showdate_Array.add(showdate);
                    min_Array.add(min);
                    max_Array.add(max);
                    avg_Array.add(avg);

                    JSONArray dataArray = json_data.getJSONArray("data");
                    if (dataArray.isNull(0)) {
                        //tv_expand_graph.setVisibility(View.GONE);
                        // llnewsfeedlegent.setVisibility(View.GONE);
                    } else {
                        //tv_expand_graph.setVisibility(View.VISIBLE);
                        //  llnewsfeedlegent.setVisibility(View.VISIBLE);
                        for (int j = 0; j < dataArray.length(); j++) {
                            String data = dataArray.getString(j);
                            if (i == 0) {
                                lastyearmarketprice_Arrayone.add(data);
                            } else if (i == 1) {
                                lastyearmarketprice_Arraytwo.add(data);
                            } else if (i == 2) {
                                lastyearmarketprice_Arraythree.add(data);
                            } else if (i == 3) {
                                lastyearmarketprice_Arrayfour.add(data);
                            }
                        }

                        for (int index = 0; index < countlen; index++) {
                            JSONObject object = (JSONObject) jsonarray.get(index);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<String> tempArr = new ArrayList<>();
                            for (int j = 0; j < array.length(); j++) {
                                tempArr.add((String) array.get(j));
                            }
                            dataArraya.add(tempArr);
                        }
                    }
                }

                for (int j = 1; j <= 96; j++) {
                    String numberAsString = Integer.toString(j);
                    blockarrayListfrom.add(numberAsString);
                }

                /*LastYearMarketPriceCustomList adapter = new LastYearMarketPriceCustomList(
                        getActivity(), blockarrayListfrom, lastyearmarketprice_Arrayone, lastyearmarketprice_Arraytwo,
                        lastyearmarketprice_Arraythree, lastyearmarketprice_Arrayfour);*/


                if (date_Array.size() == 2) {
                    tvheadertwoone.setText(date_Array.get(0));
                    tvheadertwotwo.setText(date_Array.get(1));

                    llheaderfour.setVisibility(View.GONE);
                    llheaderthree.setVisibility(View.GONE);
                    llheadertwo.setVisibility(View.VISIBLE);
                    tv_datepickerone.setText(AppDateUtils.getReverseDate(showdate_Array.get(0)));
                    tv_datepickertwo.setText(AppDateUtils.getReverseDate(showdate_Array.get(1)));
                    //rlthree.setVisibility(View.INVISIBLE);
                    // tv_datepickerfour.setVisibility(View.INVISIBLE);
                    AdapterMCPComparisionTwo adapter_MCPComparision = new AdapterMCPComparisionTwo(
                            getActivity(), blockarrayListfrom, lastyearmarketprice_Arrayone, lastyearmarketprice_Arraytwo
                    );
                    mainListView.setAdapter(adapter_MCPComparision);
                }


                if (date_Array.size() == 3) {
                    tvheaderthreeone.setText(date_Array.get(0));
                    tvheaderthreetwo.setText(date_Array.get(1));
                    tvheaderthreethree.setText(date_Array.get(2));

                    llheaderfour.setVisibility(View.GONE);
                    llheaderthree.setVisibility(View.VISIBLE);
                    llheadertwo.setVisibility(View.GONE);

                    tv_datepickerone.setText(AppDateUtils.getReverseDate(showdate_Array.get(0)));
                    tv_datepickertwo.setText(AppDateUtils.getReverseDate(showdate_Array.get(1)));
                    tv_datepickerthree.setText(AppDateUtils.getReverseDate(showdate_Array.get(2)));
                    // tv_datepickerfour.setVisibility(View.INVISIBLE);
                    AdapterMCPComparisionThree adapter_MCPComparision = new AdapterMCPComparisionThree(
                            getActivity(), blockarrayListfrom, lastyearmarketprice_Arrayone, lastyearmarketprice_Arraytwo,
                            lastyearmarketprice_Arraythree);
                    mainListView.setAdapter(adapter_MCPComparision);
                } else if (date_Array.size() == 4) {
                    tvheaderone.setText(date_Array.get(0));
                    tvheadertwo.setText(date_Array.get(1));
                    tvheaderthree.setText(date_Array.get(2));
                    tvheaderfour.setText(date_Array.get(3));

                    llheaderfour.setVisibility(View.VISIBLE);
                    llheaderthree.setVisibility(View.GONE);
                    llheadertwo.setVisibility(View.GONE);

                    tv_datepickerone.setText(AppDateUtils.getReverseDate(showdate_Array.get(0)));
                    tv_datepickertwo.setText(AppDateUtils.getReverseDate(showdate_Array.get(1)));
                    tv_datepickerthree.setText(AppDateUtils.getReverseDate(showdate_Array.get(2)));
                    tv_datepickerfour.setText(AppDateUtils.getReverseDate(showdate_Array.get(3)));
                    // tv_datepickerfour.setVisibility(View.VISIBLE);
                    AdapterMCPComparision adapter_MCPComparision = new AdapterMCPComparision(
                            getActivity(), blockarrayListfrom, lastyearmarketprice_Arrayone, lastyearmarketprice_Arraytwo,
                            lastyearmarketprice_Arraythree, lastyearmarketprice_Arrayfour);
                    mainListView.setAdapter(adapter_MCPComparision);
                }
            } catch (JSONException e) {
                if (result != null) {
                    try {
                        date_Array.clear();
                        //   graphlist.setVisibility(View.GONE);
                        //   newsfeed_graph.setVisibility(View.GONE);
                        //   tv_expand_graph.setVisibility(View.GONE);
                        //   llnewsfeedlegent.setVisibility(View.GONE);
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.has("message")) {
                            String errormassage = jsonObject.getString("message");
                            Toast.makeText(getActivity(), errormassage, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    //Toast.makeText(getActivity(), "Invalid Response from Server.", Toast.LENGTH_LONG).show();
                }
                e.printStackTrace();
            }
            if (date_Array.size() > 1) {
                drawChartNav();
            } else {
                //Toast.makeText(getActivity(), "No Data Found for Graph Preview.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class HttpAsyncTasklastyearmarketpricewithdate extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            LastYearMarketPrice lastYearMarketPrice = new LastYearMarketPrice();
            lastYearMarketPrice.setType(typesend);
            lastYearMarketPrice.setDevice_id(device_id);
            lastYearMarketPrice.setState_id(state_id);
            lastYearMarketPrice.setDatearray(dataarraysend);
            return LastYearMarketPriceWithDateAPIResponse.POST(urls[0], lastYearMarketPrice);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            try {
                JSONArray jsonarray = new JSONArray(result);
                int countlen = jsonarray.length();
                if (countlen > 1) {
                    // graphlist.setVisibility(View.VISIBLE);
                    //  llheading.setVisibility(View.VISIBLE);
                    //  newsfeed_graph.setVisibility(View.VISIBLE);
                    //tv_expand_graph.setVisibility(View.VISIBLE);
                    //   llnewsfeedlegent.setVisibility(View.VISIBLE);
                } else {
                    //   llheading.setVisibility(View.GONE);
                    //   graphlist.setVisibility(View.GONE);
                    //   newsfeed_graph.setVisibility(View.GONE);
                    // tv_expand_graph.setVisibility(View.GONE);
                    //    llnewsfeedlegent.setVisibility(View.GONE);
                }
                int i;
                date_Array.clear();
                showdate_Array.clear();
                min_Array.clear();
                max_Array.clear();
                avg_Array.clear();
                blockarrayListfrom.clear();
                lastyearmarketprice_Arrayone.clear();
                lastyearmarketprice_Arraytwo.clear();
                lastyearmarketprice_Arraythree.clear();
                lastyearmarketprice_Arrayfour.clear();
                for (i = 0; i < jsonarray.length(); i++) {
                    JSONObject json_data = jsonarray.getJSONObject(i);
                    String date = json_data.getString("date");
                    String showdate = json_data.getString("showdate");
                    String min = json_data.getString("min");
                    String max = json_data.getString("max");
                    String avg = json_data.getString("avg");
                    date_Array.add(date);
                    showdate_Array.add(showdate);
                    min_Array.add(min);
                    max_Array.add(max);
                    avg_Array.add(avg);

                    JSONArray dataArray = json_data.getJSONArray("data");
                    if (dataArray.isNull(0)) {
                        //tv_expand_graph.setVisibility(View.GONE);
                        // llnewsfeedlegent.setVisibility(View.GONE);
                    } else {
                        //tv_expand_graph.setVisibility(View.VISIBLE);
                        //  llnewsfeedlegent.setVisibility(View.VISIBLE);
                        for (int j = 0; j < dataArray.length(); j++) {
                            String data = dataArray.getString(j);
                            if (i == 0) {
                                lastyearmarketprice_Arrayone.add(data);
                            } else if (i == 1) {
                                lastyearmarketprice_Arraytwo.add(data);
                            } else if (i == 2) {
                                lastyearmarketprice_Arraythree.add(data);
                            } else if (i == 3) {
                                lastyearmarketprice_Arrayfour.add(data);
                            }
                        }

                        for (int index = 0; index < countlen; index++) {
                            JSONObject object = (JSONObject) jsonarray.get(index);
                            JSONArray array = object.getJSONArray("data");
                            ArrayList<String> tempArr = new ArrayList<>();
                            for (int j = 0; j < array.length(); j++) {
                                tempArr.add((String) array.get(j));
                            }
                            dataArraya.add(tempArr);
                        }
                    }
                }

                for (int j = 1; j <= 96; j++) {
                    String numberAsString = Integer.toString(j);
                    blockarrayListfrom.add(numberAsString);
                }
                /*LastYearMarketPriceCustomList adapter = new LastYearMarketPriceCustomList(
                        getActivity(), blockarrayListfrom, lastyearmarketprice_Arrayone, lastyearmarketprice_Arraytwo,
                        lastyearmarketprice_Arraythree, lastyearmarketprice_Arrayfour);
                mainListView.setAdapter(adapter);*/
                /*if(date_Array.size() == 1){
                    tvheadertwoone.setText(date_Array.get(0));

                    llheaderfour.setVisibility(View.GONE);
                    llheaderthree.setVisibility(View.GONE);
                    llheadertwo.setVisibility(View.VISIBLE);
                    tv_datepickerone.setText(AppDateUtils.getReverseDate(showdate_Array.get(0)));
                    tv_datepickertwo.setText(AppDateUtils.getReverseDate(showdate_Array.get(1)));
                    //rlthree.setVisibility(View.INVISIBLE);
                    // tv_datepickerfour.setVisibility(View.INVISIBLE);
                    AdapterMCPComparisionTwo adapter_MCPComparision = new AdapterMCPComparisionTwo(
                            getActivity(), blockarrayListfrom, lastyearmarketprice_Arrayone, lastyearmarketprice_Arraytwo
                    );
                    mainListView.setAdapter(adapter_MCPComparision);
                }*/

                if (date_Array.size() == 2) {
                    tvheadertwoone.setText(date_Array.get(0));
                    tvheadertwotwo.setText(date_Array.get(1));

                    llheaderfour.setVisibility(View.GONE);
                    llheaderthree.setVisibility(View.GONE);
                    llheadertwo.setVisibility(View.VISIBLE);
                    tv_datepickerone.setText(AppDateUtils.getReverseDate(showdate_Array.get(0)));
                    tv_datepickertwo.setText(AppDateUtils.getReverseDate(showdate_Array.get(1)));
                    //rlthree.setVisibility(View.INVISIBLE);
                    // tv_datepickerfour.setVisibility(View.INVISIBLE);
                    AdapterMCPComparisionTwo adapter_MCPComparision = new AdapterMCPComparisionTwo(
                            getActivity(), blockarrayListfrom, lastyearmarketprice_Arrayone, lastyearmarketprice_Arraytwo
                    );
                    mainListView.setAdapter(adapter_MCPComparision);
                }


                if (date_Array.size() == 3) {
                    tvheaderthreeone.setText(date_Array.get(0));
                    tvheaderthreetwo.setText(date_Array.get(1));
                    tvheaderthreethree.setText(date_Array.get(2));

                    llheaderfour.setVisibility(View.GONE);
                    llheaderthree.setVisibility(View.VISIBLE);
                    llheadertwo.setVisibility(View.GONE);

                    tv_datepickerone.setText(AppDateUtils.getReverseDate(showdate_Array.get(0)));
                    tv_datepickertwo.setText(AppDateUtils.getReverseDate(showdate_Array.get(1)));
                    tv_datepickerthree.setText(AppDateUtils.getReverseDate(showdate_Array.get(2)));
                    // tv_datepickerfour.setVisibility(View.INVISIBLE);
                    AdapterMCPComparisionThree adapter_MCPComparision = new AdapterMCPComparisionThree(
                            getActivity(), blockarrayListfrom, lastyearmarketprice_Arrayone, lastyearmarketprice_Arraytwo,
                            lastyearmarketprice_Arraythree);
                    mainListView.setAdapter(adapter_MCPComparision);
                } else if (date_Array.size() == 4) {
                    tvheaderone.setText(date_Array.get(0));
                    tvheadertwo.setText(date_Array.get(1));
                    tvheaderthree.setText(date_Array.get(2));
                    tvheaderfour.setText(date_Array.get(3));

                    llheaderfour.setVisibility(View.VISIBLE);
                    llheaderthree.setVisibility(View.GONE);
                    llheadertwo.setVisibility(View.GONE);

                    tv_datepickerone.setText(AppDateUtils.getReverseDate(showdate_Array.get(0)));
                    tv_datepickertwo.setText(AppDateUtils.getReverseDate(showdate_Array.get(1)));
                    tv_datepickerthree.setText(AppDateUtils.getReverseDate(showdate_Array.get(2)));
                    tv_datepickerfour.setText(AppDateUtils.getReverseDate(showdate_Array.get(3)));
                    // tv_datepickerfour.setVisibility(View.VISIBLE);
                    AdapterMCPComparision adapter_MCPComparision = new AdapterMCPComparision(
                            getActivity(), blockarrayListfrom, lastyearmarketprice_Arrayone, lastyearmarketprice_Arraytwo,
                            lastyearmarketprice_Arraythree, lastyearmarketprice_Arrayfour);
                    mainListView.setAdapter(adapter_MCPComparision);
                }

            } catch (JSONException e) {
                if (result != null) {
                    try {
                        date_Array.clear();

                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.has("message")) {
                            String errormassage = jsonObject.getString("message");
                            Toast.makeText(getActivity(), errormassage, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    //Toast.makeText(getActivity(), "Invalid Response from Server.", Toast.LENGTH_LONG).show();
                }
                e.printStackTrace();
            }
            if (date_Array.size() > 1) {
                drawChartNav();
            } else {
                //Toast.makeText(getActivity(), "No Data Found for Graph Preview.", Toast.LENGTH_LONG).show();
            }
        }
    }
}