package com.pts.invetech.dashboardupdate.background;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by vaibhav on 13/1/17.
 */

public class NewsResultReceiver extends ResultReceiver {

    private Receiver mReceiver;

    public interface Receiver {
        void onReceiveResult(int resultCode, Bundle resultData);
    }

    public NewsResultReceiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if(mReceiver!=null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }
}
