package com.pts.invetech.classes.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.pts.invetech.APICalling.AppBaseActivity;
import com.pts.invetech.R;
import com.pts.invetech.utils.AppLogger;
import com.pts.model.PowerDemandComparision;
import com.pts.model.PowerDemandOuter;
import com.pts.model.testmodel.InnerTableData;
import com.pts.model.testmodel.OuterTableData;

import java.util.ArrayList;

public class PowerDemandGraphActivity extends AppBaseActivity {

    private TextView tabDemand, tabPeak;
    private int TAB_MODE = 0;

    private int[] GRAPH_COLOR = {Color.RED, Color.BLUE, Color.GREEN,
            Color.YELLOW, Color.MAGENTA, Color.BLACK, Color.CYAN};

    private ArrayList<PowerDemandOuter> graphData;
    private ArrayList<OuterTableData> tableData;

    private BarChart mLineChart;
    private RelativeLayout tableholder;
    private ImageView backFull;
    private LinearLayout date_holder;
    private int COLUMN = 4;
    private View.OnClickListener tabListner = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (graphData == null || graphData.isEmpty()) {
                AppLogger.showToastShort(getBaseContext(), getString(R.string.nodata));
                return;
            }

            if (v.getId() == R.id.tab_demand_full) {
                TAB_MODE = 0;

                tabDemand.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tabDemand.setTextColor(Color.WHITE);
                tabPeak.setTextColor(Color.BLACK);
                tabPeak.setBackgroundColor(Color.WHITE);
            } else if (v.getId() == R.id.tab_peak_full) {
                TAB_MODE = 1;
                tabPeak.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tabPeak.setTextColor(Color.WHITE);
                tabDemand.setTextColor(Color.BLACK);
                tabDemand.setBackgroundColor(Color.WHITE);
            }

            drawDemandGraph(graphData);
            drawTableNew(tableData);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_power_demand_graph);

        tabDemand = (TextView) findViewById(R.id.tab_demand_full);
        tabDemand.setOnClickListener(tabListner);
        tabPeak = (TextView) findViewById(R.id.tab_peak_full);
        tabPeak.setOnClickListener(tabListner);
        date_holder = (LinearLayout) findViewById(R.id.date_holder);

        mLineChart = (BarChart) findViewById(R.id.graph_full_data);

        tableholder = (RelativeLayout) findViewById(R.id.tableholder);

        backFull = (ImageView) findViewById(R.id.graph_full_back);
        backFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        if (intent.hasExtra("graphdata") && intent.hasExtra("tableData")) {
            graphData = intent.getParcelableArrayListExtra("graphdata");
            tableData = intent.getParcelableArrayListExtra("tableData");
            if (graphData != null && tableData != null) {
                drawDemandGraph(graphData);
                drawTableNew(tableData);
                drawBottomLabels(graphData);
            }
        }

    }

    private void drawBottomLabels(ArrayList<PowerDemandOuter> graphData) {

        date_holder.removeAllViews();

        int outerSize = (graphData.size() / COLUMN) + (graphData.size() % COLUMN);
        int init = 0;

        for (int index = 0; index < outerSize; index++) {
            LinearLayout midLAy = new LinearLayout(this);
            midLAy.setOrientation(LinearLayout.HORIZONTAL);

            for (int inY = init; inY < graphData.size(); inY++) {
                TextView tmpVu = new TextView(this);
                tmpVu.setText(graphData.get(inY).getYear());

                midLAy.addView(tmpVu);
            }

            date_holder.addView(midLAy);
        }
    }

    private void drawDemandGraph(ArrayList<PowerDemandOuter> powerDemandComparisiondata) {

        ArrayList<String> labelsStates = new ArrayList<String>();

        ArrayList<ArrayList<BarEntry>> entryHolder = new ArrayList<>();

        for (int index = 0; index < powerDemandComparisiondata.size(); index++) {
            PowerDemandOuter oTst = powerDemandComparisiondata.get(index);
            ArrayList<BarEntry> tmpArr = new ArrayList<>();
            ArrayList<PowerDemandComparision> iModl = oTst.getPdCompareList();

            for (int y = 0; y < iModl.size(); y++) {
                PowerDemandComparision iiiMod = iModl.get(y);
                if (TAB_MODE == 0) {
                    tmpArr.add(new BarEntry(Float.valueOf(iiiMod.getDemand()), y));
                } else {
                    tmpArr.add(new BarEntry(Float.valueOf(iiiMod.getPeak()), y));
                }
                if (!labelsStates.contains(iiiMod.getState())) {
                    labelsStates.add(iiiMod.getState());
                }
            }
            entryHolder.add(tmpArr);
        }

        ArrayList<BarDataSet> aLineDataList = new ArrayList<>();

        for (int index = 0; index < entryHolder.size(); index++) {
            int cCode = GRAPH_COLOR[index % GRAPH_COLOR.length];
            BarDataSet dataset_first = new BarDataSet(entryHolder.get(index),
                    powerDemandComparisiondata.get(index).getYear());
            //dataset_first.setCircleColor(cCode);
            dataset_first.setColor(cCode);
            dataset_first.setDrawValues(false);
            aLineDataList.add(dataset_first);
        }

        BarData lineDataset = new BarData(labelsStates, aLineDataList);
        lineDataset.setValueTextSize(10);

        mLineChart.setData(lineDataset);
        mLineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry entry, int i, Highlight highlight) {
                if (entry != null) {
                    AppLogger.showToastLong(PowerDemandGraphActivity.this, String.valueOf(entry.getVal()));
                }
            }

            @Override
            public void onNothingSelected() {

            }
        });
        mLineChart.setDescription("");
        mLineChart.getAxisLeft().setEnabled(true);
        mLineChart.getAxisRight().setEnabled(false);
        mLineChart.getLegend().setEnabled(true);

//        mLineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mLineChart.getXAxis().setEnabled(false);
        mLineChart.invalidate();
    }

    private void drawTableNew(ArrayList<OuterTableData> outerTestList) {

        TableLayout tableLay = (TableLayout) tableholder.findViewById(R.id.tablelayoutfull);
        tableLay.removeAllViews();

        TableRow.LayoutParams mParams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 1f);

        TableRow headerRow = new TableRow(this);

        TextView outtt = new TextView(this);
        outtt.setText("*");
        outtt.setMaxLines(1);
        outtt.setBackgroundResource(R.drawable.valuecellborder);
        outtt.setTextColor(Color.WHITE);
        outtt.setGravity(Gravity.CENTER);

        headerRow.addView(outtt, mParams);

        ArrayList<InnerTableData> tmpInnList = outerTestList.get(0).getInnerModelList();

        for (InnerTableData inMod : tmpInnList) {
            TextView testVu = new TextView(this);
            testVu.setMaxLines(1);
            testVu.setTypeface(null, Typeface.BOLD);
            testVu.setBackgroundResource(R.drawable.valuecellborder);
            testVu.setTextColor(Color.WHITE);
            testVu.setGravity(Gravity.CENTER);

            testVu.setText(String.valueOf(inMod.getYear()));
            headerRow.addView(testVu, mParams);
        }

        tableLay.addView(headerRow);

        for (OuterTableData oTest : outerTestList) {
            TableRow innRow = new TableRow(this);

            TextView stateName = new TextView(this);
            stateName.setText(oTest.getStateName());
            stateName.setMaxLines(1);
            stateName.setTypeface(null, Typeface.BOLD);
            stateName.setBackgroundResource(R.drawable.valuecellborder);
            stateName.setTextColor(Color.WHITE);
            stateName.setGravity(Gravity.CENTER);

            innRow.addView(stateName, mParams);

            ArrayList<InnerTableData> innMod = oTest.getInnerModelList();

            for (InnerTableData tmpInn : innMod) {

                TextView innDmd = new TextView(this);
                if (TAB_MODE == 0) {
                    innDmd.setText(tmpInn.getDemand());
                } else {
                    innDmd.setText(tmpInn.getPeak());
                }
                innDmd.setMaxLines(1);
                innDmd.setGravity(Gravity.CENTER);
                innDmd.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);
                innDmd.setBackgroundResource(R.drawable.valuecellborder_white);

                innRow.addView(innDmd, mParams);
            }

            tableLay.addView(innRow);
        }

    }
}
