package com.pts.invetech.customlist;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.pojo.MCP_Pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mustajeeb on 4/18/2016.
 */
public class AdapterMCPComparision extends ArrayAdapter<String> {
    private final Activity context;
    static ArrayList<String> blockarrayListfrom = new ArrayList<String>();
    static ArrayList<String> lastyearmarketprice_Arrayone = new ArrayList<String>();
    static ArrayList<String> lastyearmarketprice_Arraytwo = new ArrayList<String>();
    static ArrayList<String> lastyearmarketprice_Arraythree = new ArrayList<String>();
    static ArrayList<String> lastyearmarketprice_Arrayfour = new ArrayList<String>();

    public AdapterMCPComparision(Activity context, ArrayList<String> blockarrayListfrom,
                                 ArrayList<String> lastyearmarketprice_Arrayone
            , ArrayList<String> lastyearmarketprice_Arraytwo
            , ArrayList<String> lastyearmarketprice_Arraythree, ArrayList<String> lastyearmarketprice_Arrayfour
    ) {
        super(context, R.layout.listitem_mcp_list, blockarrayListfrom);
        this.blockarrayListfrom = blockarrayListfrom;
        this.lastyearmarketprice_Arrayone = lastyearmarketprice_Arrayone;
        this.lastyearmarketprice_Arraytwo = lastyearmarketprice_Arraytwo;
        this.lastyearmarketprice_Arraythree = lastyearmarketprice_Arraythree;
        this.lastyearmarketprice_Arrayfour = lastyearmarketprice_Arrayfour;
        this.context = context;

    }


   /* public AdapterMCPComparision(Activity context, ArrayList<String> blockarrayList,
                                 ArrayList<String> actualarrayList
            , ArrayList<String> curtailmentarrayList
            , ArrayList<String> revisedarrayList) {
        super(context, R.layout.list_lastyearmarketprice, blockarrayList);
        this.blockarrayList = blockarrayList;
        this.actualarrayList = actualarrayList;
        this.curtailmentarrayList = curtailmentarrayList;
        this.revisedarrayList = revisedarrayList;
        this.context = context;

    }*/

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View rowView = inflater.inflate(R.layout.listitem_mcp_list, null, true);
        TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
        TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
        TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
        TextView txt4 = (TextView) rowView.findViewById(R.id.txt4);
        TextView txt5 = (TextView) rowView.findViewById(R.id.txt5);

        txt1.setText(blockarrayListfrom.get(position));
        txt2.setText(lastyearmarketprice_Arrayone.get(position));
        txt3.setText(lastyearmarketprice_Arraytwo.get(position));
        txt4.setText(lastyearmarketprice_Arraythree.get(position));
        txt5.setText(lastyearmarketprice_Arrayfour.get(position));


        return rowView;
    }


}
























/* extends ArrayAdapter<MCP_Pojo> {
    private static final String TAG = "MCP_Pojo";
    private List<MCP_Pojo> cardList = new ArrayList<MCP_Pojo>();

    static class CardViewHolder {
        TextView date;
        TextView text;

    }

    public AdapterMCPComparision(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public void add(MCP_Pojo object) {
        cardList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.cardList.size();
    }

    @Override
    public MCP_Pojo getItem(int index) {
        return this.cardList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CardViewHolder viewHolder;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.listitem_mcp_list, parent, false);
            viewHolder = new CardViewHolder();
            viewHolder.date = (TextView) row.findViewById(R.id.tv_dateMCP);
            viewHolder.text = (TextView) row.findViewById(R.id.textmcp);

            row.setTag(viewHolder);
        } else {
            viewHolder = (CardViewHolder)row.getTag();
        }
        MCP_Pojo card = getItem(position);
        viewHolder.date.setText(card.getLine1());
        viewHolder.text.setText(card.getLine2());

        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}*/