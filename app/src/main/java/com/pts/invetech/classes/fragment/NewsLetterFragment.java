package com.pts.invetech.classes.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.NewsLetterAPIResponse;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.classes.afterlogin.NewsLetterPDFshowActivityfromFragment;
import com.pts.invetech.customlist.NewsLetterFragmentCustomList;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.pojo.NewLetterDetails;
import com.pts.invetech.pojo.NewsLetter;
import com.pts.invetech.utils.Constant;
import com.pts.invetech.utils.JSONUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Ashish on 19-10-2016.
 */

public class NewsLetterFragment extends Fragment {
    boolean bResponse = false;
    private LinearLayout openLayout;
    private View rootView;
    private String[] mTestArray;
    private LinearLayout mopenLayout, newsletterlistheading;
    private TextView fromDateEtxt, toDateEtxt, etxt_fromdate_set, etxt_todate_set, tvnewsletternull, tvnewsletternulltext;
    private SQLiteDatabase db;
    private String datefrom, dateto, dbacess;
    private Button btreportgo;
    private Dialog prgDialog;
    private ProgressDialog prgDialog1;
    private String part1, part2;
    private NonScrollListView newsletterlist;
    private NewLetterDetails mypojo;
    private HashMap<Integer, NewLetterDetails> myHashmap;
    private String[] serialnumberadapter, dateadapter, urladapter, textadapter;
    private NotificationManager myNotificationManager;
    private int notificationIdOne = 111;
    private int numMessagesOne = 0;
    private String partsix;
    private ArrayList<String> serialnumber = new ArrayList<String>();
    private ArrayList<String> newsletterdate = new ArrayList<String>();
    private ArrayList<String> newsletterurl = new ArrayList<String>();
    private ArrayList<String> newslettertext = new ArrayList<String>();

    /*protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_newsletter);*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        newsletterBaseUrl = SharedPrefHandler.getDeviceString(getActivity(), "newsletterBaseUrl");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_newsletter, container,
                false);
			/*prgDialog = new ProgressDialog(NewsLetterActivity.this);
		    prgDialog.setMessage("Please wait...");
		    prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        prgDialog1 = new ProgressDialog(getActivity());
        prgDialog1.setMessage("Downloading...");
        prgDialog1.setCancelable(false);

        myHashmap = new HashMap<Integer, NewLetterDetails>();

        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NewFeedMainActivity) getActivity()).open();
            }
        });

        fromDateEtxt = (TextView) rootView.findViewById(R.id.etxt_fromdate);
        toDateEtxt = (TextView) rootView.findViewById(R.id.etxt_todate);
        etxt_fromdate_set = (TextView) rootView.findViewById(R.id.etxt_fromdate_set);
        etxt_todate_set = (TextView) rootView.findViewById(R.id.etxt_todate_set);
        btreportgo = (Button) rootView.findViewById(R.id.btreportgo);
        tvnewsletternull = (TextView) rootView.findViewById(R.id.tvnewsletternull);
        tvnewsletternulltext = (TextView) rootView.findViewById(R.id.tvnewsletternulltext);
        newsletterlistheading = (LinearLayout) rootView.findViewById(R.id.newsletterlistheading);
        newsletterlist = (NonScrollListView) rootView.findViewById(R.id.newsletterlist);

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -7);
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        // formattedDate have current date/time
        String[] parts = formattedDate.split(" ");
        datefrom = parts[0]; // 004
        String[] ddmmyyfrom = datefrom.split("-");
        String yyf = ddmmyyfrom[0];
        String mmf = ddmmyyfrom[1];
        String ddf = ddmmyyfrom[2];
        fromDateEtxt.setText(ddf + "-" + mmf + "-" + yyf);

        Calendar ccc = Calendar.getInstance();
        System.out.println("Current time => " + ccc.getTime());
        SimpleDateFormat dff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDatee = dff.format(ccc.getTime());
        // formattedDate have current date/time
        String[] parts1 = formattedDatee.split(" ");
        dateto = parts1[0]; // 004
        String[] ddmmyyto = dateto.split("-");
        String yyt = ddmmyyto[0];
        String mmt = ddmmyyto[1];
        String ddt = ddmmyyto[2];
        toDateEtxt.setText(ddt + "-" + mmt + "-" + yyt);

        new HttpAsyncTask().execute(Constant.MPPLNEWSLETTER);


        fromDateEtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectFromDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");

            }
        });

        toDateEtxt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectToDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");

            }
        });

        LinearLayout lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        LinearLayout lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        TextView tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        TextView tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        Intent in = getActivity().getIntent();
        String loginconfig = in.getStringExtra("LOGINCONFIG");

        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        lltoplogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),
                        MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        btreportgo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if ((datefrom != null) && (dateto != null)) {
                    ConnectionDetector cd = new ConnectionDetector(getActivity());
                    if (!cd.isConnectingToInternet()) {
                        Toast.makeText(getActivity(), "Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        new HttpAsyncTask().execute("https://www.mittalsgroup.com/newsletter/services/newsletter_for_app.php");
                    }
                } else {
                    Toast.makeText(getActivity(), "Please fill the all Dates.", Toast.LENGTH_LONG).show();
                }
            }
        });

        return rootView;

    }

    public class SelectFromDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                fromDateEtxt.setText(dayplace + "-" + monthplace + "-" + year);
                etxt_fromdate_set.setText(year + "-" + monthplace + "-"
                        + dayplace);
                datefrom = etxt_fromdate_set.getText().toString();
                // Toast.makeText(getActivity(), "DownloadResult" +
                // as,Toast.LENGTH_LONG).show();
            } else if (month <= 9) {
                String monthplace = "0" + month;
                fromDateEtxt.setText(day + "-" + monthplace + "-" + year);
                etxt_fromdate_set.setText(year + "-" + monthplace + "-" + day);
                datefrom = etxt_fromdate_set.getText().toString();
            } else if (day < 10) {
                // String dayplace = "0" + day;
                fromDateEtxt.setText(day + "-" + month + "-" + year);
                etxt_fromdate_set.setText(year + "-" + month + "-" + day);
                datefrom = etxt_fromdate_set.getText().toString();
            } else {
                fromDateEtxt.setText(day + "-" + month + "-" + year);
                etxt_fromdate_set.setText(year + "-" + month + "-" + day);
                datefrom = etxt_fromdate_set.getText().toString();
            }

            // fromDateEtxt.setText(month + "/" + day + "/" + year);
        }

    }

    public class SelectToDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                toDateEtxt.setText(dayplace + "-" + monthplace + "-" + year);
                etxt_todate_set.setText(year + "-" + monthplace + "-"
                        + dayplace);
                dateto = etxt_todate_set.getText().toString();
                // Toast.makeText(getActivity(), "DownloadResult" +
                // as,Toast.LENGTH_LONG).show();
            } else if (month <= 9) {
                String monthplace = "0" + month;
                toDateEtxt.setText(day + "-" + monthplace + "-" + year);
                etxt_todate_set.setText(year + "-" + monthplace + "-" + day);
                dateto = etxt_todate_set.getText().toString();
            } else if (day < 10) {
                // String dayplace = "0" + day;
                toDateEtxt.setText(day + "-" + month + "-" + year);
                etxt_todate_set.setText(year + "-" + month + "-" + day);
                dateto = etxt_todate_set.getText().toString();
            } else {
                toDateEtxt.setText(day + "-" + month + "-" + year);
                etxt_todate_set.setText(year + "-" + month + "-" + day);
                dateto = etxt_todate_set.getText().toString();
            }
            // toDateEtxt.setText(month + "/" + day + "/" + year);
        }
    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm datefrom, dateto
     */
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            NewsLetter newsLetter = new NewsLetter();
            newsLetter.setDatefrom(datefrom);
            newsLetter.setDateto(dateto);
            return NewsLetterAPIResponse.POST(urls[0], newsLetter);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get NewsLetter Result:", result);
            prgDialog.hide();
            // Toast.makeText(getActivity(), "NewsLetter" +result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    serialnumber.clear();
                    newsletterdate.clear();
                    newsletterurl.clear();
                    newslettertext.clear();
                    JSONArray jsonarray = new JSONArray(result);
                    if (jsonarray.isNull(0)) {
                        Toast.makeText(getActivity(), "Data is empty.", Toast.LENGTH_LONG).show();
                        tvnewsletternull.setBackgroundResource(R.drawable.sad);
                        tvnewsletternulltext.setText("NO News Letter Found from " + datefrom + " to " + dateto + ". Please Select another Dates.");
                        tvnewsletternull.setVisibility(View.VISIBLE);
                        tvnewsletternulltext.setVisibility(View.VISIBLE);
                        newsletterlistheading.setVisibility(View.GONE);
                        newsletterlist.setVisibility(View.GONE);
                    } else {
                        newsletterlistheading.setVisibility(View.VISIBLE);
                        newsletterlist.setVisibility(View.VISIBLE);
                        tvnewsletternull.setVisibility(View.GONE);
                        tvnewsletternulltext.setVisibility(View.GONE);
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject obj = jsonarray.getJSONObject(i);
                            String date = obj.getString("date");
                            String url = obj.getString("url");
                            String text = obj.getString("text");
                            String j = String.valueOf(i + 1);
				        /*mypojo = new NewLetterDetails(j, date, url, text);
					    myHashmap.put(i, mypojo);
					    serialnumber
					    newsletterdate
					    newsletterurl
					    newslettertext
						Collection<NewLetterDetails> c = myHashmap.values();
						Iterator<NewLetterDetails> itr = c.iterator();
						while (itr.hasNext())
						{
							NewLetterDetails value = (NewLetterDetails) itr.next();
							serialnumber.add(value.getJ());
							newsletterdate.add(value.getDate());
							newsletterurl.add(value.getUrl());
							newslettertext.add(value.getText());
						}*/
                            serialnumber.add(j);
                            newsletterdate.add(date);
                            newsletterurl.add(url);
                            newslettertext.add(text);
                        }
					/*serialnumberadapter = serialnumber.toArray(new String[serialnumber.size()]);
					Integer[] intarray=new Integer[serialnumberadapter.length];
				    int i=0;
				    for(String str:serialnumberadapter){
				        intarray[i]=Integer.parseInt(str.trim());//Exception in this line
				        i++;
				    }
				    Arrays.sort(intarray);
				    String[] a=Arrays.toString(intarray).split("[\\[\\]]")[1].split(", ");
				    System.out.println(Arrays.toString(a));

					dateadapter = newsletterdate.toArray(new String[newsletterdate.size()]);
					urladapter = newsletterurl.toArray(new String[newsletterurl.size()]);
					textadapter = newslettertext.toArray(new String[newslettertext.size()]);*/
                        NewsLetterFragmentCustomList adapter = new NewsLetterFragmentCustomList(getActivity(), serialnumber, newsletterdate, newsletterurl,
                                newslettertext);
                        newsletterlist.setAdapter(adapter);
                        //myHashmap.clear();
                        newsletterlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view,
                                                    final int position, long id) {
                                String newsletterurlone = newsletterurl.get(position);
                                String[] part = newsletterurlone.split("/");
                                partsix = part[part.length - 1];
                                partsix = partsix.replace("%20", "_");
                                //String customURL = newsletterurlone;
                                //MyTask task = new MyTask();
                                //Toast.makeText(NewsLetterActivity.this, customURL, Toast.LENGTH_LONG).show();
                                // task.execute(customURL);
                                //if(bResponse==true){
                                new DownloadFileAsync().execute(newsletterurl.get(position));
                                Intent in = new Intent(getActivity(), NewsLetterPDFshowActivityfromFragment.class);
                                in.putExtra("newsletterurl", newsletterurlone);
                                in.putExtra("newsletterfilename", partsix);
                                startActivity(in);
                                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                                // getActivity().finish();
						/*}
						else {
							Toast.makeText(getApplicationContext(), "file Does not Exists: Please Contact to Admin.", Toast.LENGTH_LONG).show();
						}*/
                            }
							/*Notification.Builder  mBuilder = new Notification.Builder(NewsLetterActivity.this);

						      mBuilder.setContentTitle("PTS");
						      if(urladapter[+position].endsWith(".pdf")){
							      mBuilder.setContentText(partsix);
							      }

						      mBuilder.setTicker("Downloading");
						      mBuilder.setSmallIcon(R.drawable.logopts);

						      // Increase notification number every time a new notification arrives
						      mBuilder.setNumber(++numMessagesOne);
						      mBuilder.setAutoCancel(true);
						      // Creates an explicit intent for an Activity in your app
						      Intent myIntent=new Intent();
						      myIntent.setAction(android.content.Intent.ACTION_VIEW);
						      if(urladapter[+position].endsWith(".pdf")){
						      File file =new File("/sdcard/"+partsix);
						      myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
						      }
						      //This ensures that navigating backward from the Activity leads out of the app to Home page

						      TaskStackBuilder stackBuilder = TaskStackBuilder.create(NewsLetterActivity.this);
						      // Adds the back stack for the Intent
						      stackBuilder.addParentStack(NotificationOne.class);

						      // Adds the Intent that starts the Activity to the top of the stack
						      stackBuilder.addNextIntent(myIntent);
						      PendingIntent resultPendingIntent =
						         stackBuilder.getPendingIntent(
						            0,
						            PendingIntent.FLAG_ONE_SHOT //can only be used once
						         );
						      // start the activity when the user clicks the notification text
						      mBuilder.setContentIntent(resultPendingIntent);
						      Notification noti = mBuilder.build();
						      myNotificationManager = (NotificationManager) NewsLetterActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
						      noti.defaults |= Notification.DEFAULT_VIBRATE;
						      noti.defaults |= Notification.DEFAULT_SOUND;
						      noti.flags |= Notification.FLAG_SHOW_LIGHTS;
						      noti.ledARGB = 0xff00ff00;
						      noti.ledOnMS = 300;
						      noti.ledOffMS = 1000;
						      noti.flags |= Notification.FLAG_AUTO_CANCEL;
						      // pass the Notification object to the system
						      myNotificationManager.notify(notificationIdOne, mBuilder.build());*/

                        });
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getActivity(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

        }
    }


    /**
     * Background Async Task to download file
     */
    class DownloadFileAsync extends AsyncTask<String, String, String> {
        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //prgDialog1.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            Random r = new Random();
            int i1 = r.nextInt(80 - 65) + 65;
            String string = f_url[0];
            try {
                if (string.endsWith(".pdf")) {
                    try {
                        URL url = new URL(f_url[0]);
                        URLConnection conection = url.openConnection();
                        conection.connect();
                        // getting file length
                        int lenghtOfFile = conection.getContentLength();

                        // input stream to read file - with 8k buffer
                        InputStream input = new BufferedInputStream(
                                url.openStream(), 8192);

                        // Output stream to write file
                        OutputStream output = new FileOutputStream(
                                Environment.getExternalStorageDirectory().getPath() + "/" + partsix);

                        byte[] data = new byte[1024];
                        long total = 0;
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress(""
                                    + (int) ((total * 100) / lenghtOfFile));

                            // writing data to file
                            output.write(data, 0, count);
                        }

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();

                    } catch (Exception e) {
                        Log.e("Error: ", e.getMessage());
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //prgDialog1.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            //prgDialog1.dismiss();
        }
    }

    private class MyTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Boolean doInBackground(String... params) {

            try {
                HttpURLConnection.setFollowRedirects(false);
                HttpURLConnection con = (HttpURLConnection) new URL(params[0]).openConnection();
                con.setRequestMethod("HEAD");
                System.out.println(con.getResponseCode());
                return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            bResponse = result;
            if (bResponse == true) {
                // Toast.makeText(NewsLetterActivity.this, "File exists!", Toast.LENGTH_SHORT).show();
            } else {
                // Toast.makeText(NewsLetterActivity.this, "File does not exist!", Toast.LENGTH_SHORT).show();
            }
        }
    }

   /* @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
        startActivity(in);
        getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
        getActivity().finish();
        return;
    }*/
}