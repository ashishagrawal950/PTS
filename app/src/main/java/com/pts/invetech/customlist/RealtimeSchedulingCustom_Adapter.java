package com.pts.invetech.customlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.model.realtimedetail.Datum;
import com.pts.model.realtimedetail.RealDetailData;

import java.util.ArrayList;
import java.util.List;

public class RealtimeSchedulingCustom_Adapter extends ArrayAdapter<Datum> {

    private List<Datum> orgData;
    private List<Datum> data;

    private LayoutInflater inflater ;
    private Filter dataFilter;

    static class CardViewHolder {
        TextView tvno;

        TextView tvtrader;
        TextView tvbuyer;
        TextView tvseller;
        TextView tvapproval;
        TextView tvtotalqtm;

        TextView trackingOn;
    }

    public RealtimeSchedulingCustom_Adapter(Context context, RealDetailData realDetailData) {
        super(context, R.layout.listitem_realtimedetails);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        orgData=realDetailData.getData();
        data=realDetailData.getData();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Datum getItem(int index) {
        return data.get(index);
    }

    public List<Datum> getData(){
        return data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CardViewHolder viewHolder;
        if (row == null) {

            row = inflater.inflate(R.layout.listitem_realtimedetails, parent, false);

            viewHolder = new CardViewHolder();

            viewHolder.tvno = (TextView) row.findViewById(R.id.tvno);
            viewHolder.tvbuyer = (TextView) row.findViewById(R.id.tvbuyer);
            viewHolder.tvseller = (TextView) row.findViewById(R.id.tvseller);
            viewHolder.tvapproval = (TextView) row.findViewById(R.id.tvapprov);
            viewHolder.tvtotalqtm = (TextView) row.findViewById(R.id.tvtotalqtm);
            viewHolder.trackingOn = (TextView) row.findViewById(R.id.tracking);
            viewHolder.tvtrader = (TextView) row.findViewById(R.id.tvtrader);

            row.setTag(viewHolder);
        } else {
            viewHolder = (CardViewHolder) row.getTag();
        }
        Datum datum = getItem(position);

        viewHolder.tvno.setText(""+(position+1));
        viewHolder.tvtrader.setText(datum.getTrader());
        viewHolder.tvbuyer.setText(datum.getBuyer());
        viewHolder.tvseller.setText(datum.getSeller());
        viewHolder.tvapproval.setText(datum.getApprovalNo());
        viewHolder.tvtotalqtm.setText(datum.getTotalqtm());

        if(datum.getTrack()==0){
            viewHolder.trackingOn.setVisibility(View.INVISIBLE);
        }else{
            viewHolder.trackingOn.setVisibility(View.VISIBLE);
        }

        return row;
    }

    public void resetData() {
        data=orgData;
    }

    public void updateList(List<Datum> dt){
        data=dt;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        if (dataFilter == null)
            dataFilter = new DatumFilter();

        return dataFilter;
    }

    public class DatumFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            List<Datum> datumList = new ArrayList<Datum>();

            if (constraint == null || constraint.length() == 0) {
                results.values = data;
                results.count = data.size();
            }
            else {
                for (Datum p : data) {
                    if (p.getBuyer().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            p.getSeller().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            p.getTrader().toLowerCase().contains(constraint.toString().toLowerCase()))

                        datumList.add(p);
                }
                results.values = datumList;
                results.count = datumList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,Filter.FilterResults results) {

            if (results.count == 0)
                notifyDataSetInvalidated();
            else {
                data = (List<Datum>) results.values;
                notifyDataSetChanged();
            }
        }
    }
}