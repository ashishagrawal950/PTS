package com.pts.invetech.dashboardupdate.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.dashboardupdate.model.news.SubNews;

import java.util.List;

/**
 * Created by vaibhav on 10/1/17.
 */
public class RecAdapter extends RecyclerView.Adapter<RecAdapter.ViewHolder> {

    private List<SubNews> lList=null;
    private boolean notNull=false;

    public RecAdapter(List<SubNews> key){
        lList=key;

        if(key!=null){
            notNull=true;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_listitem_newsfeed, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(notNull) {
            position = position % lList.size();
            SubNews ls=lList.get(position);

            holder.header.setText(ls.getTitle());
            holder.source.setText(ls.getSource());
            holder.type.setText(ls.getType());
            holder.time.setText(ls.getTime());
            holder.date.setText(ls.getPubdate());
        }
    }



    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView header,source,type,time,date;

        public ViewHolder(View itemView) {
            super(itemView);
            header = (TextView) itemView.findViewById(R.id.nheader);
            source = (TextView) itemView.findViewById(R.id.nsource);
            type = (TextView) itemView.findViewById(R.id.ntype);
            time = (TextView) itemView.findViewById(R.id.tvtime);
            date = (TextView) itemView.findViewById(R.id.tvdate);

        }
    }
}