package com.pts.invetech.customlist;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pts.invetech.R;

public class MainActivityCustomList extends ArrayAdapter<String> {
	private final Activity context;
	static ArrayList<String> teacher_id = new ArrayList<String>();
	static ArrayList<Integer> teacher_pic = new ArrayList<Integer>();
	private int lastPosition = -1;
	public MainActivityCustomList(Activity context, ArrayList<String> teacher_id, ArrayList<Integer> complete_attendancestaff_imglistarray) {
		
			  super(context, R.layout.list_mainactivity, teacher_id);
			  this.context = context;
			  this.teacher_id = teacher_id;
			  this.teacher_pic = complete_attendancestaff_imglistarray;
	}

	

	  @Override
	  public View getView(int position, View view, ViewGroup parent) {
	  LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	  View rowView = inflater.inflate(R.layout.list_mainactivity, null, true);
	  TextView tvcardheading = (TextView) rowView.findViewById(R.id.iviamge);
	  TextView tvcardname = (TextView) rowView.findViewById(R.id.tvtext);
	 
	  tvcardheading.setBackgroundResource(teacher_pic.get(position));
	 // tvcardheading.setb(teacher_pic.get(position));
	  tvcardname.setText(teacher_id.get(position));
	  Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
	  rowView.startAnimation(animation);
	  lastPosition = position;

	
	  return rowView;
	 }


}













/* extends ArrayAdapter<String> {

	private  Activity context;
	private  String[] text1;
	private  String[] text2;
	private  String[] text3;
	private static LayoutInflater inflater = null;
	private int lastPosition = -1;

	public MainActivityCustomList(Activity context, String[] text2, String[] text1) {
		super(context, R.layout.nobid_single_bid, text2);
		this.context = context;
		this.text1 = text1;
		this.text2 = text2;
		this.text3 = text3;
	}

	
	
//	NoBidCustomList adapter = new NoBidCustomList(getActivity(), nobidid,nobidclientid, nobiddate);

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.nobid_single_bid, null, true);
		TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
		TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
		TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
		txt1.setText(text1[position]);
		txt2.setText(text2[position]);
		Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		rowView.startAnimation(animation);
		lastPosition = position;
		//txt3.setText("Delete");
		// imageView.setImageResource(imageId[position]);
		return rowView;
	}
}*/
