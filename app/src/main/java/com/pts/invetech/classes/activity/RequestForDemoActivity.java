package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.RequestForDemoAPIResponse;
import com.pts.invetech.pojo.RequestForDemo;
import com.pts.invetech.utils.JSONUtils;
import com.pts.invetech.utils.Validation;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class RequestForDemoActivity extends Activity {
    private EditText edtfullname, edtcompanyname, edtemail, edtmobile;
    private Button btnsubmit;
    private String fullname, companyname, email, mobile, device_id,domain;
    private ConnectionDetector cd;
    private Dialog prgDialog;
    private SQLiteDatabase db;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_requestfordemo);
		/*prgDialog = new ProgressDialog(this);
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        db = openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientid(clientid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS notificationcount(notificationcount VARCHAR);");

        Cursor c = db.rawQuery("SELECT * FROM device", null);
        if (c.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("uuid: " + c.getString(0) + "\n");
            device_id = c.getString(0);
        }
		domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
        edtfullname = (EditText) findViewById(R.id.edtfullname);
        edtcompanyname = (EditText) findViewById(R.id.edtcompanyname);
        edtemail = (EditText) findViewById(R.id.edtemail);
        edtmobile = (EditText) findViewById(R.id.edtmobile);
        btnsubmit = (Button) findViewById(R.id.btnsubmit);

        btnsubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                fullname = edtfullname.getText().toString();
                companyname = edtcompanyname.getText().toString();
                email = edtemail.getText().toString();
                mobile = edtmobile.getText().toString();
                if (Validation.isFieldEmpty(edtfullname)) {
                    edtfullname.setError("Name is Required");
                } else if (Validation.isFieldEmpty(edtcompanyname)) {
                    edtcompanyname.setError("Company Name is Required");
                } else if (Validation.isFieldEmpty(edtemail)) {
                    edtemail.setError("Email is Required");
                } else if (Validation.isFieldEmpty(edtmobile)) {
                    edtmobile.setError("Mobile Number is Required");
                } else if (Validation.isEmailValid(email)) {
                    edtemail.setError("Fill the correct email");
                } else if (Validation.isMobileValid(mobile)) {
                    edtmobile.setError("Fill the correct mobile");
                } else {
                    cd = new ConnectionDetector(RequestForDemoActivity.this);
                    if (!cd.isConnectingToInternet()) {
                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        new HttpAsyncRequestforDemoTask().execute(domain + "/trading/services/api/registerclient.php");
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        // Display alert message when back button has been pressed
        Intent in = new Intent(RequestForDemoActivity.this, LoginInActivity.class);
        in.putExtra("revision", "revision");
        in.putExtra("date", "date");
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    private class HttpAsyncRequestforDemoTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            RequestForDemo requestForDemo = new RequestForDemo();
            requestForDemo.setDevice_id(device_id);
            requestForDemo.setFullname(fullname);
            requestForDemo.setCompanyname(companyname);
            requestForDemo.setEmail(email);
            requestForDemo.setMobilenumber(mobile);
            return RequestForDemoAPIResponse.POST(urls[0], requestForDemo);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Request For Demo Result:", result);
            //Toast.makeText(getBaseContext(), "result" + result, Toast.LENGTH_LONG).show();
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    String message = mainObject.getString("message");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        edtfullname.setText("");
                        edtcompanyname.setText("");
                        edtemail.setText("");
                        edtmobile.setText("");
                        //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                RequestForDemoActivity.this);
                        alertDialogBuilder
                                .setMessage(message);
                        alertDialogBuilder.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        Intent in = new Intent(RequestForDemoActivity.this, LoginInActivity.class);
                                        in.putExtra("revision", "revision");
                                        in.putExtra("date", "date");
                                        startActivity(in);
                                        overridePendingTransition(R.anim.animation, R.anim.animation2);
                                        finish();
                                    }
                                });

					/*alertDialogBuilder.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// finish();
								}
							});*/
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } else {
                        Toast.makeText(RequestForDemoActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }

}