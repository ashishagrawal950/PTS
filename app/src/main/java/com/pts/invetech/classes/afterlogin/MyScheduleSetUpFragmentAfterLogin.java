package com.pts.invetech.classes.afterlogin;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcel;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.cardview.widget.CardView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.pts.api.ApiParamConvertor;
import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseFragment;
import com.pts.invetech.R;
import com.pts.invetech.adapters.AdapterMyScheduleSetUpList;
import com.pts.invetech.module.myschedule_delete.AlertDFragment;
import com.pts.invetech.module.myschedule_delete.ICommunicatorBundle;
import com.pts.invetech.module.myschedule_delete.SimpleData;
import com.pts.invetech.myscheduling.GetSaveScheduleSetup;
import com.pts.invetech.myscheduling.GetSaveScheduleSetupDatum;
import com.pts.invetech.myscheduling.SaveFilter;
import com.pts.invetech.utils.AppDeviceUtils;
import com.pts.invetech.utils.AppLogger;

import org.json.JSONException;

public class MyScheduleSetUpFragmentAfterLogin extends AppBaseFragment implements AdapterView.OnItemSelectedListener {

    private final String DEL_URL = "https://mittalpower.com/mobile/pxs_app/service/schedule_track/deleteclienttrack.php";
    private final String ACCESS_KEY = "wazmrogqbjqhbkeddvelefdttxpeesabtznkoubqdrxfcbigwnrasvtlsjqxcqyv";
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private String deviceid;
    private Dialog prgDialog;
    private Spinner spn_selectRegion, spn_selectClient;
    private String[] regionSpinner = {"WR", "ER", "NR", "NER", "SR"};
    private TextView btn_setup;
    private CardView crd_setUpAccount;
    private Button btn_saveSetUpAccount;
    private ListView listView;
    private SQLiteDatabase db;
    private String dbacess, regionSend, domain;
    private EditText edt_selectClient;
    private Button btn_save, btn_cancel;
    private View.OnClickListener loadDemandClose = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) v.getTag();

            GetSaveScheduleSetupDatum data = ((AdapterMyScheduleSetUpList) listView
                    .getAdapter()).getItem(position);

            onShowLogoutPopup(data);

        }
    };
    /*
     * Api Calling
     */
    private NetworkCallBack networkCallBack = new NetworkCallBack() {

        @Override
        public void onResultString(String data, int id) {
            if (data == null) {
                //  AppLogger.showToastShort(getActivity(), "Some Error Occurred.");
                return;
            }
            switch (id) {
                case 1:
                    AppLogger.showToastShort(getActivity(), data + "");
                    break;
            }
        }

        @Override
        public void onResultObject(Object data, int id) {
            if (data == null) {
                // AppLogger.showToastShort(getActivity(), "Some Error Occurred.");
                return;
            }
            switch (id) {
                case 1:
                    GetSaveScheduleSetup scheduleList = (GetSaveScheduleSetup) data;
                    setFlowAdapter(scheduleList);
                    break;
                case 2:
                    SaveFilter filter = (SaveFilter) data;
                    AppLogger.showToastShort(getActivity(), filter.getMessage() + "");
                    try {
                        apiGetSaveSchedule();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case 3:
                    SimpleData sData = (SimpleData) data;
                    AppLogger.showToastShort(getContext(), sData.getMessage());
                    if (sData.getStatus().equalsIgnoreCase("SUCCESS")) {
                        try {
                            apiGetSaveSchedule();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }
    };
    private ICommunicatorBundle<GetSaveScheduleSetupDatum> iCom = new ICommunicatorBundle<GetSaveScheduleSetupDatum>() {
        @Override
        public void onDataSend(GetSaveScheduleSetupDatum data) {
            onDeleteApi(data);
        }

        @Override
        public void onDataReject() {

        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {

        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = getActivity().openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM baby", null);
        if (c.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("access_key: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
//        domain = getArguments().getString("domain");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_myschedulesetupaccount, container, false);
        LinearLayout openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivityAfterLogin) getActivity()).open();
            }
        });

        btn_setup = (TextView) rootView.findViewById(R.id.btn_setup);
        crd_setUpAccount = (CardView) rootView.findViewById(R.id.crd_setUpAccount);
        btn_save = (Button) rootView.findViewById(R.id.btn_save);
        btn_cancel = (Button) rootView.findViewById(R.id.btn_cancel);

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        lltoplogin.setVisibility(View.GONE);
        lltoplogout.setVisibility(View.VISIBLE);
        tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
        tvlogouttext.setText("Home");
        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(in);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);

            }
        });

        listView = (ListView) rootView.findViewById(R.id.lv_myScheduleSetUp);
        spn_selectRegion = (Spinner) rootView.findViewById(R.id.spn_selectRegion);
        edt_selectClient = (EditText) rootView.findViewById(R.id.edt_selectClient);
        spn_selectRegion.setOnItemSelectedListener(this);

        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);

        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        prgDialog.setCancelable(false);

        deviceid = AppDeviceUtils.getTelephonyId(getActivity());

        selectRegionSpinner();
        try {
            apiGetSaveSchedule();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        btn_setup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_setup.setVisibility(View.GONE);
                crd_setUpAccount.setVisibility(View.VISIBLE);
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String clientSend = edt_selectClient.getText().toString();
                if (clientSend.length() == 0) {
                    AppLogger.showToastShort(getActivity(), "Please fill the Filter tag");
                } else {
                    try {
                        apiSaveCode(regionSend, clientSend);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    btn_setup.setVisibility(View.VISIBLE);
                    crd_setUpAccount.setVisibility(View.GONE);
                    edt_selectClient.setText("");
                }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_setup.setVisibility(View.VISIBLE);
                crd_setUpAccount.setVisibility(View.GONE);
            }
        });

        return rootView;
    }

    private void selectRegionSpinner() {
        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, regionSpinner);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_selectRegion.setAdapter(aa);
    }

    private void apiGetSaveSchedule() throws JSONException {
        String URL_SAVEFILTER = domain + "/mobile/pxs_app/service/schedule_track/getsaveschedule.php";

        String param = ApiParamConvertor.getSaveSchedule("");

        new NetworkHandlerModel(getActivity(), networkCallBack,
                GetSaveScheduleSetup.class, 1).execute(URL_SAVEFILTER, param);
    }

    private void apiSaveCode(String regionSend, String clientSend) throws JSONException {
        String URL_SAVEFILTER = domain + "/mobile/pxs_app/service/schedule_track/savecode.php";


        String saveParam = ApiParamConvertor.saveCodeApi(
                ACCESS_KEY,
                regionSend, edt_selectClient.getText().toString());

        new NetworkHandlerModel(getActivity(), networkCallBack, SaveFilter.class, 2).execute(URL_SAVEFILTER, saveParam);
    }

    private void setFlowAdapter(GetSaveScheduleSetup scheduleList) {
        AdapterMyScheduleSetUpList adapterone = new AdapterMyScheduleSetUpList(getActivity(),
                scheduleList, loadDemandClose);
        listView.setAdapter(adapterone);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner regionsp = (Spinner) parent;
        if (regionsp.getId() == R.id.spn_selectRegion) {
            regionSend = regionSpinner[position];
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /*
     * Confirm POP up
     */

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP
                        && keyCode == KeyEvent.KEYCODE_BACK) {
                    Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                    return true;
                }

                return false;
            }
        });
    }

    private void onShowLogoutPopup(GetSaveScheduleSetupDatum data) {

        FragmentManager fManager = getActivity().getSupportFragmentManager();
        if (fManager != null) {
            AlertDFragment alertdFragment = new AlertDFragment<GetSaveScheduleSetupDatum>();

            Bundle args = new Bundle();
            args.putParcelable("key", iCom);
            args.putParcelable("data", data);
            alertdFragment.setArguments(args);

            alertdFragment.show(fManager, "Alert Dialog Fragment");
        }
    }

    private void onDeleteApi(GetSaveScheduleSetupDatum data) {
        String param = ApiParamConvertor.getDeleteSchedule(
                ACCESS_KEY,
                data.getId(), data.getRegion(), data.getCode());


        new NetworkHandlerModel(getActivity(), networkCallBack, SimpleData.class, 3)
                .execute(domain + "/mobile/pxs_app/service/schedule_track/deleteclienttrack.php", param);
    }


}