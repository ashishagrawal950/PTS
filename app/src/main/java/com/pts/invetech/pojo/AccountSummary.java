package com.pts.invetech.pojo;

public class AccountSummary {

	private String datefrom;
	private String dateto;
	private String lasttransaction;
	private String access_key;
	public String getDatefrom() {
		return datefrom;
	}
	public void setDatefrom(String datefrom) {
		this.datefrom = datefrom;
	}
	public String getDateto() {
		return dateto;
	}
	public void setDateto(String dateto) {
		this.dateto = dateto;
	}
	public String getAccess_key() {
		return access_key;
	}
	public void setAccess_key(String access_key) {
		this.access_key = access_key;
	}
	public String getLasttransaction() {
		return lasttransaction;
	}
	public void setLasttransaction(String lasttransaction) {
		this.lasttransaction = lasttransaction;
	}
	
	
	
}
