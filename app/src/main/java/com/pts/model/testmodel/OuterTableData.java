package com.pts.model.testmodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Ashish on 28-07-2017.
 */

public class OuterTableData implements Parcelable {

    private String stateName;
    private String stateId;
    private ArrayList<InnerTableData> innerModelList=new ArrayList<>();

    public OuterTableData(){}

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public ArrayList<InnerTableData> getInnerModelList() {
        return innerModelList;
    }

    public void setInnerModelList(ArrayList<InnerTableData> innerModelList) {
        this.innerModelList = innerModelList;
    }

    public void addInnerData(InnerTableData _innerData){
        if(innerModelList==null){
            innerModelList=new ArrayList<>();
        }

        innerModelList.add(_innerData);
    }

    public boolean hasState(String state){
        return stateName.equalsIgnoreCase(state);
    }


    protected OuterTableData(Parcel in) {
        stateName = in.readString();
        stateId=in.readString();
        if (in.readByte() == 0x01) {
            innerModelList = new ArrayList<InnerTableData>();
            in.readList(innerModelList, InnerTableData.class.getClassLoader());
        } else {
            innerModelList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(stateName);
        dest.writeString(stateId);
        if (innerModelList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(innerModelList);
        }
    }

    @SuppressWarnings("unused")
    public static final Creator<OuterTableData> CREATOR = new Creator<OuterTableData>() {
        @Override
        public OuterTableData createFromParcel(Parcel in) {
            return new OuterTableData(in);
        }

        @Override
        public OuterTableData[] newArray(int size) {
            return new OuterTableData[size];
        }
    };

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }
}
