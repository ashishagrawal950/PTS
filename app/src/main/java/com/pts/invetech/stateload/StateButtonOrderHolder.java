package com.pts.invetech.stateload;

import android.widget.Button;

/**
 * Created by vaibhav on 25/1/17.
 */

public class StateButtonOrderHolder {

    private boolean incDec;
    private Button btnId;

    public StateButtonOrderHolder(Button _id){
        incDec=false;
        btnId=_id;
    }

    public boolean isIncDec() {
        return incDec;
    }

    public void setIncDec(boolean incDec) {
        this.incDec = incDec;
    }

    public Button getBtnId() {
        return btnId;
    }

    public void setBtnId(Button btnId) {
        this.btnId = btnId;
    }
}
