package com.pts.invetech.pojo;

/**
 * Created by Ashish on 25-10-2016.
 */

public class MonthlyATC {
    private String device_id;
    private String fy;
    private String type;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getFy() {
        return fy;
    }

    public void setFy(String fy) {
        this.fy = fy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
