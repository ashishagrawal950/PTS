package com.pts.invetech.pojo;

public class NewSaveClientBid {
	private String bidtype;
	private String btype;
	private String blockfrom;
	private String blockto;
	private String price;
	private String bid;
	private String noofblock;
	private String type;
	private String date;
	private String addbidclientsavesaveaccesskey;
	public String getBidtype() {
		return bidtype;
	}
	public void setBidtype(String bidtype) {
		this.bidtype = bidtype;
	}
	public String getBtype() {
		return btype;
	}
	public void setBtype(String btype) {
		this.btype = btype;
	}
	public String getBlockfrom() {
		return blockfrom;
	}
	public void setBlockfrom(String blockfrom) {
		this.blockfrom = blockfrom;
	}
	public String getBlockto() {
		return blockto;
	}
	public void setBlockto(String blockto) {
		this.blockto = blockto;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getNoofblock() {
		return noofblock;
	}
	public void setNoofblock(String noofblock) {
		this.noofblock = noofblock;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAddbidclientsavesaveaccesskey() {
		return addbidclientsavesaveaccesskey;
	}
	public void setAddbidclientsavesaveaccesskey(
			String addbidclientsavesaveaccesskey) {
		this.addbidclientsavesaveaccesskey = addbidclientsavesaveaccesskey;
	}
	
	
	
	
	
}
