package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.pts.invetech.R;

/**
 * Created by Ashish on 19-05-2016.
 */
public class ContactusActivity extends Activity {
    private String loginconfig;
    private TextView tvphonenumber, tvextensionnumber, tvtoolfreenubernumber, tvemail, tvwebsite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_contact_us);

        Intent in = getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");

        ImageView ivDrawer = (ImageView) findViewById(R.id.ivDrawer);
        ivDrawer.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent in = new Intent(ContactusActivity.this, NewFeedMainActivity.class);
                in.putExtra("LOGINCONFIG", loginconfig);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        tvphonenumber = (TextView) findViewById(R.id.tvphonenumber);
        tvphonenumber.setPaintFlags(tvphonenumber.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvextensionnumber = (TextView) findViewById(R.id.tvextensionnumber);
        tvextensionnumber.setPaintFlags(tvextensionnumber.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvtoolfreenubernumber = (TextView) findViewById(R.id.tvtoolfreenubernumber);
        tvtoolfreenubernumber.setPaintFlags(tvtoolfreenubernumber.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvemail = (TextView) findViewById(R.id.tvemail);
        tvemail.setPaintFlags(tvemail.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvwebsite = (TextView) findViewById(R.id.tvwebsite);
        tvwebsite.setPaintFlags(tvwebsite.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvphonenumber.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String tvphonenumbershow = tvphonenumber.getText().toString().trim();
                Intent dial = new Intent();
                dial.setAction("android.intent.action.DIAL");
                dial.setData(Uri.parse("tel:" + tvphonenumbershow));
                startActivity(dial);
            }
        });

        tvextensionnumber.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String tvextensionnumbershow = tvextensionnumber.getText().toString().trim();
                Intent dial = new Intent();
                dial.setAction("android.intent.action.DIAL");
                dial.setData(Uri.parse("tel:" + tvextensionnumbershow));
                startActivity(dial);
            }
        });

        tvtoolfreenubernumber.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String tvtoolfreenubernumbershow = tvtoolfreenubernumber.getText().toString().trim();
                Intent dial = new Intent();
                dial.setAction("android.intent.action.DIAL");
                dial.setData(Uri.parse("tel:" + tvtoolfreenubernumbershow));
                startActivity(dial);
            }
        });

        tvemail.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String tvemailshow = tvemail.getText().toString().trim();
				/*Intent i = new Intent(Intent.ACTION_SEND);
				i.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ tvemailshow });
				//i.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
				//i.putExtra(android.content.Intent.EXTRA_TEXT, text);
				startActivity(Intent.createChooser(i, "Send email"));*/
                Intent intent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {tvemailshow};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                //intent.putExtra(Intent.EXTRA_CC,"cc");
                intent.setType("text/html");
                startActivity(Intent.createChooser(intent, "Send mail"));
            }
        });

        tvwebsite.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String tvwebsiteshow = tvwebsite.getText().toString().trim();
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://" + tvwebsiteshow));
                startActivity(myIntent);
            }
        });


    }


    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(ContactusActivity.this, NewFeedMainActivity.class);
        in.putExtra("LOGINCONFIG", loginconfig);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }
}
