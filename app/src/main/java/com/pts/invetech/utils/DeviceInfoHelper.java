package com.pts.invetech.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

import com.pts.invetech.APICalling.AppBaseActivity;

/**
 * Created by Vaibhav on 23-06-2016.
 */
public class DeviceInfoHelper {

    public static String[] getDeviceHeightWidth(AppBaseActivity activity) {

        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        String[] str = new String[2];
        str[0] = String.valueOf(metrics.heightPixels);
        str[1] = String.valueOf(metrics.widthPixels);

        return str;
    }

    @SuppressLint("MissingPermission")
    public static String getDeviceId(AppBaseActivity activity) {
        String myAndroidDeviceId = "";
        TelephonyManager mTelephony = (TelephonyManager)activity. getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephony.getDeviceId() != null){
            myAndroidDeviceId = mTelephony.getDeviceId();
        }else{
            myAndroidDeviceId = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return myAndroidDeviceId;
    }

//    public String getUniqueID(){
//
//    }

    public static String getDeviceName() {
        return Build.MODEL;
    }

    public static String getDeviceCompany() {
        return Build.MANUFACTURER;
    }

    public static String getDevicePlatform() {
        return "Android";
    }

    public static String getDeviceVersion() {
        return Build.VERSION.RELEASE;
    }

    public static boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);

    }


}