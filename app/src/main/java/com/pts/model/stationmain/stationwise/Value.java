
package com.pts.model.stationmain.stationwise;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {

    @SerializedName("states")
    @Expose
    private List<State> states = null;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("revision")
    @Expose
    private String revision;

    public List<State> getStates() {
        return states;
    }

    public void setStates(List<State> states) {
        this.states = states;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

}
