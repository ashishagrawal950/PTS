package com.pts.model.realtimeschedule;

import java.util.ArrayList;

public class RealTimeData {

    private String date;

    private ArrayList<RealTimeIntermed> innerDatas;

    public RealTimeData(){
        innerDatas =new ArrayList<>();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private void addInnerData(RealTimeInnerData data){
//        ArrayList<RealTimeInnerData> temp=innerDatas.get(innerDatas.size()-1);
//        temp.add(data);
    }

    public RealTimeIntermed getDataByIndex(int index){
        return innerDatas.get(index);
    }

    public void addData(RealTimeIntermed data){
        innerDatas.add(data);
    }

    public void setInnerDatas(ArrayList<RealTimeIntermed> innData){
        innerDatas=innData;
    }

    public ArrayList<RealTimeIntermed> getInnerData(){
        return innerDatas;
    }
}
