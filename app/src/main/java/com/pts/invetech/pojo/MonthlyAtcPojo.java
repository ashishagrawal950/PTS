package com.pts.invetech.pojo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vaibhav on 5/11/16.
 */

public class MonthlyAtcPojo {

    private String month;
    private List<MonthData> monthData;
    private boolean isExpandable;

    {
        monthData = new ArrayList<>();
        month = "NIL";
        isExpandable = false;
    }

    public List<MonthData> getMonthData() {
        return monthData;
    }

    public void setMonthData(List<MonthData> monthData) {
        this.monthData = monthData;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void addMonth(MonthData data) {
        this.monthData.add(data);
    }


    public void addMonthJsonArray(JSONArray arr) {
        try {
            for (int index = 0; index < arr.length(); index++) {
                JSONObject obj = arr.getJSONObject(index);

                MonthData dta = new MonthData();
                dta.setFileName(obj.getString("file_name"));
                dta.setFileAddress(obj.getString("file_address"));
                if(obj.has("timestamp")){
                    dta.setTime(obj.getString("timestamp"));
                }

                monthData.add(dta);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public boolean isExpandable() {
        return isExpandable;
    }

    public void setExpandable(boolean expandable) {
        isExpandable = expandable;
    }

}