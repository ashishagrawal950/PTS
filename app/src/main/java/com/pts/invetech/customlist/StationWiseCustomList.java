package com.pts.invetech.customlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.model.stationmain.stationwise.State;

import java.util.List;

public class StationWiseCustomList extends BaseAdapter {

    private Activity context;
    private  List<State> states;



    public StationWiseCustomList(Activity context, List<State> _states) {
        this.context = context;
        this.states = _states;
    }

    @Override
    public int getCount() {
        return states.size();
    }

    @Override
    public State getItem(int position) {
        return states.get(position);
    }

    @Override
    public long getItemId(int position) {
        return states.get(position).hashCode();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if(view==null){
            LayoutInflater inflater = context.getLayoutInflater();
            view= inflater.inflate(R.layout.listitem_station, null, true);

            holder=new ViewHolder();
            holder.txt2= (TextView) view.findViewById(R.id.txt2);
            holder.txt3= (TextView) view.findViewById(R.id.txt3);

            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }

        State station =getItem(position);

        holder.txt2.setText(station.getName());
        holder.txt3.setText(Integer.toString(station.getQtmvalue()));



        return view;
    }

    class ViewHolder{
        TextView txt2,txt3;

    }
}
