package com.pts.invetech.myscedulegraph;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.invetech.APICalling.AppBaseActivity;
import com.pts.invetech.R;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.myscedulegraph.model.MyScheduleDetailData;
import com.pts.invetech.myscedulegraph.model.MyScheduleGraphData;
import com.pts.invetech.utils.AppDeviceUtils;
import com.pts.invetech.utils.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MySchedulingGraphActivity extends AppBaseActivity {

    private ImageView imgBack;
    private final String SCHEDULING = "https://www.mittalpower.com/mobile/pxs_app/service/scheduling/compareqtmwithcurrentrev.php";

    private NonScrollListView nonScrollList;

    private String dataId = null;
    private LineChart lineChart;
    private TextView prevQtm, currentQtm, prevRevHeader, currentRevHeader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_scheduling_graph);

        Intent intent = getIntent();
        if (intent.hasExtra(Constant.VALUE_DATAID)) {
            dataId = intent.getStringExtra(Constant.VALUE_DATAID);
        } else {
            finish();
            return;
        }

        lineChart = (LineChart) findViewById(R.id.msch_graph);
        nonScrollList = (NonScrollListView) findViewById(R.id.mysch_graph_list);
        prevQtm = (TextView) findViewById(R.id.txt_prev_qtm);

        prevRevHeader = (TextView) findViewById(R.id.grph_head_prev);
        currentRevHeader = (TextView) findViewById(R.id.grph_head_cur);

        currentQtm = (TextView) findViewById(R.id.txt_current_qtm);

        imgBack = (ImageView) findViewById(R.id.msch_graph_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        try {
            getGraphData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getGraphData() throws JSONException {
        JSONObject object = new JSONObject();
        object.put("device_id", AppDeviceUtils.getTelephonyId(this));
        object.put("data_id", dataId);
        new NetworkHandlerModel(this, networkCallBack, MyScheduleGraphData.class, 1).execute(SCHEDULING, object.toString());
    }

    private NetworkCallBack networkCallBack = new NetworkCallBack() {

        @Override
        public void onResultObject(Object data, int id) {
            if (data != null) {
                MyScheduleGraphData graphData = (MyScheduleGraphData) data;
                updateUI(graphData);
                updateGraph(graphData);

                if (graphData.getPreviousRev() != null) {
                    prevQtm.setTextColor(Color.GREEN);
                    prevQtm.setText(" Revision- " + graphData.getPreviousRev());
                    prevQtm.setVisibility(View.VISIBLE);
                    prevRevHeader.setText("R-" + graphData.getPreviousRev());
                } else {
                    prevQtm.setVisibility(View.INVISIBLE);
                    prevRevHeader.setText("R");
                }

                if (graphData.getCurrentRev() != null) {
                    currentQtm.setTextColor(Color.BLUE);
                    currentQtm.setText(" Revision- " + graphData.getCurrentRev());
                    currentRevHeader.setText("R-" + graphData.getCurrentRev());
                    currentQtm.setVisibility(View.VISIBLE);
                } else {
                    currentRevHeader.setText("R");
                    currentQtm.setVisibility(View.INVISIBLE);
                }
            }
        }

    };

    private void updateUI(MyScheduleGraphData graphData) {
        MySchGraphAdapter adapter = new MySchGraphAdapter(this, graphData.getDetail());
        nonScrollList.setAdapter(adapter);
    }

    private void updateGraph(MyScheduleGraphData graphData) {
        if (lineChart == null) {
            return;
        }

        List<MyScheduleDetailData> lineData = graphData.getDetail();

        LineData data = new LineData(setXAxisValues(lineData), getDataSets(lineData));
        lineChart.setData(data);
        lineChart.animateY(3000);


    }

    private ArrayList<String> setXAxisValues(List<MyScheduleDetailData> _data) {
        ArrayList<String> xVals = new ArrayList<String>();

        for (MyScheduleDetailData mData : _data) {
            xVals.add(String.valueOf(mData.getBlock()));
        }

        return xVals;
    }

    private ArrayList<LineDataSet> getDataSets(List<MyScheduleDetailData> _data) {

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();

        if (_data.get(0).getPQtm() != null) {
            ArrayList<Entry> prevEntries = new ArrayList<>();
            for (int index = 0; index < _data.size(); index++) {
                prevEntries.add(new Entry(Float.parseFloat(_data.get(index).getCQtm()), index));
            }
            LineDataSet datasetprevoursday = new LineDataSet(prevEntries, "");
            datasetprevoursday.setCircleColor(Color.BLUE);
            datasetprevoursday.setColor(Color.BLUE);
            datasetprevoursday.setValueTextColor(Color.BLUE);
            datasetprevoursday.setCircleSize(3f);
            dataSets.add(datasetprevoursday);
        }


        if (_data.get(0).getCQtm() != null) {
            ArrayList<Entry> currentEntries = new ArrayList<>();

            for (int index = 0; index < _data.size(); index++) {
                currentEntries.add(new Entry(Float.valueOf(_data.get(index).getPQtm()), index));
            }

            LineDataSet datasetcurrent = new LineDataSet(currentEntries, "");
            datasetcurrent.setCircleColor(Color.GREEN);
            datasetcurrent.setColor(Color.GREEN);
            datasetcurrent.setValueTextColor(Color.GREEN);
            datasetcurrent.setCircleSize(3f);
            dataSets.add(datasetcurrent);
        }


        return dataSets;

    }


}
