package com.pts.model.appned;

/**
 * Created by vaibhav on 1/8/17.
 */

public class AdapterStateModel {
    private String name;
    private String id;
    private String region;
    private boolean isClick=false;

    public AdapterStateModel(String _name,String _id,String _region){
        name=_name;
        isClick=false;
        id=_id;

        region=_region;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
