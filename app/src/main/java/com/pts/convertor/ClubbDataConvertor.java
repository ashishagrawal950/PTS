package com.pts.convertor;

import com.pts.invetech.myscheduling.Clubbed;
import com.pts.invetech.myscheduling.ClubbedDatum;
import com.pts.invetech.myscheduling.GetSchedulingDataClubbedView;
import com.pts.invetech.myscheduling.Unclubbed;
import com.pts.invetech.myscheduling.UnclubbedData;
import com.pts.model.myscheduling.ClubViewData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vaibhav on 29/3/18.
 */

public class ClubbDataConvertor {
    final static char[] cArr={'a','b','c','d','e','f','g','h','i','j','k','l','m',
            'n','o','p','q','r','s','t','u','v','w','x','y','z'};
    public static ArrayList<ClubViewData> getTotalData(GetSchedulingDataClubbedView data){

        ArrayList<ClubViewData> clubViewData=new ArrayList<>();

        List<Clubbed> cData=data.getClubbed();

        boolean club=false;
        int sNo=1;

        if(cData!=null){

        for(int index=0;index<cData.size();index++){
            List<ClubbedDatum> clData=cData.get(index).getData();

            for(int iner=0;iner<clData.size();iner++){
                ClubViewData clView=new ClubViewData();
                if(club){
                    clView.setIsClubbed(1);
                }else{
                    clView.setIsClubbed(2);
                }
                clView.setsNo(String.valueOf(sNo)+"."+cArr[iner]+")");
                clView.setRegion(clData.get(iner).getRegion());
                clView.setBuyer(clData.get(iner).getBuyer());
                clView.setSeller(clData.get(iner).getSeller());
                clView.setApproval(clData.get(iner).getAppNo());
                clView.setQtm(clData.get(iner).getQtm());
                clView.setId(clData.get(iner).getId());
                clView.setPrevId(clData.get(iner).getPreviousId());
                clView.setRevision(clData.get(iner).getRevision());
                clView.setDifference(cData.get(index).getDifference());

                clubViewData.add(clView);
            }

            club=!club;
            sNo++;
        }

        }

        List<Unclubbed> unClubData=data.getUnclubbed();

        if(unClubData!=null) {

            for (int index = 0; index < unClubData.size(); index++) {

                UnclubbedData uData = unClubData.get(index).getData();

                ClubViewData clView = new ClubViewData();
                clView.setIsClubbed(3);
                clView.setsNo(String.valueOf(sNo));
                clView.setRegion(uData.getRegion());
                clView.setBuyer(uData.getBuyer());
                clView.setSeller(uData.getSeller());
                clView.setApproval(uData.getAppNo());
                clView.setQtm(uData.getQtm());
                clView.setId(uData.getId());
                clView.setPrevId(uData.getPreviousId());
                clView.setRevision(uData.getRevision());
                clView.setDifference(unClubData.get(index).getChange());

                clubViewData.add(clView);
                sNo++;
            }
        }

        return clubViewData;
    }

}
