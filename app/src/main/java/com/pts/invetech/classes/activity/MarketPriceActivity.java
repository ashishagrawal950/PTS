package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.MarketAPIResponse;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.customlist.MarketPriceCustomListAgain;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.pojo.Login;
import com.pts.invetech.pojo.MarketPriceIEXDetails;
import com.pts.invetech.pojo.MarketPricePXILDetails;
import com.pts.invetech.utils.JSONUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


public class MarketPriceActivity extends Activity {

    public boolean minisTrue = true;
    public boolean avgisTrue = true;
    public boolean maxisTrue = true;
    public boolean pxilminisTrue = true;
    public boolean pxilavgisTrue = true;
    public boolean pxilmaxisTrue = true;
    private Dialog prgDialog;
    private ArrayAdapter<String> adapter;
    private Context context;
    private MarketPriceIEXDetails myIEXpojo;
    private MarketPricePXILDetails myPXILpojo;
    private LinearLayout mopenLayout;
    private TextView fromDateEtxt, toDateEtxt;
    private NonScrollListView marketpicelist;
    private String[] Array;
    private HashMap<String, MarketPriceIEXDetails> myIEXHashmap;
    private HashMap<String, MarketPricePXILDetails> myPXILHashmap;
    private SQLiteDatabase db;
    private String dbacess;
    private HomeActivity homeActivity;
    private LinearLayout iexmin, iexavg, iexmax;
    private LinearLayout pxilmin, pxilavg, pxilmax;
    private String striexmin, striexavg, striexmax;
    private TextView tviexmintext, tviexminline, tviexavgtext, tviexavgline, tviexmaxtext, tviexmaxline;
    private TextView tvpxilmintext, tvpxilminline, tvpxilavgtext, tvpxilavgline, tvpxilmaxtext, tvpxilmaxline;
    private LineChart iexchart, pxilchart;
    private ConnectionDetector cd;
    private ArrayList<String> iexArr1 = new ArrayList<>();
    private ArrayList<String> iexArr2 = new ArrayList<>();
    private ArrayList<String> iexArr3 = new ArrayList<>();
    private ArrayList<String> iexArr4 = new ArrayList<>();

    private ArrayList<String> pxilArr1 = new ArrayList<>();
    private ArrayList<String> pxilArr2 = new ArrayList<>();
    private ArrayList<String> pxilArr3 = new ArrayList<>();
    private ArrayList<String> pxilArr4 = new ArrayList<>();
    private String domain;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_marketprice);

        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM baby", null);
        if (c.getCount() == 0) {
            showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("access_key: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
		/*prgDialog = new ProgressDialog(MarketPriceActivity.this);
	    prgDialog.setMessage("Please wait...");
	    prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);
        myIEXHashmap = new HashMap<String, MarketPriceIEXDetails>();
        myPXILHashmap = new HashMap<String, MarketPricePXILDetails>();
        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        iexchart = (LineChart) findViewById(R.id.iexchart);
        pxilchart = (LineChart) findViewById(R.id.pxilchart);
        //	fromDateEtxt = (TextView) rootView.findViewById(R.id.etxt_fromdate);
        //	toDateEtxt = (TextView) rootView.findViewById(R.id.etxt_todate);

        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MarketPriceActivity.this, MainActivityAfterLogin.class);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();

            }
        });

		/*fromDateEtxt.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				DialogFragment newFragment = new SelectDateFragment();
				newFragment.show(getFragmentManager(), "DatePicker");

			}
		});*/

		/*toDateEtxt.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				DialogFragment newFragment = new ToDateFragment();
				newFragment.show(getFragmentManager(), "DatePicker");

			}
		});*/

        marketpicelist = (NonScrollListView) findViewById(R.id.marketpicelist);
        iexmin = (LinearLayout) findViewById(R.id.iexmin);
        tviexmintext = (TextView) findViewById(R.id.tviexmintext);
        tviexminline = (TextView) findViewById(R.id.tviexminline);

        iexavg = (LinearLayout) findViewById(R.id.iexavg);
        tviexavgtext = (TextView) findViewById(R.id.tviexavgtext);
        tviexavgline = (TextView) findViewById(R.id.tviexavgline);

        iexmax = (LinearLayout) findViewById(R.id.iexmax);
        tviexmaxtext = (TextView) findViewById(R.id.tviexmaxtext);
        tviexmaxline = (TextView) findViewById(R.id.tviexmaxline);


        pxilmin = (LinearLayout) findViewById(R.id.pxilmin);
        tvpxilmintext = (TextView) findViewById(R.id.tvpxilmintext);
        tvpxilminline = (TextView) findViewById(R.id.tvpxilminline);

        pxilavg = (LinearLayout) findViewById(R.id.pxilavg);
        tvpxilavgtext = (TextView) findViewById(R.id.tvpxilavgtext);
        tvpxilavgline = (TextView) findViewById(R.id.tvpxilavgline);

        pxilmax = (LinearLayout) findViewById(R.id.pxilmax);
        tvpxilmaxtext = (TextView) findViewById(R.id.tvpxilmaxtext);
        tvpxilmaxline = (TextView) findViewById(R.id.tvpxilmaxline);


        iexmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Toast.makeText(MarketPriceActivity.this, "Ashish", Toast.LENGTH_LONG).show();
                if (minisTrue == true) {
                    minisTrue = false;
                    String f = "false";
                    tviexmintext.setTextColor(Color.GRAY);
                    tviexminline.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(MarketPriceActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    minisTrue = true;
                    String t = "true";
                    tviexmintext.setTextColor(Color.GREEN);
                    tviexminline.setBackgroundColor(Color.GREEN);
                    //Toast.makeText(MarketPriceActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartiexgraphAll();
            }
        });

        iexavg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (avgisTrue == true) {
                    avgisTrue = false;
                    tviexavgtext.setTextColor(Color.GRAY);
                    tviexavgline.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(MarketPriceActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    avgisTrue = true;
                    tviexavgtext.setTextColor(Color.CYAN);
                    tviexavgline.setBackgroundColor(Color.CYAN);
                    //Toast.makeText(MarketPriceActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartiexgraphAll();
            }
        });

        iexmax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (maxisTrue == true) {
                    maxisTrue = false;
                    tviexmaxtext.setTextColor(Color.GRAY);
                    tviexmaxline.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(MarketPriceActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    maxisTrue = true;
                    tviexmaxtext.setTextColor(Color.RED);
                    tviexmaxline.setBackgroundColor(Color.RED);
                    //Toast.makeText(MarketPriceActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartiexgraphAll();
            }
        });

        pxilmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Toast.makeText(MarketPriceActivity.this, "Ashish", Toast.LENGTH_LONG).show();
                if (pxilminisTrue == true) {
                    pxilminisTrue = false;
                    tvpxilmintext.setTextColor(Color.GRAY);
                    tvpxilminline.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(MarketPriceActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    pxilminisTrue = true;
                    tvpxilmintext.setTextColor(Color.GREEN);
                    tvpxilminline.setBackgroundColor(Color.GREEN);
                    //Toast.makeText(MarketPriceActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartpxilgraphAll();
            }
        });
        pxilavg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                if (pxilavgisTrue == true) {
                    pxilavgisTrue = false;
                    tvpxilavgtext.setTextColor(Color.GRAY);
                    tvpxilavgline.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(MarketPriceActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    pxilavgisTrue = true;
                    tvpxilavgtext.setTextColor(Color.CYAN);
                    tvpxilavgline.setBackgroundColor(Color.CYAN);
                    //Toast.makeText(MarketPriceActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartpxilgraphAll();
            }
        });

        pxilmax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (pxilmaxisTrue == true) {
                    pxilmaxisTrue = false;
                    tvpxilmaxtext.setTextColor(Color.GRAY);
                    tvpxilmaxline.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(MarketPriceActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    pxilmaxisTrue = true;
                    tvpxilmaxtext.setTextColor(Color.RED);
                    tvpxilmaxline.setBackgroundColor(Color.RED);
                    //Toast.makeText(MarketPriceActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartpxilgraphAll();
            }
        });


        cd = new ConnectionDetector(MarketPriceActivity.this);
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            //alert.showAlertDialog(SplashScreenActivity.this,"Internet Connection Error","Please connect to working Internet connection", false);
            // stop executing code by return
            Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            new HttpAsyncTask()
                    .execute(domain + "/mobile/pxs_app/html/IEXmarketclearingprice.php");
        }

    }

    public void drawChartiexgraphAll() {
        ArrayList<String> labels = new ArrayList<String>();
        ArrayList<Entry> entriesmin = new ArrayList<>();
        ArrayList<Entry> entriesavg = new ArrayList<>();
        ArrayList<Entry> entriesmax = new ArrayList<>();
        for (int date = 0; date < iexArr1.size(); date++) {
            labels.add(iexArr1.get(date));
        }
        for (int min = 0; min < iexArr2.size(); min++) {
            entriesmin.add(new Entry(Float.parseFloat(iexArr2.get(min)), min));
        }
        LineDataSet datasetmin = new LineDataSet(entriesmin, "");
        datasetmin.setColor(Color.GREEN);
        datasetmin.setCircleColor(Color.GREEN);
        datasetmin.setValueTextColor(Color.GREEN);
        datasetmin.setDrawCubic(true);

        for (int avg = 0; avg < iexArr3.size(); avg++) {
            entriesavg.add(new Entry(Float.parseFloat(iexArr3.get(avg)), avg));
        }
        LineDataSet datasetavg = new LineDataSet(entriesavg, "");
        datasetavg.setColor(Color.CYAN);
        datasetavg.setCircleColor(Color.CYAN);
        datasetavg.setValueTextColor(Color.CYAN);
        datasetavg.setDrawCubic(true);

        for (int max = 0; max < iexArr4.size(); max++) {
            entriesmax.add(new Entry(Float.parseFloat(iexArr4.get(max)), max));
        }
        LineDataSet datasetmax = new LineDataSet(entriesmax, "");
        datasetmax.setColor(Color.RED);
        datasetmax.setCircleColor(Color.RED);
        datasetmax.setValueTextColor(Color.RED);

        if (minisTrue == false && avgisTrue == false && maxisTrue == false) {
            //Toast.makeText(MarketPriceActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            tviexmintext.setTextColor(Color.GREEN);
            tviexminline.setBackgroundColor(Color.GREEN);
            tviexavgtext.setTextColor(Color.CYAN);
            tviexavgline.setBackgroundColor(Color.CYAN);
            tviexmaxtext.setTextColor(Color.RED);
            tviexmaxline.setBackgroundColor(Color.RED);
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetavg);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            iexchart.setData(data); // set the data and list of lables into
            iexchart.animateY(3000);
            minisTrue = true;
            avgisTrue = true;
            maxisTrue = true;
        } else if (minisTrue == false && avgisTrue == false && maxisTrue == true) {
            //iexonlymaxdrawChart();
            LineData datamax = new LineData(labels, datasetmax);
            iexchart.setData(datamax); // set the data and list of lables
            iexchart.animateY(3000);
        } else if (minisTrue == false && avgisTrue == true && maxisTrue == false) {
            //iexonlyavgdrawChart();
            LineData dataavg = new LineData(labels, datasetavg);
            iexchart.setData(dataavg); // set the data and list of lables
            iexchart.animateY(3000);
        } else if (minisTrue == false && avgisTrue == true && maxisTrue == true) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetavg);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            iexchart.setData(data); // set the data and list of lables into
            iexchart.animateY(3000);
        } else if (minisTrue == true && avgisTrue == false && maxisTrue == false) {
            //iexonlymindrawChart();
            LineData datamin = new LineData(labels, datasetmin);
            iexchart.setData(datamin); // set the data and list of lables
            iexchart.animateY(3000);
        } else if (minisTrue == true && avgisTrue == false && maxisTrue == true) {
            //iexavgdrawChartMixMAx();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            iexchart.setData(data); // set the data and list of lables into
            iexchart.animateY(3000);
        } else if (minisTrue == true && avgisTrue == true && maxisTrue == false) {
            //iexmaxdrawChartMinAvg();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetavg);
            LineData data = new LineData(labels, dataSets);
            iexchart.setData(data); // set the data and list of lables into
            iexchart.animateY(3000);
        } else if (minisTrue == true && avgisTrue == true && maxisTrue == true) {
            //Toast.makeText(MarketPriceActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            tviexmintext.setTextColor(Color.GREEN);
            tviexminline.setBackgroundColor(Color.GREEN);
            tviexavgtext.setTextColor(Color.CYAN);
            tviexavgline.setBackgroundColor(Color.CYAN);
            tviexmaxtext.setTextColor(Color.RED);
            tviexmaxline.setBackgroundColor(Color.RED);
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetavg);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            iexchart.setData(data); // set the data and list of lables into
            iexchart.animateY(3000);
        }
        iexchart.setDescription("Values in Rupees");
        iexchart.setDescriptionColor(Color.WHITE);
        XAxis xl = iexchart.getXAxis();
        xl.setTextColor(Color.WHITE);

        YAxis leftAxis = iexchart.getAxisLeft();
        leftAxis.setTextColor(Color.WHITE);

        YAxis rightAxis = iexchart.getAxisRight();
        rightAxis.setTextColor(Color.WHITE);
        iexchart.setDrawGridBackground(false);
    }

    public void drawChartpxilgraphAll() {
        ArrayList<String> labels = new ArrayList<String>();
        ArrayList<Entry> entriesmin = new ArrayList<>();
        ArrayList<Entry> entriesavg = new ArrayList<>();
        ArrayList<Entry> entriesmax = new ArrayList<>();

        for (int date = 0; date < pxilArr1.size(); date++) {
            labels.add(pxilArr1.get(date));
        }

        for (int min = 0; min < pxilArr2.size(); min++) {
            entriesmin.add(new Entry(Float.parseFloat(pxilArr2.get(min)), min));
        }
        LineDataSet datasetmin = new LineDataSet(entriesmin, "");
        datasetmin.setColor(Color.GREEN);
        datasetmin.setCircleColor(Color.GREEN);
        datasetmin.setValueTextColor(Color.GREEN);
        datasetmin.setDrawCubic(true);

        for (int avg = 0; avg < pxilArr3.size(); avg++) {
            entriesavg.add(new Entry(Float.parseFloat(pxilArr3.get(avg)), avg));
        }
        LineDataSet datasetavg = new LineDataSet(entriesavg, "");
        datasetavg.setColor(Color.CYAN);
        datasetavg.setCircleColor(Color.CYAN);
        datasetavg.setValueTextColor(Color.CYAN);
        datasetavg.setDrawCubic(true);

        for (int max = 0; max < pxilArr4.size(); max++) {
            entriesmax.add(new Entry(Float.parseFloat(pxilArr4.get(max)), max));
        }
        LineDataSet datasetmax = new LineDataSet(entriesmax, "");
        datasetmax.setColor(Color.RED);
        datasetmax.setCircleColor(Color.RED);
        datasetmax.setValueTextColor(Color.RED);

        if (pxilminisTrue == false && pxilavgisTrue == false && pxilmaxisTrue == false) {
            //Toast.makeText(MarketPriceActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            tvpxilmintext.setTextColor(Color.GREEN);
            tvpxilminline.setBackgroundColor(Color.GREEN);
            tvpxilavgtext.setTextColor(Color.CYAN);
            tvpxilavgline.setBackgroundColor(Color.CYAN);
            tvpxilmaxtext.setTextColor(Color.RED);
            tvpxilmaxline.setBackgroundColor(Color.RED);
            //drawChartPXILAll();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetavg);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            pxilchart.setData(data); // set the data and list of lables into
            pxilchart.animateY(3000);
            pxilminisTrue = true;
            pxilavgisTrue = true;
            pxilmaxisTrue = true;
        } else if (pxilminisTrue == false && pxilavgisTrue == false && pxilmaxisTrue == true) {
            //pxilonlymaxdrawChart();
            LineData datamax = new LineData(labels, datasetmax);
            pxilchart.setData(datamax);
            pxilchart.animateY(3000);
        } else if (pxilminisTrue == false && pxilavgisTrue == true && pxilmaxisTrue == false) {
            //pxilonlyavgdrawChart();
            LineData dataavg = new LineData(labels, datasetavg);
            pxilchart.setData(dataavg);
            pxilchart.animateY(3000);
        } else if (pxilminisTrue == false && pxilavgisTrue == true && pxilmaxisTrue == true) {
            //pxildrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetavg);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            pxilchart.setData(data); // set the data and list of lables into
            pxilchart.animateY(3000);
        } else if (pxilminisTrue == true && pxilavgisTrue == false && pxilmaxisTrue == false) {
            LineData datamin = new LineData(labels, datasetmin);
            pxilchart.setData(datamin);
            pxilchart.animateY(3000);
        } else if (pxilminisTrue == true && pxilavgisTrue == false && pxilmaxisTrue == true) {
            //pxildrawChartMixMAx();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            pxilchart.setData(data); // set the data and list of lables into
            pxilchart.animateY(3000);
        } else if (pxilminisTrue == true && pxilavgisTrue == true && pxilmaxisTrue == false) {
            //pxildrawChartMinAvg();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetavg);
            LineData data = new LineData(labels, dataSets);
            pxilchart.setData(data); // set the data and list of lables into
            pxilchart.animateY(3000);
        } else if (pxilminisTrue == true && pxilavgisTrue == true && pxilmaxisTrue == true) {
            //Toast.makeText(MarketPriceActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            tvpxilmintext.setTextColor(Color.GREEN);
            tvpxilminline.setBackgroundColor(Color.GREEN);
            tvpxilavgtext.setTextColor(Color.CYAN);
            tvpxilavgline.setBackgroundColor(Color.CYAN);
            tvpxilmaxtext.setTextColor(Color.RED);
            tvpxilmaxline.setBackgroundColor(Color.RED);
            //drawChartPXILAll();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetavg);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            pxilchart.setData(data); // set the data and list of lables into
            pxilchart.animateY(3000);
        }
        pxilchart.setDescription("Values in Rupees");
        pxilchart.setDescriptionColor(Color.WHITE);
        XAxis xl = pxilchart.getXAxis();
        xl.setTextColor(Color.WHITE);

        YAxis leftAxis = pxilchart.getAxisLeft();
        leftAxis.setTextColor(Color.WHITE);

        YAxis rightAxis = pxilchart.getAxisRight();
        rightAxis.setTextColor(Color.WHITE);

        pxilchart.setDrawGridBackground(false);


    }

    public void showMessage(String title, String message) {
        Builder builder = new Builder(MarketPriceActivity.this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(MarketPriceActivity.this, MainActivityAfterLogin.class);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    public class SelectDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            fromDateEtxt.setText(month + "/" + day + "/" + year);
        }

    }

    public class ToDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            toDateEtxt.setText(month + "/" + day + "/" + year);
        }

    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Login login = new Login();
            login.setAccess_key(dbacess);
            return MarketAPIResponse.POST(urls[0], login);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Marketprice Result:", result);
            prgDialog.hide();
            //Toast.makeText(getActivity(), "MarketPriceResult" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            JSONObject mainObject = null;
            JSONArray arr1 = null, arr2 = null;
            if (firstStringValid == true) {
                try {
                    JSONObject object = new JSONObject(result);
                    arr1 = object.getJSONArray("iex");
                    arr2 = object.getJSONArray("pxil");
                    int len1 = arr1.length();
                    int len2 = arr2.length();
                    for (int index = 0; index < len1; index++) {
                        JSONArray tmpArr = arr1.getJSONArray(index);
                        iexArr1.add(tmpArr.getString(0));
                        iexArr2.add(tmpArr.getString(1));
                        iexArr3.add(tmpArr.getString(2));
                        iexArr4.add(tmpArr.getString(3));
                    }
                    for (int index = 0; index < len2; index++) {
                        JSONArray tmpArr = arr2.getJSONArray(index);
                        pxilArr1.add(tmpArr.getString(0));
                        pxilArr2.add(tmpArr.getString(1));
                        pxilArr3.add(tmpArr.getString(2));
                        pxilArr4.add(tmpArr.getString(3));
                    }
                    MarketPriceCustomListAgain adapter = new MarketPriceCustomListAgain(
                            MarketPriceActivity.this, iexArr1, iexArr2, iexArr3, iexArr4, pxilArr2, pxilArr3, pxilArr4);
                    marketpicelist.setAdapter(adapter);
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject1 = new JSONObject(result);
                        String errormass = mainObject1.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            if (arr1.length() == 0 && arr2.length() == 0) {

            } else {
                drawChartiexgraphAll();
                drawChartpxilgraphAll();
            }

        }

    }
}
