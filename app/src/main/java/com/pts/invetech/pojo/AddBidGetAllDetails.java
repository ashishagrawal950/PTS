package com.pts.invetech.pojo;

public class AddBidGetAllDetails {
	public String k;
	public String id;
	public String date;
	public String price;
	public String bid;
	public String blockfrom;
	public String blockto;
	public String status;

	public AddBidGetAllDetails(String k,String id, String date, String price, 
			 String bid, String blockfrom, String blockto, String status ) {
		// TODO Auto-generated constructor stub
		this.k = k;
		this.id = id;
		this.date = date;
		this.price = price;
		this.bid = bid;
		this.blockfrom = blockfrom;
		this.blockto = blockto;
		this.status = status;
	}

	
	
	

	public String getK() {
		return k;
	}





	public void setK(String k) {
		this.k = k;
	}





	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getDate() {
		return date;
	}



	public void setDate(String date) {
		this.date = date;
	}



	public String getPrice() {
		return price;
	}



	public void setPrice(String price) {
		this.price = price;
	}



	public String getBid() {
		return bid;
	}



	public void setBid(String bid) {
		this.bid = bid;
	}



	public String getBlockfrom() {
		return blockfrom;
	}



	public void setBlockfrom(String blockfrom) {
		this.blockfrom = blockfrom;
	}



	public String getBlockto() {
		return blockto;
	}



	public void setBlockto(String blockto) {
		this.blockto = blockto;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	@Override
	public String toString() {
		return "AddBidGetAllDetails[id=" + id + ", date=" + date
				+ ", price=" + price + ", ]";
	}



}
