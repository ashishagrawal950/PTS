package com.pts.invetech.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Iex {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("blockfrom")
    @Expose
    private String blockfrom;
    @SerializedName("blockto")
    @Expose
    private String blockto;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("bid")
    @Expose
    private String bid;
    @SerializedName("btype")
    @Expose
    private String btype;
    @SerializedName("status")
    @Expose
    private String status;

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The blockfrom
     */
    public String getBlockfrom() {
        return blockfrom;
    }

    /**
     * 
     * @param blockfrom
     *     The blockfrom
     */
    public void setBlockfrom(String blockfrom) {
        this.blockfrom = blockfrom;
    }

    /**
     * 
     * @return
     *     The blockto
     */
    public String getBlockto() {
        return blockto;
    }

    /**
     * 
     * @param blockto
     *     The blockto
     */
    public void setBlockto(String blockto) {
        this.blockto = blockto;
    }

    /**
     * 
     * @return
     *     The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The bid
     */
    public String getBid() {
        return bid;
    }

    /**
     * 
     * @param bid
     *     The bid
     */
    public void setBid(String bid) {
        this.bid = bid;
    }

    /**
     * 
     * @return
     *     The btype
     */
    public String getBtype() {
        return btype;
    }

    /**
     * 
     * @param btype
     *     The btype
     */
    public void setBtype(String btype) {
        this.btype = btype;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

}