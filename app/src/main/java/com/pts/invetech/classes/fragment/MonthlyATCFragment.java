package com.pts.invetech.classes.fragment;


import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.R;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.customlist.MyViewAdapter;
import com.pts.invetech.dashboardupdate.views.DashBoardUpdate;
import com.pts.invetech.pojo.MonthSetter;
import com.pts.invetech.utils.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish on 04-10-2016.
 */

public class MonthlyATCFragment extends Fragment {

    private LinearLayout openLayout;
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private String loginconfig, domain;
    private Spinner spinner;
    private AdapterView.OnItemSelectedListener itemClick = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String dta = spinner.getSelectedItem().toString();
            //dta = dta+"-"+(Integer.parseInt(dta)+1);
            ((NewFeedMainActivity) getActivity()).updateCallback(dta);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //AppLogger.showMsgWithoutTag("On Crete Monthly Atc");
        View rootView = inflater.inflate(R.layout.activity_monthly_atc, container, false);

        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NewFeedMainActivity) getActivity()).open();
            }
        });

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        Intent in = getActivity().getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");

        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        lltoplogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });


        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
//      viewPager.removeAllViews();

        spinner = (Spinner) rootView.findViewById(R.id.spinner);

        List<String> categories = new ArrayList<String>();
        MonthSetter.getFinancialYears(getActivity(), categories);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_monthlyatc, categories);
        dataAdapter.setDropDownViewResource(R.layout.spinner_item_monthlyatcwhite);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(itemClick);
        spinner.setSelection(MonthSetter.getSpinnerSelection());
        setupViewPager(viewPager, in);

        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        return rootView;
    }

    private void setupViewPager(ViewPager viewPager, Intent in) {
        MyViewAdapter adapter = new MyViewAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new InterCountryFragment(), "Inter Country");
        adapter.addFragment(new InterRegionalFragment(), "Inter Regional");
        adapter.addFragment(new IntraRegionalFragment(), "Intra Regional");
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        setCurrentPager(viewPager, in);
    }

    private void setCurrentPager(ViewPager viewPager, Intent in) {
        if (in.hasExtra(Constant.VALUE_DATAPASS)) {
            String str = in.getStringExtra(Constant.VALUE_DATAPASS);
            Log.i("Type:", str);
            if (str.equalsIgnoreCase("inter-country")) {
                viewPager.setCurrentItem(0);
            } else if (str.equalsIgnoreCase("inter-regional")) {
                viewPager.setCurrentItem(1);
            } else if (str.equalsIgnoreCase("intra-regional")) {
                viewPager.setCurrentItem(2);
            }
        } else {
            viewPager.setCurrentItem(0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // AppLogger.showMsgWithoutTag("On Key Clicked");
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashBoardUpdate dashBoardUpdate = new DashBoardUpdate();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.content_frame, dashBoardUpdate)
                                .commit();
                        /*FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, newsFeedFragment);
                        fragmentTransaction.commit();*/
                    }
                    return true;
                }
                return false;
            }
        });
    }
}
