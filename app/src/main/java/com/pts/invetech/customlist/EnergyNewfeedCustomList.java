package com.pts.invetech.customlist;

import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

public class EnergyNewfeedCustomList extends ArrayAdapter<String> {

	private  Activity context;
	private  ArrayList<Integer> elementiconarray_element;
	private  ArrayList<String> title_key_element;
	private  ArrayList<String> pubdate_key_element;
	private  ArrayList<String> time_key_element;
	private  ArrayList<String> source_key_element;
	private static LayoutInflater inflater = null;
	private int lastPosition = -1;
	private ArrayList<String> arraylist;

	public EnergyNewfeedCustomList(Activity context, ArrayList<Integer> elementiconarray_element, ArrayList<String> title_key_element,
			ArrayList<String> pubdate_key_element, ArrayList<String> time_key_element, ArrayList<String> source_key_element) {
		// TODO Auto-generated constructor stub
		super(context, R.layout.energynewsfeed_list, title_key_element);
		this.context = context;
		this.elementiconarray_element = elementiconarray_element;
		this.title_key_element = title_key_element;
		this.pubdate_key_element = pubdate_key_element;
		this.time_key_element = time_key_element;
		this.source_key_element = source_key_element;
		
		this.arraylist = new ArrayList<String>();
	    this.arraylist.addAll(title_key_element);
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.energynewsfeed_list, null, true);
		//TextView aboveline = (TextView) rowView.findViewById(R.id.aboveline);
		TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
		TextView tvdate= (TextView) rowView.findViewById(R.id.tvdate);
		TextView tvtime= (TextView) rowView.findViewById(R.id.tvtime);
		TextView tvsource= (TextView) rowView.findViewById(R.id.tvsource);
		//TextView belowline = (TextView) rowView.findViewById(R.id.belowline);
		if(title_key_element.get(position).isEmpty()){
			//aboveline.setVisibility(view.GONE);
			txt1.setVisibility(view.INVISIBLE);
			tvdate.setVisibility(view.INVISIBLE);
			tvtime.setVisibility(view.INVISIBLE);
			tvsource.setVisibility(view.INVISIBLE);
			//belowline.setVisibility(view.GONE);
		}
		else{
		//aboveline.setVisibility(view.VISIBLE);
		txt1.setVisibility(view.VISIBLE);
		tvdate.setVisibility(view.VISIBLE);
		tvtime.setVisibility(view.VISIBLE);
		txt1.setText(title_key_element.get(position));
		tvdate.setText(pubdate_key_element.get(position));
		tvtime.setText(time_key_element.get(position));
		if(source_key_element.get(position).equalsIgnoreCase("null") || source_key_element.get(position).length()==0){
		tvsource.setVisibility(view.GONE);
		tvsource.setText("");	
		}else{
		tvsource.setText(source_key_element.get(position));
		tvsource.setVisibility(view.VISIBLE);
		}
		//belowline.setVisibility(view.VISIBLE);
		}
		return rowView;
	}

	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		title_key_element.clear();
		if (charText.length() == 0) {
			title_key_element.addAll(arraylist);
		} else {
			for (String wp : arraylist) {
				if (wp.toLowerCase(Locale.getDefault()).contains(charText)) {
					title_key_element.add(wp);
				}
			}
		}
		notifyDataSetChanged();
	}
}