package com.pts.retrofit;

import java.util.HashMap;
import java.util.Map;

public class ApiParam {

    public static Map<String, String> firebaseAndroid(String userId, String refreshedToken) {

        Map<String, String> map = new HashMap<>();
        map.put("deviceid", userId);
        map.put("regId", refreshedToken);

        return map;
    }
}
