package com.pts.invetech.myscheduling;

/**
 * Created by Ashish on 27-03-2018.
 */
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class GetSaveScheduleSetupDatum implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("region")
    @Expose
    private String region;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.code);
        dest.writeString(this.region);
    }

    public GetSaveScheduleSetupDatum() {
    }

    protected GetSaveScheduleSetupDatum(Parcel in) {
        this.id = in.readString();
        this.code = in.readString();
        this.region = in.readString();
    }

    public static final Parcelable.Creator<GetSaveScheduleSetupDatum> CREATOR = new Parcelable.Creator<GetSaveScheduleSetupDatum>() {
        @Override
        public GetSaveScheduleSetupDatum createFromParcel(Parcel source) {
            return new GetSaveScheduleSetupDatum(source);
        }

        @Override
        public GetSaveScheduleSetupDatum[] newArray(int size) {
            return new GetSaveScheduleSetupDatum[size];
        }
    };
}
