package com.pts.handler;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.pts.invetech.R;
import com.pts.invetech.myscheduling.GetSchedulingData;
import com.pts.invetech.utils.AppLogger;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class NetworkHandlerList extends AsyncTask<String, Void, Object> {

    private NetworkCallBack mActivity = null;
    private Dialog prgDialog = null;
    private int currentId = -1;
    private Context mcontext ;
    private TypeToken<List<GetSchedulingData>> token;

    public NetworkHandlerList(Context context, NetworkCallBack splsh,
                              TypeToken<List<GetSchedulingData>> _token, int id) {
        currentId = id;
        mActivity = splsh;
        mcontext = context;
        token=_token;
    }

    @Override
    protected void onPreExecute() {
            if(prgDialog==null){
            prgDialog = new Dialog(mcontext);
            prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            prgDialog.setContentView(R.layout.progessdialog);
            ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
            Animation animation = AnimationUtils.loadAnimation(mcontext, R.anim.zoom);
            image.startAnimation(animation);
            prgDialog.setCancelable(false);
        }
        prgDialog.show();
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(String... params) {
        return networkCall(params[0], params[1]);
    }

    @Override
    protected void onPostExecute(Object res) {
        mActivity.onResultObject(res, currentId);
        if ((prgDialog != null) && prgDialog.isShowing()) {
            prgDialog.dismiss();
        }
        prgDialog=null;
    }

    private Object networkCall(String currentUrl, String jsonParam) {
        URL url = null;
        HttpURLConnection connection = null;
        List contactList  = null;

        try {
            url = new URL(currentUrl);

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);

            DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
            if (jsonParam != null) {
                dStream.writeBytes("data=" + jsonParam);
            }
            dStream.flush();
            dStream.close();

            AppLogger.showMsgWithoutTag(currentUrl + "     " + "data=" + jsonParam);

            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream content = connection.getInputStream();
                Reader reader = new InputStreamReader(content);

                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
//                Type type = new TypeToken<List<T>>(){}.getType();
                contactList = gson.fromJson(reader, token.getType());

                content.close();
            }
        } catch (Exception e) {
            AppLogger.showMsg("Error in Network Handler List", e.getMessage());
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return contactList;
    }
}