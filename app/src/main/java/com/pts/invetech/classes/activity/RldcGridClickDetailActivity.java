package com.pts.invetech.classes.activity;

import android.os.Bundle;
import android.widget.ListView;

import com.pts.invetech.APICalling.AppBaseActivity;
import com.pts.invetech.R;

public class RldcGridClickDetailActivity extends AppBaseActivity {

    private ListView lv_block_grid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rldc_grid_click_detail);

        lv_block_grid= (ListView) findViewById(R.id.lv_block_grid);

    }
}
