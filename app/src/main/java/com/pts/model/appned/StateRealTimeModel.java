package com.pts.model.appned;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by vaibhav on 4/8/17.
 */

public class StateRealTimeModel implements Parcelable {

    private String stateId;
    private String region;
    private String date;

    public StateRealTimeModel(){}

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    protected StateRealTimeModel(Parcel in) {
        stateId = in.readString();
        region = in.readString();
        date = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(stateId);
        dest.writeString(region);
        dest.writeString(date);
    }

    @SuppressWarnings("unused")
    public static final Creator<StateRealTimeModel> CREATOR = new Creator<StateRealTimeModel>() {
        @Override
        public StateRealTimeModel createFromParcel(Parcel in) {
            return new StateRealTimeModel(in);
        }

        @Override
        public StateRealTimeModel[] newArray(int size) {
            return new StateRealTimeModel[size];
        }
    };
}
