package com.pts.invetech.customlist;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.pts.invetech.R;

public class URSCustomList extends ArrayAdapter<String> {

	private  Activity activity;
	private  ArrayList<String>  text1;
	private  ArrayList<String>  text2;
	private  ArrayList<String> text3;
	private  ArrayList<String> text4;

	public URSCustomList(Activity activity,
			ArrayList<String> text1, ArrayList<String> text2,
			ArrayList<String> text3, ArrayList<String> text4) {
		// TODO Auto-generated constructor stub
		super(activity, R.layout.list_item_main, text1);
		this.activity = activity;
		this.text1 = text1;
		this.text2 = text2;
		this.text3 = text3;
		this.text4 = text4;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = activity.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.list_item_main, null, true);
		TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
		TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
		TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
		TextView txt4 = (TextView) rowView.findViewById(R.id.txt4);
		txt1.setText(text1.get(position));
		txt2.setText(text2.get(position));
		txt3.setText(text3.get(position));
		txt4.setText(text4.get(position));
		//Animation animation = AnimationUtils.loadAnimation(activity, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		//rowView.startAnimation(animation);
		//lastPosition = position;
		//txt3.setText("Delete");
		// imageView.setImageResource(imageId[position]);
		return rowView;
	}
}
