package com.pts.invetech.customlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

public class AddBidRECCustomList extends ArrayAdapter<String> {

	private Activity context;
	private String[] serialnumberadapter;
	private String[] blockfromadapter;
	private String[] blocktoadapter;
	private String[] priceadapter;
	private String[] dateadapter;
	private String[] bidadapter;

	public AddBidRECCustomList(Activity context, String[] serialnumberadapter, String[] blockfromadapter,
			String[] blocktoadapter, String[] priceadapter,
			String[] dateadapter, String[] bidadapter) {
		// TODO Auto-generated constructor stub
		super(context, R.layout.list_new_bid, blockfromadapter);
		this.context = context;
		this.serialnumberadapter = serialnumberadapter;
		this.blockfromadapter = blockfromadapter;
		this.blocktoadapter = blocktoadapter;
		this.priceadapter = priceadapter;
		this.dateadapter = dateadapter;
		this.bidadapter = bidadapter;
	}
	
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.list_new_bid, null, true);
		TextView txt5 = (TextView) rowView.findViewById(R.id.txt5);
		TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
		TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
		TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
		txt5.setText(serialnumberadapter[position]);
		txt1.setText(blockfromadapter[position]);
		txt2.setText(priceadapter[position]);
		txt3.setText(bidadapter[position]);
		// imageView.setImageResource(imageId[position]);
		return rowView;
	}
}