package com.pts.invetech.utils;

import android.content.res.Resources;
import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish Karn on 25-07-2017.
 */

public class ColorTemplates {
    public static final int COLOR_NONE = -1;
    public static final int COLOR_SKIP = -2;
    public static final int[] MY_COLORS = new int[]{
            Color.parseColor("#0078ff"),
            Color.parseColor("#d001cf"),
            Color.parseColor("#79e381"),
            Color.parseColor("#ff6633"),
            Color.parseColor("#447700"),
            Color.parseColor("#eeaa00"),
            Color.parseColor("#e12241"),
            Color.parseColor("#41a62a"),
            Color.parseColor("#ffd700"),
            Color.parseColor("#00cbbc"),
            Color.parseColor("#bbcc66"),
            Color.parseColor("#FF6961"),
            Color.parseColor("#CB99C9"),
            Color.parseColor("#D1E231"),
            Color.parseColor("#B57EDC"),
            Color.parseColor("#E30B5D"),
            Color.parseColor("#704214"),
            Color.parseColor("#CE1620"),
            Color.parseColor("#e8bbc0"),
            Color.parseColor("#fff4e1"),};


    public ColorTemplates() {
    }

    public static int getHoloBlue() {
        return Color.rgb(51, 181, 229);
    }

    public static List<Integer> createColors(Resources r, int[] colors) {
        ArrayList result = new ArrayList();
        int[] var6 = colors;
        int var5 = colors.length;

        for(int var4 = 0; var4 < var5; ++var4) {
            int i = var6[var4];
            result.add(Integer.valueOf(r.getColor(i)));
        }

        return result;
    }

    public static List<Integer> createColors(int[] colors) {
        ArrayList result = new ArrayList();
        int[] var5 = colors;
        int var4 = colors.length;

        for(int var3 = 0; var3 < var4; ++var3) {
            int i = var5[var3];
            result.add(Integer.valueOf(i));
        }

        return result;
    }
}