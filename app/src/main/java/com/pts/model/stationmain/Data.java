
package com.pts.model.stationmain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("gas")
    @Expose
    private List<Ga> gas = null;
    @SerializedName("other")
    @Expose
    private List<Other> other = null;
    @SerializedName("coal")
    @Expose
    private List<Coal> coal = null;

    public List<Ga> getGas() {
        return gas;
    }

    public void setGas(List<Ga> gas) {
        this.gas = gas;
    }

    public List<Other> getOther() {
        return other;
    }

    public void setOther(List<Other> other) {
        this.other = other;
    }

    public List<Coal> getCoal() {
        return coal;
    }

    public void setCoal(List<Coal> coal) {
        this.coal = coal;
    }

}
