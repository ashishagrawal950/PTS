package com.pts.invetech.pojo;

/**
 * Created by Ashish on 19-09-2016.
 */
public class Landedcost {
    private String device_id;
    private String client_type;
    private String discom_id;
    private String voltage_level;
    private String quantum;
    private String no_of_hours;
    private String price;
    private String dbacess;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getClient_type() {
        return client_type;
    }

    public void setClient_type(String client_type) {
        this.client_type = client_type;
    }

    public String getDiscom_id() {
        return discom_id;
    }

    public void setDiscom_id(String discom_id) {
        this.discom_id = discom_id;
    }

    public String getVoltage_level() {
        return voltage_level;
    }

    public void setVoltage_level(String voltage_level) {
        this.voltage_level = voltage_level;
    }

    public String getNo_of_hours() {
        return no_of_hours;
    }

    public void setNo_of_hours(String no_of_hours) {
        this.no_of_hours = no_of_hours;
    }

    public String getQuantum() {
        return quantum;
    }

    public void setQuantum(String quantum) {
        this.quantum = quantum;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDbacess() {
        return dbacess;
    }

    public void setDbacess(String dbacess) {
        this.dbacess = dbacess;
    }
}
