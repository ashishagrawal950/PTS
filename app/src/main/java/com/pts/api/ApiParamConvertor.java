package com.pts.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vaibhav on 29/3/18.
 */

public class ApiParamConvertor {

    // Blck Data for 96 block
    public static String getBlockDataClubbedParam(String accessKey,
                                                  String id, String prevId) {
        try {
            JSONObject object = new JSONObject();
            object.put("access_key", accessKey);
            object.put("id", id);
            object.put("previous_id", prevId);
            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Scheduling data
    public static String getSchedulingData(String accessKey, String type,
                                           String region, String terms) {
        try {
            JSONObject object = new JSONObject();
            object.put("access_key", accessKey);
            object.put("type", type);
            object.put("region", region);
            object.put("term", terms);
            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    //  wazmrogqbjqhbkeddvelefdttxpeesabtznkoubqdrxfcbigwnrasvtlsjqxcqyv

    public static String saveCodeApi(String accessKey, String region, String code) {
        try {
            JSONObject object = new JSONObject();
            object.put("access_key", accessKey);
            object.put("detail", getClientJsonArray(region, code));
            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static JSONArray getClientJsonArray(String region, String code) throws JSONException {
        JSONArray array = new JSONArray();

        JSONObject obj = new JSONObject();
        obj.put("region", region);
        obj.put("code", code != null ? code : "");
        array.put(obj);

        return array;
    }

    // Get Save Schedule
    public static String getSaveSchedule(String accessKey) {
        try {
            JSONObject object = new JSONObject();
            object.put("access_key", accessKey);
            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Get Schedule Delete
    public static String getDeleteSchedule(String accessKey,String id,
                                           String region,String code){
        try {
            JSONObject object = new JSONObject();
            object.put("id", id);
            object.put("region", region);
            object.put("access_key", accessKey);
            object.put("code", code);
            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
