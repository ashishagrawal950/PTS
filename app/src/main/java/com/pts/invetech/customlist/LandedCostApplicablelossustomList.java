package com.pts.invetech.customlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

import java.util.ArrayList;

/**
 * Created by Ashish on 19-09-2016.
 */
public class LandedCostApplicablelossustomList extends ArrayAdapter<String> {

    private Activity context;
    private ArrayList<String> applicable_losses_name_array;
    private ArrayList<String> applicable_losses_array;

    public LandedCostApplicablelossustomList(Activity context, ArrayList<String> applicable_losses_name_array, ArrayList<String> applicable_losses_array) {
        // TODO Auto-generated constructor stub
        super(context, R.layout.list_item_landedcost, applicable_losses_name_array);
        this.context = context;
        this.applicable_losses_name_array = applicable_losses_name_array;
        this.applicable_losses_array = applicable_losses_array;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_item_landedcost, null, true);
        TextView txt1 = (TextView) rowView.findViewById(R.id.textView1);
        TextView txt2 = (TextView) rowView.findViewById(R.id.textView2);
        txt1.setText(applicable_losses_name_array.get(position));
        txt2.setText(applicable_losses_array.get(position));
        // imageView.setImageResource(imageId[position]);
        return rowView;
    }
}