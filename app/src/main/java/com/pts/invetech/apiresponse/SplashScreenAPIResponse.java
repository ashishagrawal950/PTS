package com.pts.invetech.apiresponse;

import android.util.Log;

import com.pts.invetech.pojo.DeviceRegister;
import com.pts.invetech.utils.AppLogger;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class SplashScreenAPIResponse {
	
	public static String POST(String url, DeviceRegister deviceregister){
		InputStream inputStream = null;
		String result = "";
		HttpClient httpclient;
		try {

			// 1. create HttpClient
			httpclient = new DefaultHttpClient();
			//httpclient.getConnectionManager().getSchemeRegistry().register(‌​new Scheme("SSLSocketFactory", SSLSocketFactory.getSocketFactory(), 443));

			//httpclient.getConnectionManager().getSchemeRegistry().register(new Scheme("SSLSocketFactory", SSLSocketFactory.getSocketFactory(), 443));
			// 2. make POST request to the given URL
		    HttpPost httpPost = new HttpPost(url);
		    
		    String json = " ";
		    
		    //String platform, String uuid, String version, String width, String height, String colordepth
		  
		    // 3. build jsonObject
		    JSONObject jsonObject = new JSONObject();
		    
		    jsonObject.accumulate("name", deviceregister.getName());
		    jsonObject.accumulate("platform", deviceregister.getPlatform());
		    jsonObject.accumulate("uuid", deviceregister.getUuid());
		    jsonObject.accumulate("version", deviceregister.getVersion());
		    jsonObject.accumulate("width", deviceregister.getWidth());
		    jsonObject.accumulate("height", deviceregister.getHeight());
		    jsonObject.accumulate("colordepth", deviceregister.getColordepth());
		    
		    
		    // 4. convert JSONObject to JSON to String
		    json = jsonObject.toString();

			AppLogger.showError("params",json);
		    
		    
		    // ** Alternative way to convert Person object to JSON string usin Jackson Lib 
		    // ObjectMapper mapper = new ObjectMapper();
		    // json = mapper.writeValueAsString(person);
		    
		    // 5. set json to StringEntity
		    StringEntity se = new StringEntity("data="+json);
		    
		    // 6. set httpPost Entity
		    httpPost.setEntity(se);
		    
		    // 7. Set some headers to inform server about the type of the content   
		    httpPost.setHeader("Accept", "*/*");
		    httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
			//httpPost.setHeader("Accept-Encoding", "gzip, deflate");

			// 8. Execute POST request to the given URL
			HttpResponse httpResponse = httpclient.execute(httpPost);
			
			// 9. receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();
			
		    
			// 10. convert inputstream to string
			if(inputStream != null)
				result = convertInputStreamToString(inputStream);
			else
				result = "Did not work!";
		
		} catch (Exception e) {
			Log.d("InputStream", e.getLocalizedMessage());
		}
		
		// 11. return result
		return result;
	}
	
	private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        
        inputStream.close();
        return result;
        
    }
}
