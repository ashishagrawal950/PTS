package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pts.badge.ShortcutBadger;
import com.pts.invetech.GCMIntentService;
import com.pts.invetech.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NewsFeedActivityWebViewNotification extends Activity {

    private Context context;
    private LinearLayout mopenLayout;
    private String client_id;
    private SQLiteDatabase db;
    private String link_key_element, category, message, link;
    private String loginconfig;
    private String icontypefrom, dbacess, displaydate, displaytime;
    private String[] icontypearray;
    private String[] array;
    private String[] arraydispalydate;
    private String[] arraydispalytime;
    private Integer[] elementiconarray;
    private String lastelementdtate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_newsfeedwebview);


        clearSharedPref(this);
        ShortcutBadger.applyCount(NewsFeedActivityWebViewNotification.this, 0);
        db = openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientid(clientid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationnewsfeed(newMessage TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");

        //String dummyinsert = "http://energy.economictimes.indiatimes.com/news/renewable/clean-energy-tamil-nadu-spinning-mills-turn-to-rooftop-solar-farms/52430891";
        //db.execSQL("INSERT INTO pushnotificationnewsfeed VALUES('" + dummyinsert + "', '" + dummyinsert + "', '" + dummyinsert + "')");

        Intent in = getIntent();
        link_key_element = in.getStringExtra("link");
        //loginconfig = in.getStringExtra("LOGINCONFIG");

        Cursor c = db.rawQuery("SELECT * FROM pushnotificationnewsfeed", null);
        if (c.getCount() == 0) {
            //category =  key_element.get(0);
            category = "";
            link = "http://energy.economictimes.indiatimes.com/news/renewable/clean-energy-tamil-nadu-spinning-mills-turn-to-rooftop-solar-farms/52430891";
            //showMessage("Error", "No Notification found");
            //Toast.makeText(NotificationActivity.this, "No Notification found", Toast.LENGTH_LONG).show();
        } else {
            icontypearray = new String[c.getCount()];
            array = new String[c.getCount()];
            arraydispalydate = new String[c.getCount()];
            arraydispalytime = new String[c.getCount()];
            elementiconarray = new Integer[c.getCount()];
            int i = 0;
            while (c.moveToNext()) {
                String dbacessfrom = c.getString(0);
                String[] parts = dbacessfrom.split("/");
                icontypefrom = parts[0];
                dbacess = parts[1];
                displaydate = c.getString(1);
                displaytime = c.getString(2);
                //goMessage.setText("All Notification:" + dbacess);
                array[i] = dbacess;
                arraydispalydate[i] = displaydate;
                arraydispalytime[i] = displaytime;
                icontypearray[i] = icontypefrom;
                i++;
            }
            List<String> icontypearraylist = Arrays.asList(icontypearray);
            Collections.reverse(icontypearraylist);
            icontypearray = (String[]) icontypearraylist.toArray();
            String lastelement = icontypearray[0];

            List<String> list = Arrays.asList(array);
            Collections.reverse(list);
            array = (String[]) list.toArray();
            String lastelementdetail = array[0];

            List<String> displaydatelist = Arrays.asList(arraydispalydate);
            Collections.reverse(displaydatelist);
            arraydispalydate = (String[]) displaydatelist.toArray();
            lastelementdtate = arraydispalydate[0];
            //String[] split = lastelementdtate.split("/");
            //String splitnotification = split[1];


            List<String> displaytimelist = Arrays.asList(arraydispalytime);
            Collections.reverse(displaytimelist);
            arraydispalytime = (String[]) displaytimelist.toArray();
            // category = key_element.get(0);
            category = "";
            link = "";
            try {
                JSONObject jsonObj = new JSONObject(lastelementdtate);
                if (jsonObj.has("message")) {
                    message = jsonObj.getString("message");
                }
                if (jsonObj.has("category")) {
                    category = jsonObj.getString("category");
                }
                if (jsonObj.has("link")) {
                    link = jsonObj.getString("link");
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }


        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(NewsFeedActivityWebViewNotification.this, NewFeedMainActivity.class);
                in.putExtra("LOGINCONFIG", "LOGIN");
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        WebView myWebView = (WebView) findViewById(R.id.webView);
        myWebView.setClickable(true);
        myWebView.setFocusableInTouchMode(true);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.loadUrl(link_key_element);
        WebClientClass webViewClient = new WebClientClass();
        myWebView.setWebViewClient(webViewClient);
        WebChromeClient webChromeClient = new WebChromeClient();
        myWebView.setWebChromeClient(webChromeClient);

        ImageView ivshare = (ImageView) findViewById(R.id.ivshare);
        ivshare.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Resources resources = getResources();

                Intent emailIntent = new Intent();
                emailIntent.setAction(Intent.ACTION_SEND);
                // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
                emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_native)));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
                emailIntent.setType("message/rfc822");

                PackageManager pm = getPackageManager();
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");

                Intent openInChooser = Intent.createChooser(emailIntent, resources.getString(R.string.share_chooser_text));
                List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
                List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
                for (int i = 0; i < resInfo.size(); i++) {
                    // Extract the label, append it, and repackage it in a LabeledIntent
                    ResolveInfo ri = resInfo.get(i);
                    String packageName = ri.activityInfo.packageName;
                    if (packageName.contains("android.email")) {
                        emailIntent.setPackage(packageName);
                    } else if (packageName.contains("whatsapp") || packageName.contains("gm")) {
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        if (packageName.contains("whatsapp")) {
                            intent.putExtra(Intent.EXTRA_TEXT, link_key_element);
                            //	startActivity(Intent.createChooser(intent, ""));

                        } else if (packageName.contains("android.gm")) {
                            intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_gmail)));
                            intent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
                            intent.setType("message/rfc822");
                            intent.putExtra(Intent.EXTRA_SUBJECT, "News: PTS");
                            intent.putExtra(Intent.EXTRA_TEXT, link_key_element);
                        }
                        intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
                    }
                }

                // convert intentList to array
                LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);

                openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
                startActivity(openInChooser);
            }

        });
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(NewsFeedActivityWebViewNotification.this, NewFeedMainActivity.class);
        in.putExtra("LOGINCONFIG", "LOGIN");
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    private void clearSharedPref(Context context) {
        SharedPreferences settings = context.getSharedPreferences(GCMIntentService.SHAREDPREF_PTS_DATA,
                Context.MODE_PRIVATE);
        settings.edit().clear().commit();
    }

    public class WebClientClass extends WebViewClient {
        ProgressDialog pd = null;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            pd = new ProgressDialog(NewsFeedActivityWebViewNotification.this);
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            pd.dismiss();
        }
    }
}