package com.pts.invetech.customlist;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.model.stationmain.stationwise.blockwise.BlockDatum;

import java.util.List;

public class StationWiseBlockWiseCustomList extends BaseAdapter {

    private Activity context;
    private List<BlockDatum> blockDatumList;


    public StationWiseBlockWiseCustomList(Activity context, List<BlockDatum> _blockDatumList) {
        this.context = context;
        blockDatumList = _blockDatumList;
    }

    @Override
    public int getCount() {
        return blockDatumList.size();
    }

    @Override
    public BlockDatum getItem(int position) {
        return blockDatumList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return blockDatumList.get(position).hashCode();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if(view==null){
            LayoutInflater inflater = context.getLayoutInflater();
            view= inflater.inflate(R.layout.listitem_states_blockwise, null, true);

            holder=new ViewHolder();
            holder.txt1= (TextView) view.findViewById(R.id.txt1);
            holder.txt2= (TextView) view.findViewById(R.id.txt2);
            holder.txt3= (TextView) view.findViewById(R.id.txt3);
            holder.llback = (LinearLayout) view.findViewById(R.id.llback);

            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }

        BlockDatum blockDatum=getItem(position);
        String serial=Integer.toString(position+1);
        holder.txt1.setText(serial);
        holder.txt2.setText(blockDatum.getFromtime() + " - " + blockDatum.getTotime());
        if(blockDatum.getDisable().equalsIgnoreCase("YES")){
            holder.txt3.setText(Double.toString(blockDatum.getAvailable()));
            holder.llback.setBackgroundColor(Color.parseColor("#caebeceb"));
            holder.txt3.setTextColor(Color.RED);
        }
        else{
            holder.txt3.setText(Double.toString(blockDatum.getAvailable()));
            holder.llback.setBackgroundColor(view.getResources().getColor(R.color.white));
            holder.txt3.setTextColor(Color.GREEN);
        }


        return view;
    }

    class ViewHolder{
        LinearLayout llback;
        TextView txt1,txt2,txt3;

    }
}
