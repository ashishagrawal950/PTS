package com.pts.invetech.classes.afterlogin;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseFragment;
import com.pts.invetech.APICalling.NetworkManagerFragment;
import com.pts.invetech.R;
import com.pts.invetech.classes.activity.RealtimeSchedulingDetailActivity;
import com.pts.invetech.pojo.ParserRealTimeData;
import com.pts.invetech.utils.AppDateUtils;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Constant;
import com.pts.invetech.utils.DatePickerFragmentRTS;
import com.pts.model.realtimeschedule.RealTimeData;
import com.pts.model.realtimeschedule.RealTimeInnerData;
import com.pts.model.realtimeschedule.RealTimeIntermed;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class RealtimeSchedulingFragmentAfterLogin extends AppBaseFragment implements View.OnClickListener {

    static final int DATE_DIALOG_ID = 999;
    private final String URL_REALTIME = "https://www.mittalpower.com/mobile/pxs_app/service/scheduling/getqtm_importtypewise.php";
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private View rootView;
    private TextView tvDisplayDate;
    private int year;
    private int month;
    private int day;
    private Handler handler = new Handler();
    private RealTimeData realTimeData;
    private SQLiteDatabase db;
    private String device_id, domain;
    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            int monthone = month + 1;
            if ((monthone <= 9) && (day < 10)) {
                String monthplace = "0" + monthone;
                String dayplace = "0" + day;
                tvDisplayDate.setText(dayplace + "-" + monthplace + "-" + year);
            } else if (monthone <= 9) {
                String monthplace = "0" + monthone;
                tvDisplayDate.setText(day + "-" + monthplace + "-" + year);
            } else if (day < 10) {
                // String dayplace = "0" + day;
                tvDisplayDate.setText(day + "-" + monthone + "-" + year);
            } else {
                tvDisplayDate.setText(day + "-" + monthone + "-" + year);
            }
            callAPIwithdate();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String deviceId = "device_id";
        db = getActivity().openOrCreateDatabase("deviceDBSecond",
                Context.MODE_PRIVATE, null);
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
//        domain = getArguments().getString("domain");
        JSONObject networkObject = new JSONObject();
        try {
            networkObject.put(deviceId, device_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new NetworkManagerFragment(this, networkObject, domain + "/mobile/pxs_app/service/scheduling/getqtm_importtypewise.php").execute();
    }

    private void getData() {
        AppLogger.showMsgWithoutTag(realTimeData.getDate());
        ArrayList<RealTimeIntermed> arr = realTimeData.getInnerData();

        for (int index = 0; index < arr.size(); index++) {
            RealTimeIntermed data = arr.get(index);
            AppLogger.showMsgWithoutTag(data.getName() + "   " + data.getInnerDatas().get(0).getRegionname());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_realtimescheduling, container, false);

        LinearLayout openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivityAfterLogin) getActivity()).open();
            }
        });

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        lltoplogin.setVisibility(View.GONE);
        lltoplogout.setVisibility(View.VISIBLE);
        tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
        tvlogouttext.setText("Home");

        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(in);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);

            }
        });

        setAnimation();
        tvDisplayDate = (TextView) rootView.findViewById(R.id.tv_date);
        tvDisplayDate.setOnClickListener(this);

        return rootView;
    }

    private void setAnimation() {
        LinearLayout layout_firstpage = (LinearLayout) rootView.findViewById(R.id.layout_firstpage);

        Animation animation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.realtime_scheduling_slide_left_to_right);
        layout_firstpage.startAnimation(animation);
    }

    private void setGraphData(LayoutInflater inflater, RealTimeData data) {
        if (data == null) {
            return;
        }

        ArrayList<RealTimeIntermed> innerData = data.getInnerData();

        for (int index = 0; index < innerData.size(); index++) {
            setPietChart(inflater, innerData.get(index));
        }
    }

    public void setPietChart(LayoutInflater inflater, RealTimeIntermed interData) {

        ArrayList<RealTimeInnerData> realTimeInnerData = interData.getInnerDatas();

        LinearLayout holderVu = (LinearLayout) rootView.findViewById(R.id.mapholder);
        LinearLayout itemView = (LinearLayout) inflater.inflate(R.layout.item_realtime, null);

        PieChart pieChart = (PieChart) itemView.findViewById(R.id.chartitem);

        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<String>();

        for (int index = 0; index < realTimeInnerData.size(); index++) {
            RealTimeInnerData temp = realTimeInnerData.get(index);

            entries.add(new Entry(Float.parseFloat(temp.getPercentage()), index, temp));
            labels.add(temp.getRegionname());
        }

        PieDataSet dataset = new PieDataSet(entries, "");

        PieData data = new PieData(labels, dataset);
        data.setValueFormatter(new PercentFormatter());

        pieChart.getLegend().setEnabled(false);
        dataset.setColors(ColorTemplate.COLORFUL_COLORS);

        pieChart.setData(data);
        pieChart.setDescription("");
        pieChart.animateY(3000);

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

                RelativeLayout lay = (RelativeLayout) rootView.findViewById(R.id.shadowlayout);
                if (lay.getVisibility() == View.VISIBLE) {
                    return;
                }

                if (e == null)
                    return;

                RealTimeInnerData temp = (RealTimeInnerData) e.getData();

                Intent intent = new Intent(getActivity(), RealtimeSchedulingDetailActivity.class);

                intent.putExtra(Constant.VALUE_DATE, realTimeData.getDate());
                intent.putExtra(Constant.VALUE_REGION, temp.getRegionname());
                intent.putExtra(Constant.VALUE_MAXREV, temp.getMaxrevision());
                intent.putExtra(Constant.VALUE_IMPORTTYPE, temp.getHeaderString());
                intent.putStringArrayListExtra(Constant.VALUE_REVARRAY, temp.getRevisionList());

                startActivity(intent);
            }

            @Override
            public void onNothingSelected() {
            }

        });

        postAtTime();
        setHelperData(inflater, itemView, interData);
        holderVu.addView(itemView);
    }

    private void setHelperData(LayoutInflater inflater, LinearLayout itemView, RealTimeIntermed interData) {
        ((TextView) itemView.findViewById(R.id.mapbelowtext)).setText(interData.getName());


        ArrayList<RealTimeInnerData> realTimeInnerData = interData.getInnerDatas();
        int[] colorArr = ColorTemplate.COLORFUL_COLORS;
        LinearLayout holder = (LinearLayout) itemView.findViewById(R.id.charthelperparent);

        int colorId = 0;
        for (int index = 0; index < realTimeInnerData.size(); index++) {

            LinearLayout charthelperitem = (LinearLayout) inflater.inflate(R.layout.realtime_item_chart_helper, null);
            colorId = index % colorArr.length;

            ((TextView) charthelperitem.findViewById(R.id.helpercolor)).setBackgroundColor(colorArr[colorId]);
            ((TextView) charthelperitem.findViewById(R.id.helpertext)).setText(realTimeInnerData.get(index).getTotalqtm() + " MW/h");

            holder.addView(charthelperitem);
        }
    }

    @Override
    public void updateStringResult(String string) {
        if (string == null) {
            AppLogger.showMsgWithoutTag("No Data Found or Some error occurred");
            return;
        }

        try {
            AppLogger.showMsgWithoutTag(string);
            realTimeData = new RealTimeData();
            new ParserRealTimeData(realTimeData, string);

            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            setGraphData(inflater, realTimeData);

            tvDisplayDate.setText(realTimeData.getDate());

        } catch (JSONException e) {
            e.printStackTrace();
            AppLogger.showMsgWithoutTag("Some Error Occurred.");
        }
    }

    private void postAtTime() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isVisible()) {
                    showFirstTimeHelper();
                }
            }
        }, 2500);
    }

    private void showFirstTimeHelper() {

        if (SharedPrefHandler.checkFirstTimeMap(this.getActivity())) {

            RelativeLayout lay = (RelativeLayout) rootView.findViewById(R.id.shadowlayout);
            lay.setVisibility(View.VISIBLE);

            ((TextView) rootView.findViewById(R.id.shdclick)).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    SharedPrefHandler.setFirstTimeMap(RealtimeSchedulingFragmentAfterLogin.this.getActivity());

                    RelativeLayout lay = (RelativeLayout) rootView.findViewById(R.id.shadowlayout);
                    lay.setVisibility(View.GONE);
                }
            });

        }
    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP
                        && keyCode == KeyEvent.KEYCODE_BACK) {
                    Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                    return true;

                }

                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == tvDisplayDate) {
            showDatePicker();
        }
    }

    private void showDatePicker() {
        DatePickerFragmentRTS rtsdate = new DatePickerFragmentRTS();
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        rtsdate.setArguments(args);
        rtsdate.setCallBack(ondate);
        rtsdate.show(getFragmentManager(), "Date Picker");

    }

    private void callAPIwithdate() {
        JSONObject networkObject = new JSONObject();
        try {
            networkObject.put("date", AppDateUtils.getReverseDate(tvDisplayDate.getText().toString()));
            networkObject.put("device_id", device_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AppLogger.showMsgWithoutTag(networkObject.toString());
        new NetworkManagerFragment(this, networkObject, domain + "/mobile/pxs_app/service/scheduling/getqtm_importtypewise.php").execute();
    }
}