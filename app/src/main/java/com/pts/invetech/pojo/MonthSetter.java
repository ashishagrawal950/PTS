package com.pts.invetech.pojo;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

import java.util.Calendar;
import java.util.List;

/**
 * Created by vaibhav on 28/11/16.
 */

public class MonthSetter {

    public static void getFinancialYears(Context context,List<String> finYrs){
        finYrs.clear();

        getYr(finYrs);
    }

    public static int getSpinnerSelection(){
        Calendar calender=Calendar.getInstance();
        int monthsss=calender.get(Calendar.MONTH);
        if(monthsss==Calendar.JANUARY
                || monthsss==Calendar.FEBRUARY || monthsss==Calendar.MARCH){

            return 1;
        }

        return 0;
    }

    private static void getYr(List<String> finYrs){
        int baseYr=2016;

        Calendar calender=Calendar.getInstance();
        int yr=calender.get(Calendar.YEAR);

        if(yr<=baseYr){
            finYrs.add("2016-2017");
            return;
        }



        while (yr>=baseYr){
            finYrs.add(yr+"-"+(yr+1));
            yr=yr-1;
        }
    }

}
