
package com.pts.model.stationmain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WRLDC {

    @SerializedName("available")
    @Expose
    private String available;
    @SerializedName("select")
    @Expose
    private String select;
    @SerializedName("revision")
    @Expose
    private String revision;

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

}
