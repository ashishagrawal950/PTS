package com.pts.invetech.module.rtclubbed_detail.model;

/**
 * Created by vaibhav on 29/3/18.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClubDetailData {

    @SerializedName("currentRev")
    @Expose
    private String currentRev;
    @SerializedName("prevRev")
    @Expose
    private String prevRev;
    @SerializedName("app_no")
    @Expose
    private Object appNo;
    @SerializedName("blockdata")
    @Expose
    private Blockdata blockdata;

    public String getCurrentRev() {
        return currentRev;
    }

    public void setCurrentRev(String currentRev) {
        this.currentRev = currentRev;
    }

    public String getPrevRev() {
        return prevRev;
    }

    public void setPrevRev(String prevRev) {
        this.prevRev = prevRev;
    }

    public Object getAppNo() {
        return appNo;
    }

    public void setAppNo(Object appNo) {
        this.appNo = appNo;
    }

    public Blockdata getBlockdata() {
        return blockdata;
    }

    public void setBlockdata(Blockdata blockdata) {
        this.blockdata = blockdata;
    }

}