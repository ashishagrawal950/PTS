
package com.pts.model.ursdashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class URSDashboard {

    @SerializedName("NRLDC")
    @Expose
    private NRLDC nRLDC;
    @SerializedName("WRLDC")
    @Expose
    private WRLDC wRLDC;
    @SerializedName("ERLDC")
    @Expose
    private ERLDC eRLDC;
    @SerializedName("SRLDC")
    @Expose
    private SRLDC sRLDC;
    @SerializedName("NERLDC")
    @Expose
    private NERLDC nERLDC;

    public NRLDC getNRLDC() {
        return nRLDC;
    }

    public void setNRLDC(NRLDC nRLDC) {
        this.nRLDC = nRLDC;
    }

    public WRLDC getWRLDC() {
        return wRLDC;
    }

    public void setWRLDC(WRLDC wRLDC) {
        this.wRLDC = wRLDC;
    }

    public ERLDC getERLDC() {
        return eRLDC;
    }

    public void setERLDC(ERLDC eRLDC) {
        this.eRLDC = eRLDC;
    }

    public SRLDC getSRLDC() {
        return sRLDC;
    }

    public void setSRLDC(SRLDC sRLDC) {
        this.sRLDC = sRLDC;
    }

    public NERLDC getNERLDC() {
        return nERLDC;
    }

    public void setNERLDC(NERLDC nERLDC) {
        this.nERLDC = nERLDC;
    }

}
