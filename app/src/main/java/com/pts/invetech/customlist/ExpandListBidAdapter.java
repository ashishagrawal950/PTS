package com.pts.invetech.customlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.customscrollview.widget.AnimatedExpandableListView;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.pojo.BidSummery;
import com.pts.invetech.pojo.InnerData;

import java.util.List;

public class ExpandListBidAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {

    private LayoutInflater inflater;
    private List<BidSummery> items;
    private Context mContext;
    private View.OnClickListener onClick;

    public ExpandListBidAdapter(Context context, List<BidSummery> data, View.OnClickListener click) {
        inflater = LayoutInflater.from(context);
        items = data;
        onClick = click;

        mContext = context;
    }

    public void setData(List<BidSummery> items) {
        this.items = items;
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getRealChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

        ChildHolder holder;
        BidSummery item = getGroup(groupPosition);

        if (convertView == null) {
            holder = new ChildHolder();
            convertView = inflater.inflate(R.layout.list_item_previous_bid, parent, false);
            holder.listView = (NonScrollListView) convertView.findViewById(R.id.childlist);
            holder.txtHeader = (TextView) convertView.findViewById(R.id.headtxt);
            holder.layoutCopy = (LinearLayout) convertView.findViewById(R.id.ic_copy);
            holder.layoutCopy.setOnClickListener(onClick);
            holder.bidEdit = (TextView) convertView.findViewById(R.id.bidedit);
            holder.bidEdit.setOnClickListener(onClick);

            convertView.setTag(holder);
        } else {
            holder = (ChildHolder) convertView.getTag();
        }

        if(childPosition>0){
            holder.layoutCopy.setVisibility(View.GONE);
        }else{
            holder.layoutCopy.setVisibility(View.VISIBLE);
        }

        setChildDataInList(item, childPosition, convertView, holder);
        holder.txtHeader.setText(item.getHeaderName(childPosition));

        holder.layoutCopy.setTag(groupPosition + ":" + childPosition);
        holder.bidEdit.setTag(groupPosition + ":" + childPosition);

        return convertView;
    }

    private void setChildDataInList(BidSummery item, int childPosition, View convertView, ChildHolder holder) {
        List<InnerData> datumList = item.getRefrence(childPosition);
        ExpandChildAdapter adapter = new ExpandChildAdapter(mContext, datumList, inflater);
        holder.listView.setAdapter(adapter);
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        BidSummery sumr = items.get(groupPosition);
        return sumr.incrementChildCount();
    }


    @Override
    public BidSummery getGroup(int groupPosition) {
        return items.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public int getGroupCount() {
        return items.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupHolder holder;

        if (convertView == null) {
            holder = new GroupHolder();
            convertView = inflater.inflate(R.layout.list_group_previous_bid, parent, false);

            holder.title = (TextView) convertView.findViewById(R.id.listtitle);
            holder.submitbidbtn = (TextView) convertView.findViewById(R.id.submitbidbtn);

            convertView.setTag(holder);
        } else {
            holder = (GroupHolder) convertView.getTag();
        }
        BidSummery item = getGroup(groupPosition);

        holder.title.setText((groupPosition + 1) + ". Trading Date- " + item.getDeliveryDate());

        if (item.getStatus().equalsIgnoreCase("TRUE")) {
            holder.submitbidbtn.setVisibility(View.INVISIBLE);
        } else {
            holder.submitbidbtn.setVisibility(View.VISIBLE);
            holder.submitbidbtn.setTag(groupPosition);
            holder.submitbidbtn.setOnClickListener(onClick);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        return true;
    }

    public class ChildHolder {
        public NonScrollListView listView;
        TextView txtHeader;
        LinearLayout layoutCopy;
        TextView bidEdit;
    }

    public class GroupHolder {
        TextView title;
        TextView submitbidbtn;
    }
}