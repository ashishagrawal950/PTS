package com.pts.invetech.classes.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerString;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.classes.activity.MonthlyATCPDFshowActivity;
import com.pts.invetech.R;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.customlist.ExpendableListAdapter;
import com.pts.invetech.pojo.MonthlyAtcPojo;
import com.pts.invetech.utils.AppDeviceUtils;
import com.pts.invetech.utils.AppLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class InterRegionalFragment extends Fragment implements NewFeedMainActivity.MyDataPasser {

    private ExpendableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    private List<MonthlyAtcPojo> monthlyAtcList;
    private String dateQuater = "2016-2017";
    private String partsix, domain;
    private String extation;
    private NetworkCallBack objectCallBack = new NetworkCallBack() {
        @Override
        public void onResultString(String data, int id) {
            if (data == null || data.isEmpty()) {
                AppLogger.showToastShort(getContext(), "No Data Found");
                clearUI();
                return;
            }

            try {
                monthlyAtcList = new ArrayList<>();
                JSONObject issueObj = new JSONObject(data);
                Iterator iterator = issueObj.keys();


                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    JSONArray arr = issueObj.getJSONArray(key);

                    MonthlyAtcPojo pojo = new MonthlyAtcPojo();
                    pojo.setMonth(key);
                    pojo.addMonthJsonArray(arr);

                    monthlyAtcList.add(pojo);
                }

                updateUifterData();
            } catch (Exception ex) {
                ex.printStackTrace();
                AppLogger.showToastShort(getContext(), "No Data Found.");
                clearUI();
            }
        }

    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateQuater = ((NewFeedMainActivity) getActivity()).addPasserCallback(this);
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((NewFeedMainActivity) getActivity()).removePasserCallback(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_one, container, false);

        expListView = (ExpandableListView) rootView.findViewById(R.id.lvExp);

        try {
            networkData(dateQuater);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rootView;
    }

    private void clearUI() {
        if (listAdapter != null) {
            monthlyAtcList.clear();
            listAdapter.updateList(monthlyAtcList);
        }
        showEmptyData();
    }

    private void updateUifterData() {
        listAdapter = new ExpendableListAdapter(getActivity(), monthlyAtcList);

        expListView.setAdapter(listAdapter);

        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            expListView.expandGroup(i);
        }
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent,
                                        View v, int groupPosition, int childPosition, long id) {
                //AppLogger.showToastShort(getActivity(),monthlyAtcList.get(groupPosition).getMonthData().get(childPosition).getFileAddress()+"");
                new DownloadFileAsync().execute(monthlyAtcList.get(groupPosition).getMonthData().get(childPosition).getFileAddress());
                partsix = monthlyAtcList.get(groupPosition).getMonthData().get(childPosition).getFileAddress();
                Intent in = new Intent(getActivity(), MonthlyATCPDFshowActivity.class);
                in.putExtra("url", monthlyAtcList.get(groupPosition).getMonthData().get(childPosition).getFileAddress());

                startActivity(in);
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                return false;
            }
        });
        showListData();
    }

    private void showEmptyData() {
        TextView tVu = (TextView) getView().findViewById(R.id.nodatatxt);
        tVu.setVisibility(View.VISIBLE);

        if (expListView != null) {
            expListView.setVisibility(View.GONE);
        }
    }

    private void showListData() {
        TextView tVu = (TextView) getView().findViewById(R.id.nodatatxt);
        tVu.setVisibility(View.GONE);

        if (expListView != null) {
            expListView.setVisibility(View.VISIBLE);
        }
    }

    private void networkData(String time) throws JSONException {

        String[] data = new String[2];
        JSONObject object = new JSONObject();
        object.put("device_id", AppDeviceUtils.getTelephonyId(getActivity()));
        object.put("fy", time);
        object.put("type", "InterRegional");


        data[0] = "https://www.mittalpower.com/mobile/pxs_app/service/market/monthly_atc.php";
        data[1] = object.toString();
        new NetworkHandlerString(getActivity(), objectCallBack, 1).execute(data);
    }


    @Override
    public void updateDateString(String date) {
        dateQuater = date;

        if (isVisible()) {
            try {
                networkData(dateQuater);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileAsync extends AsyncTask<String, String, String> {
        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            String myUrl;
            final int BUFFER_SIZE = 4096;
            try {
                myUrl = f_url[0];
                String to = myUrl.replaceAll("(?<!http:|https:)//", "/");
                String finalstring = to.replaceAll(" ", "%20");
                URL url = new URL(finalstring);
                URLConnection conection = url.openConnection();
                conection.connect();
                extation = "";
                String disposition = conection.getHeaderField("Content-Disposition");
                String contentType = conection.getContentType();
                int contentLength = conection.getContentLength();
                if (disposition != null) {
                    // extracts file name from header field
                    int index = disposition.indexOf("filename=");
                    if (index > 0) {
                        extation = disposition.substring(index + 10,
                                disposition.length() - 1);
                    }
                } else {
                    // extracts file name from URL
                    extation = myUrl.substring(myUrl.lastIndexOf("/") + 1
                    );
                }
                System.out.println("Content-Type = " + contentType);
                System.out.println("Content-Disposition = " + disposition);
                System.out.println("Content-Length = " + contentLength);
                System.out.println("fileName = " + extation);
                InputStream inputStream = conection.getInputStream();
                String saveFilePath = Environment
                        .getExternalStorageDirectory().toString() + "/" + extation;
                // opens an output stream to save into file
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);
                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
                outputStream.close();
                inputStream.close();
                System.out.println("File downloaded.");
            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            //dismissDialog(progress_bar_type);

        }
    }
}