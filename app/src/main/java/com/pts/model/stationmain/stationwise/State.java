
package com.pts.model.stationmain.stationwise;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class State {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("qtmvalue")
    @Expose
    private Integer qtmvalue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQtmvalue() {
        return qtmvalue;
    }

    public void setQtmvalue(Integer qtmvalue) {
        this.qtmvalue = qtmvalue;
    }

}
