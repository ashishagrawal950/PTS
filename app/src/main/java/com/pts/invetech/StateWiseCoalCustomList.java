package com.pts.invetech;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.pts.model.statewiseurs.Coal;

import java.util.List;

public class StateWiseCoalCustomList extends BaseAdapter {

    private Activity context;
    private List<Coal> coals;



    public StateWiseCoalCustomList(Activity context, List<Coal> coalList) {
        this.context = context;
        this.coals = coalList;
    }

    @Override
    public int getCount() {
        if(coals!=null){
            return coals.size();
        }
        return 0;
    }

    @Override
    public Coal getItem(int position) {
        return coals.get(position);
    }

    @Override
    public long getItemId(int position) {
        return coals.get(position).hashCode();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if(view==null){
            LayoutInflater inflater = context.getLayoutInflater();
            view= inflater.inflate(R.layout.list_item_states, null, true);

            holder=new ViewHolder();
            holder.txt2= (TextView) view.findViewById(R.id.txt2);
            holder.txt3= (TextView) view.findViewById(R.id.txt3);

            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }

        Coal coal =getItem(position);

        holder.txt2.setText(coal.getStation());
        holder.txt3.setText(Double.toString(coal.getQtmvalue()));



        return view;
    }

    class ViewHolder{
        TextView txt2,txt3;

    }
}

