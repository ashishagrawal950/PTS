package com.pts.invetech.utils;

/**
 * Created by Ashish on 05-09-2017.
 */
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

import java.util.Date;

public class DatePickerFragmentRTS extends DialogFragment {
    OnDateSetListener ondateSet;

    public DatePickerFragmentRTS() {
    }

    public void setCallBack(OnDateSetListener ondate) {
        ondateSet = ondate;
    }

    private int year, month, day;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        year = args.getInt("year");
        month = args.getInt("month");
        day = args.getInt("day");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), ondateSet, year, month+1, day);
        dialog.getDatePicker().setMaxDate(new Date().getTime());
        return dialog;
        //return new DatePickerDialog(getActivity(), ondateSet, year, month+1, day);

    }
}