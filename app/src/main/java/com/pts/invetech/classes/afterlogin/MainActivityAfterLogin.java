package com.pts.invetech.classes.afterlogin;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;

import com.pts.invetech.R;
import com.pts.invetech.adapters.AdapterMyScheduling;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.fragment.MainActivityFragment;
import com.pts.invetech.classes.fragment.MySchedulingFragment;
import com.pts.invetech.classes.fragment.NewsFeedFragment;
import com.pts.invetech.customlist.MenuCustomAdapter;
import com.pts.invetech.customlist.ReltimeSchedulelistCustomAdapter;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.utils.Constant;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;


public class MainActivityAfterLogin extends AppCompatActivity {

    private Dialog prgDialog;
    private DrawerLayout mDrawerLayout;
    private RelativeLayout
            llNewsFeed,
            layout_Transmission_Corridor,
            layout_Landed_Cost_Calculator,
            layout_My_Schedule,
            layout_Power_Demand_Comparision,
            layout_Monthly_ACT,
            layout_MCP_Comparision,
            layout_stateload,
            layout_Notification_Setting,
            layout_Contact_us, layout_Disclaimer,
            layout_Feebback;

    private LinearLayout
            linearLayout,
            layout_myScheduleList;
    @SuppressWarnings("deprecation")
    private ActionBarDrawerToggle mDrawerToggle;
    // nav drawer title
    private CharSequence mDrawerTitle;
    // used to store app title
    private CharSequence mTitle;
    // slide menu items
    private NewsFeedFragment newsFeedFragment = null;
    private AllNewsFeedFragmentafterlogin allNewsFeedFragmentafterlogin = null;
    private CurrentAvaiableURSFragmentAfterLogin currentAvaiableURSFragmentAfterLogin = null;
    private StationURSFragmentAfterLogin stationURSFragmentAfterLogin = null;
    private StateURSFragmentAfterLogin stateURSFragmentAfterLogin = null;
    private NewsFeedSearchbyCategoryFragmentafterlogin newsFeedSearchbyCategoryFragmentafterlogin = null;
    private TransmissionCorridorFragmentAfterLogin transmissionCorridorFragmentAfterLogin = null;
    private RealtimeSchedulingFragmentAfterLogin realtimeSchedulingFragmentAfterLogin = null;
    private MySchedulingFragment mySchedulingFragment = null;
    private LandedCostCalculatorFragmentAfterLogin landedCostCalculatorFragment = null;
    private MCP_ComparisionFragmentAfterLogin mCP_ComparisionFragmentAfterLogin = null;
    private StateLoadFragmentAfterLogin stateLoadFragmentAfterLogin = null;
    private MySchedulingFragmentAfterLogin mySchedulingFragmentAfterLogin = null;
    private StatewiseRealtimeSchedulingFragmentAfterlogin statewiseRealtimeSchedulingFragmentAfterlogin = null;
    private PowerDemandComparisionFragmentAfterlogin powerDemandComparisionFragmentAfterlogin = null;
    private MonthlyATCFragmentAfterLogin monthlyATCFragmentAfterLogin = null;
    private NotificationSettingFragmentafterLogin notificationSettingFragment = null;
    private ContactusFragmentAfterLogin contactusFragment = null;
    private DisclaimerFragmentAfterLogin disclaimerFragment = null;
    private FeedBackFragmentAfterLogin feedBackFragment = null;

    private MyScheduleSetUpFragmentAfterLogin myScheduleSetUpFragmentAfterLogin = null;
    private MyScheduleClubviewFragmentAfterLogin myScheduleClubviewFragmentAfterLogin = null;
    private MyScheduleListviewFragmentAfterLogin myScheduleListviewFragmentAfterLogin = null;

    private SQLiteDatabase db;
    private String saveResponse = "";
    private String companyName, domain,newsletterBaseUrl;
    private ArrayList<String> key_element = new ArrayList<String>();
    private ArrayList<String> realtime_element = new ArrayList<String>();
    private ArrayList<String> myScheduleElement = new ArrayList<String>();
    private boolean isvisible = true;
    private boolean isrealtimevisible = true;
    private NonScrollListView searchlistView, realtimelistView, lv_mySchedule;
    private ArrayList<MainActivityAfterLogin.MyDataPasser> passerArrayList = new ArrayList<>();
    private String lastValue = "2016-2017";

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newfeed_activity_main);


//		Toast.makeText(MainActivityAfterLogin.this,newsletterBaseUrl,Toast.LENGTH_LONG).show();


//		Toast.makeText(MainActivityAfterLogin.this,domain,Toast.LENGTH_LONG).show();
//		getActionBar().hide();
        /*prgDialog = new ProgressDialog(MainActivityAfterLogin.this);
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);

        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animationprogess = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animationprogess);
        //prgDialog.show();
        prgDialog.setCancelable(false);
        db = openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientid(clientid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationnewsfeed(newMessage TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS gpsloaction(latitude VARCHAR, longitude VARCHAR, state_id VARCHAR, state_serial VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS availableurs(region VARCHAR, available VARCHAR, select_yes VARCHAR, revision VARCHAR, region_id VARCHAR);");

        Cursor companynamec = db.rawQuery("SELECT * FROM companyname", null);
        if (companynamec.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        StringBuffer companynamecbuffer = new StringBuffer();
        while (companynamec.moveToNext()) {
            companynamecbuffer.append("companynamecbuffer: "
                    + companynamec.getString(0) + "\n");
            companyName = companynamec.getString(0);
            // Toast.makeText(getApplicationContext(), "companyName:" +
            // companyName, Toast.LENGTH_LONG).show();
        }
        newsletterBaseUrl = SharedPrefHandler.getDeviceString(getApplicationContext(), "newsletterBaseUrl");
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");

        linearLayout = (LinearLayout) findViewById(R.id.menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final Animation animation = AnimationUtils.loadAnimation(this, R.anim.menu_slide_down);

        llNewsFeed = (RelativeLayout) findViewById(R.id.llNewsFeed);
        final ImageView imageView3 = (ImageView) findViewById(R.id.imageView3);
        final ImageView imgMySchedule = (ImageView) findViewById(R.id.imgMySchedule);
        final RelativeLayout layout_current_urs = (RelativeLayout) findViewById(R.id.layout_current_urs);
        final ImageView imageView34 = (ImageView) findViewById(R.id.imageView34);

        final LinearLayout layout_station_urs = (LinearLayout) findViewById(R.id.layout_station_urs);
        final LinearLayout layout_state_urs = (LinearLayout) findViewById(R.id.layout_state_urs);


        final RelativeLayout layout_news2 = (RelativeLayout) findViewById(R.id.layout_news2);
        final LinearLayout layout_listview = (LinearLayout) findViewById(R.id.layout_listview);
        layout_listview.setLayoutAnimation(new LayoutAnimationController(animation));

        final LinearLayout layout_myScheduleList = (LinearLayout) findViewById(R.id.layout_myScheduleList);
        layout_myScheduleList.setLayoutAnimation(new LayoutAnimationController(animation));


        layout_Transmission_Corridor = (RelativeLayout) findViewById(R.id.layout_Transmission_Corridor);

        final RelativeLayout layout_rl = (RelativeLayout) findViewById(R.id.layout_rl);
        final RelativeLayout layout_MySchedule = (RelativeLayout) findViewById(R.id.layout_MySchedule);
        final LinearLayout layout_realtime = (LinearLayout) findViewById(R.id.layout_realtime);
        layout_realtime.setLayoutAnimation(new LayoutAnimationController(animation));
        final ImageView imageView4 = (ImageView) findViewById(R.id.imageView4);

        layout_Landed_Cost_Calculator = (RelativeLayout) findViewById(R.id.layout_Landed_Cost_Calculator);
        layout_Monthly_ACT = (RelativeLayout) findViewById(R.id.layout_Monthly_ACT);
        //layout_Realtime_Scheduling = (RelativeLayout) findViewById(R.id.layout_Realtime_Scheduling);
        //layout_Statewise_Realtime_Scheduling = (RelativeLayout) findViewById(R.id.layout_Statewise_Realtime_Scheduling);
        layout_Power_Demand_Comparision = (RelativeLayout) findViewById(R.id.layout_Power_Demand_Comparision);
        layout_MCP_Comparision = (RelativeLayout) findViewById(R.id.layout_MCP_Comparision);
        layout_stateload = (RelativeLayout) findViewById(R.id.layout_stateload);
        layout_stateload = (RelativeLayout) findViewById(R.id.layout_stateload);
        layout_My_Schedule = (RelativeLayout) findViewById(R.id.layout_My_Schedule);
        layout_Notification_Setting = (RelativeLayout) findViewById(R.id.layout_Notification_Setting);
        layout_Contact_us = (RelativeLayout) findViewById(R.id.layout_Contact_us);
        layout_Disclaimer = (RelativeLayout) findViewById(R.id.layout_Disclaimer);
        layout_Feebback = (RelativeLayout) findViewById(R.id.layout_Feebback);
        mTitle = mDrawerTitle = getTitle();


        llNewsFeed.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //View();
                //layout_news2.setBackgroundColor(Color.parseColor("#00e9e9"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                ViewClick();
            }
        });

        layout_current_urs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExpandableLayout exp_manageLead = (ExpandableLayout) findViewById(R.id.exp_manageLead);
                if (exp_manageLead.isExpanded()) {
                    exp_manageLead.collapse();
                    layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                    imageView34.setImageResource((R.drawable.menu_dwn_arrow));
                } else {
                    exp_manageLead.expand();
                    layout_current_urs.setBackgroundColor(Color.parseColor("#00e9e9"));
                    imageView34.setImageResource((R.drawable.menu_up_arow));

                }
//				llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
//				layout_current_urs.setBackgroundColor(Color.parseColor("#00e9e9"));
//				layout_news2.setBackgroundColor(Color.parseColor("#009999"));
//				layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
//				layout_rl.setBackgroundColor(Color.parseColor("#009999"));
//				layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
//				layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
//				layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
//				layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
//				layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
//				layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
//				layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
//				layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
//				layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
//				currentAvaiableURSFragmentAfterLogin = new CurrentAvaiableURSFragmentAfterLogin();
//				if (currentAvaiableURSFragmentAfterLogin != null) {
//					android.app.FragmentManager fragmentManager = getFragmentManager();
//					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//					fragmentTransaction.replace(R.id.content_frame, currentAvaiableURSFragmentAfterLogin);
//					fragmentTransaction.commit();
//					mDrawerLayout.closeDrawer(linearLayout);
//				} else {
//					Log.e("MainActivity", "Error in creating fragment");
//				}
            }
        });

        layout_station_urs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_station_urs.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                stationURSFragmentAfterLogin = new StationURSFragmentAfterLogin();
                if (stationURSFragmentAfterLogin != null) {
                    androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                    androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, stationURSFragmentAfterLogin);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        layout_state_urs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_state_urs.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                stateURSFragmentAfterLogin = new StateURSFragmentAfterLogin();
                if (stateURSFragmentAfterLogin != null) {
                    androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                    androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, stateURSFragmentAfterLogin);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });


        searchlistView = (NonScrollListView) findViewById(R.id.searchlistView);
        listfromdb();

        layout_MySchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isvisible == true) {
                    isvisible = false;

                    layout_myScheduleList.setVisibility(View.VISIBLE);
                    searchlistView.setVisibility(View.VISIBLE);
                    imgMySchedule.setImageResource((R.drawable.menu_up_arow));
                    layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MySchedule.setBackgroundColor(Color.parseColor("#00e9e9"));
                    llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                    layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                    layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                    layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                    layout_listview.setLayoutAnimation(new LayoutAnimationController(animation));
                } else {
                    isvisible = true;
                    layout_myScheduleList.setVisibility(View.GONE);
                    searchlistView.setVisibility(View.GONE);
                    imgMySchedule.setImageResource((R.drawable.menu_dwn_arrow));
                    layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                    llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                    layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                    layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                    layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                    // tv_news_search.setVisibility(View.GONE);
                    // Intent i=new Intent(getApplicationContext(), Activity_Previous_bid.class);
                }
            }
        });


        layout_news2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isvisible == true) {
                    isvisible = false;
                    layout_listview.setVisibility(View.VISIBLE);
                    searchlistView.setVisibility(View.VISIBLE);
                    imageView3.setImageResource((R.drawable.menu_up_arow));
                    layout_news2.setBackgroundColor(Color.parseColor("#00e9e9"));
                    llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                    layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                    layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                    layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                    layout_listview.setLayoutAnimation(new LayoutAnimationController(animation));
                } else {
                    isvisible = true;
                    layout_listview.setVisibility(View.GONE);
                    searchlistView.setVisibility(View.GONE);
                    imageView3.setImageResource((R.drawable.menu_dwn_arrow));
                    layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                    llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                    layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                    layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                    layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                    // tv_news_search.setVisibility(View.GONE);
                    // Intent i=new Intent(getApplicationContext(), Activity_Previous_bid.class);
                }
            }
        });

        layout_Transmission_Corridor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#00e9e9"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                transmissionCorridorFragmentAfterLogin = new TransmissionCorridorFragmentAfterLogin();
                if (transmissionCorridorFragmentAfterLogin != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, transmissionCorridorFragmentAfterLogin);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        realtimelistView = (NonScrollListView) findViewById(R.id.realtimelistView);
        realtime_element.add("RLDC wise Realtime Scheduling");
        realtime_element.add("State wise Realtime Scheduling");
        realtimelistView.setAdapter(new ReltimeSchedulelistCustomAdapter(this, realtime_element));

        layout_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isrealtimevisible == true) {
                    isrealtimevisible = false;
                    layout_realtime.setVisibility(View.VISIBLE);
                    realtimelistView.setVisibility(View.VISIBLE);
                    imageView4.setImageResource((R.drawable.menu_up_arow));
                    layout_rl.setBackgroundColor(Color.parseColor("#00e9e9"));
                    layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                    llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                    layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                    layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                    layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                    realtimelistView.setLayoutAnimation(new LayoutAnimationController(animation));
                } else {
                    isrealtimevisible = true;
                    layout_realtime.setVisibility(View.GONE);
                    realtimelistView.setVisibility(View.GONE);
                    imageView4.setImageResource((R.drawable.menu_dwn_arrow));
                    layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                    llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                    layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                    layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                    layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                    layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                    // tv_news_search.setVisibility(View.GONE);
                    // Intent i=new Intent(getApplicationContext(), Activity_Previous_bid.class);
                }
            }
        });

        realtimelistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, final int position, long id) {
                if (realtime_element.get(position).equalsIgnoreCase("RLDC wise Realtime Scheduling")) {
                    realtimeSchedulingFragmentAfterLogin = new RealtimeSchedulingFragmentAfterLogin();
                    if (realtimeSchedulingFragmentAfterLogin != null) {
                        androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                        androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, realtimeSchedulingFragmentAfterLogin);
                        fragmentTransaction.commit();
                        mDrawerLayout.closeDrawer(linearLayout);
                    } else {
                        Log.e("MainActivity", "Error in creating fragment");
                    }
                } else if (realtime_element.get(position).equalsIgnoreCase("State wise Realtime Scheduling")) {
                    statewiseRealtimeSchedulingFragmentAfterlogin = new StatewiseRealtimeSchedulingFragmentAfterlogin();
                    if (statewiseRealtimeSchedulingFragmentAfterlogin != null) {
                        androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                        androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, statewiseRealtimeSchedulingFragmentAfterlogin);
                        fragmentTransaction.commit();
                        mDrawerLayout.closeDrawer(linearLayout);
                    } else {
                        Log.e("MainActivity", "Error in creating fragment");
                    }
                }
            }
        });


        lv_mySchedule = (NonScrollListView) findViewById(R.id.lv_mySchedule);
        myScheduleElement.add("Setup Account");
        myScheduleElement.add("List View");
        myScheduleElement.add("Club View");
        //myScheduleElement.add("Dashboard");

        lv_mySchedule.setAdapter(new AdapterMyScheduling(this, myScheduleElement));
        lv_mySchedule.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, final int position, long id) {
                if (myScheduleElement.get(position).equalsIgnoreCase("Setup Account")) {
                    myScheduleSetUpFragmentAfterLogin = new MyScheduleSetUpFragmentAfterLogin();
                    if (myScheduleSetUpFragmentAfterLogin != null) {
                        androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                        androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, myScheduleSetUpFragmentAfterLogin);
                        fragmentTransaction.commit();
                        mDrawerLayout.closeDrawer(linearLayout);
                    } else {
                        Log.e("MainActivity", "Error in creating fragment");
                    }
                } else if (myScheduleElement.get(position).equalsIgnoreCase("List View")) {
                    myScheduleListviewFragmentAfterLogin = new MyScheduleListviewFragmentAfterLogin();
                    if (myScheduleListviewFragmentAfterLogin != null) {
                        androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                        androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, myScheduleListviewFragmentAfterLogin);
                        fragmentTransaction.commit();
                        mDrawerLayout.closeDrawer(linearLayout);
                    } else {
                        Log.e("MainActivity", "Error in creating fragment");
                    }
                } else if (myScheduleElement.get(position).equalsIgnoreCase("Club View")) {
                    myScheduleClubviewFragmentAfterLogin = new MyScheduleClubviewFragmentAfterLogin();
                    if (myScheduleClubviewFragmentAfterLogin != null) {
                        androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                        androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, myScheduleClubviewFragmentAfterLogin);
                        fragmentTransaction.commit();
                        mDrawerLayout.closeDrawer(linearLayout);
                    } else {
                        Log.e("MainActivity", "Error in creating fragment");
                    }
                }
            }
        });


		/*layout_Realtime_Scheduling.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
				llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
				layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
				layout_news2.setBackgroundColor(Color.parseColor("#009999"));

				layout_Realtime_Scheduling.setBackgroundColor(Color.parseColor("#00e9e9"));
				layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
				layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
				layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
				layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
				layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
				layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
				layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
				layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
				layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
				realtimeSchedulingFragment = new RealtimeSchedulingFragmentAfterLogin();
				if (realtimeSchedulingFragment !=null) {
					android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.content_frame, realtimeSchedulingFragment)
							.commit();
					mDrawerLayout.closeDrawer(linearLayout);
				}
				else {
					Log.e("MainActivity", "Error in creating fragment");
				}
			}
		});*/

        layout_Landed_Cost_Calculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                landedCostCalculatorFragment = new LandedCostCalculatorFragmentAfterLogin();
                if (landedCostCalculatorFragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, landedCostCalculatorFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        layout_Monthly_ACT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                monthlyATCFragmentAfterLogin = new MonthlyATCFragmentAfterLogin();
                if (monthlyATCFragmentAfterLogin != null) {
                    androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                    androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, monthlyATCFragmentAfterLogin);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

		/*layout_Statewise_Realtime_Scheduling.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
				layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
				layout_news2.setBackgroundColor(Color.parseColor("#009999"));
				layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
				layout_Realtime_Scheduling.setBackgroundColor(Color.parseColor("#009999"));
				layout_Statewise_Realtime_Scheduling.setBackgroundColor(Color.parseColor("#00e9e9"));
				layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
				layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
				layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
				layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
				layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
				layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
				layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
				layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
				layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
				statewiseRealtimeSchedulingFragmentAfterlogin = new StatewiseRealtimeSchedulingFragmentAfterlogin();
				if (statewiseRealtimeSchedulingFragmentAfterlogin !=null) {
					android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
					android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, statewiseRealtimeSchedulingFragmentAfterlogin);
					fragmentTransaction.commit();
					mDrawerLayout.closeDrawer(linearLayout);
				}
				else {
					Log.e("MainActivity", "Error in creating fragment");
				}
			}
		});*/

        layout_Power_Demand_Comparision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                powerDemandComparisionFragmentAfterlogin = new PowerDemandComparisionFragmentAfterlogin();
                if (powerDemandComparisionFragmentAfterlogin != null) {
                    androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                    androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, powerDemandComparisionFragmentAfterlogin);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

		/*layout_News_Letter.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
				layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
				layout_news2.setBackgroundColor(Color.parseColor("#009999"));
				layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
				layout_Realtime_Scheduling.setBackgroundColor(Color.parseColor("#009999"));
				layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
				layout_News_Letter.setBackgroundColor(Color.parseColor("#00e9e9"));
				layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
				layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
				layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
				newsLetterFragmentafterlogin = new NewsLetterFragmentafterlogin();
				if (newsLetterFragmentafterlogin !=null) {
					android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
					android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, newsLetterFragmentafterlogin);
					fragmentTransaction.commit();
					mDrawerLayout.closeDrawer(linearLayout);
				}
				else {
					Log.e("MainActivity", "Error in creating fragment");
				}
			}
		});*/

        layout_stateload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_stateload.setBackgroundColor(Color.parseColor("#00e9e9"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));

                stateLoadFragmentAfterLogin = new StateLoadFragmentAfterLogin();
                if (stateLoadFragmentAfterLogin != null) {
                    androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                    androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, stateLoadFragmentAfterLogin);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        layout_My_Schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                mySchedulingFragmentAfterLogin = new MySchedulingFragmentAfterLogin();
                if (mySchedulingFragmentAfterLogin != null) {
                    androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                    androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, mySchedulingFragmentAfterLogin);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        layout_Notification_Setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                notificationSettingFragment = new NotificationSettingFragmentafterLogin();
                if (notificationSettingFragment != null) {
                    androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, notificationSettingFragment)
                            .commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        layout_Contact_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                contactusFragment = new ContactusFragmentAfterLogin();
                if (contactusFragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, contactusFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        layout_Disclaimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                disclaimerFragment = new DisclaimerFragmentAfterLogin();
                if (disclaimerFragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, disclaimerFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });


        layout_Feebback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Feebback.setBackgroundColor(Color.parseColor("#00e9e9"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                feedBackFragment = new FeedBackFragmentAfterLogin();
                if (feedBackFragment != null) {
                    androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                    androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, feedBackFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        layout_MCP_Comparision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_MySchedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                mCP_ComparisionFragmentAfterLogin = new MCP_ComparisionFragmentAfterLogin();
                if (mCP_ComparisionFragmentAfterLogin != null) {
                    androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                    androidx.fragment.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, mCP_ComparisionFragmentAfterLogin);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

		/*layout_Disclaimer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final Dialog dialog = new Dialog(MainActivityAfterLogin.this);
				// Include dialog.xml file
				dialog.setContentView(R.layout.dialog_disclaimer);
				// Set dialog title
				dialog.setTitle( Html.fromHtml("<font color='#009999'>Disclaimer</font>"));
				String htmlText = " %s ";
				String textshow = "Invetech Solutions has taken due care and caution in compilation and reporting of data and information as has been obtained from various sources including which it considers reliable and first hand. However, Invetech Solutions does not guarantee the accuracy, adequacy or completeness of any information and is not responsible for errors or omissions or for the results obtained from the use of such information and especially states that it has no financial liability whatsoever to the users of contents of this page. This research and information does not constitute recommendation or advice for trading or investment purposes and therefore Invetech Solutions will not be liable for any loss accrued as a result of a trading/investment activity that is undertaken on the basis of information contained on this page. Invetech Solutions does not consider itself to undertake Regulated Activities as defined in Section 22 of the Financial Services and Markets Act 2000 and it is not registered with the Financial Services Authority of the UK.\n\nThanks\nInvetech Solutions";
				// set values for custom dialog components - text, image and button
				TextView text = (TextView) dialog.findViewById(R.id.textDialog);
				// text.loadData(String.format(htmlText, textshow), "text/html", "utf-8");
				dialog.show();


				Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
				// if decline button is clicked, close the custom dialog
				declineButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						// Close dialog
						dialog.dismiss();
					}
				});
			}
		});*/


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.toggle, // nav menu toggle icon
                R.string.sliderheading, // nav drawer open - description for
                // accessibility
                R.string.sliderheading // nav drawer close - description for
                // accessibility
        ) {
            public void onDrawerClosed(View view) {
                //getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                //getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            // on first time display view for first nav item
            View();
        }
    }

    private void View() {
        Bundle bundle = new Bundle();
        bundle.putString("domain", domain);
        MainActivityFragment mainActivityFragment = new MainActivityFragment();
        if (mainActivityFragment != null) {
            mainActivityFragment.setArguments(bundle);
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, mainActivityFragment);
            fragmentTransaction.commit();
            mDrawerLayout.closeDrawer(linearLayout);
        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    private void ViewClick() {
		/*Intent in = new Intent(MainActivityAfterLogin.this,
				ScheduleVsCurtailmentActivity.class);
		in.putExtra("revision", "revision");
		in.putExtra("date", "date");
		startActivity(in);
		overridePendingTransition(R.anim.animation, R.anim.animation2);
		finish();*/
        Intent in = new Intent(MainActivityAfterLogin.this,
                NewFeedMainActivity.class);
        in.putExtra("LOGINCONFIG", companyName);
        in.putExtra("revision", "revision");
        in.putExtra("date", "date");
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        mDrawerLayout.closeDrawer(linearLayout);
    }

    public void listfromdb() {
        Cursor newfeedsavec = db.rawQuery("select * from newfeedsave", null);
        if (newfeedsavec.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        while (newfeedsavec.moveToNext()) {
            saveResponse = newfeedsavec.getString(newfeedsavec.getColumnIndex("response"));
        }
        if (saveResponse.length() == 0) {
            new HttpAsyncTaskgetnewsfeedinfoAgain().execute(domain + "/mobile/pxs_app/service/getnewsfeedinfo.php");
        } else {
            try {
                JSONObject mainObject = new JSONObject(saveResponse);
                key_element.clear();
                key_element.add("All");
                Iterator<String> iter = mainObject.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    key_element.add(key);
                }
                searchlistView.setAdapter(new MenuCustomAdapter(this, key_element));

                searchlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            View view, final int position, long id) {
                        if (key_element.get(position).equalsIgnoreCase("All")) {
                            allNewsFeedFragmentafterlogin = new AllNewsFeedFragmentafterlogin();
                            if (allNewsFeedFragmentafterlogin != null) {
                                FragmentManager fragmentManager = getFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.content_frame, allNewsFeedFragmentafterlogin);
                                fragmentTransaction.commit();
                                mDrawerLayout.closeDrawer(linearLayout);
                            } else {
                                Log.e("MainActivity", "Error in creating fragment");
                            }
                        } else {
                            newsFeedSearchbyCategoryFragmentafterlogin = new NewsFeedSearchbyCategoryFragmentafterlogin();
                            if (newsFeedSearchbyCategoryFragmentafterlogin != null) {
                                FragmentManager fragmentManager = getFragmentManager();
                                Bundle bundle = new Bundle();
                                bundle.putString("Key", key_element.get(position));
                                newsFeedSearchbyCategoryFragmentafterlogin.setArguments(bundle);
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.content_frame, newsFeedSearchbyCategoryFragmentafterlogin);
                                fragmentTransaction.commit();
                                mDrawerLayout.closeDrawer(linearLayout);
                            } else {
                                Log.e("MainActivity", "Error in creating fragment");
                            }
                        }
                    }
                });

            } catch (JSONException e) {
                try {
                    JSONObject mainObject = new JSONObject(saveResponse);
                    String errormass = mainObject.getString("message");
                    Toast.makeText(MainActivityAfterLogin.this, errormass, Toast.LENGTH_LONG).show();
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                if (saveResponse != null) {
                    //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    Document html = Jsoup.parse(saveResponse);
                    String title = html.title();
                    Toast.makeText(MainActivityAfterLogin.this, "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();
        switch (id) {

            case R.id.action_call:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* *
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(linearLayout);
        menu.findItem(R.id.action_call).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     */
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        //getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void open() {
        // TODO Auto-generated method stub
        mDrawerLayout.openDrawer(Gravity.LEFT);

    }

    public String addPasserCallback(MainActivityAfterLogin.MyDataPasser passer) {
        passerArrayList.add(passer);
        return lastValue;
    }

    public boolean removePasserCallback(MainActivityAfterLogin.MyDataPasser passer) {
        return passerArrayList.remove(passer);
    }

    public void updateCallback(String data) {
        lastValue = data;
        for (MainActivityAfterLogin.MyDataPasser d : passerArrayList) {
            d.updateDateString(data);
        }
    }

    //resultIntent.putExtra(Constant.VALUE_APPNUMBERFROMGRAPH, data_idforgraph);
    @Override
    protected void onResume() {
        super.onResume();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String shedule = extras.getString(Constant.VALUE_SHEDULETRACK);
            if (shedule != null) {
                mySchedulingFragmentAfterLogin = new MySchedulingFragmentAfterLogin();
                if (mySchedulingFragmentAfterLogin != null) {
                    androidx.fragment.app.FragmentManager fragmentManager = getSupportFragmentManager();
                    androidx.fragment.app.FragmentTransaction fragmentTransaction =
                            fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, mySchedulingFragmentAfterLogin);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                }
            }
        }/*else{
			setInitView();
		}*/
    }

    public interface MyDataPasser {
        void updateDateString(String date);
    }

    private class HttpAsyncTaskgetnewsfeedinfoAgain extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //Toast.makeText(getBaseContext(), "Received!"+result, Toast.LENGTH_LONG).show();
            try {
                if (result.contains("'s")) {
                    result = result.replace("'s", "");
                }
                if (result.contains("'")) {
                    result = result.replace("'", "");
                }
				/*if(result.contains(",")){
					result = result.replace(",", " ");
				}*/
                //result = DatabaseUtils.sqlEscapeString(result);
                db.execSQL("DROP TABLE IF EXISTS newfeedsave");
                db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
                db.execSQL("INSERT INTO newfeedsave VALUES('" + result + "', '" + result + "', '" + result + "')");
                JSONObject mainObjectnotused = new JSONObject(result);

            } catch (JSONException e) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String errormass = mainObject.getString("message");
                    Toast.makeText(MainActivityAfterLogin.this, errormass, Toast.LENGTH_LONG).show();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                if (result != null) {
                    //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(MainActivityAfterLogin.this, "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            listfromdb();
        }
    }
}

