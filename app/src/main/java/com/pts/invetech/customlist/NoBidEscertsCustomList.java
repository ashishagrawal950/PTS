package com.pts.invetech.customlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.classes.activity.NoBidActivity;

import java.util.ArrayList;

/**
 * Created by Ashish on 16-10-2017.
 */

public class NoBidEscertsCustomList extends ArrayAdapter<String> {

    private Activity context;
    private ArrayList<String> serialnumberescerts;
    private  ArrayList<String>  nobiddateescerts;
    private int lastPosition = -1;

    public NoBidEscertsCustomList(NoBidActivity context,
                                  ArrayList<String> serialnumberescerts, ArrayList<String> nobiddateescerts
                              ) {
        super(context, R.layout.nobidrec_single_bid, serialnumberescerts);
        this.context = context;
        this.serialnumberescerts = serialnumberescerts;
        this.nobiddateescerts = nobiddateescerts;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.nobidrec_single_bid, null, true);
        TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
        TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
        txt1.setText(serialnumberescerts.get(position));
        txt2.setText(nobiddateescerts.get(position));
        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        rowView.startAnimation(animation);
        lastPosition = position;
        //txt3.setText("Delete");
        // imageView.setImageResource(imageId[position]);
        return rowView;
    }
}