package com.pts.invetech.customlist;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.pts.invetech.R;
import com.pts.invetech.utils.AppLogger;
import com.pts.model.appned.AdapterYearModel;
import com.pts.model.appned.YearMonthModel;

import java.util.ArrayList;

/**
 * Created by Ashish on 27-07-2017.
 */

public class YearandMontnthCostumAdapter  extends BaseAdapter {

    private Context context;
    private ArrayList<AdapterYearModel> dataList;

    public YearandMontnthCostumAdapter(Context _context,
                                       ArrayList<AdapterYearModel> _adapterList) {

        context=_context;
        dataList=_adapterList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public AdapterYearModel getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        YrMonthViewHolder vHolder;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_dialog, null, true);

            vHolder=new YrMonthViewHolder();
            vHolder.checkHolder=(CheckBox) view.findViewById(R.id.chk_year);
            vHolder.checkHolder.setOnCheckedChangeListener(checkListner);
            view.setTag(vHolder);

        } else {
            vHolder= (YrMonthViewHolder) view.getTag();
        }

        AdapterYearModel aModel=getItem(position);

        vHolder.position=position;
        vHolder.checkHolder.setText(aModel.getName());
        vHolder.checkHolder.setChecked(aModel.isClick());

        return view;

    }

    private CompoundButton.OnCheckedChangeListener checkListner = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            YrMonthViewHolder tmp=(YrMonthViewHolder) buttonView.getTag();
            int pos = tmp.position;
            AdapterYearModel aModel=getItem(pos);
            aModel.setClick(isChecked);
        }
    };

    public YearMonthModel getAllYears(){
        YearMonthModel tmp=new YearMonthModel();
        StringBuilder buffer=new StringBuilder();
        for(int index=0;index<dataList.size();index++){
            AdapterYearModel chkModel=dataList.get(index);
            if(chkModel.isClick()){
                buffer.append(chkModel.getName()+",");
                tmp.addMonth(chkModel.getName());
            }
        }
        tmp.setTitle(buffer.toString());
        return tmp;
    }

    public String getAllYearsNames(){

        StringBuilder buffer=new StringBuilder();

        for(int index=0;index<dataList.size();index++){
            AdapterYearModel chkModel=dataList.get(index);
            if(chkModel.isClick()){
                buffer.append(chkModel.getName()+",");
            }
        }

        return buffer.toString();
    }

    public ArrayList<String> getYrlist(){
        ArrayList<String> yrList=new ArrayList<>();

        for(int index=0;index<dataList.size();index++){
            AdapterYearModel chkModel=dataList.get(index);
            if(chkModel.isClick()){
                yrList.add(chkModel.getId());
            }
        }

        return yrList;
    }

    static class YrMonthViewHolder{
        CheckBox checkHolder;
        int position;
    }
}