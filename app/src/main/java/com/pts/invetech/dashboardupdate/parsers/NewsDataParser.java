package com.pts.invetech.dashboardupdate.parsers;

import com.pts.invetech.dashboardupdate.model.news.NewsDataInter;
import com.pts.invetech.dashboardupdate.model.news.NewsDataUpdate;
import com.pts.invetech.dashboardupdate.model.news.SubNews;
import com.pts.invetech.utils.AppLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by vaibhav on 20/1/17.
 */

public class NewsDataParser {

    private static final String ID = "id";
    private static final String TITLE = "title";
    private static final String TYPE = "type";
    private static final String LINK = "link";
    private static final String COMMENT = "comment";
    private static final String PUBDATE = "pubdate";
    private static final String TIME = "time";
    private static final String SOURCE = "source";



    public static NewsDataUpdate parseNews(String str) throws JSONException {
        if (str == null) {
            return null;
        }

        JSONObject parent = new JSONObject(str);
        Iterator<String> keys = parent.keys();

        NewsDataUpdate dataUpdate = new NewsDataUpdate();
        while (keys.hasNext()) {

            String tmpData = keys.next();
            AppLogger.showMsgWithoutTag(tmpData);

            NewsDataInter interData = new NewsDataInter();
            interData.setTitle(tmpData);

            JSONArray array = parent.getJSONArray(tmpData);

            for (int index = 0; index < array.length(); index++) {
                JSONObject obj = array.getJSONObject(index);
                interData.add(parseSubNews(obj));
            }

            dataUpdate.addNewsInter(interData);
        }
        return dataUpdate;

    }

    private static SubNews parseSubNews(JSONObject obj) throws JSONException {
        SubNews subNews = new SubNews();

        if (obj.has(ID)) subNews.setId(obj.getString(ID));
        if (obj.has(TITLE)) subNews.setTitle(obj.getString(TITLE));
        if (obj.has(TYPE)) subNews.setType(obj.getString(TYPE));
        if (obj.has(LINK)) subNews.setLink(obj.getString(LINK));
        if (obj.has(COMMENT)) subNews.setComment(obj.getString(COMMENT));
        if (obj.has(PUBDATE)) subNews.setPubdate(obj.getString(PUBDATE));
        if (obj.has(TIME)) subNews.setTime(obj.getString(TIME));
        if (obj.has(SOURCE)) subNews.setSource(obj.getString(SOURCE));

        return subNews;
    }

}
