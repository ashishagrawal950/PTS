package com.pts.invetech.utils;

import java.text.DateFormatSymbols;

/**
 * Created by vaibhav on 18/10/16.
 */

public class AppDateUtils {

    public static String getCopyDateFormatted(int selectedYear, int selectedMonth, int selectedDay) {
        StringBuilder currentDate = new StringBuilder();
        currentDate.append(selectedYear + "-");
        if (selectedMonth < 10) {
            currentDate.append("0" + selectedMonth + "-");
        } else {
            currentDate.append(selectedMonth + "-");
        }

        if (selectedDay < 10) {
            currentDate.append("0" + selectedDay);
        } else {
            currentDate.append(selectedDay + "");
        }

        return currentDate.toString();
    }


    public static String getCopyShowDate(int selectedYear, int selectedMonth, int selectedDay) {

        StringBuilder currentDate = new StringBuilder();
        if (selectedDay < 10) {
            currentDate.append("0" + selectedDay);
        } else {
            currentDate.append(selectedDay + "");
        }
        String mon=new DateFormatSymbols().getShortMonths()[selectedMonth];
        currentDate.append(" "+mon+" ");
        currentDate.append(selectedYear);

        return currentDate.toString();
    }

    public static String getReverseDate(String date){
        String[] dates=date.split("-");
        String dts=dates[2]+"-"+dates[1]+"-"+dates[0];
        return dts;
    }

    public static int getMonthNumber(String monthName) {
        if(monthName.equalsIgnoreCase("Apr")){
            return 1;
        }else if(monthName.equalsIgnoreCase("May")){
            return 2;
        }else if(monthName.equalsIgnoreCase("Jun")){
            return 3;
        }else if(monthName.equalsIgnoreCase("Jul")){
            return 4;
        }else if(monthName.equalsIgnoreCase("Aug")){
            return 5;
        }else if(monthName.equalsIgnoreCase("Sep")){
            return 6;
        }else if(monthName.equalsIgnoreCase("Oct")){
            return 7;
        }else if(monthName.equalsIgnoreCase("Nov")){
            return 8;
        }else if(monthName.equalsIgnoreCase("Dec")){
            return 9;
        }else if(monthName.equalsIgnoreCase("Jan")){
            return 10;
        }else if(monthName.equalsIgnoreCase("Feb")){
            return 11;
        }else{
            return 12;
        }

    }

}
