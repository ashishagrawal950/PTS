package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NewsLetterPDFshowActivity extends Activity {

    private Context context;
    private LinearLayout mopenLayout;
    private String client_id;
    private SQLiteDatabase db;
    private ImageView ivshare, ivdownload;
    private String categoryurlfileSend, newsletterfilenameSend,newsletterBaseUrl;
    private int numMessagesOne = 0;
    private NotificationManager myNotificationManager;
    private int notificationIdOne = 111;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_newsletterpdfshow);
        //in.putExtra("newsletterurl", newsletterurlone);
        //in.putExtra("newsletterfilename", partsix);
        newsletterBaseUrl = SharedPrefHandler.getDeviceString(getApplicationContext(), "newsletterBaseUrl");
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            categoryurlfileSend = extras.getString("newsletterurl");
            newsletterfilenameSend = extras.getString("newsletterfilename");

        } else {
            //..oops!
        }
        ivshare = (ImageView) findViewById(R.id.ivshare);
        ivdownload = (ImageView) findViewById(R.id.ivdownload);
        if (categoryurlfileSend.contains(".pdf")) {
            ivshare.setVisibility(View.VISIBLE);
            ivdownload.setVisibility(View.GONE);
        } else {
            ivshare.setVisibility(View.GONE);
            ivdownload.setVisibility(View.GONE);
        }
        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });
        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(NewsLetterPDFshowActivity.this, NewsLetterActivity.class);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });
		
		
		
		
		
		/*ivshare.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Resources resources = getResources();

			    Intent emailIntent = new Intent();
			    emailIntent.setAction(Intent.ACTION_SEND);
			    // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
			    emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_native)));
			    emailIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
			    emailIntent.setType("message/rfc822");

			    PackageManager pm = getPackageManager();
			    Intent sendIntent = new Intent(Intent.ACTION_SEND);     
			    sendIntent.setType("text/plain");

			    Intent openInChooser = Intent.createChooser(emailIntent, resources.getString(R.string.share_chooser_text));
			    List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
			    List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();        
			    for (int i = 0; i < resInfo.size(); i++) {
			        // Extract the label, append it, and repackage it in a LabeledIntent
			        ResolveInfo ri = resInfo.get(i);
			        String packageName = ri.activityInfo.packageName;
			        if(packageName.contains("android.email")) {
			            emailIntent.setPackage(packageName);
			        } else if(packageName.contains("whatsapp") || packageName.contains("gm")) {
			            Intent intent = new Intent();
			            intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
			            intent.setAction(Intent.ACTION_SEND);
			            intent.setType("text/plain");
			            if(packageName.contains("whatsapp")) {
			            	String myFilePath = Environment.getExternalStorageDirectory().getPath()+"/"+newsletterfilename;
				            File fileWithinMyDir = new File(myFilePath);
				            if(fileWithinMyDir.exists()) {
				            	intent.setType("application/pdf");
				            	intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+fileWithinMyDir));
				            	//intent.putExtra(Intent.EXTRA_SUBJECT,"PTS");
				            	//intent.putExtra(Intent.EXTRA_TEXT, "PTS");
				               //startActivity(Intent.createChooser(intent, "Share File"));
				            }
			            	
			               // intent.putExtra(Intent.EXTRA_TEXT, resources.getString(R.string.share_twitter));
			            } 
			            else if(packageName.contains("android.gm")) {
			                intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_gmail)));
			                intent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));               
			                intent.setType("message/rfc822");
			                String myFilePath = Environment.getExternalStorageDirectory().getPath()+"/"+newsletterfilename;
				            File fileWithinMyDir = new File(myFilePath);
				            if(fileWithinMyDir.exists()) {
				            	intent.setType("application/pdf");
				            	intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+myFilePath));
				            	intent.putExtra(Intent.EXTRA_SUBJECT,"PTS");
				            	intent.putExtra(Intent.EXTRA_TEXT, "PTS");
				            //startActivity(Intent.createChooser(intent, "Share File"));
				            }
			            }
			            intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
			        }
			    }

			    // convert intentList to array
			    LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);

			    openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
			    startActivity(openInChooser);       
			}
			
		});*/


        ivshare.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Resources resources = getResources();

                Intent emailIntent = new Intent();
                emailIntent.setAction(Intent.ACTION_SEND);
                // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
                emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_native)));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
                emailIntent.setType("message/rfc822");

                PackageManager pm = getPackageManager();
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");

                Intent openInChooser = Intent.createChooser(emailIntent, resources.getString(R.string.share_chooser_text));
                List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
                List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
                for (int i = 0; i < resInfo.size(); i++) {
                    // Extract the label, append it, and repackage it in a LabeledIntent
                    ResolveInfo ri = resInfo.get(i);
                    String packageName = ri.activityInfo.packageName;
                    if (packageName.contains("android.email")) {
                        emailIntent.setPackage(packageName);
                    } else if (packageName.contains("whatsapp") || packageName.contains("gm")) {
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        if (packageName.contains("whatsapp")) {
                            String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/" + newsletterfilenameSend;
                            File fileWithinMyDir = new File(myFilePath);
                            if (fileWithinMyDir.exists()) {
                                intent.setType("application/*");
                                intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + myFilePath));
                                //intent.putExtra(Intent.EXTRA_SUBJECT,"PTS");
                                //intent.putExtra(Intent.EXTRA_TEXT, "PTS");
                                //startActivity(Intent.createChooser(intent, "Share File"));
                            }

                            // intent.putExtra(Intent.EXTRA_TEXT, resources.getString(R.string.share_twitter));
                        } else if (packageName.contains("android.gm")) {
                            intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_gmail)));
                            intent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
                            intent.setType("message/rfc822");
                            String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/" + newsletterfilenameSend;
                            File fileWithinMyDir = new File(myFilePath);
                            if (fileWithinMyDir.exists()) {
                                intent.setType("application/pdf");
                                intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + myFilePath));
                                intent.putExtra(Intent.EXTRA_SUBJECT, "PTS");
                                intent.putExtra(Intent.EXTRA_TEXT, "PTS");
                                //startActivity(Intent.createChooser(intent, "Share File"));
                            }
                        }
                        intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
                    }
                }

                // convert intentList to array
                LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);

                openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
                startActivity(openInChooser);
            }

        });

        WebView myWebView = (WebView) findViewById(R.id.webView);
        myWebView.setClickable(true);
        myWebView.setFocusableInTouchMode(true);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setPluginState(PluginState.ON);

        //---you need this to prevent the webview from
        // launching another browser when a url
        // redirection occurs---
        //myWebView.setWebViewClient(new Callback());
        //String webUrl = "http://"+appurl+categoryurl;
        myWebView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + newsletterBaseUrl +"/"+categoryurlfileSend);
        WebClientClass webViewClient = new WebClientClass();
        myWebView.setWebViewClient(webViewClient);
        WebChromeClient webChromeClient = new WebChromeClient();
        myWebView.setWebChromeClient(webChromeClient);

    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(NewsLetterPDFshowActivity.this, NewsLetterActivity.class);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    public class WebClientClass extends WebViewClient {
        Dialog prgDialog = null;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
		   /*pd = new ProgressDialog(NewsLetterPDFshowActivity.this);
		   pd.setTitle("Please wait");
		   pd.setMessage("Loading..");
		   pd.show();*/
            prgDialog = new Dialog(NewsLetterPDFshowActivity.this);
            prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            prgDialog.setContentView(R.layout.progessdialog);
            ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
            image.startAnimation(animation);
            prgDialog.show();
            prgDialog.setCancelable(false);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if(prgDialog!=null)
              prgDialog.dismiss();
        }
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileAsync extends AsyncTask<String, String, String> {
        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //prgDialog1.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            Random r = new Random();
            int i1 = r.nextInt(80 - 65) + 65;
            String string = f_url[0];
            string.endsWith(".xls");
            try {
                if (string.endsWith(".pdf")) {
                    try {
                        URL url = new URL(f_url[0]);
                        URLConnection conection = url.openConnection();
                        conection.connect();
                        // getting file length
                        int lenghtOfFile = conection.getContentLength();

                        // input stream to read file - with 8k buffer
                        InputStream input = new BufferedInputStream(
                                url.openStream(), 8192);
                        String[] filename = string.split("/");
                        String filenameSave = filename[filename.length - 1];
                        // Output stream to write file
                        //String[] filenamePDF = filenameSave.split(".");
                        //String filenamepng = filenamePDF[1];
                        OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/" + filenameSave);

                        byte[] data = new byte[1024];
                        long total = 0;
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress(""
                                    + (int) ((total * 100) / lenghtOfFile));

                            // writing data to file
                            output.write(data, 0, count);
                        }

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();

                    } catch (Exception e) {
                        Log.e("Error: ", e.getMessage());
                    }
                } else if (string.endsWith(".xlsx")) {
                    try {
                        URL url = new URL(f_url[0]);
                        URLConnection conection = url.openConnection();
                        conection.connect();
                        // getting file length
                        int lenghtOfFile = conection.getContentLength();

                        // input stream to read file - with 8k buffer
                        InputStream input = new BufferedInputStream(
                                url.openStream(), 8192);

                        // Output stream to write file
                        String[] filename = string.split("/");
                        String filenameSave = filename[filename.length - 1];
                        // Output stream to write file
                        OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/" + filenameSave);

                        byte[] data = new byte[1024];
                        long total = 0;
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress(""
                                    + (int) ((total * 100) / lenghtOfFile));

                            // writing data to file
                            output.write(data, 0, count);
                        }

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();

                    } catch (Exception e) {
                        Log.e("Error: ", e.getMessage());
                    }
                } else if (string.endsWith(".xls")) {
                    try {
                        URL url = new URL(f_url[0]);
                        URLConnection conection = url.openConnection();
                        conection.connect();
                        // getting file length
                        int lenghtOfFile = conection.getContentLength();

                        // input stream to read file - with 8k buffer
                        InputStream input = new BufferedInputStream(
                                url.openStream(), 8192);

                        // Output stream to write file
                        String[] filename = string.split("/");
                        String filenameSave = filename[filename.length - 1];
                        // Output stream to write file
                        OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/" + filenameSave);

                        byte[] data = new byte[1024];
                        long total = 0;
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress(""
                                    + (int) ((total * 100) / lenghtOfFile));

                            // writing data to file
                            output.write(data, 0, count);
                        }

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();

                    } catch (Exception e) {
                        Log.e("Error: ", e.getMessage());
                    }
                }

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //prgDialog1.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            //prgDialog1.dismiss();
            // Displaying downloaded image into image view
            // Reading image path from sdcard
            //String imagePath = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.jpg";
            // setting downloaded into image view
            //my_image.setImageDrawable(Drawable.createFromPath(imagePath));
        }
    }


}