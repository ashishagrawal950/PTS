package com.pts.invetech.pojo;

/**
 * Created by Ashish Karn on 28-06-2016.
 */
public class MorePojo {
    private String tv_content,tv_price,tv_qty;


    public MorePojo(String line1,String line2,String line3) {
        this.tv_content = line1;
        this.tv_price = line2;
        this.tv_qty = line3;

    }

    public String getLine1() {return tv_content;}
    public String getLine2() {return tv_price;}
    public String getLine3() {return tv_qty;}



}