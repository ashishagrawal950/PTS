package com.pts.invetech.classes.activity;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.INetworkCallback;
import com.pts.handler.NetworkHandlerNewModel;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.StationwisedataURSdetailsAPIResponse;
import com.pts.invetech.classes.fragment.CurrentAvaiableURSFragment;
import com.pts.invetech.customlist.StationWiseCustomList;
import com.pts.invetech.customlist.URSGeneratorWisenameCustomList;
import com.pts.invetech.pojo.AvaiableURS;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.JSONUtils;
import com.pts.invetech.utils.Utility;
import com.pts.model.stationmain.stationwise.State;
import com.pts.model.stationmain.stationwise.StationWise;
import com.pts.model.stationmain.stationwise.Value;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class StationURSDetailsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private SQLiteDatabase db;
    private Dialog prgDialog;
    private String station_name_Send, available_Send;
    private String available, generator_name, region, revision, fromtime, totime, disable;
    private ArrayList<String> serialarray = new ArrayList<String>();
    private ArrayList<String> availablearray = new ArrayList<String>();
    private ArrayList<String> fromtimearray = new ArrayList<String>();
    private ArrayList<String> totimearray = new ArrayList<String>();
    private ArrayList<String> disablearray = new ArrayList<String>();
    private TextView tvgenertorname, tvtotal_amount, tvnodatafound;
    private ListView lv_more;
    private String device_id, domain;
    private LinearLayout openLayout;
    private ProgressBar pbprogressBar;
    private String from;
    private String region_Send, owner_Send, revision_Send;
    private StationWiseCustomList adapter;

    private CurrentAvaiableURSFragment currentAvaiableURSFragment = null;
    private INetworkCallback callback = new INetworkCallback() {

        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
//                AppLogger.showToastShort(getApplicationContext(), "No Data Found");
                tvnodatafound.setVisibility(View.VISIBLE);
                tvnodatafound.setText("Data Not Found.");
                lv_more.setVisibility(View.GONE);
                return;
            }
            tvnodatafound.setVisibility(View.GONE);
            lv_more.setVisibility(View.VISIBLE);
            StationWise stationWise = (StationWise) obj;
            Value value = stationWise.getValue();
            List<State> states = value.getStates();
            adapter = new StationWiseCustomList(
                    StationURSDetailsActivity.this,
                    states);
            lv_more.setAdapter(adapter);

            Utility.setListViewHeightBasedOnChildren(lv_more);


        }

    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_stationurs);

        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
		 /*prgDialog = new ProgressDialog(CurrentAvaiableURSDetailsActivity.this);
	     prgDialog.setMessage("Please wait...");
	     prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        openLayout = (LinearLayout) findViewById(R.id.openLayout);
        tvgenertorname = (TextView) findViewById(R.id.tvgenertorname);
        tvtotal_amount = (TextView) findViewById(R.id.tvtotal_amount);
        tvnodatafound = (TextView) findViewById(R.id.tvnodatafound);
        pbprogressBar = (ProgressBar) findViewById(R.id.pbprogressBar);


        Intent in = getIntent();
        region_Send = in.getStringExtra("region_Send");
        station_name_Send = in.getStringExtra("station_name_Send");
        available_Send = in.getStringExtra("available_Send");
        revision_Send = in.getStringExtra("revision_Send");


        tvgenertorname.setText(station_name_Send);
        tvtotal_amount.setText(available_Send);
        lv_more = (ListView) findViewById(R.id.lv_more);
        from = "start";
        lv_more.setOnItemClickListener(this);

        ConnectionDetector cd = new ConnectionDetector(StationURSDetailsActivity.this);
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(StationURSDetailsActivity.this, "Please Check your network.",
                    Toast.LENGTH_LONG).show();
        } else {
            setStationWise();
//            new HttpAsyncTaskstationwisedata().execute("http://mittalpower.com/mobile/pxs_app/service/newurs/stationwisedata.php");
        }


        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

		/*new CountDownTimer(120000, 1000) {
			public void onTick(long millisUntilFinished) {
				// mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
			}
			public void onFinish() {
				from = "counter";
					new HttpAsyncTaskgetursdata_generatorwise().execute("http://www.mittalpower.com/mobile/pxs_app/service/getursdata_generatorwise.php");
			}
		}.start();*/

    }

    private void setStationWise() {
        JSONObject param = new JSONObject();
        try {
            param.put("device_id", device_id);
            param.put("region", region_Send);
            param.put("station", station_name_Send);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String[] str = new String[2];

        str[0] = domain + "/services/mobile_service/new_urs/stationwisedata.php";
        str[1] = param.toString();

        AppLogger.showMsg("param", param.toString());

        new NetworkHandlerNewModel(this, callback, StationWise.class, 1).execute(str);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == lv_more) {
            String station = station_name_Send;
            Pattern regex = Pattern.compile("[&]");
            Intent intent = new Intent(this, StationBlockWiseURSActivity.class);
            intent.putExtra("type", "STATIONWISE");
            intent.putExtra("region", region_Send);
            if (regex.matcher(station).find()) {
                intent.putExtra("station", station.replaceAll("[&]+", "~"));
            } else {
                intent.putExtra("station", station);
            }
            intent.putExtra("beneficiary", adapter.getItem(position).getName());
            intent.putExtra("revision_Send", revision_Send);
            startActivity(intent);
        }
    }


    private class HttpAsyncTaskstationwisedata extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AvaiableURS avaiableURS = new AvaiableURS();
            // {"device_id":867935024394966,"region":"NRLDC","station":"RIHAND1","owner":"NTPCCOAL"}
            avaiableURS.setDevice_id(device_id);
            avaiableURS.setRegion(region_Send);
            avaiableURS.setName(station_name_Send);
            avaiableURS.setOwner(owner_Send);
            return StationwisedataURSdetailsAPIResponse.POST(urls[0], avaiableURS);
        }

        @Override
        protected void onPreExecute() {
            if (from.equalsIgnoreCase("counter")) {
                pbprogressBar.setVisibility(View.VISIBLE);
            } else if (from.equalsIgnoreCase("start")) {
                prgDialog.show();
            }
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            if (from.equalsIgnoreCase("counter")) {
                pbprogressBar.setVisibility(View.INVISIBLE);
            } else if (from.equalsIgnoreCase("start")) {
                prgDialog.hide();
            }
            //Toast.makeText(getBaseContext(), "result" + result, Toast.LENGTH_LONG).show();
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    availablearray.clear();
                    fromtimearray.clear();
                    totimearray.clear();
                    disablearray.clear();
                    JSONObject mainObject = new JSONObject(result);
                    if (mainObject.has("generator_name")) {
                        generator_name = mainObject.getString("generator_name");
                    }
                    if (mainObject.has("region")) {
                        region = mainObject.getString("region");
                    }
                    if (mainObject.has("revision")) {
                        revision = mainObject.getString("revision");
                    }
                    int i;
                    JSONArray data = mainObject.getJSONArray("data");
                    for (i = 0; i < data.length(); i++) {
                        JSONObject datadata = data.getJSONObject(i);
                        if (datadata.has("available")) {
                            available = datadata.getString("available");
                        }
                        if (datadata.has("fromtime")) {
                            fromtime = datadata.getString("fromtime");
                        }
                        if (datadata.has("totime")) {
                            totime = datadata.getString("totime");
                        }
                        if (datadata.has("disable")) {
                            disable = datadata.getString("disable");
                        }
                        int counting = (i + 1);
                        String j = String.valueOf(counting);
                        serialarray.add(j);
                        availablearray.add(available);
                        fromtimearray.add(fromtime);
                        totimearray.add(totime);
                        disablearray.add(disable);
                    }
                    URSGeneratorWisenameCustomList adapter = new URSGeneratorWisenameCustomList(StationURSDetailsActivity.this,
                            serialarray, fromtimearray, totimearray, availablearray, disablearray);
                    lv_more.setAdapter(adapter);
                    Utility.setListViewHeightBasedOnChildren(lv_more);
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(StationURSDetailsActivity.this, errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(StationURSDetailsActivity.this, "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(StationURSDetailsActivity.this, "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(StationURSDetailsActivity.this, "Invalid JSON: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }


}

