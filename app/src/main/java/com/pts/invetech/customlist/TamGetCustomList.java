package com.pts.invetech.customlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.pts.invetech.R;

import java.util.ArrayList;

/**
 * Created by Ashish on 15-02-2017.
 */

public class TamGetCustomList extends ArrayAdapter<String> {

    private Activity context;
    private ArrayList<String> tamgetserialnumber;
    private ArrayList<String> tamgetfromtime;
    private ArrayList<String> tamgettotime;
    private ArrayList<String> tamgetbidquantum;

    public TamGetCustomList(Activity context, ArrayList<String> tamgetserialnumber, ArrayList<String> tamgetfromtime,
                            ArrayList<String> tamgettotime, ArrayList<String> tamgetbidquantum) {
        // TODO Auto-generated constructor stub
        super(context, R.layout.list_new_bid, tamgetserialnumber);
        this.context = context;
        this.tamgetserialnumber = tamgetserialnumber;
        this.tamgetfromtime = tamgetfromtime;
        this.tamgettotime = tamgettotime;
        this.tamgetbidquantum = tamgetbidquantum;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_new_bid, null, true);
        TextView txt5 = (TextView) rowView.findViewById(R.id.txt5);
        TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
        TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
        TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
        txt5.setText(tamgetserialnumber.get(position));
        txt1.setText(tamgetfromtime.get(position));
        txt2.setText(tamgettotime.get(position));
        txt3.setText(tamgetbidquantum.get(position));
        return rowView;
    }
}