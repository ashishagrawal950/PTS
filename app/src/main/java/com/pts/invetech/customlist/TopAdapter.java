package com.pts.invetech.customlist;

import com.pts.invetech.R;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class TopAdapter extends RecyclerView.Adapter<TopAdapter.MyViewHolder> {

	private LayoutInflater layoutInflater;
	public TopAdapter(Context context) {
		layoutInflater = LayoutInflater.from(context);
	}

	public class MyViewHolder extends RecyclerView.ViewHolder {
		public TextView title, year, genre;

		public MyViewHolder(View view) {
			super(view);
//			title = (TextView) view.findViewById(R.id.title);
//			genre = (TextView) view.findViewById(R.id.genre);
//			year = (TextView) view.findViewById(R.id.year);
		}
	}

	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return 10;
	}

	@Override
	public void onBindViewHolder(MyViewHolder holder, int arg1) {
		holder.title.setText("Title");
		holder.genre.setText("genre");
		holder.year.setText("year");
	}

	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup root, int arg1) {
		View itemView = layoutInflater.inflate(R.layout.tmp_file, root);
		return new MyViewHolder(itemView);
	}


}