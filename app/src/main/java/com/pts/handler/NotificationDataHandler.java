package com.pts.handler;

import com.pts.invetech.pojo.NotificationData;

public class NotificationDataHandler {

    public static void changeCurtailment(NotificationData data,String val){
        data.setPrecurtailment(val);
        data.setNLDCTOIEX(val);
        data.setNOTAFFECTED(val);
        data.setREALTIME(val);
        data.setRESTORE(val);
    }

    public static void changeNewsNotification(NotificationData data,String val){
        data.setNewsfeedactivity(val);
    }

    public static void changeBidNotification(NotificationData data, String val) {
        data.setMenunobid(val);
        data.setRecplacenewbid(val);
        data.setRecnobid(val);
        data.setMenunewbid(val);
    }

    public static void changeNewsLetter(NotificationData data, String val){
        data.setNEWSLETTER(val);
    }

    public static void changeAllNotification(NotificationData data,String val){

        data.setMCP(val);
        data.setNEWSLETTER(val);
        data.setMenuhome(val);
        data.setMenumarketprice(val);
        data.setMenudownload(val);
        data.setMenunewbid(val);
        data.setMenureport(val);
        data.setMenunobid(val);
        data.setRecplacenewbid(val);
        data.setRecnobid(val);
        data.setPrecurtailment(val);
        data.setNLDCTOIEX(val);
        data.setNOTAFFECTED(val);
        data.setREALTIME(val);
        data.setRESTORE(val);
        data.setNewsfeedactivity(val);

    }
}