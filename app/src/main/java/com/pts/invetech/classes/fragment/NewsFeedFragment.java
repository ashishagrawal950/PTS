package com.pts.invetech.classes.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.AlertDialogManager;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.GPSTracker;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.GPSTrakerAPIResponse;
import com.pts.invetech.apiresponse.LastYearMarketPriceAPIResponse;
import com.pts.invetech.classes.activity.LastYearMarketPriceActivity;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.activity.NewsFeedActivityWebView;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.customlist.EnergyNewfeedCustomList;
import com.pts.invetech.customlist.GraphNewfeedCustomList;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.pojo.GPSTrakerpojo;
import com.pts.invetech.pojo.LastYearMarketPrice;
import com.pts.invetech.utils.JSONUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class NewsFeedFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private final Context context = getActivity();
    public boolean minisTrue = true;
    public boolean avgisTrue = true;
    public boolean maxisTrue = true;
    // alert dialog manager
    private AlertDialogManager alert = new AlertDialogManager();
    // Internet detector
    private ConnectionDetector cd;
    private Dialog prgDialog;
    private Editor editor;
    private String descinvno, descriptiondata, uniprice, ammount, disccount, quantity,
            dusername, dpassword;
    private Boolean isInternetPresent = false;
    private SharedPreferences pref;
    private SQLiteDatabase db;
    private String device_id;
    private String access_key, status, value, message, category, companyName, client_id;
    private NonScrollListView energylist, graphlist;
    private String icontypefrom, dbacess, displaydate, displaytime;
    private String[] icontypearray;
    private String[] array;
    private String[] arraydispalydate;
    private String[] arraydispalytime;
    private Integer[] elementiconarray;
    private TextView tvRENEWABLE, tvPOWER, tvenergy, tvEVENT, tvCOAL, tvreadmore, tvnewsseven, tvnewseight, tvnewsnine, tvnewsten, tvnewseleven, tvnewstwavle,
            tvnewsthirdteen, tvnewsfourteen, tvnewsfifteen, tvnewssixteen;
    private TextView tvstripRENEWABLE, tvstripPOWER, tvstripenergy, tvstripEVENT, tvstripCOAL, tvstripreadmore, tvstripnewsseven, tvstripnewseight, tvstripnewsnine, tvstripnewsten, tvstripnewseleven,
            tvstripnewstwavle, tvstripnewsthirdteen, tvstripnewsfourteen, tvstripnewsfifteen, tvstripnewssixteen;
    private int width;
    private HorizontalScrollView hsview;
    private LinearLayout llhsview, lltoplogin, lltoplogout;
    private String loginconfig;
    private JSONObject mainObject;
    private ArrayList<String> key_element = new ArrayList<String>();
    private ArrayList<Integer> elementiconarray_element = new ArrayList<Integer>();
    private ArrayList<String> id_key_element = new ArrayList<String>();
    private ArrayList<String> title_key_element = new ArrayList<String>();
    private ArrayList<String> type_key_element = new ArrayList<String>();
    private ArrayList<String> link_key_element = new ArrayList<String>();
    private ArrayList<String> comment_key_element = new ArrayList<String>();
    private ArrayList<String> pubdate_key_element = new ArrayList<String>();
    private ArrayList<String> time_key_element = new ArrayList<String>();
    private ArrayList<String> source_key_element = new ArrayList<String>();
    private ArrayList<String> date_Array = new ArrayList<String>();
    private ArrayList<String> min_Array = new ArrayList<String>();
    private ArrayList<String> max_Array = new ArrayList<String>();
    private ArrayList<String> avg_Array = new ArrayList<String>();
    private ArrayList<String> lastyearmarketprice_Arrayone = new ArrayList<String>();
    private ArrayList<String> lastyearmarketprice_Arraytwo = new ArrayList<String>();
    private ArrayList<String> lastyearmarketprice_Arraythree = new ArrayList<String>();
    private ArrayList<String> lastyearmarketprice_Arrayfour = new ArrayList<String>();
    private ArrayList<ArrayList<String>> dataArraya = new ArrayList<ArrayList<String>>();
    private TextView tvlogintext, tvlogouttext;
    private String sprevision_send, spdate_send, saveResponse;
    private LinearLayout ll_expand_graph, openLayout, layout_strip_mcp, layout_mcp, llheading, lvlastmcpone, lvlastmcptwo, lvlastmcpthree, llnewsfeedlegent;
    private LineChart newsfeed_graph;
    private TextView tvcolorone, tvdateone, tvcolortwo, tvdatetwo, tvcolorthree, tvdatethree;
    private ImageView tvexpand;
    private GPSTracker gps;
    private double latitude, longitude;
    private ArrayList<String> categories = new ArrayList<String>();
    private ArrayList<String> state_id_Array = new ArrayList<String>();
    private ArrayList<String> state_name_Array = new ArrayList<String>();
    private ArrayAdapter<String> adapter_state;
    private Spinner dropdwn, spbidtype;
    private String state_id_array_fromdb, state_name_name_fromdb, state_id_fromdb;
    private String state_id_send, bidtype_id_send;
    private boolean isTrue = true;
    private String formattedDatee, domain,newsletterBaseUrl;
    private View rootView;
    private RecyclerView mRecyclerView;

    private ImageView leftNav, rightNav;
    private HorizontalScrollView horizontalview;

    private String[] desc = {
            "Current Avaiable URS",
            "Transmission Corridor",
            "Realtime Scheduling",
            "Landed Cost Calculator",
            "Notification Setting"
    };


    private Integer[] imageid = {
            R.drawable.ic_current_urs_newsfeed,
            R.drawable.ic_transmission_icon__newsfeed,
            R.drawable.ic_realtime_icon_newsfeed,
            R.drawable.ic_landedcost_newsfeed,
            R.drawable.ic_notification_icon_newsfeed
    };

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_newsfeed, container, false);

		/*prgDialog = new ProgressDialog(getActivity());
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        db = getActivity().openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientid(clientid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationnewsfeed(newMessage TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS gpsloaction(latitude VARCHAR, longitude VARCHAR, state_id VARCHAR, state_serial VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS availableurs(region VARCHAR, available VARCHAR, select_yes VARCHAR, revision VARCHAR, region_id VARCHAR);");

        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        newsletterBaseUrl = SharedPrefHandler.getDeviceString(getActivity(), "newsletterBaseUrl");

        dropdwn = (Spinner) rootView.findViewById(R.id.dropdwn);
        dropdwn.setOnItemSelectedListener(this);

        spbidtype = (Spinner) rootView.findViewById(R.id.spbidtype);
        categories.add("IEX");
        categories.add("PXIL");
        // Creating adapter for spinner
        ArrayAdapter<String> madapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_type, categories);
        // Drop down layout style - list view with radio button
        madapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spbidtype.setAdapter(madapter);
        spbidtype.setOnItemSelectedListener(this);

        Cursor cfrom = db.rawQuery("SELECT * FROM gpsloaction", null);
        if (cfrom.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        while (cfrom.moveToNext()) {
            state_id_array_fromdb = cfrom.getString(0);
            state_name_name_fromdb = cfrom.getString(1);
            state_id_fromdb = cfrom.getString(2);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
        }

        if (state_id_fromdb == null) {
            gps = new GPSTracker(getActivity());
            // check if GPS enabled
            if (gps.canGetLocation()) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
                // \n is for new line
                new HttpAsyncTaskgetstatenamefromgeo().execute(domain + "/mobile/pxs_app/service/getstatenamefromgeo.php");
                //  Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        } else {
            try {
                state_id_Array.clear();
                state_name_Array.clear();
                JSONObject mainObject = new JSONObject(state_id_array_fromdb);
                JSONArray statesarray = mainObject.getJSONArray("states");
                for (int j = 0; j < statesarray.length(); j++) {
                    JSONObject data = statesarray.getJSONObject(j);
                    if (data.has("id")) {
                        String id = data.getString("id");
                        state_id_Array.add(id);
                    }
                    if (data.has("state_name")) {
                        String state_name = data.getString("state_name");
                        state_name_Array.add(state_name);
                    }
                }
                String selected = mainObject.getString("selected");
                adapter_state = new ArrayAdapter<String>(
                        getActivity(),
                        R.layout.spinner_item_state,
                        state_name_Array);
                adapter_state
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                dropdwn.setAdapter(adapter_state);

                int k = state_id_Array.indexOf(state_id_fromdb);
                //String state_ID = state_name_Array.get(k);
                //fbgftgbgtfb

                String strI = Integer.toString(k);
                String state_ID;
                if (strI.equalsIgnoreCase("-1")) {
                    state_ID = "";
                } else {
                    state_ID = state_name_Array.get(k);
                }
                dropdwn.setSelection(k);
                dropdwn.setSelection(adapter_state.getPosition(state_ID));

            } catch (JSONException e) {
                try {
                    JSONObject mainObject = new JSONObject(state_id_array_fromdb);
                    String errormass = mainObject.getString("message");
                    Toast.makeText(getActivity(), errormass, Toast.LENGTH_LONG).show();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                if (state_id_array_fromdb != null) {
                    //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    Document html = Jsoup.parse(state_id_array_fromdb);
                    String title = html.title();
                    Toast.makeText(getActivity(), "" + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        tvexpand = (ImageView) rootView.findViewById(R.id.tvexpand);
        layout_strip_mcp = (LinearLayout) rootView.findViewById(R.id.layout_strip_mcp);
        layout_mcp = (LinearLayout) rootView.findViewById(R.id.layout_mcp);



		/*final MarqueeViewDashboardAdapter mAdapter;
		final MyLayoutManager mLayoutManager;
		RelativeLayout parent=(RelativeLayout)rootView.findViewById(R.id.myrecyclerview);
		mRecyclerView=new RecyclerView(getActivity());
		mRecyclerView.setHasFixedSize(true);
		mAdapter = new MarqueeViewDashboardAdapter(desc, imageid);
		mRecyclerView.setAdapter(mAdapter);
		mRecyclerView.setOverScrollMode(RecyclerView.SCROLL_STATE_SETTLING);
		mLayoutManager = new MyLayoutManager(getActivity());
		mRecyclerView.setLayoutManager(mLayoutManager);*/
        //mRecyclerView.smoothScrollToPosition(Integer.MIN_VALUE);

	    /*mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction()== MotionEvent.ACTION_DOWN){
				}else if(event.getAction()==MotionEvent.ACTION_UP){
					if(!mLayoutManager.smoothScroller.isRunning()) {
						mLayoutManager.againStart();
					}
				}
				return true;
			}
		});*/
		/*mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRecyclerView, new RecyclerTouchListener.ClickListener() {
			@Override
			public void onClick(View view, int position) {
				//Movie movie = movieList.get(position);
				Toast.makeText(getActivity(), desc[position] + " is selected!", Toast.LENGTH_SHORT).show();
				if(desc[position].equalsIgnoreCase("Current Avaiable URS")) {

				}
				else if(desc[position].equalsIgnoreCase("Transmission Corridor")) {

				}else if(desc[position].equalsIgnoreCase("Realtime Scheduling")) {

				}else if(desc[position].equalsIgnoreCase("Landed Cost Calculator")) {

				}else if(desc[position].equalsIgnoreCase("Notification Setting")) {

				}
			}

			@Override
			public void onLongClick(View view, int position) {

			}
		}));*/
        //parent.addView(mRecyclerView);


        horizontalview = (HorizontalScrollView) rootView.findViewById(R.id.horizontalview);
        //horizontalview.arrowScroll(HorizontalScrollView.)
        leftNav = (ImageView) rootView.findViewById(R.id.backward_arrow_dashboard);
        rightNav = (ImageView) rootView.findViewById(R.id.forward_arrow_dashboard);


        leftNav.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                horizontalview.smoothScrollTo((int) horizontalview.getScrollX() - 80, (int) horizontalview.getScrollY());
            }
        });
        rightNav.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                horizontalview.smoothScrollTo((int) horizontalview.getScrollX() + 80, (int) horizontalview.getScrollY());
            }
        });

        LinearLayout llCurrentAvaiableURS = (LinearLayout) rootView.findViewById(R.id.llCurrentAvaiableURS);
        LinearLayout llTransmissionCorridor = (LinearLayout) rootView.findViewById(R.id.llTransmissionCorridor);
        LinearLayout llRealtimeScheduling = (LinearLayout) rootView.findViewById(R.id.llRealtimeScheduling);
        LinearLayout llLandedCostCalculator = (LinearLayout) rootView.findViewById(R.id.llLandedCostCalculator);
        LinearLayout llmonthlyatc = (LinearLayout) rootView.findViewById(R.id.llmonthlyatc);

        LinearLayout llNewsLetter = (LinearLayout) rootView.findViewById(R.id.llNewsLetter);
        LinearLayout llNotificationSetting = (LinearLayout) rootView.findViewById(R.id.llNotificationSetting);


        llCurrentAvaiableURS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CurrentAvaiableURSFragment currentAvaiableURSFragment = new CurrentAvaiableURSFragment();
                if (currentAvaiableURSFragment != null) {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, currentAvaiableURSFragment)
                            .commit();

                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });
        llTransmissionCorridor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransmissionCorridorFragment transmissionCorridorFragment = new TransmissionCorridorFragment();
                if (transmissionCorridorFragment != null) {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, transmissionCorridorFragment)
                            .commit();

                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        llRealtimeScheduling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RealtimeSchedulingFragment realtimeSchedulingFragment = new RealtimeSchedulingFragment();
                if (realtimeSchedulingFragment != null) {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, realtimeSchedulingFragment)
                            .commit();
                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        llLandedCostCalculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LandedCostCalculatorFragment landedCostCalculatorFragment = new LandedCostCalculatorFragment();
                if (landedCostCalculatorFragment != null) {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, landedCostCalculatorFragment)
                            .commit();
                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        llmonthlyatc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MonthlyATCFragment monthlyATCFragment = new MonthlyATCFragment();
                if (monthlyATCFragment != null) {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, monthlyATCFragment)
                            .commit();
                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        llNewsLetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewsLetterFragment newsLetterFragment = new NewsLetterFragment();
                if (newsLetterFragment != null) {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, newsLetterFragment)
                            .commit();

                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        llNotificationSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationSettingFragment notificationSettingFragment = new NotificationSettingFragment();
                if (notificationSettingFragment != null) {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, notificationSettingFragment)
                            .commit();

                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        hsview = (HorizontalScrollView) rootView.findViewById(R.id.hsview);
        llhsview = (LinearLayout) rootView.findViewById(R.id.llhsview);
        tvRENEWABLE = (TextView) rootView.findViewById(R.id.tvRENEWABLE);
        tvstripRENEWABLE = (TextView) rootView.findViewById(R.id.tvstripRENEWABLE);


        newsfeed_graph = (LineChart) rootView.findViewById(R.id.newsfeed_graph);

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);

        llheading = (LinearLayout) rootView.findViewById(R.id.llheading);

        graphlist = (NonScrollListView) rootView.findViewById(R.id.graphlist);


        llnewsfeedlegent = (LinearLayout) rootView.findViewById(R.id.llnewsfeedlegent);
        lvlastmcpone = (LinearLayout) rootView.findViewById(R.id.lvlastmcpone);
        tvcolorone = (TextView) rootView.findViewById(R.id.tvcolorone);
        tvdateone = (TextView) rootView.findViewById(R.id.tvdateone);

        lvlastmcptwo = (LinearLayout) rootView.findViewById(R.id.lvlastmcptwo);
        tvcolortwo = (TextView) rootView.findViewById(R.id.tvcolortwo);
        tvdatetwo = (TextView) rootView.findViewById(R.id.tvdatetwo);


        lvlastmcpthree = (LinearLayout) rootView.findViewById(R.id.lvlastmcpthree);
        tvcolorthree = (TextView) rootView.findViewById(R.id.tvcolorthree);
        tvdatethree = (TextView) rootView.findViewById(R.id.tvdatethree);

        ll_expand_graph = (LinearLayout) rootView.findViewById(R.id.ll_expand_graph);

        tvPOWER = (TextView) rootView.findViewById(R.id.tvPOWER);
        tvstripPOWER = (TextView) rootView.findViewById(R.id.tvstripPOWER);

        tvenergy = (TextView) rootView.findViewById(R.id.tvenergy);
        tvstripenergy = (TextView) rootView.findViewById(R.id.tvstripenergy);
        energylist = (NonScrollListView) rootView.findViewById(R.id.energylist);

        tvEVENT = (TextView) rootView.findViewById(R.id.tvEVENT);
        tvstripEVENT = (TextView) rootView.findViewById(R.id.tvstripEVENT);

        tvCOAL = (TextView) rootView.findViewById(R.id.tvCOAL);
        tvstripCOAL = (TextView) rootView.findViewById(R.id.tvstripCOAL);

        tvreadmore = (TextView) rootView.findViewById(R.id.tvreadmore);
        tvstripreadmore = (TextView) rootView.findViewById(R.id.tvstripreadmore);

        tvnewsseven = (TextView) rootView.findViewById(R.id.tvnewsseven);
        tvstripnewsseven = (TextView) rootView.findViewById(R.id.tvstripnewsseven);

        tvnewseight = (TextView) rootView.findViewById(R.id.tvnewseight);
        tvstripnewseight = (TextView) rootView.findViewById(R.id.tvstripnewseight);

        tvnewsnine = (TextView) rootView.findViewById(R.id.tvnewsnine);
        tvstripnewsnine = (TextView) rootView.findViewById(R.id.tvstripnewsnine);

        tvnewsten = (TextView) rootView.findViewById(R.id.tvnewsten);
        tvstripnewsten = (TextView) rootView.findViewById(R.id.tvstripnewsten);

        tvnewseleven = (TextView) rootView.findViewById(R.id.tvnewseleven);
        tvstripnewseleven = (TextView) rootView.findViewById(R.id.tvstripnewseleven);

        tvnewstwavle = (TextView) rootView.findViewById(R.id.tvnewstwavle);
        tvstripnewstwavle = (TextView) rootView.findViewById(R.id.tvstripnewstwavle);

        tvnewsthirdteen = (TextView) rootView.findViewById(R.id.tvnewsthirdteen);
        tvstripnewsthirdteen = (TextView) rootView.findViewById(R.id.tvstripnewsthirdteen);

        tvnewsfourteen = (TextView) rootView.findViewById(R.id.tvnewsfourteen);
        tvstripnewsfourteen = (TextView) rootView.findViewById(R.id.tvstripnewsfourteen);

        tvnewsfifteen = (TextView) rootView.findViewById(R.id.tvnewsfifteen);
        tvstripnewsfifteen = (TextView) rootView.findViewById(R.id.tvstripnewsfifteen);

        tvnewssixteen = (TextView) rootView.findViewById(R.id.tvnewssixteen);
        tvstripnewssixteen = (TextView) rootView.findViewById(R.id.tvstripnewssixteen);

        Cursor c = db.rawQuery("SELECT * FROM pushnotification", null);
        if (c.getCount() == 0) {
            //category =  key_element.get(0);
            category = "";
            //showMessage("Error", "No Notification found");
            //Toast.makeText(NotificationActivity.this, "No Notification found", Toast.LENGTH_LONG).show();
        } else {
            icontypearray = new String[c.getCount()];
            array = new String[c.getCount()];
            arraydispalydate = new String[c.getCount()];
            arraydispalytime = new String[c.getCount()];
            elementiconarray = new Integer[c.getCount()];
            int i = 0;
            while (c.moveToNext()) {
                String dbacessfrom = c.getString(0);
                String[] parts = dbacessfrom.split("/");
                icontypefrom = parts[0];
                dbacess = parts[1];
                displaydate = c.getString(1);
                displaytime = c.getString(2);
                //goMessage.setText("All Notification:" + dbacess);
                array[i] = dbacess;
                arraydispalydate[i] = displaydate;
                arraydispalytime[i] = displaytime;
                icontypearray[i] = icontypefrom;
                i++;
            }
            List<String> icontypearraylist = Arrays.asList(icontypearray);
            Collections.reverse(icontypearraylist);
            icontypearray = (String[]) icontypearraylist.toArray();
            String lastelement = icontypearray[0];

            List<String> list = Arrays.asList(array);
            Collections.reverse(list);
            array = (String[]) list.toArray();
            String lastelementdetail = array[0];

            List<String> displaydatelist = Arrays.asList(arraydispalydate);
            Collections.reverse(displaydatelist);
            arraydispalydate = (String[]) displaydatelist.toArray();
            String lastelementdtate = arraydispalydate[0];

            List<String> displaytimelist = Arrays.asList(arraydispalytime);
            Collections.reverse(displaytimelist);
            arraydispalytime = (String[]) displaytimelist.toArray();
            // category = key_element.get(0);
            category = "";
            try {
                JSONObject jsonObj = new JSONObject(lastelementdetail);
                if (jsonObj.has("message")) {
                    message = jsonObj.getString("message");
                }
                if (jsonObj.has("category")) {
                    category = jsonObj.getString("category");
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        //	new HttpAsyncTasklastyearmarketprice().execute("http://www.mittalpower.com/mobile/pxs_app/service/lastyearmarketprice.php");
        Calendar ccc = Calendar.getInstance();
        System.out.println("Current time => " + ccc.getTime());
        SimpleDateFormat dff = new SimpleDateFormat("dd-MM-yyyy");
        formattedDatee = dff.format(ccc.getTime());

        cd = new ConnectionDetector(getActivity());
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getActivity(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            Cursor newfeedsavec = db.rawQuery("select * from newfeedsave", null);
            if (newfeedsavec.getCount() == 0) {
                //new HttpAsyncTaskmla().execute("http://" + ipaddressfromdb + Constant.ELECTIONGetMlaInfo);
                new HttpAsyncTaskgetnewsfeedinfo().execute(domain + "/mobile/pxs_app/service/getnewsfeedinfo.php");
            } else {
                String datefromdb = "";
                newfeedsavec.moveToFirst();
                do {
                    saveResponse = newfeedsavec.getString(newfeedsavec.getColumnIndex("response"));
                    datefromdb = newfeedsavec.getString(newfeedsavec.getColumnIndex("notificationdate"));

                    // String notificationdate =   newfeedsavec.getString(newfeedsavec.getColumnIndex("notificationdate"));
                    // Toast.makeText(getActivity(), saveResponse, Toast.LENGTH_LONG).show();
                } while (newfeedsavec.moveToNext());

                if (datefromdb.equalsIgnoreCase(formattedDatee)) {
                    fatchdataofnewfeed();
                } else {
                    new HttpAsyncTaskgetnewsfeedinfo().execute(domain + "/mobile/pxs_app/service/getnewsfeedinfo.php");
                }
            }
        }

        new CountDownTimer(300000, 1000) {
            public void onTick(long millisUntilFinished) {
                // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                if (isVisible()) {
                    Log.e("newsfeed", "in side counter");
                    new HttpAsyncTaskgetnewsfeedinfoAgain().execute(domain + "/mobile/pxs_app/service/getnewsfeedinfo.php");
                }
                Log.e("newsfeed", "out side counter");
            }
        }.start();


        Intent in = getActivity().getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");
        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            //String companyname = loginconfig.substring(0, 6);
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        layout_strip_mcp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_mcp.setVisibility(View.VISIBLE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);
                llnewsfeedlegent.setVisibility(View.INVISIBLE);
                tvexpand.setVisibility(View.INVISIBLE);
            }
        });


        llhsview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);
                llnewsfeedlegent.setVisibility(View.VISIBLE);
                tvexpand.setVisibility(View.VISIBLE);
            }
        });

        ll_expand_graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), LastYearMarketPriceActivity.class);
                in.putStringArrayListExtra("currentyear", lastyearmarketprice_Arrayone);
                in.putStringArrayListExtra("lastyear", lastyearmarketprice_Arraytwo);
                in.putStringArrayListExtra("secondlastyear", lastyearmarketprice_Arraythree);
                in.putStringArrayListExtra("previousday", lastyearmarketprice_Arrayfour);
                in.putExtra("LOGINCONFIG", loginconfig);
                in.putStringArrayListExtra("date_Array", date_Array);
                startActivity(in);
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
            }
        });

		/*newsfeed_graph.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in = new Intent(getActivity(), LastYearMarketPriceActivity.class);
				in.putStringArrayListExtra("currentyear", lastyearmarketprice_Arrayone);
				in.putStringArrayListExtra("lastyear", lastyearmarketprice_Arraytwo);
				in.putStringArrayListExtra("secondlastyear", lastyearmarketprice_Arraythree);
				in.putStringArrayListExtra("previousday", lastyearmarketprice_Arrayfour);
				in.putExtra("LOGINCONFIG",loginconfig);
				in.putStringArrayListExtra("date_Array", date_Array);
				startActivity(in);
				getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
			}
		});*/


        lvlastmcpone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Toast.makeText(MarketPriceActivity.this, "Ashish", Toast.LENGTH_LONG).show();
                if (minisTrue == true) {
                    minisTrue = false;
                    String f = "false";
                    tvdateone.setTextColor(Color.GRAY);
                    tvcolorone.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(MarketPriceActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    minisTrue = true;
                    String t = "true";
                    tvdateone.setTextColor(Color.MAGENTA);
                    tvcolorone.setBackgroundColor(Color.MAGENTA);
                    //Toast.makeText(MarketPriceActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartNav();
            }
        });

        lvlastmcptwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (maxisTrue == true) {
                    maxisTrue = false;
                    String f = "false";
                    tvdatetwo.setTextColor(Color.GRAY);
                    tvcolortwo.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(MarketPriceActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    maxisTrue = true;
                    String t = "true";
                    tvdatetwo.setTextColor(Color.RED);
                    tvcolortwo.setBackgroundColor(Color.RED);
                    //Toast.makeText(MarketPriceActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartNav();
            }
        });

        lvlastmcpthree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (avgisTrue == true) {
                    avgisTrue = false;
                    String f = "false";
                    tvdatethree.setTextColor(Color.GRAY);
                    tvcolorthree.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(MarketPriceActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    avgisTrue = true;
                    String t = "true";
                    tvdatethree.setTextColor(Color.BLUE);
                    tvcolorthree.setBackgroundColor(Color.BLUE);
                    //Toast.makeText(MarketPriceActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartNav();
            }
        });

        openLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NewFeedMainActivity) getActivity()).open();
            }
        });


        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        int height = size.y;

        tvRENEWABLE.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);

                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.VISIBLE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.BOLD);
                tvRENEWABLE.setTextSize(16);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvRENEWABLE.getLeft() - (width / 2)) + (tvRENEWABLE.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvRENEWABLE);

                String keyelementfind = (String) tvRENEWABLE.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }

            }
        });

        tvPOWER.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.VISIBLE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.BOLD);
                tvPOWER.setTextSize(16);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);


                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvPOWER.getLeft() - (width / 2)) + (tvPOWER.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvPOWER);
                //String keyelemrnfind = tvPOWER.getText();
                String keyelementfind = (String) tvPOWER.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }


            }
        });

        tvenergy.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.VISIBLE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.BOLD);
                tvenergy.setTextSize(16);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvenergy.getLeft() - (width / 2)) + (tvenergy.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvenergy);

                String keyelementfind = (String) tvenergy.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }

            }
        });

        tvEVENT.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.VISIBLE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.BOLD);
                tvEVENT.setTextSize(16);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvEVENT.getLeft() - (width / 2)) + (tvEVENT.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);

                String keyelementfind = (String) tvEVENT.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });


        tvCOAL.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);


                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.VISIBLE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);


                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.BOLD);
                tvCOAL.setTextSize(16);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvCOAL.getLeft() - (width / 2)) + (tvCOAL.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvPOWER);

                String keyelementfind = (String) tvCOAL.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvreadmore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.VISIBLE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(16);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);


                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvreadmore.getRight() - (width / 2)) + (tvreadmore.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvreadmore.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });


		/*tvnewsseven, tvnewseight, tvnewsnine, tvnewsten, tvnewseleven, tvnewstwavle,
		 tvnewsthirdteen, tvnewsfourteen, tvnewsfifteen, tvnewssixteen*/

        tvnewsseven.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);


                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.VISIBLE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(16);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);


                energylist.setVisibility(View.VISIBLE);

                ///int scrollX = (tvreadmore.getRight() - (width / 2)) + (tvreadmore.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewsseven.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewseight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.VISIBLE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(16);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);


                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvreadmore.getRight() - (width / 2)) + (tvreadmore.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewseight.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });


        tvnewsnine.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.VISIBLE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(16);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                ///int scrollX = (tvreadmore.getRight() - (width / 2)) + (tvreadmore.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvreadmore);
                String keyelementfind = (String) tvnewsnine.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }

            }

        });


        tvnewsten.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.VISIBLE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(16);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);


                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvreadmore.getRight() - (width / 2)) + (tvreadmore.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewsten.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewseleven.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.VISIBLE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(16);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);


                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvreadmore.getRight() - (width / 2)) + (tvreadmore.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewseleven.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewstwavle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.VISIBLE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(16);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);


                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvreadmore.getRight() - (width / 2)) + (tvreadmore.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewstwavle.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });


        tvnewsthirdteen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(16);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);


                energylist.setVisibility(View.VISIBLE);

                //	int scrollX = (tvreadmore.getRight() - (width / 2)) + (tvreadmore.getWidth() / 2);
                //	hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewsthirdteen.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewsfourteen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.VISIBLE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(16);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);


                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvreadmore.getRight() - (width / 2)) + (tvreadmore.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewsfourteen.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewsfifteen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);


                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.VISIBLE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(16);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);


                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvreadmore.getRight() - (width / 2)) + (tvreadmore.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewsfifteen.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewssixteen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tvexpand.setVisibility(View.VISIBLE);
                layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                layout_strip_mcp.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.VISIBLE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(16);


                energylist.setVisibility(View.VISIBLE);

                //int scrollX = (tvreadmore.getRight() - (width / 2)) + (tvreadmore.getWidth() / 2);
                //hsview.smoothScrollTo(scrollX, 0);
                //llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewssixteen.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject.getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);
                        }
                    });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        lltoplogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                //db.execSQL("DROP TABLE IF EXISTS baby");
                //db.execSQL("DROP TABLE IF EXISTS logininfo");
                //db.execSQL("DROP TABLE IF EXISTS companyname");
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                getActivity().finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                //db.execSQL("DROP TABLE IF EXISTS baby");
                //db.execSQL("DROP TABLE IF EXISTS logininfo");
                //db.execSQL("DROP TABLE IF EXISTS companyname");
                Intent i = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                getActivity().finish();
            }
        });


        return rootView;
    }

    public void fatchdataofnewfeed() {
        Cursor newfeedsavec = db.rawQuery("select * from newfeedsave", null);
        newfeedsavec.moveToFirst();
        do {
            saveResponse = newfeedsavec.getString(newfeedsavec.getColumnIndex("response"));
            // String notificationdate =   newfeedsavec.getString(newfeedsavec.getColumnIndex("notificationdate"));
            // Toast.makeText(getActivity(), saveResponse, Toast.LENGTH_LONG).show();
        } while (newfeedsavec.moveToNext());
        try {
            mainObject = new JSONObject(saveResponse);
            key_element.clear();
            Iterator<String> iter = mainObject.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                key_element.add(key);
            }
            try {
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                // Object value = mainObject.get(key);
                //if(category.length() != 0){
				/*
					if(key_element.contains(category)){
					key_element.remove(category);
					key_element.add(category);
					Collections.reverse(key_element);
			    	JSONArray keyResult = mainObject.getJSONArray(category);
					for (int i = 0; i < keyResult.length(); i++) {
						JSONObject keyResulttDetails = keyResult.getJSONObject(i);
						if(keyResulttDetails.has("type")){
							String type = keyResulttDetails.getString("type");
							if(type.contains(category)){
								if(keyResulttDetails.has("id")){
									String id = keyResulttDetails.getString("id");
									id_key_element.add(id);
									elementiconarray_element.add(R.drawable.news);
								}
								if(keyResulttDetails.has("title")){
									String title = keyResulttDetails.getString("title");
									title_key_element.add(Html.fromHtml(title).toString());
								}
								if(keyResulttDetails.has("link")){
									String link = keyResulttDetails.getString("link");
									link_key_element.add(link);
								}
								if(keyResulttDetails.has("comment")){
									String comment = keyResulttDetails.getString("comment");
									comment_key_element.add(comment);
								}
								if(keyResulttDetails.has("pubdate")){
									String pubdate = keyResulttDetails.getString("pubdate");
									pubdate_key_element.add(pubdate);
								}
								if(keyResulttDetails.has("time")){
									String time = keyResulttDetails.getString("time");
									time_key_element.add(time);
								}
								if(keyResulttDetails.has("source")){
									String source = keyResulttDetails.getString("source");
									source_key_element.add(source);
								}
							}
						}
					}
					EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
				    energylist.setAdapter(adapter);
					}
				 */
                //}
                //else
                //{
                category = key_element.get(0);
                JSONArray keyResult = mainObject.getJSONArray(key_element.get(0));
                for (int i = 0; i < keyResult.length(); i++) {
                    JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                    if (keyResulttDetails.has("type")) {
                        String type = keyResulttDetails.getString("type");
                        if (type.contains(key_element.get(0))) {
                            if (keyResulttDetails.has("id")) {
                                String id = keyResulttDetails.getString("id");
                                id_key_element.add(id);
                                elementiconarray_element.add(R.drawable.news);
                            }
                            if (keyResulttDetails.has("title")) {
                                String title = keyResulttDetails.getString("title");
                                title_key_element.add(Html.fromHtml(title).toString());
                            }
                            if (keyResulttDetails.has("link")) {
                                String link = keyResulttDetails.getString("link");
                                link_key_element.add(link);
                            }
                            if (keyResulttDetails.has("comment")) {
                                String comment = keyResulttDetails.getString("comment");
                                comment_key_element.add(comment);
                            }
                            if (keyResulttDetails.has("pubdate")) {
                                String pubdate = keyResulttDetails.getString("pubdate");
                                pubdate_key_element.add(pubdate);
                            }
                            if (keyResulttDetails.has("time")) {
                                String time = keyResulttDetails.getString("time");
                                time_key_element.add(time);
                            }
                            if (keyResulttDetails.has("source")) {
                                String source = keyResulttDetails.getString("source");
                                source_key_element.add(source);
                            }
                        }
                    }
                }
                EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                energylist.setAdapter(adapter);
                //}
                energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            final int position, long id) {
                        //Toast.makeText(getActivity(),"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getActivity(),"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                        //String indexid = String.valueOf(position);
                        Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                        in.putExtra("link_key_element", link_key_element.get(position));
                        in.putExtra("LOGINCONFIG", loginconfig);
                        startActivity(in);

                    }
                });


            } catch (JSONException e) {
                // Something went wrong!
            }

            //tvRENEWABLE, tvPOWER, tvenergy, tvEVENT, tvCOAL, tvstripCOAL;
            // tvnewsseven, tvnewseight, tvnewsnine, tvnewsten, tvnewseleven, tvnewstwavle,
            //	 tvnewsthirdteen, tvnewsfourteen, tvnewsfifteen, tvnewssixteen;


            //TextView tvstripRENEWABLE, tvstripPOWER, tvstripenergy, tvstripEVENT, tvreadmore, tvstripreadmore
            //  tvstripnewsseven, tvstripnewseight, tvstripnewsnine, tvstripnewsten, tvstripnewseleven,
            //	 tvstripnewstwavle, tvstripnewsthirdteen, tvstripnewsfourteen, tvstripnewsfifteen, tvstripnewssixteen


            if (key_element.size() == 0 && key_element.contains(category)) {
                //tvRENEWABLE.setText(key_element.get(0));
                //tvRENEWABLE.setVisibility(View.VISIBLE);
                //tvstripRENEWABLE.setVisibility(View.VISIBLE);
            } else if (key_element.size() == 1 && key_element.contains(category)) {
                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvstripRENEWABLE.setVisibility(View.VISIBLE);
                if (key_element.get(0).equalsIgnoreCase("News")) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }
            } else if (key_element.size() == 2 && key_element.contains(category)) {
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvPOWER.setVisibility(View.VISIBLE);

                tvRENEWABLE.setText(key_element.get(0));
                tvPOWER.setText(key_element.get(1));

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 3 && key_element.contains(category)) {
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvPOWER.setVisibility(View.VISIBLE);
                tvenergy.setVisibility(View.VISIBLE);

                tvRENEWABLE.setText(key_element.get(0));
                tvPOWER.setText(key_element.get(1));
                tvenergy.setText(key_element.get(2));

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 4 && key_element.contains(category)) {
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvPOWER.setVisibility(View.VISIBLE);
                tvenergy.setVisibility(View.VISIBLE);
                tvEVENT.setVisibility(View.VISIBLE);

                tvRENEWABLE.setText(key_element.get(0));
                tvPOWER.setText(key_element.get(1));
                tvenergy.setText(key_element.get(2));
                tvEVENT.setText(key_element.get(3));

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }
            } else if (key_element.size() == 5 && key_element.contains(category)) {
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvPOWER.setVisibility(View.VISIBLE);
                tvenergy.setVisibility(View.VISIBLE);
                tvEVENT.setVisibility(View.VISIBLE);
                tvCOAL.setVisibility(View.VISIBLE);
                tvRENEWABLE.setText(key_element.get(0));
                tvPOWER.setText(key_element.get(1));
                tvenergy.setText(key_element.get(2));
                tvEVENT.setText(key_element.get(3));
                tvCOAL.setText(key_element.get(4));

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }
            } else if (key_element.size() == 6 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);
                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);
                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);
                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);
                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 7 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 8 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }


            } else if (key_element.size() == 9 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }


            } else if (key_element.size() == 10 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);


                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 11 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                tvnewseleven.setText(key_element.get(10));
                tvnewseleven.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

                if (key_element.get(10).equalsIgnoreCase(category)) {
                    tvstripnewseleven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseleven.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 12 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                tvnewseleven.setText(key_element.get(10));
                tvnewseleven.setVisibility(View.VISIBLE);

                tvnewstwavle.setText(key_element.get(11));
                tvnewstwavle.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

                if (key_element.get(10).equalsIgnoreCase(category)) {
                    tvstripnewseleven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseleven.setVisibility(View.GONE);
                }

                if (key_element.get(11).equalsIgnoreCase(category)) {
                    tvstripnewstwavle.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewstwavle.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 13 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvstripRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);
                tvstripCOAL.setVisibility(View.GONE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                tvnewseleven.setText(key_element.get(10));
                tvnewseleven.setVisibility(View.VISIBLE);

                tvnewstwavle.setText(key_element.get(11));
                tvnewstwavle.setVisibility(View.VISIBLE);

                tvnewsthirdteen.setText(key_element.get(12));
                tvnewsthirdteen.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

                if (key_element.get(10).equalsIgnoreCase(category)) {
                    tvstripnewseleven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseleven.setVisibility(View.GONE);
                }

                if (key_element.get(11).equalsIgnoreCase(category)) {
                    tvstripnewstwavle.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewstwavle.setVisibility(View.GONE);
                }

                if (key_element.get(12).equalsIgnoreCase(category)) {
                    tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsthirdteen.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 14 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                tvnewseleven.setText(key_element.get(10));
                tvnewseleven.setVisibility(View.VISIBLE);

                tvnewstwavle.setText(key_element.get(11));
                tvnewstwavle.setVisibility(View.VISIBLE);

                tvnewsthirdteen.setText(key_element.get(12));
                tvnewsthirdteen.setVisibility(View.VISIBLE);

                tvnewsfourteen.setText(key_element.get(13));
                tvnewsfourteen.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

                if (key_element.get(10).equalsIgnoreCase(category)) {
                    tvstripnewseleven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseleven.setVisibility(View.GONE);
                }

                if (key_element.get(11).equalsIgnoreCase(category)) {
                    tvstripnewstwavle.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewstwavle.setVisibility(View.GONE);
                }

                if (key_element.get(12).equalsIgnoreCase(category)) {
                    tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsthirdteen.setVisibility(View.GONE);
                }

                if (key_element.get(13).equalsIgnoreCase(category)) {
                    tvstripnewsfourteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsfourteen.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 15 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                tvnewseleven.setText(key_element.get(10));
                tvnewseleven.setVisibility(View.VISIBLE);

                tvnewstwavle.setText(key_element.get(11));
                tvnewstwavle.setVisibility(View.VISIBLE);

                tvnewsthirdteen.setText(key_element.get(12));
                tvnewsthirdteen.setVisibility(View.VISIBLE);

                tvnewsfourteen.setText(key_element.get(13));
                tvnewsfourteen.setVisibility(View.VISIBLE);

                tvnewsfifteen.setText(key_element.get(14));
                tvnewsfifteen.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

                if (key_element.get(10).equalsIgnoreCase(category)) {
                    tvstripnewseleven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseleven.setVisibility(View.GONE);
                }

                if (key_element.get(11).equalsIgnoreCase(category)) {
                    tvstripnewstwavle.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewstwavle.setVisibility(View.GONE);
                }

                if (key_element.get(12).equalsIgnoreCase(category)) {
                    tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsthirdteen.setVisibility(View.GONE);
                }

                if (key_element.get(13).equalsIgnoreCase(category)) {
                    tvstripnewsfourteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsfourteen.setVisibility(View.GONE);
                }

                if (key_element.get(14).equalsIgnoreCase(category)) {
                    tvstripnewsfifteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsfifteen.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 16 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                tvnewseleven.setText(key_element.get(10));
                tvnewseleven.setVisibility(View.VISIBLE);

                tvnewstwavle.setText(key_element.get(11));
                tvnewstwavle.setVisibility(View.VISIBLE);

                tvnewsthirdteen.setText(key_element.get(12));
                tvnewsthirdteen.setVisibility(View.VISIBLE);

                tvnewsfourteen.setText(key_element.get(13));
                tvnewsfourteen.setVisibility(View.VISIBLE);

                tvnewsfifteen.setText(key_element.get(14));
                tvnewsfifteen.setVisibility(View.VISIBLE);

                tvnewssixteen.setText(key_element.get(15));
                tvnewssixteen.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

                if (key_element.get(10).equalsIgnoreCase(category)) {
                    tvstripnewseleven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseleven.setVisibility(View.GONE);
                }

                if (key_element.get(11).equalsIgnoreCase(category)) {
                    tvstripnewstwavle.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewstwavle.setVisibility(View.GONE);
                }

                if (key_element.get(12).equalsIgnoreCase(category)) {
                    tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsthirdteen.setVisibility(View.GONE);
                }

                if (key_element.get(13).equalsIgnoreCase(category)) {
                    tvstripnewsfourteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsfourteen.setVisibility(View.GONE);
                }

                if (key_element.get(14).equalsIgnoreCase(category)) {
                    tvstripnewsfifteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsfifteen.setVisibility(View.GONE);
                }

                if (key_element.get(15).equalsIgnoreCase(category)) {
                    tvstripnewssixteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewssixteen.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 17) {

            } else if (key_element.size() == 18) {

            } else if (key_element.size() == 19) {

            } else {
                Toast.makeText(getActivity(), "Something is wrong!", Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            try {
                JSONObject mainObject = new JSONObject(saveResponse);
                String errormass = mainObject.getString("message");
                Toast.makeText(getActivity(), errormass, Toast.LENGTH_LONG).show();
            } catch (JSONException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            if (saveResponse != null) {
                //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
            } else {
                // TODO Auto-generated catch block
                Document html = Jsoup.parse(saveResponse);
                String title = html.title();
                Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    private void drawChartNav() {
        ArrayList<String> labels = new ArrayList<String>();
        ArrayList<Entry> entriesmin = new ArrayList<>();
        ArrayList<Entry> entriesmax = new ArrayList<>();
        ArrayList<Entry> entriesavg = new ArrayList<>();

        for (int date = 0; date < date_Array.size(); date++) {
            labels.add(date_Array.get(date));
        }


        for (int min = 0; min < min_Array.size(); min++) {
            entriesmin.add(new Entry(Float.parseFloat(min_Array.get(min)), min));
        }
        LineDataSet datasetmin = new LineDataSet(entriesmin, "Min");
        datasetmin.setColor(Color.parseColor("#009999"));
        datasetmin.setCircleColor(Color.parseColor("#009999"));
        datasetmin.setValueTextColor(Color.parseColor("#009999"));
        datasetmin.setDrawCubic(true);
        datasetmin.setValueTextSize(5f);
        datasetmin.setCircleSize(3f);

        for (int max = 0; max < max_Array.size(); max++) {
            entriesmax.add(new Entry(Float.parseFloat(max_Array.get(max)), max));
        }
        LineDataSet datasetmax = new LineDataSet(entriesmax, "Max");
        datasetmax.setColor(Color.parseColor("#01DADA"));
        datasetmax.setCircleColor(Color.parseColor("#01DADA"));
        datasetmax.setValueTextColor(Color.parseColor("#01DADA"));
        datasetmax.setDrawCubic(true);
        datasetmax.setValueTextSize(5f);
        datasetmax.setCircleSize(3f);

        for (int avg = 0; avg < avg_Array.size(); avg++) {
            entriesavg.add(new Entry(Float.parseFloat(avg_Array.get(avg)), avg));
        }
        LineDataSet datasetavg = new LineDataSet(entriesavg, "Avg");
        datasetavg.setColor(Color.parseColor("#000000"));
        datasetavg.setCircleColor(Color.parseColor("#000000"));
        datasetavg.setValueTextColor(Color.parseColor("#000000"));
        datasetavg.setCircleSize(3f);
        datasetavg.setValueTextSize(5f);
        //datasetavg.setValueTextSize(5f);

		/*ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
	        dataSets.add(datasetmin);
	        dataSets.add(datasetmax);
	        dataSets.add(datasetavg);// add the datasets
	        LineData data = new LineData(labels, dataSets);

	        newsfeed_graph.setData(data); // set the data and list of lables into
	        datasetavg.setDrawCubic(true);

	        newsfeed_graph.setData(data);
	        newsfeed_graph.animateY(2000); */
        // newsfeed_graph.setBackgroundColor(R.color.gray);
        // ((ILineDataSet) newsfeed_graph).getDashPathEffect();


        //tvcolorone,tvdateone,tvcolortwo, tvdatetwo,tvcolorthree,tvdatethree

        if (minisTrue == false && avgisTrue == false && maxisTrue == false) {
            //Toast.makeText(MarketPriceActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            tvdateone.setTextColor(Color.parseColor("#009999"));
            tvcolorone.setBackgroundColor(Color.parseColor("#009999"));
            tvdatetwo.setTextColor(Color.parseColor("#01DADA"));
            tvcolortwo.setBackgroundColor(Color.parseColor("#01DADA"));
            tvdatethree.setTextColor(Color.parseColor("#000000"));
            tvcolorthree.setBackgroundColor(Color.parseColor("#000000"));
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetavg);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            newsfeed_graph.setData(data); // set the data and list of lables into
            newsfeed_graph.animateY(3000);
            minisTrue = true;
            avgisTrue = true;
            maxisTrue = true;
        } else if (minisTrue == false && maxisTrue == false && avgisTrue == true) {
            //iexonlymaxdrawChart();
            LineData datamax = new LineData(labels, datasetavg);
            newsfeed_graph.setData(datamax); // set the data and list of lables
            newsfeed_graph.animateY(3000);
        } else if (minisTrue == false && maxisTrue == true && avgisTrue == false) {
            //iexonlyavgdrawChart();
            LineData dataavg = new LineData(labels, datasetmax);
            newsfeed_graph.setData(dataavg); // set the data and list of lables
            newsfeed_graph.animateY(3000);
        } else if (minisTrue == false && maxisTrue == true && avgisTrue == true) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetavg);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            newsfeed_graph.setData(data); // set the data and list of lables into
            newsfeed_graph.animateY(3000);
        } else if (minisTrue == true && maxisTrue == false && avgisTrue == false) {
            //iexonlymindrawChart();
            LineData datamin = new LineData(labels, datasetmin);
            newsfeed_graph.setData(datamin); // set the data and list of lables
            newsfeed_graph.animateY(3000);
        } else if (minisTrue == true && maxisTrue == false && avgisTrue == true) {
            //iexavgdrawChartMixMAx();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetavg);
            LineData data = new LineData(labels, dataSets);
            newsfeed_graph.setData(data); // set the data and list of lables into
            newsfeed_graph.animateY(3000);
        } else if (minisTrue == true && maxisTrue == true && avgisTrue == false) {
            //iexmaxdrawChartMinAvg();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            newsfeed_graph.setData(data); // set the data and list of lables into
            newsfeed_graph.animateY(3000);
        } else if (minisTrue == true && maxisTrue == true && avgisTrue == true) {
            //Toast.makeText(MarketPriceActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            tvdateone.setTextColor(Color.parseColor("#009999"));
            tvcolorone.setBackgroundColor(Color.parseColor("#009999"));
            tvdatetwo.setTextColor(Color.parseColor("#01DADA"));
            tvcolortwo.setBackgroundColor(Color.parseColor("#01DADA"));
            tvdatethree.setTextColor(Color.parseColor("#000000"));
            tvcolorthree.setBackgroundColor(Color.parseColor("#000000"));
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetmin);
            dataSets.add(datasetavg);
            dataSets.add(datasetmax);
            LineData data = new LineData(labels, dataSets);
            newsfeed_graph.setData(data); // set the data and list of lables into
            newsfeed_graph.animateY(3000);

        }

        newsfeed_graph.setDescription("Values in Rupees");
        newsfeed_graph.setDescriptionTextSize(2f);
        XAxis xAxis = newsfeed_graph.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(2f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        newsfeed_graph.setTouchEnabled(false);
        newsfeed_graph.getLegend().setEnabled(false);

        newsfeed_graph.setBackgroundColor(Color.WHITE);

        YAxis axisRight = newsfeed_graph.getAxisRight();
        axisRight.setDrawLabels(false);

        YAxis axisLeft = newsfeed_graph.getAxisLeft();
        axisLeft.setDrawLabels(false);

    }

    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {

        Spinner statetypeid = (Spinner) parent;
        Spinner bidtypeid = (Spinner) parent;

        if (statetypeid.getId() == R.id.dropdwn) {
            state_id_send = state_id_Array.get(position);
            //Toast.makeText(this, "state :" +state_id_send, Toast.LENGTH_SHORT).show();
            bidtype_id_send = spbidtype.getSelectedItem().toString();
            if (bidtype_id_send.equalsIgnoreCase("") || state_id_send.equalsIgnoreCase("")) {

            } else {
                //db.execSQL("DROP TABLE IF EXISTS gpsloaction");
                //db.execSQL("CREATE TABLE IF NOT EXISTS gpsloaction(latitude VARCHAR, longitude VARCHAR, state_id VARCHAR);");
                db.execSQL("UPDATE gpsloaction SET state_id='" + state_id_send + "' WHERE state_serial='1'");
                //db.execSQL("INSERT INTO gpsloaction VALUES('"+state_id_array_fromdb+"', '"+state_id_array_fromdb+"', '"+state_id_send+"')");
                new HttpAsyncTasklastyearmarketprice().execute(domain + "/mobile/pxs_app/service/lastyearmarketprice.php");
            }
        }

        if (bidtypeid.getId() == R.id.spbidtype) {

            if (isTrue == true) {
                isTrue = false;
				/*bidtype_id_send = categories.get(position);
			//Toast.makeText(this, "bidtypeid :" +bidtype_id_send, Toast.LENGTH_SHORT).show();
			state_id_send = "";
			if(state_name_Array.size() == 0){

			}
			else{
				state_id_send = dropdwn.getSelectedItem().toString();
				int k = state_name_Array.indexOf(state_id_send);
				state_id_send = state_id_Array.get(k);
				new HttpAsyncTasklastyearmarketprice().execute("http://www.mittalpower.com/mobile/pxs_app/service/lastyearmarketprice.php");
			}*/
            } else {
                bidtype_id_send = categories.get(position);
                //Toast.makeText(this, "bidtypeid :" +bidtype_id_send, Toast.LENGTH_SHORT).show();
                state_id_send = "";
                if (state_name_Array.size() == 0) {

                } else {
                    state_id_send = dropdwn.getSelectedItem().toString();
                    int k = state_name_Array.indexOf(state_id_send);
                    state_id_send = state_id_Array.get(k);
                    new HttpAsyncTasklastyearmarketprice().execute(domain + "/mobile/pxs_app/service/lastyearmarketprice.php");
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onResume() {
        try {
            if (state_id_fromdb == null) {
                gps = new GPSTracker(getActivity());
                if (gps.canGetLocation()) {
                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                    // db.execSQL("INSERT INTO gpsloaction VALUES('"+latitude+"', '"+longitude+"')");
                    // \n is for new line
                    new HttpAsyncTaskgetstatenamefromgeo().execute(domain + "/mobile/pxs_app/service/getstatenamefromgeo.php");
                    // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    if (Double.toString(latitude).equalsIgnoreCase("0.0") || Double.toString(longitude).equalsIgnoreCase("0.0")) {
                        latitude = -1;
                        longitude = -1;
                        new HttpAsyncTaskgetstatenamefromgeo().execute(domain + "/mobile/pxs_app/service/getstatenamefromgeo.php");
                        // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                    }
                } else {
                    latitude = -1;
                    longitude = -1;
                    new HttpAsyncTaskgetstatenamefromgeo().execute(domain + "/mobile/pxs_app/service/getstatenamefromgeo.php");
                    //  Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                }
            } else {
                try {
                    state_id_Array.clear();
                    state_name_Array.clear();
                    JSONObject mainObject = new JSONObject(state_id_array_fromdb);
                    JSONArray statesarray = mainObject.getJSONArray("states");
                    for (int j = 0; j < statesarray.length(); j++) {
                        JSONObject data = statesarray.getJSONObject(j);
                        if (data.has("id")) {
                            String id = data.getString("id");
                            state_id_Array.add(id);
                        }
                        if (data.has("state_name")) {
                            String state_name = data.getString("state_name");
                            state_name_Array.add(state_name);
                        }
                    }
                    String selected = mainObject.getString("selected");
                    adapter_state = new ArrayAdapter<String>(
                            getActivity(),
                            R.layout.spinner_item_state,
                            state_name_Array);
                    adapter_state
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dropdwn.setAdapter(adapter_state);
                    int k = state_id_Array.indexOf(state_id_fromdb);

                    String state_ID = state_name_Array.get(k);
                    //int citypostion =  city_namearray.indexOf(headoffamily_city);
                    //String citynpostionid = city_idarray.get(citypostion);
                    //.getSelectedItem().toString();
                    dropdwn.setSelection(adapter_state.getPosition(state_ID));
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(state_id_array_fromdb);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getActivity(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (state_id_array_fromdb != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(state_id_array_fromdb);
                        String title = html.title();
                        Toast.makeText(getActivity(), "" + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
        }
        super.onResume();
    }

    private class HttpAsyncTaskgetnewsfeedinfo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //Toast.makeText(getBaseContext(), "Received!"+result, Toast.LENGTH_LONG).show();
            try {
                if (result.contains("'s")) {
                    result = result.replace("'s", "");
                }
                if (result.contains("'")) {
                    result = result.replace("'", "");
                }
				/*if(result.contains(",")){
					result = result.replace(",", " ");
				}*/

                //result = DatabaseUtils.sqlEscapeString(result);
                db.execSQL("DROP TABLE IF EXISTS newfeedsave");
                db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
                db.execSQL("INSERT INTO newfeedsave VALUES('" + result + "', '" + formattedDatee + "', '" + result + "')");
                mainObject = new JSONObject(result);
                key_element.clear();
                Iterator<String> iter = mainObject.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    key_element.add(key);
                }
                try {
                    elementiconarray_element.clear();
                    id_key_element.clear();
                    title_key_element.clear();
                    type_key_element.clear();
                    link_key_element.clear();
                    comment_key_element.clear();
                    pubdate_key_element.clear();
                    time_key_element.clear();
                    source_key_element.clear();
                    // Object value = mainObject.get(key);
                    //if(category.length() != 0){
					/*
						}
						if(key_element.contains(category)){
						key_element.remove(category);
						key_element.add(category);
						Collections.reverse(key_element);
				    	JSONArray keyResult = mainObject.getJSONArray(category);
						for (int i = 0; i < keyResult.length(); i++) {
							JSONObject keyResulttDetails = keyResult.getJSONObject(i);
							if(keyResulttDetails.has("type")){
								String type = keyResulttDetails.getString("type");
								if(type.contains(category)){
									if(keyResulttDetails.has("id")){
										String id = keyResulttDetails.getString("id");
										id_key_element.add(id);
										elementiconarray_element.add(R.drawable.news);
									}
									if(keyResulttDetails.has("title")){
										String title = keyResulttDetails.getString("title");
										title_key_element.add(Html.fromHtml(title).toString());
									}
									if(keyResulttDetails.has("link")){
										String link = keyResulttDetails.getString("link");
										link_key_element.add(link);
									}
									if(keyResulttDetails.has("comment")){
										String comment = keyResulttDetails.getString("comment");
										comment_key_element.add(comment);
									}
									if(keyResulttDetails.has("pubdate")){
										String pubdate = keyResulttDetails.getString("pubdate");
										pubdate_key_element.add(pubdate);
									}
									if(keyResulttDetails.has("time")){
										String time = keyResulttDetails.getString("time");
										time_key_element.add(time);
									}
									if(keyResulttDetails.has("source")){
										String source = keyResulttDetails.getString("source");
										source_key_element.add(source);
									}
								}
							}
						}
						EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(NewsFeedActivity.this, elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
					    energylist.setAdapter(adapter);
						}
					 */
					/*}
						else
						{*/
                    category = key_element.get(0);
                    JSONArray keyResult = mainObject.getJSONArray(key_element.get(0));
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(key_element.get(0))) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    // }
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(NewsFeedActivity.this,"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(NewsFeedActivity.this,"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivityWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });


                } catch (JSONException e) {
                    // Something went wrong!
                }

                //tvRENEWABLE, tvPOWER, tvenergy, tvEVENT, tvCOAL, tvstripCOAL;
                // tvnewsseven, tvnewseight, tvnewsnine, tvnewsten, tvnewseleven, tvnewstwavle,
                //	 tvnewsthirdteen, tvnewsfourteen, tvnewsfifteen, tvnewssixteen;


                //TextView tvstripRENEWABLE, tvstripPOWER, tvstripenergy, tvstripEVENT, tvreadmore, tvstripreadmore
                //  tvstripnewsseven, tvstripnewseight, tvstripnewsnine, tvstripnewsten, tvstripnewseleven,
                //	 tvstripnewstwavle, tvstripnewsthirdteen, tvstripnewsfourteen, tvstripnewsfifteen, tvstripnewssixteen


                if (key_element.size() == 0 && key_element.contains(category)) {
                    //tvRENEWABLE.setText(key_element.get(0));
                    //tvRENEWABLE.setVisibility(View.VISIBLE);
                    //tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else if (key_element.size() == 1 && key_element.contains(category)) {
                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    if (key_element.get(0).equalsIgnoreCase("News")) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }
                } else if (key_element.size() == 2 && key_element.contains(category)) {
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvRENEWABLE.setText(key_element.get(0));
                    tvPOWER.setText(key_element.get(1));

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 3 && key_element.contains(category)) {
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvPOWER.setVisibility(View.VISIBLE);
                    tvenergy.setVisibility(View.VISIBLE);

                    tvRENEWABLE.setText(key_element.get(0));
                    tvPOWER.setText(key_element.get(1));
                    tvenergy.setText(key_element.get(2));

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 4 && key_element.contains(category)) {
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvPOWER.setVisibility(View.VISIBLE);
                    tvenergy.setVisibility(View.VISIBLE);
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvRENEWABLE.setText(key_element.get(0));
                    tvPOWER.setText(key_element.get(1));
                    tvenergy.setText(key_element.get(2));
                    tvEVENT.setText(key_element.get(3));

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }
                } else if (key_element.size() == 5 && key_element.contains(category)) {
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvPOWER.setVisibility(View.VISIBLE);
                    tvenergy.setVisibility(View.VISIBLE);
                    tvEVENT.setVisibility(View.VISIBLE);
                    tvCOAL.setVisibility(View.VISIBLE);
                    tvRENEWABLE.setText(key_element.get(0));
                    tvPOWER.setText(key_element.get(1));
                    tvenergy.setText(key_element.get(2));
                    tvEVENT.setText(key_element.get(3));
                    tvCOAL.setText(key_element.get(4));

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }
                } else if (key_element.size() == 6 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);
                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);
                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);
                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);
                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 7 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 8 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }


                } else if (key_element.size() == 9 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }


                } else if (key_element.size() == 10 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);


                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 11 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);

                    tvnewseleven.setText(key_element.get(10));
                    tvnewseleven.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                    if (key_element.get(10).equalsIgnoreCase(category)) {
                        tvstripnewseleven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseleven.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 12 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);

                    tvnewseleven.setText(key_element.get(10));
                    tvnewseleven.setVisibility(View.VISIBLE);

                    tvnewstwavle.setText(key_element.get(11));
                    tvnewstwavle.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                    if (key_element.get(10).equalsIgnoreCase(category)) {
                        tvstripnewseleven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseleven.setVisibility(View.GONE);
                    }

                    if (key_element.get(11).equalsIgnoreCase(category)) {
                        tvstripnewstwavle.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewstwavle.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 13 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);
                    tvstripCOAL.setVisibility(View.GONE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);

                    tvnewseleven.setText(key_element.get(10));
                    tvnewseleven.setVisibility(View.VISIBLE);

                    tvnewstwavle.setText(key_element.get(11));
                    tvnewstwavle.setVisibility(View.VISIBLE);

                    tvnewsthirdteen.setText(key_element.get(12));
                    tvnewsthirdteen.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                    if (key_element.get(10).equalsIgnoreCase(category)) {
                        tvstripnewseleven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseleven.setVisibility(View.GONE);
                    }

                    if (key_element.get(11).equalsIgnoreCase(category)) {
                        tvstripnewstwavle.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewstwavle.setVisibility(View.GONE);
                    }

                    if (key_element.get(12).equalsIgnoreCase(category)) {
                        tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsthirdteen.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 14 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);

                    tvnewseleven.setText(key_element.get(10));
                    tvnewseleven.setVisibility(View.VISIBLE);

                    tvnewstwavle.setText(key_element.get(11));
                    tvnewstwavle.setVisibility(View.VISIBLE);

                    tvnewsthirdteen.setText(key_element.get(12));
                    tvnewsthirdteen.setVisibility(View.VISIBLE);

                    tvnewsfourteen.setText(key_element.get(13));
                    tvnewsfourteen.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                    if (key_element.get(10).equalsIgnoreCase(category)) {
                        tvstripnewseleven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseleven.setVisibility(View.GONE);
                    }

                    if (key_element.get(11).equalsIgnoreCase(category)) {
                        tvstripnewstwavle.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewstwavle.setVisibility(View.GONE);
                    }

                    if (key_element.get(12).equalsIgnoreCase(category)) {
                        tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsthirdteen.setVisibility(View.GONE);
                    }

                    if (key_element.get(13).equalsIgnoreCase(category)) {
                        tvstripnewsfourteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsfourteen.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 15 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);

                    tvnewseleven.setText(key_element.get(10));
                    tvnewseleven.setVisibility(View.VISIBLE);

                    tvnewstwavle.setText(key_element.get(11));
                    tvnewstwavle.setVisibility(View.VISIBLE);

                    tvnewsthirdteen.setText(key_element.get(12));
                    tvnewsthirdteen.setVisibility(View.VISIBLE);

                    tvnewsfourteen.setText(key_element.get(13));
                    tvnewsfourteen.setVisibility(View.VISIBLE);

                    tvnewsfifteen.setText(key_element.get(14));
                    tvnewsfifteen.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                    if (key_element.get(10).equalsIgnoreCase(category)) {
                        tvstripnewseleven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseleven.setVisibility(View.GONE);
                    }

                    if (key_element.get(11).equalsIgnoreCase(category)) {
                        tvstripnewstwavle.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewstwavle.setVisibility(View.GONE);
                    }

                    if (key_element.get(12).equalsIgnoreCase(category)) {
                        tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsthirdteen.setVisibility(View.GONE);
                    }

                    if (key_element.get(13).equalsIgnoreCase(category)) {
                        tvstripnewsfourteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsfourteen.setVisibility(View.GONE);
                    }

                    if (key_element.get(14).equalsIgnoreCase(category)) {
                        tvstripnewsfifteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsfifteen.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 16 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);

                    tvnewseleven.setText(key_element.get(10));
                    tvnewseleven.setVisibility(View.VISIBLE);

                    tvnewstwavle.setText(key_element.get(11));
                    tvnewstwavle.setVisibility(View.VISIBLE);

                    tvnewsthirdteen.setText(key_element.get(12));
                    tvnewsthirdteen.setVisibility(View.VISIBLE);

                    tvnewsfourteen.setText(key_element.get(13));
                    tvnewsfourteen.setVisibility(View.VISIBLE);

                    tvnewsfifteen.setText(key_element.get(14));
                    tvnewsfifteen.setVisibility(View.VISIBLE);

                    tvnewssixteen.setText(key_element.get(15));
                    tvnewssixteen.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                    if (key_element.get(10).equalsIgnoreCase(category)) {
                        tvstripnewseleven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseleven.setVisibility(View.GONE);
                    }

                    if (key_element.get(11).equalsIgnoreCase(category)) {
                        tvstripnewstwavle.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewstwavle.setVisibility(View.GONE);
                    }

                    if (key_element.get(12).equalsIgnoreCase(category)) {
                        tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsthirdteen.setVisibility(View.GONE);
                    }

                    if (key_element.get(13).equalsIgnoreCase(category)) {
                        tvstripnewsfourteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsfourteen.setVisibility(View.GONE);
                    }

                    if (key_element.get(14).equalsIgnoreCase(category)) {
                        tvstripnewsfifteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsfifteen.setVisibility(View.GONE);
                    }

                    if (key_element.get(15).equalsIgnoreCase(category)) {
                        tvstripnewssixteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewssixteen.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 17) {

                } else if (key_element.size() == 18) {

                } else if (key_element.size() == 19) {

                } else {
                    Toast.makeText(getActivity(), "Something is wrong!", Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String errormass = mainObject.getString("message");
                    Toast.makeText(getActivity(), errormass, Toast.LENGTH_LONG).show();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                if (result != null) {
                    //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }

    private class HttpAsyncTaskgetnewsfeedinfoAgain extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //Toast.makeText(getBaseContext(), "Received!"+result, Toast.LENGTH_LONG).show();
            try {
                if (result.contains("'s")) {
                    result = result.replace("'s", "");
                }
                if (result.contains("'")) {
                    result = result.replace("'", "");
                }
				/*if(result.contains(",")){
					result = result.replace(",", " ");
				}*/
                //result = DatabaseUtils.sqlEscapeString(result);
                db.execSQL("DROP TABLE IF EXISTS newfeedsave");
                db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
                db.execSQL("INSERT INTO newfeedsave VALUES('" + result + "', '" + formattedDatee + "', '" + result + "')");
                JSONObject mainObjectnotused = new JSONObject(result);
                fatchdataofnewfeed();

            } catch (JSONException e) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String errormass = mainObject.getString("message");
                    Toast.makeText(getActivity(), errormass, Toast.LENGTH_LONG).show();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                if (result != null) {
                    //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            new CountDownTimer(300000, 1000) {
                public void onTick(long millisUntilFinished) {
                    // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    if (isVisible()) {
                        Log.e("newsfeed", "in side counter");
                        new HttpAsyncTaskgetnewsfeedinfoAgain().execute(domain + "/mobile/pxs_app/service/getnewsfeedinfo.php");
                    }
                    Log.e("newsfeed", "out side counter");
                }
            }.start();
        }
    }

    private class HttpAsyncTasklastyearmarketprice extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            LastYearMarketPrice yearMarketPrice = new LastYearMarketPrice();
            yearMarketPrice.setDevice_id(device_id);
            yearMarketPrice.setState_id(state_id_send);
            yearMarketPrice.setType(bidtype_id_send);
            return LastYearMarketPriceAPIResponse.POST(urls[0], yearMarketPrice);

            // return GET(urls[0]);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            try {
                JSONArray jsonarray = new JSONArray(result);
                int countlen = jsonarray.length();
                if (countlen > 1) {
                    graphlist.setVisibility(View.VISIBLE);
                    llheading.setVisibility(View.VISIBLE);
                    newsfeed_graph.setVisibility(View.VISIBLE);
                    //tv_expand_graph.setVisibility(View.VISIBLE);
                    llnewsfeedlegent.setVisibility(View.VISIBLE);
                } else {
                    //llheading.setVisibility(View.GONE);
                    graphlist.setVisibility(View.GONE);
                    newsfeed_graph.setVisibility(View.GONE);
                    //tv_expand_graph.setVisibility(View.GONE);
                    llnewsfeedlegent.setVisibility(View.GONE);
                }
                int i;
                date_Array.clear();
                min_Array.clear();
                max_Array.clear();
                avg_Array.clear();
                lastyearmarketprice_Arrayone.clear();
                lastyearmarketprice_Arraytwo.clear();
                lastyearmarketprice_Arraythree.clear();
                lastyearmarketprice_Arrayfour.clear();
                for (i = 0; i < jsonarray.length(); i++) {
                    JSONObject json_data = jsonarray.getJSONObject(i);
                    String date = json_data.getString("date");
                    String min = json_data.getString("min");
                    String max = json_data.getString("max");
                    String avg = json_data.getString("avg");
                    date_Array.add(date);
                    min_Array.add(min);
                    max_Array.add(max);
                    avg_Array.add(avg);

                    JSONArray dataArray = json_data.getJSONArray("data");
                    if (dataArray.isNull(0)) {
                        //tv_expand_graph.setVisibility(View.GONE);
                        llnewsfeedlegent.setVisibility(View.GONE);
                    } else {
                        //tv_expand_graph.setVisibility(View.VISIBLE);
                        llnewsfeedlegent.setVisibility(View.VISIBLE);
                        for (int j = 0; j < dataArray.length(); j++) {
                            String data = dataArray.getString(j);
                            if (i == 0) {
                                lastyearmarketprice_Arrayone.add(data);
                            } else if (i == 1) {
                                lastyearmarketprice_Arraytwo.add(data);
                            } else if (i == 2) {
                                lastyearmarketprice_Arraythree.add(data);
                            } else if (i == 3) {
                                lastyearmarketprice_Arrayfour.add(data);
                            }
                        }

                        for (int index = 0; index < countlen; index++) {
                            JSONObject object = (JSONObject) jsonarray.get(index);
                            JSONArray array = object.getJSONArray("data");

                            ArrayList<String> tempArr = new ArrayList<>();

                            for (int j = 0; j < array.length(); j++) {
                                tempArr.add((String) array.get(j));
                            }
                            dataArraya.add(tempArr);
                        }


                    }
                }
                GraphNewfeedCustomList adapter = new GraphNewfeedCustomList(getActivity(), date_Array, min_Array, max_Array,
                        avg_Array);
                graphlist.setAdapter(adapter);
            } catch (JSONException e) {
                if (result != null) {
                    try {
                        date_Array.clear();
                        graphlist.setVisibility(View.GONE);
                        newsfeed_graph.setVisibility(View.GONE);
                        //tv_expand_graph.setVisibility(View.GONE);
                        llnewsfeedlegent.setVisibility(View.GONE);
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.has("message")) {
                            String errormassage = jsonObject.getString("message");
                            Toast.makeText(getActivity(), errormassage, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    //Toast.makeText(getActivity(), "Invalid Response from Server.", Toast.LENGTH_LONG).show();
                }
                e.printStackTrace();
            }
            if (date_Array.size() > 1) {
                drawChartNav();
            } else {
                //Toast.makeText(getActivity(), "No Data Found for Graph Preview.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class HttpAsyncTaskgetstatenamefromgeo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            GPSTrakerpojo gPSTrakerpojo = new GPSTrakerpojo();
            gPSTrakerpojo.setLatitude(Double.toString(latitude));
            gPSTrakerpojo.setLongitude(Double.toString(longitude));
            return GPSTrakerAPIResponse.POST(urls[0], gPSTrakerpojo);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            //Toast.makeText(getBaseContext(), "result" + result, Toast.LENGTH_LONG).show();
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    state_id_Array.clear();
                    state_name_Array.clear();
                    JSONObject mainObject = new JSONObject(result);
                    JSONArray statesarray = mainObject.getJSONArray("states");
                    for (int j = 0; j < statesarray.length(); j++) {
                        JSONObject data = statesarray.getJSONObject(j);
                        if (data.has("id")) {
                            String id = data.getString("id");
                            state_id_Array.add(id);
                        }
                        if (data.has("state_name")) {
                            String state_name = data.getString("state_name");
                            state_name_Array.add(state_name);
                        }
                    }
                    String selected = mainObject.getString("selected");
                    adapter_state = new ArrayAdapter<String>(
                            getActivity(),
                            R.layout.spinner_item_state,
                            state_name_Array);
                    adapter_state
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dropdwn.setAdapter(adapter_state);
                    int k = state_name_Array.indexOf(selected);
                    String strI = Integer.toString(k);
                    String state_ID;
                    if (strI.equalsIgnoreCase("-1")) {
                        state_ID = "";
                    } else {
                        state_ID = state_id_Array.get(k);
                    }
                    dropdwn.setSelection(k);
                    db.execSQL("DROP TABLE IF EXISTS gpsloaction");
                    db.execSQL("CREATE TABLE IF NOT EXISTS gpsloaction(latitude VARCHAR, longitude VARCHAR, state_id VARCHAR, state_serial VARCHAR);");
                    db.execSQL("INSERT INTO gpsloaction VALUES('" + result + "', '" + result + "', '" + state_ID + "', '1')");
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getActivity(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Invalid JSON: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }


}
