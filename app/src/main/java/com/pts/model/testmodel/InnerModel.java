package com.pts.model.testmodel;

/**
 * Created by Ashish on 28-07-2017.
 */

public class InnerModel {

    private String year;
    private String demand;
    private String peak;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDemand() {
        return demand;
    }

    public void setDemand(String demand) {
        this.demand = demand;
    }

    public String getPeak() {
        return peak;
    }

    public void setPeak(String peak) {
        this.peak = peak;
    }
}
