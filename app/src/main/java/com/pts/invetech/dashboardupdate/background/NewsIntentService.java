package com.pts.invetech.dashboardupdate.background;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pts.invetech.dashboardupdate.model.news.NewsData;
import com.pts.invetech.utils.AppLogger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by vaibhav on 13/1/17.
 */

public class NewsIntentService extends IntentService {


    public NewsIntentService() {
        super("NewsIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent != null) {
            ResultReceiver receiver = intent.getParcelableExtra("receiverTag");
            String url = intent.getStringExtra("url");

            String newsData = networkCall(url, null);

            Bundle bundle = new Bundle();
            bundle.putString("data", newsData);

            receiver.send(1, bundle);
        }
    }

    private String networkCall(String currentUrl, String jsonParam) {
        URL url = null;
        HttpURLConnection connection = null;
        String response = "";
        try {
            url = new URL(currentUrl);

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            if (jsonParam != null) {
            DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());

                dStream.writeBytes("data=" + jsonParam);

            dStream.flush();
            dStream.close();
            }


            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = null;
            }

        } catch (Exception e) {
            AppLogger.showMsg("Error in Network Handler String", e.getMessage());
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return response;
    }
}
