
package com.pts.model.stationmain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StationUrsMain {

    @SerializedName("region")
    @Expose
    private Region region;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("date")
    @Expose
    private String date;

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
