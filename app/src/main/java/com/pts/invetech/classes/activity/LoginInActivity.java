package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.AlertDialogManager;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.GPSTracker;
import com.pts.invetech.R;
import com.pts.invetech.ServerUtilities;
import com.pts.invetech.apiresponse.LoginAPIResponse;
import com.pts.invetech.apiresponse.SplashScreenAPIResponse;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.pojo.DeviceRegister;
import com.pts.invetech.pojo.Login;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Constant;
import com.pts.invetech.utils.JSONUtils;
import com.pts.invetech.utils.Validation;
import com.pts.invetech.utils.WakeLocker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import static com.pts.invetech.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.pts.invetech.CommonUtilities.EXTRA_MESSAGE;
import static com.pts.invetech.CommonUtilities.SENDER_ID;
import static com.pts.invetech.CommonUtilities.SERVER_URL;

public class LoginInActivity extends Activity {
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String USERNAME = "usernameKey";
    public static final String PASSWORD = "passwordKey";
    final Context context = this;
    // alert dialog manager
    private AlertDialogManager alert = new AlertDialogManager();
    // Internet detector
    private ConnectionDetector cd;
    private String username, pass;
    private CheckBox login_chkbox;
    private Dialog prgDialog;
    private EditText edittextusename, edittextpassword;
    private Button signin;
    private Editor editor;
    private int incoiceid;
    private String descinvno, descriptiondata, uniprice, ammount, disccount, quantity,
            dusername, dpassword;
    private Boolean isInternetPresent = false;
    private SharedPreferences pref;
    private SQLiteDatabase db;
    private String dbacess;
    private String device_id;
    private String access_key, status, value, message, companyName, client_id;
    private TextView tvchangepassword;
    private boolean isLogedin = false;
    private String name, platform, uuid, version, height, width, colordepth;
    private String sprevision_send, spdate_send;
    private AsyncTask<Void, Void, Void> mRegisterTask;
    private String newMessage;
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(LoginInActivity.this);

            /**
             * Take appropriate action on this message
             * depending upon your app requirement
             * For now i am just displaying it on the screen
             * */

            // Showing received message
            //lblMessage.append("Ashish Agrawal:" + newMessage + "\n");
            if (newMessage.equalsIgnoreCase("Your device registred with GCM") || newMessage.equalsIgnoreCase("Trying (attempt 1/5) to register device on Demo Server.") ||
                    newMessage.equalsIgnoreCase("From Demo Server: successfully added device!") || newMessage.equalsIgnoreCase("null")) {

            } else {
                //db.execSQL("INSERT INTO pushnotification VALUES('"+newMessage+"', '" + datedb + "', '" + timedb + "')");
            }
            //Toast.makeText(NotificationActivity.this, "New Message: " + newMessage, Toast.LENGTH_LONG).show();
            // Releasing wake lock
            WakeLocker.release();
        }
    };
    private TextView tvrequestfordemo;
    private GPSTracker gps;
    private String refreshedToken;
    private boolean isFrmScheduling = false;
    private String apnaNumber = "-11";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_login);
        /*prgDialog = new ProgressDialog(this);
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);*/
////        Get hold of the registration token
//        refreshedToken = FirebaseInstanceId.getInstance().getToken();
//
//        //Log the token
//        Log.d(TAG, "Login Refreshed token: " + refreshedToken);

        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

		/*TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		device_id = tManager.getDeviceId();*/
        //device_id = Secure.getString(LoginInActivity.this.getContentResolver(),Secure.ANDROID_ID);

        db = openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientid(clientid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS notificationcount(notificationcount VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS recpriceTable(recprice VARCHAR);");

		/*Cursor c = db.rawQuery("SELECT * FROM baby", null);
		if(c.getCount()==0)
		{
			//showMessage("Error", "No records found");
		}
		StringBuffer buffer=new StringBuffer();
		while(c.moveToNext())
		{
			buffer.append("uuid: "+c.getString(0)+"\n");
			access_key = c.getString(0);

			//Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
		}*/

		/*Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
		if(loginc.getCount()==0)
		{
			//showMessage("Error", "No records found");
		}
		StringBuffer logincbuffer=new StringBuffer();
		while(loginc.moveToNext())
		{
			logincbuffer.append("loginc: "+loginc.getString(0)+"\n");
			username = loginc.getString(0);
			pass = loginc.getString(1);
			new HttpAsyncLoginTask().execute(Constant.MPPLLOGIN);
			//Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
		}*/

        Cursor c = db.rawQuery("SELECT * FROM device", null);
        if (c.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("uuid: " + c.getString(0) + "\n");
            device_id = c.getString(0);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
        }

        Intent in = getIntent();
        sprevision_send = in.getStringExtra("revision");
        spdate_send = in.getStringExtra("date");

        if (in.hasExtra(Constant.VALUE_FRMSCH)) {
            isFrmScheduling = in.getBooleanExtra(Constant.VALUE_FRMSCH, false);
            apnaNumber = in.getStringExtra(Constant.VALUE_APPNUMBERFROMGRAPH);
        }

        cd = new ConnectionDetector(getApplicationContext());
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            //alert.showAlertDialog(LoginInActivity.this,"Internet Connection Error","Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }

        // Check if GCM configuration is set
        if (SERVER_URL == null || SENDER_ID == null || SERVER_URL.length() == 0
                || SENDER_ID.length() == 0) {
            // GCM sernder id / server url is missing
            alert.showAlertDialog(LoginInActivity.this,
                    "Configuration Error!",
                    "Please set your Server URL and GCM Sender ID", false);
            // stop executing code by return
            return;
        }

        findAttributesID();
		/*String change = "CHANGE PASSWORD";
	    tvchangepassword = (TextView)findViewById(R.id.tvchangepassword);
		SpannableString spanString = new SpannableString(change);
		spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
		spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
		spanString.setSpan(new StyleSpan(Typeface.ITALIC), 0, spanString.length(), 0);*/

        signin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
				/*if (login_chkbox.isChecked()) {
					pref = getApplicationContext().getSharedPreferences(
							MyPREFERENCES, Context.MODE_PRIVATE);
					editor = pref.edit();
					editor.putString(USERNAME, username);
					editor.putString(PASSWORD, pass);
					editor.commit();
				}*/
            }
        });

		/*tvchangepassword.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent in = new Intent(LoginInActivity.this, ChangePasswordActivity.class);
				startActivity(in);

			}
		});*/


        tvrequestfordemo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(LoginInActivity.this, RequestForDemoActivity.class);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

    }

    /**
     * find attributes id's
     */
    void findAttributesID() {
        edittextusename = (EditText) findViewById(R.id.editText1);
        edittextpassword = (EditText) findViewById(R.id.editText2);
        signin = (Button) findViewById(R.id.login);
        tvrequestfordemo = (TextView) findViewById(R.id.tvrequestfordemo);
        Animation anim = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.blink_request);
        tvrequestfordemo.startAnimation(anim);
        //tvrequestfordemo.setPaintFlags(tvrequestfordemo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        login_chkbox = (CheckBox) findViewById(R.id.login_chkbox);

    }

    /**
     * used for login
     */
    private void login() {
        username = edittextusename.getText().toString();
        pass = edittextpassword.getText().toString();
        if (Validation.isFieldEmpty(edittextusename)) {
            edittextusename.setError("Username is Required");
        } else if (Validation.isFieldEmpty(edittextpassword)) {
            edittextpassword.setError("Password is Required");
        } else {
            cd = new ConnectionDetector(LoginInActivity.this);
            if (!cd.isConnectingToInternet()) {
                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
            } else {
                new HttpAsyncLoginTask().execute("https://" + Constant.BASE_URL + Constant.LOGIN);
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("INSERT INTO logininfo VALUES('" + username + "','" + pass + "')");
            }
        }
    }

    @Override
    protected void onResume() {
        try {
            pref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            username = pref.getString(USERNAME, null);
            pass = pref.getString(PASSWORD, null);
//			edittextusename.setText(usernamelogin
            Cursor c = db.rawQuery("SELECT * FROM baby", null);
            if (c.getCount() == 0) {
                //showMessage("Error", "No records found");
            }
            StringBuffer buffer = new StringBuffer();
            while (c.moveToNext()) {
                buffer.append("uuid: " + c.getString(0) + "\n");
                access_key = c.getString(0);

                //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
            }

            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
            }
            StringBuffer logincbuffer = new StringBuffer();
            while (loginc.moveToNext()) {
                logincbuffer.append("loginc: " + loginc.getString(0) + "\n");
                //username = loginc.getString(0);
                ///pass = loginc.getString(1);
                if (isLogedin) {
                    //new HttpAsyncLoginTask().execute(Constant.MPPLLOGIN);

                }//Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
            }

            login_chkbox.setChecked(true);
        } catch (Exception e) {
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        isLogedin = true;
    }

    public void showMessage(String title, String message) {
        Builder builder = new Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        //backButtonHandler();
        Intent in = new Intent(LoginInActivity.this, NewFeedMainActivity.class);
        in.putExtra("LOGINCONFIG", "LOGIN");
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    public void backButtonHandler() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                LoginInActivity.this);
        // Setting Dialog Title
        //alertDialog.setTitle("Leave application?");
        // Setting Dialog Message
        alertDialog.setMessage("Are you sure you want to leave the application?");
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.mainicon);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        dialog.cancel();
                    }
                });
        // Showing Alert Message
        alertDialog.show();
    }

    public void gcmregister() {

        cd = new ConnectionDetector(LoginInActivity.this);
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(LoginInActivity.this, "Please Check your Network Connectivity.", Toast.LENGTH_LONG).show();
        }
        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(LoginInActivity.this);
        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
        GCMRegistrar.checkManifest(LoginInActivity.this);

        registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));

        // Get GCM registration id
        final String regId = GCMRegistrar.getRegistrationId(LoginInActivity.this);
        //regId = refreshedToken;
        // Check if regid already presents
        if (regId.equals("revvev")) {
            // Registration is not present, register now with GCM
            GCMRegistrar.register(LoginInActivity.this, SENDER_ID);
        } else {
            // Device is already registered on GCM
				/*if (GCMRegistrar.isRegisteredOnServer(LoginInActivity.this)) {
					// Skips registration.
				//	Toast.makeText(getActivity(), "Already registered with GCM", Toast.LENGTH_LONG).show();
				} else {*/
            // Try to register again, but not in the UI thread.
            // It's also necessary to cancel the thread onDestroy(),
            // hence the use of AsyncTask instead of a raw thread.
            final Context context = LoginInActivity.this;
            mRegisterTask = new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... params) {
                    // Register on our server
                    // On server creates a new user
                    ServerUtilities.register(context, device_id, refreshedToken);
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    mRegisterTask = null;
                }

            };
            mRegisterTask.execute(null, null, null);
            /*}*/
        }
        new HttpAsyncLoginTask().execute("https://" + Constant.BASE_URL + Constant.LOGIN);

    }

    @Override
    public void onDestroy() {
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        if (prgDialog != null) {
            prgDialog.dismiss();
            prgDialog = null;
        }
        try {
            //	getAgetunregisterReceiver(mHandleMessageReceiver);
            unregisterReceiver(mHandleMessageReceiver);
            GCMRegistrar.onDestroy(LoginInActivity.this);
        } catch (Exception e) {
            AppLogger.showMsg("UnRegister Receiver Error", e.getMessage());
        }
        super.onDestroy();
    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm username, password, dbacess
     */
    private class HttpAsyncLoginTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Login login = new Login();
            login.setU(username);
            login.setP(pass);
            login.setDevice_id(device_id);

            login.setVersion("4.2");
            //Toast.makeText(LoginInActivity.this, "result" + "2.41", Toast.LENGTH_LONG).show();
            return LoginAPIResponse.POST(urls[0], login);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("Get Login Result:", result);
            //Toast.makeText(LoginInActivity.this, "result" + result, Toast.LENGTH_LONG).show();
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    db.execSQL("DROP TABLE IF EXISTS recpriceTable");
                    db.execSQL("CREATE TABLE IF NOT EXISTS recpriceTable(recprice VARCHAR);");
                    db.execSQL("INSERT INTO recpriceTable VALUES('" + result + "')");

                    JSONObject mainObject = new JSONObject(result);
                    if (mainObject.has("status")) {
                        status = mainObject.getString("status");
                    }
                    if (mainObject.has("value")) {
                        value = mainObject.getString("value");
                    }
                    if (mainObject.has("message")) {
                        message = mainObject.getString("message");
                    }
                    if (mainObject.has("access_key")) {
                        access_key = mainObject.getString("access_key");
                        db.execSQL("INSERT INTO baby VALUES('" + access_key + "')");
                    }
                    if (mainObject.has("companyName")) {
                        companyName = mainObject.getString("companyName");
                    }
                    if (mainObject.has("client_id")) {
                        client_id = mainObject.getString("client_id");
                    }
                    if (mainObject.has("menujson")) {
                        db.execSQL("DROP TABLE IF EXISTS menuTable");
                        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
                        JSONArray menujson = mainObject.getJSONArray("menujson");
                        for (int i = 0; i < menujson.length(); i++) {
                            JSONObject menujson_details = menujson.getJSONObject(i);
                            if (menujson_details.has("Caption")) {
                                String caption = menujson_details.getString("Caption");

                                if (caption.equalsIgnoreCase("Dashboard")) {
                                    String dashboard = "Dashboard";
                                    db.execSQL("INSERT INTO menuTable VALUES('" + dashboard + "')");
                                }

                                if (caption.equalsIgnoreCase("Place New Bid")) {
                                    String placeNewBid = "Place New Bid";
                                    db.execSQL("INSERT INTO menuTable VALUES('" + placeNewBid + "')");
                                }

                                if (caption.equalsIgnoreCase("No Bid")) {
                                    String noBid = "No Bid";
                                    db.execSQL("INSERT INTO menuTable VALUES('" + noBid + "')");
                                }

                                if (caption.equalsIgnoreCase("Market Price")) {
                                    String marketPrice = "Market Price";
                                    db.execSQL("INSERT INTO menuTable VALUES('" + marketPrice + "')");
                                }

                                if (caption.equalsIgnoreCase("Downloads")) {
                                    String downloads = "Downloads";
                                    db.execSQL("INSERT INTO menuTable VALUES('" + downloads + "')");
                                }

                                if (caption.equalsIgnoreCase("Reports")) {
                                    JSONArray submenu = menujson_details.getJSONArray("submenu");
                                    for (int j = 0; j < submenu.length(); j++) {
                                        JSONObject submenu_details = submenu.getJSONObject(j);
                                        if (submenu_details.has("Caption")) {
                                            String submenucaption = submenu_details.getString("Caption");
                                            if (submenucaption.equalsIgnoreCase("Payment Details")) {
                                                String paymentDetails = "Payment Details";
                                                db.execSQL("INSERT INTO menuTable VALUES('" + paymentDetails + "')");
                                            }
                                        }
                                    }
                                }
                                if (caption.equalsIgnoreCase("Notifications")) {
                                    String notifications = "Notifications";
                                    db.execSQL("INSERT INTO menuTable VALUES('" + notifications + "')");
                                }
                            }
                        }
                    }
                    if (mainObject.has("lastBidTime")) {
                        db.execSQL("DROP TABLE IF EXISTS lastbidtimeTable");
                        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
                        String lastBidTime = mainObject.getString("lastBidTime");
                        db.execSQL("INSERT INTO lastbidtimeTable VALUES('" + lastBidTime + "')");
                    }
                    if (mainObject.has("notificationCount")) {
                        db.execSQL("DROP TABLE IF EXISTS notificationcount");
                        db.execSQL("CREATE TABLE IF NOT EXISTS notificationcount(notificationcount VARCHAR);");
                        String notificationCount = mainObject.getString("notificationCount");
                        db.execSQL("INSERT INTO notificationcount VALUES('" + notificationCount + "')");
                    }
                    if (mainObject.has("newsletterBaseUrl")) {
                        String newsletterBaseUrl = mainObject.getString("newsletterBaseUrl");
                        SharedPrefHandler.saveDeviceString(getApplicationContext(),
                                "newsletterBaseUrl", newsletterBaseUrl);
                    }
                    if (mainObject.has("domain")) {
                        String domain = mainObject.getString("domain");
                        SharedPrefHandler.saveDeviceString(getApplicationContext(),
                                "domain", domain);
                    }

				/*if(mainObject.has("recprice")){
					JSONObject solorObject = mainObject.getJSONObject("solor");
					if(solorObject.has("max")){
						solorObject.getString("max");
					}if(solorObject.has("min")){
						solorObject.getString("min");
					}
					JSONObject nonsolorObject = mainObject.getJSONObject("nonsolor");
					if(nonsolorObject.has("max")){
						nonsolorObject.get("max");
					}if(nonsolorObject.has("min")){
						nonsolorObject.get("min");
					}

				}*/


                    if (status.equalsIgnoreCase("SUCCESS")) {
                        if (isFrmScheduling) {
                            Intent in = new Intent(LoginInActivity.this, MainActivityAfterLogin.class);
                            in.putExtra(Constant.VALUE_SHEDULETRACK, "scheduleTrack");
                            in.putExtra(Constant.VALUE_APPNUMBERFROMGRAPH, apnaNumber);
                            db.execSQL("INSERT INTO clientid VALUES('" + client_id + "')");
                            db.execSQL("INSERT INTO companyname VALUES('" + companyName + "')");
                            context.startActivity(in);
                            overridePendingTransition(R.anim.animation, R.anim.animation2);
                            finish();
                        } else if ((sprevision_send.equalsIgnoreCase("revision")) && (spdate_send.equalsIgnoreCase("date"))) {
                            Intent in = new Intent(LoginInActivity.this, MainActivityAfterLogin.class);
                            db.execSQL("INSERT INTO clientid VALUES('" + client_id + "')");
                            db.execSQL("INSERT INTO companyname VALUES('" + companyName + "')");
                            context.startActivity(in);
                            overridePendingTransition(R.anim.animation, R.anim.animation2);
                            finish();
                        } else {
                            Intent in = new Intent(LoginInActivity.this, ScheduleVsCurtailmentActivity.class);
                            in.putExtra("revision", sprevision_send);
                            in.putExtra("date", spdate_send);
                            db.execSQL("INSERT INTO clientid VALUES('" + client_id + "')");
                            db.execSQL("INSERT INTO companyname VALUES('" + companyName + "')");
                            context.startActivity(in);
                            overridePendingTransition(R.anim.animation, R.anim.animation2);
                            finish();
                        }
                    } else if (status.equalsIgnoreCase("ERR")) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    } else if (status.equalsIgnoreCase("VERSION")) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                LoginInActivity.this);
                        alertDialogBuilder
                                .setMessage(message);
                        alertDialogBuilder.setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        } catch (android.content.ActivityNotFoundException anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        }
                                    }
                                });
                        alertDialogBuilder.setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        // finish();
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else if (status.equalsIgnoreCase("This Device is not not registered.Please contact to admin for more detail.")) {
                        name = android.os.Build.MODEL;
                        platform = "Android";
					/*TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
					uuid = tManager.getDeviceId();*/
                        uuid = Secure.getString(LoginInActivity.this.getContentResolver(),
                                Secure.ANDROID_ID);
                        version = android.os.Build.VERSION.RELEASE;
                        // DisplayMetrics metrics =
                        // context.getResources().getDisplayMetrics();
                        DisplayMetrics metrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(metrics);
                        int width1 = metrics.widthPixels;
                        width = String.valueOf(width1);
                        int height1 = metrics.heightPixels;
                        height = String.valueOf(height1);
                        colordepth = "";
                        new HttpAsyncTask().execute("https://" + Constant.BASE_URL + Constant.REGISTERDEVICE);
                    } else if (status.equalsIgnoreCase("GSMERR")) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        gcmregister();
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            DeviceRegister deviceregister = new DeviceRegister();
            deviceregister.setName(name);
            deviceregister.setPlatform(platform);
            deviceregister.setUuid(uuid);
            deviceregister.setVersion(version);
            deviceregister.setWidth(width);
            deviceregister.setHeight(height);
            deviceregister.setColordepth(colordepth);
            return SplashScreenAPIResponse.POST(urls[0], deviceregister);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    String message = mainObject.getString("message");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        //Intent in = new Intent(LoginInActivity.this,NewsFeedActivity.class);
                        ///in.putExtra("device_id", uuid);
                        //in.putExtra("LOGINCONFIG", "LOGIN");
                        db.execSQL("INSERT INTO device VALUES('" + uuid + "')");
                        //startActivity(in);
                        //overridePendingTransition(R.anim.animation,R.anim.animation2);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        //Intent in = new Intent(SplashScreenActivity.this,NewsFeedActivity.class);
                        //in.putExtra("device_id", uuid);
                        //in.putExtra("LOGINCONFIG", "LOGIN");
                        db.execSQL("INSERT INTO device VALUES('" + uuid + "')");
                        //startActivity(in);
                        //overridePendingTransition(R.anim.animation,R.anim.animation2);
                        //finish();
                    }
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }


}