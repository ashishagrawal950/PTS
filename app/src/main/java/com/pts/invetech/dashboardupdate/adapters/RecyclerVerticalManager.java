package com.pts.invetech.dashboardupdate.adapters;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Vaibhav on 08-08-2016.
 */

public class RecyclerVerticalManager extends LinearLayoutManager {

    private Context mContext;
    public LinearSmoothScroller smoothScroller;
    private int mPos=0;

    public RecyclerVerticalManager(Context context) {
        super(context, LinearLayoutManager.VERTICAL, false);
        mContext = context;
        smoothScroller=new New_MySmoothScroller(mContext,this);
    }

    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView,
                                       RecyclerView.State state, int position) {
        mPos=position;
        smoothScroller.setTargetPosition(position);
        startSmoothScroll(smoothScroller);
    }

    public void againStart(){
        smoothScroller.setTargetPosition(mPos);
        startSmoothScroll(smoothScroller);
    }
}
