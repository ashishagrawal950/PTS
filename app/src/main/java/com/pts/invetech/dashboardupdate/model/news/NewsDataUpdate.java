package com.pts.invetech.dashboardupdate.model.news;

import java.util.ArrayList;

/**
 * Created by vaibhav on 20/1/17.
 */

public class NewsDataUpdate {

    private ArrayList<NewsDataInter> newsDataInters=null;

    public NewsDataUpdate(){
        newsDataInters=new ArrayList<>();
    }

    public ArrayList<NewsDataInter> getNewsDataInters() {
        return newsDataInters;
    }

    public void setNewsDataInters(ArrayList<NewsDataInter> newsDataInters) {
        this.newsDataInters = newsDataInters;
    }


    public void addNewsInter(NewsDataInter dataInter){
        this.newsDataInters.add(dataInter);
    }

    public void clearList(){
        this.newsDataInters.clear();
    }

    public ArrayList<SubNews> getFullNews(){
        ArrayList<SubNews> subNewsArray=new ArrayList<>();

        for(NewsDataInter arr:newsDataInters){
            subNewsArray.addAll(arr.getSubNewsList());
        }

        return subNewsArray;
    }

}
