package com.pts.invetech.classes.afterlogin;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Html;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.AlertDialogManager;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.GPSTracker;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.SearchElementAPIResponse;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.classes.activity.NewsFeedActivitySearchWebView;
import com.pts.invetech.classes.fragment.MainActivityFragment;
import com.pts.invetech.customlist.EnergyNewfeedCustomList;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.pojo.SearchElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Iterator;

public class AllNewsFeedFragmentafterlogin extends Fragment {
    final Context context = getActivity();
    // alert dialog manager
    private AlertDialogManager alert = new AlertDialogManager();
    // Internet detector
    private ConnectionDetector cd;
    private Dialog prgDialog;
    private Editor editor;
    private LinearLayout openLayout;
    private String descinvno, descriptiondata, uniprice, ammount, disccount, quantity,
            dusername, dpassword;
    private Boolean isInternetPresent = false;
    private SharedPreferences pref;
    private SQLiteDatabase db;
    private String device_id;
    private String access_key, status, value, message, category, companyName,
            client_id;
    private NonScrollListView energylist;
    private String icontypefrom, dbacess, displaydate, displaytime;
    private String[] icontypearray;
    private String[] array;
    private String[] arraydispalydate;
    private String[] arraydispalytime;
    private Integer[] elementiconarray;
    private TextView tvRENEWABLE, tvPOWER, tvenergy, tvEVENT, tvCOAL, tvreadmore,
            tvnewsseven, tvnewseight, tvnewsnine, tvnewsten, tvnewseleven,
            tvnewstwavle, tvnewsthirdteen, tvnewsfourteen, tvnewsfifteen,
            tvnewssixteen;
    private TextView tvstripRENEWABLE, tvstripPOWER, tvstripenergy, tvstripEVENT,
            tvstripCOAL, tvstripreadmore, tvstripnewsseven, tvstripnewseight,
            tvstripnewsnine, tvstripnewsten, tvstripnewseleven,
            tvstripnewstwavle, tvstripnewsthirdteen, tvstripnewsfourteen,
            tvstripnewsfifteen, tvstripnewssixteen;
    private int width;
    private HorizontalScrollView hsview;
    private LinearLayout llhsview, lltoplogin, lltoplogout;
    private String loginconfig;
    private JSONObject mainObject;
    private ArrayList<String> key_element = new ArrayList<String>();
    private ArrayList<Integer> elementiconarray_element = new ArrayList<Integer>();
    private ArrayList<String> id_key_element = new ArrayList<String>();
    private ArrayList<String> title_key_element = new ArrayList<String>();
    private ArrayList<String> type_key_element = new ArrayList<String>();
    private ArrayList<String> link_key_element = new ArrayList<String>();
    private ArrayList<String> comment_key_element = new ArrayList<String>();
    private ArrayList<String> pubdate_key_element = new ArrayList<String>();
    private ArrayList<String> time_key_element = new ArrayList<String>();
    private ArrayList<String> source_key_element = new ArrayList<String>();

    private ArrayList<String> date_Array = new ArrayList<String>();
    private ArrayList<String> min_Array = new ArrayList<String>();
    private ArrayList<String> max_Array = new ArrayList<String>();
    private ArrayList<String> avg_Array = new ArrayList<String>();

    private ArrayList<String> lastyearmarketprice_Arrayone = new ArrayList<String>();
    private ArrayList<String> lastyearmarketprice_Arraytwo = new ArrayList<String>();
    private ArrayList<String> lastyearmarketprice_Arraythree = new ArrayList<String>();

    private TextView tvlogintext, tvlogouttext, buttonClick;
    private String sprevision_send, spdate_send, saveResponse;
    private LinearLayout layout_mcp;

    private GPSTracker gps;
    private double latitude, longitude;
    private ArrayList<String> categories = new ArrayList<String>();
    private ArrayList<String> state_id_Array = new ArrayList<String>();
    private ArrayList<String> state_name_Array = new ArrayList<String>();
    private ArrayAdapter<String> adapter_state;
    private Spinner dropdwn, spbidtype;
    private String state_id_array_fromdb, state_name_name_fromdb, state_id_fromdb;
    private String state_id_send, bidtype_id_send, domain;
    private boolean isTrue = true;
    private View rootView;
    private EditText inputSearch;
    private EnergyNewfeedCustomList newsfeedadapter;
    private ImageView ivsearch;
    private String searchelement = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
//        domain = getArguments().getString("domain");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_allnewsfeed, container,
                false);
		/*prgDialog = new ProgressDialog(getActivity());
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        db = getActivity().openOrCreateDatabase("deviceDBSecond",
                Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientid(clientid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationnewsfeed(newMessage TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS gpsloaction(latitude VARCHAR, longitude VARCHAR, state_id VARCHAR, state_serial VARCHAR);");

        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
//        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivityAfterLogin) getActivity()).open();
            }
        });

        layout_mcp = (LinearLayout) rootView.findViewById(R.id.layout_mcp);

        inputSearch = (EditText) rootView.findViewById(R.id.inputSearch);
        ivsearch = (ImageView) rootView.findViewById(R.id.ivsearch);


        hsview = (HorizontalScrollView) rootView.findViewById(R.id.hsview);
        llhsview = (LinearLayout) rootView.findViewById(R.id.llhsview);
        tvRENEWABLE = (TextView) rootView.findViewById(R.id.tvRENEWABLE);
        tvstripRENEWABLE = (TextView) rootView
                .findViewById(R.id.tvstripRENEWABLE);

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);

        tvPOWER = (TextView) rootView.findViewById(R.id.tvPOWER);
        tvstripPOWER = (TextView) rootView.findViewById(R.id.tvstripPOWER);

        tvenergy = (TextView) rootView.findViewById(R.id.tvenergy);
        tvstripenergy = (TextView) rootView.findViewById(R.id.tvstripenergy);
        energylist = (NonScrollListView) rootView.findViewById(R.id.energylist);

        tvEVENT = (TextView) rootView.findViewById(R.id.tvEVENT);
        tvstripEVENT = (TextView) rootView.findViewById(R.id.tvstripEVENT);

        tvCOAL = (TextView) rootView.findViewById(R.id.tvCOAL);
        tvstripCOAL = (TextView) rootView.findViewById(R.id.tvstripCOAL);

        tvreadmore = (TextView) rootView.findViewById(R.id.tvreadmore);
        tvstripreadmore = (TextView) rootView
                .findViewById(R.id.tvstripreadmore);

        tvnewsseven = (TextView) rootView.findViewById(R.id.tvnewsseven);
        tvstripnewsseven = (TextView) rootView
                .findViewById(R.id.tvstripnewsseven);

        tvnewseight = (TextView) rootView.findViewById(R.id.tvnewseight);
        tvstripnewseight = (TextView) rootView
                .findViewById(R.id.tvstripnewseight);

        tvnewsnine = (TextView) rootView.findViewById(R.id.tvnewsnine);
        tvstripnewsnine = (TextView) rootView
                .findViewById(R.id.tvstripnewsnine);

        tvnewsten = (TextView) rootView.findViewById(R.id.tvnewsten);
        tvstripnewsten = (TextView) rootView.findViewById(R.id.tvstripnewsten);

        tvnewseleven = (TextView) rootView.findViewById(R.id.tvnewseleven);
        tvstripnewseleven = (TextView) rootView
                .findViewById(R.id.tvstripnewseleven);

        tvnewstwavle = (TextView) rootView.findViewById(R.id.tvnewstwavle);
        tvstripnewstwavle = (TextView) rootView
                .findViewById(R.id.tvstripnewstwavle);

        tvnewsthirdteen = (TextView) rootView
                .findViewById(R.id.tvnewsthirdteen);
        tvstripnewsthirdteen = (TextView) rootView
                .findViewById(R.id.tvstripnewsthirdteen);

        tvnewsfourteen = (TextView) rootView.findViewById(R.id.tvnewsfourteen);
        tvstripnewsfourteen = (TextView) rootView
                .findViewById(R.id.tvstripnewsfourteen);

        tvnewsfifteen = (TextView) rootView.findViewById(R.id.tvnewsfifteen);
        tvstripnewsfifteen = (TextView) rootView
                .findViewById(R.id.tvstripnewsfifteen);

        tvnewssixteen = (TextView) rootView.findViewById(R.id.tvnewssixteen);
        tvstripnewssixteen = (TextView) rootView
                .findViewById(R.id.tvstripnewssixteen);

        // new
        // HttpAsyncTasklastyearmarketprice().execute("http://www.mittalpower.com/mobile/pxs_app/service/lastyearmarketprice.php");
        cd = new ConnectionDetector(getActivity());
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getActivity(), "Please Check your network.",
                    Toast.LENGTH_LONG).show();
        } else {
            Cursor newfeedsavec = db
                    .rawQuery("select * from newfeedsave", null);
            if (newfeedsavec.getCount() == 0) {
                // new HttpAsyncTaskmla().execute("http://" + ipaddressfromdb +
                // Constant.ELECTIONGetMlaInfo);
                // new
                // HttpAsyncTaskgetnewsfeedinfo().execute("http://www.mittalpower.com/mobile/pxs_app/service/getnewsfeedinfo.php");
            } else {
                newfeedsavec.moveToFirst();
                do {
                    saveResponse = newfeedsavec.getString(newfeedsavec
                            .getColumnIndex("response"));
                    // String notificationdate =
                    // newfeedsavec.getString(newfeedsavec.getColumnIndex("notificationdate"));
                    // Toast.makeText(NewsFeedActivity.this, saveResponse,
                    // Toast.LENGTH_LONG).show();
                } while (newfeedsavec.moveToNext());
                fatchdataofnewfeed();
            }
        }
        lltoplogin.setVisibility(View.GONE);
        lltoplogout.setVisibility(View.VISIBLE);
        tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
        tvlogouttext.setText("Home");


        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(in);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);

            }
        });


        llhsview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //layout_mcp.setVisibility(View.GONE);
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);
            }
        });


        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        int height = size.y;

        tvRENEWABLE.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.VISIBLE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.BOLD);
                tvRENEWABLE.setTextSize(16);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvRENEWABLE.getLeft() - (width / 2)) +
                // (tvRENEWABLE.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvRENEWABLE);

                String keyelementfind = (String) tvRENEWABLE.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }

            }
        });

        tvPOWER.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.VISIBLE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.BOLD);
                tvPOWER.setTextSize(16);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvPOWER.getLeft() - (width / 2)) +
                // (tvPOWER.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvPOWER);
                // String keyelemrnfind = tvPOWER.getText();
                String keyelementfind = (String) tvPOWER.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }

            }
        });

        tvenergy.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.VISIBLE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.BOLD);
                tvenergy.setTextSize(16);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvenergy.getLeft() - (width / 2)) +
                // (tvenergy.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvenergy);

                String keyelementfind = (String) tvenergy.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }

            }
        });

        tvEVENT.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.VISIBLE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.BOLD);
                tvEVENT.setTextSize(16);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvEVENT.getLeft() - (width / 2)) +
                // (tvEVENT.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);

                String keyelementfind = (String) tvEVENT.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvCOAL.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.VISIBLE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.BOLD);
                tvCOAL.setTextSize(16);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvCOAL.getLeft() - (width / 2)) +
                // (tvCOAL.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvPOWER);

                String keyelementfind = (String) tvCOAL.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvreadmore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.VISIBLE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(16);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvreadmore.getRight() - (width / 2)) +
                // (tvreadmore.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvreadmore.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewsseven.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.VISIBLE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(16);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // /int scrollX = (tvreadmore.getRight() - (width / 2)) +
                // (tvreadmore.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewsseven.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewseight.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.VISIBLE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(16);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvreadmore.getRight() - (width / 2)) +
                // (tvreadmore.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewseight.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    // String.valueOf(position);
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewsnine.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.VISIBLE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(16);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // /int scrollX = (tvreadmore.getRight() - (width / 2)) +
                // (tvreadmore.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvreadmore);
                String keyelementfind = (String) tvnewsnine.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }

            }

        });

        tvnewsten.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.VISIBLE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(16);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvreadmore.getRight() - (width / 2)) +
                // (tvreadmore.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewsten.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewseleven.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.VISIBLE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(16);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvreadmore.getRight() - (width / 2)) +
                // (tvreadmore.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewseleven.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewstwavle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.VISIBLE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(16);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvreadmore.getRight() - (width / 2)) +
                // (tvreadmore.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewstwavle.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewsthirdteen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(16);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvreadmore.getRight() - (width / 2)) +
                // (tvreadmore.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewsthirdteen.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewsfourteen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.VISIBLE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(16);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvreadmore.getRight() - (width / 2)) +
                // (tvreadmore.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewsfourteen.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    // String.valueOf(position);
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewsfifteen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.VISIBLE);
                tvstripnewssixteen.setVisibility(View.GONE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(16);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(14);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvreadmore.getRight() - (width / 2)) +
                // (tvreadmore.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewsfifteen.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);

                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        tvnewssixteen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                energylist.setVisibility(View.VISIBLE);
                llhsview.setVisibility(View.VISIBLE);

                tvstripRENEWABLE.setVisibility(View.GONE);
                tvstripPOWER.setVisibility(View.GONE);
                tvstripenergy.setVisibility(View.GONE);
                tvstripEVENT.setVisibility(View.GONE);
                tvstripCOAL.setVisibility(View.GONE);
                tvstripreadmore.setVisibility(View.GONE);
                tvstripnewsseven.setVisibility(View.GONE);
                tvstripnewseight.setVisibility(View.GONE);
                tvstripnewsnine.setVisibility(View.GONE);
                tvstripnewsten.setVisibility(View.GONE);
                tvstripnewseleven.setVisibility(View.GONE);
                tvstripnewstwavle.setVisibility(View.GONE);
                tvstripnewsthirdteen.setVisibility(View.GONE);
                tvstripnewsfourteen.setVisibility(View.GONE);
                tvstripnewsfifteen.setVisibility(View.GONE);
                tvstripnewssixteen.setVisibility(View.VISIBLE);

                tvRENEWABLE.setTypeface(null, Typeface.NORMAL);
                tvRENEWABLE.setTextSize(14);

                tvPOWER.setTypeface(null, Typeface.NORMAL);
                tvPOWER.setTextSize(14);

                tvenergy.setTypeface(null, Typeface.NORMAL);
                tvenergy.setTextSize(14);

                tvEVENT.setTypeface(null, Typeface.NORMAL);
                tvEVENT.setTextSize(14);

                tvCOAL.setTypeface(null, Typeface.NORMAL);
                tvCOAL.setTextSize(14);

                tvreadmore.setTypeface(null, Typeface.NORMAL);
                tvreadmore.setTextSize(14);

                tvnewsseven.setTypeface(null, Typeface.NORMAL);
                tvnewsseven.setTextSize(14);

                tvnewseight.setTypeface(null, Typeface.NORMAL);
                tvnewseight.setTextSize(14);

                tvnewsnine.setTypeface(null, Typeface.NORMAL);
                tvnewsnine.setTextSize(14);

                tvnewsten.setTypeface(null, Typeface.NORMAL);
                tvnewsten.setTextSize(14);

                tvnewseleven.setTypeface(null, Typeface.NORMAL);
                tvnewseleven.setTextSize(14);

                tvnewstwavle.setTypeface(null, Typeface.NORMAL);
                tvnewstwavle.setTextSize(14);

                tvnewsthirdteen.setTypeface(null, Typeface.NORMAL);
                tvnewsthirdteen.setTextSize(14);

                tvnewsfourteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfourteen.setTextSize(14);

                tvnewsfifteen.setTypeface(null, Typeface.NORMAL);
                tvnewsfifteen.setTextSize(14);

                tvnewssixteen.setTypeface(null, Typeface.NORMAL);
                tvnewssixteen.setTextSize(16);

                energylist.setVisibility(View.VISIBLE);

                // int scrollX = (tvreadmore.getRight() - (width / 2)) +
                // (tvreadmore.getWidth() / 2);
                // hsview.smoothScrollTo(scrollX, 0);
                // llhsview.addView(tvreadmore);

                String keyelementfind = (String) tvnewssixteen.getText();
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                try {
                    // Object value = mainObject.get(key);
                    JSONArray keyResult = mainObject
                            .getJSONArray(keyelementfind);
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult
                                .getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(keyelementfind)) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails
                                            .getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element
                                            .add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails
                                            .getString("title");
                                    title_key_element.add(Html.fromHtml(title)
                                            .toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails
                                            .getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails
                                            .getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails
                                            .getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails
                                            .getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails
                                            .getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    newsfeedadapter = new EnergyNewfeedCustomList(
                            getActivity(), elementiconarray_element,
                            title_key_element, pubdate_key_element,
                            time_key_element, source_key_element);
                    energylist.setAdapter(newsfeedadapter);
                    energylist
                            .setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent,
                                                        View view, final int position, long id) {
                                    Intent in = new Intent(getActivity(),
                                            NewsFeedActivitySearchWebView.class);
                                    in.putExtra("link_key_element",
                                            link_key_element.get(position));
                                    in.putExtra("LOGINCONFIG", loginconfig);
                                    startActivity(in);
                                }
                            });
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }
        });

        lltoplogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // db = openOrCreateDatabase("deviceDBSecond",
                // android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                // db.execSQL("DROP TABLE IF EXISTS baby");
                // db.execSQL("DROP TABLE IF EXISTS logininfo");
                // db.execSQL("DROP TABLE IF EXISTS companyname");
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // db = openOrCreateDatabase("deviceDBSecond",
                // android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                // db.execSQL("DROP TABLE IF EXISTS baby");
                // db.execSQL("DROP TABLE IF EXISTS logininfo");
                // db.execSQL("DROP TABLE IF EXISTS companyname");
                Intent i = new Intent(getActivity(), MainActivityFragment.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        ivsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchelement = inputSearch.getText().toString();
                if (searchelement.length() == 0) {
                    Toast.makeText(getActivity(), "Please fill the search field.", Toast.LENGTH_LONG).show();
                } else {
                    //dffg
                    //HttpAsyncTasksearchnewsfeeddetailsbykeyword().execute("http://www.mittalpower.com/mobile/pxs_app/service/searchnewsfeeddetails.php");
                    new HttpAsyncTasksearchnewsfeeddetailsbykeyword().execute(domain + "/mobile/pxs_app/service/searchnewsfeeddetails.php");
                }
            }
        });
		/*inputSearch.addTextChangedListener(new TextWatcher() {

		    @Override
		    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
		        // When user changed the Text
		       // getActivity().newsfeedadapter.getFilter().filter(cs);
		    	//newsfeedadapter.getFilter().filter(cs.toString());
		    	String text = inputSearch.getText().toString().toLowerCase(Locale.getDefault());
		    	newsfeedadapter.filter(text);
		    }

		    @Override
		    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
		            int arg3) {
		        // TODO Auto-generated method stub

		    }

		    @Override
		    public void afterTextChanged(Editable arg0) {
		        // TODO Auto-generated method stub
		    }
		});*/


        return rootView;
    }


    public void fatchdataofnewfeed() {
        Cursor newfeedsavec = db.rawQuery("select * from newfeedsave", null);
        newfeedsavec.moveToFirst();
        do {
            saveResponse = newfeedsavec.getString(newfeedsavec
                    .getColumnIndex("response"));
            // String notificationdate =
            // newfeedsavec.getString(newfeedsavec.getColumnIndex("notificationdate"));
            // Toast.makeText(getActivity(), saveResponse,
            // Toast.LENGTH_LONG).show();
        } while (newfeedsavec.moveToNext());
        try {
            mainObject = new JSONObject(saveResponse);
            key_element.clear();
            Iterator<String> iter = mainObject.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                key_element.add(key);
            }
            try {
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();
                // Object value = mainObject.get(key);
                // if(category.length() != 0){
                /*
                 * if(key_element.contains(category)){
                 * key_element.remove(category); key_element.add(category);
                 * Collections.reverse(key_element); JSONArray keyResult =
                 * mainObject.getJSONArray(category); for (int i = 0; i <
                 * keyResult.length(); i++) { JSONObject keyResulttDetails =
                 * keyResult.getJSONObject(i);
                 * if(keyResulttDetails.has("type")){ String type =
                 * keyResulttDetails.getString("type");
                 * if(type.contains(category)){ if(keyResulttDetails.has("id")){
                 * String id = keyResulttDetails.getString("id");
                 * id_key_element.add(id);
                 * elementiconarray_element.add(R.drawable.news); }
                 * if(keyResulttDetails.has("title")){ String title =
                 * keyResulttDetails.getString("title");
                 * title_key_element.add(Html.fromHtml(title).toString()); }
                 * if(keyResulttDetails.has("link")){ String link =
                 * keyResulttDetails.getString("link");
                 * link_key_element.add(link); }
                 * if(keyResulttDetails.has("comment")){ String comment =
                 * keyResulttDetails.getString("comment");
                 * comment_key_element.add(comment); }
                 * if(keyResulttDetails.has("pubdate")){ String pubdate =
                 * keyResulttDetails.getString("pubdate");
                 * pubdate_key_element.add(pubdate); }
                 * if(keyResulttDetails.has("time")){ String time =
                 * keyResulttDetails.getString("time");
                 * time_key_element.add(time); }
                 * if(keyResulttDetails.has("source")){ String source =
                 * keyResulttDetails.getString("source");
                 * source_key_element.add(source); } } } }
                 * EnergyNewfeedCustomList adapter = new
                 * EnergyNewfeedCustomList(getActivity(),
                 * elementiconarray_element, title_key_element,
                 * pubdate_key_element, time_key_element, source_key_element);
                 * energylist.setAdapter(adapter); }
                 */
                // }
                // else
                // {
                category = key_element.get(0);
                JSONArray keyResult = mainObject.getJSONArray(key_element
                        .get(0));
                for (int i = 0; i < keyResult.length(); i++) {
                    JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                    if (keyResulttDetails.has("type")) {
                        String type = keyResulttDetails.getString("type");
                        if (type.contains(key_element.get(0))) {
                            if (keyResulttDetails.has("id")) {
                                String id = keyResulttDetails.getString("id");
                                id_key_element.add(id);
                                elementiconarray_element
                                        .add(R.drawable.news);
                            }
                            if (keyResulttDetails.has("title")) {
                                String title = keyResulttDetails
                                        .getString("title");
                                title_key_element.add(Html.fromHtml(title)
                                        .toString());
                            }
                            if (keyResulttDetails.has("link")) {
                                String link = keyResulttDetails
                                        .getString("link");
                                link_key_element.add(link);
                            }
                            if (keyResulttDetails.has("comment")) {
                                String comment = keyResulttDetails
                                        .getString("comment");
                                comment_key_element.add(comment);
                            }
                            if (keyResulttDetails.has("pubdate")) {
                                String pubdate = keyResulttDetails
                                        .getString("pubdate");
                                pubdate_key_element.add(pubdate);
                            }
                            if (keyResulttDetails.has("time")) {
                                String time = keyResulttDetails
                                        .getString("time");
                                time_key_element.add(time);
                            }
                            if (keyResulttDetails.has("source")) {
                                String source = keyResulttDetails
                                        .getString("source");
                                source_key_element.add(source);
                            }
                        }
                    }
                }
                newsfeedadapter = new EnergyNewfeedCustomList(
                        getActivity(), elementiconarray_element,
                        title_key_element, pubdate_key_element,
                        time_key_element, source_key_element);
                energylist.setAdapter(newsfeedadapter);
                // }
                energylist
                        .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent,
                                                    View view, final int position, long id) {
                                Intent in = new Intent(getActivity(),
                                        NewsFeedActivitySearchWebView.class);
                                in.putExtra("link_key_element",
                                        link_key_element.get(position));
                                in.putExtra("LOGINCONFIG", loginconfig);
                                startActivity(in);

                            }
                        });

            } catch (JSONException e) {
                // Something went wrong!
            }

            // tvRENEWABLE, tvPOWER, tvenergy, tvEVENT, tvCOAL, tvstripCOAL;
            // tvnewsseven, tvnewseight, tvnewsnine, tvnewsten, tvnewseleven,
            // tvnewstwavle,
            // tvnewsthirdteen, tvnewsfourteen, tvnewsfifteen, tvnewssixteen;

            // TextView tvstripRENEWABLE, tvstripPOWER, tvstripenergy,
            // tvstripEVENT, tvreadmore, tvstripreadmore
            // tvstripnewsseven, tvstripnewseight, tvstripnewsnine,
            // tvstripnewsten, tvstripnewseleven,
            // tvstripnewstwavle, tvstripnewsthirdteen, tvstripnewsfourteen,
            // tvstripnewsfifteen, tvstripnewssixteen

            if (key_element.size() == 0 && key_element.contains(category)) {
                // tvRENEWABLE.setText(key_element.get(0));
                // tvRENEWABLE.setVisibility(View.VISIBLE);
                // tvstripRENEWABLE.setVisibility(View.VISIBLE);
            } else if (key_element.size() == 1
                    && key_element.contains(category)) {
                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvstripRENEWABLE.setVisibility(View.VISIBLE);
                if (key_element.get(0).equalsIgnoreCase("News")) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }
            } else if (key_element.size() == 2
                    && key_element.contains(category)) {
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvPOWER.setVisibility(View.VISIBLE);

                tvRENEWABLE.setText(key_element.get(0));
                tvPOWER.setText(key_element.get(1));

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 3
                    && key_element.contains(category)) {
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvPOWER.setVisibility(View.VISIBLE);
                tvenergy.setVisibility(View.VISIBLE);

                tvRENEWABLE.setText(key_element.get(0));
                tvPOWER.setText(key_element.get(1));
                tvenergy.setText(key_element.get(2));

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 4
                    && key_element.contains(category)) {
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvPOWER.setVisibility(View.VISIBLE);
                tvenergy.setVisibility(View.VISIBLE);
                tvEVENT.setVisibility(View.VISIBLE);

                tvRENEWABLE.setText(key_element.get(0));
                tvPOWER.setText(key_element.get(1));
                tvenergy.setText(key_element.get(2));
                tvEVENT.setText(key_element.get(3));

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }
            } else if (key_element.size() == 5 && key_element.contains(category)) {
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvPOWER.setVisibility(View.VISIBLE);
                tvenergy.setVisibility(View.VISIBLE);
                tvEVENT.setVisibility(View.VISIBLE);
                tvCOAL.setVisibility(View.VISIBLE);
                tvRENEWABLE.setText(key_element.get(0));
                tvPOWER.setText(key_element.get(1));
                tvenergy.setText(key_element.get(2));
                tvEVENT.setText(key_element.get(3));
                tvCOAL.setText(key_element.get(4));

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }
            } else if (key_element.size() == 6 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);
                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);
                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);
                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);
                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 7 && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 8
                    && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 9
                    && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 10
                    && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 11
                    && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                tvnewseleven.setText(key_element.get(10));
                tvnewseleven.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

                if (key_element.get(10).equalsIgnoreCase(category)) {
                    tvstripnewseleven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseleven.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 12
                    && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                tvnewseleven.setText(key_element.get(10));
                tvnewseleven.setVisibility(View.VISIBLE);

                tvnewstwavle.setText(key_element.get(11));
                tvnewstwavle.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

                if (key_element.get(10).equalsIgnoreCase(category)) {
                    tvstripnewseleven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseleven.setVisibility(View.GONE);
                }

                if (key_element.get(11).equalsIgnoreCase(category)) {
                    tvstripnewstwavle.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewstwavle.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 13
                    && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);
                tvstripRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);
                tvstripCOAL.setVisibility(View.GONE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                tvnewseleven.setText(key_element.get(10));
                tvnewseleven.setVisibility(View.VISIBLE);

                tvnewstwavle.setText(key_element.get(11));
                tvnewstwavle.setVisibility(View.VISIBLE);

                tvnewsthirdteen.setText(key_element.get(12));
                tvnewsthirdteen.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

                if (key_element.get(10).equalsIgnoreCase(category)) {
                    tvstripnewseleven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseleven.setVisibility(View.GONE);
                }

                if (key_element.get(11).equalsIgnoreCase(category)) {
                    tvstripnewstwavle.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewstwavle.setVisibility(View.GONE);
                }

                if (key_element.get(12).equalsIgnoreCase(category)) {
                    tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsthirdteen.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 14
                    && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                tvnewseleven.setText(key_element.get(10));
                tvnewseleven.setVisibility(View.VISIBLE);

                tvnewstwavle.setText(key_element.get(11));
                tvnewstwavle.setVisibility(View.VISIBLE);

                tvnewsthirdteen.setText(key_element.get(12));
                tvnewsthirdteen.setVisibility(View.VISIBLE);

                tvnewsfourteen.setText(key_element.get(13));
                tvnewsfourteen.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

                if (key_element.get(10).equalsIgnoreCase(category)) {
                    tvstripnewseleven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseleven.setVisibility(View.GONE);
                }

                if (key_element.get(11).equalsIgnoreCase(category)) {
                    tvstripnewstwavle.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewstwavle.setVisibility(View.GONE);
                }

                if (key_element.get(12).equalsIgnoreCase(category)) {
                    tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsthirdteen.setVisibility(View.GONE);
                }

                if (key_element.get(13).equalsIgnoreCase(category)) {
                    tvstripnewsfourteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsfourteen.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 15
                    && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                tvnewseleven.setText(key_element.get(10));
                tvnewseleven.setVisibility(View.VISIBLE);

                tvnewstwavle.setText(key_element.get(11));
                tvnewstwavle.setVisibility(View.VISIBLE);

                tvnewsthirdteen.setText(key_element.get(12));
                tvnewsthirdteen.setVisibility(View.VISIBLE);

                tvnewsfourteen.setText(key_element.get(13));
                tvnewsfourteen.setVisibility(View.VISIBLE);

                tvnewsfifteen.setText(key_element.get(14));
                tvnewsfifteen.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

                if (key_element.get(10).equalsIgnoreCase(category)) {
                    tvstripnewseleven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseleven.setVisibility(View.GONE);
                }

                if (key_element.get(11).equalsIgnoreCase(category)) {
                    tvstripnewstwavle.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewstwavle.setVisibility(View.GONE);
                }

                if (key_element.get(12).equalsIgnoreCase(category)) {
                    tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsthirdteen.setVisibility(View.GONE);
                }

                if (key_element.get(13).equalsIgnoreCase(category)) {
                    tvstripnewsfourteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsfourteen.setVisibility(View.GONE);
                }

                if (key_element.get(14).equalsIgnoreCase(category)) {
                    tvstripnewsfifteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsfifteen.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 16
                    && key_element.contains(category)) {

                tvRENEWABLE.setText(key_element.get(0));
                tvRENEWABLE.setVisibility(View.VISIBLE);

                tvPOWER.setText(key_element.get(1));
                tvPOWER.setVisibility(View.VISIBLE);

                tvenergy.setText(key_element.get(2));
                tvenergy.setVisibility(View.VISIBLE);

                tvEVENT.setText(key_element.get(3));
                tvEVENT.setVisibility(View.VISIBLE);

                tvCOAL.setText(key_element.get(4));
                tvCOAL.setVisibility(View.VISIBLE);

                tvreadmore.setText(key_element.get(5));
                tvreadmore.setVisibility(View.VISIBLE);

                tvnewsseven.setText(key_element.get(6));
                tvnewsseven.setVisibility(View.VISIBLE);

                tvnewseight.setText(key_element.get(7));
                tvnewseight.setVisibility(View.VISIBLE);

                tvnewsnine.setText(key_element.get(8));
                tvnewsnine.setVisibility(View.VISIBLE);

                tvnewsten.setText(key_element.get(9));
                tvnewsten.setVisibility(View.VISIBLE);

                tvnewseleven.setText(key_element.get(10));
                tvnewseleven.setVisibility(View.VISIBLE);

                tvnewstwavle.setText(key_element.get(11));
                tvnewstwavle.setVisibility(View.VISIBLE);

                tvnewsthirdteen.setText(key_element.get(12));
                tvnewsthirdteen.setVisibility(View.VISIBLE);

                tvnewsfourteen.setText(key_element.get(13));
                tvnewsfourteen.setVisibility(View.VISIBLE);

                tvnewsfifteen.setText(key_element.get(14));
                tvnewsfifteen.setVisibility(View.VISIBLE);

                tvnewssixteen.setText(key_element.get(15));
                tvnewssixteen.setVisibility(View.VISIBLE);

                if (key_element.get(0).equalsIgnoreCase(category)) {
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else {
                    tvstripRENEWABLE.setVisibility(View.GONE);
                }

                if (key_element.get(1).equalsIgnoreCase(category)) {
                    tvstripPOWER.setVisibility(View.VISIBLE);
                } else {
                    tvstripPOWER.setVisibility(View.GONE);
                }

                if (key_element.get(2).equalsIgnoreCase(category)) {
                    tvstripenergy.setVisibility(View.VISIBLE);
                } else {
                    tvstripenergy.setVisibility(View.GONE);
                }

                if (key_element.get(3).equalsIgnoreCase(category)) {
                    tvstripEVENT.setVisibility(View.VISIBLE);
                } else {
                    tvstripEVENT.setVisibility(View.GONE);
                }

                if (key_element.get(4).equalsIgnoreCase(category)) {
                    tvstripCOAL.setVisibility(View.VISIBLE);
                } else {
                    tvstripCOAL.setVisibility(View.GONE);
                }

                if (key_element.get(5).equalsIgnoreCase(category)) {
                    tvstripreadmore.setVisibility(View.VISIBLE);
                } else {
                    tvstripreadmore.setVisibility(View.GONE);
                }

                if (key_element.get(6).equalsIgnoreCase(category)) {
                    tvstripnewsseven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsseven.setVisibility(View.GONE);
                }

                if (key_element.get(7).equalsIgnoreCase(category)) {
                    tvstripnewseight.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseight.setVisibility(View.GONE);
                }

                if (key_element.get(8).equalsIgnoreCase(category)) {
                    tvstripnewsnine.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsnine.setVisibility(View.GONE);
                }

                if (key_element.get(9).equalsIgnoreCase(category)) {
                    tvstripnewsten.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsten.setVisibility(View.GONE);
                }

                if (key_element.get(10).equalsIgnoreCase(category)) {
                    tvstripnewseleven.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewseleven.setVisibility(View.GONE);
                }

                if (key_element.get(11).equalsIgnoreCase(category)) {
                    tvstripnewstwavle.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewstwavle.setVisibility(View.GONE);
                }

                if (key_element.get(12).equalsIgnoreCase(category)) {
                    tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsthirdteen.setVisibility(View.GONE);
                }

                if (key_element.get(13).equalsIgnoreCase(category)) {
                    tvstripnewsfourteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsfourteen.setVisibility(View.GONE);
                }

                if (key_element.get(14).equalsIgnoreCase(category)) {
                    tvstripnewsfifteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewsfifteen.setVisibility(View.GONE);
                }

                if (key_element.get(15).equalsIgnoreCase(category)) {
                    tvstripnewssixteen.setVisibility(View.VISIBLE);
                } else {
                    tvstripnewssixteen.setVisibility(View.GONE);
                }

            } else if (key_element.size() == 17) {

            } else if (key_element.size() == 18) {

            } else if (key_element.size() == 19) {

            } else {
                Toast.makeText(getActivity(), "Something is wrong!",
                        Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            Toast.makeText(getActivity(),
                    "500 Internal Server Error, Please Check your network.",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP
                        && keyCode == KeyEvent.KEYCODE_BACK) {
                    Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                    startActivity(in);
                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);

                    return true;

                }

                return false;
            }
        });
    }

    //HttpAsyncTasksearchnewsfeeddetails
    private class HttpAsyncTasksearchnewsfeeddetailsbykeyword extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            SearchElement searchElement = new SearchElement();
            searchElement.setKeyword(searchelement);
            searchElement.setDeviceid(device_id);
            return SearchElementAPIResponse.POST(urls[0], searchElement);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //Toast.makeText(getBaseContext(), "Received!"+result, Toast.LENGTH_LONG).show();
            try {
                if (result.contains("'s")) {
                    result = result.replace("'s", "");
                }
                if (result.contains("'")) {
                    result = result.replace("'", "");
                }
				/*if(result.contains(",")){
					result = result.replace(",", " ");
				}*/

                //result = DatabaseUtils.sqlEscapeString(result);
                //db.execSQL("DROP TABLE IF EXISTS newfeedsave");
                //db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
                //db.execSQL("INSERT INTO newfeedsave VALUES('"+ result +"', '" + result + "', '" + result + "')");
                mainObject = new JSONObject(result);
                hsview.setVisibility(View.VISIBLE);
                energylist.setVisibility(View.VISIBLE);
                key_element.clear();
                Iterator<String> iter = mainObject.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    key_element.add(key);
                }
                try {
                    elementiconarray_element.clear();
                    id_key_element.clear();
                    title_key_element.clear();
                    type_key_element.clear();
                    link_key_element.clear();
                    comment_key_element.clear();
                    pubdate_key_element.clear();
                    time_key_element.clear();
                    source_key_element.clear();
                    // Object value = mainObject.get(key);
                    //if(category.length() != 0){
					/*
						}
						if(key_element.contains(category)){
						key_element.remove(category);
						key_element.add(category);
						Collections.reverse(key_element);
				    	JSONArray keyResult = mainObject.getJSONArray(category);
						for (int i = 0; i < keyResult.length(); i++) {
							JSONObject keyResulttDetails = keyResult.getJSONObject(i);
							if(keyResulttDetails.has("type")){
								String type = keyResulttDetails.getString("type");
								if(type.contains(category)){
									if(keyResulttDetails.has("id")){
										String id = keyResulttDetails.getString("id");
										id_key_element.add(id);
										elementiconarray_element.add(R.drawable.news);
									}
									if(keyResulttDetails.has("title")){
										String title = keyResulttDetails.getString("title");
										title_key_element.add(Html.fromHtml(title).toString());
									}
									if(keyResulttDetails.has("link")){
										String link = keyResulttDetails.getString("link");
										link_key_element.add(link);
									}
									if(keyResulttDetails.has("comment")){
										String comment = keyResulttDetails.getString("comment");
										comment_key_element.add(comment);
									}
									if(keyResulttDetails.has("pubdate")){
										String pubdate = keyResulttDetails.getString("pubdate");
										pubdate_key_element.add(pubdate);
									}
									if(keyResulttDetails.has("time")){
										String time = keyResulttDetails.getString("time");
										time_key_element.add(time);
									}
									if(keyResulttDetails.has("source")){
										String source = keyResulttDetails.getString("source");
										source_key_element.add(source);
									}
								}
							}
						}
						EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(NewsFeedActivity.this, elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
					    energylist.setAdapter(adapter);
						}
					 */
					/*}
						else
						{*/
                    category = key_element.get(0);
                    JSONArray keyResult = mainObject.getJSONArray(key_element.get(0));
                    for (int i = 0; i < keyResult.length(); i++) {
                        JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                        if (keyResulttDetails.has("type")) {
                            String type = keyResulttDetails.getString("type");
                            if (type.contains(key_element.get(0))) {
                                if (keyResulttDetails.has("id")) {
                                    String id = keyResulttDetails.getString("id");
                                    id_key_element.add(id);
                                    elementiconarray_element.add(R.drawable.news);
                                }
                                if (keyResulttDetails.has("title")) {
                                    String title = keyResulttDetails.getString("title");
                                    title_key_element.add(Html.fromHtml(title).toString());
                                }
                                if (keyResulttDetails.has("link")) {
                                    String link = keyResulttDetails.getString("link");
                                    link_key_element.add(link);
                                }
                                if (keyResulttDetails.has("comment")) {
                                    String comment = keyResulttDetails.getString("comment");
                                    comment_key_element.add(comment);
                                }
                                if (keyResulttDetails.has("pubdate")) {
                                    String pubdate = keyResulttDetails.getString("pubdate");
                                    pubdate_key_element.add(pubdate);
                                }
                                if (keyResulttDetails.has("time")) {
                                    String time = keyResulttDetails.getString("time");
                                    time_key_element.add(time);
                                }
                                if (keyResulttDetails.has("source")) {
                                    String source = keyResulttDetails.getString("source");
                                    source_key_element.add(source);
                                }
                            }
                        }
                    }
                    EnergyNewfeedCustomList adapter = new EnergyNewfeedCustomList(getActivity(), elementiconarray_element, title_key_element, pubdate_key_element, time_key_element, source_key_element);
                    energylist.setAdapter(adapter);
                    // }
                    energylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            //Toast.makeText(NewsFeedActivity.this,"You Clicked at " + link_key_element.get(position),Toast.LENGTH_SHORT).show();
                            //Toast.makeText(NewsFeedActivity.this,"You Clicked at " + comment_key_element.get(position),Toast.LENGTH_SHORT).show();

                            //String indexid = String.valueOf(position);
                            Intent in = new Intent(getActivity(), NewsFeedActivitySearchWebView.class);
                            in.putExtra("link_key_element", link_key_element.get(position));
                            in.putExtra("LOGINCONFIG", loginconfig);
                            startActivity(in);

                        }
                    });


                } catch (JSONException e) {
                    // Something went wrong!
                    // Something went wrong!
                    //{"status":"ERR","message":"Invalid Json.","value":"-1"}
                    String status = "", message = "";
                    if (mainObject.has("status")) {
                        status = mainObject.getString("status");

                    }
                    if (mainObject.has("message")) {
                        message = mainObject.getString("message");
                    }

                    if (status.equalsIgnoreCase("ERR")) {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        hsview.setVisibility(View.GONE);
                        energylist.setVisibility(View.GONE);
                    }
                }

                //tvRENEWABLE, tvPOWER, tvenergy, tvEVENT, tvCOAL, tvstripCOAL;
                // tvnewsseven, tvnewseight, tvnewsnine, tvnewsten, tvnewseleven, tvnewstwavle,
                //	 tvnewsthirdteen, tvnewsfourteen, tvnewsfifteen, tvnewssixteen;


                //TextView tvstripRENEWABLE, tvstripPOWER, tvstripenergy, tvstripEVENT, tvreadmore, tvstripreadmore
                //  tvstripnewsseven, tvstripnewseight, tvstripnewsnine, tvstripnewsten, tvstripnewseleven,
                //	 tvstripnewstwavle, tvstripnewsthirdteen, tvstripnewsfourteen, tvstripnewsfifteen, tvstripnewssixteen


                if (key_element.size() == 0 && key_element.contains(category)) {
                    //tvRENEWABLE.setText(key_element.get(0));
                    //tvRENEWABLE.setVisibility(View.VISIBLE);
                    //tvstripRENEWABLE.setVisibility(View.VISIBLE);
                } else if (key_element.size() == 1 && key_element.contains(category)) {
                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    if (key_element.get(0).equalsIgnoreCase("News")) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }
                } else if (key_element.size() == 2 && key_element.contains(category)) {
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvRENEWABLE.setText(key_element.get(0));
                    tvPOWER.setText(key_element.get(1));

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 3 && key_element.contains(category)) {
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvPOWER.setVisibility(View.VISIBLE);
                    tvenergy.setVisibility(View.VISIBLE);

                    tvRENEWABLE.setText(key_element.get(0));
                    tvPOWER.setText(key_element.get(1));
                    tvenergy.setText(key_element.get(2));

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 4 && key_element.contains(category)) {
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvPOWER.setVisibility(View.VISIBLE);
                    tvenergy.setVisibility(View.VISIBLE);
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvRENEWABLE.setText(key_element.get(0));
                    tvPOWER.setText(key_element.get(1));
                    tvenergy.setText(key_element.get(2));
                    tvEVENT.setText(key_element.get(3));

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }
                } else if (key_element.size() == 5 && key_element.contains(category)) {
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvPOWER.setVisibility(View.VISIBLE);
                    tvenergy.setVisibility(View.VISIBLE);
                    tvEVENT.setVisibility(View.VISIBLE);
                    tvCOAL.setVisibility(View.VISIBLE);
                    tvRENEWABLE.setText(key_element.get(0));
                    tvPOWER.setText(key_element.get(1));
                    tvenergy.setText(key_element.get(2));
                    tvEVENT.setText(key_element.get(3));
                    tvCOAL.setText(key_element.get(4));

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }
                } else if (key_element.size() == 6 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);
                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);
                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);
                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);
                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 7 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 8 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }


                } else if (key_element.size() == 9 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }


                } else if (key_element.size() == 10 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);


                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 11 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);

                    tvnewseleven.setText(key_element.get(10));
                    tvnewseleven.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                    if (key_element.get(10).equalsIgnoreCase(category)) {
                        tvstripnewseleven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseleven.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 12 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);

                    tvnewseleven.setText(key_element.get(10));
                    tvnewseleven.setVisibility(View.VISIBLE);

                    tvnewstwavle.setText(key_element.get(11));
                    tvnewstwavle.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                    if (key_element.get(10).equalsIgnoreCase(category)) {
                        tvstripnewseleven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseleven.setVisibility(View.GONE);
                    }

                    if (key_element.get(11).equalsIgnoreCase(category)) {
                        tvstripnewstwavle.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewstwavle.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 13 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);
                    tvstripRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);
                    tvstripCOAL.setVisibility(View.GONE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);

                    tvnewseleven.setText(key_element.get(10));
                    tvnewseleven.setVisibility(View.VISIBLE);

                    tvnewstwavle.setText(key_element.get(11));
                    tvnewstwavle.setVisibility(View.VISIBLE);

                    tvnewsthirdteen.setText(key_element.get(12));
                    tvnewsthirdteen.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                    if (key_element.get(10).equalsIgnoreCase(category)) {
                        tvstripnewseleven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseleven.setVisibility(View.GONE);
                    }

                    if (key_element.get(11).equalsIgnoreCase(category)) {
                        tvstripnewstwavle.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewstwavle.setVisibility(View.GONE);
                    }

                    if (key_element.get(12).equalsIgnoreCase(category)) {
                        tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsthirdteen.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 14 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);

                    tvnewseleven.setText(key_element.get(10));
                    tvnewseleven.setVisibility(View.VISIBLE);

                    tvnewstwavle.setText(key_element.get(11));
                    tvnewstwavle.setVisibility(View.VISIBLE);

                    tvnewsthirdteen.setText(key_element.get(12));
                    tvnewsthirdteen.setVisibility(View.VISIBLE);

                    tvnewsfourteen.setText(key_element.get(13));
                    tvnewsfourteen.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                    if (key_element.get(10).equalsIgnoreCase(category)) {
                        tvstripnewseleven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseleven.setVisibility(View.GONE);
                    }

                    if (key_element.get(11).equalsIgnoreCase(category)) {
                        tvstripnewstwavle.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewstwavle.setVisibility(View.GONE);
                    }

                    if (key_element.get(12).equalsIgnoreCase(category)) {
                        tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsthirdteen.setVisibility(View.GONE);
                    }

                    if (key_element.get(13).equalsIgnoreCase(category)) {
                        tvstripnewsfourteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsfourteen.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 15 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);

                    tvnewseleven.setText(key_element.get(10));
                    tvnewseleven.setVisibility(View.VISIBLE);

                    tvnewstwavle.setText(key_element.get(11));
                    tvnewstwavle.setVisibility(View.VISIBLE);

                    tvnewsthirdteen.setText(key_element.get(12));
                    tvnewsthirdteen.setVisibility(View.VISIBLE);

                    tvnewsfourteen.setText(key_element.get(13));
                    tvnewsfourteen.setVisibility(View.VISIBLE);

                    tvnewsfifteen.setText(key_element.get(14));
                    tvnewsfifteen.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                    if (key_element.get(10).equalsIgnoreCase(category)) {
                        tvstripnewseleven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseleven.setVisibility(View.GONE);
                    }

                    if (key_element.get(11).equalsIgnoreCase(category)) {
                        tvstripnewstwavle.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewstwavle.setVisibility(View.GONE);
                    }

                    if (key_element.get(12).equalsIgnoreCase(category)) {
                        tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsthirdteen.setVisibility(View.GONE);
                    }

                    if (key_element.get(13).equalsIgnoreCase(category)) {
                        tvstripnewsfourteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsfourteen.setVisibility(View.GONE);
                    }

                    if (key_element.get(14).equalsIgnoreCase(category)) {
                        tvstripnewsfifteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsfifteen.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 16 && key_element.contains(category)) {

                    tvRENEWABLE.setText(key_element.get(0));
                    tvRENEWABLE.setVisibility(View.VISIBLE);

                    tvPOWER.setText(key_element.get(1));
                    tvPOWER.setVisibility(View.VISIBLE);

                    tvenergy.setText(key_element.get(2));
                    tvenergy.setVisibility(View.VISIBLE);

                    tvEVENT.setText(key_element.get(3));
                    tvEVENT.setVisibility(View.VISIBLE);

                    tvCOAL.setText(key_element.get(4));
                    tvCOAL.setVisibility(View.VISIBLE);

                    tvreadmore.setText(key_element.get(5));
                    tvreadmore.setVisibility(View.VISIBLE);

                    tvnewsseven.setText(key_element.get(6));
                    tvnewsseven.setVisibility(View.VISIBLE);

                    tvnewseight.setText(key_element.get(7));
                    tvnewseight.setVisibility(View.VISIBLE);

                    tvnewsnine.setText(key_element.get(8));
                    tvnewsnine.setVisibility(View.VISIBLE);

                    tvnewsten.setText(key_element.get(9));
                    tvnewsten.setVisibility(View.VISIBLE);

                    tvnewseleven.setText(key_element.get(10));
                    tvnewseleven.setVisibility(View.VISIBLE);

                    tvnewstwavle.setText(key_element.get(11));
                    tvnewstwavle.setVisibility(View.VISIBLE);

                    tvnewsthirdteen.setText(key_element.get(12));
                    tvnewsthirdteen.setVisibility(View.VISIBLE);

                    tvnewsfourteen.setText(key_element.get(13));
                    tvnewsfourteen.setVisibility(View.VISIBLE);

                    tvnewsfifteen.setText(key_element.get(14));
                    tvnewsfifteen.setVisibility(View.VISIBLE);

                    tvnewssixteen.setText(key_element.get(15));
                    tvnewssixteen.setVisibility(View.VISIBLE);

                    if (key_element.get(0).equalsIgnoreCase(category)) {
                        tvstripRENEWABLE.setVisibility(View.VISIBLE);
                    } else {
                        tvstripRENEWABLE.setVisibility(View.GONE);
                    }

                    if (key_element.get(1).equalsIgnoreCase(category)) {
                        tvstripPOWER.setVisibility(View.VISIBLE);
                    } else {
                        tvstripPOWER.setVisibility(View.GONE);
                    }

                    if (key_element.get(2).equalsIgnoreCase(category)) {
                        tvstripenergy.setVisibility(View.VISIBLE);
                    } else {
                        tvstripenergy.setVisibility(View.GONE);
                    }

                    if (key_element.get(3).equalsIgnoreCase(category)) {
                        tvstripEVENT.setVisibility(View.VISIBLE);
                    } else {
                        tvstripEVENT.setVisibility(View.GONE);
                    }

                    if (key_element.get(4).equalsIgnoreCase(category)) {
                        tvstripCOAL.setVisibility(View.VISIBLE);
                    } else {
                        tvstripCOAL.setVisibility(View.GONE);
                    }

                    if (key_element.get(5).equalsIgnoreCase(category)) {
                        tvstripreadmore.setVisibility(View.VISIBLE);
                    } else {
                        tvstripreadmore.setVisibility(View.GONE);
                    }

                    if (key_element.get(6).equalsIgnoreCase(category)) {
                        tvstripnewsseven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsseven.setVisibility(View.GONE);
                    }

                    if (key_element.get(7).equalsIgnoreCase(category)) {
                        tvstripnewseight.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseight.setVisibility(View.GONE);
                    }

                    if (key_element.get(8).equalsIgnoreCase(category)) {
                        tvstripnewsnine.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsnine.setVisibility(View.GONE);
                    }

                    if (key_element.get(9).equalsIgnoreCase(category)) {
                        tvstripnewsten.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsten.setVisibility(View.GONE);
                    }

                    if (key_element.get(10).equalsIgnoreCase(category)) {
                        tvstripnewseleven.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewseleven.setVisibility(View.GONE);
                    }

                    if (key_element.get(11).equalsIgnoreCase(category)) {
                        tvstripnewstwavle.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewstwavle.setVisibility(View.GONE);
                    }

                    if (key_element.get(12).equalsIgnoreCase(category)) {
                        tvstripnewsthirdteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsthirdteen.setVisibility(View.GONE);
                    }

                    if (key_element.get(13).equalsIgnoreCase(category)) {
                        tvstripnewsfourteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsfourteen.setVisibility(View.GONE);
                    }

                    if (key_element.get(14).equalsIgnoreCase(category)) {
                        tvstripnewsfifteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewsfifteen.setVisibility(View.GONE);
                    }

                    if (key_element.get(15).equalsIgnoreCase(category)) {
                        tvstripnewssixteen.setVisibility(View.VISIBLE);
                    } else {
                        tvstripnewssixteen.setVisibility(View.GONE);
                    }

                } else if (key_element.size() == 17) {

                } else if (key_element.size() == 18) {

                } else if (key_element.size() == 19) {

                } else {
                    Toast.makeText(getActivity(), "Something is wrong!", Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String errormass = mainObject.getString("message");
                    Toast.makeText(getActivity(), errormass, Toast.LENGTH_LONG).show();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    try {
                        JSONArray jsonArray = new JSONArray(result);
                        if (jsonArray.length() == 0) {
                            Toast.makeText(getActivity(), "No Data found.", Toast.LENGTH_LONG).show();
                            hsview.setVisibility(View.GONE);
                            energylist.setVisibility(View.GONE);
                        }
                    } catch (JSONException e2) {
                        // TODO Auto-generated catch block
                        e2.printStackTrace();
                    }
                    e1.printStackTrace();
                }
                if (result != null) {
                    //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }


}
