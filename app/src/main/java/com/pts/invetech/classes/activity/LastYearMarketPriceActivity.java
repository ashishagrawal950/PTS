package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.pts.invetech.R;
import com.pts.invetech.customlist.LastYearMarketPriceCustomList;
import com.pts.invetech.customscrollview.widget.NonScrollListView;

import java.util.ArrayList;

public class LastYearMarketPriceActivity extends Activity {

    public boolean currentisTrue = true;
    public boolean previousisTrue = true;
    public boolean lastyearisTrue = true;
    public boolean secondlastyearisTrue = true;
    public boolean isTrue = true;
    private LinearLayout mopenLayout, chart, lliex, llpxil, lltransaction, mopenPopup;
    private Context context;
    private SQLiteDatabase db;
    private LineChart lineChart;
    private String loginconfig;
    private ArrayList<String> currentyear = new ArrayList<String>();
    private ArrayList<String> lastyear = new ArrayList<String>();
    private ArrayList<String> secondlastyear = new ArrayList<String>();
    private ArrayList<String> blockarrayListfrom = new ArrayList<String>();
    private ArrayList<String> date_Array = new ArrayList<String>();
    private ArrayList<String> previousday = new ArrayList<String>();
    private LinearLayout llgraphcurrent, llgraphlast, llgraphsecondlast, llgraphpreviours;
    private TextView tvcurrenttext, tvcurrentline, tvlasttext, tvlastline, tvsecondlasttext, tvsecondlastline, tvreviourstext, tvrevioursline;
    private String revision_get, date_get;
    private ImageView tvback, tvbackagain;
    private TextView tvcurrenttable, tvlastyeartable, tvsecondlasttable, tvprevioustable, tvblock;
    private NonScrollListView marketpicelist;
    private LinearLayout lltableheading, llaaboveheading, tvshowintabe;
    private LinearLayout llgraphview, lltableview;
    private boolean flag = true;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_lastyearmarketprice);

        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);

        tvback = (ImageView) findViewById(R.id.tvback);
        lineChart = (LineChart) findViewById(R.id.navchart);
        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);

        Intent in = getIntent();
        currentyear = in.getStringArrayListExtra("currentyear");
        lastyear = in.getStringArrayListExtra("lastyear");
        secondlastyear = in.getStringArrayListExtra("secondlastyear");
        previousday = in.getStringArrayListExtra("previousday");
        date_Array = in.getStringArrayListExtra("date_Array");
        loginconfig = in.getStringExtra("LOGINCONFIG");


        tvback.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag) {
                    tvback.setEnabled(false);
                    flag = false;
//					Intent in = new Intent(LastYearMarketPriceActivity.this, NewFeedMainActivity.class);
//					in.putExtra("LOGINCONFIG",loginconfig);
//					startActivity(in);
//					overridePendingTransition(R.anim.animation, R.anim.animation2);
                    finish();
                }
            }
        });

        llgraphcurrent = (LinearLayout) findViewById(R.id.llgraphcurrent);
        tvcurrenttext = (TextView) findViewById(R.id.tvcurrenttext);
        tvcurrenttext.setText(date_Array.get(0));
        tvcurrentline = (TextView) findViewById(R.id.tvcurrentline);

        llgraphpreviours = (LinearLayout) findViewById(R.id.llgraphpreviours);
        tvreviourstext = (TextView) findViewById(R.id.tvreviourstext);
        tvreviourstext.setText(date_Array.get(1));
        tvrevioursline = (TextView) findViewById(R.id.tvrevioursline);

        llgraphlast = (LinearLayout) findViewById(R.id.llgraphlast);
        tvlasttext = (TextView) findViewById(R.id.tvlasttext);
        tvlasttext.setText(date_Array.get(2));
        tvlastline = (TextView) findViewById(R.id.tvlastline);

        llgraphsecondlast = (LinearLayout) findViewById(R.id.llgraphsecondlast);
        tvsecondlasttext = (TextView) findViewById(R.id.tvsecondlasttext);
        tvsecondlasttext.setText(date_Array.get(3));
        tvsecondlastline = (TextView) findViewById(R.id.tvsecondlastline);

        tvshowintabe = (LinearLayout) findViewById(R.id.tvshowintabe);
        //tvshowintabe.setPaintFlags(tvshowintabe.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        lltableheading = (LinearLayout) findViewById(R.id.lltableheading);
        tvcurrenttable = (TextView) findViewById(R.id.tvcurrenttable);
        tvcurrenttable.setText(date_Array.get(0));
        tvprevioustable = (TextView) findViewById(R.id.tvprevioustable);
        tvprevioustable.setText(date_Array.get(1));
        tvlastyeartable = (TextView) findViewById(R.id.tvlastyeartable);
        tvlastyeartable.setText(date_Array.get(2));
        tvsecondlasttable = (TextView) findViewById(R.id.tvsecondlasttable);
        tvsecondlasttable.setText(date_Array.get(3));

        llaaboveheading = (LinearLayout) findViewById(R.id.llaaboveheading);

        tvblock = (TextView) findViewById(R.id.tvblock);

        llgraphview = (LinearLayout) findViewById(R.id.llgraphview);
        lltableview = (LinearLayout) findViewById(R.id.lltableview);
        tvbackagain = (ImageView) findViewById(R.id.tvbackagain);

        drawChartNav();
        marketpicelist = (NonScrollListView) findViewById(R.id.marketpicelist);
        LastYearMarketPriceCustomList adapter = new LastYearMarketPriceCustomList(
                LastYearMarketPriceActivity.this, blockarrayListfrom, currentyear, lastyear, secondlastyear, previousday);
        marketpicelist.setAdapter(adapter);

        llgraphcurrent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (currentisTrue == true) {
                    currentisTrue = false;
                    tvcurrenttext.setTextColor(Color.GRAY);
                    tvcurrentline.setBackgroundColor(Color.GRAY);
                } else {
                    currentisTrue = true;
                    tvcurrenttext.setTextColor(Color.MAGENTA);
                    tvcurrentline.setBackgroundColor(Color.MAGENTA);
                }
                drawChartNav();

            }
        });


        //llgraphpreviours, tvreviourstext, tvrevioursline
        llgraphpreviours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (previousisTrue == true) {
                    previousisTrue = false;
                    tvreviourstext.setTextColor(Color.GRAY);
                    tvrevioursline.setBackgroundColor(Color.GRAY);
                } else {
                    previousisTrue = true;
                    tvreviourstext.setTextColor(Color.BLUE);
                    tvrevioursline.setBackgroundColor(Color.BLUE);
                }
                drawChartNav();

            }
        });


        llgraphlast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (lastyearisTrue == true) {
                    lastyearisTrue = false;
                    tvlasttext.setTextColor(Color.GRAY);
                    tvlastline.setBackgroundColor(Color.GRAY);
                } else {
                    lastyearisTrue = true;
                    tvlasttext.setTextColor(Color.CYAN);
                    tvlastline.setBackgroundColor(Color.CYAN);
                }
                drawChartNav();

            }
        });


        llgraphsecondlast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (secondlastyearisTrue == true) {
                    secondlastyearisTrue = false;
                    tvsecondlasttext.setTextColor(Color.GRAY);
                    tvsecondlastline.setBackgroundColor(Color.GRAY);
                } else {
                    secondlastyearisTrue = true;
                    tvsecondlasttext.setTextColor(Color.RED);
                    tvsecondlastline.setBackgroundColor(Color.RED);
                }
                drawChartNav();
            }
        });

        //
        tvshowintabe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(LastYearMarketPriceActivity.this, LastYearMarketPriceActivityTableView.class);
                in.putStringArrayListExtra("currentyear", currentyear);
                in.putStringArrayListExtra("lastyear", lastyear);
                in.putStringArrayListExtra("secondlastyear", secondlastyear);
                in.putExtra("LOGINCONFIG", loginconfig);
                in.putStringArrayListExtra("date_Array", date_Array);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);

            }
        });

    }

    private void drawChartNav() {
        // creating list of entry
        blockarrayListfrom.clear();
        for (int i = 1; i <= 96; i++) {
            String numberAsString = Integer.toString(i);
            blockarrayListfrom.add(numberAsString);
        }

        ArrayList<Entry> entriescurrent = new ArrayList<>();
        for (int min = 0; min < currentyear.size(); min++) {
            entriescurrent.add(new Entry(Float.parseFloat(currentyear.get(min)), min));
        }
        LineDataSet datasetcurrent = new LineDataSet(entriescurrent, "");
        datasetcurrent.setCircleColor(Color.MAGENTA);
        datasetcurrent.setColor(Color.MAGENTA);
        datasetcurrent.setValueTextColor(Color.MAGENTA);
        datasetcurrent.setCircleSize(3f);


        // creating list of entry
        ArrayList<Entry> entriespreviours = new ArrayList<>();
        for (int min = 0; min < previousday.size(); min++) {
            entriespreviours.add(new Entry(Float.parseFloat(previousday.get(min)), min));
        }
        LineDataSet datasetprevoursday = new LineDataSet(entriespreviours, "");
        datasetprevoursday.setCircleColor(Color.BLUE);
        datasetprevoursday.setColor(Color.BLUE);
        datasetprevoursday.setValueTextColor(Color.BLUE);
        datasetprevoursday.setCircleSize(3f);

        // creating list of entry
        ArrayList<Entry> entrieslast = new ArrayList<>();
        for (int min = 0; min < lastyear.size(); min++) {
            entrieslast.add(new Entry(Float.parseFloat(lastyear.get(min)), min));
        }
        LineDataSet datasetlast = new LineDataSet(entrieslast, "");
        datasetlast.setCircleColor(Color.CYAN);
        datasetlast.setColor(Color.CYAN);
        datasetlast.setValueTextColor(Color.CYAN);
        datasetlast.setCircleSize(3f);

        // creating list of entry 
        ArrayList<Entry> entriessecondlastyear = new ArrayList<>();
        for (int min = 0; min < secondlastyear.size(); min++) {
            entriessecondlastyear.add(new Entry(Float.parseFloat(secondlastyear.get(min)), min));
        }
        LineDataSet datasetsecondlastyear = new LineDataSet(entriessecondlastyear, "");
        datasetsecondlastyear.setCircleColor(Color.RED);
        datasetsecondlastyear.setColor(Color.RED);
        datasetsecondlastyear.setValueTextColor(Color.RED);
        datasetsecondlastyear.setCircleSize(3f);


        if (currentisTrue == false && previousisTrue == false && lastyearisTrue == false && secondlastyearisTrue == false) {
            //Toast.makeText(MarketPriceActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            tvcurrenttext.setTextColor(Color.MAGENTA);
            tvcurrentline.setBackgroundColor(Color.MAGENTA);

            tvreviourstext.setTextColor(Color.BLUE);
            tvrevioursline.setBackgroundColor(Color.BLUE);

            tvlasttext.setTextColor(Color.CYAN);
            tvlastline.setBackgroundColor(Color.CYAN);

            tvsecondlasttext.setTextColor(Color.RED);
            tvsecondlastline.setBackgroundColor(Color.RED);
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetcurrent);
            dataSets.add(datasetprevoursday);
            dataSets.add(datasetlast);
            dataSets.add(datasetsecondlastyear);

            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
            currentisTrue = true;
            previousisTrue = true;
            lastyearisTrue = true;
            secondlastyearisTrue = true;
        } else if (currentisTrue == false && previousisTrue == false && lastyearisTrue == false && secondlastyearisTrue == true) {
            //iexonlymaxdrawChart();
            LineData datamax = new LineData(blockarrayListfrom, datasetsecondlastyear);
            lineChart.setData(datamax); // set the data and list of lables
            lineChart.animateY(3000);
        } else if (currentisTrue == false && previousisTrue == false && lastyearisTrue == true && secondlastyearisTrue == false) {
            //iexonlyavgdrawChart();
            LineData dataavg = new LineData(blockarrayListfrom, datasetlast);
            lineChart.setData(dataavg); // set the data and list of lables
            lineChart.animateY(3000);
        } else if (currentisTrue == false && previousisTrue == false && lastyearisTrue == true && secondlastyearisTrue == true) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetlast);
            dataSets.add(datasetsecondlastyear);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == false && previousisTrue == true && lastyearisTrue == false && secondlastyearisTrue == false) {
            //iexonlymindrawChart();
            LineData datamin = new LineData(blockarrayListfrom, datasetprevoursday);
            lineChart.setData(datamin); // set the data and list of lables
            lineChart.animateY(3000);
        } else if (currentisTrue == false && previousisTrue == true && lastyearisTrue == false && secondlastyearisTrue == true) {
            //iexavgdrawChartMixMAx();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetprevoursday);
            dataSets.add(datasetsecondlastyear);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == false && previousisTrue == true && lastyearisTrue == true && secondlastyearisTrue == false) {
            //iexmaxdrawChartMinAvg();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetprevoursday);
            dataSets.add(datasetlast);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == false && previousisTrue == true && lastyearisTrue == true && secondlastyearisTrue == true) {
            //Toast.makeText(MarketPriceActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetprevoursday);
            dataSets.add(datasetlast);
            dataSets.add(datasetsecondlastyear);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == false && previousisTrue == false && lastyearisTrue == true && secondlastyearisTrue == true) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetlast);
            dataSets.add(datasetsecondlastyear);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == true && previousisTrue == false && lastyearisTrue == false && secondlastyearisTrue == false) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetcurrent);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == true && previousisTrue == false && lastyearisTrue == false && secondlastyearisTrue == true) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetcurrent);
            dataSets.add(datasetsecondlastyear);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == true && previousisTrue == false && lastyearisTrue == true && secondlastyearisTrue == false) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetcurrent);
            dataSets.add(datasetlast);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == false && previousisTrue == false && lastyearisTrue == true && secondlastyearisTrue == true) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetlast);
            dataSets.add(datasetsecondlastyear);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == true && previousisTrue == false && lastyearisTrue == true && secondlastyearisTrue == true) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetcurrent);
            dataSets.add(datasetlast);
            dataSets.add(datasetsecondlastyear);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == true && previousisTrue == true && lastyearisTrue == false && secondlastyearisTrue == false) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetcurrent);
            dataSets.add(datasetprevoursday);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == true && previousisTrue == true && lastyearisTrue == false && secondlastyearisTrue == true) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetlast);
            dataSets.add(datasetsecondlastyear);
            dataSets.add(datasetsecondlastyear);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == true && previousisTrue == true && lastyearisTrue == true && secondlastyearisTrue == false) {
            //iexmindrawChartAvgMax();
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetcurrent);
            dataSets.add(datasetprevoursday);
            dataSets.add(datasetlast);
            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
        } else if (currentisTrue == true && previousisTrue == true && lastyearisTrue == true && secondlastyearisTrue == true) {
            //Toast.makeText(MarketPriceActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            tvcurrenttext.setTextColor(Color.MAGENTA);
            tvcurrentline.setBackgroundColor(Color.MAGENTA);

            tvreviourstext.setTextColor(Color.BLUE);
            tvrevioursline.setBackgroundColor(Color.BLUE);

            tvlasttext.setTextColor(Color.CYAN);
            tvlastline.setBackgroundColor(Color.CYAN);

            tvsecondlasttext.setTextColor(Color.RED);
            tvsecondlastline.setBackgroundColor(Color.RED);
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(datasetcurrent);
            dataSets.add(datasetprevoursday);
            dataSets.add(datasetlast);
            dataSets.add(datasetsecondlastyear);

            LineData data = new LineData(blockarrayListfrom, dataSets);
            lineChart.setData(data); // set the data and list of lables into
            lineChart.animateY(3000);
            currentisTrue = true;
            previousisTrue = true;
            lastyearisTrue = true;
            secondlastyearisTrue = true;
        }


        lineChart.setDescription("Values in Rupees");  // set the description
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(4f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);
        lineChart.setDrawGridBackground(false);
    }


    public void showMessage(String title, String message) {
        Builder builder = new Builder(LastYearMarketPriceActivity.this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }


    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(LastYearMarketPriceActivity.this, NewFeedMainActivity.class);
        in.putExtra("LOGINCONFIG", loginconfig);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }


}