package com.pts.invetech.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.myscheduling.GetSaveScheduleSetup;
import com.pts.invetech.myscheduling.GetSaveScheduleSetupDatum;

/**
 * Created by Ashish Karn on 20-03-2018.
 */

public class AdapterMyScheduleSetUpList  extends BaseAdapter {

    private GetSaveScheduleSetup scheduleList;
    private View.OnClickListener clickListener;
    private Context mContext;

    public AdapterMyScheduleSetUpList(Context _context,
                                      GetSaveScheduleSetup _scheduleList,
                                      View.OnClickListener loadDemandClose) {

        scheduleList = _scheduleList;
        clickListener=loadDemandClose;
        mContext=_context;
    }


    @Override
    public int getCount() {
        return scheduleList.getData().size();
    }

    @Override
    public GetSaveScheduleSetupDatum getItem(int i) {
        return scheduleList.getData().get(i);
    }

    @Override
    public long getItemId(int i) {
        return scheduleList.getData().get(i).hashCode();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        SetUpViewHolder setViewHolder;

        if (view == null) {
            view = LayoutInflater.from(mContext)
                    .inflate(R.layout.listitem_myschedule_setup, null);

            setViewHolder=new SetUpViewHolder(view,clickListener);

            view.setTag(setViewHolder);
        }else{
            setViewHolder= (SetUpViewHolder) view.getTag();
        }

        GetSaveScheduleSetupDatum tmp = getItem(i);


        int serno = i + 1;
        setViewHolder.tv1.setText(String.valueOf(serno));
        if (tmp.getCode() != null) {
            setViewHolder.tv2.setText(tmp.getCode());
        } else {
            setViewHolder.tv2.setText("NA");
        }
        if (tmp.getRegion() != null) {
            setViewHolder.tv3.setText(tmp.getRegion());
        } else {
            setViewHolder.tv3.setText("NA");
        }

        setViewHolder.close.setTag(i);


        return view;
    }

    class SetUpViewHolder{

        TextView tv1,tv2,tv3;
        ImageView close;

        SetUpViewHolder(View view, View.OnClickListener clickListener){
            tv1 = (TextView) view.findViewById(R.id.txt1);
            tv2 = (TextView) view.findViewById(R.id.txt2);
            tv3 = (TextView) view.findViewById(R.id.txt3);
            close = (ImageView) view.findViewById(R.id.loaddemand);
            close.setOnClickListener(clickListener);
        }

    }

}


