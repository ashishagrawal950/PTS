package com.pts.invetech.dashboardupdate.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.dashboardupdate.model.dashboard.Mcp;

import java.util.List;

/**
 * Created by Ashish Karn on 13-10-2016.
 */

public class McpTableAdapter extends BaseAdapter {

    private List<Mcp> mcpData=null;
    private Activity context;

    public McpTableAdapter(Activity _context, List<Mcp> _names) {
        this.context = _context;
        this.mcpData = _names;
    }

    @Override
    public int getCount() {
        return mcpData.size();
    }

    @Override
    public Mcp getItem(int i) {
        return mcpData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            LayoutInflater inflater = context.getLayoutInflater();
            convertView = inflater.inflate(R.layout.new_listitem_mcp_on_dashboard, null, true);
        }

        Mcp data=getItem(position);

        TextView date = (TextView) convertView.findViewById(R.id.tdate);
        date.setText(data.getDate());

        TextView max = (TextView) convertView.findViewById(R.id.tmax);
        max.setText(String.valueOf(data.getMax()));

        TextView min = (TextView) convertView.findViewById(R.id.tmin);
        min.setText(String.valueOf(data.getMin()));

        TextView avg = (TextView) convertView.findViewById(R.id.tavg);
        avg.setText(String.valueOf(data.getAvg()));

        return  convertView;
    }
}