package com.pts.invetech.pojo;

/**
 * Created by Ashish on 09-03-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class MyShcheduleGetCurrentFlow {

    private String id;
    private String buyer;
    private String seller;
    private String periphery;
    private String region;
    private String state;
    private String appNo;
    private String revision;
    private String date;
    private String importtype;
    private String trader;
    private String device_id;
    private String company;
    private String mobile;
    private String email;
    private ArrayList<Detail> detail = null;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getPeriphery() {
        return periphery;
    }

    public void setPeriphery(String periphery) {
        this.periphery = periphery;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAppNo() {
        return appNo;
    }

    public void setAppNo(String appNo) {
        this.appNo = appNo;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImporttype() {
        return importtype;
    }

    public void setImporttype(String importtype) {
        this.importtype = importtype;
    }

    public String getTrader() {
        return trader;
    }

    public void setTrader(String trader) {
        this.trader = trader;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Detail> getDetail() {
        return detail;
    }

    public JSONArray getDetailArray() {
        if(this.detail==null || this.detail.isEmpty()){
            return null;
        }
        JSONArray arr=new JSONArray();
        for(Detail dtl:this.detail){
            arr.put(dtl.getDetailObj());
        }
        return arr;
    }

    public void setDetail(ArrayList<Detail> detail) {
        this.detail = detail;
    }

    public void addDetail(Detail dtl){
        if(this.detail==null){
            detail=new ArrayList<Detail>();
        }

        detail.add(dtl);
    }
}
