package com.pts.invetech.classes.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;

import com.pts.invetech.R;
import com.pts.invetech.classes.afterlogin.NewsFeedSearchbyCategoryFragment;
import com.pts.invetech.classes.fragment.AllNewsFeedFragment;
import com.pts.invetech.classes.fragment.ContactusFragment;
import com.pts.invetech.classes.fragment.CurrentAvaiableURSFragment;
import com.pts.invetech.classes.fragment.DisclaimerFragment;
import com.pts.invetech.classes.fragment.FeedBackFragment;
import com.pts.invetech.classes.fragment.LandedCostCalculatorFragment;
import com.pts.invetech.classes.fragment.MCP_ComparisionFragment;
import com.pts.invetech.classes.fragment.MonthlyATCFragment;
import com.pts.invetech.classes.fragment.MySchedulingFragment;
import com.pts.invetech.classes.fragment.NewsFeedFragment;
import com.pts.invetech.classes.fragment.NotificationSettingFragment;
import com.pts.invetech.classes.fragment.PowerDemandComparisionFragment;
import com.pts.invetech.classes.fragment.RealtimeSchedulingFragment;
import com.pts.invetech.classes.fragment.StateURSFragment;
import com.pts.invetech.classes.fragment.StatewiseRealtimeSchedulingFragment;
import com.pts.invetech.classes.fragment.StationURSFragment;
import com.pts.invetech.classes.fragment.TransmissionCorridorFragment;
import com.pts.invetech.customlist.MenuCustomAdapter;
import com.pts.invetech.customlist.ReltimeSchedulelistCustomAdapter;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.dashboardupdate.views.DashBoardUpdate;
import com.pts.invetech.stateload.StateLoadFragment;
import com.pts.invetech.utils.Constant;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

public class NewFeedMainActivity extends AppCompatActivity {
    private Dialog prgDialog;
    private DrawerLayout mDrawerLayout;
    private RelativeLayout llNewsFeed, llDashboard, layout_Transmission_Corridor, layout_Landed_Cost_Calculator, layout_My_Schedule,
            layout_MySchedule, layout_Power_Demand_Comparision, layout_MCP_Comparision, layout_Monthly_ACT, layout_stateload, layout_Notification_Setting, layout_Contact_us, layout_Disclaimer, layout_Feebback;
    private LinearLayout linearLayout, layout_myScheduleList;
    @SuppressWarnings("deprecation")
    private ActionBarDrawerToggle mDrawerToggle;
    // nav drawer title
    private CharSequence mDrawerTitle;
    // used to store app title
    private CharSequence mTitle;
    // slide menu items
    private DashBoardUpdate dashBoardUpdate = null;
    private NewsFeedFragment newsFeedFragment = null;
    private AllNewsFeedFragment allNewsFeedFragment = null;
    private CurrentAvaiableURSFragment currentAvaiableURSFragment = null;
    private NewsFeedSearchbyCategoryFragment newsFeedSearchbyCategoryFragment = null;
    private TransmissionCorridorFragment transmissionCorridorFragment = null;
    private RealtimeSchedulingFragment realtimeSchedulingFragment = null;
    private StatewiseRealtimeSchedulingFragment statewiseRealtimeSchedulingFragment = null;
    private PowerDemandComparisionFragment powerDemandComparisionFragment = null;
    private MySchedulingFragment mySchedulingFragment = null;
    private LandedCostCalculatorFragment landedCostCalculatorFragment = null;
    private MCP_ComparisionFragment mCP_ComparisionFragment = null;
    private MonthlyATCFragment monthlyATCFragment = null;
    private StateLoadFragment stateLoadFragment = null;
    private NotificationSettingFragment notificationSettingFragment = null;
    private ContactusFragment contactusFragment = null;
    private DisclaimerFragment disclaimerFragment = null;
    private FeedBackFragment feedBackFragment = null;
    private SQLiteDatabase db;
    private String saveResponse = "";
    private ArrayList<String> key_element = new ArrayList<String>();
    private ArrayList<String> realtime_element = new ArrayList<String>();
    private boolean isvisible = true;
    private boolean isrealtimevisible = true;
    private NonScrollListView searchlistView, realtimelistView;
    private String loginconfig = "";
    private String domain,newsletterBaseUrl;
    private boolean isFirst = true;
    private ArrayList<NewFeedMainActivity.MyDataPasser> passerArrayList = new ArrayList<>();

  /*	private void View() {
		newsFeedFragment = new NewsFeedFragment();
		if (newsFeedFragment !=null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.content_frame, newsFeedFragment);
			fragmentTransaction.commit();
			mDrawerLayout.closeDrawer(linearLayout);
		}
		else {
			Log.e("MainActivity", "Error in creating fragment");
		}
	}*/
    private String lastValue = "2016-2017";

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

	/*private void setInitView() {
		NewsFeedFragment newsFeedFragment = new NewsFeedFragment();
		if (newsFeedFragment !=null) {
			android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.content_frame, newsFeedFragment);
			fragmentTransaction.commit();
			mDrawerLayout.closeDrawer(linearLayout);
		}
		else {
			Log.e("MainActivity", "Error in creating fragment");
		}
	}*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newfeed_activity_main);
        //getActionBar().hide();
        /*prgDialog = new ProgressDialog(NewFeedMainActivity.this);
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = prgDialog.findViewById(R.id.img);
        Animation animationprogess = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animationprogess);
        //prgDialog.show();
        prgDialog.setCancelable(false);
        db = openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientid(clientid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationnewsfeed(newMessage TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS gpsloaction(latitude VARCHAR, longitude VARCHAR, state_id VARCHAR, state_serial VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS availableurs(region VARCHAR, available VARCHAR, select_yes VARCHAR, revision VARCHAR, region_id VARCHAR);");



        linearLayout = (LinearLayout) findViewById(R.id.menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        final Animation animation = AnimationUtils.loadAnimation(this, R.anim.menu_slide_down);
        //llDashboard  = (RelativeLayout) findViewById(R.id.llDashboard);
        llNewsFeed = (RelativeLayout) findViewById(R.id.llNewsFeed);
        final ImageView imageView3 = (ImageView) findViewById(R.id.imageView3);
        final RelativeLayout layout_current_urs = (RelativeLayout) findViewById(R.id.layout_current_urs);
        final LinearLayout layout_station_urs = (LinearLayout) findViewById(R.id.layout_station_urs);
        final LinearLayout layout_state_urs = (LinearLayout) findViewById(R.id.layout_state_urs);
        final RelativeLayout layout_news2 = (RelativeLayout) findViewById(R.id.layout_news2);
        final LinearLayout layout_listview = (LinearLayout) findViewById(R.id.layout_listview);
        layout_listview.setLayoutAnimation(new LayoutAnimationController(animation));
        final ImageView imageView34 = (ImageView) findViewById(R.id.imageView34);
        layout_Transmission_Corridor = (RelativeLayout) findViewById(R.id.layout_Transmission_Corridor);

        final RelativeLayout layout_rl = (RelativeLayout) findViewById(R.id.layout_rl);
        final LinearLayout layout_realtime = (LinearLayout) findViewById(R.id.layout_realtime);
        layout_realtime.setLayoutAnimation(new LayoutAnimationController(animation));
        final ImageView imageView4 = (ImageView) findViewById(R.id.imageView4);

        layout_Landed_Cost_Calculator = (RelativeLayout) findViewById(R.id.layout_Landed_Cost_Calculator);
        layout_My_Schedule = (RelativeLayout) findViewById(R.id.layout_My_Schedule);
        layout_My_Schedule.setVisibility(View.GONE);
        layout_myScheduleList = (LinearLayout) findViewById(R.id.layout_myScheduleList);
        layout_myScheduleList.setVisibility(View.GONE);
        layout_MySchedule = (RelativeLayout) findViewById(R.id.layout_MySchedule);
        layout_MySchedule.setVisibility(View.GONE);

        layout_Power_Demand_Comparision = (RelativeLayout) findViewById(R.id.layout_Power_Demand_Comparision);
        layout_Monthly_ACT = (RelativeLayout) findViewById(R.id.layout_Monthly_ACT);
        layout_MCP_Comparision = (RelativeLayout) findViewById(R.id.layout_MCP_Comparision);
        layout_stateload = (RelativeLayout) findViewById(R.id.layout_stateload);
        layout_Notification_Setting = (RelativeLayout) findViewById(R.id.layout_Notification_Setting);
        layout_Contact_us = (RelativeLayout) findViewById(R.id.layout_Contact_us);
        layout_Disclaimer = (RelativeLayout) findViewById(R.id.layout_Disclaimer);
        layout_Feebback = (RelativeLayout) findViewById(R.id.layout_Feebback);

        Intent in = getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");
        if (loginconfig.equalsIgnoreCase("")) {
            layout_station_urs.setVisibility(View.GONE);
            layout_state_urs.setVisibility(View.GONE);
            layout_current_urs.setVisibility(View.GONE);
        } else if (loginconfig.equalsIgnoreCase("LOGIN")) {
            layout_station_urs.setVisibility(View.GONE);
            layout_state_urs.setVisibility(View.GONE);
            layout_current_urs.setVisibility(View.GONE);
        } else {
            layout_station_urs.setVisibility(View.VISIBLE);
            layout_state_urs.setVisibility(View.VISIBLE);
            layout_current_urs.setVisibility(View.VISIBLE);
        }
        if(!isFirst){
            domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
            newsletterBaseUrl = SharedPrefHandler.getDeviceString(getApplicationContext(), "newsletterBaseUrl");
        }
        mTitle = mDrawerTitle = getTitle();
        llNewsFeed.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setInitView();
                llNewsFeed.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
            }
        });

        layout_current_urs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExpandableLayout exp_manageLead = (ExpandableLayout) findViewById(R.id.exp_manageLead);
                if (exp_manageLead.isExpanded()) {
                    exp_manageLead.collapse();
                    layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                    imageView34.setImageResource((R.drawable.menu_dwn_arrow));
                } else {
                    exp_manageLead.expand();
                    layout_current_urs.setBackgroundColor(Color.parseColor("#00e9e9"));
                    imageView34.setImageResource((R.drawable.menu_up_arow));

                }
//                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
//                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
//                layout_current_urs.setBackgroundColor(Color.parseColor("#00e9e9"));
//                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
//                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
//                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
//                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
//                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
//                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
//                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
//                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
//                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
//                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
//                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
//                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
//                currentAvaiableURSFragment = new CurrentAvaiableURSFragment();
//                if (currentAvaiableURSFragment != null) {
//                    FragmentManager fragmentManager = getSupportFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.content_frame, currentAvaiableURSFragment);
//                    fragmentTransaction.commit();
//                    mDrawerLayout.closeDrawer(linearLayout);
//                }
//                else {
//                    Log.e("MainActivity", "Error in creating fragment");
//                }
            }
        });

        layout_station_urs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_station_urs.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                StationURSFragment stationURSFragment = new StationURSFragment();
                if (stationURSFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, stationURSFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        layout_state_urs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_station_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_state_urs.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                StateURSFragment stateURSFragment = new StateURSFragment();
                if (stateURSFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, stateURSFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });



		/*llDashboard.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setInitView();
				//layout_news2.setBackgroundColor(Color.parseColor("#00e9e9"));
				llNewsFeed.setBackgroundColor(Color.parseColor("#00e9e9"));
				layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
				layout_news2.setBackgroundColor(Color.parseColor("#009999"));
				layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
				layout_Realtime_Scheduling.setBackgroundColor(Color.parseColor("#009999"));
				layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
				//layout_News_Letter.setBackgroundColor(Color.parseColor("#009999"));
				layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
				layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
				layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
				layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
				layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
				dashBoardUpdate = new DashBoardUpdate();
				if (dashBoardUpdate !=null) {
					FragmentManager fragmentManager = getSupportFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.content_frame, dashBoardUpdate)
							.commit();
					mDrawerLayout.closeDrawer(linearLayout);
				}
				else {
					Log.e("MainActivity", "Error in creating fragment");
				}
			}
		});*/

        /*llNewsFeed.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                currentAvaiableURSFragment = new CurrentAvaiableURSFragment();
                if (currentAvaiableURSFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, currentAvaiableURSFragment)
                            .commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });*/

        searchlistView = (NonScrollListView) findViewById(R.id.searchlistView);
        listfromdb();

        layout_news2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isvisible == true) {
                    isvisible = false;
                    layout_listview.setVisibility(View.VISIBLE);
                    searchlistView.setVisibility(View.VISIBLE);
                    imageView3.setImageResource((R.drawable.menu_up_arow));
                    layout_news2.setBackgroundColor(Color.parseColor("#00e9e9"));
                    llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                    layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                    layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                    layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                    layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                    layout_listview.setLayoutAnimation(new LayoutAnimationController(animation));
                } else {
                    isvisible = true;
                    layout_listview.setVisibility(View.GONE);
                    searchlistView.setVisibility(View.GONE);
                    imageView3.setImageResource((R.drawable.menu_dwn_arrow));
                    layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                    llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                    layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                    layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                    layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                    layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                    // tv_news_search.setVisibility(View.GONE);
                    // Intent i=new Intent(getApplicationContext(), Activity_Previous_bid.class);
                }
            }
        });

        layout_Transmission_Corridor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#00e9e9"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                transmissionCorridorFragment = new TransmissionCorridorFragment();
                if (transmissionCorridorFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, transmissionCorridorFragment)
                            .commit();

                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        realtimelistView = (NonScrollListView) findViewById(R.id.realtimelistView);
        realtime_element.add("RLDC wise Realtime Scheduling");
        realtime_element.add("State wise Realtime Scheduling");
        realtimelistView.setAdapter(new ReltimeSchedulelistCustomAdapter(this, realtime_element));

        layout_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isrealtimevisible == true) {
                    isrealtimevisible = false;
                    layout_realtime.setVisibility(View.VISIBLE);
                    realtimelistView.setVisibility(View.VISIBLE);
                    imageView4.setImageResource((R.drawable.menu_up_arow));
                    layout_rl.setBackgroundColor(Color.parseColor("#00e9e9"));
                    llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                    layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                    layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                    layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                    realtimelistView.setLayoutAnimation(new LayoutAnimationController(animation));
                } else {
                    isrealtimevisible = true;
                    layout_realtime.setVisibility(View.GONE);
                    realtimelistView.setVisibility(View.GONE);
                    imageView4.setImageResource((R.drawable.menu_dwn_arrow));
                    layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                    llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                    layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                    layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                    layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                    layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                    layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                    // tv_news_search.setVisibility(View.GONE);
                    // Intent i=new Intent(getApplicationContext(), Activity_Previous_bid.class);
                }
            }
        });

        realtimelistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view, final int position, long id) {
                if (realtime_element.get(position).equalsIgnoreCase("RLDC wise Realtime Scheduling")) {
                    realtimeSchedulingFragment = new RealtimeSchedulingFragment();
                    if (realtimeSchedulingFragment != null) {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, realtimeSchedulingFragment);
                        fragmentTransaction.commit();
                        mDrawerLayout.closeDrawer(linearLayout);
                    } else {
                        Log.e("MainActivity", "Error in creating fragment");
                    }
                } else if (realtime_element.get(position).equalsIgnoreCase("State wise Realtime Scheduling")) {
                    statewiseRealtimeSchedulingFragment = new StatewiseRealtimeSchedulingFragment();
                    if (statewiseRealtimeSchedulingFragment != null) {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, statewiseRealtimeSchedulingFragment);
                        fragmentTransaction.commit();
                        mDrawerLayout.closeDrawer(linearLayout);
                    } else {
                        Log.e("MainActivity", "Error in creating fragment");
                    }
                }
            }
        });


        layout_Landed_Cost_Calculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                landedCostCalculatorFragment = new LandedCostCalculatorFragment();
                if (landedCostCalculatorFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, landedCostCalculatorFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });




		/*layout_Realtime_Scheduling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
				layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
				layout_news2.setBackgroundColor(Color.parseColor("#009999"));
				layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
				layout_Realtime_Scheduling.setBackgroundColor(Color.parseColor("#00e9e9"));
				layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
				layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
				layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
				layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
				layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
				layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
				layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
				layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
				layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
				realtimeSchedulingFragment = new RealtimeSchedulingFragment();
				if (realtimeSchedulingFragment !=null) {
					FragmentManager fragmentManager = getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, realtimeSchedulingFragment);
					fragmentTransaction.commit();
					mDrawerLayout.closeDrawer(linearLayout);
				}
				else {
					Log.e("MainActivity", "Error in creating fragment");
				}
            }
        });*/


		/*layout_Statewise_Realtime_Scheduling.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
						layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
						layout_news2.setBackgroundColor(Color.parseColor("#009999"));
						layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
						layout_Realtime_Scheduling.setBackgroundColor(Color.parseColor("#009999"));
						layout_Statewise_Realtime_Scheduling.setBackgroundColor(Color.parseColor("#00e9e9"));
						layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
						layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
						layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
						layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
						layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
						layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
						layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
						layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
						layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
						statewiseRealtimeSchedulingFragment = new StatewiseRealtimeSchedulingFragment();
						if (statewiseRealtimeSchedulingFragment !=null) {
							FragmentManager fragmentManager = getSupportFragmentManager();
							FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
							fragmentTransaction.replace(R.id.content_frame, statewiseRealtimeSchedulingFragment);
							fragmentTransaction.commit();
							mDrawerLayout.closeDrawer(linearLayout);
						}
						else {
							Log.e("MainActivity", "Error in creating fragment");
						}
					}
				});*/

        layout_Power_Demand_Comparision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                powerDemandComparisionFragment = new PowerDemandComparisionFragment();
                if (powerDemandComparisionFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, powerDemandComparisionFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        layout_Monthly_ACT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                monthlyATCFragment = new MonthlyATCFragment();
                if (monthlyATCFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, monthlyATCFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

		/*layout_News_Letter.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
				layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
				layout_news2.setBackgroundColor(Color.parseColor("#009999"));
				layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
				layout_Realtime_Scheduling.setBackgroundColor(Color.parseColor("#009999"));
				layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
				layout_News_Letter.setBackgroundColor(Color.parseColor("#00e9e9"));
				layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
				layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
				layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
				newsLetterFragment = new NewsLetterFragment();
				if (newsLetterFragment !=null) {
					FragmentManager fragmentManager = getSupportFragmentManager();
					FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
					fragmentTransaction.replace(R.id.content_frame, newsLetterFragment);
					fragmentTransaction.commit();
					mDrawerLayout.closeDrawer(linearLayout);
				}
				else {
					Log.e("MainActivity", "Error in creating fragment");
				}
			}
		});*/


        layout_stateload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_stateload.setBackgroundColor(Color.parseColor("#00e9e9"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                stateLoadFragment = new StateLoadFragment();
                if (stateLoadFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, stateLoadFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });


        layout_My_Schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                mySchedulingFragment = new MySchedulingFragment();
                if (mySchedulingFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, mySchedulingFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        layout_Notification_Setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                notificationSettingFragment = new NotificationSettingFragment();
                if (notificationSettingFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, notificationSettingFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        layout_Contact_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                contactusFragment = new ContactusFragment();
                if (contactusFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, contactusFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        layout_Disclaimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                disclaimerFragment = new DisclaimerFragment();
                if (disclaimerFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, disclaimerFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });


        layout_Feebback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_Feebback.setBackgroundColor(Color.parseColor("#00e9e9"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                feedBackFragment = new FeedBackFragment();
                if (feedBackFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, feedBackFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });


        layout_MCP_Comparision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout_MCP_Comparision.setBackgroundColor(Color.parseColor("#00e9e9"));
                layout_Feebback.setBackgroundColor(Color.parseColor("#009999"));
                llNewsFeed.setBackgroundColor(Color.parseColor("#009999"));
                layout_current_urs.setBackgroundColor(Color.parseColor("#009999"));
                layout_news2.setBackgroundColor(Color.parseColor("#009999"));
                layout_Transmission_Corridor.setBackgroundColor(Color.parseColor("#009999"));
                layout_Landed_Cost_Calculator.setBackgroundColor(Color.parseColor("#009999"));
                layout_Monthly_ACT.setBackgroundColor(Color.parseColor("#009999"));
                layout_stateload.setBackgroundColor(Color.parseColor("#009999"));
                layout_rl.setBackgroundColor(Color.parseColor("#009999"));
                layout_Power_Demand_Comparision.setBackgroundColor(Color.parseColor("#009999"));
                layout_My_Schedule.setBackgroundColor(Color.parseColor("#009999"));
                layout_Notification_Setting.setBackgroundColor(Color.parseColor("#009999"));
                layout_Contact_us.setBackgroundColor(Color.parseColor("#009999"));
                layout_Disclaimer.setBackgroundColor(Color.parseColor("#009999"));
                mCP_ComparisionFragment = new MCP_ComparisionFragment();
                if (mCP_ComparisionFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, mCP_ComparisionFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });



		/*layout_Disclaimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				final Dialog dialog = new Dialog(NewFeedMainActivity.this);
				// Include dialog.xml file
				dialog.setContentView(R.layout.dialog_disclaimer);
				// Set dialog title
				dialog.setTitle( Html.fromHtml("<font color='#009999'>Disclaimer</font>"));
				String htmlText = " %s ";
				String textshow = "Invetech Solutions has taken due care and caution in compilation and reporting of data and information as has been obtained from various sources including which it considers reliable and first hand. However, Invetech Solutions does not guarantee the accuracy, adequacy or completeness of any information and is not responsible for errors or omissions or for the results obtained from the use of such information and especially states that it has no financial liability whatsoever to the users of contents of this page. This research and information does not constitute recommendation or advice for trading or investment purposes and therefore Invetech Solutions will not be liable for any loss accrued as a result of a trading/investment activity that is undertaken on the basis of information contained on this page. Invetech Solutions does not consider itself to undertake Regulated Activities as defined in Section 22 of the Financial Services and Markets Act 2000 and it is not registered with the Financial Services Authority of the UK.\n\nThanks\nInvetech Solutions";
				// set values for custom dialog components - text, image and button
				TextView text = (TextView) dialog.findViewById(R.id.textDialog);
				// text.loadData(String.format(htmlText, textshow), "text/html", "utf-8");
				dialog.show();


				Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
				// if decline button is clicked, close the custom dialog
				declineButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						// Close dialog
						dialog.dismiss();
					}
				});
			}
        });*/


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.toggle, // nav menu toggle icon
                R.string.sliderheading, // nav drawer open - description for
                // accessibility
                R.string.sliderheading // nav drawer close - description for
                // accessibility
        ) {
            public void onDrawerClosed(View view) {
                //getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                //getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        if (savedInstanceState == null) {
            // on first time display view for first nav item
            setInitView();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String str = extras.getString(Constant.VALUE_DATAPASS);
            String shedule = extras.getString(Constant.VALUE_SHEDULETRACK);
            if (str != null) {
                monthlyATCFragment = new MonthlyATCFragment();
                if (monthlyATCFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction =
                            fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, monthlyATCFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                }
            } else if (shedule != null) {
                mySchedulingFragment = new MySchedulingFragment();
                if (mySchedulingFragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction =
                            fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, mySchedulingFragment);
                    fragmentTransaction.commit();
                    mDrawerLayout.closeDrawer(linearLayout);
                }
            }
        }/*else{
			setInitView();
		}*/
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    private void setInitView() {
        DashBoardUpdate dashBoardUpdate = new DashBoardUpdate();
        if (dashBoardUpdate != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, dashBoardUpdate);
            fragmentTransaction.commit();
            mDrawerLayout.closeDrawer(linearLayout);
        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    public void listfromdb() {
        Cursor newfeedsavec = db.rawQuery("select * from newfeedsave", null);
        if (newfeedsavec.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        while (newfeedsavec.moveToNext()) {
            saveResponse = newfeedsavec.getString(newfeedsavec.getColumnIndex("response"));

        }
        if (saveResponse.length() == 0) {
            if(isFirst){
                new HttpAsyncTaskgetnewsfeedinfoAgain().execute("https://www.mittalpower.com/mobile/pxs_app/service/getnewsfeedinfo.php");
            }
            else{
                new HttpAsyncTaskgetnewsfeedinfoAgain().execute(domain + "/mobile/pxs_app/service/getnewsfeedinfo.php");
            }

        } else {
            try {
                JSONObject mainObject = new JSONObject(saveResponse);
                key_element.clear();
                key_element.add("All");
                Iterator<String> iter = mainObject.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    key_element.add(key);
                }
                searchlistView.setAdapter(new MenuCustomAdapter(this, key_element));

                searchlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            View view, final int position, long id) {
                        if (key_element.get(position).equalsIgnoreCase("All")) {
                            allNewsFeedFragment = new AllNewsFeedFragment();
                            if (allNewsFeedFragment != null) {
                                FragmentManager fragmentManager = getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.content_frame, allNewsFeedFragment);
                                fragmentTransaction.commit();
                                mDrawerLayout.closeDrawer(linearLayout);
                            } else {
                                Log.e("MainActivity", "Error in creating fragment");
                            }
                        } else {
                            Bundle bundle = new Bundle();
                            bundle.putString("Key", key_element.get(position));
                            newsFeedSearchbyCategoryFragment = new NewsFeedSearchbyCategoryFragment();
                            if (newsFeedSearchbyCategoryFragment != null) {
                                FragmentManager fragmentManager = getSupportFragmentManager();
                                newsFeedSearchbyCategoryFragment.setArguments(bundle);
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.content_frame, newsFeedSearchbyCategoryFragment);
                                fragmentTransaction.commit();
                                mDrawerLayout.closeDrawer(linearLayout);
                            } else {
                                Log.e("MainActivity", "Error in creating fragment");
                            }
                        }
                    }
                });

            } catch (JSONException e) {
                try {
                    JSONObject mainObject = new JSONObject(saveResponse);
                    String errormass = mainObject.getString("message");
                    Toast.makeText(NewFeedMainActivity.this, errormass, Toast.LENGTH_LONG).show();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                if (saveResponse != null) {
                    //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(saveResponse);
                    String title = html.title();
                    Toast.makeText(NewFeedMainActivity.this, "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();
        switch (id) {

            case R.id.action_call:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* *
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(linearLayout);
        menu.findItem(R.id.action_call).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     */
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        //getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void open() {
        // TODO Auto-generated method stub
        mDrawerLayout.openDrawer(Gravity.LEFT);

    }

    public String addPasserCallback(MyDataPasser passer) {
        passerArrayList.add(passer);
        return lastValue;
    }

    public boolean removePasserCallback(MyDataPasser passer) {
        return passerArrayList.remove(passer);
    }

    public void updateCallback(String data) {
        lastValue = data;
        for (MyDataPasser d : passerArrayList) {
            d.updateDateString(data);
        }
    }

    public interface MyDataPasser {
        void updateDateString(String date);
    }

    private class HttpAsyncTaskgetnewsfeedinfoAgain extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //Toast.makeText(getBaseContext(), "Received!"+result, Toast.LENGTH_LONG).show();
            try {
                if (result.contains("'s")) {
                    result = result.replace("'s", "");
                }
                if (result.contains("'")) {
                    result = result.replace("'", "");
                }
				/*if(result.contains(",")){
					result = result.replace(",", " ");
				}*/
                //result = DatabaseUtils.sqlEscapeString(result);
                db.execSQL("DROP TABLE IF EXISTS newfeedsave");
                db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
                db.execSQL("INSERT INTO newfeedsave VALUES('" + result + "', '" + result + "', '" + result + "')");
                JSONObject mainObjectnotused = new JSONObject(result);

            } catch (JSONException e) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String errormass = mainObject.getString("message");
                    Toast.makeText(NewFeedMainActivity.this, errormass, Toast.LENGTH_LONG).show();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                if (result != null) {
                    //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(NewFeedMainActivity.this, "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            listfromdb();
        }
    }

}
