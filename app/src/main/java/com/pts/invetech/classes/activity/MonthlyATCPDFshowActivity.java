package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pts.invetech.NotificationOne;
import com.pts.invetech.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish on 07-11-2016.
 */

public class MonthlyATCPDFshowActivity extends Activity {


    private LinearLayout mopenLayout;
    private ImageView ivshare, ivdownload;
    private String urlfileSend;
    private String extation = "";
    private int numMessagesOne = 0;
    private NotificationManager myNotificationManager;
    private int notificationIdOne = 111;
    private String extationSend = "", extetaionagain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_monthlyatcpdf);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            urlfileSend = extras.getString("url");
            new DownloadFileAsync().execute(urlfileSend);
        }
        ivshare = (ImageView) findViewById(R.id.ivshare);
        ivdownload = (ImageView) findViewById(R.id.ivdownload);

        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        mopenLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Resources resources = getResources();
                Intent emailIntent = new Intent();
                emailIntent.setAction(Intent.ACTION_SEND);
                // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
                emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_native)));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
                emailIntent.setType("message/rfc822");

                PackageManager pm = getPackageManager();
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");

                Intent openInChooser = Intent.createChooser(emailIntent, resources.getString(R.string.share_chooser_text));
                List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
                List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();

                for (int i = 0; i < resInfo.size(); i++) {
                    ResolveInfo ri = resInfo.get(i);
                    String packageName = ri.activityInfo.packageName;
                    if (packageName.contains("android.email")) {
                        emailIntent.setPackage(packageName);
                    } else if (packageName.contains("whatsapp") || packageName.contains("gm")) {
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        if (packageName.contains("whatsapp")) {
                            String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/" + extetaionagain;
                            File fileWithinMyDir = new File(myFilePath);
                            if (fileWithinMyDir.exists()) {
                                intent.setType("application*//*");
                                intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + myFilePath));
                                //intent.putExtra(Intent.EXTRA_SUBJECT,"PTS");
                                //intent.putExtra(Intent.EXTRA_TEXT, "PTS");
                                //startActivity(Intent.createChooser(intent, "Share File"));
                            }
                        } else if (packageName.contains("android.gm")) {
                            intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_gmail)));
                            intent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
                            intent.setType("message/rfc822");
                            //String myFilePath = Environment.getExternalStorageDirectory().getPath()+"/"+categoryurlfileSend;
                            String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/" + extetaionagain;
                            File fileWithinMyDir = new File(myFilePath);
                            if (fileWithinMyDir.exists()) {
                                intent.setType("application/PDF");
                                intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + myFilePath));
                                intent.putExtra(Intent.EXTRA_SUBJECT, "PTS");
                                intent.putExtra(Intent.EXTRA_TEXT, "PTS");
                            }
                        }
                        intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
                    }
                }

                LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);
                openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
                startActivity(openInChooser);
            }

        });

        ivdownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadFileAsync().execute(urlfileSend);


            }
        });

        WebView myWebView = (WebView) findViewById(R.id.webView);
        myWebView.setClickable(true);
        myWebView.setFocusableInTouchMode(true);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        myWebView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + urlfileSend);
        WebClientClass webViewClient = new WebClientClass();
        myWebView.setWebViewClient(webViewClient);
        WebChromeClient webChromeClient = new WebChromeClient();
        myWebView.setWebChromeClient(webChromeClient);

    }

    public class WebClientClass extends WebViewClient {
        Dialog prgDialog = null;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            prgDialog = new Dialog(MonthlyATCPDFshowActivity.this);
            prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            prgDialog.setContentView(R.layout.progessdialog);
            ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
            image.startAnimation(animation);
            prgDialog.show();
            prgDialog.setCancelable(false);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            prgDialog.dismiss();
        }
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileAsync extends AsyncTask<String, String, String> {
        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            String myUrl;
            final int BUFFER_SIZE = 4096;
            try {
                myUrl = f_url[0];
                String to = myUrl.replaceAll("(?<!http:|https:)//", "/");
                String finalstring = to.replaceAll(" ", "%20");
                //  String str="https://docs.google.com/viewer?url="+finalstring;
                URL url = new URL(finalstring);
                URLConnection conection = url.openConnection();
                conection.connect();
                extation = "";
                String disposition = conection.getHeaderField("Content-Disposition");
                String contentType = conection.getContentType();
                int contentLength = conection.getContentLength();
                if (disposition != null) {
                    int index = disposition.indexOf("filename=");
                    if (index > 0) {
                        extation = disposition.substring(index + 10,
                                disposition.length() - 1);
                    }
                } else {
                    extation = myUrl.substring(myUrl.lastIndexOf("/") + 1
                    );
                }
                extetaionagain = extation.replaceAll("%", "_");
                InputStream inputStream = conection.getInputStream();
                String saveFilePath = Environment
                        .getExternalStorageDirectory().toString() + "/" + extetaionagain;
                // opens an output stream to save into file
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);
                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
                outputStream.close();
                inputStream.close();
            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {

            if (extetaionagain.endsWith(".pdf")) {
                ivshare.setVisibility(View.VISIBLE);
                ivdownload.setVisibility(View.VISIBLE);
            } else if (extetaionagain.endsWith(".PDF")) {
                ivshare.setVisibility(View.VISIBLE);
                ivdownload.setVisibility(View.VISIBLE);
            } else {
                ivshare.setVisibility(View.GONE);
                ivdownload.setVisibility(View.GONE);
            }

            Notification.Builder mBuilder = new Notification.Builder(MonthlyATCPDFshowActivity.this);
            //String[] parts =  categoryurl.split("/");
            //String filename = parts[parts.length-1];
            mBuilder.setContentTitle("PTS");
            if (extetaionagain.endsWith(".pdf")) {
                mBuilder.setContentText("Monthly ATC: " + extetaionagain + " file download.(File is in PDF format.)");
            }
            if (extetaionagain.endsWith(".PDF")) {
                mBuilder.setContentText("Monthly ATC: " + extetaionagain + " file download.(File is in PDF format.)");
            } else if (extetaionagain.endsWith(".xlsx")) {
                mBuilder.setContentText("Monthly ATC: " + extetaionagain + " file download.(File is in xlsx format.)");
            } else if (urlfileSend.endsWith(".xls")) {
                mBuilder.setContentText("Monthly ATC: " + extetaionagain + " file download.(File is in xls format.)");
            }
            mBuilder.setTicker("Downloading");
            mBuilder.setSmallIcon(R.drawable.logopts);

            // Increase notification number every time a new notification arrives
            mBuilder.setNumber(++numMessagesOne);
            mBuilder.setAutoCancel(true);
            // Creates an explicit intent for an Activity in your app
            Intent myIntent = new Intent();
            myIntent.setAction(Intent.ACTION_VIEW);
            if (extetaionagain.endsWith(".pdf")) {
                File file = new File(Environment.getExternalStorageDirectory().getPath() + "/" + extetaionagain);
                if (file.exists()) {
                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                } else {

                }
            }
            if (extetaionagain.endsWith(".PDF")) {
                File file = new File(Environment.getExternalStorageDirectory().getPath() + "/" + extetaionagain);
                if (file.exists()) {
                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                } else {

                }
            } else if (extetaionagain.endsWith(".xlsx")) {
                File file = new File(Environment.getExternalStorageDirectory().getPath() + "/" + extetaionagain);
                if (file.exists()) {
                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                } else {
                    //Toast.makeText(DownloadFileActivity.this, "Unable to ", Toast.LENGTH_SHORT).show();
                }
            } else if (extetaionagain.endsWith(".xls")) {
                File file = new File(Environment.getExternalStorageDirectory().getPath() + "/" + extetaionagain);
                if (file.exists()) {
                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                } else {

                }
            }
            //This ensures that navigating backward from the Activity leads out of the app to Home page
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(MonthlyATCPDFshowActivity.this);
            // Adds the back stack for the Intent
            stackBuilder.addParentStack(NotificationOne.class);

            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(myIntent);
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(
                            0,
                            PendingIntent.FLAG_ONE_SHOT //can only be used once
                    );
            // start the activity when the user clicks the notification text
            mBuilder.setContentIntent(resultPendingIntent);
            Notification noti = mBuilder.build();
            myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            noti.defaults |= Notification.DEFAULT_VIBRATE;
            noti.defaults |= Notification.DEFAULT_SOUND;
            noti.flags |= Notification.FLAG_SHOW_LIGHTS;
            noti.ledARGB = 0xff00ff00;
            noti.ledOnMS = 300;
            noti.ledOffMS = 1000;
            noti.flags |= Notification.FLAG_AUTO_CANCEL;
            // pass the Notification object to the system
            myNotificationManager.notify(notificationIdOne, mBuilder.build());
        }

    }
}



/* extends Activity {

    Context context;
    LinearLayout mopenLayout;
    String client_id;
    SQLiteDatabase db;
    ImageView ivshare,ivdownload;
    String urlfileSend;
    String extation="";
    private int numMessagesOne = 0;
    private NotificationManager myNotificationManager;
    private int notificationIdOne = 111;
    String extationSend= "", extetaionagain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_monthlyatcpdf);
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            urlfileSend = extras.getString("url");
            new DownloadFileAsync().execute(urlfileSend);
        }
        else
        {
            //..oops!
        }
        ivshare = (ImageView) findViewById(R.id.ivshare);
        ivdownload = (ImageView) findViewById(R.id.ivdownload);



        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        mopenLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

        ivshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Resources resources = getResources();
                Intent emailIntent = new Intent();
                emailIntent.setAction(Intent.ACTION_SEND);
                // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
                emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_native)));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
                emailIntent.setType("message/rfc822");

                PackageManager pm = getPackageManager();
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");

                Intent openInChooser = Intent.createChooser(emailIntent, resources.getString(R.string.share_chooser_text));
                List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
                List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
                for (int i = 0; i < resInfo.size(); i++) {
                    // Extract the label, append it, and repackage it in a LabeledIntent
                    ResolveInfo ri = resInfo.get(i);
                    String packageName = ri.activityInfo.packageName;
                    if(packageName.contains("android.email")) {
                        emailIntent.setPackage(packageName);
                    } else if(packageName.contains("whatsapp") || packageName.contains("gm")) {
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        if(packageName.contains("whatsapp")) {
                            //String myFilePath = Environment.getExternalStorageDirectory().getPath()+"/"+categoryurlfileSend;
                            String myFilePath = Environment.getExternalStorageDirectory().getPath()+"/"+extetaionagain;
                            Log.d("myFilePath",myFilePath);
                            File fileWithinMyDir = new File(myFilePath);
                            if(fileWithinMyDir.exists()) {
                                Log.d("myFilePath1",myFilePath);
                                intent.setType("application*//**//*");
                                intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+myFilePath));
                                //intent.putExtra(Intent.EXTRA_SUBJECT,"PTS");
                                //intent.putExtra(Intent.EXTRA_TEXT, "PTS");
                                //startActivity(Intent.createChooser(intent, "Share File"));
                            }

                            // intent.putExtra(Intent.EXTRA_TEXT, resources.getString(R.string.share_twitter));
                        }
                        else if(packageName.contains("android.gm")) {
                            intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_gmail)));
                            intent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
                            intent.setType("message/rfc822");
                            //String myFilePath = Environment.getExternalStorageDirectory().getPath()+"/"+categoryurlfileSend;
                            String myFilePath = Environment.getExternalStorageDirectory().getPath()+"/"+extetaionagain;
                            File fileWithinMyDir = new File(myFilePath);
                            if(fileWithinMyDir.exists()) {
                                Log.d("myFilePathgmail",myFilePath);
                                intent.setType("application/PDF");
                                intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+myFilePath));
                                intent.putExtra(Intent.EXTRA_SUBJECT,"PTS");
                                intent.putExtra(Intent.EXTRA_TEXT, "PTS");

                            }
                        }
                        intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
                    }
                }
                // convert intentList to array
                LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);
                openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
                startActivity(openInChooser);
            }

        });

        ivdownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadFileAsync().execute(urlfileSend);

                Notification.Builder  mBuilder = new Notification.Builder(MonthlyATCPDFshowActivity.this);
                //String[] parts =  categoryurl.split("/");
                //String filename = parts[parts.length-1];
                mBuilder.setContentTitle("PTS");
                if(urlfileSend.endsWith(".pdf")){
                    mBuilder.setContentText(extetaionagain + " file download.(File is in PDF format.)");
                }
                if(urlfileSend.endsWith(".PDF")){
                    mBuilder.setContentText(extetaionagain + " file download.(File is in PDF format.)");
                }
                else if(urlfileSend.endsWith(".xlsx")){
                    mBuilder.setContentText(extetaionagain + " file download.(File is in xlsx format.)");
                }
                else if(urlfileSend.endsWith(".xls")){
                    mBuilder.setContentText(extetaionagain + " file download.(File is in xls format.)");
                }
                mBuilder.setTicker("Downloading");
                mBuilder.setSmallIcon(R.drawable.logopts);

                // Increase notification number every time a new notification arrives
                mBuilder.setNumber(++numMessagesOne);
                mBuilder.setAutoCancel(true);
                // Creates an explicit intent for an Activity in your app
                Intent myIntent=new Intent();
                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                if(urlfileSend.endsWith(".pdf")){
                    File file =new File(Environment.getExternalStorageDirectory().getPath()+"/"+extetaionagain);
                    if(file.exists()){
                        myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                    }else{

                    }
                }
                if(urlfileSend.endsWith(".PDF")){
                    File file =new File(Environment.getExternalStorageDirectory().getPath()+"/"+extetaionagain);
                    if(file.exists()){
                        myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                    }else{

                    }
                }
                else if(urlfileSend.endsWith(".xlsx")){
                    File file =new File(Environment.getExternalStorageDirectory().getPath()+"/"+extetaionagain);
                    if(file.exists()){
                        myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                    }else{
                        //Toast.makeText(DownloadFileActivity.this, "Unable to ", Toast.LENGTH_SHORT).show();
                    }
                }
                else if(urlfileSend.endsWith(".xls")){
                    File file =new File(Environment.getExternalStorageDirectory().getPath()+"/"+extetaionagain);
                    if(file.exists()){
                        myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                    }else{

                    }
                }
                //This ensures that navigating backward from the Activity leads out of the app to Home page
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(MonthlyATCPDFshowActivity.this);
                // Adds the back stack for the Intent
                stackBuilder.addParentStack(NotificationOne.class);

                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(myIntent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(
                                0,
                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                        );
                // start the activity when the user clicks the notification text
                mBuilder.setContentIntent(resultPendingIntent);
                Notification noti = mBuilder.build();
                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                noti.defaults |= Notification.DEFAULT_VIBRATE;
                noti.defaults |= Notification.DEFAULT_SOUND;
                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                noti.ledARGB = 0xff00ff00;
                noti.ledOnMS = 300;
                noti.ledOffMS = 1000;
                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                // pass the Notification object to the system
                myNotificationManager.notify(notificationIdOne, mBuilder.build());

            }
        });

        WebView myWebView = (WebView) findViewById(R.id.webView);
        myWebView.setClickable(true);
        myWebView.setFocusableInTouchMode(true);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        myWebView.loadUrl("https://docs.google.com/gview?embedded=true&url="+ urlfileSend);
        MonthlyATCPDFshowActivity.WebClientClass webViewClient = new MonthlyATCPDFshowActivity.WebClientClass();
        myWebView.setWebViewClient(webViewClient);
        WebChromeClient webChromeClient=new WebChromeClient();
        myWebView.setWebChromeClient(webChromeClient);

    }

    public class WebClientClass extends WebViewClient {
        Dialog prgDialog = null;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            prgDialog = new Dialog(MonthlyATCPDFshowActivity.this);
            prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            prgDialog.setContentView(R.layout.progessdialog);
            ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
            image.startAnimation(animation);
            prgDialog.show();
            prgDialog.setCancelable(false);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            prgDialog.dismiss();
        }
    }

    *//**
 * Background Async Task to download file
 * <p>
 * Before starting background thread Show Progress Bar Dialog
 * <p>
 * Downloading file in background thread
 * <p>
 * Updating progress bar
 * <p>
 * After completing background task Dismiss the progress dialog
 **//*
    class DownloadFileAsync extends AsyncTask<String, String, String> {
        *//**
 * Before starting background thread Show Progress Bar Dialog
 * *//*
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showDialog(progress_bar_type);
        }

        *//**
 * Downloading file in background thread
 * *//*
        @Override
        protected String doInBackground(String... f_url) {
            String myUrl;
            final int BUFFER_SIZE = 4096;
            try {
                myUrl=f_url[0];
                String to = myUrl.replaceAll("(?<!http:|https:)//", "/");
                String finalstring =  to.replaceAll(" ","%20");
                URL url = new URL(finalstring);
                URLConnection conection = url.openConnection();
                conection.connect();
                extation = "";
                String disposition = conection.getHeaderField("Content-Disposition");
                String contentType = conection.getContentType();
                int contentLength = conection.getContentLength();
                if (disposition != null) {
                    // extracts file name from header field
                    int index = disposition.indexOf("filename=");
                    if (index > 0) {
                        extation = disposition.substring(index + 10,
                                disposition.length() - 1);
                    }
                } else {
                    // extracts file name from URL
                    extation = myUrl.substring(myUrl.lastIndexOf("/") + 1,
                            myUrl.length());
                }
                System.out.println("Content-Type = " + contentType);
                System.out.println("Content-Disposition = " + disposition);
                System.out.println("Content-Length = " + contentLength);
                System.out.println("fileName = " + extation);
                extetaionagain = extation.replaceAll("%","_");
                InputStream inputStream = conection.getInputStream();
                String saveFilePath = Environment
                        .getExternalStorageDirectory().toString()+"/"+extetaionagain;
                // opens an output stream to save into file
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);
                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
                outputStream.close();
                inputStream.close();
                System.out.println("File downloaded.");
            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        *//**
 * Updating progress bar
 * *//*
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        *//**
 * After completing background task Dismiss the progress dialog
 * **//*
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            // dismissDialog(progress_bar_type);
            if(urlfileSend.endsWith(".pdf")){
                ivshare.setVisibility(View.VISIBLE);
                ivdownload.setVisibility(View.VISIBLE);
            }else if(urlfileSend.endsWith(".PDF")){
                ivshare.setVisibility(View.VISIBLE);
                ivdownload.setVisibility(View.VISIBLE);
            }else{
                ivshare.setVisibility(View.GONE);
                ivdownload.setVisibility(View.GONE);
            }
        }

    }



   *//*@Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        *//**//*Intent in = new Intent(MonthlyATCPDFshowActivity.this, NewsLetterActivity.class);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);*//**//*
        finish();
        return;
    }*//*


}*/