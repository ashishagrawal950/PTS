
package com.pts.invetech;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

public class DownloadIEXCustomGrid extends BaseAdapter{
    private Context mContext;
    private final ArrayList<String> iex_namearray;
    private final ArrayList<Integer> iex_imgarray;

      public DownloadIEXCustomGrid(Context c,ArrayList<String> iex_namearray,ArrayList<Integer> iex_imgarray ) {
          mContext = c;
          this.iex_imgarray = iex_imgarray;
          this.iex_namearray = iex_namearray;
      }

      @Override
      public int getCount() {
          // TODO Auto-generated method stub
          return iex_namearray.size();
      }

      @Override
      public Object getItem(int position) {
          // TODO Auto-generated method stub
          return null;
      }

      @Override
      public long getItemId(int position) {
          // TODO Auto-generated method stub
          return 0;
      }

      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
          // TODO Auto-generated method stub
          View grid;
          LayoutInflater inflater = (LayoutInflater) mContext
              .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

          if (convertView == null) {

              grid = new View(mContext);
              grid = inflater.inflate(R.layout.grid_downloadiex, null);
              TextView imageView = (TextView)grid.findViewById(R.id.grid_image);
              imageView.setBackgroundResource(iex_imgarray.get(position));
          } else {
              grid = (View) convertView;
          }

          return grid;
      }
}

