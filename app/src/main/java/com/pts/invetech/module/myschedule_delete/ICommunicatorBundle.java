package com.pts.invetech.module.myschedule_delete;

import android.os.Parcelable;

/**
 * Created by vaibhav on 30/3/18.
 */

public interface ICommunicatorBundle<T> extends Parcelable {
    void onDataSend(T data);
    void onDataReject();
}
