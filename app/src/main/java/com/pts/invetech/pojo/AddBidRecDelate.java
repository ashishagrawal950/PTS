package com.pts.invetech.pojo;

/**
 * Created by Ashish on 01-04-2016.
 */
public class AddBidRecDelate {

    private String id;
    private String type;
    private String rectype;
    private String ordernature;
    private String date;
    private String access_key;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRectype() {
        return rectype;
    }

    public void setRectype(String rectype) {
        this.rectype = rectype;
    }

    public String getOrdernature() {
        return ordernature;
    }

    public void setOrdernature(String ordernature) {
        this.ordernature = ordernature;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }
}
