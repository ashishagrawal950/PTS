package com.pts.invetech.pojo;

public class AddBidSubmit {
	private String bidtype;
	private String type;
	private String biddate;
	private String addbidsaveaccesskey;
	private String rectype;
	private String id;
	private String price;
	private String noofrec;
	
	public String getBidtype() {
		return bidtype;
	}
	public void setBidtype(String bidtype) {
		this.bidtype = bidtype;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBiddate() {
		return biddate;
	}
	public void setBiddate(String biddate) {
		this.biddate = biddate;
	}
	public String getAddbidsaveaccesskey() {
		return addbidsaveaccesskey;
	}
	public void setAddbidsaveaccesskey(String addbidsaveaccesskey) {
		this.addbidsaveaccesskey = addbidsaveaccesskey;
	}
	public String getRectype() {
		return rectype;
	}
	public void setRectype(String rectype) {
		this.rectype = rectype;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getNoofrec() {
		return noofrec;
	}
	public void setNoofrec(String noofrec) {
		this.noofrec = noofrec;
	}
	
	
	
}
