package com.pts.invetech.pojo;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ashish on 30-03-2017.
 */

public class Detail {
    private String region;
    private String code;

    public Detail(){}

    public Detail(String _region,String _code){
        region=_region;
        code=_code;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public JSONObject getDetailObj(){
        JSONObject obj=new JSONObject();

        try {
            obj.accumulate("region",region);
            obj.accumulate("code",code);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
    }



}
