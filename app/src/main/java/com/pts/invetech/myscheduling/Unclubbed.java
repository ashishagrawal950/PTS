
package com.pts.invetech.myscheduling;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Unclubbed {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("data")
    @Expose
    private UnclubbedData data;
    @SerializedName("change")
    @Expose
    private String change;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UnclubbedData getData() {
        return data;
    }

    public void setData(UnclubbedData data) {
        this.data = data;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

}

