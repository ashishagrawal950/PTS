package com.pts.invetech.pojo;

public class DeviceRegister {
	private int id;
	private String name;
	private String platform;
	private String uuid;
	private String version;
	private String width;
	private String height;
	private String colordepth;
	public static String access_key;
	public static String aceess_keyagain;
	
	public DeviceRegister() {
	}

	public DeviceRegister(String name, String platform, String uuid,
			String version, String width, String height, String colordepth) {
		this.name = name;
		this.platform = platform;
		this.uuid = uuid;
		this.version = version;
		this.width = width;
		this.height = height;
		this.colordepth = colordepth;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPlatform() {
		return platform;
	}


	public void setPlatform(String platform) {
		this.platform = platform;
	}


	public String getUuid() {
		return uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public String getWidth() {
		return width;
	}


	public void setWidth(String width) {
		this.width = width;
	}


	public String getHeight() {
		return height;
	}


	public void setHeight(String height) {
		this.height = height;
	}


	public String getColordepth() {
		return colordepth;
	}


	public void setColordepth(String colordepth) {
		this.colordepth = colordepth;
	}

	public static String getAccess_key() {
		return access_key;
	}

	public static void setAccess_key(String access_key) {
		DeviceRegister.access_key = access_key;
	}

	

}
