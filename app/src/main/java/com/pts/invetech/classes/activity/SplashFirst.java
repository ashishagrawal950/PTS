package com.pts.invetech.classes.activity;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.pts.badge.ShortcutBadger;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseActivity;
import com.pts.invetech.R;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Utility;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashFirst extends AppBaseActivity {

    public static String deviceid;
    private SQLiteDatabase db;
    private RelativeLayout parentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        db = openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS device(uuid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");

        deviceid = Secure.getString(SplashFirst.this.getContentResolver(),
                Secure.ANDROID_ID);

        if (SharedPrefHandler.checkFirstTime(SplashFirst.this)) {
            showPopup();
        } else {
            alwaysCheck();
        }

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.pts.invetech",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }


    }

    private void alwaysCheck() {
        conditionCheck();
        ShortcutBadger.applyCount(SplashFirst.this, 0);
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) getApplicationContext().getSystemService(ns);
        nMgr.cancelAll();
    }

    private void showPopup() {
        parentLayout = (RelativeLayout) findViewById(R.id.holderpopvu);
        View child = getLayoutInflater().inflate(R.layout.pop_up_window, null);

        ((WebView) child.findViewById(R.id.tv_privacy_policy)).loadData(getString(R.string.data_show),
                "text/html; charset=utf-8", "utf-8");

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(Utility.getwidth(this, 80),
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        parentLayout.addView(child, params);

        Button btn = (Button) child.findViewById(R.id.agree_ok);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox checkBox = (CheckBox) findViewById(R.id.agreecheck);
                if (checkBox.isChecked()) {
                    if (parentLayout != null && parentLayout.getVisibility() == View.VISIBLE) {
                        parentLayout.setVisibility(View.GONE);
                        SharedPrefHandler.setFirstTime(SplashFirst.this);
                    }
                    alwaysCheck();
                } else {
                    AppLogger.showToastLong(SplashFirst.this, "Please Check the box.");
                }
            }
        });
    }

    public void returndeviceid() {
//        TelephonyManager tManagerr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        String deviceidunique = tManagerr.getDeviceId();
    }

    @SuppressLint("NewApi")
    private void conditionCheck() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!getAskedPermission()) {
                try {
                    java.lang.reflect.Method methodRequestPermission = Activity.class.getMethod("requestPermissions", java.lang.String[].class, int.class);
                    methodRequestPermission.invoke(this, new String[]
                            {
                                    Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION
                            }, 6);
                } catch (Exception ex) {
                }
            } else {
                movetofirstscreen();
            }
        } else {
            movetofirstscreen();
        }
    }

    @SuppressLint("NewApi")
    private boolean getAskedPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
		return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
	}

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 6: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && getAllPermissionChecked(grantResults)) {
//                    returndeviceid();
                    movetofirstscreen();
                } else {
//                    finish();
                    movetofirstscreen();
                }
                return;
            }
        }
    }

    private boolean getAllPermissionChecked(int[] results) {
        for (int num : results) {
            if (num != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public void movetofirstscreen() {
        Intent in = new Intent(SplashFirst.this, SplashScreenActivity.class);
        startActivity(in);
//        finish();
    }

}