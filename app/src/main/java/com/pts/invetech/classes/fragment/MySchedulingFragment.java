package com.pts.invetech.classes.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.handler.NetworkHandlerString;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseFragment;
import com.pts.invetech.R;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.classes.activity.MySchedulingDetailBlockActivity;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.customlist.AdapterMyScheduling;
import com.pts.invetech.dashboardupdate.views.DashBoardUpdate;
import com.pts.invetech.myscheduling.CurrentFlowModel;
import com.pts.invetech.myscheduling.GetSaveSchedule;
import com.pts.invetech.myscheduling.MyScheduleDatum;
import com.pts.invetech.myscheduling.MySchedulingConvertor;
import com.pts.invetech.myscheduling.SaveFilter;
import com.pts.invetech.utils.AppDeviceUtils;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish on 16-08-2016.
 */
public class MySchedulingFragment extends AppBaseFragment implements AdapterView.OnItemSelectedListener {

    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private Button btn_filter_one, btn_filterTwo, btn_apply, btn_cancel;
    private LinearLayout layout_one_myscheduling, layouttwo_myscheduling, listlayout, layout_region;
    private CardView cv_filter;
    private String deviceid,URL_SAVEFILTER,URL_AFTERSAVE,URL_SAVEFILTERs;
    private Dialog prgDialog;
    private ListView list;
    private List<String> categories;
    private Spinner spn_region_one, spn_region_two, spn_region_three, spn_region_four, spn_region_five;

    private Spinner spn_clientname_one, spn_clientname_two, spn_clientname_three, spn_clientname_four, spn_clientname_five;

    //String companyName, mobileNo, email;
    //EditText edtcompanyname, edtmobile, edtemail;

    private LinearLayout llregionandclientone, llregionandclienttwo,
            llregionandclientthree, llregionandclientfour, llregionandclientfive;
    private String spn_clientname_two_value = "", spn_clientname_three_value = "", spn_clientname_four_value = "",
            spn_clientname_five_value = "";

    private ImageView img_add_region_one, img_add_region_two, img_add_region_three, img_add_region_four;

    private ImageView img_delete_region_two, img_delete_region_three, img_delete_region_four, img_delete_region_five;
    private ArrayAdapter<String> dataAdapterregion;

    private String[] strHolder = {null, null, null, null, null};
    private boolean isFirst = true;
    private String domain;
    // Delete Buttons Click Listner
    private View.OnClickListener deleteListner = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            switch (view.getId()) {

                case R.id.img_delete_region_two:
                    llregionandclienttwo.setVisibility(View.GONE);
                    img_add_region_one.setVisibility(View.VISIBLE);
                    spn_clientname_two_value = "";
                    break;

                case R.id.img_delete_region_three:
                    llregionandclientthree.setVisibility(View.GONE);
                    img_delete_region_two.setVisibility(View.VISIBLE);
                    img_add_region_two.setVisibility(View.VISIBLE);
                    spn_clientname_three_value = "";
                    break;

                case R.id.img_delete_region_four:
                    llregionandclientfour.setVisibility(View.GONE);
                    img_delete_region_three.setVisibility(View.VISIBLE);
                    img_add_region_three.setVisibility(View.VISIBLE);
                    spn_clientname_four_value = "";
                    break;

                case R.id.img_delete_region_five:
                    llregionandclientfive.setVisibility(View.GONE);
                    img_delete_region_four.setVisibility(View.VISIBLE);
                    img_add_region_four.setVisibility(View.VISIBLE);
                    spn_clientname_five_value = "";
                    break;

            }

        }
    };
    private NetworkCallBack networkCallBack = new NetworkCallBack() {

        @Override
        public void onResultString(String data, int id) {

            if (data == null) {
//                AppLogger.showToastShort(getActivity(), "Some Error Occurred.");
                return;
            }

            switch (id) {
                case 0:
                    updateClientIds(MySchedulingConvertor.getStateListFrmRegion(data), 0);
                    break;

                case 1:
                    updateClientIds(MySchedulingConvertor.getStateListFrmRegion(data), 1);
                    break;

                case 2:
                    updateClientIds(MySchedulingConvertor.getStateListFrmRegion(data), 2);
                    break;

                case 3:
                    updateClientIds(MySchedulingConvertor.getStateListFrmRegion(data), 3);
                    break;

                case 4:
                    updateClientIds(MySchedulingConvertor.getStateListFrmRegion(data), 4);
                    break;

                case 99:
                    List<CurrentFlowModel> flowModel = new ArrayList<>();
                    try {
                        MySchedulingConvertor.getCurrentFlowData(flowModel, data);
                        cv_filter.setVisibility(View.GONE);
                        listlayout.setVisibility(View.VISIBLE);
                        btn_filterTwo.setVisibility(View.VISIBLE);

                        setFlowAdapter(flowModel);

                        SharedPrefHandler.saveBoolean(getActivity(), "myscheduledetail", Boolean.TRUE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
            }
        }

        @Override
        public void onResultObject(Object data, int id) {
            if (data == null) {
//                AppLogger.showToastShort(getActivity(), "Some Error Occurred.");
                return;
            }

            switch (id) {
                case 1:
                    SaveFilter filter = (SaveFilter) data;
                    AppLogger.showToastShort(getActivity(), filter.getMessage() + "");
                    try {
                        apiAfterSave();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case 2:
                    GetSaveSchedule saveSchedule = (GetSaveSchedule) data;
                    updateAfterGetSaveSchedule(saveSchedule);
                    break;

            }
        }
    };
    // Add Buttons Click Listner
    private View.OnClickListener addLister = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.img_add_region_one:
                    llregionandclienttwo.setVisibility(View.VISIBLE);
                    img_add_region_one.setVisibility(View.INVISIBLE);
                    addApiCall(categories.get(spn_region_two.getSelectedItemPosition()), 1);
                    break;

                case R.id.img_add_region_two:
                    llregionandclientthree.setVisibility(View.VISIBLE);
                    img_add_region_two.setVisibility(View.INVISIBLE);
                    img_delete_region_two.setVisibility(View.INVISIBLE);
                    addApiCall(categories.get(spn_region_three.getSelectedItemPosition()), 2);
                    break;

                case R.id.img_add_region_three:
                    llregionandclientfour.setVisibility(View.VISIBLE);
                    img_add_region_three.setVisibility(View.INVISIBLE);
                    img_delete_region_three.setVisibility(View.INVISIBLE);
                    addApiCall(categories.get(spn_region_four.getSelectedItemPosition()), 3);
                    break;

                case R.id.img_add_region_four:
                    llregionandclientfive.setVisibility(View.VISIBLE);
                    img_add_region_four.setVisibility(View.INVISIBLE);
                    img_delete_region_four.setVisibility(View.INVISIBLE);
                    addApiCall(categories.get(spn_region_five.getSelectedItemPosition()), 4);
                    break;
            }

        }
    };
    private View.OnClickListener otherClicks = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.btn_apply) {

                //companyName = edtcompanyname.getText().toString();
                //mobileNo = edtmobile.getText().toString();
                //email = edtemail.getText().toString();
                /*if (AppStringUtils.isTextEmpty(companyName)) {
                    AppLogger.showToastShort(getActivity(), "Please Enter Company Name");
                    return;
                }

                if (!AppStringUtils.isValidPhoneNumber(mobileNo)) {
                    AppLogger.showToastShort(getActivity(), "Mobile Number is not Valid.");
                    return;
                }

                if (!AppStringUtils.isEmailValid(email)) {
                    AppLogger.showToastShort(getActivity(), "Email is not Valid.");
                    return;
                }

                try {
                    saveFilter(companyName, mobileNo, email);
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
                try {
                    saveFilter();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (view.getId() == R.id.btn_cancel) {
                //if (isFirst) {
                layout_one_myscheduling.setVisibility(View.GONE);
                layouttwo_myscheduling.setVisibility(View.VISIBLE);

                btn_filterTwo.setVisibility(View.VISIBLE);
                cv_filter.setVisibility(View.GONE);
                listlayout.setVisibility(View.VISIBLE);
                /*}else {
                    cv_filter.setVisibility(View.VISIBLE);
                    //layout_one_myscheduling.setVisibility(View.GONE);
                    layouttwo_myscheduling.setVisibility(View.VISIBLE);
                    btn_filterTwo.setVisibility(View.GONE);
                    listlayout.setVisibility(View.VISIBLE);
                }*/
            } else if (view.getId() == R.id.btn_filter_one) {
                layout_one_myscheduling.setVisibility(View.GONE);
                layouttwo_myscheduling.setVisibility(View.VISIBLE);
                listlayout.setVisibility(View.GONE);
                addApiCall(categories.get(spn_region_one.getSelectedItemPosition()), 0);

            } else if (view.getId() == R.id.btn_filterTwo) {
                layout_one_myscheduling.setVisibility(View.GONE);
                btn_filterTwo.setVisibility(View.GONE);
                layouttwo_myscheduling.setVisibility(View.VISIBLE);
                cv_filter.setVisibility(View.VISIBLE);
                listlayout.setVisibility(View.GONE);

                try {
                    apiGetSaveSchedule();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_myscheduling, container, false);

        LinearLayout openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NewFeedMainActivity) getActivity()).open();
            }
        });
        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);

        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        prgDialog.setCancelable(false);


        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            String str = extras.getString(Constant.VALUE_APPNUMBERFROMGRAPH);
            AppLogger.showMsg("app number from graph", "" + str);

        }
        if(!isFirst){
            domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        }
        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);

        Intent in = getActivity().getIntent();
        String loginconfig = in.getStringExtra("LOGINCONFIG");
        if (in.hasExtra(Constant.VALUE_DATAPASS)) {
            String str = in.getStringExtra(Constant.VALUE_DATAPASS);
            Log.i("Type:", str);
        }

        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        lltoplogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        deviceid = AppDeviceUtils.getTelephonyId(getActivity());

        list = (ListView) rootView.findViewById(R.id.lv_mysheduling);
        listlayout = (LinearLayout) rootView.findViewById(R.id.listlayout);

        layout_one_myscheduling = (LinearLayout) rootView.findViewById(R.id.layout_one_myscheduling);
        layouttwo_myscheduling = (LinearLayout) rootView.findViewById(R.id.layouttwo_myscheduling);
        cv_filter = (CardView) rootView.findViewById(R.id.cv_filter);
        //edtcompanyname = (EditText) rootView.findViewById(R.id.edtcompanyname);
        //edtmobile = (EditText) rootView.findViewById(R.id.edtmobile);
        //edtemail = (EditText) rootView.findViewById(R.id.edtemail);


        spn_clientname_one = (Spinner) rootView.findViewById(R.id.spn_clientname_one);
        spn_clientname_one.setOnItemSelectedListener(this);
        spn_clientname_two = (Spinner) rootView.findViewById(R.id.spn_clientname_two);
        spn_clientname_two.setOnItemSelectedListener(this);
        spn_clientname_three = (Spinner) rootView.findViewById(R.id.spn_clientname_three);
        spn_clientname_three.setOnItemSelectedListener(this);
        spn_clientname_four = (Spinner) rootView.findViewById(R.id.spn_clientname_four);
        spn_clientname_four.setOnItemSelectedListener(this);
        spn_clientname_five = (Spinner) rootView.findViewById(R.id.spn_clientname_five);
        spn_clientname_five.setOnItemSelectedListener(this);


        spn_region_one = (Spinner) rootView.findViewById(R.id.spn_region_one);
        spn_region_one.setOnItemSelectedListener(this);
        spn_region_two = (Spinner) rootView.findViewById(R.id.spn_region_two);
        spn_region_two.setOnItemSelectedListener(this);
        spn_region_three = (Spinner) rootView.findViewById(R.id.spn_region_three);
        spn_region_three.setOnItemSelectedListener(this);
        spn_region_four = (Spinner) rootView.findViewById(R.id.spn_region_four);
        spn_region_four.setOnItemSelectedListener(this);
        spn_region_five = (Spinner) rootView.findViewById(R.id.spn_region_five);
        spn_region_five.setOnItemSelectedListener(this);

        categories = new ArrayList<String>();
        categories.add("ER");
        categories.add("NR");
        categories.add("WR");
        categories.add("SR");
        categories.add("NER");

        dataAdapterregion = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_voltage, categories);
        dataAdapterregion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_region_one.setAdapter(dataAdapterregion);
        spn_region_one.setSelection(0);
        spn_region_two.setAdapter(dataAdapterregion);
        spn_region_two.setSelection(1);
        spn_region_three.setAdapter(dataAdapterregion);
        spn_region_three.setSelection(2);
        spn_region_four.setAdapter(dataAdapterregion);
        spn_region_four.setSelection(3);
        spn_region_five.setAdapter(dataAdapterregion);
        spn_region_five.setSelection(4);

        //llregionandclienttwo,llregionandclientthree,llregionandclientfour,llregionandclientfive

        llregionandclientone = (LinearLayout) rootView.findViewById(R.id.llregionandclientone);
        llregionandclienttwo = (LinearLayout) rootView.findViewById(R.id.llregionandclienttwo);
        llregionandclientthree = (LinearLayout) rootView.findViewById(R.id.llregionandclienttthree);
        llregionandclientfour = (LinearLayout) rootView.findViewById(R.id.llregionandclientfour);
        llregionandclientfive = (LinearLayout) rootView.findViewById(R.id.llregionandclientfive);

        // ImageView img_add_region_one,img_add_region_two,img_add_region_three,img_add_region_four,img_add_region_five
        img_add_region_one = (ImageView) rootView.findViewById(R.id.img_add_region_one);
        img_add_region_two = (ImageView) rootView.findViewById(R.id.img_add_region_two);
        img_add_region_three = (ImageView) rootView.findViewById(R.id.img_add_region_three);
        img_add_region_four = (ImageView) rootView.findViewById(R.id.img_add_region_four);

        img_add_region_one.setOnClickListener(addLister);
        img_add_region_two.setOnClickListener(addLister);
        img_add_region_three.setOnClickListener(addLister);
        img_add_region_four.setOnClickListener(addLister);

        //ImageView img_delete_region_one,img_delete_region_two,img_delete_region_three,img_delete_region_four,img_delete_region_five
        img_delete_region_two = (ImageView) rootView.findViewById(R.id.img_delete_region_two);
        img_delete_region_three = (ImageView) rootView.findViewById(R.id.img_delete_region_three);
        img_delete_region_four = (ImageView) rootView.findViewById(R.id.img_delete_region_four);
        img_delete_region_five = (ImageView) rootView.findViewById(R.id.img_delete_region_five);

        img_delete_region_two.setOnClickListener(deleteListner);
        img_delete_region_three.setOnClickListener(deleteListner);
        img_delete_region_four.setOnClickListener(deleteListner);
        img_delete_region_five.setOnClickListener(deleteListner);


        btn_filter_one = (Button) rootView.findViewById(R.id.btn_filter_one);
        btn_filter_one.setOnClickListener(otherClicks);

        btn_apply = (Button) rootView.findViewById(R.id.btn_apply);
        btn_apply.setOnClickListener(otherClicks);

        btn_cancel = (Button) rootView.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(otherClicks);


        btn_filterTwo = (Button) rootView.findViewById(R.id.btn_filterTwo);
        btn_filterTwo.setOnClickListener(otherClicks);


        isFirst = SharedPrefHandler.getBoolean(getActivity(), "myscheduledetail");

        if (isFirst) {
            layout_one_myscheduling.setVisibility(View.GONE);
            layouttwo_myscheduling.setVisibility(View.VISIBLE);
            listlayout.setVisibility(View.GONE);

            cv_filter.setVisibility(View.GONE);
            btn_filterTwo.setVisibility(View.VISIBLE);
            listlayout.setVisibility(View.VISIBLE);

            try {
                apiAfterSave();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return rootView;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.spn_region_one) {
            addApiCall(categories.get(spn_region_one.getSelectedItemPosition()), 0);
        } else if (parent.getId() == R.id.spn_region_two) {
            addApiCall(categories.get(spn_region_two.getSelectedItemPosition()), 1);
        } else if (parent.getId() == R.id.spn_region_three) {
            addApiCall(categories.get(spn_region_three.getSelectedItemPosition()), 2);
        } else if (parent.getId() == R.id.spn_region_four) {
            addApiCall(categories.get(spn_region_four.getSelectedItemPosition()), 3);
        } else if (parent.getId() == R.id.spn_region_five) {
            addApiCall(categories.get(spn_region_five.getSelectedItemPosition()), 4);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /*
     * Api Calling
     */

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashBoardUpdate dashBoardUpdate = new DashBoardUpdate();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, dashBoardUpdate);
                        fragmentTransaction.commit();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void addApiCall(String region, int num) {
        String[] data = new String[2];
        JSONObject object = new JSONObject();
        try {
            object.put("device_id", AppDeviceUtils.getTelephonyId(getActivity()));
            object.put("region", region);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(isFirst){
            data[0] = "https://www.mittalpower.com/mobile/pxs_app/service/schedule_track/getclientcodeofregion.php";
        }else{
            data[0] = domain + "/mobile/pxs_app/service/schedule_track/getclientcodeofregion.php";
        }
        data[1] = object.toString();

        new NetworkHandlerString(getActivity(), networkCallBack, num).execute(data);
    }

    private void updateAfterGetSaveSchedule(GetSaveSchedule saveSchedule) {
        //edtcompanyname.setText(saveSchedule.getCompany());
        //edtmobile.setText(saveSchedule.getMobile());
        //edtemail.setText(saveSchedule.getEmail());

        llregionandclientone.setVisibility(View.GONE);
        llregionandclienttwo.setVisibility(View.GONE);
        llregionandclientthree.setVisibility(View.GONE);
        llregionandclientfour.setVisibility(View.GONE);
        llregionandclientfive.setVisibility(View.GONE);

        List<MyScheduleDatum> dataDatum = saveSchedule.getData();
        if (dataDatum.size() > 0) {
            llregionandclientone.setVisibility(View.VISIBLE);
            addSaveSchedule(spn_region_one, dataDatum.get(0).getRegion());
            strHolder[0] = dataDatum.get(0).getCode();
//            addSaveSchedule(spn_clientname_one,Arrays.asList(dataDatum.get(0).getCode()));
        }

        if (dataDatum.size() > 1) {
            llregionandclienttwo.setVisibility(View.VISIBLE);
            addSaveSchedule(spn_region_two, dataDatum.get(1).getRegion());
//            addSaveSchedule(spn_clientname_two,Arrays.asList(dataDatum.get(1).getCode()));
            strHolder[1] = dataDatum.get(1).getCode();
        }

        if (dataDatum.size() > 2) {
            llregionandclientthree.setVisibility(View.VISIBLE);
            addSaveSchedule(spn_region_three, dataDatum.get(2).getRegion());
//            addSaveSchedule(spn_clientname_three,Arrays.asList(dataDatum.get(2).getCode()));
            strHolder[2] = dataDatum.get(2).getCode();
        }

        if (dataDatum.size() > 3) {
            llregionandclientfour.setVisibility(View.VISIBLE);
            addSaveSchedule(spn_region_four, dataDatum.get(3).getRegion());
//            addSaveSchedule(spn_clientname_four,Arrays.asList(dataDatum.get(3).getCode()));
            strHolder[3] = dataDatum.get(3).getCode();
        }

        if (dataDatum.size() > 4) {
            llregionandclientfive.setVisibility(View.VISIBLE);
            addSaveSchedule(spn_region_five, dataDatum.get(4).getRegion());
//            addSaveSchedule(spn_clientname_five,Arrays.asList(dataDatum.get(4).getCode()));
            strHolder[4] = dataDatum.get(4).getCode();
        }


    }

    private void updateClientIds(List<String> clientIdList, int num) {
        switch (num) {
            case 0:
                setClntSpinner(spn_clientname_one, clientIdList, strHolder[0]);
                break;
            case 1:
                setClntSpinner(spn_clientname_two, clientIdList, strHolder[1]);
                break;
            case 2:
                setClntSpinner(spn_clientname_three, clientIdList, strHolder[2]);
                break;
            case 3:
                setClntSpinner(spn_clientname_four, clientIdList, strHolder[3]);
                break;
            case 4:
                setClntSpinner(spn_clientname_five, clientIdList, strHolder[4]);
                break;
        }
    }

    private synchronized void setClntSpinner(Spinner spinner, List<String> clientIdList, String word) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item_voltage, clientIdList);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        if (word != null) {
            spinner.setSelection(getPosition(clientIdList, word));
        }
    }

    private void addSaveSchedule(Spinner spinner, String clientId) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item_voltage, categories);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(getPosition(categories, clientId));
    }

    /*
     * Other clicks
     */

    private synchronized int getPosition(List<String> stList, String value) {
        return stList.indexOf(value);
    }

    private void apiGetSaveSchedule() throws JSONException {
        if(isFirst){
             URL_SAVEFILTER = "https://www.mittalpower.com/mobile/pxs_app/service/schedule_track/getsaveschedule.php";
        }else{
             URL_SAVEFILTER = domain + "/mobile/pxs_app/service/schedule_track/getsaveschedule.php";
        }
        JSONObject object = new JSONObject();
        object.put("device_id", AppDeviceUtils.getTelephonyId(getActivity()));

        new NetworkHandlerModel(getActivity(), networkCallBack,
                GetSaveSchedule.class, 2).execute(URL_SAVEFILTER, object.toString());
    }

    private void apiAfterSave() throws JSONException {
        if(isFirst){
             URL_AFTERSAVE = "https://www.mittalpower.com/mobile/pxs_app/service/schedule_track/getcurrentflow.php";
        }else{
             URL_AFTERSAVE = domain + "/mobile/pxs_app/service/schedule_track/getcurrentflow.php";
        }
        String[] data = new String[2];

        JSONObject obj = new JSONObject();
        obj.put("device_id", AppDeviceUtils.getTelephonyId(getActivity()));

        data[0] = URL_AFTERSAVE;
        data[1] = obj.toString();

        new NetworkHandlerString(getActivity(), networkCallBack, 99).execute(data);
    }

    private void saveFilter() throws JSONException {
        if(isFirst){
            URL_SAVEFILTERs = "https://www.mittalpower.com/mobile/pxs_app/service/schedule_track/savecode.php";
        }else{
            URL_SAVEFILTERs = domain + "/mobile/pxs_app/service/schedule_track/savecode.php";
        }
        JSONArray array = new JSONArray();
        getClientJsonArray(array);

        JSONObject object = new JSONObject();
        object.put("device_id", AppDeviceUtils.getTelephonyId(getActivity()));
        //object.put("company", companyName);
        //object.put("mobile", mobileNo);
        //object.put("email", email);
        object.put("detail", array);

        new NetworkHandlerModel(getActivity(), networkCallBack,
                SaveFilter.class, 1).execute(URL_SAVEFILTERs, object.toString());
    }

    private void getClientJsonArray(JSONArray array) throws JSONException {

        if (llregionandclientone.getVisibility() == View.VISIBLE) {
            JSONObject obj = new JSONObject();
            obj.put("region", spn_region_one.getSelectedItem().toString());
            String tmp = spn_clientname_one.getSelectedItem().toString();
            obj.put("code", tmp != null ? tmp : "");

            array.put(obj);
        }

        if (llregionandclienttwo.getVisibility() == View.VISIBLE) {
            JSONObject obj = new JSONObject();
            obj.put("region", spn_region_two.getSelectedItem().toString());
            String tmp = spn_clientname_two.getSelectedItem().toString();
            obj.put("code", tmp != null ? tmp : "");

            array.put(obj);
        }

        if (llregionandclientthree.getVisibility() == View.VISIBLE) {
            JSONObject obj = new JSONObject();
            obj.put("region", spn_region_three.getSelectedItem().toString());
            String tmp = spn_clientname_three.getSelectedItem().toString();
            obj.put("code", tmp != null ? tmp : "");

            array.put(obj);
        }

        if (llregionandclientfour.getVisibility() == View.VISIBLE) {
            JSONObject obj = new JSONObject();
            obj.put("region", spn_region_four.getSelectedItem().toString());
            String tmp = spn_clientname_four.getSelectedItem().toString();
            obj.put("code", tmp != null ? tmp : "");

            array.put(obj);
        }

        if (llregionandclientfive.getVisibility() == View.VISIBLE) {
            JSONObject obj = new JSONObject();
            obj.put("region", spn_region_five.getSelectedItem().toString());
            String tmp = spn_clientname_five.getSelectedItem().toString();
            obj.put("code", tmp != null ? tmp : "");

            array.put(obj);
        }
    }

    private void setFlowAdapter(List<CurrentFlowModel> flowNodel) {
        final AdapterMyScheduling adapter = new AdapterMyScheduling(getContext(), flowNodel, "-11");
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(),
                        MySchedulingDetailBlockActivity.class);

                if (adapter == null) {
                    AppLogger.showToastLong(getActivity(), "Some Error Occured");
                    return;
                }
                CurrentFlowModel myScheduleDatum = adapter.getData().get(position);
                intent.putExtra(Constant.VALUE_DATAID, myScheduleDatum.getId());
                intent.putExtra(Constant.DATA_APPROVAL, myScheduleDatum.getAppNo());
                intent.putExtra(Constant.DATA_REV, myScheduleDatum.getRevision());
                startActivity(intent);
            }
        });

    }

}