package com.pts.invetech.pojo;

import com.pts.model.realtimeschedule.RealTimeData;
import com.pts.model.realtimeschedule.RealTimeInnerData;
import com.pts.model.realtimeschedule.RealTimeIntermed;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Vaibhav on 11-08-2016.
 */
public class ParserRealTimeData {

    private String REALTIME_DATE="date";
    private String REALTIME_DATA="data";

    private String REALTIME_INNER_REGION="regionname";
    private String REALTIME_INNER_MAXREV="maxrevision";
    private String REALTIME_INNER_TOTALQTM="totalqtm";
    private String REALTIME_INNER_PERCEN="percentage";
    private String REALTIME_INNER_REVLIST="revision";

    public ParserRealTimeData(RealTimeData realTimeData,String jsonData) throws JSONException {
        parseJson(realTimeData,jsonData);
    }

    private void parseJson(RealTimeData realTimeData,String jsonData) throws JSONException {

        JSONObject object=new JSONObject(jsonData);

        if(object.has(REALTIME_DATE)){
            realTimeData.setDate(object.getString(REALTIME_DATE));
        }
        if(object.has(REALTIME_DATA)){
            parseInnerData(realTimeData,object.getString(REALTIME_DATA));
        }
    }


    private void parseInnerData(RealTimeData realTimeData,String string) throws JSONException {
        JSONObject object=new JSONObject(string);
        Iterator iterator = object.keys();

        while(iterator.hasNext()){
            String key = (String)iterator.next();
            JSONArray innerArr = object.getJSONArray(key);

            RealTimeIntermed interMedData=new RealTimeIntermed();
            interMedData.setName(key);

            for(int j=0;j<innerArr.length();j++){
                JSONObject innOb=innerArr.getJSONObject(j);
                RealTimeInnerData innderData=new RealTimeInnerData();

                innderData.setRegionname(innOb.getString(REALTIME_INNER_REGION));
                innderData.setMaxrevision(innOb.getString(REALTIME_INNER_MAXREV));
                innderData.setTotalqtm(innOb.getString(REALTIME_INNER_TOTALQTM));
                innderData.setPercentage(innOb.getString(REALTIME_INNER_PERCEN));
                innderData.setHeaderString(key);

                JSONArray revArr=innOb.getJSONArray(REALTIME_INNER_REVLIST);

                for(int index=0;index<revArr.length();index++){
                    innderData.addValueInRevision(revArr.getString(index));
                }
                interMedData.addInnerData(innderData);
            }
            realTimeData.addData(interMedData);
        }
    }

}