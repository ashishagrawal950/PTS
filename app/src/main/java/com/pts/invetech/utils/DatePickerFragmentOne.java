package com.pts.invetech.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

import android.widget.DatePicker;
import android.widget.TextView;

import com.pts.invetech.R;

import java.util.Calendar;

/**
 * Created by Ashish on 21-02-2017.
 */

public class DatePickerFragmentOne extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        populateSetDate(yy, mm + 1, dd);
    }

    public void populateSetDate(int year, int month, int day) {
        TextView tv_datepickerone= (TextView) getActivity().findViewById(R.id.tv_datepickerone);
        if ((month <= 9) && (day < 10)) {
            String monthplace = "0" + month;
            String dayplace = "0" + day;
            tv_datepickerone.setText(dayplace + "-" + monthplace + "-" + year);
        }
        else if (month <= 9) {
            String monthplace = "0" + month;
            tv_datepickerone.setText(day + "-" + monthplace + "-" + year);
        }
        else if (day < 10) {
            String dayplace = "0" + day;
            tv_datepickerone.setText(dayplace + "-" + month + "-" + year);
        }
        else {
            tv_datepickerone.setText(day + "-" + month + "-" + year);
        }
    }
}
/*

public class SelectDateFragment extends android.app.DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        populateSetDate(yy, mm + 1, dd);
    }

    public void populateSetDate(int year, int month, int day) {
        // fromDateEtxt.setText(month + "/" + day + "/" + year);
        if ((month <= 9) && (day < 10)) {
            String monthplace = "0" + month;
            String dayplace = "0" + day;
            etxt_fromdate.setText(dayplace + "-" + monthplace + "-" + year);
            etxt_fromdate_set.setText(year + "-" + monthplace + "-" + dayplace);
            as = etxt_fromdate_set.getText().toString();
            // Toast.makeText(getActivity(), "DownloadResult" +
            // as,Toast.LENGTH_LONG).show();
        }
        else if (month <= 9) {
            String monthplace = "0" + month;
            etxt_fromdate.setText(day + "-" + monthplace + "-" + year);
            etxt_fromdate_set.setText(year + "-" + monthplace + "-" + day);
            as = etxt_fromdate_set.getText().toString();
        }
        else if (day < 10) {
            String dayplace = "0" + day;
            etxt_fromdate.setText(dayplace + "-" + month + "-" + year);
            etxt_fromdate_set.setText(year + "-" + month + "-" + dayplace);
            as = etxt_fromdate_set.getText().toString();
        }
        else {
            etxt_fromdate.setText(day + "-" + month + "-" + year);
            etxt_fromdate_set.setText(year + "-" + month + "-" + day);
            as = etxt_fromdate_set.getText().toString();
        }
    }
}*/
