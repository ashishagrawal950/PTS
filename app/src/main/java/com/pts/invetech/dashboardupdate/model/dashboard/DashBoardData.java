
package com.pts.invetech.dashboardupdate.model.dashboard;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashBoardData {

    @SerializedName("urs")
    @Expose
    private List<Ur> urs = null;
    @SerializedName("scheduling")
    @Expose
    private List<Scheduling> scheduling = null;
    @SerializedName("mcp")
    @Expose
    private List<Mcp> mcp = null;
    @SerializedName("losses")
    @Expose
    private List<Loss> losses = null;
    @SerializedName("stateload")
    @Expose
    private Stateload stateload;

    public List<Ur> getUrs() {
        return urs;
    }

    public void setUrs(List<Ur> urs) {
        this.urs = urs;
    }

    public List<Scheduling> getScheduling() {
        return scheduling;
    }

    public void setScheduling(List<Scheduling> scheduling) {
        this.scheduling = scheduling;
    }

    public List<Mcp> getMcp() {
        return mcp;
    }

    public void setMcp(List<Mcp> mcp) {
        this.mcp = mcp;
    }

    public List<Loss> getLosses() {
        return losses;
    }

    public void setLosses(List<Loss> losses) {
        this.losses = losses;
    }

    public Stateload getStateload() {
        return stateload;
    }

    public void setStateload(Stateload stateload) {
        this.stateload = stateload;
    }

}
