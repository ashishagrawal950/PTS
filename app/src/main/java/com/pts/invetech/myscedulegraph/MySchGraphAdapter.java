package com.pts.invetech.myscedulegraph;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.myscedulegraph.model.MyScheduleDetailData;

import java.util.List;

/**
 * Created by vaibhav on 24/5/17.
 */

public class MySchGraphAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<MyScheduleDetailData> schList;

    public MySchGraphAdapter(Context _context,List<MyScheduleDetailData> _data){

        schList=_data;
        inflater=LayoutInflater.from(_context);
    }


    @Override
    public int getCount() {
        return schList.size();
    }

    @Override
    public MyScheduleDetailData getItem(int i) {
        return schList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return schList.get(i).hashCode();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view==null){
            view=inflater.inflate(R.layout.row_sch_graph,null);
        }

        MyScheduleDetailData tmp=getItem(i);

        TextView tv1= (TextView) view.findViewById(R.id.grp_txt_1);
        TextView tv2= (TextView) view.findViewById(R.id.grp_txt_2);
        TextView tv3= (TextView) view.findViewById(R.id.grp_txt_3);
        TextView tv4= (TextView) view.findViewById(R.id.grp_txt_4);

        tv1.setText(""+tmp.getBlock());
        tv2.setText(tmp.getTime());

        if(tmp.getPQtm()!=null){
            tv3.setText(tmp.getPQtm());
        }else{
            tv3.setText("NA");
        }

        if(tmp.getCQtm()!=null){
            tv4.setText(tmp.getCQtm());
        }else{
            tv4.setText("NA");
        }


        if(tmp.getPQtm()!=null && tmp.getCQtm()!=null){
            float prr=Float.parseFloat(tmp.getPQtm());
            float crr=Float.parseFloat(tmp.getCQtm());
            if(prr!=crr){
                tv3.setTextColor(Color.RED);
                tv4.setTextColor(Color.RED);
            }else{
                tv3.setTextColor(Color.BLACK);
                tv4.setTextColor(Color.BLACK);
            }
        }

        return view;
    }
}
