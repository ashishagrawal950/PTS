package com.pts.invetech.marquee;

import android.content.Context;
import android.graphics.PointF;
import androidx.recyclerview.widget.LinearSmoothScroller;
import android.util.DisplayMetrics;

/**
 * Created by Vaibhav on 09-08-2016.
 */
public class MySmoothScroller extends LinearSmoothScroller {

    private MyLayoutManager mManager;
    private final float MILLISECONDS_PER_INCH = 3000f;

    public MySmoothScroller(Context context,MyLayoutManager manager){
        super(context);
        mManager=manager;
    }

    @Override
    protected void onStart() {
        super.onStart();
      //  AppLogger.showMsgWithoutTag("Smooth Start");
    }

    @Override
    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
        return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
    }

    @Override
    public PointF computeScrollVectorForPosition(int targetPosition) {
        return mManager.computeScrollVectorForPosition(targetPosition);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
