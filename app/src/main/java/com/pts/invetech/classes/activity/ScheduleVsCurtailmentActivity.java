package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.CurtailmentAPIResponse;
import com.pts.invetech.apiresponse.CurtailmentGetAPIResponse;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.customlist.ScheduleVsCurtailmentCustomList;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.pojo.Curtailment;
import com.pts.invetech.pojo.MarketPriceIEXDetails;
import com.pts.invetech.pojo.MarketPricePXILDetails;
import com.pts.invetech.utils.JSONUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

public class ScheduleVsCurtailmentActivity extends Activity implements OnItemSelectedListener {


    private Dialog prgDialog;
    private LinearLayout mopenLayout, openPopupMarketPrice;
    private TextView fromDateEtxt, toDateEtxt, tvtradingdate_set;
    private Spinner tvtradingdate;

    private NonScrollListView marketpicelist;

    private HashMap<String, MarketPriceIEXDetails> myIEXHashmap;
    private HashMap<String, MarketPricePXILDetails> myPXILHashmap;
    private SQLiteDatabase db;
    private String dbacess;
    //   CustomScrollView scrollmarketprice;
    private HomeActivity homeActivity;
    private String as;
    private Button btgoDownload;
    private String bbb = "{[\"0\",\"1\"]}";
    //String abc = "{\"1\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"2\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"3\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"4\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"5\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"6\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"7\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"8\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"9\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"10\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"11\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"12\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"13\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"14\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"15\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"16\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"17\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"18\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"19\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"20\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"21\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"22\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"23\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"24\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"25\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"26\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"27\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"28\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"29\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"30\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"31\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"32\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"33\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"34\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"35\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"36\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"37\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"38\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"39\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"40\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"41\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"42\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"43\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"44\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"45\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"46\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"47\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"48\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"49\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"50\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"51\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"52\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"53\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"54\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"55\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"56\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"57\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"58\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"59\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"60\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"61\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"62\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"63\":{\"actual\":\"1.30\",\"revised\":\"1.69\"},\"64\":{\"actual\":\"1.30\",\"revised\":\"1.69\"},\"65\":{\"actual\":\"1.30\",\"revised\":\"1.68\"},\"66\":{\"actual\":\"1.30\",\"revised\":\"1.68\"},\"67\":{\"actual\":\"1.30\",\"revised\":\"1.67\"},\"68\":{\"actual\":\"1.30\",\"revised\":\"1.67\"},\"69\":{\"actual\":\"1.30\",\"revised\":\"1.63\"},\"70\":{\"actual\":\"1.30\",\"revised\":\"1.59\"},\"71\":{\"actual\":\"1.30\",\"revised\":\"1.19\"},\"72\":{\"actual\":\"1.30\",\"revised\":\"1.08\"},\"73\":{\"actual\":\"1.30\",\"revised\":\"1.65\"},\"74\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"75\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"76\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"77\":{\"actual\":\"1.30\",\"revised\":\"1.30\"},\"78\":{\"actual\":\"1.30\",\"revised\":\"1.75\"},\"79\":{\"actual\":\"1.30\",\"revised\":\"1.68\"},\"80\":{\"actual\":\"1.30\",\"revised\":\"1.6\"},\"81\":{\"actual\":\"1.30\",\"revised\":\"1.39\"},\"82\":{\"actual\":\"1.30\",\"revised\":\"1.27\"},\"83\":{\"actual\":\"1.30\",\"revised\":\"1.28\"},\"84\":{\"actual\":\"1.30\",\"revised\":\"1.23\"},\"85\":{\"actual\":\"1.30\",\"revised\":\"1.58\"},\"86\":{\"actual\":\"1.30\",\"revised\":\"1.43\"},\"87\":{\"actual\":\"1.30\",\"revised\":\"1.59\"},\"88\":{\"actual\":\"1.30\",\"revised\":\"1.45\"},\"89\":{\"actual\":\"1.30\",\"revised\":\"0.43\"},\"90\":{\"actual\":\"1.30\",\"revised\":\"0.34\"},\"91\":{\"actual\":\"1.30\",\"revised\":\"0.45\"},\"92\":{\"actual\":\"1.30\",\"revised\":\"0.44\"},\"93\":{\"actual\":\"1.30\",\"revised\":\"0.33\"},\"94\":{\"actual\":\"1.30\",\"revised\":\"0.46\"},\"95\":{\"actual\":\"1.30\",\"revised\":\"0.5\"},\"96\":{\"actual\":\"1.30\",\"revised\":\"0.54\"}}";
    private ArrayList<String> blockarrayListfrom = new ArrayList<String>();
    private ArrayList<String> blockarrayList = new ArrayList<String>();
    private ArrayList<String> actualarrayList = new ArrayList<String>();
    private ArrayList<String> revisedarrayList = new ArrayList<String>();
    private ArrayList<String> curtailmentarrayList = new ArrayList<String>();

    private ArrayList<String> blockarraygraphList = new ArrayList<String>();
    private ArrayList<String> actualarraygraphList = new ArrayList<String>();
    private ArrayList<String> revisedarraygraphList = new ArrayList<String>();
    private ArrayList<String> curtailmentgrapharrayList = new ArrayList<String>();

    private ArrayList<String> revisionarrayList = new ArrayList<String>();

    private Spinner sprevision;
    private LinearLayout chartContainer;
    private ArrayAdapter<String> adapter_revision, adapter_date;
    private String sprevision_send, spdate_send;
    private LinearLayout tvviewgraph, lldaterevsion, lltableheading, llfromnotification;

    private ArrayList<String> key_element = new ArrayList<String>();
    private ArrayList<String> key_element_search = new ArrayList<String>();
    private TextView tvnotificationnull, tvnotificationnulltext, belowline, tvdatemassage, tvnselectdate;
    private JSONObject jsonObjectGlobal;
    private LinearLayout DataNotFound_Layout;
    private String domain;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_schedulevscurtailment);
        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM baby", null);
        if (c.getCount() == 0) {
            showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("access_key: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");

		/*prgDialog = new ProgressDialog(ScheduleVsCurtailmentActivity.this);
	    prgDialog.setMessage("Please wait...");
	    prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);
        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        tvtradingdate = (Spinner) findViewById(R.id.tvtradingdate);
        tvtradingdate.setOnItemSelectedListener(this);
        tvtradingdate_set = (TextView) findViewById(R.id.tvtradingdate_set);
        sprevision = (Spinner) findViewById(R.id.sprevision);
        sprevision.setOnItemSelectedListener(this);
        chartContainer = (LinearLayout) findViewById(R.id.marketpricechart);
        tvnotificationnull = (TextView) findViewById(R.id.tvnotificationnull);
        tvnotificationnulltext = (TextView) findViewById(R.id.tvnotificationnulltext);
        lldaterevsion = (LinearLayout) findViewById(R.id.lldaterevsion);
        lltableheading = (LinearLayout) findViewById(R.id.lltableheading);
        belowline = (TextView) findViewById(R.id.belowline);
        llfromnotification = (LinearLayout) findViewById(R.id.llfromnotification);
        tvdatemassage = (TextView) findViewById(R.id.tvdatemassage);
        tvnselectdate = (TextView) findViewById(R.id.tvnselectdate);
        btgoDownload = (Button) findViewById(R.id.btgoDownload);
        DataNotFound_Layout = (LinearLayout) findViewById(R.id.DataNotFound_Layout);

        Intent in = getIntent();
        sprevision_send = in.getStringExtra("revision");
        spdate_send = in.getStringExtra("date");
        if (sprevision_send.equalsIgnoreCase("revision") && spdate_send.equalsIgnoreCase("date")) {
            // Nothing for call
            lldaterevsion.setVisibility(View.VISIBLE);
            tvtradingdate.setVisibility(View.VISIBLE);
            sprevision.setVisibility(View.VISIBLE);
            btgoDownload.setVisibility(View.VISIBLE);
            llfromnotification.setVisibility(View.GONE);
        } else {
            ConnectionDetector cd = new ConnectionDetector(ScheduleVsCurtailmentActivity.this);
            if (!cd.isConnectingToInternet()) {
                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
            } else {
                String[] ddmmyyfrom = spdate_send.split("-");
                String yyf = ddmmyyfrom[0];
                String mmf = ddmmyyfrom[1];
                String ddf = ddmmyyfrom[2];
                String dateformate = ddf + "-" + mmf + "-" + yyf;
                lldaterevsion.setVisibility(View.GONE);
                llfromnotification.setVisibility(View.VISIBLE);
                tvdatemassage.setText("Showing Curtailment for Trading Date: ");
                tvnselectdate.setPaintFlags(tvnselectdate.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                tvnselectdate.setText(dateformate);

                new HttpAsyncTaskgetcurtailmentinfo().execute(domain + "/mobile/pxs_app/service/curtailment/getcurtailmentinfo.php");
            }
        }


        ConnectionDetector cd = new ConnectionDetector(ScheduleVsCurtailmentActivity.this);
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            new HttpAsyncTaskgetrevision().execute(domain + "/mobile/pxs_app/service/curtailment/getrevision.php");
        }

        tvnselectdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                lldaterevsion.setVisibility(View.VISIBLE);
                llfromnotification.setVisibility(View.GONE);
            }
        });


        blockarrayListfrom.add("00:15");
        blockarrayListfrom.add("00:15");
        blockarrayListfrom.add("00:30");
        blockarrayListfrom.add("00:45");
        blockarrayListfrom.add("01:00");

        blockarrayListfrom.add("01:15");
        blockarrayListfrom.add("01:30");
        blockarrayListfrom.add("01:45");
        blockarrayListfrom.add("02:00");

        blockarrayListfrom.add("02:15");
        blockarrayListfrom.add("02:30");
        blockarrayListfrom.add("02:45");
        blockarrayListfrom.add("03:00");

        blockarrayListfrom.add("03:15");
        blockarrayListfrom.add("03:30");
        blockarrayListfrom.add("03:45");
        blockarrayListfrom.add("04:00");

        blockarrayListfrom.add("04:15");
        blockarrayListfrom.add("04:30");
        blockarrayListfrom.add("04:45");
        blockarrayListfrom.add("05:00");

        blockarrayListfrom.add("05:15");
        blockarrayListfrom.add("05:30");
        blockarrayListfrom.add("05:45");
        blockarrayListfrom.add("06:00");

        blockarrayListfrom.add("06:15");
        blockarrayListfrom.add("06:30");
        blockarrayListfrom.add("06:45");
        blockarrayListfrom.add("07:00");

        blockarrayListfrom.add("07:15");
        blockarrayListfrom.add("07:30");
        blockarrayListfrom.add("07:45");
        blockarrayListfrom.add("08:00");

        blockarrayListfrom.add("08:15");
        blockarrayListfrom.add("08:30");
        blockarrayListfrom.add("08:45");
        blockarrayListfrom.add("09:00");

        blockarrayListfrom.add("09:15");
        blockarrayListfrom.add("09:30");
        blockarrayListfrom.add("09:45");
        blockarrayListfrom.add("10:00");

        blockarrayListfrom.add("10:15");
        blockarrayListfrom.add("10:30");
        blockarrayListfrom.add("10:45");
        blockarrayListfrom.add("11:00");

        blockarrayListfrom.add("11:15");
        blockarrayListfrom.add("11:30");
        blockarrayListfrom.add("11:45");
        blockarrayListfrom.add("12:00");

        blockarrayListfrom.add("12:15");
        blockarrayListfrom.add("12:30");
        blockarrayListfrom.add("12:45");
        blockarrayListfrom.add("13:00");

        blockarrayListfrom.add("13:15");
        blockarrayListfrom.add("13:30");
        blockarrayListfrom.add("13:45");
        blockarrayListfrom.add("14:00");

        blockarrayListfrom.add("14:15");
        blockarrayListfrom.add("14:30");
        blockarrayListfrom.add("14:45");
        blockarrayListfrom.add("15:00");

        blockarrayListfrom.add("15:15");
        blockarrayListfrom.add("15:30");
        blockarrayListfrom.add("15:45");
        blockarrayListfrom.add("16:00");

        blockarrayListfrom.add("16:15");
        blockarrayListfrom.add("16:30");
        blockarrayListfrom.add("16:45");
        blockarrayListfrom.add("17:00");

        blockarrayListfrom.add("17:15");
        blockarrayListfrom.add("17:30");
        blockarrayListfrom.add("17:45");
        blockarrayListfrom.add("18:00");

        blockarrayListfrom.add("18:15");
        blockarrayListfrom.add("18:30");
        blockarrayListfrom.add("18:45");
        blockarrayListfrom.add("19:00");

        blockarrayListfrom.add("19:15");
        blockarrayListfrom.add("19:30");
        blockarrayListfrom.add("19:45");
        blockarrayListfrom.add("20:00");

        blockarrayListfrom.add("20:15");
        blockarrayListfrom.add("20:30");
        blockarrayListfrom.add("20:45");
        blockarrayListfrom.add("21:00");

        blockarrayListfrom.add("21:15");
        blockarrayListfrom.add("21:30");
        blockarrayListfrom.add("21:45");
        blockarrayListfrom.add("22:00");

        blockarrayListfrom.add("22:15");
        blockarrayListfrom.add("22:30");
        blockarrayListfrom.add("22:45");
        blockarrayListfrom.add("23:00");

        blockarrayListfrom.add("23:15");
        blockarrayListfrom.add("23:30");
        blockarrayListfrom.add("23:45");
        blockarrayListfrom.add("24:00");

        myIEXHashmap = new HashMap<String, MarketPriceIEXDetails>();
        myPXILHashmap = new HashMap<String, MarketPricePXILDetails>();


        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ScheduleVsCurtailmentActivity.this, MainActivityAfterLogin.class);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();

            }
        });
        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });


        marketpicelist = (NonScrollListView) findViewById(R.id.marketpicelist);
		
		/*tvtradingdate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {

				DialogFragment newFragment = new SelectDateFragment();
				newFragment.show(getFragmentManager(), "DatePicker");
				
			}
		});*/

        tvviewgraph = (LinearLayout) findViewById(R.id.tvviewgraph);
        tvviewgraph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent in = new Intent(ScheduleVsCurtailmentActivity.this, ScheduleVsCurtailmenGraphtActivity.class);
                //blockarrayList.add(block);
                //actualarrayList.add(actual);
                //curtailmentarrayList.add(curtailment);
                //revisedarrayList.add(revised);
                in.putStringArrayListExtra("block", blockarrayList);
                in.putStringArrayListExtra("actual", actualarrayList);
                in.putStringArrayListExtra("curtailment", curtailmentarrayList);
                in.putStringArrayListExtra("revised", revisedarrayList);
                in.putExtra("revision", sprevision_send);
                in.putExtra("date", spdate_send);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
            }
        });


        btgoDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                sprevision_send = sprevision.getSelectedItem().toString();
                String spdate = tvtradingdate.getSelectedItem().toString();
                if (spdate.equalsIgnoreCase("Select Date")) {
                    Toast.makeText(ScheduleVsCurtailmentActivity.this, "Please Select Date.", Toast.LENGTH_LONG).show();
                } else if (sprevision_send.equalsIgnoreCase("Select Revision")) {
                    Toast.makeText(ScheduleVsCurtailmentActivity.this, "Please Select Revision.", Toast.LENGTH_LONG).show();
                } else {
                    String[] parts = spdate.split("-");
                    String dd = parts[0];
                    String mm = parts[1];
                    String yy = parts[2];
                    spdate_send = (yy + "-" + mm + "-" + dd);
                    new HttpAsyncTaskgetcurtailmentinfo().execute(domain + "/mobile/pxs_app/service/curtailment/getcurtailmentinfo.php");
                }
            }
        });

    }

    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        Spinner spinrevision = (Spinner) parent;
        Spinner spintradingdate = (Spinner) parent;

        if (spintradingdate.getId() == R.id.tvtradingdate) {
            //Toast.makeText(ScheduleVsCurtailmentActivity.this, key_element_search.get(position), Toast.LENGTH_LONG).show();
            if (key_element_search.contains(spdate_send)) {
                String[] parts = spdate_send.split("-");
                String yyyy = parts[0];
                String mmmm = parts[1];
                String dddd = parts[2];
                int k = key_element.indexOf(dddd + "-" + mmmm + "-" + yyyy);
                tvtradingdate.setSelection(k);
                revisionarrayList.clear();
                revisionarrayList.add("Select Revision");
                try {
                    JSONArray keyResult = jsonObjectGlobal.getJSONArray(spdate_send);
                    for (int i = 0; i < keyResult.length(); i++) {
                        String revision = keyResult.getString(i);
                        revisionarrayList.add(revision);
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                }
                adapter_revision = new ArrayAdapter<String>(
                        ScheduleVsCurtailmentActivity.this,
                        R.layout.spinner_item,
                        revisionarrayList);
                adapter_revision
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sprevision.setAdapter(adapter_revision);
                int j = revisionarrayList.indexOf(sprevision_send);
                sprevision.setSelection(j);
                sprevision.setVisibility(View.VISIBLE);
                btgoDownload.setVisibility(View.VISIBLE);
            } else if (key_element_search.get(position).equalsIgnoreCase("Select Date")) {
                revisionarrayList.clear();
                revisionarrayList.add("Select Revision");
                adapter_revision = new ArrayAdapter<String>(
                        ScheduleVsCurtailmentActivity.this,
                        R.layout.spinner_item,
                        revisionarrayList);
                adapter_revision
                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sprevision.setAdapter(adapter_revision);
                sprevision.setVisibility(View.VISIBLE);
                btgoDownload.setVisibility(View.VISIBLE);
            } else {
                revisionarrayList.clear();
                revisionarrayList.add("Select Revision");
                try {
                    JSONArray keyResult = jsonObjectGlobal
                            .getJSONArray(key_element_search.get(position));
                    for (int i = 0; i < keyResult.length(); i++) {
                        String revision = keyResult.getString(i);
                        revisionarrayList.add(revision);
                    }
                    adapter_revision = new ArrayAdapter<String>(
                            ScheduleVsCurtailmentActivity.this,
                            R.layout.spinner_item,
                            revisionarrayList);
                    adapter_revision
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    sprevision.setAdapter(adapter_revision);
                    sprevision.setVisibility(View.VISIBLE);
                    btgoDownload.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            //int k = key_element.indexOf(spdate_send);
            //tvtradingdate.setSelection(k);
            //int j = revisionarrayList.indexOf(sprevision_send);
            //sprevision.setSelection(j);
        }
        if (spinrevision.getId() == R.id.sprevision) {

        }

        // selVersion.setText("Selected Android OS:" + selState);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }

    public void showMessage(String title, String message) {
        Builder builder = new Builder(ScheduleVsCurtailmentActivity.this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(ScheduleVsCurtailmentActivity.this, MainActivityAfterLogin.class);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    public class SelectDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                //tvtradingdate.setText(dayplace + "-" + monthplace + "-" + year);
                tvtradingdate_set.setText(year + "-" + monthplace + "-"
                        + dayplace);
                as = tvtradingdate_set.getText().toString();
                // Toast.makeText(getActivity(), "DownloadResult" +
                // as,Toast.LENGTH_LONG).show();
            } else if (month <= 9) {
                String monthplace = "0" + month;
                //tvtradingdate.setText(day + "-" + monthplace + "-" + year);
                tvtradingdate_set.setText(year + "-" + monthplace + "-" + day);
                as = tvtradingdate_set.getText().toString();
            } else if (day < 10) {
                String dayplace = "0" + day;
                //tvtradingdate.setText(dayplace + "-" + month + "-" + year);
                tvtradingdate_set.setText(year + "-" + month + "-" + dayplace);
                as = tvtradingdate_set.getText().toString();
            } else {
                //tvtradingdate.setText(day + "-" + month + "-" + year);
                tvtradingdate_set.setText(year + "-" + month + "-" + day);
                as = tvtradingdate_set.getText().toString();
                // Toast.makeText(getActivity(), "DownloadResult" +
                // as,Toast.LENGTH_LONG).show();
                // (month + "/" + day + "/" + year);
                // (year + "-" + month + "-" + day);
            }
            new CountDownTimer(1000, 1000) {
                public void onTick(long millisUntilFinished) {
                    // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    // mTextField.setText("done!");
                    new HttpAsyncTaskgetrevision().execute(domain + "/mobile/pxs_app/service/curtailment/getrevision.php");
                }
            }.start();
        }

    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm Date, DeviceRegister.access_key
     */
    private class HttpAsyncTaskgetrevision extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Curtailment curtailment = new Curtailment();
            //curtailment.setDate(as);
            curtailment.setDbaccess(dbacess);
            return CurtailmentAPIResponse.POST(urls[0], curtailment);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Curtailemnt Result:", result);
            prgDialog.hide();
            // Toast.makeText(getActivity(), "Curtailemnt" +
            // result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    jsonObjectGlobal = new JSONObject(result);
                    if (jsonObjectGlobal.length() == 0) {
                        //tvnotificationnull.setBackgroundResource(R.drawable.sad);
                        //tvnotificationnulltext.setText("No Curtailment found.");
                        DataNotFound_Layout.setVisibility(View.VISIBLE);
                        tvtradingdate.setVisibility(View.GONE);
                        sprevision.setVisibility(View.GONE);
                        btgoDownload.setVisibility(View.GONE);
                        tvnotificationnull.setVisibility(View.VISIBLE);
                        tvnotificationnulltext.setVisibility(View.VISIBLE);
                        lldaterevsion.setVisibility(View.GONE);
                    } else {
                        //lldaterevsion.setVisibility(View.VISIBLE);
                        DataNotFound_Layout.setVisibility(View.GONE);
                        //tvnotificationnull.setVisibility(View.GONE);
                        //tvnotificationnulltext.setVisibility(View.GONE);
                        key_element.clear();
                        key_element.add("Select Date");
                        key_element_search.add("Select Date");
                        Iterator<String> iter = jsonObjectGlobal.keys();
                        while (iter.hasNext()) {
                            String key = iter.next();
                            String[] parts = key.split("-");
                            String yyyy = parts[0];
                            String mmmm = parts[1];
                            String dddd = parts[2];
                            key_element.add(dddd + "-" + mmmm + "-" + yyyy);
                            key_element_search.add(key);
                            tvtradingdate.setVisibility(View.VISIBLE);
                        }
                        adapter_date = new ArrayAdapter<String>(
                                ScheduleVsCurtailmentActivity.this,
                                R.layout.spinner_item,
                                key_element);
                        adapter_date
                                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        tvtradingdate.setAdapter(adapter_date);
						/*try {
							revisionarrayList.clear();
							revisionarrayList.add("Select Revision");
							for (int l = 0; l < key_element_search.size(); l++) {
								JSONArray keyResult = jsonObjectGlobal
										.getJSONArray(key_element_search.get(l));
								for (int i = 0; i < keyResult.length(); i++) {
									String revision = keyResult.getString(i);
									revisionarrayList.add(revision);
								}
								adapter_revision = new ArrayAdapter<String>(
										ScheduleVsCurtailmentActivity.this,
										R.layout.spinner_item,
										revisionarrayList);
								adapter_revision
										.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
								sprevision.setAdapter(adapter_revision);
								sprevision.setVisibility(View.VISIBLE);
								btgoDownload.setVisibility(View.VISIBLE);
							}
						} catch (JSONException e) {
							// Something went wrong!
						}*/
                    }
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //tvnotificationnull.setBackgroundResource(R.drawable.sad);
                        //tvnotificationnulltext.setText("No Curtailment found.");
                        DataNotFound_Layout.setVisibility(View.VISIBLE);
                        tvtradingdate.setVisibility(View.GONE);
                        sprevision.setVisibility(View.GONE);
                        btgoDownload.setVisibility(View.GONE);
                        tvnotificationnull.setVisibility(View.VISIBLE);
                        tvnotificationnulltext.setVisibility(View.VISIBLE);
                        lldaterevsion.setVisibility(View.GONE);
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(
                                getApplicationContext(),
                                "Server Side Issue: " + title
                                        + ", Please Contact to Admin.",
                                Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(
                        getApplicationContext(),
                        "Server Side Issue: " + title
                                + ", Please Contact to Admin.",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(
                        getApplicationContext(),
                        "Response Invalid from Server: Please Contact to Admin.",
                        Toast.LENGTH_LONG).show();
            }
        }

    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm Date, DeviceRegister.access_key
     */

    private class HttpAsyncTaskgetcurtailmentinfo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Curtailment curtailment = new Curtailment();
            curtailment.setDate(spdate_send);
            curtailment.setDbaccess(dbacess);
            curtailment.setRevision(sprevision_send);
            return CurtailmentGetAPIResponse.POST(urls[0], curtailment);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Curtailment Result:", result);
            prgDialog.hide();
            //  Toast.makeText(getActivity(), "NoBidSave" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject1 = new JSONObject(result);
                    if (mainObject1.length() == 0) {
                        lltableheading.setVisibility(View.GONE);
                        belowline.setVisibility(View.GONE);
                        marketpicelist.setVisibility(View.GONE);
                        tvviewgraph.setVisibility(View.GONE);
                    } else {
                        lltableheading.setVisibility(View.VISIBLE);
                        belowline.setVisibility(View.VISIBLE);
                        marketpicelist.setVisibility(View.VISIBLE);
                        tvviewgraph.setVisibility(View.VISIBLE);
                        blockarrayList.clear();

                        for (int j = 1; j <= mainObject1.length(); j++) {
                            String str = Integer.toString(j);
                            JSONObject jsonArray = mainObject1.getJSONObject(str);
                            //Toast.makeText(ScheduleVsCurtailmentActivity.this, "Ashish", Toast.LENGTH_LONG).show();
                            String block = blockarrayListfrom.get(j);
                            String actual = jsonArray.getString("actual");
                            String revised = jsonArray.getString("revised");
                            Float actualint = Float.parseFloat(actual);
                            Float revisedint = Float.parseFloat(revised);
                            Float curtailmentint = (actualint - revisedint);
                            String curtailment = Float.toString(curtailmentint);
                            blockarrayList.add(block);
                            actualarrayList.add(actual);
                            curtailmentarrayList.add(curtailment);
                            revisedarrayList.add(revised);
                            if (curtailmentint != 0) {
                                blockarraygraphList.add(block);
                                actualarraygraphList.add(actual);
                                revisedarraygraphList.add(revised);
                                curtailmentgrapharrayList.add(curtailment);
                            }
                        }
                        ScheduleVsCurtailmentCustomList adapter = new ScheduleVsCurtailmentCustomList(
                                ScheduleVsCurtailmentActivity.this, blockarrayList, actualarrayList, curtailmentarrayList, revisedarrayList);
                        marketpicelist.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }

    }

}