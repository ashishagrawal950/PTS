package com.pts.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ashish on 27-07-2017.
 */

public class PowerDemandComparision implements Parcelable {

    private String state;
    private String id;
    private String demand;
    private String peak;

    public PowerDemandComparision(){}

    public String getDemand() {
        return demand;
    }

    public void setDemand(String demand) {
        this.demand = demand;
    }

    public String getPeak() {
        return peak;
    }

    public void setPeak(String peak) {
        this.peak = peak;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    protected PowerDemandComparision(Parcel in) {
        state = in.readString();
        demand = in.readString();
        peak = in.readString();
        id=in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(state);
        dest.writeString(demand);
        dest.writeString(peak);
        dest.writeString(id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PowerDemandComparision> CREATOR = new Parcelable.Creator<PowerDemandComparision>() {
        @Override
        public PowerDemandComparision createFromParcel(Parcel in) {
            return new PowerDemandComparision(in);
        }

        @Override
        public PowerDemandComparision[] newArray(int size) {
            return new PowerDemandComparision[size];
        }
    };
}