package com.pts.handler;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pts.invetech.R;
import com.pts.invetech.utils.AppLogger;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkHandlerModel extends AsyncTask<String, Void, Object> {

    private NetworkCallBack mActivity = null;
    private Dialog prgDialog = null;
    private int currentId = -1;
    private Class currentClass;
    private Context mcontext ;

    public NetworkHandlerModel(Context context, NetworkCallBack splsh,
                               Class currentClass ,int id) {
        currentId = id;
        mActivity = splsh;
        mcontext = context;
       // prgDialog = new ProgressDialog(context);

        this.currentClass=currentClass;
    }

    @Override
    protected void onPreExecute() {

        if(prgDialog==null){
            prgDialog = new Dialog(mcontext);
            prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            prgDialog.setContentView(R.layout.progessdialog);
            ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
            Animation animation = AnimationUtils.loadAnimation(mcontext, R.anim.zoom);
            image.startAnimation(animation);
            prgDialog.setCancelable(false);
        }

        prgDialog.show();

        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(String... params) {
        Object reader = networkCall(params[0], params[1]);
        return reader;
    }

    @Override
    protected void onPostExecute(Object res) {
        mActivity.onResultObject(res, currentId);
        if ((prgDialog != null) && prgDialog.isShowing()) {
            prgDialog.dismiss();
        }
        prgDialog=null;
    }

    private Object networkCall(String currentUrl, String jsonParam) {
        URL url = null;
        HttpURLConnection connection = null;
        Object mObject = null;
        try {
            url = new URL(currentUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
            if (jsonParam != null) {
                dStream.writeBytes("data=" + jsonParam);
            }
            dStream.flush();
            dStream.close();
            AppLogger.showMsgWithoutTag(currentUrl + "  ,  " + "data=" + jsonParam);
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream content = connection.getInputStream();
                Reader reader = new InputStreamReader(content);

                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();

                mObject = gson.fromJson(reader, currentClass);

                content.close();
            }
        } catch (Exception e) {
            AppLogger.showMsg("Error in Network Handler Model", e.getMessage());
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return mObject;
    }
}