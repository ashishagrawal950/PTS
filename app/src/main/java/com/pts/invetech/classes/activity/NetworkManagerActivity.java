package com.pts.invetech.classes.activity;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.pts.invetech.APICalling.AppBaseActivity;
import com.pts.invetech.R;
import com.pts.invetech.utils.AppLogger;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkManagerActivity extends AsyncTask<Void, Void, String> {


    private String currentUrl = null;
    private JSONObject jsonParam = null;
    private AppBaseActivity mActivity = null;
    private Dialog prgDialog = null;

    public NetworkManagerActivity(AppBaseActivity splsh, JSONObject obj, String url) {
        currentUrl = url;
        jsonParam = obj;
        mActivity = splsh;
        prgDialog = new ProgressDialog(splsh);
    }

    @Override
    protected void onPreExecute() {
        /*progress.setMessage("Please Wait");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();*/
        prgDialog = new Dialog(mActivity);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(mActivity, R.anim.zoom);
        image.startAnimation(animation);
        prgDialog.show();
        prgDialog.setCancelable(false);
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {
        String reader = networkCall();
        return reader;
    }

    @Override
    protected void onPostExecute(String res) {
        mActivity.updateStringResult(res);
        if (prgDialog != null) {
            prgDialog.dismiss();
        }
    }

    private String networkCall() {
        URL url = null;
        HttpURLConnection connection = null;
        String response = "";
        try {
            url = new URL(currentUrl);

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);

            DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
            if (jsonParam != null) {
                dStream.writeBytes("data=" + jsonParam.toString());
            }
            dStream.flush();
            dStream.close();

            int responseCode = connection.getResponseCode();

            AppLogger.showMsg("response code", "" + responseCode);
//            AppLogger.showMsg("URL", "" + connection.getURL());

            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            }else{
                response=null;
            }

        } catch (Exception e) {
            AppLogger.showMsg("Error in Fragment async", e.getMessage());
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return response;
    }
}