package com.pts.handler;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.pts.invetech.R;
import com.pts.invetech.utils.AppLogger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Vaibhav on 14-09-2016.
 */
public class NetworkHandlerStringGet extends AsyncTask<String, Void, String> {

    private NetworkCallBack mActivity = null;
    private Dialog prgDialog = null;
    private int currentId = -1;
    private Context mcontext ;

    public NetworkHandlerStringGet(Context context, NetworkCallBack splsh, int id) {
        currentId = id;
        mActivity = splsh;
        mcontext = context;
        prgDialog = new ProgressDialog(context);
    }

    @Override
    protected void onPreExecute() {
        /*progress.setMessage("Please Wait");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();*/
        prgDialog = new Dialog(mcontext);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(mcontext, R.anim.zoom);
        image.startAnimation(animation);
        prgDialog.show();
        prgDialog.setCancelable(false);
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        String reader = networkCall(params[0], params[1]);
        return reader;
    }

    @Override
    protected void onPostExecute(String res) {
        mActivity.onResultString(res, currentId);
        if (prgDialog != null) {
            prgDialog.dismiss();
        }
    }

    private String networkCall(String currentUrl, String jsonParam) {
        URL url = null;
        HttpURLConnection connection = null;
        String response = "";
        try {
            url = new URL(currentUrl);

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setDoInput(true);


            AppLogger.showMsgWithoutTag(currentUrl + "     " + "data=" + jsonParam);

            int responseCode = connection.getResponseCode();

            AppLogger.showMsgWithoutTag("responce --> "+responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = null;
            }

        } catch (Exception e) {
            AppLogger.showMsg("Error in Network Handler String", e.getMessage());
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return response;
    }
}
