package com.pts.invetech.classes.activity;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.app.NotificationCompat.Builder;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.NotificationOne;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.DownloadAPIResponse;
import com.pts.invetech.classes.fragment.MainActivityFragment;
import com.pts.invetech.pojo.Download;
import com.pts.invetech.utils.JSONUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

public class DownloadActivity extends Activity {

    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;
    private String[] mTestArray;
    private ViewPager mbanner;
    private Point p;
    private ImageView mImageVoice;
    private ArrayAdapter<String> adapter;
    private Context context;
    private Dialog prgDialog;
    private ProgressDialog prgDialog1;
    private LinearLayout mopenLayout, openPopupDownload, llObligation, llScheduling, llBill, llRatesheet, llProfitability, llObligationllScheduling,
            llBillllRatesheet;
    private TextView tvtradingdate, tvtradingdate_set, tvdownloadnull, tvdownloadnulltext;
    private TextView tvobligation, tvscheduling, tvbill, tvratesheet, tvprofitability;
    private Calendar calendar;
    private int year, month, day;
    private TextView etxt_fromdate;
    private String appurl, date, iexobligation, iexschedule, ratesheet, tradingdate, bill, profitabilty;
    private String appurlagain, dateagain, iexobligationagain, iexscheduleagain, ratesheetagain, tradingdateagain, billagain, profitabiltyagain;
    private TextView btObligation, btScheduling, btBill, btRatesheet, btProfitability;
    private Button btgoDownload;
    private Animation animbounce;
    private String place;
    private String as;
    private NotificationManager mNotifyManager;
    private Builder mBuilder;
    private int id = 1;
    private NotificationManager myNotificationManager;
    private int notificationIdOne = 111;
    private int notificationIdTwo = 112;
    private int numMessagesOne = 0;
    private int numMessagesTwo = 0;
    // ImageView obligationimg, schedulingimg, billimg, ratesheetimg, profitabilityimg;
    private String ashishdate;
    private SQLiteDatabase db;
    private String dbacess,domain;
    private HomeActivity homeActivity;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_download);
        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);

        Cursor cu = db.rawQuery("SELECT * FROM baby", null);
        if (cu.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (cu.moveToNext()) {
            buffer.append("access_key: " + cu.getString(0) + "\n");
            dbacess = cu.getString(0);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
            //buffer.append("access_key: "+c.getString(0)+"\n");
            //Toast.makeText(getApplicationContext(), "access_key" + c.getColumnIndex("access_key"), Toast.LENGTH_LONG).show();
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
		 /*prgDialog = new ProgressDialog(DownloadActivity.this);
	     prgDialog.setMessage("Please wait...");
	     prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        prgDialog1 = new ProgressDialog(DownloadActivity.this);
        prgDialog1.setMessage("Downloading...");
        prgDialog1.setCancelable(false);

        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        tvtradingdate = (TextView) findViewById(R.id.tvtradingdate);
        tvtradingdate_set = (TextView) findViewById(R.id.tvtradingdate_set);

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -1);
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        // formattedDate have current date/time
        String[] parts = formattedDate.split(" ");
        String part1 = parts[0]; // 004

        String[] partsone = part1.split("-");
        String partone = partsone[0]; // 004
        String parttwo = partsone[1]; // 004
        String partthree = partsone[2]; // 004

        //    tvtradingdate.setText(partthree+"-"+parttwo+"-"+partone);
        //    tvtradingdate_set.setText(part1);
        //tvtradingdate = (TextView) rootView.findViewById(R.id.tvtradingdate);
        //	tradingdate = tvtradingdate_set.getText().toString();
        //	btObligation = (TextView) rootView.findViewById(R.id.btObligation);
        //	btScheduling = (TextView) rootView.findViewById(R.id.btScheduling);
        //	btBill = (TextView) rootView.findViewById(R.id.btBill);
        //	btRatesheet = (TextView) rootView.findViewById(R.id.btRatesheet);
        btProfitability = (TextView) findViewById(R.id.btProfitability);

        //	obligationimg = (ImageView) rootView.findViewById(R.id.obligationimg);
        //	schedulingimg = (ImageView) rootView.findViewById(R.id.schedulingimg);
        //	billimg = (ImageView) rootView.findViewById(R.id.billimg);
        //	ratesheetimg = (ImageView) rootView.findViewById(R.id.ratesheetimg);
        //	profitabilityimg = (ImageView) rootView.findViewById(R.id.profitabilityimg);

        tvdownloadnull = (TextView) findViewById(R.id.tvdownloadnull);
        tvdownloadnulltext = (TextView) findViewById(R.id.tvdownloadnulltext);

        llObligation = (LinearLayout) findViewById(R.id.llObligation);
        llScheduling = (LinearLayout) findViewById(R.id.llScheduling);
        llBill = (LinearLayout) findViewById(R.id.llBill);
        llRatesheet = (LinearLayout) findViewById(R.id.llRatesheet);
        llProfitability = (LinearLayout) findViewById(R.id.llProfitability);
        llObligationllScheduling = (LinearLayout) findViewById(R.id.llObligationllScheduling);
        llBillllRatesheet = (LinearLayout) findViewById(R.id.llBillllRatesheet);


        animbounce = AnimationUtils.loadAnimation(DownloadActivity.this, R.anim.bounce);
        //btObligation.startAnimation(animbounce);
        //btScheduling.startAnimation(animbounce);
        //btBill.startAnimation(animbounce);
        //btRatesheet.startAnimation(animbounce);
        //btProfitability.startAnimation(animbounce);

        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DownloadActivity.this, MainActivityFragment.class);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        tvtradingdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //openPopupDownload = (LinearLayout) rootView.findViewById(R.id.openPopupDownload);
        //logoutpopup();
        new HttpAsyncTask().execute(domain + "/mobile/pxs_app/html/downloads.php");
    }

    /**
     * Showing Dialog
     */
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                prgDialog1 = new ProgressDialog(DownloadActivity.this);
                prgDialog1.setMessage("Downloading file. Please wait...");
                prgDialog1.setIndeterminate(false);
                prgDialog1.setMax(100);
                prgDialog1.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                prgDialog1.setCancelable(true);
                prgDialog1.show();
                return prgDialog1;
            default:
                return null;
        }
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(DownloadActivity.this, MainActivityFragment.class);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    public class SelectDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 10) && (day <= 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                tvtradingdate.setText(dayplace + "-" + monthplace + "-" + year);
                tvtradingdate_set.setText(year + "-" + monthplace + "-" + dayplace);
                as = tvtradingdate_set.getText().toString();
                //  Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
            } else if (month <= 10) {
                String monthplace = "0" + month;
                tvtradingdate.setText(day + "-" + monthplace + "-" + year);
                tvtradingdate_set.setText(year + "-" + monthplace + "-" + day);
                as = tvtradingdate_set.getText().toString();
            } else if (day < 10) {
                String dayplace = "0" + day;
                tvtradingdate.setText(dayplace + "-" + month + "-" + year);
                tvtradingdate_set.setText(year + "-" + month + "-" + dayplace);
                as = tvtradingdate_set.getText().toString();
            } else {
                tvtradingdate.setText(day + "-" + month + "-" + year);
                tvtradingdate_set.setText(year + "-" + month + "-" + day);
                as = tvtradingdate_set.getText().toString();
                //Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
                //(month + "/" + day + "/" + year);
                //(year + "-" + month + "-" + day);
            }
            btgoDownload = (Button) findViewById(R.id.btgoDownload);
            btgoDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    //as = tvtradingdate.getText().toString();
                    //Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
                    new HttpAsyncTaskGo()
                            .execute(domain + "/mobile/pxs_app/html/downloads.php");
                }
            });
        }

    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm username, password
     */
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Download download = new Download();
            download.setDate("LASTDATE");
            download.setAccess_key(dbacess);
            //login.setAccess_key(DeviceRegister.access_key);
            return DownloadAPIResponse.POST(urls[0], download);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            //Toast.makeText(getActivity(), "DownloadResult" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    if (mainObject.has("url")) {
                        appurl = mainObject.getString("url");
                    }
                    if (mainObject.has("date")) {
                        date = mainObject.getString("date");
                        String[] parts = date.split("-");
                        String dd = parts[0];
                        String mm = parts[1];
                        String yy = parts[2];
                        tvtradingdate.setText(yy + "-" + mm + "-" + dd);
                        tvtradingdate_set.setText(date);

                    }
                    if (mainObject.has("bill")) {
                        bill = mainObject.getString("bill");
                    }
                    if (mainObject.has("iexobligation")) {
                        iexobligation = mainObject.getString("iexobligation");
                    }
                    if (mainObject.has("iexschedule")) {
                        iexschedule = mainObject.getString("iexschedule");
                    }

                    if (mainObject.has("ratesheet")) {
                        ratesheet = mainObject.getString("ratesheet");
                    }
                    if (mainObject.has("iexprofitability")) {
                        profitabilty = mainObject.getString("iexprofitability");
                    }

                    if ((iexobligation.equalsIgnoreCase("NA")) && (iexschedule.equalsIgnoreCase("NA")) && (bill.equalsIgnoreCase("NA")) && (ratesheet == null) && (profitabilty.equalsIgnoreCase("NA"))) {
                        //	tvdownloadnull.setText("NO File Found");
                        tvdownloadnull.setBackgroundResource(R.drawable.sad);
                        tvdownloadnulltext.setText("NO file found for download, Plesae select another date.");
                        tvdownloadnull.setVisibility(View.VISIBLE);
                        tvdownloadnulltext.setVisibility(View.VISIBLE);
                    }
                    if (bill.equalsIgnoreCase("NA")) {
                        //billimg.setBackgroundResource(R.drawable.errormppl);
                    } else {
                        llBillllRatesheet.setVisibility(View.VISIBLE);
                        llBill.setVisibility(View.VISIBLE);
                        llBill.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                //fileidentifire = bill;
						    /*String[] parts = fileidentifire.split(".");
							String part1 = parts[0];
							String part2 = parts[1]; */
                                new DownloadFileAsync().execute("http://" + appurl + bill);
                                Notification.Builder mBuilder = new Notification.Builder(DownloadActivity.this);

                                mBuilder.setContentTitle("MPPL Trading");
                                if (bill.endsWith(".pdf")) {
                                    mBuilder.setContentText("Bill file download.(File is in PDF format.)");
                                } else if (bill.endsWith(".xlsx")) {
                                    mBuilder.setContentText("Bill file download.(File is in xlsx format.)");
                                } else if (bill.endsWith(".xls")) {
                                    mBuilder.setContentText("Bill file download.(File is in xls format.)");
                                }
                                mBuilder.setTicker("Downloading");
                                mBuilder.setSmallIcon(R.drawable.logopts);

                                // Increase notification number every time a new notification arrives
                                mBuilder.setNumber(++numMessagesOne);
                                mBuilder.setAutoCancel(true);
                                // Creates an explicit intent for an Activity in your app
                                Intent myIntent = new Intent();
                                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                                if (bill.endsWith(".pdf")) {
                                    File file = new File("/sdcard/24627_AIAEngineeringLtd_14_Sep_15.pdf");
                                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                } else if (bill.endsWith(".xlsx")) {
                                    File file = new File("/sdcard/IEX151016SCH_MPL0021_GJ0_AIA_Engineering_Ltd_d.xlsx");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                } else if (bill.endsWith(".xls")) {
                                    File file = new File("/sdcard/IEX151015DOR_MPL0021_GJ0_AIA_Engineering_Ltd.xls");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                }
                                //   Intent resultIntent = new Intent(getActivity(), NotificationOne.class);
                                //   resultIntent.putExtra("notificationId", notificationIdOne);

                                //This ensures that navigating backward from the Activity leads out of the app to Home page
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadActivity.this);
                                // Adds the back stack for the Intent
                                stackBuilder.addParentStack(NotificationOne.class);

                                // Adds the Intent that starts the Activity to the top of the stack
                                stackBuilder.addNextIntent(myIntent);
                                PendingIntent resultPendingIntent =
                                        stackBuilder.getPendingIntent(
                                                0,
                                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                                        );
                                // start the activity when the user clicks the notification text
                                mBuilder.setContentIntent(resultPendingIntent);
                                Notification noti = mBuilder.build();
                                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                noti.defaults |= Notification.DEFAULT_VIBRATE;
                                noti.defaults |= Notification.DEFAULT_SOUND;
                                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                                noti.ledARGB = 0xff00ff00;
                                noti.ledOnMS = 300;
                                noti.ledOffMS = 1000;
                                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                                // pass the Notification object to the system
                                myNotificationManager.notify(notificationIdOne, mBuilder.build());
                            }

                        });
                    }
                    if (iexobligation.equalsIgnoreCase("NA")) {
                        //obligationimg.setBackgroundResource(R.drawable.errormppl);
                    } else {
                        llObligationllScheduling.setVisibility(View.VISIBLE);
                        llObligation.setVisibility(View.VISIBLE);
                        llObligation.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                new DownloadFileAsync().execute("http://" + appurl + iexobligation);
                                Notification.Builder mBuilder = new Notification.Builder(DownloadActivity.this);

                                mBuilder.setContentTitle("MPPL Trading");
                                if (iexobligation.endsWith(".pdf")) {
                                    mBuilder.setContentText("Obligation file download.(File is in PDF format.)");
                                } else if (iexobligation.endsWith(".xlsx")) {
                                    mBuilder.setContentText("Obligation file download.(File is in xlsx format.)");
                                } else if (iexobligation.endsWith(".xls")) {
                                    mBuilder.setContentText("Obligation file download.(File is in xls format.)");
                                }
                                mBuilder.setTicker("Downloading");
                                mBuilder.setSmallIcon(R.drawable.logopts);

                                // Increase notification number every time a new notification arrives
                                mBuilder.setNumber(++numMessagesOne);
                                mBuilder.setAutoCancel(true);
                                Intent myIntent = new Intent();
                                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                                if (iexobligation.endsWith(".pdf")) {
                                    File file = new File("/sdcard/24627_AIAEngineeringLtd_14_Sep_15.pdf");
                                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                } else if (iexobligation.endsWith(".xlsx")) {
                                    File file = new File("/sdcard/IEX151016SCH_MPL0021_GJ0_AIA_Engineering_Ltd_d.xlsx");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                } else if (iexobligation.endsWith(".xls")) {
                                    File file = new File("/sdcard/IEX151015DOR_MPL0021_GJ0_AIA_Engineering_Ltd.xls");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                }


                                // Creates an explicit intent for an Activity in your app
                                // Intent resultIntent = new Intent(getActivity(), NotificationOne.class);
                                // resultIntent.putExtra("notificationIdo", notificationIdOne);

                                //This ensures that navigating backward from the Activity leads out of the app to Home page
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadActivity.this);
                                // Adds the back stack for the Intent
                                stackBuilder.addParentStack(NotificationOne.class);

                                // Adds the Intent that starts the Activity to the top of the stack
                                stackBuilder.addNextIntent(myIntent);
                                PendingIntent resultPendingIntent =
                                        stackBuilder.getPendingIntent(
                                                0,
                                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                                        );
                                // start the activity when the user clicks the notification text
                                mBuilder.setContentIntent(resultPendingIntent);
                                Notification noti = mBuilder.build();
                                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                noti.defaults |= Notification.DEFAULT_VIBRATE;
                                noti.defaults |= Notification.DEFAULT_SOUND;
                                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                                noti.ledARGB = 0xff00ff00;
                                noti.ledOnMS = 300;
                                noti.ledOffMS = 1000;
                                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                                // pass the Notification object to the system
                                myNotificationManager.notify(notificationIdOne, mBuilder.build());
                            }

                        });
                    }
                    if (iexschedule.equalsIgnoreCase("NA")) {
                        //schedulingimg.setBackgroundResource(R.drawable.errormppl);
                    } else {
                        llScheduling.setVisibility(View.VISIBLE);
                        llObligationllScheduling.setVisibility(View.VISIBLE);
                        llScheduling.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                new DownloadFileAsync().execute("http://" + appurl + iexschedule);
                                Notification.Builder mBuilder = new Notification.Builder(DownloadActivity.this);

                                mBuilder.setContentTitle("MPPL Trading");
                                if (iexschedule.endsWith(".pdf")) {
                                    mBuilder.setContentText("Scheduling file download.(File is in PDF format.)");
                                } else if (iexschedule.endsWith(".xls")) {
                                    mBuilder.setContentText("Scheduling file download.(File is in xls format.)");
                                } else if (iexschedule.endsWith(".xlsx")) {
                                    mBuilder.setContentText("Scheduling file download.(File is in xlsx format.)");
                                }
                                mBuilder.setTicker("Downloading");
                                mBuilder.setSmallIcon(R.drawable.logopts);

                                // Increase notification number every time a new notification arrives
                                mBuilder.setNumber(++numMessagesOne);
                                mBuilder.setAutoCancel(true);
                                Intent myIntent = new Intent();
                                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                                if (iexschedule.endsWith(".pdf")) {
                                    File file = new File("/sdcard/24627_AIAEngineeringLtd_14_Sep_15.pdf");
                                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                } else if (iexschedule.endsWith(".xlsx")) {
                                    File file = new File("/sdcard/IEX151016SCH_MPL0021_GJ0_AIA_Engineering_Ltd_d.xlsx");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                } else if (iexschedule.endsWith(".xls")) {
                                    File file = new File("/sdcard/IEX151015DOR_MPL0021_GJ0_AIA_Engineering_Ltd.xls");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                }


                                // Creates an explicit intent for an Activity in your app
                                //Intent resultIntent = new Intent(getActivity(), NotificationOne.class);
                                //resultIntent.putExtra("notificationId", notificationIdOne);

                                //This ensures that navigating backward from the Activity leads out of the app to Home page
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadActivity.this);
                                // Adds the back stack for the Intent
                                stackBuilder.addParentStack(NotificationOne.class);

                                // Adds the Intent that starts the Activity to the top of the stack
                                stackBuilder.addNextIntent(myIntent);
                                PendingIntent resultPendingIntent =
                                        stackBuilder.getPendingIntent(
                                                0,
                                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                                        );
                                // start the activity when the user clicks the notification text
                                mBuilder.setContentIntent(resultPendingIntent);
                                Notification noti = mBuilder.build();
                                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                noti.defaults |= Notification.DEFAULT_VIBRATE;
                                noti.defaults |= Notification.DEFAULT_SOUND;
                                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                                noti.ledARGB = 0xff00ff00;
                                noti.ledOnMS = 300;
                                noti.ledOffMS = 1000;
                                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                                // pass the Notification object to the system
                                myNotificationManager.notify(notificationIdOne, mBuilder.build());
                            }

                        });
                    }
                    if (ratesheet.equalsIgnoreCase("NA")) {
                        //ratesheetimg.setBackgroundResource(R.drawable.errormppl);
                    } else {
                        llRatesheet.setVisibility(View.VISIBLE);
                        llBillllRatesheet.setVisibility(View.VISIBLE);
                        llRatesheet.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                new DownloadFileAsync().execute("http://" + appurl + ratesheet);
                                Notification.Builder mBuilder = new Notification.Builder(DownloadActivity.this);

                                mBuilder.setContentTitle("MPPL Trading");
                                if (ratesheet.endsWith(".pdf")) {
                                    mBuilder.setContentText("Ratesheet file download.(File is in PDF format.)");
                                } else if (ratesheet.endsWith(".xlsx")) {
                                    mBuilder.setContentText("Ratesheet file download.(File is in xlsx format.)");
                                } else if (ratesheet.endsWith(".xls")) {
                                    mBuilder.setContentText("Ratesheet file download.(File is in xls format.)");
                                }

                                mBuilder.setTicker("Downloading");
                                mBuilder.setSmallIcon(R.drawable.logopts);

                                // Increase notification number every time a new notification arrives
                                mBuilder.setNumber(++numMessagesOne);
                                mBuilder.setAutoCancel(true);
                                Intent myIntent = new Intent();
                                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                                if (ratesheet.endsWith(".pdf")) {
                                    File file = new File("/sdcard/24627_AIAEngineeringLtd_14_Sep_15.pdf");
                                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                } else if (ratesheet.endsWith(".xlsx")) {
                                    File file = new File("/sdcard/IEX151016SCH_MPL0021_GJ0_AIA_Engineering_Ltd_d.xlsx");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                } else if (ratesheet.endsWith(".xls")) {
                                    File file = new File("/sdcard/IEX151015DOR_MPL0021_GJ0_AIA_Engineering_Ltd.xls");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                }

                                // Creates an explicit intent for an Activity in your app
                                // Intent resultIntent = new Intent(getActivity(), NotificationOne.class);
                                // resultIntent.putExtra("notificationId", notificationIdOne);

                                //This ensures that navigating backward from the Activity leads out of the app to Home page
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadActivity.this);
                                // Adds the back stack for the Intent
                                stackBuilder.addParentStack(NotificationOne.class);

                                // Adds the Intent that starts the Activity to the top of the stack
                                stackBuilder.addNextIntent(myIntent);
                                PendingIntent resultPendingIntent =
                                        stackBuilder.getPendingIntent(
                                                0,
                                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                                        );
                                // start the activity when the user clicks the notification text
                                mBuilder.setContentIntent(resultPendingIntent);
                                Notification noti = mBuilder.build();
                                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                noti.defaults |= Notification.DEFAULT_VIBRATE;
                                noti.defaults |= Notification.DEFAULT_SOUND;
                                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                                noti.ledARGB = 0xff00ff00;
                                noti.ledOnMS = 300;
                                noti.ledOffMS = 1000;
                                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                                // pass the Notification object to the system
                                myNotificationManager.notify(notificationIdOne, mBuilder.build());
                            }
                        });
                    }
                    if (profitabilty.equalsIgnoreCase("NA")) {
                        //profitabilityimg.setBackgroundResource(R.drawable.errormppl);
                    } else {
                        llProfitability.setVisibility(View.VISIBLE);
                        llProfitability.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                new DownloadFileAsync().execute("http://" + appurl + iexobligation);
                                Notification.Builder mBuilder = new Notification.Builder(DownloadActivity.this);

                                mBuilder.setContentTitle("MPPL Trading");
                                if (iexobligation.endsWith(".pdf")) {
                                    mBuilder.setContentText("Profitability file download.(File is in PDF format.)");
                                } else if (iexobligation.endsWith(".xlsx")) {
                                    mBuilder.setContentText("Profitability file download.(File is in xlsx format.)");
                                } else if (iexobligation.endsWith(".xls")) {
                                    mBuilder.setContentText("Profitability file download.(File is in xls format.)");
                                }

                                mBuilder.setTicker("Downloading");
                                mBuilder.setSmallIcon(R.drawable.logopts);

                                // Increase notification number every time a new notification arrives
                                mBuilder.setNumber(++numMessagesOne);
                                mBuilder.setAutoCancel(true);
                                Intent myIntent = new Intent();
                                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                                if (iexobligation.endsWith(".pdf")) {
                                    File file = new File("/sdcard/24627_AIAEngineeringLtd_14_Sep_15.pdf");
                                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                } else if (iexobligation.endsWith(".xlsx")) {
                                    File file = new File("/sdcard/IEX151016SCH_MPL0021_GJ0_AIA_Engineering_Ltd_d.xlsx");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                } else if (iexobligation.endsWith(".xls")) {
                                    File file = new File("/sdcard/IEX151015DOR_MPL0021_GJ0_AIA_Engineering_Ltd.xls");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                }

                                // Creates an explicit intent for an Activity in your app
                                //Intent resultIntent = new Intent(getActivity(), NotificationOne.class);
                                //resultIntent.putExtra("notificationId", notificationIdOne);

                                //This ensures that navigating backward from the Activity leads out of the app to Home page
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadActivity.this);
                                // Adds the back stack for the Intent
                                stackBuilder.addParentStack(NotificationOne.class);

                                // Adds the Intent that starts the Activity to the top of the stack
                                stackBuilder.addNextIntent(myIntent);
                                PendingIntent resultPendingIntent =
                                        stackBuilder.getPendingIntent(
                                                0,
                                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                                        );
                                // start the activity when the user clicks the notification text
                                mBuilder.setContentIntent(resultPendingIntent);
                                Notification noti = mBuilder.build();
                                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                noti.defaults |= Notification.DEFAULT_VIBRATE;
                                noti.defaults |= Notification.DEFAULT_SOUND;
                                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                                noti.ledARGB = 0xff00ff00;
                                noti.ledOnMS = 300;
                                noti.ledOffMS = 1000;
                                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                                // pass the Notification object to the system
                                myNotificationManager.notify(notificationIdOne, mBuilder.build());
                            }
                        });
                    }

                } catch (JSONException e) {
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    e.printStackTrace();

                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }


        }


    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm username, password
     */
    private class HttpAsyncTaskGo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Download download = new Download();
            download.setDate(as);
            download.setAccess_key(dbacess);
            //login.setAccess_key(DeviceRegister.access_key);
            return DownloadAPIResponse.POST(urls[0], download);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            //Toast.makeText(getActivity(), "DownloadResult" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    iexobligationagain = null;
                    iexscheduleagain = null;
                    billagain = null;
                    ratesheetagain = null;
                    profitabiltyagain = null;
                    if (mainObject.has("url")) {
                        appurlagain = mainObject.getString("url");
                    }
                    if (mainObject.has("date")) {
                        dateagain = mainObject.getString("date");
                    }
                    if (mainObject.has("bill")) {
                        billagain = mainObject.getString("bill");
                    }
                    if (mainObject.has("iexobligation")) {
                        iexobligationagain = mainObject.getString("iexobligation");
                    }
                    if (mainObject.has("iexschedule")) {
                        iexscheduleagain = mainObject.getString("iexschedule");
                    }
                    if (mainObject.has("ratesheet")) {
                        ratesheetagain = mainObject.getString("ratesheet");
                    }
                    if (mainObject.has("iexprofitability")) {
                        profitabiltyagain = mainObject.getString("iexprofitability");
                    }
                    if ((iexobligationagain.equalsIgnoreCase("NA")) && (iexscheduleagain.equalsIgnoreCase("NA")) && (billagain.equalsIgnoreCase("NA")) && (ratesheetagain.equalsIgnoreCase("NA")) && (profitabiltyagain.equalsIgnoreCase("NA"))) {
                        tvdownloadnull.setBackgroundResource(R.drawable.sad);
                        tvdownloadnulltext.setText("NO file found for download, Plesae select another date.");
                        tvdownloadnull.setVisibility(View.VISIBLE);
                        tvdownloadnulltext.setVisibility(View.VISIBLE);
                    }
                    if (billagain.equalsIgnoreCase("NA")) {
                        llBill.setVisibility(View.GONE);
                        llBillllRatesheet.setVisibility(View.GONE);
                        //tvdownloadnull.setVisibility(View.GONE);
                    } else {
                        llBill.setVisibility(View.VISIBLE);
                        llBillllRatesheet.setVisibility(View.VISIBLE);
                        tvdownloadnull.setVisibility(View.GONE);
                        llBill.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                new DownloadFileAsync().execute("http://" + appurlagain + billagain);

                                Notification.Builder mBuilder = new Notification.Builder(DownloadActivity.this);

                                mBuilder.setContentTitle("MPPL Trading");
                                if (billagain.contains(".pdf")) {
                                    mBuilder.setContentText("Bill file download.(File is in PDF format.)");
                                } else if (billagain.contains(".xlsx")) {
                                    mBuilder.setContentText("Bill file download.(File is in xlsx format.)");
                                } else if (billagain.contains(".xls")) {
                                    mBuilder.setContentText("Bill file download.(File is in xls format.)");
                                }

                                mBuilder.setTicker("Downloading");
                                mBuilder.setSmallIcon(R.drawable.logopts);

                                // Increase notification number every time a new notification arrives
                                mBuilder.setNumber(++numMessagesOne);
                                mBuilder.setAutoCancel(true);
                                Intent myIntent = new Intent();
                                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                                if (billagain.contains(".pdf")) {
                                    File file = new File("/sdcard/24627_AIAEngineeringLtd_14_Sep_15.pdf");
                                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                } else if (billagain.contains(".xlsx")) {
                                    File file = new File("/sdcard/IEX151016SCH_MPL0021_GJ0_AIA_Engineering_Ltd_d.xlsx");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                } else if (billagain.contains(".xls")) {
                                    File file = new File("/sdcard/IEX151015DOR_MPL0021_GJ0_AIA_Engineering_Ltd.xls");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                }

                                // Creates an explicit intent for an Activity in your app
                                // Intent resultIntent = new Intent(getActivity(), NotificationOne.class);
                                // resultIntent.putExtra("notificationId", notificationIdOne);

                                //This ensures that navigating backward from the Activity leads out of the app to Home page
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadActivity.this);
                                // Adds the back stack for the Intent
                                stackBuilder.addParentStack(NotificationOne.class);

                                // Adds the Intent that starts the Activity to the top of the stack
                                stackBuilder.addNextIntent(myIntent);
                                PendingIntent resultPendingIntent =
                                        stackBuilder.getPendingIntent(
                                                0,
                                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                                        );
                                // start the activity when the user clicks the notification text
                                mBuilder.setContentIntent(resultPendingIntent);
                                Notification noti = mBuilder.build();
                                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                noti.defaults |= Notification.DEFAULT_VIBRATE;
                                noti.defaults |= Notification.DEFAULT_SOUND;
                                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                                noti.ledARGB = 0xff00ff00;
                                noti.ledOnMS = 300;
                                noti.ledOffMS = 1000;
                                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                                // pass the Notification object to the system
                                myNotificationManager.notify(notificationIdOne, mBuilder.build());
                            }

                        });
                    }
                    if (iexobligationagain.equalsIgnoreCase("NA")) {
                        llObligation.setVisibility(View.GONE);
                        llObligationllScheduling.setVisibility(View.GONE);
                        //tvdownloadnull.setVisibility(View.GONE);
                    } else {
                        llObligation.setVisibility(View.VISIBLE);
                        llObligationllScheduling.setVisibility(View.VISIBLE);
                        tvdownloadnull.setVisibility(View.GONE);
                        llObligation.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                new DownloadFileAsync().execute("http://" + appurlagain + iexobligationagain);
                                Notification.Builder mBuilder = new Notification.Builder(DownloadActivity.this);

                                mBuilder.setContentTitle("MPPL Trading");
                                if (iexobligationagain.contains(".pdf")) {
                                    mBuilder.setContentText("Obligation file download.(File is in PDF format.)");
                                } else if (iexobligationagain.contains(".xlsx")) {
                                    mBuilder.setContentText("Obligation file download.(File is in xlsx format.)");
                                } else if (iexobligationagain.contains(".xls")) {
                                    mBuilder.setContentText("Obligation file download.(File is in xls format.)");
                                }

                                mBuilder.setTicker("Downloading");
                                mBuilder.setSmallIcon(R.drawable.logopts);

                                // Increase notification number every time a new notification arrives
                                mBuilder.setNumber(++numMessagesOne);
                                mBuilder.setAutoCancel(true);
                                Intent myIntent = new Intent();
                                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                                if (iexobligationagain.contains(".pdf")) {
                                    File file = new File("/sdcard/24627_AIAEngineeringLtd_14_Sep_15.pdf");
                                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                } else if (iexobligationagain.contains(".xlsx")) {
                                    File file = new File("/sdcard/IEX151016SCH_MPL0021_GJ0_AIA_Engineering_Ltd_d.xlsx");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                } else if (iexobligationagain.contains(".xls")) {
                                    File file = new File("/sdcard/IEX151015DOR_MPL0021_GJ0_AIA_Engineering_Ltd.xls");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                }
                                // Creates an explicit intent for an Activity in your app
                                // Intent resultIntent = new Intent(getActivity(), NotificationOne.class);
                                //  resultIntent.putExtra("notificationId", notificationIdOne);

                                //This ensures that navigating backward from the Activity leads out of the app to Home page
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadActivity.this);
                                // Adds the back stack for the Intent
                                stackBuilder.addParentStack(NotificationOne.class);

                                // Adds the Intent that starts the Activity to the top of the stack
                                stackBuilder.addNextIntent(myIntent);
                                PendingIntent resultPendingIntent =
                                        stackBuilder.getPendingIntent(
                                                0,
                                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                                        );
                                // start the activity when the user clicks the notification text
                                mBuilder.setContentIntent(resultPendingIntent);
                                Notification noti = mBuilder.build();
                                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                noti.defaults |= Notification.DEFAULT_VIBRATE;
                                noti.defaults |= Notification.DEFAULT_SOUND;
                                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                                noti.ledARGB = 0xff00ff00;
                                noti.ledOnMS = 300;
                                noti.ledOffMS = 1000;
                                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                                // pass the Notification object to the system
                                myNotificationManager.notify(notificationIdOne, mBuilder.build());
                            }

                        });
                    }
                    if (iexscheduleagain.equalsIgnoreCase("NA")) {
                        llScheduling.setVisibility(View.GONE);
                        llObligationllScheduling.setVisibility(View.GONE);
                        //tvdownloadnull.setVisibility(View.GONE);
                    } else {
                        llScheduling.setVisibility(View.VISIBLE);
                        llObligationllScheduling.setVisibility(View.VISIBLE);
                        tvdownloadnull.setVisibility(View.GONE);
                        llScheduling.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                new DownloadFileAsync().execute("http://" + appurlagain + iexscheduleagain);
                                Notification.Builder mBuilder = new Notification.Builder(DownloadActivity.this);

                                mBuilder.setContentTitle("MPPL Trading");
                                if (iexscheduleagain.contains(".pdf")) {
                                    mBuilder.setContentText("Scheduling file download.(File is in PDF format.)");
                                } else if (iexscheduleagain.contains(".xlsx")) {
                                    mBuilder.setContentText("Scheduling file download.(File is in xlsx format.)");
                                } else if (iexscheduleagain.contains(".xls")) {
                                    mBuilder.setContentText("Scheduling file download.(File is in xls format.)");
                                }

                                mBuilder.setTicker("Downloading");
                                mBuilder.setSmallIcon(R.drawable.logopts);

                                // Increase notification number every time a new notification arrives
                                mBuilder.setNumber(++numMessagesOne);
                                mBuilder.setAutoCancel(true);
                                Intent myIntent = new Intent();
                                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                                if (iexscheduleagain.contains(".pdf")) {
                                    File file = new File("/sdcard/24627_AIAEngineeringLtd_14_Sep_15.pdf");
                                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                } else if (iexscheduleagain.contains(".xlsx")) {
                                    File file = new File("/sdcard/IEX151016SCH_MPL0021_GJ0_AIA_Engineering_Ltd_d.xlsx");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                } else if (iexscheduleagain.contains(".xls")) {
                                    File file = new File("/sdcard/IEX151015DOR_MPL0021_GJ0_AIA_Engineering_Ltd.xls");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                }

                                // Creates an explicit intent for an Activity in your app
                                // Intent resultIntent = new Intent(getActivity(), NotificationOne.class);
                                // resultIntent.putExtra("notificationId", notificationIdOne);

                                //This ensures that navigating backward from the Activity leads out of the app to Home page
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadActivity.this);
                                // Adds the back stack for the Intent
                                stackBuilder.addParentStack(NotificationOne.class);

                                // Adds the Intent that starts the Activity to the top of the stack
                                stackBuilder.addNextIntent(myIntent);
                                PendingIntent resultPendingIntent =
                                        stackBuilder.getPendingIntent(
                                                0,
                                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                                        );
                                // start the activity when the user clicks the notification text
                                mBuilder.setContentIntent(resultPendingIntent);
                                Notification noti = mBuilder.build();
                                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                noti.defaults |= Notification.DEFAULT_VIBRATE;
                                noti.defaults |= Notification.DEFAULT_SOUND;
                                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                                noti.ledARGB = 0xff00ff00;
                                noti.ledOnMS = 300;
                                noti.ledOffMS = 1000;
                                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                                // pass the Notification object to the system
                                myNotificationManager.notify(notificationIdOne, mBuilder.build());
                            }

                        });
                    }
                    if (ratesheetagain.equalsIgnoreCase("NA")) {
                        llRatesheet.setVisibility(View.GONE);
                        llBillllRatesheet.setVisibility(View.GONE);
                        //tvdownloadnull.setVisibility(View.GONE);
                    } else {
                        llRatesheet.setVisibility(View.VISIBLE);
                        llBillllRatesheet.setVisibility(View.VISIBLE);
                        tvdownloadnull.setVisibility(View.GONE);
                        llRatesheet.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                new DownloadFileAsync().execute("http://" + appurlagain + ratesheetagain);
                                Notification.Builder mBuilder = new Notification.Builder(DownloadActivity.this);

                                mBuilder.setContentTitle("MPPL Trading");
                                if (ratesheetagain.contains(".pdf")) {
                                    mBuilder.setContentText("Ratesheet file download.(File is in PDF format.)");
                                } else if (ratesheetagain.contains(".xlsx")) {
                                    mBuilder.setContentText("Ratesheet file download.(File is in xlsx format.)");
                                } else if (ratesheetagain.contains(".xls")) {
                                    mBuilder.setContentText("Ratesheet file download.(File is in xls format.)");
                                }

                                mBuilder.setTicker("Downloading");
                                mBuilder.setSmallIcon(R.drawable.logopts);

                                // Increase notification number every time a new notification arrives
                                mBuilder.setNumber(++numMessagesOne);
                                mBuilder.setAutoCancel(true);
                                Intent myIntent = new Intent();
                                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                                if (ratesheetagain.contains(".pdf")) {
                                    File file = new File("/sdcard/24627_AIAEngineeringLtd_14_Sep_15.pdf");
                                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                } else if (ratesheetagain.contains(".xlsx")) {
                                    File file = new File("/sdcard/IEX151016SCH_MPL0021_GJ0_AIA_Engineering_Ltd_d.xlsx");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                } else if (ratesheetagain.contains(".xls")) {
                                    File file = new File("/sdcard/IEX151015DOR_MPL0021_GJ0_AIA_Engineering_Ltd.xls");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                }
                                // Creates an explicit intent for an Activity in your app
                                //Intent resultIntent = new Intent(getActivity(), NotificationOne.class);
                                // resultIntent.putExtra("notificationId", notificationIdOne);

                                //This ensures that navigating backward from the Activity leads out of the app to Home page
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadActivity.this);
                                // Adds the back stack for the Intent
                                stackBuilder.addParentStack(NotificationOne.class);

                                // Adds the Intent that starts the Activity to the top of the stack
                                stackBuilder.addNextIntent(myIntent);
                                PendingIntent resultPendingIntent =
                                        stackBuilder.getPendingIntent(
                                                0,
                                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                                        );
                                // start the activity when the user clicks the notification text
                                mBuilder.setContentIntent(resultPendingIntent);
                                Notification noti = mBuilder.build();
                                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                noti.defaults |= Notification.DEFAULT_VIBRATE;
                                noti.defaults |= Notification.DEFAULT_SOUND;
                                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                                noti.ledARGB = 0xff00ff00;
                                noti.ledOnMS = 300;
                                noti.ledOffMS = 1000;
                                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                                // pass the Notification object to the system
                                myNotificationManager.notify(notificationIdOne, mBuilder.build());
                            }
                        });
                    }
                    if (profitabiltyagain.equalsIgnoreCase("NA")) {
                        llProfitability.setVisibility(View.GONE);
                        //tvdownloadnull.setVisibility(View.GONE);
                    } else {
                        llProfitability.setVisibility(View.VISIBLE);
                        tvdownloadnull.setVisibility(View.GONE);
                        llProfitability.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                new DownloadFileAsync().execute("http://" + appurlagain + iexobligationagain);
                                Notification.Builder mBuilder = new Notification.Builder(DownloadActivity.this);

                                mBuilder.setContentTitle("MPPL Trading");
                                if (iexobligationagain.contains(".pdf")) {
                                    mBuilder.setContentText("Profitability file download.(File is in PDF format.)");
                                } else if (iexobligationagain.contains(".xlsx")) {
                                    mBuilder.setContentText("Profitability file download.(File is in xlsx format.)");
                                } else if (iexobligationagain.contains(".xls")) {
                                    mBuilder.setContentText("Profitability file download.(File is in xls format.)");
                                }

                                mBuilder.setTicker("Downloading");
                                mBuilder.setSmallIcon(R.drawable.logopts);

                                // Increase notification number every time a new notification arrives
                                mBuilder.setNumber(++numMessagesOne);
                                mBuilder.setAutoCancel(true);

                                Intent myIntent = new Intent();
                                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                                if (iexobligationagain.contains(".pdf")) {
                                    File file = new File("/sdcard/24627_AIAEngineeringLtd_14_Sep_15.pdf");
                                    myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                } else if (iexobligationagain.contains(".xlsx")) {
                                    File file = new File("/sdcard/IEX151016SCH_MPL0021_GJ0_AIA_Engineering_Ltd_d.xlsx");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                } else if (iexobligationagain.contains(".xls")) {
                                    File file = new File("/sdcard/IEX151015DOR_MPL0021_GJ0_AIA_Engineering_Ltd.xls");
                                    myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                                }

                                // Creates an explicit intent for an Activity in your app
                                // Intent resultIntent = new Intent(getActivity(), NotificationOne.class);
                                //resultIntent.putExtra("notificationId", notificationIdOne);

                                //This ensures that navigating backward from the Activity leads out of the app to Home page
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadActivity.this);
                                // Adds the back stack for the Intent
                                stackBuilder.addParentStack(NotificationOne.class);

                                // Adds the Intent that starts the Activity to the top of the stack
                                stackBuilder.addNextIntent(myIntent);
                                PendingIntent resultPendingIntent =
                                        stackBuilder.getPendingIntent(
                                                0,
                                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                                        );
                                // start the activity when the user clicks the notification text
                                mBuilder.setContentIntent(resultPendingIntent);
                                Notification noti = mBuilder.build();
                                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                noti.defaults |= Notification.DEFAULT_VIBRATE;
                                noti.defaults |= Notification.DEFAULT_SOUND;
                                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                                noti.ledARGB = 0xff00ff00;
                                noti.ledOnMS = 300;
                                noti.ledOffMS = 1000;
                                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                                // pass the Notification object to the system
                                myNotificationManager.notify(notificationIdOne, mBuilder.build());
                            }
                        });
                    }
                } catch (JSONException e) {
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }

    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileAsync extends AsyncTask<String, String, String> {
        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            prgDialog1.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            Random r = new Random();
            int i1 = r.nextInt(80 - 65) + 65;
            String string = f_url[0];
            string.endsWith(".xls");
            try {
                if (string.endsWith(".pdf")) {
                    try {
                        URL url = new URL(f_url[0]);
                        URLConnection conection = url.openConnection();
                        conection.connect();
                        // getting file length
                        int lenghtOfFile = conection.getContentLength();

                        // input stream to read file - with 8k buffer
                        InputStream input = new BufferedInputStream(
                                url.openStream(), 8192);

                        // Output stream to write file
                        OutputStream output = new FileOutputStream(
                                "/sdcard/24627_AIAEngineeringLtd_14_Sep_15.pdf");

                        byte[] data = new byte[1024];
                        long total = 0;
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress(""
                                    + (int) ((total * 100) / lenghtOfFile));

                            // writing data to file
                            output.write(data, 0, count);
                        }

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();

                    } catch (Exception e) {
                        Log.e("Error: ", e.getMessage());
                    }
                } else if (string.endsWith(".xlsx")) {
                    try {
                        URL url = new URL(f_url[0]);
                        URLConnection conection = url.openConnection();
                        conection.connect();
                        // getting file length
                        int lenghtOfFile = conection.getContentLength();

                        // input stream to read file - with 8k buffer
                        InputStream input = new BufferedInputStream(
                                url.openStream(), 8192);

                        // Output stream to write file
                        OutputStream output = new FileOutputStream(
                                "/sdcard/IEX151016SCH_MPL0021_GJ0_AIA_Engineering_Ltd_d.xlsx");

                        byte[] data = new byte[1024];
                        long total = 0;
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress(""
                                    + (int) ((total * 100) / lenghtOfFile));

                            // writing data to file
                            output.write(data, 0, count);
                        }

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();

                    } catch (Exception e) {
                        Log.e("Error: ", e.getMessage());
                    }
                } else if (string.endsWith(".xls")) {
                    try {
                        URL url = new URL(f_url[0]);
                        URLConnection conection = url.openConnection();
                        conection.connect();
                        // getting file length
                        int lenghtOfFile = conection.getContentLength();

                        // input stream to read file - with 8k buffer
                        InputStream input = new BufferedInputStream(
                                url.openStream(), 8192);

                        // Output stream to write file
                        OutputStream output = new FileOutputStream(
                                "/sdcard/IEX151015DOR_MPL0021_GJ0_AIA_Engineering_Ltd.xls");

                        byte[] data = new byte[1024];
                        long total = 0;
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            publishProgress(""
                                    + (int) ((total * 100) / lenghtOfFile));

                            // writing data to file
                            output.write(data, 0, count);
                        }

                        // flushing output
                        output.flush();

                        // closing streams
                        output.close();
                        input.close();

                    } catch (Exception e) {
                        Log.e("Error: ", e.getMessage());
                    }
                }

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            prgDialog1.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            prgDialog1.dismiss();
            // Displaying downloaded image into image view
            // Reading image path from sdcard
            //String imagePath = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.jpg";
            // setting downloaded into image view
            //my_image.setImageDrawable(Drawable.createFromPath(imagePath));
        }
    }


}