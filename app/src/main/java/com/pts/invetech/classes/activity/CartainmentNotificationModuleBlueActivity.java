package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.os.Bundle;
import androidx.core.app.NotificationCompat.Builder;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.pts.invetech.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CartainmentNotificationModuleBlueActivity extends Activity {

    private String[] mTestArray;
    private ViewPager mbanner;
    private Point p;
    private Context context;
    private LinearLayout mopenLayout;
    private Animation animbounce;
    private String place;
    private Builder mBuilder;
    private int id = 1;
    private NotificationManager myNotificationManager;
    private int notificationIdOne = 111;
    private int numMessagesOne = 0;
    private int numMessagesTwo = 0;
    private SQLiteDatabase db;
    private String dbacessone;
    private Button btnknowmore;
    private WebView webviewActionView;
    private String icontypefrom, dbacess, displaydate, displaytime;
    private String[] icontypearray;
    private String[] array;
    private String[] arraydispalydate;
    private String[] arraydispalytime;
    private Integer[] elementiconarray;
    private TextView curtailmenttitle, curtailmentdate, tvclickhere;
    private String revision, date, colorget;
    private ScrollView customscrrollview;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_curtailmentnotificationmoduleblue);
        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);

        Cursor cu = db.rawQuery("SELECT * FROM baby", null);
        if (cu.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (cu.moveToNext()) {
            buffer.append("access_key: " + cu.getString(0) + "\n");
            dbacessone = cu.getString(0);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
            //buffer.append("access_key: "+c.getString(0)+"\n");
            //Toast.makeText(getApplicationContext(), "access_key" + c.getColumnIndex("access_key"), Toast.LENGTH_LONG).show();
        }
        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        webviewActionView = (WebView) findViewById(R.id.webviewActionView);

        customscrrollview = (ScrollView) findViewById(R.id.customscrrollview);
        curtailmenttitle = (TextView) findViewById(R.id.curtailmenttitle);
        curtailmentdate = (TextView) findViewById(R.id.curtailmentdate);
        tvclickhere = (TextView) findViewById(R.id.tvclickhere);
        btnknowmore = (Button) findViewById(R.id.btnknowmore);

        Cursor c = db.rawQuery("SELECT * FROM pushnotification", null);
        //	goMessage.setText("All Notification:" + c.getCount());
        if (c.getCount() == 0) {
            //showMessage("Error", "No Notification found");
            //Toast.makeText(NotificationActivity.this, "No Notification found", Toast.LENGTH_LONG).show();
        } else {
            icontypearray = new String[c.getCount()];
            array = new String[c.getCount()];
            arraydispalydate = new String[c.getCount()];
            arraydispalytime = new String[c.getCount()];
            elementiconarray = new Integer[c.getCount()];
            int i = 0;
            while (c.moveToNext()) {
                buffer.append("dbacess: " + c.getString(0) + "\n");
                String dbacessfrom = c.getString(0);

                String[] parts = dbacessfrom.split("/");
                icontypefrom = parts[0];
                dbacess = parts[1];
                displaydate = c.getString(1);
                displaytime = c.getString(2);
                //goMessage.setText("All Notification:" + dbacess);
                array[i] = dbacess;
                arraydispalydate[i] = displaydate;
                arraydispalytime[i] = displaytime;
                icontypearray[i] = icontypefrom;

                i++;
                Log.d("icontype: ", icontypefrom);
                Log.d("dbacess: ", dbacess);
                Log.d("displaydateandtime: ", displaydate);
                Log.d("displaydateandtime: ", displaytime);
                //Toast.makeText(getActivity(), dbacess , Toast.LENGTH_LONG).show();
            }
            List<String> icontypearraylist = Arrays.asList(icontypearray);
            Collections.reverse(icontypearraylist);
            icontypearray = (String[]) icontypearraylist.toArray();
            String lastelement = icontypearray[0];

            List<String> list = Arrays.asList(array);
            Collections.reverse(list);
            array = (String[]) list.toArray();
            String lastelementdetail = array[0];

            List<String> displaydatelist = Arrays.asList(arraydispalydate);
            Collections.reverse(displaydatelist);
            arraydispalydate = (String[]) displaydatelist.toArray();
            String lastelementdtate = arraydispalydate[0];

            List<String> displaytimelist = Arrays.asList(arraydispalytime);
            Collections.reverse(displaytimelist);
            arraydispalytime = (String[]) displaytimelist.toArray();
            try {
                JSONObject jsonObj = new JSONObject(lastelementdetail);
                String message = jsonObj.getString("message");
                revision = jsonObj.getString("revision");
                date = jsonObj.getString("date");
                colorget = jsonObj.getString("color");
                String button = jsonObj.getString("button");
                curtailmenttitle.setText(message);
                curtailmentdate.setText(date);
                curtailmentdate.setVisibility(View.VISIBLE);
                if (button.equalsIgnoreCase("hide")) {
                    tvclickhere.setVisibility(View.GONE);
                    btnknowmore.setVisibility(View.GONE);
                } else if (button.equalsIgnoreCase("show")) {
                    tvclickhere.setVisibility(View.VISIBLE);
                    btnknowmore.setVisibility(View.VISIBLE);
                } else {

                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        webviewActionView.loadUrl("file:///android_asset/blue_bell.gif");
        //webviewActionView.setBackgroundColor(color.curtailmentblue);
			/*if(colorget.equalsIgnoreCase("precurtailment")){
			customscrrollview.setBackgroundColor(color.red);
			webviewActionView.setBackgroundColor(color.red);
			}
			else if (colorget.equalsIgnoreCase("NLDCTOIEX")) {
			customscrrollview.setBackgroundColor(color.red);
			webviewActionView.setBackgroundColor(0x0000FF);
			}
			else if(colorget.equalsIgnoreCase("NOTAFFECTED")){
			customscrrollview.setBackgroundColor(color.dark_blue);
			webviewActionView.setBackgroundColor(0x0000FF);
			}
			else if(colorget.equalsIgnoreCase("REALTIME")){
			customscrrollview.setBackgroundColor(color.dark_blue);
			webviewActionView.setBackgroundColor(0x0000FF);
			}
			else if(colorget.equalsIgnoreCase("RESTORE")){
			customscrrollview.setBackgroundColor(color.dark_blue);
			webviewActionView.setBackgroundColor(0x0000FF);
			}
			else{
			 customscrrollview.setBackgroundColor(color.white);
			 webviewActionView.setBackgroundColor(0xFFFFFF);	
			}*/
        //webviewActionView.loadUrl("https://www.hackread.com/wp-content/uploads/2016/02/alarm-london.gif");
	        /*webviewActionView.setWebViewClient(new MyWebViewClient());
	        webviewActionView.getSettings().setJavaScriptEnabled(true);

	        GifWebView view = new GifWebView(this, stream);
	        webviewActionView.addView(view);*/


        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        animbounce = AnimationUtils.loadAnimation(CartainmentNotificationModuleBlueActivity.this, R.anim.bounce);
        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent in = new Intent(CartainmentNotificationActivity.this, MainActivity.class);
                //  startActivity(in);
                //   overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });


        btnknowmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
                Intent in;
                if (loginc.getCount() == 0) {
                    //showMessage("Error", "No records found");
                    in = new Intent(CartainmentNotificationModuleBlueActivity.this, LoginInActivity.class);
                    in.putExtra("revision", revision);
                    in.putExtra("date", date);
                    startActivity(in);
                    overridePendingTransition(R.anim.animation, R.anim.animation2);
                } else {
                    in = new Intent(CartainmentNotificationModuleBlueActivity.this, ScheduleVsCurtailmentActivity.class);
                    in.putExtra("revision", revision);
                    in.putExtra("date", date);
                    startActivity(in);
                    overridePendingTransition(R.anim.animation, R.anim.animation2);
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
		 /*Intent in = new Intent(CartainmentNotificationActivity.this, MainActivity.class);
		 startActivity(in);
		 overridePendingTransition(R.anim.animation, R.anim.animation2);*/
        finish();
        return;
    }


}