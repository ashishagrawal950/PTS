
package com.pts.model.stationmain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NERLDC {

    @SerializedName("available")
    @Expose
    private String available;
    @SerializedName("select")
    @Expose
    private String select;
    @SerializedName("revision")
    @Expose
    private Integer revision;

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public Integer getRevision() {
        return revision;
    }

    public void setRevision(Integer revision) {
        this.revision = revision;
    }

}
