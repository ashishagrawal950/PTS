package com.pts.invetech.dashboardupdate.views;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.handler.NetworkHandlerString;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseFragment;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.classes.activity.LastYearMarketPriceActivity;
import com.pts.invetech.classes.activity.LoginInActivity;

import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;

import com.pts.invetech.R;

import com.pts.invetech.classes.fragment.LandedCostCalculatorFragment;
import com.pts.invetech.classes.fragment.MCP_ComparisionFragment;
import com.pts.invetech.classes.fragment.MonthlyATCFragment;
import com.pts.invetech.classes.fragment.NotificationSettingFragment;
import com.pts.invetech.classes.fragment.RealtimeSchedulingFragment;
import com.pts.invetech.classes.fragment.TransmissionCorridorFragment;
import com.pts.invetech.dashboardupdate.adapters.DashBoardMarqueeAdapter;
import com.pts.invetech.dashboardupdate.adapters.GraphHListAdapter;
import com.pts.invetech.dashboardupdate.adapters.McpTableAdapter;
import com.pts.invetech.dashboardupdate.adapters.RecAdapter;
import com.pts.invetech.dashboardupdate.adapters.RecyclerLayoutManager;
import com.pts.invetech.dashboardupdate.adapters.RecyclerVerticalManager;
import com.pts.invetech.dashboardupdate.adapters.StateAdapter;
import com.pts.invetech.dashboardupdate.background.GpsTrackerUpdate;
import com.pts.invetech.dashboardupdate.background.NewsIntentService;
import com.pts.invetech.dashboardupdate.background.NewsResultReceiver;
import com.pts.invetech.dashboardupdate.fragments.SubNewsFragment;
import com.pts.invetech.dashboardupdate.lib.AutoTypeTextView;
import com.pts.invetech.dashboardupdate.lib.FragmentViewPagerAdapter;
import com.pts.invetech.dashboardupdate.model.dashboard.DashBoardData;
import com.pts.invetech.dashboardupdate.model.dashboard.Mcp;
import com.pts.invetech.dashboardupdate.model.dashboard.RegionLdc;
import com.pts.invetech.dashboardupdate.model.dashboard.Scheduling;
import com.pts.invetech.dashboardupdate.model.graphs.GraphLine;
import com.pts.invetech.dashboardupdate.model.graphs.GraphPie;
import com.pts.invetech.dashboardupdate.model.news.NewsDataInter;
import com.pts.invetech.dashboardupdate.model.news.NewsDataUpdate;
import com.pts.invetech.dashboardupdate.model.state.State;
import com.pts.invetech.dashboardupdate.model.state.StateList;
import com.pts.invetech.dashboardupdate.parsers.NewsDataParser;
import com.pts.invetech.stateload.StateLoadFragment;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Constant;
import com.pts.model.ursdashboard.URSDashboard;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class DashBoardUpdate extends Fragment implements NewsResultReceiver.Receiver, View.OnClickListener {

    private final String URL_DASHBOARD = "https://www.mittalpower.com/pts-client/api/index.php/mobile/getdashboarddata";
    private final String URL_STATELIST = "https://www.mittalpower.com/mobile/pxs_app/service/getstatenamefromgeo.php";
    private final String URL_NEWSFEED = "https://www.mittalpower.com/mobile/pxs_app/service/getnewsfeedinfo.php";
    private final String URL_URSDASHBOARD = "http://ntpc.powertradingsolutions.com/services/mobile_service/new_urs/getursdashboarddata.php";
    private final int CONSTANT_NEWS = 1;
    private final int CONSTANT_STATE = 2;
    private final int CONSTANT_DASHBOARD = 3;
    private final int CONSTANT_URSDASHBOARD = 4;
    private final long DELAY_TIME = 1000 * 60 * 10; // * 10 is for  10 minutes to change minute change value of 10
    private RecyclerLayoutManager mLayoutManager;
    private RecyclerVerticalManager verticalManager;
    private boolean boolType = false, boolState = false;
    private String currentTYpe = "IEX";
    private int currentStateID = 1;
    private String iexregion;
    //    private NewsData newsData;
    private NewsDataUpdate newsDataUpdate;
    private RecyclerView recView;
    private NewsResultReceiver resultReceiver;
    private Timer mTimer;
    private LineData mcpLineGraph;
    private RecyclerView horizontalChartView;

    private boolean[] lineVals = {true, true, true};
    // private int[] lineColors = {Color.parseColor("#49da00"), Color.BLACK, Color.parseColor("#01dede")};
    private int[] lineColors = {Color.RED, Color.BLACK, Color.BLUE};
    private int[] lineIds = {R.id.color_min, R.id.color_avg, R.id.color_max};
    private RelativeLayout tutorial;
    private TimerTask aTask;
    private String loginconfig, device_id, domain;
    private DashBoardData dashBoardData;
    private URSDashboard ursDashboard;
    private SQLiteDatabase db;
    private TextView txtiexregion;
    private boolean isFirst = true;
    private View.OnClickListener graphClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (view.getId() == R.id.parentlinechart) {
                StateLoadFragment frag = new StateLoadFragment();
                LinearLayout demand = view.findViewById(R.id.data_demand);

                Bundle bundle = new Bundle();
                bundle.putParcelable(Constant.VALUE_STATELOAD, dashBoardData.getStateload());

                int val = (int) demand.getTag();
                if (val == 1) {
                    bundle.putBoolean(Constant.VALUE_RLDC, true);
                } else {
                    bundle.putBoolean(Constant.VALUE_RLDC, false);
                }

                frag.setArguments(bundle);
                moveToOtherFragment(frag);

            } else if (view.getId() == R.id.graphpie) {
                RealtimeSchedulingFragment realtimeSchedulingFragment = new RealtimeSchedulingFragment();
                moveToOtherFragment(realtimeSchedulingFragment);
            }
        }
    };
    private NetworkCallBack callBack = new NetworkCallBack() {

        @Override
        public void onResultObject(Object data, int id) {
            if (data == null) {
//                AppLogger.showToastLong(getActivity(), "Some Error Occurred in id - " + id);
                return;
            }
            switch (id) {
                case CONSTANT_STATE:
                    StateList stateList = (StateList) data;
                    if (getView() != null) {
                        setSpinnerRegion(getView(), stateList);
                        //stateList.getIexregionname();
                        try {
                            SharedPrefHandler.saveStateList(getActivity(), stateList.getStates());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    break;

                case CONSTANT_DASHBOARD:
                    dashBoardData = (DashBoardData) data;
                    updateUI(dashBoardData);
                    break;

                case CONSTANT_URSDASHBOARD:
                    ursDashboard = (URSDashboard) data;
                    updateURSUI(ursDashboard);
                    break;

            }
        }

        @Override
        public void onResultString(String data, int id) {
            if (data == null) {
//                AppLogger.showToastLong(getActivity(), "Some Error Occurred in News");
                return;
            }

            switch (id) {
                case CONSTANT_NEWS:
                    try {
                        newsDataUpdate = NewsDataParser.parseNews(data);
                        if (getView() != null) {
                            setNewsFeedDashBoard(getView(), newsDataUpdate);
                            setNewsPanel(getView(), newsDataUpdate);
                        }
                    } catch (JSONException e) {
                        AppLogger.showMsgWithoutTag(e.getLocalizedMessage() + "");
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!isFirst){
            domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        }
        int stateId = SharedPrefHandler.getIntData(getActivity().getApplicationContext(), getString(R.string.db_stateid));
        if (stateId < 0) {
            Location location = GpsTrackerUpdate.getLastLocation(getActivity());
            if (location == null) {
                SharedPrefHandler.saveInt(getActivity().getApplicationContext(), getString(R.string.db_stateid), 1);
                getStatesWoLoc(1, "IEX");
            } else {
                try {
                    getStateList(location);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            if(isFirst){
                getStatesWoLoc(stateId, "IEX");
            }else{
                getStatesWoLoc(stateId, "IEX");
            }

        }

        if(isFirst){
            new NetworkHandlerString(getActivity(), callBack, CONSTANT_NEWS).execute(URL_NEWSFEED, null);
        }else{
            new NetworkHandlerString(getActivity(), callBack, CONSTANT_NEWS).execute(domain + "/mobile/pxs_app/service/getnewsfeedinfo.php", null);
        }


        if (resultReceiver == null) {
            resultReceiver = new NewsResultReceiver(new Handler());
        }
    }

    private void getStatesWoLoc(int stateId, String type) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("state_id", stateId);
            obj.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(isFirst){
            new NetworkHandlerModel(getActivity(), callBack, StateList.class, CONSTANT_STATE).execute(URL_STATELIST, obj.toString());
        }else{
            new NetworkHandlerModel(getActivity(), callBack, StateList.class, CONSTANT_STATE).execute(domain + "/mobile/pxs_app/service/getstatenamefromgeo.php", obj.toString());
        }

    }

    private void getStateList(Location loc) throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("latitude", loc.getLatitude());
        obj.put("logtitude", loc.getLongitude());
        if(isFirst){
            new NetworkHandlerModel(getActivity(), callBack, StateList.class, CONSTANT_STATE).execute(URL_STATELIST, obj.toString());
        }else{
            new NetworkHandlerModel(getActivity(), callBack, StateList.class, CONSTANT_STATE).execute(domain + "/mobile/pxs_app/service/getstatenamefromgeo.php", obj.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.update_dashboard, container, false);

        db = getActivity().openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }

        ImageView imgToggle = view.findViewById(R.id.dashtoggle);
        imgToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((NewFeedMainActivity) getActivity()).open();
            }
        });

        LinearLayout layout = view.findViewById(R.id.lay_min);
        layout.setOnClickListener(this);

        layout = view.findViewById(R.id.lay_avg);
        layout.setOnClickListener(this);

        layout = view.findViewById(R.id.lay_max);
        layout.setOnClickListener(this);

        setSpinnerType(view);

//        boolean isFirst=SharedPrefHandler.getBoolean(getActivity(),"com.ptssharedpref.firstanim");
//        tutorial= (RelativeLayout) view.findViewById(R.id.tutorialscreen);
//        tutorial.setVisibility(View.GONE);
//
//        if(!isFirst){
//            tutorial.setVisibility(View.GONE);
//            tutorial.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    tutorial.setVisibility(View.GONE);
//                    tutorial.setOnClickListener(null);
//                }
//            });
//            SharedPrefHandler.saveBoolean(getActivity(),"com.ptssharedpref.firstanim",true);
//        }


        ImageView ivrealtime = view.findViewById(R.id.ivrealtime);
        ImageView ivcorridor = view.findViewById(R.id.ivcorridor);
        ImageView ivatc = view.findViewById(R.id.ivatc);
        ImageView ivstateload = view.findViewById(R.id.ivstateload);
        ImageView ivcalulator = view.findViewById(R.id.ivcalulator);
        ImageView ivnotification = view.findViewById(R.id.ivnotification);

        Button btn_mcp_comparision = view.findViewById(R.id.btn_mcp_comparision);

        LinearLayout llgraphclick = view.findViewById(R.id.llgraphclick);
        ImageView ivgraphclick = view.findViewById(R.id.ivgraphclick);

        LinearLayout llnrldc = view.findViewById(R.id.llnrldc);
        LinearLayout llwrldc = view.findViewById(R.id.llwrldc);
        LinearLayout llsrldc = view.findViewById(R.id.llsrldc);
        LinearLayout llnerldc = view.findViewById(R.id.llnerldc);
        LinearLayout llerldc = view.findViewById(R.id.llerldc);

        LinearLayout lltoplogin = view.findViewById(R.id.lltoplogin);
        LinearLayout lltoplogout = view.findViewById(R.id.lltoplogout);
        TextView tvlogintext = view.findViewById(R.id.tvlogintext);
        TextView tvlogouttext = view.findViewById(R.id.tvlogouttext);

        Intent in = getActivity().getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");
        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            //String companyname = loginconfig.substring(0, 6);
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        ivrealtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFirst", true);
                RealtimeSchedulingFragment realtimeSchedulingFragment = new RealtimeSchedulingFragment();
                if (realtimeSchedulingFragment != null) {
                    realtimeSchedulingFragment.setArguments(bundle);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, realtimeSchedulingFragment)
                            .commit();
                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });


        ivcorridor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFirst", true);
                TransmissionCorridorFragment transmissionCorridorFragment = new TransmissionCorridorFragment();
                if (transmissionCorridorFragment != null) {
                    transmissionCorridorFragment.setArguments(bundle);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, transmissionCorridorFragment)
                            .commit();

                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });


        ivatc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFirst", true);
                MonthlyATCFragment monthlyATCFragment = new MonthlyATCFragment();
                if (monthlyATCFragment != null) {
                    monthlyATCFragment.setArguments(bundle);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, monthlyATCFragment)
                            .commit();
                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });


        ivcalulator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFirst", true);
                LandedCostCalculatorFragment landedCostCalculatorFragment = new LandedCostCalculatorFragment();
                if (landedCostCalculatorFragment != null) {
                    landedCostCalculatorFragment.setArguments(bundle);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, landedCostCalculatorFragment)
                            .commit();
                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });


        ivstateload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFirst", true);
                StateLoadFragment stateLoadFragment = new StateLoadFragment();
                if (stateLoadFragment != null) {
                    stateLoadFragment.setArguments(bundle);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, stateLoadFragment)
                            .commit();
                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        ivnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFirst", true);
                NotificationSettingFragment notificationSettingFragment = new NotificationSettingFragment();
                if (notificationSettingFragment != null) {
                    notificationSettingFragment.setArguments(bundle);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, notificationSettingFragment)
                            .commit();

                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });

        btn_mcp_comparision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFirst", true);
                MCP_ComparisionFragment mCP_ComparisionFragment = new MCP_ComparisionFragment();
                if (mCP_ComparisionFragment != null) {
                    mCP_ComparisionFragment.setArguments(bundle);
                    /*Spinner spinner_region = (Spinner) getView().findViewById(R.id.spinner_region);
                    StateAdapter sAdapter=(StateAdapter)spinner_region.getAdapter();
                    ArrayList<State> sList=sAdapter.getStatesArrayList();

                    Bundle bundle=new Bundle();
                    bundle.putInt("currentPosition",spinner_region.getSelectedItemPosition());
                    bundle.putParcelableArrayList("stateList",sList);
                    mCP_ComparisionFragment.setArguments(bundle);*/
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_frame, mCP_ComparisionFragment)
                            .commit();
                    //mDrawerLayout.closeDrawer(linearLayout);
                } else {
                    Log.e("MainActivity", "Error in creating fragment");
                }
            }
        });


        llgraphclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<Mcp> mcpList = dashBoardData.getMcp();
                ArrayList<ArrayList<String>> dataHolder = new ArrayList<ArrayList<String>>();
                ArrayList<String> date_Array = new ArrayList<String>();
                String[] nameArr = {"currentyear", "lastyear", "secondlastyear", "previousday"};
                for (Mcp mcp : mcpList) {
                    ArrayList<String> tmp = new ArrayList<String>();
                    tmp.addAll(mcp.getData());
                    dataHolder.add(tmp);
                }
                for (Mcp mcp : mcpList) {
                    date_Array.add(mcp.getDate());
                }
                if (dataHolder.isEmpty()) {
                    Toast.makeText(getActivity(), "Data Not found, Please select another state.", Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(getActivity(), LastYearMarketPriceActivity.class);
                    int index = 0;
                    for (ArrayList<String> tmpStr : dataHolder) {
                        intent.putStringArrayListExtra(nameArr[index], tmpStr);
                        index++;
                    }
                    intent.putExtra("LOGINCONFIG", loginconfig);
                    intent.putStringArrayListExtra("date_Array", date_Array);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                }
            }
        });


        ivgraphclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectionDetector cd = new ConnectionDetector(getActivity());
                if (!cd.isConnectingToInternet()) {
                    Toast.makeText(getActivity(), "Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    List<Mcp> mcpList = dashBoardData.getMcp();
                    ArrayList<ArrayList<String>> dataHolder = new ArrayList<ArrayList<String>>();
                    ArrayList<String> date_Array = new ArrayList<String>();
                    String[] nameArr = {"currentyear", "lastyear", "secondlastyear", "previousday"};
                    for (Mcp mcp : mcpList) {
                        ArrayList<String> tmp = new ArrayList<String>();
                        tmp.addAll(mcp.getData());
                        dataHolder.add(tmp);
                    }
                    for (Mcp mcp : mcpList) {
                        date_Array.add(mcp.getDate());
                    }
                    if (dataHolder.isEmpty()) {
                        Toast.makeText(getActivity(), "Data Not found, Please select another state.", Toast.LENGTH_LONG).show();
                    } else {
                        Intent intent = new Intent(getActivity(), LastYearMarketPriceActivity.class);
                        int index = 0;
                        for (ArrayList<String> tmpStr : dataHolder) {
                            intent.putStringArrayListExtra(nameArr[index], tmpStr);
                            index++;
                        }
                        intent.putExtra("LOGINCONFIG", loginconfig);
                        intent.putStringArrayListExtra("date_Array", date_Array);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    }
                }
            }
        });

//        llnrldc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                db.execSQL("UPDATE availableurs SET region='NRLDC' WHERE region_id='1'");
//                CurrentAvaiableURSFragment currentAvaiableURSFragment = new CurrentAvaiableURSFragment();
//                if (currentAvaiableURSFragment != null) {
//                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.content_frame, currentAvaiableURSFragment)
//                            .commit();
//                    //mDrawerLayout.closeDrawer(linearLayout);
//                } else {
//                    Log.e("MainActivity", "Error in creating fragment");
//                }
//            }
//        });
//
//        llwrldc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                db.execSQL("UPDATE availableurs SET region='WRLDC' WHERE region_id='1'");
//                CurrentAvaiableURSFragment currentAvaiableURSFragment = new CurrentAvaiableURSFragment();
//                if (currentAvaiableURSFragment != null) {
//                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.content_frame, currentAvaiableURSFragment)
//                            .commit();
//                    //mDrawerLayout.closeDrawer(linearLayout);
//                } else {
//                    Log.e("MainActivity", "Error in creating fragment");
//                }
//            }
//        });
//
//        llsrldc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                db.execSQL("UPDATE availableurs SET region='SRLDC' WHERE region_id='1'");
//                CurrentAvaiableURSFragment currentAvaiableURSFragment = new CurrentAvaiableURSFragment();
//                if (currentAvaiableURSFragment != null) {
//                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.content_frame, currentAvaiableURSFragment)
//                            .commit();
//                    //mDrawerLayout.closeDrawer(linearLayout);
//                } else {
//                    Log.e("MainActivity", "Error in creating fragment");
//                }
//            }
//        });
//
//        llnerldc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                db.execSQL("UPDATE availableurs SET region='NERLDC' WHERE region_id='1'");
//                CurrentAvaiableURSFragment currentAvaiableURSFragment = new CurrentAvaiableURSFragment();
//                if (currentAvaiableURSFragment != null) {
//                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.content_frame, currentAvaiableURSFragment)
//                            .commit();
//                    //mDrawerLayout.closeDrawer(linearLayout);
//                } else {
//                    Log.e("MainActivity", "Error in creating fragment");
//                }
//            }
//        });
//
//
//        llerldc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                db.execSQL("UPDATE availableurs SET region='ERLDC' WHERE region_id='1'");
//                CurrentAvaiableURSFragment currentAvaiableURSFragment = new CurrentAvaiableURSFragment();
//                if (currentAvaiableURSFragment != null) {
//                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.content_frame, currentAvaiableURSFragment)
//                            .commit();
//                    //mDrawerLayout.closeDrawer(linearLayout);
//                } else {
//                    Log.e("MainActivity", "Error in creating fragment");
//                }
//            }
//        });


        lltoplogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isVisible()) {
                    return;
                }
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);

                getActivity().finish();
            }
        });

        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                getActivity().finish();
            }
        });

        setUrsDashboardData(device_id, "REGION", "NRLDC");

        return view;
    }

    private void setUrsDashboardData(String device_id, String type, String region) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("device_id", device_id);
            obj.put("type", type);
            obj.put("region", region);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        if(isFirst){
//            new NetworkHandlerModel(getActivity(), callBack, URSDashboard.class, CONSTANT_URSDASHBOARD).execute(URL_URSDASHBOARD, obj.toString());
//        }else{
            new NetworkHandlerModel(getActivity(), callBack, URSDashboard.class, CONSTANT_URSDASHBOARD).execute(domain + "/services/mobile_service/new_urs/getursdashboarddata.php", obj.toString());
//        }


    }

    private void updateURSUI(URSDashboard ursDashboard) {
        if (getView() != null) {
            this.updateURSUI(getView(), ursDashboard);
        }
    }

    private void updateURSUI(View view, URSDashboard ursDashboard) {

        setUrsDetails(view, ursDashboard);
    }

    private void dashBoardApiCall(int stateId, String type) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("state_id", stateId);
            obj.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(isFirst){
            new NetworkHandlerModel(getActivity(), callBack, DashBoardData.class, CONSTANT_DASHBOARD).execute(URL_DASHBOARD, obj.toString());
        }else{
            new NetworkHandlerModel(getActivity(), callBack, DashBoardData.class, CONSTANT_DASHBOARD).execute(domain + "/pts-client/api/index.php/mobile/getdashboarddata", obj.toString());
        }

    }

    private void updateUI(DashBoardData _dashData) {
        if (getView() != null) {
            this.updateUI(getView(), _dashData);
        }
    }

    private void updateUI(View view, DashBoardData dashBoardData) {

        setHorizontalGraphList(view, dashBoardData);

        setMcpLineChart(view, dashBoardData);
        setMarqueeData(view, dashBoardData);

        setTypeTableData(view, dashBoardData);
//        setUrsDetails(view, dashBoardData);
    }

    private void setUrsDetails(View view, URSDashboard ursDashboard) {

        AutoTypeTextView autoOne = view.findViewById(R.id.autoone);
        AutoTypeTextView autoTwo = view.findViewById(R.id.autotwo);
        AutoTypeTextView autoThree = view.findViewById(R.id.autothree);
        AutoTypeTextView autoFour = view.findViewById(R.id.autofour);
        AutoTypeTextView autoFive = view.findViewById(R.id.autofive);

        autoOne.animateDecryption(String.valueOf(ursDashboard.getNRLDC().getResheduleUrsRemaining()));
        autoTwo.animateDecryption(String.valueOf(ursDashboard.getWRLDC().getResheduleUrsRemaining()));
        autoThree.animateDecryption(String.valueOf(ursDashboard.getSRLDC().getResheduleUrsRemaining()));
        autoFour.animateDecryption(String.valueOf(ursDashboard.getNERLDC().getResheduleUrsRemaining()));
        autoFive.animateDecryption(String.valueOf(ursDashboard.getERLDC().getResheduleUrsRemaining()));


//        int[] idHeaders = {R.id.header1, R.id.header2, R.id.header3, R.id.header4, R.id.header5};
//        int[] idAutoVu = {R.id.autoone, R.id.autotwo, R.id.autothree, R.id.autofour, R.id.autofive};

//        List<Ur> urList = ursDashboard.getUrs();
//        int index = 0;
//
//        for (Ur urData : urList) {
//            TextView txt = (TextView) view.findViewById(idHeaders[index]);
//            txt.setText(urData.getName());
//
//            AutoTypeTextView tView = (AutoTypeTextView) view.findViewById(idAutoVu[index]);
//            tView.animateDecryption(String.valueOf(urData.getValue()));
//
//            ++index;
//        }
    }

    private void setTypeTableData(View view, DashBoardData dashBoardData) {
        McpTableAdapter customList = new McpTableAdapter(getActivity(), dashBoardData.getMcp());
        ListView list = view.findViewById(R.id.lv_exchangePrices);
        list.setAdapter(customList);
    }

    private void setNewsFeedDashBoard(View view, NewsDataUpdate newsData) {
        recView = view.findViewById(R.id.recyclerview);
        recView.setHasFixedSize(true);

        RecAdapter mAdapter = new RecAdapter(newsData.getFullNews());
        recView.setAdapter(mAdapter);
        recView.setOverScrollMode(RecyclerView.SCROLL_STATE_SETTLING);

        verticalManager = new RecyclerVerticalManager(getActivity());

        recView.setLayoutManager(verticalManager);
        recView.smoothScrollToPosition(Integer.MAX_VALUE);

        recView.post(new Runnable() {
            @Override
            public void run() {
                recView.smoothScrollToPosition(Integer.MAX_VALUE);
            }
        });

        recView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (!verticalManager.smoothScroller.isRunning()) {
                        verticalManager.againStart();
                    }
                }
                return true;
            }
        });
    }

    private void setSpinnerType(View view) {
        Spinner spinner = view.findViewById(R.id.spinner_pxl);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!boolType) {
                    boolType = true;
                    return;
                }
                int stateId = SharedPrefHandler.getIntData(getActivity().getApplicationContext(), getString(R.string.db_stateid));
                currentTYpe = adapterView.getSelectedItem().toString();
                dashBoardApiCall(stateId, currentTYpe);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        List<String> categories = new ArrayList<String>();
        categories.add("IEX");
        categories.add("PXIL");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    private void setSpinnerRegion(View view, StateList data) {

        Spinner spinner_region = view.findViewById(R.id.spinner_region);
        txtiexregion = view.findViewById(R.id.txtiexregion);
        spinner_region.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!boolState) {
                    boolState = true;
                    return;
                }
                currentStateID = Integer.parseInt(((State) adapterView.getSelectedItem()).getId());
                iexregion = ((State) adapterView.getSelectedItem()).getIexregion();
                txtiexregion.setText("(" + iexregion + ")");
                SharedPrefHandler.saveInt(getActivity().getApplicationContext(), getString(R.string.db_stateid), currentStateID);
                SharedPrefHandler.saveString(getActivity().getApplicationContext(), getString(R.string.db_region), ((State) adapterView.getSelectedItem()).getRegion());
                SharedPrefHandler.saveString(getActivity().getApplicationContext(), getString(R.string.db_iexregion), ((State) adapterView.getSelectedItem()).getIexregion());
                dashBoardApiCall(currentStateID, currentTYpe);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        StateAdapter dataAdapter1 = new StateAdapter(getActivity(), data);
        spinner_region.setAdapter(dataAdapter1);
        currentStateID = SharedPrefHandler.getIntData(getActivity().getApplicationContext(), getString(R.string.db_stateid));
        spinner_region.setSelection(data.getStateIndexSelected(currentStateID));

        // spinner_region.setSelection(data.getStateIndexSelected(currentStateID)+"("+data.getregionIndexSelected(currentStateID)+")");
        // txtiexregion.setText("("+data.getiexregionSelected(currentStateID)+")");
        dashBoardApiCall(currentStateID, "IEX");
    }

    private void setMarqueeData(View view, DashBoardData data) {

        RelativeLayout parent = view.findViewById(R.id.marqueelayout);
        if (parent.getChildCount() > 0) {
            parent.removeAllViews();
        }
        RecyclerView mRecyclerView = new RecyclerView(getActivity());

        mRecyclerView.setHasFixedSize(true);

        DashBoardMarqueeAdapter mAdapter = new DashBoardMarqueeAdapter(data.getLosses());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setOverScrollMode(RecyclerView.SCROLL_STATE_SETTLING);

        mLayoutManager = new RecyclerLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.smoothScrollToPosition(Integer.MAX_VALUE);

        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (!mLayoutManager.smoothScroller.isRunning()) {
                        mLayoutManager.againStart();
                    }
                }
                return true;
            }
        });

        parent.addView(mRecyclerView);
    }

    private void setMcpLineChart(View view, DashBoardData dashData) {

        List<Mcp> mMcp = dashData.getMcp();
        ArrayList<String> labels = new ArrayList<String>();
        ArrayList<Entry> minEnt = new ArrayList<>();
        ArrayList<Entry> maxEnt = new ArrayList<>();
        ArrayList<Entry> avgEnt = new ArrayList<>();

        int index = 0;
        for (Mcp mcp : mMcp) {

            labels.add(mcp.getDate());
            minEnt.add(new Entry(mcp.getMin(), index));
            maxEnt.add(new Entry(mcp.getMax(), index));
            avgEnt.add(new Entry(mcp.getAvg(), index));
            index++;
        }
        LineDataSet dataset = new LineDataSet(minEnt, "Min");
        LineDataSet datasetone = new LineDataSet(maxEnt, "Max");
        LineDataSet datasetAvg = new LineDataSet(avgEnt, "Avg");


        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(dataset);
        dataSets.add(datasetAvg);
        dataSets.add(datasetone);

        mcpLineGraph = new LineData(labels, dataSets);

        dataset.setColor(Color.RED);
        dataset.setValueTextSize(0.5f);
        dataset.setCircleColor(Color.RED);
        dataset.setDrawFilled(false);
        dataset.setCircleSize(2.5f);
        dataset.setDrawCubic(true);

        datasetone.setValueTextSize(0.5f);
        datasetone.setColor(Color.BLUE);
        datasetone.setDrawFilled(false);
        datasetone.setCircleColor(Color.BLUE);
        datasetone.setCircleSize(2.5f);
        datasetone.setDrawCubic(true);

        datasetAvg.setValueTextSize(0.5f);
        datasetAvg.setColor(Color.BLACK);
        datasetAvg.setCircleColor(Color.BLACK);
        datasetAvg.setCircleSize(2.5f);
        datasetAvg.setDrawFilled(false);
        datasetAvg.setDrawCubic(true);


        LineChart graph_power = view.findViewById(R.id.graph_dashboard);
        graph_power.getAxisRight().setEnabled(false);
        graph_power.getLegend().setEnabled(false);
        graph_power.setTouchEnabled(false);
        graph_power.setData(mcpLineGraph);
        graph_power.setDescription("Values in Rupees");
        graph_power.animateY(5000);
    }

    private void setNewsPanel(View view, NewsDataUpdate dashData) {

        ViewPager viewPager = view.findViewById(R.id.viewpager);
        SmartTabLayout viewPagerTab = view.findViewById(R.id.bottom_newstab);
        viewPagerTab.setOnTouchListener(new OnTouch());

        FragmentPagerItems pages = new FragmentPagerItems(getActivity());

        for (NewsDataInter dataInter : dashData.getNewsDataInters()) {
            Bundle bndle1 = new Bundle();
            bndle1.putParcelableArrayList("data", dataInter.getSubNewsList());
            pages.add(FragmentPagerItem.of(dataInter.getTitle(), SubNewsFragment.class, bndle1));
        }

//        Bundle bndle1 = new Bundle();
//        bndle1.putParcelableArrayList("data", dashData.getCoal());
//        pages.add(FragmentPagerItem.of("Coal", SubNewsFragment.class, bndle1));
//
//        Bundle bndle2 = new Bundle();
//        bndle2.putParcelableArrayList("data", dashData.getRenewableEnergy());
//        pages.add(FragmentPagerItem.of("Renewable Energy", SubNewsFragment.class, bndle2));
//
//        Bundle bndle3 = new Bundle();
//        bndle3.putParcelableArrayList("data", dashData.getPower());
//        pages.add(FragmentPagerItem.of("Power", SubNewsFragment.class, bndle3));
//
//        Bundle bndle4 = new Bundle();
//        bndle4.putParcelableArrayList("data", dashData.getNuclear());
//        pages.add(FragmentPagerItem.of("Nuclear Power", SubNewsFragment.class, bndle4));

        FragmentViewPagerAdapter adapter = new FragmentViewPagerAdapter(getFragmentManager(), pages);

        viewPager.setScrollContainer(false);
        viewPager.setAdapter(adapter);
        viewPagerTab.setViewPager(viewPager);
    }

    private void setHorizontalGraphList(View view, DashBoardData dashBoardData) {

        List<Object> hListGraphs = new ArrayList<>();

        PieData data = addData(dashBoardData);

        GraphPie pieData = new GraphPie();
        pieData.setData(data);
        pieData.setTitle("Real Time Scheduling");
        hListGraphs.add(pieData);

        int[] ccc = {Color.parseColor("#F59783"), Color.parseColor("#CBFF76")};
        GraphLine wrldcGraph = new GraphLine();
        wrldcGraph.setColorArr(ccc);
        GraphLine nrldcGraph = new GraphLine();
        nrldcGraph.setColorArr(ccc);

        LineData wrldcLine = getLineDataWrldc(dashBoardData.getStateload().getWRLDC(), ccc);
        //   LineData wrldcLine = getLineDataNrldc(dashBoardData.getStateload().getNRLDC(), ccc);
        LineData nrldcLine = getLineDataNrldc(dashBoardData.getStateload().getNRLDC(), ccc);

        wrldcGraph.setTitle("State load WRLDC");
        wrldcGraph.setLineData(wrldcLine);
        hListGraphs.add(wrldcGraph);

        nrldcGraph.setTitle("State load NRLDC");
        nrldcGraph.setLineData(nrldcLine);
        hListGraphs.add(nrldcGraph);

        horizontalChartView = view.findViewById(R.id.recycleview_piechart);
        GraphHListAdapter listGraphAdapter = new GraphHListAdapter(hListGraphs, graphClick);

        horizontalChartView.setAdapter(listGraphAdapter);

        horizontalChartView.setOverScrollMode(RecyclerView.SCROLL_STATE_SETTLING);

        mLayoutManager = new RecyclerLayoutManager(getActivity());

        horizontalChartView.setLayoutManager(mLayoutManager);

        horizontalChartView.postDelayed(new Runnable() {
            @Override
            public void run() {
                horizontalChartView.smoothScrollToPosition(3);

            }
        }, 1000);

    }

    /*
       Get Graph data's for list
    */
    private LineData getLineDataNrldc(List<RegionLdc> ldcList, int[] ccc) {

        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<Entry> entry = new ArrayList<>();
        String[] xAxis = new String[ldcList.size()];

        ArrayList<LineDataSet> lines = new ArrayList<LineDataSet>();

        int index = 0;
        for (RegionLdc ldc : ldcList) {
            entries.add(new Entry(Float.valueOf(ldc.getDemand()), index));
            entry.add(new Entry(Float.valueOf(ldc.getSchDrawal()), index));

            xAxis[index] = ldc.getState();
            index++;
        }

        LineDataSet lDataSet1 = new LineDataSet(entries, "Demand");
        lDataSet1.setColor(ccc[0]);
        lDataSet1.setCircleColor(ccc[0]);
        lDataSet1.setCircleColorHole(ccc[0]);
        lDataSet1.setFillColor(ccc[0]);
        lDataSet1.setDrawFilled(true);
        lines.add(lDataSet1);

        LineDataSet lDataSet2 = new LineDataSet(entry, "Schedule Drawl");
        lDataSet2.setColor(ccc[1]);
        lDataSet2.setCircleColor(ccc[1]);
        lDataSet2.setCircleColorHole(ccc[1]);
        lDataSet2.setFillColor(ccc[1]);
        lDataSet2.setDrawFilled(true);
        lines.add(lDataSet2);

        return new LineData(xAxis, lines);
    }

    private LineData getLineDataWrldc(List<RegionLdc> ldcList, int[] ccc) {

        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<Entry> entry = new ArrayList<>();
        String[] xAxis = new String[ldcList.size()];

        ArrayList<LineDataSet> lines = new ArrayList<LineDataSet>();

        int index = 0;
        for (RegionLdc ldc : ldcList) {
            entries.add(new Entry(Float.valueOf(ldc.getDemand()), index));
            entry.add(new Entry(Float.valueOf(ldc.getSchDrawal()), index));
            xAxis[index] = ldc.getState();
            index++;
        }

        LineDataSet lDataSet1 = new LineDataSet(entries, "Demand");
        lDataSet1.setColor(ccc[0]);
        lDataSet1.setCircleColor(ccc[0]);
        lDataSet1.setCircleColorHole(ccc[0]);
        lDataSet1.setFillColor(ccc[0]);
        lDataSet1.setDrawFilled(true);
        lines.add(lDataSet1);

        LineDataSet lDataSet2 = new LineDataSet(entry, "Schedule Drawl");
        lDataSet2.setColor(ccc[1]);
        lDataSet2.setCircleColor(ccc[1]);
        lDataSet2.setCircleColorHole(ccc[1]);
        lDataSet2.setFillColor(ccc[1]);
        lDataSet2.setDrawFilled(true);
        lines.add(lDataSet2);

        return new LineData(xAxis, lines);
    }

    private PieData addData(DashBoardData dashBoardData) {

        List<Scheduling> scheduling = dashBoardData.getScheduling();

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < scheduling.size(); i++) {
            yVals1.add(new Entry(scheduling.get(i).getValue(), i));
            xVals.add(scheduling.get(i).getName());
        }

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setSliceSpace(3);
        dataSet.setSelectionShift(5);

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.COLORFUL_COLORS) {
            colors.add(c);
        }

        dataSet.setColors(colors);

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);

        return data;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        try {
            if (resultData != null) {
                String str = resultData.getString("data");
                newsDataUpdate = NewsDataParser.parseNews(str);
                setNewsFeedDashBoard(getView(), newsDataUpdate);
                setNewsPanel(getView(), newsDataUpdate);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.lay_min) {
            updateGraphMin(0);
        } else if (view.getId() == R.id.lay_avg) {
            updateGraphMin(1);
        } else if (view.getId() == R.id.lay_max) {
            updateGraphMin(2);
        }
    }


    /*
        Network callbacks
     */

    private void updateGraphMin(int pos) {
        for (int index = 0; index < lineVals.length; index++) {
            if (index == pos) {
                lineVals[index] = !lineVals[index];
                break;
            }
        }

        int num = 0;
        for (int index = 0; index < lineVals.length; index++) {
            if (lineVals[index]) {
                num++;
            }
        }

        if (num <= 0) {
            for (int index = 0; index < lineVals.length; index++) {
                lineVals[index] = true;
            }
        }

        List<LineDataSet> tmpSet = mcpLineGraph.getDataSets();
        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();

        for (int index = 0; index < lineVals.length; index++) {
            if (lineVals[index]) {
                TextView tv = getView().findViewById(lineIds[index]);
                tv.setBackgroundColor(lineColors[index]);
                dataSets.add(tmpSet.get(index));
            } else {
                TextView tv = getView().findViewById(lineIds[index]);
                tv.setBackgroundColor(Color.GRAY);
            }
        }

        LineData graphhhh = new LineData(mcpLineGraph.getXVals(), dataSets);

        LineChart graph_power = getView().findViewById(R.id.graph_dashboard);
        graph_power.getAxisRight().setEnabled(false);
        graph_power.getLegend().setEnabled(false);
        graph_power.setData(graphhhh);
        graph_power.setDescription("Values in Rupees");
        graph_power.animateY(3000);
    }




    /*
        Timer Related
     */

    @Override
    public void onResume() {
        super.onResume();
        if (resultReceiver != null) {
            resultReceiver.setReceiver(DashBoardUpdate.this);
        }

        if (mTimer == null) {
            startTimer();
        }
    }

    private void startTimer() {
        if (mTimer == null) {
            mTimer = new Timer();
            aTask = new MyTimerTask();
            mTimer.scheduleAtFixedRate(aTask, DELAY_TIME, DELAY_TIME);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (resultReceiver != null) {
            resultReceiver.setReceiver(null);
        }

        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    private void moveToOtherFragment(AppBaseFragment frag) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, frag);
        fragmentTransaction.commit();
    }

    private class OnTouch implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }

    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            if (resultReceiver == null) {
                resultReceiver = new NewsResultReceiver(new Handler());
                resultReceiver.setReceiver(DashBoardUpdate.this);
            }

            Intent intent = new Intent(getActivity(), NewsIntentService.class);
            intent.putExtra("receiverTag", resultReceiver);
            if(isFirst){
                intent.putExtra("url", URL_NEWSFEED);
            }else{
                intent.putExtra("url", domain + "/mobile/pxs_app/service/getnewsfeedinfo.php");
            }
            getActivity().startService(intent);
        }
    }
}