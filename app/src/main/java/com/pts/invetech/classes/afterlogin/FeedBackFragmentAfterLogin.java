package com.pts.invetech.classes.afterlogin;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.R;
import com.pts.invetech.pojo.FeedbackData;
import com.pts.invetech.utils.AppDeviceUtils;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.AppStringUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ashish on 12-01-2017.
 */
public class FeedBackFragmentAfterLogin extends Fragment implements View.OnClickListener {

    private final String URL_FEEDBACK = "https://www.mittalpower.com/mobile/pxs_app/service/submitfeedback.php";
    private EditText mName, mMail, mNumber, mData;
    private LinearLayout openLayout;
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private String loginconfig, domain;
    private View rootView;
    private NetworkCallBack callBack = new NetworkCallBack() {
        @Override
        public void onResultObject(Object data, int id) {
            if (data == null) {
//                AppLogger.showToastLong(getActivity(), "Some Error occurred while sending mail.");
                return;
            }
            FeedbackData feedback = (FeedbackData) data;
            AppLogger.showToastLong(getActivity(), feedback.getMessage());
            mName.setText("");
            mMail.setText("");
            mNumber.setText("");
            mData.setText("");
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
//        domain = getArguments().getString("domain");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frag_feedback, container, false);
        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivityAfterLogin) getActivity()).open();
            }
        });

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        lltoplogin.setVisibility(View.GONE);
        lltoplogout.setVisibility(View.VISIBLE);
        tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
        tvlogouttext.setText("Home");


        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(in);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);

            }
        });

        mName = (EditText) rootView.findViewById(R.id.feedback_name);
        mMail = (EditText) rootView.findViewById(R.id.feedback_mail);
        mNumber = (EditText) rootView.findViewById(R.id.feedback_number);
        mData = (EditText) rootView.findViewById(R.id.feedback_data);
        Button send = (Button) rootView.findViewById(R.id.feedback_send);
        send.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.feedback_send) {
            verifyFeedback();
        }
    }

    private void verifyFeedback() {
        String name = mName.getText().toString();
        String email = mMail.getText().toString();
        String number = mNumber.getText().toString();
        String data = mData.getText().toString();

        if (AppStringUtils.isTextEmpty(name)) {
            AppLogger.showToastLong(getActivity(), getString(R.string.error_empty_name));
            return;
        }

        if (!AppStringUtils.isValidPhoneNumber(number)) {
            AppLogger.showToastLong(getActivity(), getString(R.string.error_empty_number));
            return;
        }

        if (!AppStringUtils.isEmailValid(email)) {
            AppLogger.showToastLong(getActivity(), getString(R.string.error_empty_mail));
            return;
        }

        if (AppStringUtils.isTextLess(data)) {
            AppLogger.showToastLong(getActivity(), getString(R.string.error_empty_data));
            return;
        }

        try {
            sendFeedback(name, email, number, data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendFeedback(String name, String email, String number, String data) throws JSONException {
        JSONObject object = new JSONObject();
        object.put("device_ip", AppDeviceUtils.getTelephonyId(getActivity()));
        object.put("name", name);
        object.put("mobile", number);
        object.put("email", email);
        object.put("feedback", data);
        new NetworkHandlerModel(getActivity(), callBack, FeedbackData.class, 1).execute(domain + "/mobile/pxs_app/service/submitfeedback.php", object.toString());
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP
                        && keyCode == KeyEvent.KEYCODE_BACK) {
                    Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                    return true;
                }
                return false;
            }
        });
    }
}