package com.pts.invetech.dashboardupdate.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.dashboardupdate.model.dashboard.Loss;

import java.util.List;

/**
 * Created by Ashish Karn on 28-11-2016.
 */

public class DashBoardMarqueeAdapter extends RecyclerView.Adapter<DashBoardMarqueeAdapter.ViewHolder> {

    private List<Loss> lList=null;
    private boolean notNull=false;

    public DashBoardMarqueeAdapter(List<Loss> key) {
        lList=key;

        if(key!=null){
            notNull=true;
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_listitem_dashboard_marquee, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(notNull) {
            position = position % lList.size();
            Loss ls=lList.get(position);

            holder.firstView.setText(ls.getName());
            holder.secondView.setText(ls.getValue());
        }

    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView firstView,secondView;

        public ViewHolder(View v) {
            super(v);
            firstView =  (TextView) v.findViewById(R.id.name);
            secondView =  (TextView) v.findViewById(R.id.value);
        }
    }
}