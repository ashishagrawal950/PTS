package com.pts.invetech.customlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.pojo.InnerData;

import java.util.List;

/**
 * Created by Vaibhav on 12-09-2016.
 */
public class ExpandChildAdapter extends ArrayAdapter {

    private LayoutInflater inflater;
    private List<InnerData> dataList;

    public ExpandChildAdapter(Context context,List<InnerData> dList ,LayoutInflater inflte) {
        super(context, R.layout.child_expandible_view);

        inflater=inflte;
        dataList=dList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DataHolder dataHolder;
        if(convertView==null){
            dataHolder=new DataHolder();
            convertView=inflater.inflate(R.layout.child_expandible_view, parent, false);

            dataHolder.slot= (TextView) convertView.findViewById(R.id.dataslot);
            dataHolder.price= (TextView) convertView.findViewById(R.id.dataprice);
            dataHolder.bid= (TextView) convertView.findViewById(R.id.databid);
            dataHolder.buySell= (TextView) convertView.findViewById(R.id.databuysell);
            dataHolder.dataNumber= (TextView) convertView.findViewById(R.id.datanumber);

            convertView.setTag(dataHolder);
        }else{
            dataHolder = (DataHolder) convertView.getTag();
        }

        InnerData dta=getItem(position);

        dataHolder.slot.setText(dta.getBlockfrom()+" - "+dta.getBlockto());
        dataHolder.price.setText(dta.getPrice());
        dataHolder.bid.setText(dta.getBid());
        dataHolder.buySell.setText(dta.getBtype());
        dataHolder.dataNumber.setText(""+(position+1));
        return convertView;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public InnerData getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    private class DataHolder{
        TextView slot;
        TextView price;
        TextView bid;
        TextView buySell;
        TextView dataNumber;
    }
}
