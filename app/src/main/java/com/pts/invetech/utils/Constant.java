package com.pts.invetech.utils;

public class Constant {

	public static final String FALSE = "false";
	public static final String TXT_BLANK = "";

	/*For the MPPL Trading*/
	//public static final String MPPLGENERATEACCESSKEY = "https://www.mittalpower.com/mobile/pxs_app/service/registerdevice.php";
	//public static final String MPPLLOGIN = "https://www.mittalpower.com/mobile/pxs_app/service/login.php";
	public static final String MPPLNEWSLETTER = "https://www.mittalsgroup.com/newsletter/services/newsletter_for_app.php";


	public static final String BASE_URL = "www.mittalpower.com";
	public static final String DEV_URL = "https://dev.invetech.in";
	public static final String REGISTERDEVICE = "/mobile/pxs_app/service/registerdevice.php";
	public static final String LOGIN = "/mobile/pxs_app/service/login.php";
	public static final String HOMEFULLDETAIL = "/mobile/pxs_app/html/fulldetail.php";

	public static final String GETNOBID = "/mobile/pxs_app/service/nobid/get_nobid.php";
	public static final String SAVENOBID =  "/mobile/pxs_app/service/nobid/save_nobid.php";
	public static final String DELETENOBID = "/mobile/pxs_app/service/nobid/delete_nobid.php";

	public static final String APP_BASE_URL="https://www.mittalpower.com";
	public static final String SUBURL_GET_FULL_BID_DETAILS="/mobile/pxs_app/service/newbid/getfullbiddetail.php";
	public static final String SUBURL_COPYBID="/mobile/pxs_app/service/newbid/copybid.php";
	public static final String SUBURL_SUBMIT_BID_DATE_WISE="/mobile/pxs_app/service/newbid/submitbiddatewise.php";

	//TAM
	public static final String GET_TAM_BIDDETAIL="/trading/mobile/pxs_app/service/tam/gettambiddetails.php";
	public static final String SAVE_TAM_BID="/trading/mobile/pxs_app/service/tam/savetambid.php";
	public static final String SUBMIT_TAM_BID="/trading/mobile/pxs_app/service/tam/submittambid.php";
	public static final String DELETE_TAM_BID="/trading/mobile/pxs_app/service/tam/deletetambid.php";


	/*
	* http://mittalpower.com//mobile/pxs_app/service/newurs/stationwisedata.php
	* */

	public static final String STATION_WISE="http://ntpc.powertradingsolutions.com/services/mobile_service/new_urs/";

	public static class Config {
		public static final boolean DEVELOPER_MODE = false;
	}

	public static class Extra {
		public static final String IMAGES = "com.nostra13.example.universalimageloader.IMAGES";
		public static final String IMAGE_POSITION = "com.nostra13.example.universalimageloader.IMAGE_POSITION";
	}

	public static final String VALUE_ACCESSKEY="access_key";

	public static final String VALUE_DATE="date";
	public static final String VALUE_REGION="region";
	public static final String VALUE_MAXREV="revision";

	public static final String VALUE_REVARRAY="revisionarray";
	public static final String VALUE_IMPORTTYPE="importtype";

	public static final String VALUE_DATAID="data_id";
	public static final String VALUE_DEVICEID="device_id";

	public static final String DATA_APPROVAL="data_approval";
	public static final String DATA_REV="data_maxrev";

	public static final String INTENT_SERVICE_URL="ptsserviceweburl";
	public static final String INTENT_SERVICE_DATA="ptservicedata";

	public static final String VALUE_DATAPASS="notifdatapass";
	public static final String VALUE_SHEDULETRACK="sheduletrack";
	public static final String VALUE_APPNUMBERFROMGRAPH="appnumberfromfrom";


	public static final String VALUE_STATELOAD="stateload";
	public static final String VALUE_RLDC="rldc";



	public static final String VALUE_FRMSCH="frmsch";


}
