package com.pts.invetech.classes.activity;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.INetworkCallback;
import com.pts.handler.NetworkHandlerNewModel;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.StateWiseCoalCustomList;
import com.pts.invetech.classes.fragment.CurrentAvaiableURSFragment;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Utility;
import com.pts.model.statewiseurs.Coal;
import com.pts.model.statewiseurs.StateWise;
import com.pts.model.statewiseurs.Station;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class StateURSDetailsActivity extends AppCompatActivity
        implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {
    private SQLiteDatabase db;
    private Dialog prgDialog;
    private String station_name_Send, available_Send, revision_Send;
    private String available, generator_name, region, revision, fromtime, totime, disable;
    private TextView tvgenertorname, tvtotal_amount, tvnodatafound, tvnodata;
    private ListView lv_more;
    private String device_id;
    private LinearLayout openLayout;
    private ProgressBar pbprogressBar;
    private String from, domain;
    private String region_Send, owner_Send;
    private StateWiseCoalCustomList adapter;
    private Map<String, List<Coal>> gaMap;

    private CurrentAvaiableURSFragment currentAvaiableURSFragment = null;
    private Spinner spn_ntpc;
    private ArrayList<String> categories1;
    private LinearLayout spinners;
    private Station station;
    private int check = 0;
    private INetworkCallback callback = new INetworkCallback() {

        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
//                AppLogger.showToastShort(getApplicationContext(), "No Data Found");
                tvnodatafound.setVisibility(View.VISIBLE);
                tvnodatafound.setText(R.string.datanotfound);
                lv_more.setVisibility(View.GONE);
                return;
            }
            tvnodatafound.setVisibility(View.GONE);
            lv_more.setVisibility(View.VISIBLE);
            StateWise stateWise = (StateWise) obj;
            Station station = stateWise.getStation();

            gaMap.put("COAL", station.getCoal());
            gaMap.put("GAS", station.getGas());
            gaMap.put("OTHER", station.getOther());

            String types = spn_ntpc.getSelectedItem().toString();
            if (gaMap.get(types).size() == 0) {
//                AppLogger.showToastShort(StateURSDetailsActivity.this,"No data Found");
                tvnodata.setVisibility(View.VISIBLE);
                tvnodata.setText(R.string.datanotfound);
                lv_more.setVisibility(View.GONE);
                return;
            }
            tvnodata.setVisibility(View.GONE);
            lv_more.setVisibility(View.VISIBLE);
            adapter = new StateWiseCoalCustomList(StateURSDetailsActivity.this, gaMap.get(types));
            lv_more.setAdapter(adapter);
            Utility.setListViewHeightBasedOnChildren(lv_more);


        }

    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_stateurs);
        gaMap = new HashMap<>();
        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
		 /*prgDialog = new ProgressDialog(CurrentAvaiableURSDetailsActivity.this);
	     prgDialog.setMessage("Please wait...");
	     prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        openLayout = (LinearLayout) findViewById(R.id.openLayout);
        tvgenertorname = (TextView) findViewById(R.id.tvgenertorname);
        tvtotal_amount = (TextView) findViewById(R.id.tvtotal_amount);
        pbprogressBar = (ProgressBar) findViewById(R.id.pbprogressBar);

        spn_ntpc = (Spinner) findViewById(R.id.spn_ntpc);
        spn_ntpc.setOnItemSelectedListener(this);

        categories1 = new ArrayList<String>();
        categories1.add("COAL");
        categories1.add("GAS");
        categories1.add("OTHER");
        // Creating adapter for spinnerString types
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_state, categories1);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spn_ntpc.setAdapter(dataAdapter);

        Intent in = getIntent();
        region_Send = in.getStringExtra("region");
        station_name_Send = in.getStringExtra("station_name_Send");
        available_Send = in.getStringExtra("available_Send");
        revision_Send = in.getStringExtra("revision_Send");


        tvgenertorname.setText(station_name_Send);
        tvtotal_amount.setText(available_Send);
        lv_more = (ListView) findViewById(R.id.lv_more);
        spinners = (LinearLayout) findViewById(R.id.spinners);
        tvnodatafound = (TextView) findViewById(R.id.tvnodatafound);
        tvnodata = (TextView) findViewById(R.id.tvnodata);
        from = "start";
        lv_more.setOnItemClickListener(this);

        ConnectionDetector cd = new ConnectionDetector(StateURSDetailsActivity.this);
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(StateURSDetailsActivity.this, "Please Check your network.",
                    Toast.LENGTH_LONG).show();
        } else {
            setStateWise();
        }


        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

		/*new CountDownTimer(120000, 1000) {
			public void onTick(long millisUntilFinished) {
				// mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
			}
			public void onFinish() {
				from = "counter";
					new HttpAsyncTaskgetursdata_generatorwise().execute("http://www.mittalpower.com/mobile/pxs_app/service/getursdata_generatorwise.php");
			}
		}.start();*/

    }

    private void setStateWise() {
        JSONObject param = new JSONObject();
        try {
            param.put("device_id", device_id);
            param.put("region", region_Send);
            param.put("state", station_name_Send);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String[] str = new String[2];

        str[0] = domain + "/services/mobile_service/new_urs/statewisedata.php";
        str[1] = param.toString();

        AppLogger.showMsg("param", param.toString());

        new NetworkHandlerNewModel(this, callback, StateWise.class, 1).execute(str);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == lv_more) {
            String station = adapter.getItem(position).getStation();
//            Pattern regex = Pattern.compile("[$&+,:;=\\\\?@&#|/'<>.^*()%!-]");
            Pattern regex = Pattern.compile("[&]");

            Intent intent = new Intent(this, StationBlockWiseURSActivity.class);
            intent.putExtra("type", "STATEWISE");
            intent.putExtra("region", region_Send);
            if (regex.matcher(station).find()) {
                intent.putExtra("station", adapter.getItem(position).getStation().replaceAll("[&]+", "~"));
            } else {
                intent.putExtra("station", adapter.getItem(position).getStation());
            }

            intent.putExtra("beneficiary", station_name_Send);
            intent.putExtra("revision_Send", revision_Send);
            startActivity(intent);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (++check > 1) {
            String types = spn_ntpc.getSelectedItem().toString();
            if (gaMap.get(types).size() == 0) {
                tvnodata.setVisibility(View.VISIBLE);
                tvnodata.setText(R.string.datanotfound);
                lv_more.setVisibility(View.GONE);
            } else {
                tvnodata.setVisibility(View.GONE);
                lv_more.setVisibility(View.VISIBLE);
                adapter = new StateWiseCoalCustomList(StateURSDetailsActivity.this, gaMap.get(types));
                lv_more.setAdapter(adapter);
                Utility.setListViewHeightBasedOnChildren(lv_more);
            }
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}

