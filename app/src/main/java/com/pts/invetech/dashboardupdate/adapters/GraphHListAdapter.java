package com.pts.invetech.dashboardupdate.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.pts.invetech.R;
import com.pts.invetech.dashboardupdate.model.graphs.GraphLine;
import com.pts.invetech.dashboardupdate.model.graphs.GraphPie;

import java.util.List;

/**
 * Created by Ashish Karn on 02-01-2017.
 */

public class GraphHListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Object> horizontalList;

    private final int GRAPH_LINE = 0, GRAPH_PIE = 1;
    private View.OnClickListener chartSelected;

    public GraphHListAdapter(List<Object> _list, View.OnClickListener _sel) {
        this.horizontalList = _list;
        chartSelected = _sel;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case GRAPH_LINE:
                View v1 = inflater.inflate(R.layout.listitem_dashboard_linechart, parent, false);
                viewHolder = new LineGraphHolder(v1);
                v1.setTag(viewHolder);

                v1.setOnClickListener(chartSelected);
                break;
            case GRAPH_PIE:
                View v2 = inflater.inflate(R.layout.listitem_dashboard_piegraph, parent, false);
                viewHolder = new PieGraphHolder(v2);

                v2.setOnClickListener(chartSelected);
                break;
            default:
                viewHolder = null;
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case GRAPH_LINE:
                LineGraphHolder vh1 = (LineGraphHolder) holder;
                configureLineGraph(vh1, position);
                break;
            case GRAPH_PIE:
                PieGraphHolder vh2 = (PieGraphHolder) holder;
                configurePieGraph(vh2, position);
                break;
            default:

                break;
        }
    }

    private void configurePieGraph(PieGraphHolder holder, int position) {
        GraphPie graphPie = (GraphPie) horizontalList.get(position);
        holder.chart.setData(graphPie.getData());
        holder.txtView.setText(graphPie.getTitle());
    }

    private void configureLineGraph(LineGraphHolder holder, int position) {
        GraphLine graphPie = (GraphLine) horizontalList.get(position);
        holder.chart.setData(graphPie.getLineDataForGraph());
        holder.txtView.setText(graphPie.getTitle());
        holder.demand.setTag(position);
        holder.scheDrwl.setTag(position);
        holder.colorDemand.setBackgroundColor(graphPie.getDemandColor());
        holder.colorScheduling.setBackgroundColor(graphPie.getSchColor());
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }


    @Override
    public int getItemViewType(int position) {
        if (horizontalList.get(position) instanceof GraphLine) {
            return GRAPH_LINE;
        } else if (horizontalList.get(position) instanceof GraphPie) {
            return GRAPH_PIE;
        }
        return -1;
    }


    /*
        Holder Class
    */

    public class PieGraphHolder extends RecyclerView.ViewHolder {

        public TextView txtView;
        public PieChart chart;

        public PieGraphHolder(View view) {
            super(view);
            txtView = (TextView) view.findViewById(R.id.tv_piegraph);
            chart = (PieChart) view.findViewById(R.id.chartitem);
            chart.setUsePercentValues(true);
            chart.setRotationEnabled(false);
           // chart.setDescription("");
            chart.getLegend().setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
            chart.setTouchEnabled(false);
        }
    }


    public class LineGraphHolder extends RecyclerView.ViewHolder {

        public TextView txtView;
        public LineChart chart;
        public LinearLayout demand, scheDrwl;
        public TextView colorDemand, colorScheduling;

        public LineGraphHolder(View view) {
            super(view);
            txtView = (TextView) view.findViewById(R.id.header_line);
            chart = (LineChart) view.findViewById(R.id.item_linechart);
            chart.getAxisRight().setEnabled(false);
           // chart.setDescription("");
            chart.getLegend().setEnabled(false);
            chart.setTouchEnabled(false);

            demand = (LinearLayout) view.findViewById(R.id.data_demand);
            demand.setOnClickListener(demandListner);

            scheDrwl = (LinearLayout) view.findViewById(R.id.data_schdrwl);
            scheDrwl.setOnClickListener(schListner);

            colorDemand = (TextView) view.findViewById(R.id.color_demand);
            colorScheduling = (TextView) view.findViewById(R.id.color_scheduling);
        }
    }

    /*
        OnClick Listners for Demand and Scheduling Drawl
     */

    private View.OnClickListener demandListner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int num = (int) view.getTag();

            GraphLine lineData = (GraphLine) horizontalList.get(num);
            lineData.updateArr(1);

            notifyDataSetChanged();
        }
    };

    private View.OnClickListener schListner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int num = (int) view.getTag();

            GraphLine lineData = (GraphLine) horizontalList.get(num);
            lineData.updateArr(2);

            notifyDataSetChanged();
        }
    };
}