package com.pts.invetech.customlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

import java.util.ArrayList;

public class LastYearMarketPriceCustomList extends ArrayAdapter<String> {
	private final Activity context;
	static ArrayList<String> blockarrayList = new ArrayList<String>();
	static ArrayList<String> actualarrayList = new ArrayList<String>();
	static ArrayList<String> curtailmentarrayList = new ArrayList<String>();
	static ArrayList<String> revisedarrayList = new ArrayList<String>();
	static ArrayList<String> previousdayList = new ArrayList<String>();
	//private int lastPosition = -1;
	
	
	public LastYearMarketPriceCustomList(Activity context, ArrayList<String> blockarrayList,
			ArrayList<String> actualarrayList
			   , ArrayList<String> curtailmentarrayList
			   , ArrayList<String> revisedarrayList,  ArrayList<String> previousday) {
		super(context, R.layout.list_lastyearmarketprice, blockarrayList);
		this.blockarrayList = blockarrayList;
		this.actualarrayList = actualarrayList;
		this.curtailmentarrayList = curtailmentarrayList;
		this.revisedarrayList = revisedarrayList;
		this.previousdayList  = previousday;
		this.context = context;
		
	}
	
	  @Override
	  public View getView(int position, View view, ViewGroup parent) {
	  LayoutInflater inflater = ((Activity) context).getLayoutInflater();
	  View rowView = inflater.inflate(R.layout.list_lastyearmarketprice, null, true);
	  TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
	  TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
	  TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
	  TextView txt4 = (TextView) rowView.findViewById(R.id.txt4);
	  TextView txt5 = (TextView) rowView.findViewById(R.id.txt5);

	  txt1.setText(blockarrayList.get(position));
	  txt2.setText(actualarrayList.get(position));
	  txt3.setText(revisedarrayList.get(position));
	  txt4.setText(previousdayList.get(position));
	  txt5.setText(curtailmentarrayList.get(position));
	  
	 // Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
	 // rowView.startAnimation(animation);
	 // lastPosition = position;
	 //  txtTitle.setImageResource(RechargeAmmout[position]);
	  return rowView;
	 }

	
	}

