package com.pts.handler;

import android.content.Context;

public class AppDbHandler {
    public final static String TABLE_DEVICEID="pts.deviceid";

    public static void saveDeviceId(Context _context, String data){
        SharedPrefHandler.saveDeviceString(_context,TABLE_DEVICEID,data);
    }
}
