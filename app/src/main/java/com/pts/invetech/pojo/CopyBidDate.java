package com.pts.invetech.pojo;

public class CopyBidDate {
	private String copydate;
	private String todate;
	private String access_key;
	public String getCopydate() {
		return copydate;
	}
	public void setCopydate(String copydate) {
		this.copydate = copydate;
	}
	public String getTodate() {
		return todate;
	}
	public void setTodate(String todate) {
		this.todate = todate;
	}
	public String getAccess_key() {
		return access_key;
	}
	public void setAccess_key(String access_key) {
		this.access_key = access_key;
	}
	
	

}
