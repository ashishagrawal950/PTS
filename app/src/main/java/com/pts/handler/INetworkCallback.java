package com.pts.handler;

/**
 * Created by upama on 4/1/17.
 */

public interface INetworkCallback {

    void onUpdateResult(Object obj, int id);
}
