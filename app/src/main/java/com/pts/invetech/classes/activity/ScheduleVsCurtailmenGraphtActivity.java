package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.pts.invetech.AlertDialogManager;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.pojo.ObligationDetails;

import java.util.ArrayList;
import java.util.HashMap;


public class ScheduleVsCurtailmenGraphtActivity extends Activity {

    public static String name;
    public static String email;
    public boolean curtailmentisTrue = true;
    public boolean revisedisTrue = true;
    private Point p;
    private LinearLayout mopenLayout;
    private Context context;
    private Animation animbounce;
    private Dialog prgDialog;
    private TextView tvcompanyName;
    private String date, IEX;
    private HashMap<Integer, ObligationDetails> myHashmap;
    private int i;
    private SQLiteDatabase db;
    private String dbacess, companyName;
    private AlertDialogManager alert = new AlertDialogManager();
    private ConnectionDetector cd;
    private String newMessage;
    private TextView tvnotificationicon;
    private LineChart lineChart;
    private ArrayList<String> block_array = new ArrayList<String>();
    private ArrayList<String> actual_array = new ArrayList<String>();
    private ArrayList<String> curtailment_array = new ArrayList<String>();
    private ArrayList<String> revised_array = new ArrayList<String>();
    private ArrayList<Float> actual_array_graph = new ArrayList<Float>();
    private ArrayList<Float> curtailment_array_graph = new ArrayList<Float>();
    private ArrayList<Float> revised_array_graph = new ArrayList<Float>();
    private LinearLayout llgraphcurtailment, llgraphrevised;
    private TextView tvcurtailmenttext, tvcurtailmentline, tvrevisedtext, tvrevisedline;
    private String revision_get, date_get;
    private ImageView tvback;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_curtailmentgraph);

        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        Cursor c = db.rawQuery("SELECT * FROM baby", null);
        if (c.getCount() == 0) {
            showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("access_key: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
        }
        //	mopenPopup = (LinearLayout) findViewById(R.id.openPopup);
        tvnotificationicon = (TextView) findViewById(R.id.tvnotificationicon);
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
        Cursor cNot = db.rawQuery("SELECT * FROM pushnotificationUnRead", null);
        //	goMessage.setText("All Notification:" + c.getCount());
        if (cNot.getCount() == 0) {
            //mopenPopup.setVisibility(View.GONE);
        } else {
            String count = Integer.toString(cNot.getCount());
            //mopenPopup.setVisibility(View.VISIBLE);
            //tvnotificationicon.setVisibility(View.VISIBLE);
            //tvnotificationicon.setText(count);
        }
		/*prgDialog = new ProgressDialog(this);
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);
        myHashmap = new HashMap<Integer, ObligationDetails>();
        tvcompanyName = (TextView) findViewById(R.id.tvcompanyName);
        //tvcompanyName.setText(companyName);
        //tvcompanyName.setText("Demo Client Pvt. Ltd.");
        tvback = (ImageView) findViewById(R.id.tvback);
        lineChart = (LineChart) findViewById(R.id.navchart);
        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);


        Intent in = getIntent();
        block_array = in.getStringArrayListExtra("block");
        actual_array = in.getStringArrayListExtra("actual");
        curtailment_array = in.getStringArrayListExtra("curtailment");
        revised_array = in.getStringArrayListExtra("revised");
        revision_get = in.getStringExtra("revision");
        date_get = in.getStringExtra("date");
        in.putExtra("revision", revision_get);
        in.putExtra("date", date_get);
        for (int i = 0; i < actual_array.size(); i++) {
            String actuallist = actual_array.get(i);
            Float actualint = Float.parseFloat(actuallist);
            actual_array_graph.add(actualint);
        }

        for (int i = 0; i < curtailment_array.size(); i++) {
            String actuallist = curtailment_array.get(i);
            Float actualint = Float.parseFloat(actuallist);
            curtailment_array_graph.add(actualint);
        }

        for (int i = 0; i < revised_array.size(); i++) {
            String actuallist = revised_array.get(i);
            Float actualint = Float.parseFloat(actuallist);
            revised_array_graph.add(actualint);
        }
        tvback.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent in = new Intent(ScheduleVsCurtailmenGraphtActivity.this, ScheduleVsCurtailmentActivity.class);
                in.putExtra("revision", revision_get);
                in.putExtra("date", date_get);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });
        llgraphcurtailment = (LinearLayout) findViewById(R.id.llgraphcurtailment);
        tvcurtailmenttext = (TextView) findViewById(R.id.tvcurtailmenttext);
        tvcurtailmentline = (TextView) findViewById(R.id.tvcurtailmentline);

        llgraphrevised = (LinearLayout) findViewById(R.id.llgraphrevised);
        tvrevisedtext = (TextView) findViewById(R.id.tvrevisedtext);
        tvrevisedline = (TextView) findViewById(R.id.tvrevisedline);

        drawChartNav();

        llgraphcurtailment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Toast.makeText(MarketPriceActivity.this, "Ashish", Toast.LENGTH_LONG).show();
                if (curtailmentisTrue == true) {
                    curtailmentisTrue = false;
                    String f = "false";
                    tvcurtailmenttext.setTextColor(Color.GRAY);
                    tvcurtailmentline.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(HomeActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    curtailmentisTrue = true;
                    String t = "true";
                    tvcurtailmenttext.setTextColor(Color.GREEN);
                    tvcurtailmentline.setBackgroundColor(Color.GREEN);
                    //Toast.makeText(HomeActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartNav();

            }
        });


        llgraphrevised.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Toast.makeText(MarketPriceActivity.this, "Ashish", Toast.LENGTH_LONG).show();
                if (revisedisTrue == true) {
                    revisedisTrue = false;
                    String f = "false";
                    tvrevisedtext.setTextColor(Color.GRAY);
                    tvrevisedline.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(HomeActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    revisedisTrue = true;
                    String t = "true";
                    tvrevisedtext.setTextColor(Color.RED);
                    tvrevisedline.setBackgroundColor(Color.RED);
                    //Toast.makeText(HomeActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartNav();
				/*if(curtailmentisTrue==false && revisedisTrue==false){
					//Toast.makeText(ScheduleVsCurtailmenGraphtActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
					tvcurtailmenttext.setTextColor(Color.GREEN);
					tvcurtailmentline.setBackgroundColor(Color.GREEN);
					tvrevisedtext.setTextColor(Color.RED);
					tvrevisedline.setBackgroundColor(Color.RED);
					drawChartNav();
				}
				else if(curtailmentisTrue==false && revisedisTrue==true){
					reviseddrawChartNav();
				}
				else if(curtailmentisTrue==true && revisedisTrue==false){
					curtailmentdrawChartNav();
				}
				else if(curtailmentisTrue==true && revisedisTrue==true){
					//Toast.makeText(ScheduleVsCurtailmenGraphtActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
					tvcurtailmenttext.setTextColor(Color.GREEN);
					tvcurtailmentline.setBackgroundColor(Color.GREEN);
					tvrevisedtext.setTextColor(Color.RED);
					tvrevisedline.setBackgroundColor(Color.RED);
					drawChartNav();
				}*/
            }
        });

        //

    }

    private void drawChartNav() {
        // creating list of entry
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<Entry> entriespxil = new ArrayList<>();
        for (int i = 0; i < revised_array_graph.size(); i++) {
            entries.add(new Entry(revised_array_graph.get(i), i));
            entriespxil.add(new Entry(actual_array_graph.get(i), i));
        }

        LineDataSet dataset = new LineDataSet(entries, "");
        dataset.setCircleColor(Color.RED);
        dataset.setColor(Color.RED);
        dataset.setValueTextColor(Color.RED);

        LineDataSet datasetpxil = new LineDataSet(entriespxil, "");
        datasetpxil.setCircleColor(Color.GREEN);
        datasetpxil.setColor(Color.GREEN);
        datasetpxil.setValueTextColor(Color.GREEN);

        if (curtailmentisTrue == false && revisedisTrue == false) {
            //Toast.makeText(ScheduleVsCurtailmenGraphtActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            tvcurtailmenttext.setTextColor(Color.GREEN);
            tvcurtailmentline.setBackgroundColor(Color.GREEN);
            tvrevisedtext.setTextColor(Color.RED);
            tvrevisedline.setBackgroundColor(Color.RED);

            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(dataset);
            dataSets.add(datasetpxil); // add the datasets
            LineData data = new LineData(block_array, dataSets);

            lineChart.setData(data); // set the data and list of lables into chart
            lineChart.animateY(3000);
            curtailmentisTrue = true;
            revisedisTrue = true;
        } else if (curtailmentisTrue == false && revisedisTrue == true) {
            LineData dataset1 = new LineData(block_array, dataset);
            lineChart.setData(dataset1); // set the data and list of lables
            lineChart.animateY(3000);
        } else if (curtailmentisTrue == true && revisedisTrue == false) {
            LineData datapxil1 = new LineData(block_array, datasetpxil);
            lineChart.setData(datapxil1); // set the data and list of lables
            lineChart.animateY(3000);
        } else if (curtailmentisTrue == true && revisedisTrue == true) {
            //Toast.makeText(ScheduleVsCurtailmenGraphtActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
            tvcurtailmenttext.setTextColor(Color.GREEN);
            tvcurtailmentline.setBackgroundColor(Color.GREEN);
            tvrevisedtext.setTextColor(Color.RED);
            tvrevisedline.setBackgroundColor(Color.RED);
            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(dataset);
            dataSets.add(datasetpxil); // add the datasets

            LineData data = new LineData(block_array, dataSets);
            lineChart.setData(data); // set the data and list of lables into chart
            lineChart.animateY(3000);
        }
        lineChart.setDescription("Values in MW/h");  // set the description
        lineChart.setDescriptionColor(Color.WHITE);
        XAxis xl = lineChart.getXAxis();
        xl.setTextColor(Color.WHITE);

        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setTextColor(Color.WHITE);

        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setTextColor(Color.WHITE);

        lineChart.setDrawGridBackground(false);
    }

    public void showMessage(String title, String message) {
        Builder builder = new Builder(ScheduleVsCurtailmenGraphtActivity.this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }


    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(ScheduleVsCurtailmenGraphtActivity.this, ScheduleVsCurtailmentActivity.class);
        in.putExtra("revision", revision_get);
        in.putExtra("date", date_get);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }


}

