package com.pts.invetech.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Call;

public class AppLogger {

    public static void show(String msg) {
        showMsg("TAG", msg);
    }

    public static void showMsgWithoutTag(String msg){
        Log.e("No Tag",msg);
    }

    public static void showMsg(String tag,String msg){
        Log.e(tag,msg);
    }

    public static void showToastLong(Context context,String msg){
        Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
    }

    public static void showToastShort(Context context,String msg){
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    public static void printPostCall(Call call) {
        AppLogger.showMsg("Request --> ", "" + call.request().url().url().toString());
        AppLogger.showError("BODY -->", bodyToString(call.request().body()));
    }
    public static void showError(String tag, String msg) {
        if (tag != null && msg != null) {
            Log.e(tag, msg);
        }
    }

    private static String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}