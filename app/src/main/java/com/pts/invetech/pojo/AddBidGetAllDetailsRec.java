package com.pts.invetech.pojo;

/**
 * Created by Ashish on 01-04-2016.
 */
public class AddBidGetAllDetailsRec {

    public String k;
    public String id;
    public String clientid;
    public String date;
    public String type;
    public String rectype;
    public String price;
    public String noofrec;
    public String ordernature;
    public String status;
    public String tstatus;
    public String extra;
    public String entryby;
    public String timestamp;
    public  String noofcertificates;


    public AddBidGetAllDetailsRec(String k,String id,String clientid,String  date,String type,String rectype,
                               String price,String noofrec,String  ordernature,String status,String  tstatus,
                               String extra,String entryby, String timestamp ) {
        // TODO Auto-generated constructor stub
        this.k = k;
        this.id = id;
        this.clientid = clientid;
        this.date = date;
        this.type = type;
        this.rectype =rectype;
        this.price =price;
        this.noofrec=noofrec;
        this.ordernature =ordernature;
        this.status =status;
        this.tstatus =tstatus;
        this.extra =extra;
        this.entryby =entryby;
        this.timestamp =timestamp;
    }

    public AddBidGetAllDetailsRec(String k,String id,String clientid,String  date,String type,
                                  String price,String noofcertificates,String  ordernature,String status,String  tstatus,
                                  String extra,String entryby, String timestamp ) {
        // TODO Auto-generated constructor stub
        this.k = k;
        this.id = id;
        this.clientid = clientid;
        this.date = date;
        this.type = type;
        this.price =price;
        this.noofcertificates=noofcertificates;
        this.ordernature =ordernature;
        this.status =status;
        this.tstatus =tstatus;
        this.extra =extra;
        this.entryby =entryby;
        this.timestamp =timestamp;
    }

    public String getK() {
        return k;
    }

    public void setK(String k) {
        this.k = k;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRectype() {
        return rectype;
    }

    public void setRectype(String rectype) {
        this.rectype = rectype;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNoofrec() {
        return noofrec;
    }

    public void setNoofrec(String noofrec) {
        this.noofrec = noofrec;
    }

    public String getOrdernature() {
        return ordernature;
    }

    public void setOrdernature(String ordernature) {
        this.ordernature = ordernature;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getTstatus() {
        return tstatus;
    }

    public void setTstatus(String tstatus) {
        this.tstatus = tstatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEntryby() {
        return entryby;
    }

    public void setEntryby(String entryby) {
        this.entryby = entryby;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNoofcertificates() {
        return noofcertificates;
    }

    public void setNoofcertificates(String noofcertificates) {
        this.noofcertificates = noofcertificates;
    }

    @Override
    public String toString() {
        return "AddBidGetAllDetailsRec[id=" + id + ", date=" + date
                + ", price=" + price + ", ]";
    }

}
