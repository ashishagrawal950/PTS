package com.pts.invetech.customlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

import java.util.ArrayList;

/**
 * Created by Ashish on 21-02-2017.
 */

public class AdapterMCPComparisionTwo extends ArrayAdapter<String> {
    private final Activity context;
    static ArrayList<String> blockarrayListfrom = new ArrayList<String>();
    static ArrayList<String> lastyearmarketprice_Arrayone = new ArrayList<String>();
    static ArrayList<String> lastyearmarketprice_Arraytwo = new ArrayList<String>();


    public AdapterMCPComparisionTwo(Activity context, ArrayList<String> blockarrayListfrom,
                                      ArrayList<String> lastyearmarketprice_Arrayone
            , ArrayList<String> lastyearmarketprice_Arraytwo)

    {
        super(context, R.layout.listitem_mcp_list_two, blockarrayListfrom);
        this.blockarrayListfrom = blockarrayListfrom;
        this.lastyearmarketprice_Arrayone = lastyearmarketprice_Arrayone;
        this.lastyearmarketprice_Arraytwo = lastyearmarketprice_Arraytwo;
        this.context = context;

    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View rowView = inflater.inflate(R.layout.listitem_mcp_list_two, null, true);
        TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
        TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
        TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);


        txt1.setText(blockarrayListfrom.get(position));
        txt2.setText(lastyearmarketprice_Arrayone.get(position));
        txt3.setText(lastyearmarketprice_Arraytwo.get(position));

        return rowView;
    }


}