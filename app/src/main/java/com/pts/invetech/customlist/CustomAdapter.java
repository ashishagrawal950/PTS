package com.pts.invetech.customlist;

import java.util.ArrayList;

import com.pts.invetech.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapter extends BaseAdapter{
    private Context mContext;
    private final ArrayList<String> web;
    private final ArrayList<Integer> Imageid;

      public CustomAdapter(Context c,ArrayList<String> complete_attendancestaff_namearray,ArrayList<Integer> complete_attendancestaff_imgarray ) {
          mContext = c;
          this.Imageid = complete_attendancestaff_imgarray;
          this.web = complete_attendancestaff_namearray;
      }

      @Override
      public int getCount() {
          // TODO Auto-generated method stub
          return web.size();
      }

      @Override
      public Object getItem(int position) {
          // TODO Auto-generated method stub
          return null;
      }

      @Override
      public long getItemId(int position) {
          // TODO Auto-generated method stub
          return 0;
      }

      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
          // TODO Auto-generated method stub
          View grid;
          LayoutInflater inflater = (LayoutInflater) mContext
              .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

          if (convertView == null) {

              grid = new View(mContext);
              grid = inflater.inflate(R.layout.programlist, null);
              TextView textView = (TextView) grid.findViewById(R.id.textView1);
              TextView imageView = (TextView) grid.findViewById(R.id.imageView1);
              textView.setText(web.get(position));
              imageView.setBackgroundResource(Imageid.get(position));
          } else {
              grid = (View) convertView;
          }

          return grid;
      }
}