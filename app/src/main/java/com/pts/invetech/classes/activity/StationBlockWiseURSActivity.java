package com.pts.invetech.classes.activity;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.INetworkCallback;
import com.pts.handler.NetworkHandlerNewModel;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseActivity;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.customlist.StationWiseBlockWiseCustomList;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Utility;
import com.pts.model.stationmain.stationwise.blockwise.BlockDatum;
import com.pts.model.stationmain.stationwise.blockwise.StationBlockWiseData;

import org.json.JSONObject;

import java.util.List;

public class StationBlockWiseURSActivity extends AppBaseActivity {

    private SQLiteDatabase db;
    private Dialog prgDialog;
    private String device_id,domain;
    private ProgressBar pbprogressBar;
    private LinearLayout openLayout;
    private String region_Send, station_name_Send, state_Send, revision_Send, typeSend;
    private TextView tvgenertorname, tvtotal_amount;
    private StationWiseBlockWiseCustomList adapter;
    private ListView lv_more;
    private INetworkCallback callback = new INetworkCallback() {

        @Override
        public void onUpdateResult(Object obj, int id) {
            if (obj == null) {
                AppLogger.showToastShort(getApplicationContext(), "Some Error Ocurred");
                return;
            }

            StationBlockWiseData stationBlockWiseData = (StationBlockWiseData) obj;
            com.pts.model.stationmain.stationwise.blockwise.Value value = stationBlockWiseData.getValue();
            if (value.getBlockData().size() == 0) {
                AppLogger.showToastShort(getApplicationContext(), "No data found");
                return;
            }
            tvtotal_amount.setText(Integer.toString(value.getSum()));
            List<BlockDatum> blockData = value.getBlockData();
            adapter = new StationWiseBlockWiseCustomList(
                    StationBlockWiseURSActivity.this, blockData);
            lv_more.setAdapter(adapter);

            Utility.setListViewHeightBasedOnChildren(lv_more);


        }

    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_blockwise);
        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);


        openLayout = (LinearLayout) findViewById(R.id.openLayout);
        tvgenertorname = (TextView) findViewById(R.id.tvgenertorname);
        tvtotal_amount = (TextView) findViewById(R.id.tvtotal_amount);
        pbprogressBar = (ProgressBar) findViewById(R.id.pbprogressBar);
        lv_more = (ListView) findViewById(R.id.lv_more);

        Intent in = getIntent();
        typeSend = in.getStringExtra("type");
        region_Send = in.getStringExtra("region");
        station_name_Send = in.getStringExtra("station");
        state_Send = in.getStringExtra("beneficiary");
        revision_Send = in.getStringExtra("revision_Send");

        tvgenertorname.setText(state_Send + " - " + station_name_Send);


        ConnectionDetector cd = new ConnectionDetector(StationBlockWiseURSActivity.this);
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(StationBlockWiseURSActivity.this, "Please Check your network.",
                    Toast.LENGTH_LONG).show();
        } else {
            setStationBlockWise();
//            new HttpAsyncTaskstationwisedata().execute("http://mittalpower.com/mobile/pxs_app/service/newurs/stationwisedata.php");
        }
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setStationBlockWise() {
        /*
         *
         * {"device_id":867935024394966,
         * "station":"DADRT2","beneficiary":"RAJASTHAN","region":"NRLDC","revision":"47"}
         *
         *
         * */
        JSONObject param = new JSONObject();
        try {
            param.put("device_id", device_id);
            param.put("region", region_Send);
            param.put("station", station_name_Send);
            param.put("beneficiary", state_Send);
            param.put("revision", revision_Send);
            param.put("type", typeSend);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String[] str = new String[2];

        str[0] = domain + "/services/mobile_service/new_urs/blockwisedata.php";
        str[1] = param.toString();

        AppLogger.showMsg("param", param.toString());

        new NetworkHandlerNewModel(this, callback, StationBlockWiseData.class, 1).execute(str);
    }

}
