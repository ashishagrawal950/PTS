
package com.pts.model.ursdashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NRLDC {

    @SerializedName("reshedule_urs_remaining")
    @Expose
    private Integer resheduleUrsRemaining;

    public Integer getResheduleUrsRemaining() {
        return resheduleUrsRemaining;
    }

    public void setResheduleUrsRemaining(Integer resheduleUrsRemaining) {
        this.resheduleUrsRemaining = resheduleUrsRemaining;
    }

}
