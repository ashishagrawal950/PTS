package com.pts.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Ashish on 27-07-2017.
 */

public class PowerDemandOuter  implements Parcelable {

    private String year;
    private ArrayList<PowerDemandComparision> pdCompareList=new ArrayList<>();

    public PowerDemandOuter(){}

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public ArrayList<PowerDemandComparision> getPdCompareList() {
        return pdCompareList;
    }

    public void setPdCompareList(ArrayList<PowerDemandComparision> pdCompareList) {
        this.pdCompareList = pdCompareList;
    }

    public void addPowerDemand(PowerDemandComparision pd){

        if(pdCompareList==null){
            pdCompareList=new ArrayList<>();
        }

        pdCompareList.add(pd);

    }

    protected PowerDemandOuter(Parcel in) {
        year = in.readString();
        if (in.readByte() == 0x01) {
            pdCompareList = new ArrayList<PowerDemandComparision>();
            in.readList(pdCompareList, PowerDemandComparision.class.getClassLoader());
        } else {
            pdCompareList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(year);
        if (pdCompareList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(pdCompareList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PowerDemandOuter> CREATOR = new Parcelable.Creator<PowerDemandOuter>() {
        @Override
        public PowerDemandOuter createFromParcel(Parcel in) {
            return new PowerDemandOuter(in);
        }

        @Override
        public PowerDemandOuter[] newArray(int size) {
            return new PowerDemandOuter[size];
        }
    };
}

