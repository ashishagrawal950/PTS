package com.pts.invetech.stateload;

import com.pts.invetech.dashboardupdate.model.dashboard.RegionLdc;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by vaibhav on 25/1/17.
 */

public class StateLoadSorter {


    public static void sortState(List<RegionLdc> ldcList, boolean isAssending){

        if(isAssending){
            Collections.sort(ldcList,new Comparator<RegionLdc>() {
                @Override
                public int compare(RegionLdc d1, RegionLdc d2) {
                    return d1.getState().compareToIgnoreCase(d2.getState());
                }
            });
        }else{
            Collections.sort(ldcList,new Comparator<RegionLdc>() {
                @Override
                public int compare(RegionLdc d1, RegionLdc d2) {
                    return d2.getState().compareToIgnoreCase(d1.getState());
                }
            });
        }
    }

    /*
        Sort Generation in ORDER
     */

    public static void sortGeneration(List<RegionLdc> ldcList,boolean isAssending){

        if(isAssending){
            Collections.sort(ldcList, new Comparator<RegionLdc>() {
                @Override
                public int compare(RegionLdc regionLdc, RegionLdc t1) {
                    float change1 = Float.parseFloat(regionLdc.getGeneration());
                    float change2 = Float.parseFloat(t1.getGeneration());
                    if (change1 < change2) return -1;
                    if (change1 > change2) return 1;
                    return 0;
                }
            });
        }else {
            Collections.sort(ldcList, new Comparator<RegionLdc>() {
                @Override
                public int compare(RegionLdc regionLdc, RegionLdc t1) {
                    float change1 = Float.parseFloat(regionLdc.getGeneration());
                    float change2 = Float.parseFloat(t1.getGeneration());
                    if (change1 < change2) return 1;
                    if (change1 > change2) return -1;
                    return 0;
                }
            });
        }
    }


    /*
        Sort Demand in ORDER
     */
    public static void sortDemand(List<RegionLdc> ldcList,boolean isAssending){

        if(isAssending){
            Collections.sort(ldcList, new Comparator<RegionLdc>() {
                @Override
                public int compare(RegionLdc regionLdc, RegionLdc t1) {
                    float change1 = Float.parseFloat(regionLdc.getDemand());
                    float change2 = Float.parseFloat(t1.getDemand());
                    if (change1 < change2) return -1;
                    if (change1 > change2) return 1;
                    return 0;
                }
            });
        }else {
            Collections.sort(ldcList, new Comparator<RegionLdc>() {
                @Override
                public int compare(RegionLdc regionLdc, RegionLdc t1) {
                    float change1 = Float.parseFloat(regionLdc.getDemand());
                    float change2 = Float.parseFloat(t1.getDemand());
                    if (change1 < change2) return 1;
                    if (change1 > change2) return -1;
                    return 0;
                }
            });
        }
    }

    /*
        Sort Scheduling Drawl
     */
    public static void sortSchDrawl(List<RegionLdc> ldcList,boolean isAssending){

        if(isAssending){
            Collections.sort(ldcList, new Comparator<RegionLdc>() {
                @Override
                public int compare(RegionLdc regionLdc, RegionLdc t1) {
                    float change1 = Float.parseFloat(regionLdc.getSchDrawal());
                    float change2 = Float.parseFloat(t1.getSchDrawal());
                    if (change1 < change2) return -1;
                    if (change1 > change2) return 1;
                    return 0;
                }
            });
        }else {
            Collections.sort(ldcList, new Comparator<RegionLdc>() {
                @Override
                public int compare(RegionLdc regionLdc, RegionLdc t1) {
                    float change1 = Float.parseFloat(regionLdc.getSchDrawal());
                    float change2 = Float.parseFloat(t1.getSchDrawal());
                    if (change1 < change2) return 1;
                    if (change1 > change2) return -1;
                    return 0;
                }
            });
        }
    }

    /*
        Sort Actual Drawl
     */
    public static void sortActualDrawl(List<RegionLdc> ldcList,boolean isAssending){

        if(isAssending){
            Collections.sort(ldcList, new Comparator<RegionLdc>() {
                @Override
                public int compare(RegionLdc regionLdc, RegionLdc t1) {
                    float change1 = Float.parseFloat(regionLdc.getActDrawal());
                    float change2 = Float.parseFloat(t1.getActDrawal());
                    if (change1 < change2) return -1;
                    if (change1 > change2) return 1;
                    return 0;
                }
            });
        }else {
            Collections.sort(ldcList, new Comparator<RegionLdc>() {
                @Override
                public int compare(RegionLdc regionLdc, RegionLdc t1) {
                    float change1 = Float.parseFloat(regionLdc.getActDrawal());
                    float change2 = Float.parseFloat(t1.getActDrawal());
                    if (change1 < change2) return 1;
                    if (change1 > change2) return -1;
                    return 0;
                }
            });
        }
    }

    /*
        Sort Deviation
     */
    public static void sortDeviation(List<RegionLdc> ldcList,boolean isAssending){

        if(isAssending){
            Collections.sort(ldcList, new Comparator<RegionLdc>() {
                @Override
                public int compare(RegionLdc regionLdc, RegionLdc t1) {
                    float change1 = Float.parseFloat(regionLdc.getDeviation());
                    float change2 = Float.parseFloat(t1.getDeviation());
                    if (change1 < change2) return -1;
                    if (change1 > change2) return 1;
                    return 0;
                }
            });
        }else {
            Collections.sort(ldcList, new Comparator<RegionLdc>() {
                @Override
                public int compare(RegionLdc regionLdc, RegionLdc t1) {
                    float change1 = Float.parseFloat(regionLdc.getDeviation());
                    float change2 = Float.parseFloat(t1.getDeviation());
                    if (change1 < change2) return 1;
                    if (change1 > change2) return -1;
                    return 0;
                }
            });
        }
    }





}
