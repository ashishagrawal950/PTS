
package com.pts.model.testmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Delhi {

    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("demand")
    @Expose
    private String demand;
    @SerializedName("peak")
    @Expose
    private String peak;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDemand() {
        return demand;
    }

    public void setDemand(String demand) {
        this.demand = demand;
    }

    public String getPeak() {
        return peak;
    }

    public void setPeak(String peak) {
        this.peak = peak;
    }

}
