package com.pts.invetech.dashboardupdate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.dashboardupdate.model.news.SubNews;

import java.util.List;

/**
 * Created by vaibhav on 10/1/17.
 */

public class SubNewsAdapter extends BaseAdapter {

    private List<SubNews> list;
    private Context mContext;

    public SubNewsAdapter(Context _con,List<SubNews> _news){
        mContext=_con;
        list=_news;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public SubNews getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view==null){
            LayoutInflater inf=LayoutInflater.from(mContext);
            view= inf.inflate(R.layout.new_listitem_newsfeed,null);
        }

        SubNews data=getItem(i);

        TextView header= (TextView) view.findViewById(R.id.nheader);
        header.setText(data.getTitle());

        TextView source= (TextView) view.findViewById(R.id.nsource);
        source.setText(data.getSource());

        TextView type= (TextView) view.findViewById(R.id.ntype);
        type.setText(data.getType());

        TextView time= (TextView) view.findViewById(R.id.tvtime);
        time.setText(data.getTime());

        TextView date= (TextView) view.findViewById(R.id.tvdate);
        date.setText(data.getPubdate());

        return view;
    }
}
