package com.pts.invetech.classes.activity;


import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pts.invetech.APICalling.AppBaseActivity;
import com.pts.invetech.utils.AppLogger;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;


public class NetworkManagerActivityGson extends AsyncTask<Void, Void, Object> {


    private String currentUrl = null;
    private JSONObject jsonParam = null;
    private AppBaseActivity mActivity = null;
    private ProgressDialog progress = null;
    private Class currentClass = null;

    public NetworkManagerActivityGson(AppBaseActivity splsh, JSONObject obj, String url, Class currentClass) {
        currentUrl = url;
        jsonParam = obj;
        mActivity = splsh;

        this.currentClass = currentClass;
    }

    @Override
    protected void onPreExecute() {
        if (progress == null) {
            progress = new ProgressDialog(mActivity);
            progress.setMessage("Please Wait");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setCancelable(true);
            progress.setCanceledOnTouchOutside(false);
            progress.show();
        } else if (!progress.isShowing()) {
            progress.show();
        }
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Void... params) {
        Object reader = networkCall();
        return reader;
    }

    @Override
    protected void onPostExecute(Object res) {
        mActivity.updateObjectResult(res);
        if (progress != null) {
            progress.dismiss();
        }
    }

    private Object networkCall() {
        URL url = null;
        HttpURLConnection connection = null;
        Object mObject = null;
        try {
            url = new URL(currentUrl);

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);

            DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
            if (jsonParam != null) {
                dStream.writeBytes("data=" + jsonParam.toString());
            }
            dStream.flush();
            dStream.close();

            int responseCode = connection.getResponseCode();

            AppLogger.showMsg("response code", "" + responseCode);

            InputStream content = connection.getInputStream();
            Reader reader = new InputStreamReader(content);

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();

            mObject = gson.fromJson(reader, currentClass);

            content.close();

        } catch (Exception e) {
            AppLogger.showMsg("Error in async", e.getMessage());
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return mObject;
    }
}