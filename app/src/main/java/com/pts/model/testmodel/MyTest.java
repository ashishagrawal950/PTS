
package com.pts.model.testmodel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyTest {

    @SerializedName("UP")
    @Expose
    private List<UP> uP = null;
    @SerializedName("delhi")
    @Expose
    private List<Delhi> delhi = null;
    @SerializedName("mumbai")
    @Expose
    private List<Mumbai> mumbai = null;

    public List<UP> getUP() {
        return uP;
    }

    public void setUP(List<UP> uP) {
        this.uP = uP;
    }

    public List<Delhi> getDelhi() {
        return delhi;
    }

    public void setDelhi(List<Delhi> delhi) {
        this.delhi = delhi;
    }

    public List<Mumbai> getMumbai() {
        return mumbai;
    }

    public void setMumbai(List<Mumbai> mumbai) {
        this.mumbai = mumbai;
    }

}
