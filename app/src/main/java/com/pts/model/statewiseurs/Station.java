
package com.pts.model.statewiseurs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Station {

    @SerializedName("gas")
    @Expose
    private List<Coal> gas = null;
    @SerializedName("coal")
    @Expose
    private List<Coal> coal = null;
    @SerializedName("other")
    @Expose
    private List<Coal> other = null;

    public List<Coal> getGas() {
        if (gas == null)
            return new ArrayList<>();
        return gas;
    }

    public void setGas(List<Coal> gas) {
        this.gas = gas;
    }

    public List<Coal> getCoal() {
        if (coal == null)
            return new ArrayList<>();
        return coal;
    }

    public void setCoal(List<Coal> coal) {
        this.coal = coal;
    }

    public List<Coal> getOther() {
        if (other == null)
            return new ArrayList<>();
        return other;
    }

    public void setOther(List<Coal> other) {
        this.other = other;
    }
}
