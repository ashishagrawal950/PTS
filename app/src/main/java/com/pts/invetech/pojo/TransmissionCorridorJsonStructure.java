package com.pts.invetech.pojo;

import java.util.ArrayList;

public class TransmissionCorridorJsonStructure {
	 private ArrayList<ArrayList<String>>  importArray;
	    private ArrayList<ArrayList<String>>  exportArray;
	    private ArrayList<ArrayList<String>>  corridorArray;

	    public TransmissionCorridorJsonStructure(){
	        importArray=new ArrayList<>();
	        exportArray=new ArrayList<>();
	        corridorArray=new ArrayList<>();
	    }

	    public void addImport(ArrayList<String> imp){
	        importArray.add(imp);
	    }

	    public ArrayList<String> getImportByValue(int index){
	        return importArray.get(index);
	    }

	    public ArrayList<ArrayList<String>> getAllImport(){
	        return importArray;
	    }

	    public void addExport(ArrayList<String> exp){
	        exportArray.add(exp);
	    }

	    public ArrayList<String> getExportByValue(int index){
	        return exportArray.get(index);
	    }

	    public ArrayList<ArrayList<String>> getAllExport(){
	        return exportArray;
	    }

	    public void addCorridor(ArrayList<String> cor){
	        corridorArray.add(cor);
	    }

	    public ArrayList<String> getCorridorByValue(int index){
	        return corridorArray.get(index);
	    }

	    public ArrayList<ArrayList<String>> getAllCorridor(){
	        return corridorArray;
	    }
	}