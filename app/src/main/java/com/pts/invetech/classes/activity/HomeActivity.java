package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.AlertDialogManager;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.apiresponse.HomeAPIResponse;
import com.pts.invetech.pojo.Login;
import com.pts.invetech.pojo.ObligationDetails;
import com.pts.invetech.utils.JSONUtils;

import org.apache.commons.lang.ArrayUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class HomeActivity extends Activity {

    public static String name;
    public static String email;
    static ArrayList<String> date_namearray = new ArrayList<String>();
    static ArrayList<Float> iex_namearray = new ArrayList<Float>();
    static ArrayList<Float> pxil_namearray = new ArrayList<Float>();
    static ArrayList<String> block_noarray = new ArrayList<String>();
    static ArrayList<String> new_qtmearray = new ArrayList<String>();
    static ArrayList<String> new_qtmarray = new ArrayList<String>();
    public boolean iexisTrue = true;
    public boolean pxilisTrue = true;
    private ViewPager mbanner;
    private Point p;
    private LinearLayout mopenLayout, chart, lliex, llpxil, lltransaction, mopenPopup;
    private ImageView mImageVoice;
    private Context context;
    private Animation animbounce;
    private Dialog prgDialog;
    private String iexNocDetailBuy, portfolioIex, portfolioPxil, iexNocDetailSell, pxilNocDetailBuy, pxilNocDetailSell, iexRegistrationDetail,
            pxilRegistrationDetail, clientBalance, lastpaid, lastpaiddate, lastpaidmoderef, lastpaidmode, recSolar,
            recNonSolar;
    private TextView tvcompanyName;
    private TextView iexPortfoliocodedate, iexnocbuydate, iexnocselldate, iexregistrationvaliddate;
    private TextView pxilportfoliocodedate, pxilnocbuydate, pxilnocselldate, pxilregistrationvaliddate;
    private TextView transactionbalanceamount, transactionlastamountpaid, transactionondate, transactionpaymentmode;
    private String date, IEX, PXIL;
    private HashMap<Integer, ObligationDetails> myHashmap;
    private ObligationDetails mypojo;
    private int i;
    private SQLiteDatabase db;
    private String dbacess, companyName;
    // Alert dialog manager
    private AlertDialogManager alert = new AlertDialogManager();
    // Connection detector
    private ConnectionDetector cd;
    private TextView lblMessage;
    private String newMessage, domain;
    private LinearLayout llgraphiex, llgraphpxil;
    private TextView tviextext, tviexline, tvpxiltext, tvpxilline;
    private LineChart lineChart, navchartfornotification;
    private TextView viewgraph;
    private String dateforno, app_noforno, lastRevforno, currentRevforno, current_qtmforno, last_qtmforno;

    private String bb = "{\n" +
            "  \"date\": \"18-04-2017\",\n" +
            "  \"app_no\": \"232\",\n" +
            "  \"lastRev\": \"2\",\n" +
            "  \"currentRev\":\"3\",\n" +
            "  \"current_qtm\": \"232.23\",\n" +
            "  \"last_qtm\": \"222.32\",\n" +
            "  \"details\": {\n" +
            "    \"0\": {\n" +
            "      \"block_no\": \"1\",\n" +
            "      \"last_qtm\": \"12\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"1\": {\n" +
            "      \"block_no\": \"2\",\n" +
            "      \"last_qtm\": \"13\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"2\": {\n" +
            "      \"block_no\": \"3\",\n" +
            "      \"last_qtm\": \"11\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"3\": {\n" +
            "      \"block_no\": \"4\",\n" +
            "      \"last_qtm\": \"3\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"4\": {\n" +
            "      \"block_no\": \"5\",\n" +
            "      \"last_qtm\": \"4\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"5\": {\n" +
            "      \"block_no\": \"13\",\n" +
            "      \"last_qtm\": \"6\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"6\": {\n" +
            "      \"block_no\": \"7\",\n" +
            "      \"last_qtm\": \"6\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"7\": {\n" +
            "      \"block_no\": \"8\",\n" +
            "      \"last_qtm\": \"7\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"8\": {\n" +
            "      \"block_no\": \"9\",\n" +
            "      \"last_qtm\": \"8\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"9\": {\n" +
            "      \"block_no\": \"10\",\n" +
            "      \"last_qtm\": \"2\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"10\": {\n" +
            "      \"block_no\": \"11\",\n" +
            "      \"last_qtm\": \"11\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"11\": {\n" +
            "      \"block_no\": \"11\",\n" +
            "      \"last_qtm\": \"4\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"13\": {\n" +
            "      \"block_no\": \"14\",\n" +
            "      \"last_qtm\": \"9\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"14\": {\n" +
            "      \"block_no\": \"15\",\n" +
            "      \"last_qtm\": \"9\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"15\": {\n" +
            "      \"block_no\": \"16\",\n" +
            "      \"last_qtm\": \"9\",\n" +
            "      \"new_qtm\": \"10\"\n" +
            "    },\n" +
            "    \"16\": {\n" +
            "      \"block_no\": \"17\",\n" +
            "      \"last_qtm\": \"10\",\n" +
            "      \"new_qtm\": \"9\"\n" +
            "    },\n" +
            "    \"17\": {\n" +
            "      \"block_no\": \"18\",\n" +
            "      \"last_qtm\": \"10\",\n" +
            "      \"new_qtm\": \"8\"\n" +
            "    },\n" +
            "    \"18\": {\n" +
            "      \"block_no\": \"19\",\n" +
            "      \"last_qtm\": \"10\",\n" +
            "      \"new_qtm\": \"7\"\n" +
            "    },\n" +
            "    \"19\": {\n" +
            "      \"block_no\": \"20\",\n" +
            "      \"last_qtm\": \"10\",\n" +
            "      \"new_qtm\": \"6\"\n" +
            "    },\n" +
            "    \"20\": {\n" +
            "      \"block_no\": \"10\",\n" +
            "      \"last_qtm\": \"10\",\n" +
            "      \"new_qtm\": \"5\"\n" +
            "    },\n" +
            "    \"21\": {\n" +
            "      \"block_no\": \"22\",\n" +
            "      \"last_qtm\": \"10\",\n" +
            "      \"new_qtm\": \"4\"\n" +
            "    }\n" +
            "  }\n" +
            "}\n" +
            "\n";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_home);


        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        Cursor c = db.rawQuery("SELECT * FROM baby", null);
        if (c.getCount() == 0) {
            showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("access_key: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
        }

        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");

        //	mopenPopup = (LinearLayout) findViewById(R.id.openPopup);
			/*tvnotificationicon = (TextView) findViewById(R.id.tvnotificationicon);
			 db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
			 Cursor cNot = db.rawQuery("SELECT * FROM pushnotificationUnRead", null);
			    //	goMessage.setText("All Notification:" + c.getCount());
					if(cNot.getCount()==0)
					{
						//mopenPopup.setVisibility(View.GONE);
					}
					else{
						String count = Integer.toString(cNot.getCount());
						//mopenPopup.setVisibility(View.VISIBLE);
						tvnotificationicon.setVisibility(View.VISIBLE);
						tvnotificationicon.setText(count);
					}*/


        Intent in = getIntent();
        companyName = in.getStringExtra("companyName");

        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);


        myHashmap = new HashMap<Integer, ObligationDetails>();


        tvcompanyName = (TextView) findViewById(R.id.tvcompanyName);
        tvcompanyName.setText(companyName);
        //tvcompanyName.setText("Demo Client Pvt. Ltd.");

        chart = (LinearLayout) findViewById(R.id.chart);
        lineChart = (LineChart) findViewById(R.id.navchart);
        navchartfornotification = (LineChart) findViewById(R.id.navchartfornotification);


        animbounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        chart.startAnimation(animbounce);


        iexPortfoliocodedate = (TextView) findViewById(R.id.iexPortfoliocodedate);
        iexnocbuydate = (TextView) findViewById(R.id.iexnocbuydate);
        iexnocselldate = (TextView) findViewById(R.id.iexnocselldate);
        iexregistrationvaliddate = (TextView) findViewById(R.id.iexregistrationvaliddate);

        pxilportfoliocodedate = (TextView) findViewById(R.id.pxilportfoliocodedate);
        pxilnocbuydate = (TextView) findViewById(R.id.pxilnocbuydate);
        pxilnocselldate = (TextView) findViewById(R.id.pxilnocselldate);
        pxilregistrationvaliddate = (TextView) findViewById(R.id.pxilregistrationvaliddate);

        transactionbalanceamount = (TextView) findViewById(R.id.transactionbalanceamount);
        transactionlastamountpaid = (TextView) findViewById(R.id.transactionlastamountpaid);
        transactionondate = (TextView) findViewById(R.id.transactionondate);
        transactionpaymentmode = (TextView) findViewById(R.id.transactionpaymentmode);

        TextView tvtransaction = (TextView) findViewById(R.id.tvtransaction);
        TextView tviex = (TextView) findViewById(R.id.tviex);
        TextView tvpxil = (TextView) findViewById(R.id.tvpxil);

        llgraphiex = (LinearLayout) findViewById(R.id.llgraphiex);
        tviextext = (TextView) findViewById(R.id.tviextext);
        tviexline = (TextView) findViewById(R.id.tviexline);

        llgraphpxil = (LinearLayout) findViewById(R.id.llgraphpxil);
        tvpxiltext = (TextView) findViewById(R.id.tvpxiltext);
        tvpxilline = (TextView) findViewById(R.id.tvpxilline);

        viewgraph = (TextView) findViewById(R.id.viewgraph);

        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        mopenLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent in = new Intent(HomeActivity.this, MainActivityAfterLogin.class);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        tvtransaction.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent in = new Intent(HomeActivity.this, ReportActivity.class);
                in.putExtra("from", "HomeActivity");
                in.putExtra("companyName", companyName);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        tviex.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent in = new Intent(HomeActivity.this, NewBidActivity.class);
                in.putExtra("from", "HomeActivity");
                in.putExtra("name", "IEX");
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        tvpxil.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent in = new Intent(HomeActivity.this, NewBidActivity.class);
                in.putExtra("from", "HomeActivity");
                in.putExtra("name", "PXIL");
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });
		
		/*mopenPopup.setOnClickListener(new OnClickListener() {
			   @Override
			   public void onClick(View v) {
			    Intent in = new Intent();
			    startActivity(in);
			   }
			  });*/

        llgraphiex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Toast.makeText(MarketPriceActivity.this, "Ashish", Toast.LENGTH_LONG).show();
                if (iexisTrue == true) {
                    iexisTrue = false;
                    String f = "false";
                    tviextext.setTextColor(Color.GRAY);
                    tviexline.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(HomeActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    iexisTrue = true;
                    String t = "true";
                    tviextext.setTextColor(Color.CYAN);
                    tviexline.setBackgroundColor(Color.CYAN);
                    //Toast.makeText(HomeActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartNav();

            }
        });

        llgraphpxil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Toast.makeText(MarketPriceActivity.this, "Ashish", Toast.LENGTH_LONG).show();
                if (pxilisTrue == true) {
                    pxilisTrue = false;
                    String f = "false";
                    tvpxiltext.setTextColor(Color.GRAY);
                    tvpxilline.setBackgroundColor(Color.GRAY);
                    //Toast.makeText(HomeActivity.this, f, Toast.LENGTH_LONG).show();
                } else {
                    pxilisTrue = true;
                    String t = "true";
                    tvpxiltext.setTextColor(Color.GREEN);
                    tvpxilline.setBackgroundColor(Color.GREEN);
                    //Toast.makeText(HomeActivity.this, t, Toast.LENGTH_LONG).show();
                }
                drawChartNav();

            }
        });

        viewgraph.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (navchartfornotification.saveToGallery("PTS", 50)) {
                    Toast.makeText(getApplicationContext(), "Saving SUCCESSFUL!",
                            Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getApplicationContext(), "Saving FAILED!", Toast.LENGTH_SHORT)
                            .show();

                NotificationCompat.Builder nb = new NotificationCompat.Builder(HomeActivity.this);
                nb.setSmallIcon(R.drawable.logopts);
                nb.setContentTitle("PTS");
                nb.setContentText("Set Content text");
                nb.setTicker("Set Ticker text");
                //nb.setLargeIcon(result).build();

                File filea = new File(Environment.getExternalStorageDirectory().getPath() + "/DCIM/" + "PTS");
                if (filea.exists()) {
                    Bitmap bitmap_image = BitmapFactory.decodeFile(filea.getAbsolutePath());
                    //Bitmap bitmap_image = BitmapFactory.decodeResource(HomeActivity.this.getResources(), bitmap);
                    NotificationCompat.BigPictureStyle s = new NotificationCompat.BigPictureStyle().bigPicture(bitmap_image);
                    s.setSummaryText("Summary text appears on expanding the notification");
                    nb.setStyle(s);
                } else {

                }

                Intent resultIntent = new Intent(HomeActivity.this, MainActivityAfterLogin.class);
                TaskStackBuilder TSB = TaskStackBuilder.create(HomeActivity.this);
                TSB.addParentStack(NewFeedMainActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                TSB.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent =
                        TSB.getPendingIntent(
                                0,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                nb.setContentIntent(resultPendingIntent);
                nb.setAutoCancel(true);
                NotificationManager mNotificationManager =
                        (NotificationManager) HomeActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
                // mId allows you to update the notification later on.
                mNotificationManager.notify(11221, nb.build());
            }
        });


        cd = new ConnectionDetector(HomeActivity.this);
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            new HttpAsyncTask().execute(domain + "/mobile/pxs_app/html/fulldetail.php");
        }

		/*try {
			JSONObject mainObject = new JSONObject(bb);
			 dateforno = mainObject.getString("date");
			 app_noforno = mainObject.getString("app_no");
			 lastRevforno = mainObject.getString("lastRev");
			 currentRevforno = mainObject.getString("currentRev");
			 current_qtmforno = mainObject.getString("current_qtm");
			 last_qtmforno = mainObject.getString("last_qtm");
			JSONObject details = mainObject.getJSONObject("details");
			block_noarray.clear();
			new_qtmearray.clear();
			pxil_namearray.clear();
			for (i = 0; i < details.length(); i++) {
				String strI = String.valueOf(i);
				JSONObject obligationdetail = details.getJSONObject(strI);
				String block_no = obligationdetail.getString("block_no");
				String last_qtmfo = obligationdetail.getString("last_qtm");
				String new_qtm = obligationdetail.getString("new_qtm");
				block_noarray.add(block_no);
				new_qtmearray.add(last_qtmfo);
				new_qtmarray.add(new_qtm);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}*/

        //drawChartNavfornotification();

    }

    private void drawChartNav() {

        ArrayList<String> date = new ArrayList<String>();
        ArrayList<Float> iEX = new ArrayList<Float>();
        ArrayList<Float> pXIL = new ArrayList<Float>();
        Collection<ObligationDetails> c = myHashmap.values();
        Iterator<ObligationDetails> itr = c.iterator();
        while (itr.hasNext()) {
            ObligationDetails value = (ObligationDetails) itr.next();
            date.add(value.getDATE());
            iEX.add(value.getIEX());
            pXIL.add(value.getPXIL());
        }
        if (date.size() > 0 && iEX.size() > 0 && pXIL.size() > 0) {
            String[] mMonth = date.toArray(new String[date.size()]);
            Log.d("Ashish", mMonth.toString());

            Float[] wrapperArr = iEX.toArray(new Float[iEX.size()]);
            float[] income = ArrayUtils.toPrimitive(wrapperArr);
            float iEXlargest = Integer.MIN_VALUE;
            for (int i = 0; i < income.length; i++) {
                if (income[i] > iEXlargest) {
                    iEXlargest = income[i];
                }
            }

            Log.d("Ashish", income.toString());

            Float[] wrapperArr1 = pXIL.toArray(new Float[pXIL.size()]);
            float[] expense = ArrayUtils.toPrimitive(wrapperArr1);
            float pXILlargest = Integer.MIN_VALUE;
            for (int i = 0; i < expense.length; i++) {
                if (expense[i] > pXILlargest) {
                    pXILlargest = expense[i];
                }
            }
            // creating list of entry
            ArrayList<Entry> entriesiex = new ArrayList<>();
            entriesiex.add(new Entry(iex_namearray.get(0), 0));
            entriesiex.add(new Entry(iex_namearray.get(1), 1));
            entriesiex.add(new Entry(iex_namearray.get(2), 2));
            entriesiex.add(new Entry(iex_namearray.get(3), 3));
            entriesiex.add(new Entry(iex_namearray.get(4), 4));
            entriesiex.add(new Entry(iex_namearray.get(5), 5));
            entriesiex.add(new Entry(iex_namearray.get(6), 6));

            LineDataSet datasetiex = new LineDataSet(entriesiex, "");
            datasetiex.setCircleColor(Color.CYAN);
            datasetiex.setColor(Color.CYAN);
            datasetiex.setValueTextColor(Color.CYAN);
            //datasetiex.setCircleRadius(2f);


            // creating list of entry
            ArrayList<Entry> entriespxil = new ArrayList<>();
            entriespxil.add(new Entry(pxil_namearray.get(0), 0));
            entriespxil.add(new Entry(pxil_namearray.get(1), 1));
            entriespxil.add(new Entry(pxil_namearray.get(2), 2));
            entriespxil.add(new Entry(pxil_namearray.get(3), 3));
            entriespxil.add(new Entry(pxil_namearray.get(4), 4));
            entriespxil.add(new Entry(pxil_namearray.get(5), 5));
            entriespxil.add(new Entry(pxil_namearray.get(6), 6));

            LineDataSet datasetpxil = new LineDataSet(entriespxil, "");

            datasetpxil.setCircleColor(Color.GREEN);
            datasetpxil.setColor(Color.GREEN);
            datasetpxil.setValueTextColor(Color.GREEN);

            if (iexisTrue == false && pxilisTrue == false) {
                //Toast.makeText(HomeActivity.this, "Graph Reset.",Toast.LENGTH_LONG).show();
                tviextext.setTextColor(Color.CYAN);
                tviexline.setBackgroundColor(Color.CYAN);
                tvpxiltext.setTextColor(Color.GREEN);
                tvpxilline.setBackgroundColor(Color.GREEN);
                ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
                dataSets.add(datasetiex);
                dataSets.add(datasetpxil); // add the datasets
                LineData data = new LineData(date_namearray, dataSets);
                lineChart.setData(data); // set the data and list of lables into
                lineChart.animateY(3000);
                iexisTrue = true;
                pxilisTrue = true;
            } else if (iexisTrue == false && pxilisTrue == true) {
                LineData datapxil = new LineData(date_namearray, datasetpxil);
                lineChart.setData(datapxil); // set the data and list of lables
                // into chart
                lineChart.animateY(3000);
            } else if (iexisTrue == true && pxilisTrue == false) {
                LineData dataiex = new LineData(date_namearray, datasetiex);
                lineChart.setData(dataiex); // set the data and list of lables
                // into chart
                lineChart.animateY(3000);
            } else if (iexisTrue == true && pxilisTrue == true) {
                //Toast.makeText(HomeActivity.this, "Graph Reset.", Toast.LENGTH_LONG).show();
                tviextext.setTextColor(Color.CYAN);
                tviexline.setBackgroundColor(Color.CYAN);
                tvpxiltext.setTextColor(Color.GREEN);
                tvpxilline.setBackgroundColor(Color.GREEN);
                ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
                dataSets.add(datasetiex);
                dataSets.add(datasetpxil); // add the datasets

                LineData data = new LineData(date_namearray, dataSets);
                lineChart.setData(data); // set the data and list of lables into chart
                lineChart.animateY(3000);
            }
            lineChart.setDescription("Values in MW/h");  // set the description
            lineChart.setDescriptionColor(Color.WHITE);
            XAxis xl = lineChart.getXAxis();
            xl.setTextColor(Color.WHITE);

            YAxis leftAxis = lineChart.getAxisLeft();
            leftAxis.setTextColor(Color.WHITE);

            YAxis rightAxis = lineChart.getAxisRight();
            rightAxis.setTextColor(Color.WHITE);
            lineChart.setDrawGridBackground(false);


        }
    }

    private void drawChartNavfornotification() {


        ArrayList<Entry> entriescurrent = new ArrayList<>();
        for (int min = 0; min < new_qtmearray.size(); min++) {
            entriescurrent.add(new Entry(Float.parseFloat(new_qtmearray.get(min)), min));
        }
        LineDataSet datasetcurrent = new LineDataSet(entriescurrent, "");
        datasetcurrent.setCircleColor(Color.GREEN);
        datasetcurrent.setColor(Color.GREEN);
        datasetcurrent.setValueTextColor(Color.GREEN);
        datasetcurrent.setCircleSize(3f);


        // creating list of entry
        ArrayList<Entry> entriespreviours = new ArrayList<>();
        for (int min = 0; min < new_qtmarray.size(); min++) {
            entriespreviours.add(new Entry(Float.parseFloat(new_qtmarray.get(min)), min));
        }
        LineDataSet datasetprevoursday = new LineDataSet(entriespreviours, "");
        datasetprevoursday.setCircleColor(Color.BLUE);
        datasetprevoursday.setColor(Color.BLUE);
        datasetprevoursday.setValueTextColor(Color.BLUE);
        datasetprevoursday.setCircleSize(3f);

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(datasetcurrent);
        dataSets.add(datasetprevoursday);

        LineData data = new LineData(block_noarray, dataSets);
        navchartfornotification.setData(data); // set the data and list of lables into
        navchartfornotification.animateY(3000);

        navchartfornotification.setDescription("Values in Rupees");  // set the description
        XAxis xAxis = navchartfornotification.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(4f);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setDrawGridLines(true);
        xAxis.setDrawAxisLine(true);

        navchartfornotification.setDrawGridBackground(false);
    }

    public void showMessage(String title, String message) {
        Builder builder = new Builder(HomeActivity.this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(HomeActivity.this, MainActivityAfterLogin.class);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    /**
     * @author Ashish make HTTP Request to the server
     * @parm access_key
     */
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Login login = new Login();
            login.setAccess_key(dbacess);
            return HomeAPIResponse.POST(urls[0], login);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                //	Toast.makeText(getActivity(), "HomeResult" + result, Toast.LENGTH_LONG).show();
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    iexNocDetailBuy = mainObject.getString("iexNocDetailBuy");
                    portfolioIex = mainObject.getString("portfolioIex");
                    portfolioPxil = mainObject.getString("portfolioPxil");
                    if (portfolioPxil.length() == 0) {
                        portfolioPxil = "NA";
                    }
                    iexNocDetailSell = mainObject.getString("iexNocDetailSell");
                    pxilNocDetailBuy = mainObject.getString("pxilNocDetailBuy");
                    pxilNocDetailSell = mainObject.getString("pxilNocDetailSell");
                    iexRegistrationDetail = mainObject.getString("iexRegistrationDetail");
                    pxilRegistrationDetail = mainObject.getString("pxilRegistrationDetail");
                    clientBalance = mainObject.getString("clientBalance");
                    lastpaid = mainObject.getString("lastpaid");
                    lastpaiddate = mainObject.getString("lastpaiddate");
                    lastpaidmoderef = mainObject.getString("lastpaidmoderef");
                    lastpaidmode = mainObject.getString("lastpaidmode");
                    recSolar = mainObject.getString("recSolar");
                    recNonSolar = mainObject.getString("recNonSolar");
                    iexPortfoliocodedate.setText(portfolioIex);
                    iexnocbuydate.setText(iexNocDetailBuy);
                    iexnocselldate.setText(iexNocDetailSell);
                    iexregistrationvaliddate.setText(iexRegistrationDetail);
                    pxilportfoliocodedate.setText(portfolioPxil);
                    pxilnocbuydate.setText(pxilNocDetailBuy);
                    pxilnocselldate.setText(pxilNocDetailSell);
                    pxilregistrationvaliddate.setText(pxilRegistrationDetail);
                    transactionbalanceamount.setText(clientBalance);
                    transactionlastamountpaid.setText(lastpaid);
                    transactionondate.setText(lastpaiddate);
                    transactionpaymentmode.setText(lastpaidmode);

                    JSONArray seriesData = mainObject.getJSONArray("seriesData");
                    date_namearray.clear();
                    iex_namearray.clear();
                    pxil_namearray.clear();
                    for (i = 0; i < seriesData.length(); i++) {
                        JSONObject obligationdetail = seriesData.getJSONObject(i);
                        date = obligationdetail.getString("DATE");
                        IEX = obligationdetail.getString("IEX");
                        PXIL = obligationdetail.getString("PXIL");
                        float income = Float.parseFloat(IEX);
                        float expense = Float.parseFloat(PXIL);
                        date_namearray.add(date);
                        iex_namearray.add(income);
                        pxil_namearray.add(expense);
                        mypojo = new ObligationDetails(date, income, expense);
                        myHashmap.put(i, mypojo);
						/*Toast.makeText(getActivity(),"DATE = " + DATE + "IEX = " + IEX + "PXIL = " + PXIL,
								Toast.LENGTH_LONG).show();*/
                    }
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

            drawChartNav();
        }
    }

}
