package com.pts.invetech.customlist;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.classes.fragment.StatewiseRealtimeSchedulingFragment;
import com.pts.model.statewise.StateRealTimeValue;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Ashish on 25-07-2017.
 */

public class StateWiseCustomGrid extends BaseAdapter {


    private Context mContext;
    private ArrayList<StateRealTimeValue> stateRealTimeValues;
    private int[] myColors;
    private String message;
    private StatewiseRealtimeSchedulingFragment.IStateWiseDataPasser gridClick;


    public StateWiseCustomGrid(Context context,
                               ArrayList<StateRealTimeValue> stateRealTimeValues,
                               int[] myColors, String message,
                              StatewiseRealtimeSchedulingFragment.IStateWiseDataPasser _gridClick) {

        mContext = context;
        this.stateRealTimeValues = stateRealTimeValues;
        this.myColors = myColors;
        this.message = message;
        gridClick=_gridClick;
    }

    @Override
    public int getCount() {
        return stateRealTimeValues.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            grid = inflater.inflate(R.layout.recycleritem_statewise, null);
            grid.setOnClickListener(gridListner);
            TextView txt_lable = (TextView) grid.findViewById(R.id.txt_lable);
            TextView txt_sum = (TextView)grid.findViewById(R.id.txt_sum);
            TextView txt_avg = (TextView)grid.findViewById(R.id.txt_avg);

            txt_lable.setText(stateRealTimeValues.get(position).getName());
            txt_sum.setText(stateRealTimeValues.get(position).getSum() + " " + message );
            txt_avg.setText(stateRealTimeValues.get(position).getAvg() + " " );
            txt_lable.setBackgroundColor(myColors[position]);

        } else {
            grid = (View) convertView;
        }

        grid.setTag(position);

        return grid;
    }


    private View.OnClickListener gridListner=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pos= (int) v.getTag();
            gridClick.sendImportType(stateRealTimeValues.get(pos).getName(), stateRealTimeValues.get(pos).getRevision());
        }
    };

}