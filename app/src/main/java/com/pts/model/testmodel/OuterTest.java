package com.pts.model.testmodel;

import java.util.ArrayList;

/**
 * Created by Ashish on 28-07-2017.
 */

public class OuterTest {

    private String stateName;
    private ArrayList<InnerModel> innerModelList=new ArrayList<>();

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public ArrayList<InnerModel> getInnerModelList() {
        return innerModelList;
    }

    public void setInnerModelList(ArrayList<InnerModel> innerModelList) {
        this.innerModelList = innerModelList;
    }

    public void addInnerData(InnerModel _innerData){
        if(innerModelList==null){
            innerModelList=new ArrayList<>();
        }

        innerModelList.add(_innerData);
    }

    public boolean hasState(String state){
       return stateName.equalsIgnoreCase(state);
    }
}
