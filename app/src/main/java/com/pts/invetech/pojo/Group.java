package com.pts.invetech.pojo;

import java.util.ArrayList;

/**
 * Created by Ashish on 03-11-2016.
 */

public class Group {

    private String name;
    private ArrayList<Child> items;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Child> getItems() {
        return items;
    }

    public void setItems(ArrayList<Child> items) {
        this.items = items;
    }
}
