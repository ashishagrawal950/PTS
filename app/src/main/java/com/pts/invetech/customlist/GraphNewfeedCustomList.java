package com.pts.invetech.customlist;

import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

public class GraphNewfeedCustomList extends ArrayAdapter<String> {

	private  Activity context;
	private  ArrayList<String> date_Array;
	private  ArrayList<String> min_Array;
	private  ArrayList<String> max_Array;
	private  ArrayList<String> avg_Array;

	
	
	public GraphNewfeedCustomList(Activity context, ArrayList<String> date_Array, ArrayList<String> min_Array,
			ArrayList<String> max_Array, ArrayList<String> avg_Array) {
		// TODO Auto-generated constructor stub
		super(context, R.layout.graphnewsfeed_list, date_Array);
		this.context = context;
		this.date_Array = date_Array;
		this.min_Array = min_Array;
		this.max_Array = max_Array;
		this.avg_Array = avg_Array;
		
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.graphnewsfeed_list, null, true);
		TextView aboveline = (TextView) rowView.findViewById(R.id.aboveline);
		TextView tvcurrentone = (TextView) rowView.findViewById(R.id.tvcurrentone);
		TextView tvcurrenttwo= (TextView) rowView.findViewById(R.id.tvcurrenttwo);
		TextView tvcurrentthree = (TextView) rowView.findViewById(R.id.tvcurrentthree);
		TextView tvcurrentfour= (TextView) rowView.findViewById(R.id.tvcurrentfour);
		TextView belowline = (TextView) rowView.findViewById(R.id.belowline);
		if(date_Array.get(position).isEmpty()){
			aboveline.setVisibility(view.GONE);
			tvcurrentone.setVisibility(view.INVISIBLE);
			tvcurrenttwo.setVisibility(view.INVISIBLE);
			tvcurrentthree.setVisibility(view.INVISIBLE);
			tvcurrentfour.setVisibility(view.INVISIBLE);
			belowline.setVisibility(view.GONE);
		}
		else{
		aboveline.setVisibility(view.VISIBLE);
		tvcurrentone.setVisibility(view.VISIBLE);
		tvcurrenttwo.setVisibility(view.VISIBLE);
		tvcurrentthree.setVisibility(view.VISIBLE);
		tvcurrentfour.setVisibility(view.VISIBLE);
		
		tvcurrentone.setText(date_Array.get(position));
		tvcurrenttwo.setText(min_Array.get(position));
		tvcurrentthree.setText(max_Array.get(position));
		tvcurrentfour.setText(avg_Array.get(position));
		belowline.setVisibility(view.VISIBLE);
		}
		
		return rowView;
	}

	
}