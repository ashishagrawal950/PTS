package com.pts.invetech.module.rtclubbed_detail.model;

/**
 * Created by vaibhav on 29/3/18.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Blockdata {

    @SerializedName("first")
    @Expose
    private List<First> first = null;
    @SerializedName("second")
    @Expose
    private List<First> second = null;

    public List<First> getFirst() {
        return first;
    }

    public void setFirst(List<First> first) {
        this.first = first;
    }

    public List<First> getSecond() {
        return second;
    }

    public void setSecond(List<First> second) {
        this.second = second;
    }

}
