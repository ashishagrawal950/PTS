package com.pts.invetech.classes.fragment;


import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.R;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.dashboardupdate.views.DashBoardUpdate;

public class ContactusFragment extends Fragment {
    private LinearLayout openLayout;
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private String loginconfig;
    private View rootView;
    private TextView tvphonenumber, tvextensionnumber, tvtoolfreenubernumber, tvemail, tvwebsite;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_contactus, container,
                false);

        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NewFeedMainActivity) getActivity()).open();
            }
        });

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        Intent in = getActivity().getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");

        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        lltoplogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });

        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });


        tvphonenumber = (TextView) rootView.findViewById(R.id.tvphonenumber);
        tvphonenumber.setPaintFlags(tvphonenumber.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvextensionnumber = (TextView) rootView.findViewById(R.id.tvextensionnumber);
        tvextensionnumber.setPaintFlags(tvextensionnumber.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvtoolfreenubernumber = (TextView) rootView.findViewById(R.id.tvtoolfreenubernumber);
        tvtoolfreenubernumber.setPaintFlags(tvtoolfreenubernumber.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvemail = (TextView) rootView.findViewById(R.id.tvemail);
        tvemail.setPaintFlags(tvemail.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvwebsite = (TextView) rootView.findViewById(R.id.tvwebsite);
        tvwebsite.setPaintFlags(tvwebsite.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tvphonenumber.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String tvphonenumbershow = tvphonenumber.getText().toString().trim();
                Intent dial = new Intent();
                dial.setAction("android.intent.action.DIAL");
                dial.setData(Uri.parse("tel:" + tvphonenumbershow));
                startActivity(dial);
            }
        });

        tvextensionnumber.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String tvextensionnumbershow = tvextensionnumber.getText().toString().trim();
                Intent dial = new Intent();
                dial.setAction("android.intent.action.DIAL");
                dial.setData(Uri.parse("tel:" + tvextensionnumbershow));
                startActivity(dial);
            }
        });

        tvtoolfreenubernumber.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String tvtoolfreenubernumbershow = tvtoolfreenubernumber.getText().toString().trim();
                Intent dial = new Intent();
                dial.setAction("android.intent.action.DIAL");
                dial.setData(Uri.parse("tel:" + tvtoolfreenubernumbershow));
                startActivity(dial);
            }
        });

        tvemail.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String tvemailshow = tvemail.getText().toString().trim();
				/*Intent i = new Intent(Intent.ACTION_SEND);
				i.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ tvemailshow });
				//i.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
				//i.putExtra(android.content.Intent.EXTRA_TEXT, text);
				startActivity(Intent.createChooser(i, "Send email"));*/
                Intent intent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {tvemailshow};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                //intent.putExtra(Intent.EXTRA_CC,"cc");
                intent.setType("text/html");
                startActivity(Intent.createChooser(intent, "Send mail"));
            }
        });

        tvwebsite.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String tvwebsiteshow = tvwebsite.getText().toString().trim();
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + tvwebsiteshow));
                startActivity(myIntent);
            }
        });


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashBoardUpdate dashBoardUpdate = new DashBoardUpdate();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, dashBoardUpdate);
                        fragmentTransaction.commit();
                    }
                    return true;
                }
                return false;
            }
        });
    }

}