package com.pts.invetech.myscheduling;

/**
 * Created by Ashish on 25-03-2018.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class UnclubbedData {
    @SerializedName("real_time_data_current_id")
    @Expose
    private String realTimeDataCurrentId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("buyer")
    @Expose
    private String buyer;
    @SerializedName("seller")
    @Expose
    private String seller;
    @SerializedName("route")
    @Expose
    private String route;
    @SerializedName("periphery")
    @Expose
    private String periphery;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("stateid")
    @Expose
    private Object stateid;
    @SerializedName("app_no")
    @Expose
    private String appNo;
    @SerializedName("app_no_int")
    @Expose
    private String appNoInt;
    @SerializedName("revision")
    @Expose
    private String revision;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("importtype")
    @Expose
    private String importtype;
    @SerializedName("corridor")
    @Expose
    private String corridor;
    @SerializedName("trader")
    @Expose
    private String trader;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("data_id")
    @Expose
    private String dataId;
    @SerializedName("read_status")
    @Expose
    private String readStatus;
    @SerializedName("sum")
    @Expose
    private String sum;
    @SerializedName("real_time_data_blocks_id")
    @Expose
    private String realTimeDataBlocksId;
    @SerializedName("previous_id")
    @Expose
    private String previousId;
    @SerializedName("qtm")
    @Expose
    private String qtm;
    @SerializedName("prevRev")
    @Expose
    private String prevRev;
    @SerializedName("currentRev")
    @Expose
    private String currentRev;
    @SerializedName("difference")
    @Expose
    private String difference;
    @SerializedName("difference_value")
    @Expose
    private String differenceValue;

    public String getRealTimeDataCurrentId() {
        return realTimeDataCurrentId;
    }

    public void setRealTimeDataCurrentId(String realTimeDataCurrentId) {
        this.realTimeDataCurrentId = realTimeDataCurrentId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getPeriphery() {
        return periphery;
    }

    public void setPeriphery(String periphery) {
        this.periphery = periphery;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Object getStateid() {
        return stateid;
    }

    public void setStateid(Object stateid) {
        this.stateid = stateid;
    }

    public String getAppNo() {
        return appNo;
    }

    public void setAppNo(String appNo) {
        this.appNo = appNo;
    }

    public String getAppNoInt() {
        return appNoInt;
    }

    public void setAppNoInt(String appNoInt) {
        this.appNoInt = appNoInt;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImporttype() {
        return importtype;
    }

    public void setImporttype(String importtype) {
        this.importtype = importtype;
    }

    public String getCorridor() {
        return corridor;
    }

    public void setCorridor(String corridor) {
        this.corridor = corridor;
    }

    public String getTrader() {
        return trader;
    }

    public void setTrader(String trader) {
        this.trader = trader;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getRealTimeDataBlocksId() {
        return realTimeDataBlocksId;
    }

    public void setRealTimeDataBlocksId(String realTimeDataBlocksId) {
        this.realTimeDataBlocksId = realTimeDataBlocksId;
    }

    public String getPreviousId() {
        return previousId;
    }

    public void setPreviousId(String previousId) {
        this.previousId = previousId;
    }

    public String getQtm() {
        return qtm;
    }

    public void setQtm(String qtm) {
        this.qtm = qtm;
    }

    public String getPrevRev() {
        return prevRev;
    }

    public void setPrevRev(String prevRev) {
        this.prevRev = prevRev;
    }

    public String getCurrentRev() {
        return currentRev;
    }

    public void setCurrentRev(String currentRev) {
        this.currentRev = currentRev;
    }

    public String getDifference() {
        return difference;
    }

    public void setDifference(String difference) {
        this.difference = difference;
    }

    public String getDifferenceValue() {
        return differenceValue;
    }

    public void setDifferenceValue(String differenceValue) {
        this.differenceValue = differenceValue;
    }
}
