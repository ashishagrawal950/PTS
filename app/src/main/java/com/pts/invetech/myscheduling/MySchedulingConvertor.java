package com.pts.invetech.myscheduling;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vaibhav on 11/4/17.
 */

public class MySchedulingConvertor {

    /*
          \\u([0-9]|[a-fA-F])([0-9]|[a-fA-F])([0-9]|[a-fA-F])([0-9]|[a-fA-F])/
     */
    public static List<String> getStateListFrmRegion(String data){
        if(data==null){
            return null;
        }
        List<String> stateList=new ArrayList<String>();

        try {
            JSONArray array=new JSONArray(data);

            for(int index=0;index<array.length();index++){
                String name=array.getString(index);
                stateList.add(name.replaceAll("\\u0000",""));
            }

            return stateList;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void getCurrentFlowData(List<CurrentFlowModel> flowModel,String data) throws JSONException {

        JSONArray array=new JSONArray(data);

        for(int index=0;index<array.length();index++){
            JSONObject object=array.getJSONObject(index);

            CurrentFlowModel model=new CurrentFlowModel();
            if(object.has("id")) model.setId(object.getString("id"));
            if(object.has("buyer")) model.setBuyer(object.getString("buyer"));
            if(object.has("seller")) model.setSeller(object.getString("seller"));
            if(object.has("periphery")) model.setPeriphery(object.getString("periphery"));
            if(object.has("region")) model.setRegion(object.getString("region"));
            if(object.has("state")) model.setState(object.getString("state"));
            if(object.has("app_no")) model.setAppNo(object.getString("app_no"));
            if(object.has("revision")) model.setRevision(object.getString("revision"));
            if(object.has("date")) model.setDate(object.getString("date"));
            if(object.has("importtype")) model.setImporttype(object.getString("importtype"));
            if(object.has("trader")) model.setTrader(object.getString("trader"));
            if(object.has("qtm")) model.setQtm(object.getString("qtm"));


            flowModel.add(model);
        }

    }


}
