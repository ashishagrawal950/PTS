
package com.pts.model.stationmain.stationwise.blockwise;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {

    @SerializedName("block_data")
    @Expose
    private List<BlockDatum> blockData = null;
    @SerializedName("sum")
    @Expose
    private Integer sum;

    public List<BlockDatum> getBlockData() {
        return blockData;
    }

    public void setBlockData(List<BlockDatum> blockData) {
        this.blockData = blockData;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

}
