package com.pts.invetech.dashboardupdate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.dashboardupdate.model.state.State;
import com.pts.invetech.dashboardupdate.model.state.StateList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vaibhav on 5/1/17.
 */

public class StateAdapter extends BaseAdapter {

    private List<State> statesList;
    private Context mContext;

    public StateAdapter(Context _con, StateList _list){
        mContext=_con;
        statesList=_list.getStates();
    }


    @Override
    public int getCount() {
        return statesList.size();
    }

    @Override
    public State getItem(int i) {
        return statesList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view==null){
            view= LayoutInflater.from(mContext).inflate(R.layout.spinner_text,null);
        }

        ((TextView)view).setText(getItem(i).getStateName()+" ("+getItem(i).getIexregion()+")");
        return view;
    }

    public ArrayList<State> getStatesArrayList(){
        ArrayList<State> arrayList=new ArrayList<>();
        arrayList.addAll(statesList);

        return arrayList;
    }
}
