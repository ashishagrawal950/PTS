package com.pts.model.myscheduling;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by vaibhav on 29/3/18.
 */

public class ClubViewData implements Parcelable {

    private int isClubbed=0;

    private String sNo;
    private String region;
    private String buyer;
    private String seller;
    private String approval;
    private String qtm;
    private String difference;
    private String id;
    private String prevId;
    private String revision;

    public ClubViewData(){}

    protected ClubViewData(Parcel in) {
        isClubbed = in.readInt();
        sNo = in.readString();
        region = in.readString();
        buyer = in.readString();
        seller = in.readString();
        approval = in.readString();
        qtm = in.readString();
        difference = in.readString();
        id = in.readString();
        prevId = in.readString();
        revision = in.readString();
    }

    public static final Creator<ClubViewData> CREATOR = new Creator<ClubViewData>() {
        @Override
        public ClubViewData createFromParcel(Parcel in) {
            return new ClubViewData(in);
        }

        @Override
        public ClubViewData[] newArray(int size) {
            return new ClubViewData[size];
        }
    };

    public int getIsClubbed() {
        return isClubbed;
    }

    public void setIsClubbed(int isClubbed) {
        this.isClubbed = isClubbed;
    }

    public String getsNo() {
        return sNo;
    }

    public void setsNo(String sNo) {
        this.sNo = sNo;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    public String getQtm() {
        return qtm;
    }

    public void setQtm(String qtm) {
        this.qtm = qtm;
    }

    public String getDifference() {
        return difference;
    }

    public void setDifference(String difference) {
        this.difference = difference;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrevId() {
        return prevId;
    }

    public void setPrevId(String prevId) {
        this.prevId = prevId;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(isClubbed);
        dest.writeString(sNo);
        dest.writeString(region);
        dest.writeString(buyer);
        dest.writeString(seller);
        dest.writeString(approval);
        dest.writeString(qtm);
        dest.writeString(difference);
        dest.writeString(id);
        dest.writeString(prevId);
        dest.writeString(revision);
    }
}
