package com.pts.invetech.pojo;

public class NewBidDeleteClientBid {
	private String id;
	private String bidtype;
	private String type;
	private String biddate;
	private String addbiddaleteaccesskey;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBidtype() {
		return bidtype;
	}
	public void setBidtype(String bidtype) {
		this.bidtype = bidtype;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBiddate() {
		return biddate;
	}
	public void setBiddate(String biddate) {
		this.biddate = biddate;
	}
	public String getAddbiddaleteaccesskey() {
		return addbiddaleteaccesskey;
	}
	public void setAddbiddaleteaccesskey(String addbiddaleteaccesskey) {
		this.addbiddaleteaccesskey = addbiddaleteaccesskey;
	}
	
	
	
}
