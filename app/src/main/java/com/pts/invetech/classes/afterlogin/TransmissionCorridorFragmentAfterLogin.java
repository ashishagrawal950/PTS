package com.pts.invetech.classes.afterlogin;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.customlist.TransmissionCorridorCorridorCustomAdapter;
import com.pts.invetech.customlist.TransmissionCorridorExportCustomAdapter;
import com.pts.invetech.customlist.TransmissionCorridorImportCustomAdapter;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.pojo.TransmissionCorridorJsonStructure;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class TransmissionCorridorFragmentAfterLogin extends Fragment {
    private final String TAG_IMPORT = "IMPORT";
    private final String TAG_EXPORT = "EXPORT";
    private final String TAG_CORRIDOR = "CORRIDOR";
    private ConnectionDetector cd;
    private Dialog prgDialog;
    private LinearLayout openLayout;
    private SQLiteDatabase db;
    private String device_id;
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext, buttonClick, tvnodatafound;
    private TransmissionCorridorImportCustomAdapter transmissionCorridorImportCustomAdapter;
    private NonScrollListView lv_import, lv_export, lv_corridor;
    private ArrayList<String> blockarrayListfrom = new ArrayList<String>();
    private View rootView;
    private String from, domain;
    private ProgressBar pbprogressBar;
    private TextView tvupdatedateandtime;
    private TransmissionCorridorJsonStructure structure;

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_transmissioncorridor, container,
                false);
		/*prgDialog = new ProgressDialog(getActivity());
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        db = getActivity().openOrCreateDatabase("deviceDBSecond",
                Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientid(clientid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationnewsfeed(newMessage TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS gpsloaction(latitude VARCHAR, longitude VARCHAR, state_id VARCHAR, state_serial VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS availableurs(region VARCHAR, available VARCHAR, select_yes VARCHAR, revision VARCHAR, region_id VARCHAR);");
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
//        domain = getArguments().getString("domain");
        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivityAfterLogin) getActivity()).open();
            }
        });

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        lltoplogin.setVisibility(View.GONE);
        lltoplogout.setVisibility(View.VISIBLE);
        tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
        tvlogouttext.setText("Home");

        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(in);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);

            }
        });

        ConnectionDetector cd = new ConnectionDetector(getActivity());
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getActivity(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            from = "start";
            new HttpAsyncTaskcoridoravailibility().execute(domain + "/mobile/pxs_app/service/coridoravailibility.php");
        }

        new CountDownTimer(15000, 1000) {
            public void onTick(long millisUntilFinished) {
                // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
				/*ConnectionDetector cd = new ConnectionDetector(getActivity());
				if (!cd.isConnectingToInternet()) {
					Toast.makeText(getActivity(), "Please Check your network.",
							Toast.LENGTH_LONG).show();
				} else {*/
                if (isVisible()) {
                    Log.e("newsfeed", "in side counter");
                    from = "counter";
                    new HttpAsyncTaskcoridoravailibility()
                            .execute(domain + "/mobile/pxs_app/service/coridoravailibility.php");
                }
                Log.e("newsfeed", "out side counter");
                //}
            }
        }.start();
        tvupdatedateandtime = (TextView) rootView.findViewById(R.id.tvupdatedateandtime);
        pbprogressBar = (ProgressBar) rootView.findViewById(R.id.pbprogressBar);
        lv_import = (NonScrollListView) rootView.findViewById(R.id.lv_import);
        lv_export = (NonScrollListView) rootView.findViewById(R.id.lv_export);
        lv_corridor = (NonScrollListView) rootView.findViewById(R.id.lv_corridor);

        return rootView;
    }

    private void updateImportList() {
        if (structure == null) {
            return;
        }
        if (getActivity() != null) {
            TransmissionCorridorImportCustomAdapter adapter = new TransmissionCorridorImportCustomAdapter(
                    getActivity(), structure.getAllImport());
            lv_import.setAdapter(adapter);

            TransmissionCorridorExportCustomAdapter adapter1 = new TransmissionCorridorExportCustomAdapter(
                    getActivity(), structure.getAllExport());
            lv_export.setAdapter(adapter1);

            TransmissionCorridorCorridorCustomAdapter adapter2 = new TransmissionCorridorCorridorCustomAdapter(
                    getActivity(), structure.getAllCorridor());
            lv_corridor.setAdapter(adapter2);
        }


    }

    private void setData(TransmissionCorridorJsonStructure structure, JSONArray jsonArray, int tag) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONArray arr = jsonArray.getJSONArray(i);
            ArrayList<String> second = new ArrayList<>();
            for (int j = 0; j < arr.length(); j++) {

                String str = arr.getString(j);

                if (str == null || str.isEmpty()) {
                    str = "-";
                }
                second.add(str);
            }
            switch (tag) {
                case 0:
                    structure.addImport(second);
                    break;
                case 1:
                    structure.addExport(second);
                    break;
                case 2:
                    structure.addCorridor(second);
                    break;
            }
        }
    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP
                        && keyCode == KeyEvent.KEYCODE_BACK) {
                    Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                    getActivity().finish();
                    return true;

                }

                return false;
            }
        });
    }

    private class HttpAsyncTaskcoridoravailibility extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }

        @Override
        protected void onPreExecute() {
            if (from.equalsIgnoreCase("counter")) {
                pbprogressBar.setVisibility(View.VISIBLE);
            } else if (from.equalsIgnoreCase("start")) {
                prgDialog.show();
            }
            super.onPreExecute();
        }


        @SuppressLint("LongLogTag")
        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Request For Coridor availibility:", result);
            if (from.equalsIgnoreCase("counter")) {
                pbprogressBar.setVisibility(View.INVISIBLE);
            } else if (from.equalsIgnoreCase("start")) {
                prgDialog.hide();
            }
            try {
                JSONObject jsonRootObject = new JSONObject(result);
                String datefromApi = jsonRootObject.getString("date");
                tvupdatedateandtime.setText(datefromApi.trim());
                structure = new TransmissionCorridorJsonStructure();

                JSONObject datajsonArray = jsonRootObject.getJSONObject("data");
                String[] tagsArr = {TAG_IMPORT, TAG_EXPORT, TAG_CORRIDOR};
                for (int index = 0; index < tagsArr.length; index++) {
                    if (datajsonArray.has(tagsArr[index])) {
                        JSONArray jsonArray = datajsonArray.optJSONArray(tagsArr[index]);
                        setData(structure, jsonArray, index);
                    }
                }
            } catch (JSONException e) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String errormass = mainObject.getString("message");
                    Toast.makeText(getActivity(), errormass, Toast.LENGTH_LONG).show();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                if (result != null) {
                    //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                } else {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            updateImportList();

            new CountDownTimer(15000, 1000) {
                public void onTick(long millisUntilFinished) {
                    // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                }

                public void onFinish() {
					/*ConnectionDetector cd = new ConnectionDetector(getActivity());
					if (!cd.isConnectingToInternet()) {
						Toast.makeText(getActivity(), "Please Check your network.",
								Toast.LENGTH_LONG).show();
					} else {*/
                    if (isVisible()) {
                        Log.e("newsfeed", "in side counter");
                        from = "counter";
                        new HttpAsyncTaskcoridoravailibility().execute(domain + "/mobile/pxs_app/service/coridoravailibility.php");
                        //}
                    }
                    Log.e("newsfeed", "out side counter");
                }
            }.start();
        }
    }


}