package com.pts.invetech.customlist;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pts.invetech.R;

public class URSGeneratorWisenameCustomList extends ArrayAdapter<String> {

	private Activity activity;
	private ArrayList<String> text1;
	private ArrayList<String> text2;
	private ArrayList<String> text3;
	private ArrayList<String> text4;
	private ArrayList<String> text5;

	public URSGeneratorWisenameCustomList(Activity activity, ArrayList<String> text1,
			ArrayList<String> text2, ArrayList<String> text3, ArrayList<String> text4, ArrayList<String> text5) {
		// TODO Auto-generated constructor stub
		super(activity, R.layout.list_urs_generator, text1);
		this.activity = activity;
		this.text1 = text1;
		this.text2 = text2;
		this.text3 = text3;
		this.text4 = text4;
		this.text5 = text5;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = activity.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.list_urs_generator, null, true);
		LinearLayout llback = (LinearLayout) rowView.findViewById(R.id.llback);
		TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
		TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
		TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
		txt1.setText(text1.get(position));
		txt2.setText(text2.get(position) + " - " + text3.get(position));
		//txt3.setText(text4.get(position));
		
		if(text5.get(position).equalsIgnoreCase("YES")){
			llback.setBackgroundColor(Color.parseColor("#caebeceb"));
			txt3.setText(text4.get(position));
			txt3.setTextColor(Color.RED);
		}else{
			llback.setBackgroundColor(Color.WHITE);
			txt3.setText(text4.get(position));
			txt3.setTextColor(Color.GREEN);
		}
		// Animation animation = AnimationUtils.loadAnimation(activity,
		// (position > lastPosition) ? R.anim.up_from_bottom :
		// R.anim.down_from_top);
		// rowView.startAnimation(animation);
		// lastPosition = position;
		// txt3.setText("Delete");
		// imageView.setImageResource(imageId[position]);
		return rowView;
	}
}