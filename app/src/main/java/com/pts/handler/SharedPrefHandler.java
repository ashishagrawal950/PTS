package com.pts.handler;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.pts.invetech.dashboardupdate.model.state.State;
import com.pts.invetech.utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vaibhav on 16-08-2016.
 */
public class SharedPrefHandler {

    private static final String DB_SHAREDPREF="com.ptssharedpref";
    private static final String DB_ISFIRSTTIME="com.ptssharedpref.isfirsttime";
    private static final String DB_FIRSTMAP="com.ptssharedpref.firstmap";
    private static final String DB_STATELIST="com.pts.db.state";


    public static final String VALUE_MYSCHEDULE_DETAIL="myscheduledetail";

    public static boolean checkFirstTime(Context context){

        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandler.DB_SHAREDPREF, Context.MODE_PRIVATE);

        return sharedPref.getBoolean(DB_ISFIRSTTIME,true);
    }

    public static void setFirstTime(Context context){

        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandler.DB_SHAREDPREF, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(SharedPrefHandler.DB_ISFIRSTTIME, false);
        editor.commit();
    }

    public static void saveString(Context context,String dbName,String data){

        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandler.DB_SHAREDPREF, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(dbName, data);
        editor.commit();
    }
    public static void saveDeviceString(Context context, String column, String value) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(column, value);
        editor.commit();
    }

    public static boolean checkFirstTimeMap(Context context){

        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandler.DB_SHAREDPREF, Context.MODE_PRIVATE);

        return sharedPref.getBoolean(DB_FIRSTMAP,true);
    }

    public static void setFirstTimeMap(Context context){

        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandler.DB_SHAREDPREF, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(SharedPrefHandler.DB_FIRSTMAP, false);
        editor.commit();
    }

    /*
   Saving DataBase data
*/

    public static void saveInt(Context context, String key, int value){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();

    }






/*
    Getting Database Data
 */

    public static int getIntData(Context context, String key){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        int data=preferences.getInt(key, -1);

        return data;
    }

    public static void saveBoolean(Activity context, String key, boolean value){
        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandler.DB_SHAREDPREF, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }

    public static String getString(Context context,String table){

        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandler.DB_SHAREDPREF, Context.MODE_PRIVATE);

        return sharedPref.getString(table,null);
    }


    public static boolean getBoolean(Context context,String table){

        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandler.DB_SHAREDPREF, Context.MODE_PRIVATE);

        return sharedPref.getBoolean(table,false);
    }

    public static String getDeviceString(Context context, String column) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(column, null);
    }



    public static void savemysheduleDetailBoolean(Activity context, String key, boolean value){
        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandler.VALUE_MYSCHEDULE_DETAIL, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean savemysheduleDetailgetBoolean(Context context,String table){
        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandler.VALUE_MYSCHEDULE_DETAIL, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(table,false);
    }

    public static void saveStateList(Context context,List<State> stateList) throws JSONException {
        if(stateList==null){
            return;
        }
        JSONArray array=new JSONArray();

        for(int i=0;i<stateList.size();i++){
            JSONObject obj=new JSONObject();
            State st=stateList.get(i);
            obj.put("id",st.getId());
            obj.put("name",st.getStateName());
            obj.put("region",st.getRegion());
            array.put(obj);
        }
        saveString(context,DB_STATELIST,array.toString());
    }

    public static List<State> getStateList(Context context) throws JSONException {
        String data=getString(context,DB_STATELIST);

        if(data==null){
            return null;
        }

        JSONArray array=new JSONArray(data);
        List<State> stateList=new ArrayList<>();

        for(int index=0;index<array.length();index++){
            JSONObject object=array.getJSONObject(index);

            State state=new State();
            state.setStateName(object.getString("name"));
            state.setId(object.getString("id"));

            stateList.add(state);
        }

        return stateList;
    }

    public static void saveStringData(Context context,String dbName,String data){

        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandler.DB_SHAREDPREF, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(dbName, data);
        editor.commit();
    }



}