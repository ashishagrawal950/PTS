package com.pts.invetech.marquee;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Vaibhav on 08-08-2016.
 */

public class MyLayoutManager extends LinearLayoutManager{

    private Context mContext;
    public LinearSmoothScroller smoothScroller;
    private int mPos=0;

    public MyLayoutManager(Context context) {
        super(context, LinearLayoutManager.HORIZONTAL, false);
        mContext = context;
        smoothScroller=new MySmoothScroller(mContext,this);
    }

    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView,
                                       RecyclerView.State state, int position) {
        mPos=position;
        smoothScroller.setTargetPosition(position);
        startSmoothScroll(smoothScroller);
    }

    public void againStart(){
        smoothScroller.setTargetPosition(mPos);
        startSmoothScroll(smoothScroller);
    }
}