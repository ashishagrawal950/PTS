package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pts.invetech.NotificationOne;
import com.pts.invetech.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class DownloadFileActivity extends Activity {

    private Context context;
    private LinearLayout mopenLayout;
    private String client_id;
    private SQLiteDatabase db;
    private String appurl, categoryurl, extation = "";
    private ImageView ivshare, ivdownload;
    private String categoryurlfileSend;
    private int numMessagesOne = 0;
    private NotificationManager myNotificationManager;
    private int notificationIdOne = 111;
    private String lastdatesend;
    private String filenameSave;
    private String webUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_downloadfile);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            //extation = extras.getString("extation");
            appurl = extras.getString("appurl");
            categoryurl = extras.getString("categoryurl");
            //String[] categoryurlfile = categoryurl.split("/");
            // categoryurlfileSend = categoryurlfile[categoryurlfile.length-1];
            lastdatesend = extras.getString("lastdate");
            new DownloadFileAsync().execute("https://" + appurl + categoryurl);
        } else {
            //..oops!
        }
        ivshare = (ImageView) findViewById(R.id.ivshare);
        ivdownload = (ImageView) findViewById(R.id.ivdownload);
	   /*
		ivshare.setVisibility(View.VISIBLE);
	    ivdownload = (ImageView) findViewById(R.id.ivdownload);
		ivdownload.setVisibility(View.GONE);*/
	    /*if(extation.contains(".xlsx")){
			ivshare.setVisibility(View.GONE);
			ivdownload.setVisibility(View.VISIBLE);
	    }else if(extation.contains(".xls")){
			ivshare.setVisibility(View.GONE);
			ivdownload.setVisibility(View.VISIBLE);
		}else if(extation.contains(".pdf")){
			ivshare.setVisibility(View.VISIBLE);
			ivdownload.setVisibility(View.VISIBLE);
	    }else{
			ivshare.setVisibility(View.GONE);
			ivdownload.setVisibility(View.VISIBLE);
		}*/

        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DownloadFileActivity.this, DownloadAgainActivity.class);
                in.putExtra("lastdate", lastdatesend);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        ivdownload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadFileAsync().execute("http://" + appurl + categoryurl);
                Notification.Builder mBuilder = new Notification.Builder(DownloadFileActivity.this);

                //String[] parts =  categoryurl.split("/");
                //String filename = parts[parts.length-1];
                mBuilder.setContentTitle("PTS");
                if (extation.endsWith(".pdf")) {
                    mBuilder.setContentText(extation + " file download.(File is in PDF format.)");
                } else if (extation.endsWith(".xlsx")) {
                    mBuilder.setContentText(extation + " file download.(File is in xlsx format.)");
                } else if (extation.endsWith(".xls")) {
                    mBuilder.setContentText(extation + " file download.(File is in xls format.)");
                }
                mBuilder.setTicker("Downloading");
                mBuilder.setSmallIcon(R.drawable.logopts);

                // Increase notification number every time a new notification arrives
                mBuilder.setNumber(++numMessagesOne);
                mBuilder.setAutoCancel(true);
                // Creates an explicit intent for an Activity in your app
                Intent myIntent = new Intent();
                myIntent.setAction(android.content.Intent.ACTION_VIEW);
                if (categoryurl.endsWith(".pdf")) {
                    File file = new File(Environment.getExternalStorageDirectory().getPath() + "/" + extation);
                    if (file.exists()) {
                        myIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                    } else {

                    }
                } else if (categoryurl.endsWith(".xlsx")) {
                    File file = new File(Environment.getExternalStorageDirectory().getPath() + "/" + extation);
                    if (file.exists()) {
                        myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                    } else {
                        //Toast.makeText(DownloadFileActivity.this, "Unable to ", Toast.LENGTH_SHORT).show();
                    }
                } else if (categoryurl.endsWith(".xls")) {
                    File file = new File(Environment.getExternalStorageDirectory().getPath() + "/" + extation);
                    if (file.exists()) {
                        myIntent.setDataAndTypeAndNormalize(Uri.fromFile(file), "application/vnd.ms-excel");
                    } else {

                    }
                }
                // Intent resultIntent = new Intent(DownloadFileActivity.this, NotificationOne.class);
                // resultIntent.putExtra("notificationId", notificationIdOne);

                //This ensures that navigating backward from the Activity leads out of the app to Home page
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadFileActivity.this);
                // Adds the back stack for the Intent
                stackBuilder.addParentStack(NotificationOne.class);

                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(myIntent);
                PendingIntent resultPendingIntent =
                        stackBuilder.getPendingIntent(
                                0,
                                PendingIntent.FLAG_ONE_SHOT //can only be used once
                        );
                // start the activity when the user clicks the notification text
                mBuilder.setContentIntent(resultPendingIntent);
                Notification noti = mBuilder.build();
                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                noti.defaults |= Notification.DEFAULT_VIBRATE;
                noti.defaults |= Notification.DEFAULT_SOUND;
                noti.flags |= Notification.FLAG_SHOW_LIGHTS;
                noti.ledARGB = 0xff00ff00;
                noti.ledOnMS = 300;
                noti.ledOffMS = 1000;
                noti.flags |= Notification.FLAG_AUTO_CANCEL;
                // pass the Notification object to the system
                myNotificationManager.notify(notificationIdOne, mBuilder.build());
            }
        });


        ivshare.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Resources resources = getResources();

                Intent emailIntent = new Intent();
                emailIntent.setAction(Intent.ACTION_SEND);
                // Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
                emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_native)));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
                emailIntent.setType("message/rfc822");

                PackageManager pm = getPackageManager();
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");

                Intent openInChooser = Intent.createChooser(emailIntent, resources.getString(R.string.share_chooser_text));
                List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
                List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
                for (int i = 0; i < resInfo.size(); i++) {
                    // Extract the label, append it, and repackage it in a LabeledIntent
                    ResolveInfo ri = resInfo.get(i);
                    String packageName = ri.activityInfo.packageName;
                    if (packageName.contains("android.email")) {
                        emailIntent.setPackage(packageName);
                    } else if (packageName.contains("whatsapp") || packageName.contains("gm")) {
                        Intent intent = new Intent();
                        intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        if (packageName.contains("whatsapp")) {
                            //String myFilePath = Environment.getExternalStorageDirectory().getPath()+"/"+categoryurlfileSend;
                            String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/" + extation;
                            File fileWithinMyDir = new File(myFilePath);
                            Log.d("downloadmyFilePath", myFilePath);
                            if (fileWithinMyDir.exists()) {
                                intent.setType("application*//*");
                                intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + myFilePath));
                                //intent.putExtra(Intent.EXTRA_SUBJECT,"PTS");
                                //intent.putExtra(Intent.EXTRA_TEXT, "PTS");
                                //startActivity(Intent.createChooser(intent, "Share File"));
                            }

                            // intent.putExtra(Intent.EXTRA_TEXT, resources.getString(R.string.share_twitter));
                        } else if (packageName.contains("android.gm")) {
                            intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_gmail)));
                            intent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
                            intent.setType("message/rfc822");
                            //String myFilePath = Environment.getExternalStorageDirectory().getPath()+"/"+categoryurlfileSend;
                            String myFilePath = Environment.getExternalStorageDirectory().getPath() + "/" + extation;
                            File fileWithinMyDir = new File(myFilePath);
                            if (fileWithinMyDir.exists()) {
                                intent.setType("application/pdf");
                                intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + myFilePath));
                                intent.putExtra(Intent.EXTRA_SUBJECT, "PTS");
                                intent.putExtra(Intent.EXTRA_TEXT, "PTS");
                                //startActivity(Intent.createChooser(intent, "Share File"));
                            }
                        }
                        intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
                    }
                }

                // convert intentList to array
                LabeledIntent[] extraIntents = intentList.toArray(new LabeledIntent[intentList.size()]);

                openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
                startActivity(openInChooser);
            }

        });

		/*ivshare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Resources resources = getResources();

				Intent emailIntent = new Intent();
				emailIntent.setAction(Intent.ACTION_SEND);
				// Native email client doesn't currently support HTML, but it doesn't hurt to try in case they fix it
				emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_native)));
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
				emailIntent.setType("message/rfc822");

				PackageManager pm = getPackageManager();
				Intent sendIntent = new Intent(Intent.ACTION_SEND);
				sendIntent.setType("text/plain");

				Intent openInChooser = Intent.createChooser(emailIntent, resources.getString(R.string.share_chooser_text));
				List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
				List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
				for (int i = 0; i < resInfo.size(); i++) {
					// Extract the label, append it, and repackage it in a LabeledIntent
					ResolveInfo ri = resInfo.get(i);
					String packageName = ri.activityInfo.packageName;
					if(packageName.contains("android.email")) {
						emailIntent.setPackage(packageName);
					} else if(packageName.contains("whatsapp") || packageName.contains("gm")) {
						Intent intent = new Intent();
						intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
						intent.setAction(Intent.ACTION_SEND);
						intent.setType("text/plain");
						if(packageName.contains("whatsapp")) {
							*//*String myFilePath = Environment.getExternalStorageDirectory().getPath()+"/"+categoryurlfileSend;
							File fileWithinMyDir = new File(myFilePath);
							if(fileWithinMyDir.exists()) {
								intent.setType("application*//**//*");
								intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+myFilePath));
								//intent.putExtra(Intent.EXTRA_SUBJECT,"PTS");
								//intent.putExtra(Intent.EXTRA_TEXT, "PTS");
								//startActivity(Intent.createChooser(intent, "Share File"));
							}*//*
							intent.putExtra(Intent.EXTRA_TEXT, webUrl);
							startActivity(Intent.createChooser(intent, "Share File"));
							// intent.putExtra(Intent.EXTRA_TEXT, resources.getString(R.string.share_twitter));
						}
						else if(packageName.contains("android.gm")) {
							intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(resources.getString(R.string.share_email_gmail)));
							intent.putExtra(Intent.EXTRA_SUBJECT, resources.getString(R.string.share_email_subject));
							intent.setType("message/rfc822");
							*//*String myFilePath = Environment.getExternalStorageDirectory().getPath()+"/"+categoryurlfileSend;
							File fileWithinMyDir = new File(myFilePath);
							if(fileWithinMyDir.exists()) {
								intent.setType("application/pdf");
								intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+myFilePath));
								intent.putExtra(Intent.EXTRA_SUBJECT,"PTS");
								intent.putExtra(Intent.EXTRA_TEXT, "PTS");
								//startActivity(Intent.createChooser(intent, "Share File"));
							}*//*
							intent.putExtra(Intent.EXTRA_SUBJECT,"PTS");
							intent.putExtra(Intent.EXTRA_TEXT, webUrl);
						}
						intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
					}
				}

				// convert intentList to array
				LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);

				openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
				startActivity(openInChooser);
			}

		});*/


        WebView myWebView = (WebView) findViewById(R.id.webView);
        myWebView.clearCache(true);
        myWebView.setClickable(true);
        myWebView.setFocusableInTouchMode(true);
        myWebView.getSettings().setJavaScriptEnabled(true);
        //myWebView.setWebViewClient(new Callback());
        myWebView.getSettings().setAllowFileAccess(true);
        myWebView.getSettings().setPluginState(PluginState.ON);

        //---you need this to prevent the webview from
        // launching another browser when a url
        // redirection occurs---
        //myWebView.setWebViewClient(new Callback());
        webUrl = "http://" + appurl + categoryurl;
        myWebView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + webUrl);
        WebClientClass webViewClient = new WebClientClass();
        myWebView.setWebViewClient(webViewClient);
        WebChromeClient webChromeClient = new WebChromeClient();
        myWebView.setWebChromeClient(webChromeClient);

    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(DownloadFileActivity.this, DownloadAgainActivity.class);
        in.putExtra("lastdate", lastdatesend);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    public class WebClientClass extends WebViewClient {
        Dialog prgDialog = null;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
		   /*pd = new ProgressDialog(DownloadFileActivity.this);
		   pd.setTitle("Please wait");
		   pd.setMessage("Loading..");
		   pd.show();*/
            prgDialog = new Dialog(DownloadFileActivity.this);
            prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
            prgDialog.setContentView(R.layout.progessdialog);
            ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
            image.startAnimation(animation);
            prgDialog.show();
            prgDialog.setCancelable(false);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            prgDialog.dismiss();
        }
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileAsync extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                String myUrl = f_url[0];

                URL url = new URL(myUrl);
                URLConnection conection = url.openConnection();
                conection.connect();

                String depo = conection.getHeaderField("Content-Disposition");
                String[] depoSplit = depo.split("filename=");
                extation = depoSplit[1].replace("filename=", "").replace("\"", "").trim();


                int lenghtOfFile = conection.getContentLength();
                final String contentLengthStr = conection.getHeaderField("content-length");
                String tyyup = conection.getContentType();


                //AppLogger.showError("typpepe",tyyup);

                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment
                        .getExternalStorageDirectory().toString() + "/" + extation);

                byte[] data = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            // dismissDialog(progress_bar_type);
            if (extation.contains(".xlsx")) {
                ivshare.setVisibility(View.VISIBLE);
                ivdownload.setVisibility(View.VISIBLE);
            } else if (extation.contains(".xls")) {
                ivshare.setVisibility(View.VISIBLE);
                ivdownload.setVisibility(View.VISIBLE);
            } else if (extation.contains(".pdf")) {
                ivshare.setVisibility(View.VISIBLE);
                ivdownload.setVisibility(View.VISIBLE);
            } else {
                ivshare.setVisibility(View.VISIBLE);
                ivdownload.setVisibility(View.VISIBLE);
            }

        }

    }


}