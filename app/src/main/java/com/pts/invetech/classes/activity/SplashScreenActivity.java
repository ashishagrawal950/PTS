package com.pts.invetech.classes.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.MyAndroidFirebaseInstanceIDService;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.SplashScreenAPIResponse;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.pojo.DeviceRegister;
import com.pts.invetech.utils.Constant;
import com.pts.invetech.utils.JSONUtils;
import com.pts.invetech.utils.WakeLocker;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static com.pts.invetech.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.pts.invetech.CommonUtilities.EXTRA_MESSAGE;

public class SplashScreenActivity extends Activity {

    public static String deviceid;
    private String name, platform, uuid, version, height, width, colordepth;
    private Dialog prgDialog;
    private String dbdevice;
    private AsyncTask<Void, Void, Void> mRegisterTask;
    // Connection detector
    private ConnectionDetector cd;
    private SQLiteDatabase db;
    //	TextView goMessage;
    private NonScrollListView notificationlist;
    private TextView lblMessage, tvtradingdate;
    private String newMessage;
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(SplashScreenActivity.this);
            /**
             * Take appropriate action on this message
             * depending upon your app requirement
             * For now i am just displaying it on the screen
             * */
            // Showing received message
            //lblMessage.append("Ashish Agrawal:" + newMessage + "\n");
            if (newMessage.equalsIgnoreCase("Your device registred with GCM") || newMessage.equalsIgnoreCase("Trying (attempt 1/5) to register device on Demo Server.") ||
                    newMessage.equalsIgnoreCase("From Demo Server: successfully added device!") || newMessage.equalsIgnoreCase("null")) {

            } else {
                //db.execSQL("INSERT INTO pushnotification VALUES('"+newMessage+"', '" + datedb + "', '" + timedb + "')");
            }
            //Toast.makeText(NotificationActivity.this, "New Message: " + newMessage, Toast.LENGTH_LONG).show();
            // Releasing wake lock
            WakeLocker.release();
        }
    };
    private String dbacess, displaydate, displaytime;
    private String[] array;
    private String[] arraydispalydate;
    private String[] arraydispalytime;
    private String datedb, timedb, as = "";

    /*public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }*/
    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not

        setContentView(R.layout.activity_splash);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        db = openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS device(uuid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");

        //ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE); // without sdk version check

		/*deviceid = Secure.getString(SplashScreenActivity.this.getContentResolver(),
                Secure.ANDROID_ID);
		System.out.println("android_id_one => " + deviceid);*/

        //conditionCheck();


//        TelephonyManager tManagerr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        deviceid = tManagerr.getDeviceId();
        deviceid = Settings.Secure.getString(
                getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        // Toast.makeText(SplashScreenActivity.this, "IEMI Number "+deviceid, Toast.LENGTH_LONG).show();
        //String uniqueId = UUID.randomUUID().toString();

        System.out.println("deviceid => " + deviceid);
        MyAndroidFirebaseInstanceIDService.sendData(this, deviceid, refreshedToken);
        Calendar cc = Calendar.getInstance();
        System.out.println("Current time => " + cc.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = df.format(cc.getTime());
        String[] splitdateandtime = formattedDate.split(" ");
        datedb = splitdateandtime[0]; // 004
        timedb = splitdateandtime[1]; // 004

        cd = new ConnectionDetector(SplashScreenActivity.this);
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            //alert.showAlertDialog(SplashScreenActivity.this,"Internet Connection Error","Please connect to working Internet connection", false);
            // stop executing code by return
        }
        // Make sure the device has the proper dependencies.
//			GCMRegistrar.checkDevice(SplashScreenActivity.this);
        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
//			GCMRegistrar.checkManifest(SplashScreenActivity.this);

        notificationlist = (NonScrollListView) findViewById(R.id.notificationlist);
        lblMessage = (TextView) findViewById(R.id.lblMessage);
        registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));

        // Get GCM registration id
//			final String regId = GCMRegistrar.getRegistrationId(SplashScreenActivity.this);
        // Check if regid already presents
//			if (regId.equals("")) {
//				// Registration is not present, register now with GCM
//				GCMRegistrar.register(SplashScreenActivity.this, SENDER_ID);
//			} else {
        // Device is already registered on GCM
//				if (GCMRegistrar.isRegisteredOnServer(SplashScreenActivity.this)) {
//					// Skips registration.
//				//	Toast.makeText(getActivity(), "Already registered with GCM", Toast.LENGTH_LONG).show();
//				} else {
//					// Try to register again, but not in the UI thread.
//					// It's also necessary to cancel the thread onDestroy(),
//					// hence the use of AsyncTask instead of a raw thread.
//					final Context context = SplashScreenActivity.this;
//					mRegisterTask = new AsyncTask<Void, Void, Void>() {
//						@Override
//						protected Void doInBackground(Void... params) {
//							// Register on our server
//							// On server creates a new user
//							ServerUtilities.register(context, deviceid, regId);
//							return null;
//						}
//
//						@Override
//						protected void onPostExecute(Void result) {
//							mRegisterTask = null;
//						}
//
//					};
//					mRegisterTask.execute(null, null, null);
//				}
//			}
//			callNotification();


        Cursor c = db.rawQuery("SELECT * FROM device", null);
        if (c.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("uuid: " + c.getString(0) + "\n");
            dbdevice = c.getString(0);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
        }
		/*prgDialog = new ProgressDialog(this);
		prgDialog.setMessage("Please wait...Device is in process for register!!");
		prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        //TextView tv = (TextView) findViewById(R.id.logoheader);
        //Animation translatebu = AnimationUtils.loadAnimation(this,R.anim.animationfile);
        //tv.startAnimation(translatebu);
        try {
            Cursor loginc = db.rawQuery("SELECT * FROM logininfo", null);
            if (loginc.getCount() == 0) {
                //showMessage("Error", "No records found");
            }
            //pref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            //String username = pref.getString(USERNAME, null);
            ConnectivityManager cn = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nf = cn.getActiveNetworkInfo();
            if (nf != null && nf.isConnected() == true) {
                if (dbdevice == null) {
                    name = android.os.Build.MODEL;
                    platform = "Android";
//                    TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//                    uuid = tManager.getDeviceId();
                    uuid = Settings.Secure.getString(
                            SplashScreenActivity.this.getContentResolver(),
                            Settings.Secure.ANDROID_ID);
					/*uuid = Secure.getString(SplashScreenActivity.this.getContentResolver(),
			                Secure.ANDROID_ID);*/
                    version = android.os.Build.VERSION.RELEASE;
                    // DisplayMetrics metrics =
                    // context.getResources().getDisplayMetrics();
                    DisplayMetrics metrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    int width1 = metrics.widthPixels;
                    width = String.valueOf(width1);
                    int height1 = metrics.heightPixels;
                    height = String.valueOf(height1);
                    colordepth = "";
                    //new HttpAsyncTask().execute("http://www.mittalpower.com/mobile/pxs_app/service/generateaccesskey.php");
                    ConnectionDetector cd = new ConnectionDetector(SplashScreenActivity.this);
                    if (!cd.isConnectingToInternet()) {
                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        new HttpAsyncTask().execute("https://" + Constant.BASE_URL + Constant.REGISTERDEVICE);
                        //	new HttpAsyncTask().execute("http://www.powertradingsolutions.com" + Constant.REGISTERDEVICE);
                    }
                } else if (loginc.getCount() != 0) {
                    //StartAnimations();
					/*Thread background = new Thread() {
						public void run() {
							try {
								sleep(1 * 1000);
								//Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
								Intent in = new Intent(SplashScreenActivity.this, NewsFeedActivity.class);
								in.putExtra("LOGINCONFIG", "LOGIN");
								startActivity(in);
								overridePendingTransition(R.anim.animation, R.anim.animation2);
								finish();
							} catch (Exception e) {
							}
						}
					};
					background.start();*/
					/*final ProgressDialog ringProgressDialog = ProgressDialog
							.show(SplashScreenActivity.this, "Please wait ...",
									"Loading ...", true);
					ringProgressDialog.setCancelable(true);*/
                    prgDialog = new Dialog(this);
                    prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
                    prgDialog.setContentView(R.layout.progessdialog);
                    ImageView imagea = (ImageView) prgDialog.findViewById(R.id.img);
                    Animation animationprogess = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
                    imagea.startAnimation(animationprogess);
                    prgDialog.show();
                    prgDialog.setCancelable(false);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1 * 1000);
                                Intent in = new Intent(SplashScreenActivity.this, NewFeedMainActivity.class);
                                in.putExtra("LOGINCONFIG", "LOGIN");
                                in.putExtra("revision", "revision");
                                in.putExtra("date", "date");
                                in.putExtra("isFirst", true);
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            } catch (Exception e) {
                            }
                            prgDialog.dismiss();
                        }
                    }).start();
                } else {
                    //StartAnimations();
					/*Thread background = new Thread() {
						public void run() {
							try {

								sleep(1 * 1000);
								Intent intent = new Intent();
								intent.setClass(SplashScreenActivity.this,
										NewsFeedActivity.class);
								intent.putExtra("LOGINCONFIG", "LOGIN");
								SplashScreenActivity.this.startActivity(intent);
								SplashScreenActivity.this.finish();
								overridePendingTransition(
										R.anim.activityfadein,
										R.anim.splashfadeout);
								overridePendingTransition(R.anim.animation, R.anim.animation2);
								// transition from splash to main menu
								// overridePendingTransition(R.anim.animation,R.anim.animation2);

							} catch (Exception e) {
							}
						}
					};
					background.start();*/
					/*final ProgressDialog ringProgressDialog = ProgressDialog
							.show(SplashScreenActivity.this, "Please wait ...",
									"Loading ...", true);
					ringProgressDialog.setCancelable(true);*/
                    prgDialog = new Dialog(this);
                    prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
                    prgDialog.setContentView(R.layout.progessdialog);
                    ImageView imagea = (ImageView) prgDialog.findViewById(R.id.img);
                    Animation animationprogess = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
                    imagea.startAnimation(animationprogess);
                    prgDialog.show();
                    prgDialog.setCancelable(false);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                // Here you should write your time consuming
                                // task...
                                // Let the progress ring for 10 seconds...
                                Thread.sleep(1 * 1000);
                                Intent in = new Intent(SplashScreenActivity.this, NewFeedMainActivity.class);
                                in.putExtra("LOGINCONFIG", "LOGIN");
                                in.putExtra("revision", "revision");
                                in.putExtra("date", "date");
                                startActivity(in);
                                overridePendingTransition(R.anim.animation, R.anim.animation2);
                                finish();
                            } catch (Exception e) {
                            }
                            prgDialog.dismiss();
                        }
                    }).start();
                }
            } else {
                Toast.makeText(
                        this,
                        "Network Not Available, Please Check Internet Connectivity!!!",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
        }
    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l = (LinearLayout) findViewById(R.id.mainRel);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        TextView iv = (TextView) findViewById(R.id.tvspalshlogo);
        iv.clearAnimation();
        iv.startAnimation(anim);
    }

    public void showMessage(String title, String message) {
        Builder builder = new Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    public void callNotification() {
        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        //db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
        // db.execSQL("DROP TABLE IF EXISTS pushnotificationUnRead");
        Cursor c = db.rawQuery("SELECT * FROM pushnotification", null);
        //	goMessage.setText("All Notification:" + c.getCount());
        if (c.getCount() == 0) {
            //showMessage("Error", "No Notification found");
            //Toast.makeText(SplashScreenActivity.this, "No Notification found", Toast.LENGTH_LONG).show();
        }
        StringBuffer buffer = new StringBuffer();
        array = new String[c.getCount()];
        arraydispalydate = new String[c.getCount()];
        arraydispalytime = new String[c.getCount()];
        int i = 0;
        while (c.moveToNext()) {
            buffer.append("dbacess: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
            displaydate = c.getString(1);
            displaytime = c.getString(2);
            //goMessage.setText("All Notification:" + dbacess);
            array[i] = dbacess;
            arraydispalydate[i] = displaydate;
            arraydispalytime[i] = displaytime;
            i++;
            Log.d("dbacess: ", dbacess);
            Log.d("displaydateandtime: ", displaydate);
            Log.d("displaydateandtime: ", displaytime);
            //Toast.makeText(getActivity(), dbacess , Toast.LENGTH_LONG).show();
        }

        List<String> list = Arrays.asList(array);
        Collections.reverse(list);
        array = (String[]) list.toArray();

        List<String> displaydatelist = Arrays.asList(arraydispalydate);
        Collections.reverse(displaydatelist);
        arraydispalydate = (String[]) displaydatelist.toArray();

        List<String> displaytimelist = Arrays.asList(arraydispalytime);
        Collections.reverse(displaytimelist);
        arraydispalytime = (String[]) displaytimelist.toArray();

        // NotificationCustomList adapter = new NotificationCustomList(SplashScreenActivity.this, array, arraydispalydate, arraydispalytime);
        // notificationlist.setAdapter(adapter);

    }

//		@Override
//		public void onDestroy() {
//			if (mRegisterTask != null) {
//				mRegisterTask.cancel(true);
//			}
//			try {
//			//	getAgetunregisterReceiver(mHandleMessageReceiver);
//				unregisterReceiver(mHandleMessageReceiver);
//				GCMRegistrar.onDestroy(SplashScreenActivity.this);
//			} catch (Exception e) {
//				Log.e("UnRegister Receiver Error", "> " + e.getMessage());
//			}
//			super.onDestroy();
//		}

    @Override
    public void onDestroy() {

        try {
            if (mHandleMessageReceiver != null)
                unregisterReceiver(mHandleMessageReceiver);

        } catch (Exception e) {
        }

        super.onDestroy();
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            DeviceRegister deviceregister = new DeviceRegister();
            deviceregister.setName(name);
            deviceregister.setPlatform(platform);
            deviceregister.setUuid(uuid);
            deviceregister.setVersion(version);
            deviceregister.setWidth(width);
            deviceregister.setHeight(height);
            deviceregister.setColordepth(colordepth);
            return SplashScreenAPIResponse.POST(urls[0], deviceregister);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            //Toast.makeText(SplashScreenActivity.this, "result" + result, Toast.LENGTH_LONG).show();
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    String message = mainObject.getString("message");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Intent in = new Intent(SplashScreenActivity.this, NewFeedMainActivity.class);
                        in.putExtra("device_id", uuid);
                        in.putExtra("LOGINCONFIG", "LOGIN");
                        in.putExtra("revision", "revision");
                        in.putExtra("date", "date");
                        db.execSQL("INSERT INTO device VALUES('" + uuid + "')");
                        startActivity(in);
                        overridePendingTransition(R.anim.animation, R.anim.animation2);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        Intent in = new Intent(SplashScreenActivity.this, NewFeedMainActivity.class);
                        in.putExtra("device_id", uuid);
                        in.putExtra("LOGINCONFIG", "LOGIN");
                        in.putExtra("revision", "revision");
                        in.putExtra("date", "date");
                        db.execSQL("INSERT INTO device VALUES('" + uuid + "')");
                        startActivity(in);
                        overridePendingTransition(R.anim.animation, R.anim.animation2);
                        finish();
                    }
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }


}