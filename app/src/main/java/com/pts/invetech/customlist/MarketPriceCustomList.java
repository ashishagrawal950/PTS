package com.pts.invetech.customlist;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

public class MarketPriceCustomList extends ArrayAdapter<String> {

	private  Activity context;
	private  String[] dateIEX;
	private  double[] minIEX;
	private  double[] avgIEX;
	private  double[] maxIEX;
	private  double[] minPXIL;
	private  double[] avgPXIL;
	private  double[] maxPXIL;

	public MarketPriceCustomList(Activity context, String[] dateIEX,
			double[] minIEX, double[] avgIEX, double[] maxIEX, double[] minPXIL,
			double[] avgPXIL, double[] maxPXIL ) {
		// TODO Auto-generated constructor stub
		super(context, R.layout.marketprice_single_bid, dateIEX);
		this.context = context;
		this.dateIEX = dateIEX;
		this.minIEX = minIEX;
		this.avgIEX = avgIEX;
		this.maxIEX = maxIEX;
		this.minPXIL = minPXIL;
		this.avgPXIL = avgPXIL;
		this.maxPXIL = maxPXIL;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.marketprice_single_bid, null, true);
		TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
		TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
		TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
		TextView txt4 = (TextView) rowView.findViewById(R.id.txt4);
		TextView txt5 = (TextView) rowView.findViewById(R.id.txt5);
		TextView txt6 = (TextView) rowView.findViewById(R.id.txt6);
		TextView txt7 = (TextView) rowView.findViewById(R.id.txt7);
		txt1.setText(dateIEX[position]);
		txt2.setText(String.valueOf(minIEX[position]));
		txt3.setText(String.valueOf(avgIEX[position]));
		txt4.setText(String.valueOf(maxIEX[position]));
		txt5.setText(String.valueOf(minPXIL[position]));
		txt6.setText(String.valueOf(avgPXIL[position]));
		txt7.setText(String.valueOf(maxPXIL[position]));
		return rowView;
	}
}
