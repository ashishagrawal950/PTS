package com.pts.invetech.classes.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.NoBidDeleteAPIResponse;
import com.pts.invetech.apiresponse.NoBidGetAllDataAPIResponse;
import com.pts.invetech.apiresponse.NoBidSaveAPIResponse;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.customlist.NoBidCustomList;
import com.pts.invetech.customlist.NoBidEscertsCustomList;
import com.pts.invetech.customlist.NoBidRECCustomList;
import com.pts.invetech.pojo.Login;
import com.pts.invetech.pojo.NoBidDelete;
import com.pts.invetech.pojo.NoBidGetAllDetails;
import com.pts.invetech.pojo.NoBidSave;
import com.pts.invetech.utils.JSONUtils;
import com.pts.invetech.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class NoBidActivity extends Activity implements OnItemSelectedListener {

    private ArrayAdapter<String> adapter;
    private Context context;
    private Dialog prgDialog;
    private LinearLayout mopenLayout, nobidlistheading;
    private ListView list;
    static TextView fromDateEtxt, etxt_fromdate_set, tvnobidnull, tvnobidnulltext;
    static String as, monthplace, dayplace;
    private Button btnobiddate;
    private HashMap<Integer, NoBidGetAllDetails> myHashmap;
    // String[] a;
    private String iddelete, datedelete;
    private SQLiteDatabase db;
    private String dbacess;
    private ArrayList<String> serialnumber = new ArrayList<String>();
    private ArrayList<String> nobidid = new ArrayList<String>();
    private ArrayList<String> nobidclientid = new ArrayList<String>();
    private ArrayList<String> nobiddate = new ArrayList<String>();
    private ArrayList<String> nobiddateshow = new ArrayList<String>();

    ////REC NO BID
    private Spinner etxt_fromrecnobiddate;
    private ArrayList<String> recdate_array = new ArrayList<String>();
    private ArrayList<String> recprintdate_array = new ArrayList<String>();
    private String recnodbiddatesend;
    private ArrayAdapter<String> adapter_date_nobid;
    private ArrayList<String> serialnumberrec = new ArrayList<String>();
    private ArrayList<String> nobididrec = new ArrayList<String>();
    private ArrayList<String> nobidclientidrec = new ArrayList<String>();
    private ArrayList<String> nobiddaterec = new ArrayList<String>();
    private ArrayList<String> nobiddateshowrec = new ArrayList<String>();
    private ListView nobidreclist;
    private Button btrecnobiddate;
    private String recnobididdelete, recnobiddatedelete;
    private TextView tvrecnobidnull, tvrecnobidnulltext;
    private LinearLayout nobidlistheadingrec;

    // Escerts
    private Spinner etxt_fromescertsnobiddate;
    private ArrayList<String> escertsdate_array = new ArrayList<String>();
    private ArrayList<String> escertsprintdate_array = new ArrayList<String>();
    private ArrayAdapter<String> adapter_date_escertsnobid;
    private ArrayList<String> serialnumberescerts = new ArrayList<String>();
    private ArrayList<String> nobididescerts = new ArrayList<String>();
    private ArrayList<String> nobidclientidescerts = new ArrayList<String>();
    private ArrayList<String> nobiddateescerts = new ArrayList<String>();
    private ArrayList<String> nobiddateshowescerts = new ArrayList<String>();
    private TextView tvescertsnobidnull, tvescertsnobidnulltext;
    private LinearLayout nobidlistheadingescerts;
    private ListView nobidescertslist;
    private String escertsnobididdelete, escertsnobiddatedelete;
    private Button btescertsnobiddate;
    private String escertsid, escertsclientid, escertsdate, escertststatus;
    private String escertsnodbiddatesend, domain;
    private TextView etxt_fromescertsnoddate_set;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE); // for disable screenshot of app.
        setContentView(R.layout.activity_nobid);

        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM baby", null);
        if (c.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("access_key: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
            //buffer.append("access_key: "+c.getString(0)+"\n");
            //Toast.makeText(getApplicationContext(), "access_key" + c.getColumnIndex("access_key"), Toast.LENGTH_LONG).show();
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
		/* prgDialog = new ProgressDialog(NoBidActivity.this);
	     prgDialog.setMessage("Please wait...");
	     prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(this);
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        fromDateEtxt = (TextView) findViewById(R.id.etxt_fromdate);
        etxt_fromdate_set = (TextView) findViewById(R.id.etxt_fromdate_set);
        tvnobidnull = (TextView) findViewById(R.id.tvnobidnull);
        tvnobidnulltext = (TextView) findViewById(R.id.tvnobidnulltext);
        nobidlistheading = (LinearLayout) findViewById(R.id.nobidlistheading);
        list = (ListView) findViewById(R.id.nobidlist);


        final Animation myAnimation = AnimationUtils.loadAnimation(this, R.anim.blink);
        final Button dam = (Button) findViewById(R.id.tab_DAM);
        final Button damTwo = (Button) findViewById(R.id.tab_DAM2);

        final Button tam = (Button) findViewById(R.id.tab_TAM);
        final Button tamTwo = (Button) findViewById(R.id.tab_TAM2);

        final Button rec = (Button) findViewById(R.id.tab_REC);
        final Button recTwo = (Button) findViewById(R.id.tab_REC2);

        final Button tab_Escerts = (Button) findViewById(R.id.tab_Escerts);
        final Button tab_EscertsTwo = (Button) findViewById(R.id.tab_Escerts2);

        final ScrollView damscollview = (ScrollView) findViewById(R.id.damscollview);
        final ScrollView recnobidtabscroolview = (ScrollView) findViewById(R.id.recnobidtabscroolview);
        final ScrollView escertsnobidtabscroolview = (ScrollView) findViewById(R.id.escertsnobidtabscroolview);


        damTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dam.setVisibility(View.VISIBLE);
                damTwo.setVisibility(View.GONE);
                // tam.setVisibility(View.GONE);
                //  tamTwo.setVisibility(View.VISIBLE);
                rec.setVisibility(View.GONE);
                recTwo.setVisibility(View.VISIBLE);

                dam.startAnimation(myAnimation);
                recnobidtabscroolview.setVisibility(View.GONE);
                damscollview.setVisibility(View.VISIBLE);
                escertsnobidtabscroolview.setVisibility(View.GONE);
            }
        });

        tamTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dam.setVisibility(View.GONE);
                damTwo.setVisibility(View.VISIBLE);
                // tam.setVisibility(View.VISIBLE);
                // tamTwo.setVisibility(View.GONE);
                rec.setVisibility(View.GONE);
                recTwo.setVisibility(View.VISIBLE);

                tab_Escerts.setVisibility(View.GONE);
                tab_EscertsTwo.setVisibility(View.VISIBLE);

                tam.startAnimation(myAnimation);
            }
        });

        recTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dam.setVisibility(View.GONE);
                damTwo.setVisibility(View.VISIBLE);
                // tam.setVisibility(View.GONE);
                // tamTwo.setVisibility(View.VISIBLE);
                rec.setVisibility(View.VISIBLE);
                recTwo.setVisibility(View.GONE);

                tab_Escerts.setVisibility(View.GONE);
                tab_EscertsTwo.setVisibility(View.VISIBLE);

                rec.startAnimation(myAnimation);
                recnobidtabscroolview.setVisibility(View.VISIBLE);
                damscollview.setVisibility(View.GONE);
                escertsnobidtabscroolview.setVisibility(View.GONE);
                new HttpAsyncTaskRECgetallrectradingdate().execute(domain + "/mobile/pxs_app/service/rec/getallrectradingdate.php");
            }
        });


        tab_EscertsTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dam.setVisibility(View.GONE);
                damTwo.setVisibility(View.VISIBLE);
                // tam.setVisibility(View.GONE);
                // tamTwo.setVisibility(View.VISIBLE);
                rec.setVisibility(View.GONE);
                recTwo.setVisibility(View.VISIBLE);

                tab_Escerts.setVisibility(View.VISIBLE);
                tab_EscertsTwo.setVisibility(View.GONE);

                tab_EscertsTwo.startAnimation(myAnimation);

                recnobidtabscroolview.setVisibility(View.GONE);
                damscollview.setVisibility(View.GONE);
                escertsnobidtabscroolview.setVisibility(View.VISIBLE);
                new HttpAsyncTaskESCERTgetallescerttradingdate().execute(domain + "/mobile/pxs_app/service/escert/getallescerttradingdate.php");
            }
        });

        //// REC NO BID 
        etxt_fromrecnobiddate = (Spinner) findViewById(R.id.etxt_fromrecnobiddate);
        etxt_fromrecnobiddate.setOnItemSelectedListener(this);

        tvrecnobidnull = (TextView) findViewById(R.id.tvrecnobidnull);
        tvrecnobidnulltext = (TextView) findViewById(R.id.tvrecnobidnulltext);
        nobidlistheadingrec = (LinearLayout) findViewById(R.id.nobidlistheadingrec);
        nobidreclist = (ListView) findViewById(R.id.nobidreclist);

        btrecnobiddate = (Button) findViewById(R.id.btrecnobiddate);
        btrecnobiddate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (recnodbiddatesend.equalsIgnoreCase("Select Trading Date")) {
                    Toast.makeText(getApplicationContext(),
                            "Please Select Date", Toast.LENGTH_LONG).show();
                } else {
                    new HttpAsyncTaskRECNoBidGo()
                            .execute(domain + "/mobile/pxs_app/service/rec/nobid/saverecnobid.php");
                }
            }

        });


        ///////// ESCERTS NO BID
        etxt_fromescertsnobiddate = (Spinner) findViewById(R.id.etxt_fromescertsnobiddate);
        etxt_fromescertsnobiddate.setOnItemSelectedListener(this);
        etxt_fromescertsnoddate_set = (TextView) findViewById(R.id.etxt_fromescertsnoddate_set);
        tvescertsnobidnull = (TextView) findViewById(R.id.tvescertsnobidnull);
        tvescertsnobidnulltext = (TextView) findViewById(R.id.tvescertsnobidnulltext);
        nobidlistheadingescerts = (LinearLayout) findViewById(R.id.nobidlistheadingescerts);
        nobidescertslist = (ListView) findViewById(R.id.nobidescertslist);
        btescertsnobiddate = (Button) findViewById(R.id.btescertsnobiddate);

        btescertsnobiddate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (escertsnodbiddatesend.equalsIgnoreCase("Select Trading Date")) {
                    Toast.makeText(getApplicationContext(),
                            "Please Select Date", Toast.LENGTH_LONG).show();
                } else {
                    new HttpAsyncTaskESCERTSNoBidGo()
                            .execute(domain + "/mobile/pxs_app/service/escert/nobid/saveescertnobid.php");
                }
            }

        });

		/*etxt_fromescertsnoddate_set.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//DialogFragment newFragment = new SelectDateFragmentEscerts();
				//newFragment.show(getFragmentManager(), "DatePicker");
				Calendar now = Calendar.getInstance();
				DatePickerDialog dpd = DatePickerDialog.newInstance(
						DatePickerFragment.this,
						now.get(Calendar.YEAR),
						now.get(Calendar.MONTH),
						now.get(Calendar.DAY_OF_MONTH)
				);
			}
		});*/


        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        mopenLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(NoBidActivity.this, MainActivityAfterLogin.class);
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        fromDateEtxt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");

            }
        });

        btnobiddate = (Button) findViewById(R.id.btnobiddate);
        btnobiddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (as == null) {
                    Toast.makeText(NoBidActivity.this, "Please Select No bid Date.", Toast.LENGTH_LONG).show();
                } else {
                    //as = tvtradingdate.getText().toString();
                    //Toast.makeText(getActivity(), "DownloadResult" + as, Toast.LENGTH_LONG).show();
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            NoBidActivity.this);
                    alertDialogBuilder
                            .setMessage("Are you sure you want to place NoBid?");
                    alertDialogBuilder.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    ConnectionDetector cd = new ConnectionDetector(NoBidActivity.this);
                                    if (!cd.isConnectingToInternet()) {
                                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                    } else {
                                        new HttpAsyncTaskNoBidGo()
                                                .execute(domain + "/mobile/pxs_app/service/nobid/save_nobid.php");
                                    }
                                }
                            });

                    alertDialogBuilder.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    // finish();
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }
            }
        });
        ConnectionDetector cd = new ConnectionDetector(NoBidActivity.this);
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            new HttpAsyncTaskNoBidGetAllData().execute(domain + "/mobile/pxs_app/service/nobid/get_nobid.php");
            //	openPopupNobid = (LinearLayout) rootView.findViewById(R.id.openPopupNobid);
            //	logoutpopup();
        }
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinnobid = (Spinner) parent;
        Spinner spinescertsnobid = (Spinner) parent;

        if (spinnobid.getId() == R.id.etxt_fromrecnobiddate) {
            recnodbiddatesend = recdate_array.get(position);
            //Toast.makeText(NewBidActivity.this, recplacedbiddatesend, Toast.LENGTH_LONG).show();
        }

        if (spinescertsnobid.getId() == R.id.etxt_fromescertsnobiddate) {
            escertsnodbiddatesend = escertsdate_array.get(position);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onBackPressed() {
        //Display alert message when back button has been pressed
        Intent in = new Intent(NoBidActivity.this, MainActivityAfterLogin.class);
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
        return;
    }

    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            //fromDateEtxt.setText(month + "/" + day + "/" + year);
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                //fromDateEtxt.setText(year + "-" + month + "-" + day);
                fromDateEtxt.setText(dayplace + "-" + monthplace + "-" + year);
                etxt_fromdate_set.setText(year + "-" + monthplace + "-" + dayplace);
                as = etxt_fromdate_set.getText().toString();
                //  Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
            } else if (month <= 9) {
                String monthplace = "0" + month;
                fromDateEtxt.setText(day + "-" + monthplace + "-" + year);
                etxt_fromdate_set.setText(year + "-" + monthplace + "-" + day);
                as = etxt_fromdate_set.getText().toString();
            } else if (day < 10) {
                String dayplace = "0" + day;
                fromDateEtxt.setText(dayplace + "-" + month + "-" + year);
                etxt_fromdate_set.setText(year + "-" + month + "-" + dayplace);
                as = etxt_fromdate_set.getText().toString();
            } else {
                fromDateEtxt.setText(day + "-" + month + "-" + year);
                etxt_fromdate_set.setText(year + "-" + month + "-" + day);
                as = etxt_fromdate_set.getText().toString();
                //Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
                //(month + "/" + day + "/" + year);
                //(year + "-" + month + "-" + day);
            }


        }

    }

    // REC NO BID API CALL

    private class HttpAsyncTaskNoBidGetAllData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Login login = new Login();
            login.setAccess_key(dbacess);
            return NoBidGetAllDataAPIResponse.POST(urls[0], login);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                //Toast.makeText(getActivity(), "NoBidGetAllData" + result,Toast.LENGTH_LONG).show();
                serialnumber.clear();
                nobidid.clear();
                nobidclientid.clear();
                nobiddate.clear();
                nobiddateshow.clear();
                try {
                    JSONArray jsonarray = new JSONArray(result);
                    if (jsonarray.isNull(0)) {
                        //Toast.makeText(getActivity(), "Data is empty." ,Toast.LENGTH_LONG).show();
                        tvnobidnull.setBackgroundResource(R.drawable.sad);
                        tvnobidnulltext.setText("You haven't placed no bid yet.");
                        tvnobidnull.setVisibility(View.VISIBLE);
                        tvnobidnulltext.setVisibility(View.VISIBLE);
                        nobidlistheading.setVisibility(View.GONE);
                        list.setVisibility(View.GONE);
                    } else {
                        nobidlistheading.setVisibility(View.VISIBLE);
                        list.setVisibility(View.VISIBLE);
                        tvnobidnull.setVisibility(View.GONE);
                        tvnobidnulltext.setVisibility(View.GONE);
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject obj = jsonarray.getJSONObject(i);
                            String id = obj.getString("id");
                            String clientid = obj.getString("clientid");
                            String date = obj.getString("date");
                            String[] parts = date.split("-");
                            String datepartone = parts[0];
                            String dateparttwo = parts[1];
                            String datepartthree = parts[2];
                            String dateshow = (datepartthree + "-" + dateparttwo + "-" + datepartone);
                            int counting = (i + 1);
                            String j = String.valueOf(counting);
				       /* mypojo = new NoBidGetAllDetails(j, id,clientid,date, dateshow);
					    myHashmap.put(i, mypojo);
				       // Toast.makeText(getActivity(), "id" + id ,Toast.LENGTH_LONG).show();
				       // Toast.makeText(getActivity(), "clientid" + clientid ,Toast.LENGTH_LONG).show();
				      //  Toast.makeText(getActivity(), "date" + date ,Toast.LENGTH_LONG).show();

				         nobidid = new ArrayList<String>();
						 nobidclientid = new ArrayList<String>();
						 nobiddate = new ArrayList<String>();
						 nobiddateshow = new ArrayList<String>();
						Collection<NoBidGetAllDetails> c = myHashmap.values();
						Iterator<NoBidGetAllDetails> itr = c.iterator();
						while (itr.hasNext())
						{
							NoBidGetAllDetails value = (NoBidGetAllDetails) itr.next();
							serialnumber.add(value.getJ());
							nobidid.add(value.getId());
							nobidclientid.add(value.getClientid());
							nobiddate.add(value.getDate());
							nobiddateshow.add(value.getDateshow());
						}*/
                            serialnumber.add(j);
                            nobidid.add(id);
                            nobidclientid.add(clientid);
                            nobiddate.add(date);
                            nobiddateshow.add(dateshow);
                        }
                        //Collections.sort(serialnumber);
					/*serialnumberadapter = serialnumber.toArray(new String[serialnumber.size()]);
					Integer[] intarray=new Integer[serialnumberadapter.length];
				    int i=0;
				    for(String str:serialnumberadapter){
				        intarray[i]=Integer.parseInt(str.trim());//Exception in this line
				        i++;
				    }
				    Arrays.sort(intarray);
				    a=Arrays.toString(intarray).split("[\\[\\]]")[1].split(", ");
				    System.out.println(Arrays.toString(a));

				    idadapter = nobidid.toArray(new String[nobidid.size()]);
					String[] clientidadapter = nobidclientid.toArray(new String[nobidclientid.size()]);
					dateadapter = nobiddate.toArray(new String[nobiddate.size()]);
					dateshowadapter  = nobiddateshow.toArray(new String[nobiddateshow.size()]);*/

                        NoBidCustomList adapter = new NoBidCustomList(NoBidActivity.this, nobiddateshow, serialnumber, nobidclientid);
                        list.setAdapter(adapter);
                        Utility.setListViewHeightBasedOnChildren(list);
                        //myHashmap.clear();

                        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view,
                                                    final int position, long id) {
                                //Toast.makeText(getActivity(),"You Clicked at " + idadapter[+position],Toast.LENGTH_SHORT).show();
                                //Toast.makeText(getActivity(),"You Clicked at " + dateadapter[+position],Toast.LENGTH_SHORT).show();
                                //String indexid = String.valueOf(position);
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                        NoBidActivity.this);
                                alertDialogBuilder
                                        .setMessage("Are you sure you want to delete?");
                                alertDialogBuilder.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                iddelete = nobidid.get(position);
                                                datedelete = nobiddate.get(position);
                                                //	Toast.makeText(getActivity(),"You Clicked at " + iddelete,Toast.LENGTH_SHORT).show();
                                                //	Toast.makeText(getActivity(),"You Clicked at " + datedelete,Toast.LENGTH_SHORT).show();
                                                ConnectionDetector cd = new ConnectionDetector(NoBidActivity.this);
                                                if (!cd.isConnectingToInternet()) {
                                                    Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                                } else {
                                                    new HttpAsyncTaskNoBidDelete().execute(domain + "/mobile/pxs_app/service/nobid/delete_nobid.php");
                                                }
                                            }
                                        });


                                alertDialogBuilder.setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog,
                                                                int which) {
                                                // finish();
                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }
                        });


                    }
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
            } else {
                //Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

        }

    }

    private class HttpAsyncTaskNoBidGo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            NoBidSave noBidSave = new NoBidSave();
            noBidSave.setDate(as);
            noBidSave.setAccess_key(dbacess);
            return NoBidSaveAPIResponse.POST(urls[0], noBidSave);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            //  Toast.makeText(getActivity(), "NoBidSave" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    String value = mainObject.getString("value");
                    String message = mainObject.getString("message");
                    if (status == "SUCCESS") {
                        Toast.makeText(NoBidActivity.this, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(NoBidActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                //Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

            ConnectionDetector cd = new ConnectionDetector(NoBidActivity.this);
            if (!cd.isConnectingToInternet()) {
                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
            } else {

                new HttpAsyncTaskNoBidGetAllData().execute(domain + "/mobile/pxs_app/service/nobid/get_nobid.php");
            }
        }
    }

    private class HttpAsyncTaskNoBidDelete extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            NoBidDelete noBidDelete = new NoBidDelete();
            //iddelete, datedelete
            noBidDelete.setId(iddelete);
            noBidDelete.setDate(datedelete);
            noBidDelete.setAccess_key(dbacess);
            return NoBidDeleteAPIResponse.POST(urls[0], noBidDelete);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("Get Login Result:", result);
            prgDialog.hide();
            //  Toast.makeText(getActivity(), "NoBidSave" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    String value = mainObject.getString("value");
                    String message = mainObject.getString("message");
                    if (status == "SUCCESS") {
                        Toast.makeText(NoBidActivity.this, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(NoBidActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        // TODO Auto-generated catch block
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

            ConnectionDetector cd = new ConnectionDetector(NoBidActivity.this);
            if (!cd.isConnectingToInternet()) {
                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
            } else {
                new HttpAsyncTaskNoBidGetAllData().execute(domain + "/mobile/pxs_app/service/nobid/get_nobid.php");
            }
        }
    }

    private class HttpAsyncTaskRECgetallrectradingdate extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Login login = new Login();
            login.setAccess_key(dbacess);
            return NoBidGetAllDataAPIResponse.POST(urls[0], login);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                //Toast.makeText(getActivity(), "NoBidGetAllData" + result,Toast.LENGTH_LONG).show();
                try {
                    recdate_array.clear();
                    recprintdate_array.clear();
                    JSONArray jsonarray = new JSONArray(result);
                    recdate_array.add("Select Trading Date");
                    recprintdate_array.add("Select Trading Date");
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonObject = jsonarray.getJSONObject(i);
                        if (jsonObject.has("date")) {
                            String date = jsonObject.getString("date");
                            recdate_array.add(date);
                        }
                        if (jsonObject.has("printDate")) {
                            String printDate = jsonObject.getString("printDate");
                            recprintdate_array.add(printDate);
                        }
                    }

                    adapter_date_nobid = new ArrayAdapter<String>(
                            NoBidActivity.this,
                            R.layout.spinner_item,
                            recprintdate_array);
                    adapter_date_nobid
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    etxt_fromrecnobiddate.setAdapter(adapter_date_nobid);


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Invalid JSON: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskRECNoBidGetAllData().execute(domain + "/mobile/pxs_app/service/rec/nobid/getrecdatedetails.php");
        }

    }

    // ESCERT NO BID API CALL

    private class HttpAsyncTaskRECNoBidGetAllData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Login login = new Login();
            login.setAccess_key(dbacess);
            return NoBidGetAllDataAPIResponse.POST(urls[0], login);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                //Toast.makeText(getActivity(), "NoBidGetAllData" + result,Toast.LENGTH_LONG).show();
                serialnumberrec.clear();
                nobididrec.clear();
                nobidclientidrec.clear();
                nobiddaterec.clear();
                nobiddateshowrec.clear();
                try {
                    JSONArray jsonarray = new JSONArray(result);
                    if (jsonarray.isNull(0)) {
                        //Toast.makeText(getActivity(), "Data is empty." ,Toast.LENGTH_LONG).show();
                        tvrecnobidnull.setBackgroundResource(R.drawable.sad);
                        tvrecnobidnulltext.setText("You haven't placed no bid yet.");
                        tvrecnobidnull.setVisibility(View.VISIBLE);
                        tvrecnobidnulltext.setVisibility(View.VISIBLE);
                        nobidlistheadingrec.setVisibility(View.GONE);
                        nobidreclist.setVisibility(View.GONE);
                    } else {
                        nobidlistheadingrec.setVisibility(View.VISIBLE);
                        nobidreclist.setVisibility(View.VISIBLE);
                        tvrecnobidnull.setVisibility(View.GONE);
                        tvrecnobidnulltext.setVisibility(View.GONE);
                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject obj = jsonarray.getJSONObject(i);
                            String id = obj.getString("id");
                            String clientid = obj.getString("clientid");
                            String printdate = obj.getString("printdate");
                            String date = obj.getString("date");
                            //String[] parts = date.split("-");
                            // String datepartone = parts[0];
                            // String dateparttwo = parts[1];
                            // String datepartthree = parts[2];
                            //  String dateshow = (datepartthree+"-"+dateparttwo+"-"+datepartone);
                            int counting = (i + 1);
                            String j = String.valueOf(counting);

                            serialnumberrec.add(j);
                            nobididrec.add(id);
                            nobidclientidrec.add(clientid);
                            nobiddaterec.add(date);
                            nobiddateshowrec.add(printdate);
                        }


                        NoBidRECCustomList adapter = new NoBidRECCustomList(NoBidActivity.this, nobiddateshowrec, serialnumberrec, nobidclientidrec);

                        nobidreclist.setAdapter(adapter);
                        Utility.setListViewHeightBasedOnChildren(nobidreclist);

                        nobidreclist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view,
                                                    final int position, long id) {
                                //Toast.makeText(getActivity(),"You Clicked at " + idadapter[+position],Toast.LENGTH_SHORT).show();
                                //Toast.makeText(getActivity(),"You Clicked at " + dateadapter[+position],Toast.LENGTH_SHORT).show();
                                //String indexid = String.valueOf(position);
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                        NoBidActivity.this);
                                alertDialogBuilder
                                        .setMessage("Are you sure you want to delete?");
                                alertDialogBuilder.setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                recnobididdelete = nobididrec.get(position);
                                                recnobiddatedelete = nobiddaterec.get(position);
                                                //	Toast.makeText(getActivity(),"You Clicked at " + iddelete,Toast.LENGTH_SHORT).show();
                                                //	Toast.makeText(getActivity(),"You Clicked at " + datedelete,Toast.LENGTH_SHORT).show();
                                                ConnectionDetector cd = new ConnectionDetector(NoBidActivity.this);
                                                if (!cd.isConnectingToInternet()) {
                                                    Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                                } else {
                                                    new HttpAsyncTaskNoBidRECDelete().execute(domain + "/mobile/pxs_app/service/rec/nobid/deletenobid.php");
                                                }
                                            }
                                        });

                                alertDialogBuilder.setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog,
                                                                int which) {
                                                // finish();
                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

        }

    }

    private class HttpAsyncTaskRECNoBidGo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            NoBidSave noBidSave = new NoBidSave();
            noBidSave.setDate(recnodbiddatesend);
            noBidSave.setAccess_key(dbacess);
            return NoBidSaveAPIResponse.POST(urls[0], noBidSave);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //  Toast.makeText(getActivity(), "NoBidSave" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    //String value = mainObject.getString("value");
                    String message = mainObject.getString("msg");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(NoBidActivity.this, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(NoBidActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }


            new HttpAsyncTaskRECNoBidGetAllData().execute(domain + "/mobile/pxs_app/service/rec/nobid/getrecdatedetails.php");
        }
    }

    private class HttpAsyncTaskNoBidRECDelete extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            NoBidDelete noBidDelete = new NoBidDelete();
            //iddelete, datedelete
            noBidDelete.setId(recnobididdelete);
            noBidDelete.setDate(recnobiddatedelete);
            noBidDelete.setAccess_key(dbacess);
            return NoBidDeleteAPIResponse.POST(urls[0], noBidDelete);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //  Toast.makeText(getActivity(), "NoBidSave" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    //String value = mainObject.getString("value");
                    String message = mainObject.getString("msg");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(NoBidActivity.this, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(NoBidActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

            new HttpAsyncTaskRECNoBidGetAllData().execute(domain + "/mobile/pxs_app/service/rec/nobid/getrecdatedetails.php");
        }
    }

    private class HttpAsyncTaskESCERTgetallescerttradingdate extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Login login = new Login();
            login.setAccess_key(dbacess);
            return NoBidGetAllDataAPIResponse.POST(urls[0], login);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    escertsdate_array.clear();
                    escertsprintdate_array.clear();
                    JSONArray jsonarray = new JSONArray(result);
                    escertsdate_array.add("Select Trading Date");
                    escertsprintdate_array.add("Select Trading Date");
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject jsonObject = jsonarray.getJSONObject(i);
                        if (jsonObject.has("date")) {
                            String date = jsonObject.getString("date");
                            escertsdate_array.add(date);
                        }
                        if (jsonObject.has("printDate")) {
                            String printDate = jsonObject.getString("printDate");
                            escertsprintdate_array.add(printDate);
                        }
                    }

                    adapter_date_escertsnobid = new ArrayAdapter<String>(
                            NoBidActivity.this,
                            R.layout.spinner_item,
                            escertsprintdate_array);
                    adapter_date_escertsnobid
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    etxt_fromescertsnobiddate.setAdapter(adapter_date_escertsnobid);
                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    if (result != null) {
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Invalid JSON: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }

            new HttpAsyncTaskESCERTSNoBidGetAllData().execute(domain + "/mobile/pxs_app/service/escert/nobid/getescertdatedetails.php");
        }

    }

    private class HttpAsyncTaskESCERTSNoBidGetAllData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            Login login = new Login();
            login.setAccess_key(dbacess);
            return NoBidGetAllDataAPIResponse.POST(urls[0], login);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                serialnumberescerts.clear();
                nobididescerts.clear();
                nobidclientidescerts.clear();
                nobiddateescerts.clear();
                nobiddateshowescerts.clear();
                try {
                    JSONObject jsonobject = new JSONObject(result);
                    nobidlistheadingescerts.setVisibility(View.VISIBLE);
                    nobidescertslist.setVisibility(View.VISIBLE);
                    tvescertsnobidnull.setVisibility(View.GONE);
                    tvescertsnobidnulltext.setVisibility(View.GONE);
                    JSONArray jsonArraynobid = jsonobject.getJSONArray("NoBid");
                    if (jsonArraynobid.isNull(0)) {
                        //Toast.makeText(getActivity(), "Data is empty." ,Toast.LENGTH_LONG).show();
                        tvescertsnobidnull.setBackgroundResource(R.drawable.sad);
                        tvescertsnobidnulltext.setText("You haven't placed no bid yet.");
                        tvescertsnobidnull.setVisibility(View.VISIBLE);
                        tvescertsnobidnulltext.setVisibility(View.VISIBLE);
                        nobidlistheadingescerts.setVisibility(View.GONE);
                        nobidescertslist.setVisibility(View.GONE);
                    } else {
                        nobidlistheadingescerts.setVisibility(View.VISIBLE);
                        nobidescertslist.setVisibility(View.VISIBLE);
                        tvescertsnobidnull.setVisibility(View.GONE);
                        tvescertsnobidnulltext.setVisibility(View.GONE);
                        for (int i = 0; i < jsonArraynobid.length(); i++) {
                            JSONObject obj = jsonArraynobid.getJSONObject(i);
                            String id = obj.getString("id");
                            String clientid = obj.getString("clientid");
                            String date = obj.getString("date");
                            int counting = (i + 1);
                            String j = String.valueOf(counting);
                            serialnumberescerts.add(j);
                            nobididescerts.add(id);
                            nobidclientidescerts.add(clientid);
                            nobiddateescerts.add(date);
                        }
                        NoBidEscertsCustomList adapter = new NoBidEscertsCustomList(NoBidActivity.this, serialnumberescerts, nobiddateescerts);
                        nobidescertslist.setAdapter(adapter);
                        Utility.setListViewHeightBasedOnChildren(nobidescertslist);
                    }
                    nobidescertslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                    NoBidActivity.this);
                            alertDialogBuilder
                                    .setMessage("Are you sure you want to delete?");
                            alertDialogBuilder.setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            escertsnobididdelete = nobididescerts.get(position);
                                            escertsnobiddatedelete = nobiddateescerts.get(position);
                                            ConnectionDetector cd = new ConnectionDetector(NoBidActivity.this);
                                            if (!cd.isConnectingToInternet()) {
                                                Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                                            } else {
                                                new HttpAsyncTaskNoBidESCERTSDelete().execute(domain + "/mobile/pxs_app/service/escert/nobid/deletenobid.php");
                                            }
                                        }
                                    });
                            alertDialogBuilder.setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            // finish();
                                        }
                                    });
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                    });

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getApplicationContext(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "103 Error occured: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class HttpAsyncTaskNoBidESCERTSDelete extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            NoBidDelete noBidDelete = new NoBidDelete();
            noBidDelete.setId(escertsnobididdelete);
            noBidDelete.setDate(escertsnobiddatedelete);
            noBidDelete.setAccess_key(dbacess);
            return NoBidDeleteAPIResponse.POST(urls[0], noBidDelete);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //  Toast.makeText(getActivity(), "NoBidSave" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    //String value = mainObject.getString("value");
                    String message = mainObject.getString("message");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(NoBidActivity.this, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(NoBidActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskESCERTSNoBidGetAllData().execute(domain + "/mobile/pxs_app/service/escert/nobid/getescertdatedetails.php");
        }
    }

    private class HttpAsyncTaskESCERTSNoBidGo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            NoBidSave noBidSave = new NoBidSave();
            noBidSave.setDate(escertsnodbiddatesend);
            noBidSave.setAccess_key(dbacess);
            return NoBidSaveAPIResponse.POST(urls[0], noBidSave);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            prgDialog.hide();
            //  Toast.makeText(getActivity(), "NoBidSave" + result,Toast.LENGTH_LONG).show();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    JSONObject mainObject = new JSONObject(result);
                    String status = mainObject.getString("status");
                    //String value = mainObject.getString("value");
                    String message = mainObject.getString("message");
                    if (status.equalsIgnoreCase("SUCCESS")) {
                        Toast.makeText(NoBidActivity.this, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(NoBidActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Document html = Jsoup.parse(result);
                    String title = html.title();
                    Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    e.printStackTrace();
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getApplicationContext(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Response Invalid from Server: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            new HttpAsyncTaskESCERTSNoBidGetAllData().execute(domain + "/mobile/pxs_app/service/escert/nobid/getescertdatedetails.php");
        }
    }

    public class SelectDateFragmentEscerts extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            //fromDateEtxt.setText(month + "/" + day + "/" + year);
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                //fromDateEtxt.setText(year + "-" + month + "-" + day);
                fromDateEtxt.setText(dayplace + "-" + monthplace + "-" + year);
                etxt_fromdate_set.setText(year + "-" + monthplace + "-" + dayplace);
                as = etxt_fromdate_set.getText().toString();
                //  Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
            } else if (month <= 9) {
                String monthplace = "0" + month;
                fromDateEtxt.setText(day + "-" + monthplace + "-" + year);
                etxt_fromdate_set.setText(year + "-" + monthplace + "-" + day);
                as = etxt_fromdate_set.getText().toString();
            } else if (day < 10) {
                String dayplace = "0" + day;
                fromDateEtxt.setText(dayplace + "-" + month + "-" + year);
                etxt_fromdate_set.setText(year + "-" + month + "-" + dayplace);
                as = etxt_fromdate_set.getText().toString();
            } else {
                fromDateEtxt.setText(day + "-" + month + "-" + year);
                etxt_fromdate_set.setText(year + "-" + month + "-" + day);
                as = etxt_fromdate_set.getText().toString();
                //Toast.makeText(getActivity(), "DownloadResult" + as,Toast.LENGTH_LONG).show();
                //(month + "/" + day + "/" + year);
                //(year + "-" + month + "-" + day);
            }


        }

    }

}
