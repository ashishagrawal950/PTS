package com.pts.invetech.customlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.model.appned.rldc.RldcClickDatum;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vaibhav on 5/8/17.
 */

public class RldcClickListAdapter extends BaseAdapter implements Filterable {

    private List<RldcClickDatum> orgData;
    private List<RldcClickDatum> tmpData;
    private Context mContext;

    private DatumFilter dataFilter;


    public RldcClickListAdapter(Context _context,List<RldcClickDatum> _data){
        mContext=_context;
        orgData =_data;
        tmpData=_data;
    }

    @Override
    public int getCount() {
        return tmpData.size();
    }

    @Override
    public RldcClickDatum getItem(int position) {
        return tmpData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CardViewHolder viewHolder;
        if (row == null) {

            row = LayoutInflater.from(mContext).inflate(R.layout.listitem_realtimedetails, parent, false);

            viewHolder = new CardViewHolder();

            viewHolder.tvno = (TextView) row.findViewById(R.id.tvno);
            viewHolder.tvbuyer = (TextView) row.findViewById(R.id.tvbuyer);
            viewHolder.tvseller = (TextView) row.findViewById(R.id.tvseller);
            viewHolder.tvapproval = (TextView) row.findViewById(R.id.tvapprov);
            viewHolder.tvtotalqtm = (TextView) row.findViewById(R.id.tvtotalqtm);
            viewHolder.trackingOn = (TextView) row.findViewById(R.id.tracking);
            viewHolder.tvtrader = (TextView) row.findViewById(R.id.tvtrader);

            row.setTag(viewHolder);
        } else {
            viewHolder = (CardViewHolder) row.getTag();
        }
        RldcClickDatum datum = getItem(position);

        viewHolder.tvno.setText(""+(position+1));
        viewHolder.tvtrader.setText(datum.getTrader());
        viewHolder.tvbuyer.setText(datum.getBuyer());
        viewHolder.tvseller.setText(datum.getSeller());
        viewHolder.tvapproval.setText(datum.getApprovalNo());
        viewHolder.tvtotalqtm.setText(datum.getTotalqtm());

        if(datum.getTrack()==0){
            viewHolder.trackingOn.setVisibility(View.INVISIBLE);
        }else{
            viewHolder.trackingOn.setVisibility(View.VISIBLE);
        }

        return row;
    }

    public void resetData() {
        tmpData=orgData;
    }

    static class CardViewHolder {
        TextView tvno;

        TextView tvtrader;
        TextView tvbuyer;
        TextView tvseller;
        TextView tvapproval;
        TextView tvtotalqtm;

        TextView trackingOn;
    }



    @Override
    public Filter getFilter() {
        if (dataFilter == null)
            dataFilter = new DatumFilter();

        return dataFilter;
    }

    public class DatumFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            List<RldcClickDatum> datumList = new ArrayList<RldcClickDatum>();

            if (constraint == null || constraint.length() == 0) {
                results.values = orgData;
                results.count = orgData.size();
            }
            else {
                for (RldcClickDatum p : orgData) {
                    if (p.getBuyer().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            p.getSeller().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            p.getTrader().toLowerCase().contains(constraint.toString().toLowerCase()))

                        datumList.add(p);
                }
                results.values = datumList;
                results.count = datumList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {

            if (results.count == 0)
                notifyDataSetInvalidated();
            else {
                tmpData = (List<RldcClickDatum>) results.values;
                notifyDataSetChanged();
            }
        }
    }
}
