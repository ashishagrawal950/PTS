package com.pts.invetech.myscheduling;

/**
 * Created by vaibhav on 11/4/17.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetSaveSchedule {

    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("data")
    @Expose
    private List<MyScheduleDatum> data = null;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public List<MyScheduleDatum> getData() {
        return data;
    }

    public void setData(List<MyScheduleDatum> data) {
        this.data = data;
    }

}