package com.pts.invetech.customlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pts.invetech.R;

import java.util.ArrayList;

/**
 * Created by Ashish on 21-02-2017.
 */

public class AdapterMCPComparisionThree extends ArrayAdapter<String> {
    private final Activity context;
    static ArrayList<String> blockarrayListfrom = new ArrayList<String>();
    static ArrayList<String> lastyearmarketprice_Arrayone = new ArrayList<String>();
    static ArrayList<String> lastyearmarketprice_Arraytwo = new ArrayList<String>();
    static ArrayList<String> lastyearmarketprice_Arraythree = new ArrayList<String>();



    public AdapterMCPComparisionThree(Activity context, ArrayList<String> blockarrayListfrom,
                                 ArrayList<String> lastyearmarketprice_Arrayone
            , ArrayList<String> lastyearmarketprice_Arraytwo
            , ArrayList<String> lastyearmarketprice_Arraythree)

    {
        super(context, R.layout.listitem_mcp_list_three, blockarrayListfrom);
        this.blockarrayListfrom = blockarrayListfrom;
        this.lastyearmarketprice_Arrayone = lastyearmarketprice_Arrayone;
        this.lastyearmarketprice_Arraytwo = lastyearmarketprice_Arraytwo;
        this.lastyearmarketprice_Arraythree = lastyearmarketprice_Arraythree;
        this.context = context;

    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View rowView = inflater.inflate(R.layout.listitem_mcp_list_three, null, true);
        TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
        TextView txt2 = (TextView) rowView.findViewById(R.id.txt2);
        TextView txt3 = (TextView) rowView.findViewById(R.id.txt3);
        TextView txt4 = (TextView) rowView.findViewById(R.id.txt4);
        txt1.setText(blockarrayListfrom.get(position));
        txt2.setText(lastyearmarketprice_Arrayone.get(position));
        txt3.setText(lastyearmarketprice_Arraytwo.get(position));
        txt4.setText(lastyearmarketprice_Arraythree.get(position));


        return rowView;
    }


}
