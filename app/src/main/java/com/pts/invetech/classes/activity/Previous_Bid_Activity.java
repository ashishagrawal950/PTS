package com.pts.invetech.classes.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerString;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseActivity;
import com.pts.invetech.R;
import com.pts.invetech.customlist.ExpandListBidAdapter;
import com.pts.invetech.customscrollview.widget.AnimatedExpandableListView;
import com.pts.invetech.pojo.BidSummery;
import com.pts.invetech.utils.AppDateUtils;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Ashish on 07-09-2016.
 */
public class Previous_Bid_Activity extends AppBaseActivity {

    private LinearLayout mopenLayout;
    private ExpandListBidAdapter adapter;
    private AnimatedExpandableListView listView;
    private List<BidSummery> bidSummeryData;
    private Button searchPrevBid;
    private String dbacess,domain;
    private SQLiteDatabase db;
    private String copyDate = null;
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            try {
                networkCall(selectedYear + "-" + (selectedMonth + 1) + "-" + selectedDay);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    private View.OnClickListener dateClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showDatePickerDialog(v);
        }
    };
    private DatePickerDialog.OnDateSetListener copyPickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            try {
                RelativeLayout holder = (RelativeLayout) findViewById(R.id.popupholder);
                if (holder.getVisibility() == View.VISIBLE) {
                    TextView dateVu = (TextView) holder.findViewById(R.id.bidtodate);
                    dateVu.setTag(AppDateUtils.getCopyDateFormatted(selectedYear, selectedMonth + 1, selectedDay));
                    dateVu.setText(AppDateUtils.getCopyShowDate(selectedYear, selectedMonth, selectedDay));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private NetworkCallBack callBack = new NetworkCallBack() {

        @Override
        public void onResultString(String data, int id) {
            if (data == null || data.isEmpty()) {
                AppLogger.showToastShort(Previous_Bid_Activity.this, "No Data Found.");
                return;
            }

            try {
                JSONObject obj = new JSONObject(data);
                String msg = obj.getString("message");
                String status = obj.getString("status");

                AppLogger.showToastShort(Previous_Bid_Activity.this, msg);
                if (status.equalsIgnoreCase("SUCCESS")) {
                    networkCall("ALL");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    private View.OnClickListener popUpClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            RelativeLayout holder = (RelativeLayout) findViewById(R.id.popupholder);
            if (v.getId() == R.id.bidpopupcopy) {
                if (holder.getVisibility() == View.VISIBLE) {
                    copyDataSet(v);
                    TextView view = (TextView) holder.findViewById(R.id.bidtodate);
                    if (view.getText().toString().equalsIgnoreCase("")) {
                        AppLogger.showToastShort(Previous_Bid_Activity.this, "Select Correct Date.");
                    } else {
                        String date = (String) view.getTag();
                        if (isValidDate(date)) {
                            try {
                                copyNetworkCall(date);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            clearPopup(holder);
                        } else {
                            AppLogger.showToastShort(Previous_Bid_Activity.this, "Select Correct Date.");
                        }
                    }
                }
            } else if (v.getId() == R.id.bidpopupcancel) {
                if (holder.getVisibility() == View.VISIBLE) {
                    clearPopup(holder);
                }
            }
        }
    };

    /*
        Date Related Stuff
     */
    private NetworkCallBack subCallBack = new NetworkCallBack() {

        @Override
        public void onResultString(String data, int id) {
            if (data == null || data.isEmpty()) {
                AppLogger.showToastShort(Previous_Bid_Activity.this, "Could not Submit Bid.");
                return;
            } else {
                try {
                    showData(data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    private View.OnClickListener copyClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            if (v.getId() == R.id.submitbidbtn) {
                showAlertDialog(v);
            } else if (v.getId() == R.id.bidedit) {
                showEditBid(v);
            } else {
                showConfirmCopyData(v);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_previous_bid);

        db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
        Cursor c = db.rawQuery("SELECT * FROM baby", null);
        if (c.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        StringBuffer buffer = new StringBuffer();
        while (c.moveToNext()) {
            buffer.append("access_key: " + c.getString(0) + "\n");
            dbacess = c.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
        mopenLayout = (LinearLayout) findViewById(R.id.openLayout);
        mopenLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Previous_Bid_Activity.this, NewBidActivity.class);
                in.putExtra("name", "");
                in.putExtra("from", "MainActivity");
                startActivity(in);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        LinearLayout lltoplogout = (LinearLayout) findViewById(R.id.lltoplogout);
        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db = openOrCreateDatabase("deviceDBSecond", android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                db.execSQL("DROP TABLE IF EXISTS baby");
                db.execSQL("DROP TABLE IF EXISTS logininfo");
                db.execSQL("DROP TABLE IF EXISTS companyname");
                db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
                db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");

                Intent i = new Intent(getApplicationContext(), SplashScreenActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.animation, R.anim.animation2);
                finish();
            }
        });

        searchPrevBid = (Button) findViewById(R.id.et_search_previous_bid);
        searchPrevBid.setOnClickListener(dateClick);

        ImageView search_icon_previous_bid = (ImageView) findViewById(R.id.search_icon_previous_bid);

        search_icon_previous_bid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchPrevBid.setVisibility(View.VISIBLE);
            }
        });


        try {
            networkCall("ALL");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
        COPY bid
     */

    private void networkCall(String date) throws JSONException {
        String url = domain + Constant.SUBURL_GET_FULL_BID_DETAILS;

        JSONObject object = new JSONObject();
        object.put(Constant.VALUE_DATE, date);
        object.put(Constant.VALUE_ACCESSKEY, dbacess);

        new NetworkManagerActivity(this, object, url).execute();
    }

    private void setBidList() {
        adapter = new ExpandListBidAdapter(this, bidSummeryData, copyClick);

        listView = (AnimatedExpandableListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);

        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            private int previousGroup = -1;

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (listView.isGroupExpanded(groupPosition)) {
                    listView.collapseGroupWithAnimation(groupPosition);
                    previousGroup = -1;
                } else {
                    listView.expandGroupWithAnimation(groupPosition);
                    if (previousGroup != -1) {
                        listView.collapseGroupWithAnimation(previousGroup);
                    }
                    previousGroup = groupPosition;
                }

                return true;
            }

        });

        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                LinearLayout ic_copy = (LinearLayout) findViewById(R.id.ic_copy);
                ic_copy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "Copy bid file", Toast.LENGTH_SHORT).show();
                    }
                });

                return false;
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                RelativeLayout holder = (RelativeLayout) findViewById(R.id.popupholder);
                if (holder.getVisibility() == View.VISIBLE) {
                    return;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        if (bidSummeryData.size() == 1) {
            searchPrevBid.setText(bidSummeryData.get(0).getDate());
        } else {
            searchPrevBid.setText("Pick Trading Date");
        }

    }

    @Override
    public void updateStringResult(String string) {

        if (string == null || string.isEmpty() || string.length() < 5) {
            AppLogger.showToastShort(this, "No Data Found.");
            return;
        }

        try {
            convertToData(string);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setBidList();
    }

    private void convertToData(String string) throws JSONException {
        JSONArray array = new JSONArray(string);
        bidSummeryData = new ArrayList<>();

        GsonBuilder gsonBuilder = new GsonBuilder();

        for (int index = 0; index < array.length(); index++) {
            JSONObject obj = array.getJSONObject(index);
            Gson gson = gsonBuilder.create();

            BidSummery bidSum = gson.fromJson(obj.toString(), BidSummery.class);
            bidSummeryData.add(bidSum);
        }
    }

    public void showDatePickerDialog(View view) {

        DatePickerDialog datePickerDialog;

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(this, pickerListener, year, month, day);
        datePickerDialog.show();
    }

    public void showDatePickerDialog(View view, DatePickerDialog.OnDateSetListener listner) {

        DatePickerDialog datePickerDialog;

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(this, listner, year, month, day);
        datePickerDialog.show();
    }

    private void showEditBid(View view) {
        String[] tag = ((String) view.getTag()).split(":");
        int groupPos = Integer.parseInt(tag[0]);
        int childPos = Integer.parseInt(tag[1]);

        BidSummery bidData = bidSummeryData.get(groupPos);

        Intent i = new Intent(Previous_Bid_Activity.this, NewBidEditActivity.class);
        i.putExtra(Constant.VALUE_DATE, bidData.getDate());
        i.putExtra("typeandexchange", bidData.getHeaderName(childPos));
        startActivity(i);
        finish();
    }

    private void showConfirmCopyData(View view) {
        RelativeLayout holder = (RelativeLayout) findViewById(R.id.popupholder);
        holder.setVisibility(View.VISIBLE);
        holder.findViewById(R.id.bidpopupcancel).setOnClickListener(popUpClick);
        int num = 0;
        try {
            String str = (String) view.getTag();
            String[] arr = str.split(":");
            num = Integer.parseInt(arr[0]);
        } catch (Exception ex) {
            AppLogger.showMsgWithoutTag("Date can not be converted.");
        }

        TextView frmDate = (TextView) holder.findViewById(R.id.bidfrmdate);
        frmDate.setText(bidSummeryData.get(num).getDeliveryDate());

        Button cpyBtn = (Button) holder.findViewById(R.id.bidpopupcopy);
        cpyBtn.setTag(view.getTag());
        cpyBtn.setOnClickListener(popUpClick);

        TextView vu = (TextView) holder.findViewById(R.id.bidtodate);
        vu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(v, copyPickerListener);
            }
        });
    }

    private void clearPopup(RelativeLayout holder) {
        holder.setVisibility(View.GONE);
        TextView toBid = (TextView) holder.findViewById(R.id.bidtodate);
        toBid.setText("");
    }

    private void copyDataSet(View view) {
        copyDate = null;
        String[] tag = ((String) view.getTag()).split(":");

        int groupPos = Integer.parseInt(tag[0]);
        int childPos = Integer.parseInt(tag[1]);
        copyDate = bidSummeryData.get(groupPos).getDate();
    }

    private void copyNetworkCall(String date) throws JSONException {
        String url = Constant.APP_BASE_URL + Constant.SUBURL_COPYBID;

        if (copyDate == null) {
            AppLogger.showToastShort(this, "Some Error Occured Please Try Again.");
            return;
        }

        JSONObject object = new JSONObject();
        object.put("copydate", copyDate);
        object.put("todate", date);
        object.put(Constant.VALUE_ACCESSKEY, dbacess);

        String[] params = new String[2];
        params[0] = url;
        params[1] = object.toString();

        AppLogger.showMsgWithoutTag(params[0]);
        AppLogger.showMsgWithoutTag(params[1]);

        new NetworkHandlerString(this, callBack, 1).execute(params);
    }

    @Override
    public void onBackPressed() {
        RelativeLayout holder = (RelativeLayout) findViewById(R.id.popupholder);
        if (holder.getVisibility() == View.VISIBLE) {
            clearPopup(holder);
            return;
        }
        super.onBackPressed();

        Intent in = new Intent(Previous_Bid_Activity.this, NewBidActivity.class);
        in.putExtra("name", "");
        in.putExtra("from", "MainActivity");
        startActivity(in);
        overridePendingTransition(R.anim.animation, R.anim.animation2);
        finish();
    }


    /*
        Submit BID
     */

    public boolean isValidDate(String dateString) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        try {
            df.parse(dateString);
            return true;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void submitBid(View v) throws JSONException {
        int num = (int) v.getTag();
        BidSummery bSum = bidSummeryData.get(num);

        JSONObject object = new JSONObject();
        object.put("biddate", bSum.getDate());
        object.put(Constant.VALUE_ACCESSKEY, dbacess);

        String[] params = new String[2];
        params[0] = domain + Constant.SUBURL_SUBMIT_BID_DATE_WISE;
        params[1] = object.toString();

        new NetworkHandlerString(this, subCallBack, 0).execute(params);
    }

    private void showData(String data) throws JSONException {
        JSONObject obj = new JSONObject(data);
        String msg = obj.getString("message");
        String status = obj.getString("status");

        AppLogger.showToastLong(this, msg);

        try {
            if (status.equalsIgnoreCase("SUCCESS")) {
                networkCall("ALL");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showAlertDialog(final View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to submit this bid ?");
        builder.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        dialog.cancel();
                    }
                });
        builder.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        try {
                            submitBid(v);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

        builder.show();
    }


}