package com.pts.convertor;

import com.pts.model.PowerDemandComparision;
import com.pts.model.PowerDemandOuter;
import com.pts.model.YearandMonthvalue;
import com.pts.model.statewise.StateRealTimeValue;
import com.pts.model.testmodel.InnerModel;
import com.pts.model.testmodel.InnerTableData;
import com.pts.model.testmodel.OuterTableData;
import com.pts.model.testmodel.OuterTest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Ashish on 24-07-2017.
 */

public class StateWiseRealConvertor {

    public static ArrayList<StateRealTimeValue> getRealTimeData(String data) throws JSONException {

        JSONObject object = new JSONObject(data);
        if (object.has("status")) {
            String status = object.getString("status");
            if (status.equalsIgnoreCase("TRUE")) {
                JSONArray innerObj = object.getJSONArray("value");
                return getInnerData(innerObj);
            } else {
                return null;
            }
        }
        return null;
    }

    public static String getmessage(String data) throws JSONException {
        JSONObject object = new JSONObject(data);
        if (object.has("message")) {
            String message = object.getString("message");
            return message;
        }
        return null;
    }

    /*private static ArrayList<StateRealTimeValue> getInnerData(JSONObject innerObj) throws JSONException {
        Iterator<String> keyItr=innerObj.keys();
        ArrayList<StateRealTimeValue> tmp=new ArrayList<>();
        while(keyItr.hasNext()){
            String key=keyItr.next();
            if(innerObj.has(key)){
                JSONObject objjjj=innerObj.getJSONObject(key);
                StateRealTimeValue rVal=new StateRealTimeValue();
                rVal.setName(key);
                rVal.setAvg(objjjj.has("avg")? String.valueOf(objjjj.getDouble("avg")):"0");
                rVal.setPer(objjjj.has("per")? String.valueOf(objjjj.getDouble("per")):"0");
                rVal.setSum(objjjj.has("sum")? String.valueOf(objjjj.getDouble("sum")) :"0");
                tmp.add(rVal);
            }
        }

        return tmp;
    }*/

    private static ArrayList<StateRealTimeValue> getInnerData(JSONArray innerObj) throws JSONException {
        ArrayList<StateRealTimeValue> tmp = new ArrayList<>();
        for (int i = 0; i < innerObj.length(); i++) {
            JSONObject json_data = innerObj.getJSONObject(i);
            Iterator<String> keyItr = json_data.keys();
            while (keyItr.hasNext()) {
                String key = keyItr.next();
                if (json_data.has(key)) {
                    JSONObject objjjj = json_data.getJSONObject(key);
                    StateRealTimeValue rVal = new StateRealTimeValue();
                    rVal.setName(key);
                    rVal.setAvg(objjjj.has("avg") ? String.valueOf(objjjj.getDouble("avg")) : "0");
                    rVal.setPer(objjjj.has("per") ? String.valueOf(objjjj.getDouble("per")) : "0");
                    rVal.setSum(objjjj.has("sum") ? String.valueOf(objjjj.getDouble("sum")) : "0");
                    rVal.setRevision(objjjj.has("revision") ? (objjjj.getString("revision")) : "0");
                    tmp.add(rVal);
                }

            }

        }
        return tmp;
    }

    public static ArrayList<YearandMonthvalue> getYearandMonthdata(String data) throws JSONException {
        JSONObject object = new JSONObject(data);
        if (object.has("status")) {
            String status = object.getString("status");
            if (status.equalsIgnoreCase("TRUE")) {
                JSONArray innerObj = object.getJSONArray("value");
                return getvalueData(innerObj);
            } else {
                return null;
            }
        }
        return null;
    }

    private static ArrayList<YearandMonthvalue> getvalueData(JSONArray innerObj) throws JSONException {
        ArrayList<YearandMonthvalue> tmp = new ArrayList<>();
        for (int i = 0; i < innerObj.length(); i++) {
            JSONArray json_data = innerObj.getJSONArray(i);
            for (int j = 0; j < json_data.length(); j++) {
                JSONObject data = json_data.getJSONObject(j);
                YearandMonthvalue rVal = new YearandMonthvalue();
                rVal.setData(data.has("data") ? (data.getString("data")) : "0");
                rVal.setVal(data.has("val") ? (data.getString("val")) : "0");
                tmp.add(rVal);
            }
        }
        return tmp;
    }


    public static ArrayList<PowerDemandOuter> getPowerDemandComparisiondata(String data) throws JSONException {
        JSONObject object = new JSONObject(data);
        if (object.has("status")) {
            String status = object.getString("status");
            if (status.equalsIgnoreCase("TRUE")) {
                JSONArray innerObj = object.getJSONArray("value");
                return getPowerDemandData(innerObj);
            }/*else if(status.equalsIgnoreCase("ERR")){
                return object.getString("message");
            } */else {
                return null;
            }
        }
        return null;

    }

    private static ArrayList<PowerDemandOuter> getPowerDemandData(JSONArray innerObj) throws JSONException {
        ArrayList<PowerDemandOuter> tmp = new ArrayList<>();
        for (int i = 0; i < innerObj.length(); i++) {
            JSONObject json_data = innerObj.getJSONObject(i);
            Iterator<String> keyItr = json_data.keys();
            while (keyItr.hasNext()) {
                PowerDemandOuter pdOuter = new PowerDemandOuter();
                String key = keyItr.next();
                pdOuter.setYear(key);
                if (json_data.has(key)) {
                    JSONObject objjjj = json_data.getJSONObject(key);
                    Iterator<String> secondkeyItr = objjjj.keys();
                    while (secondkeyItr.hasNext()) {
                        String secondkey = secondkeyItr.next();
                        if (objjjj.has(secondkey)) {
                            JSONObject secondobj = objjjj.getJSONObject(secondkey);
                            PowerDemandComparision rVal = new PowerDemandComparision();
                            rVal.setState(secondkey);
                            rVal.setDemand(secondobj.has("demand") ? secondobj.getString("demand") : "0");
                            rVal.setPeak(secondobj.has("peak") ? secondobj.getString("peak") : "0");
                            rVal.setId(secondobj.has("state_id") ? secondobj.getString("state_id") : "-1");
                            pdOuter.addPowerDemand(rVal);
                        }
                    }
                }

                tmp.add(pdOuter);
            }
        }


        return tmp;
    }

    public static ArrayList<OuterTableData> getOuterTestModel(String data) throws JSONException {
        JSONObject object = new JSONObject(data);
        if (object.has("status")) {
            String status = object.getString("status");
            if (status.equalsIgnoreCase("TRUE")) {
                JSONArray innerObj = object.getJSONArray("value");
                return getSetOuterData(innerObj);
            } else {
                return null;
            }
        }
        return null;
    }

    public static ArrayList<OuterTableData> getSetOuterData(JSONArray innerObj) throws JSONException {

        ArrayList<OuterTableData> outerData = new ArrayList<>();

        for (int i = 0; i < innerObj.length(); i++) {

            JSONObject json_data = innerObj.getJSONObject(i);
            Iterator<String> keyItr = json_data.keys();

            while (keyItr.hasNext()) {
                String year = keyItr.next();
                JSONObject midObj = json_data.getJSONObject(year);
                Iterator<String> innKey = midObj.keys();

                while (innKey.hasNext()) {
                    InnerTableData iMode = new InnerTableData();

                    String state = innKey.next();
                    JSONObject inObj = midObj.getJSONObject(state);

                    OuterTableData oData = getOut(outerData, state);
                    if (oData == null) {
                        oData = new OuterTableData();
                        oData.setStateName(state);

                        outerData.add(oData);
                    }
                    String demand = inObj.getString("demand");
                    String peak = inObj.getString("peak");

                    iMode.setDemand(demand);
                    iMode.setPeak(peak);

                    oData.addInnerData(iMode);
                }
            }
        }

        return outerData;
    }

    public static OuterTableData getOut(ArrayList<OuterTableData> _data, String stateName) {
        for (OuterTableData test : _data) {
            if (test.hasState(stateName)) {
                return test;
            }
        }
        return null;
    }

    public static ArrayList<OuterTableData> getDataForTable(ArrayList<PowerDemandOuter> _powerDemandList){

        ArrayList<OuterTableData> outerList=new ArrayList<>();

        for(PowerDemandOuter pdOuter:_powerDemandList){
            ArrayList<PowerDemandComparision> pdCompareList=pdOuter.getPdCompareList();
            for(PowerDemandComparision powerDemandComparision:pdCompareList){

                OuterTableData oTest=getOut(outerList,powerDemandComparision.getState());
                if(oTest==null){
                    oTest=new OuterTableData();
                    oTest.setStateName(powerDemandComparision.getState());
                    oTest.setStateId(powerDemandComparision.getId());
                }

                InnerTableData inModel=new InnerTableData();
                inModel.setDemand(powerDemandComparision.getDemand());
                inModel.setPeak(powerDemandComparision.getPeak());
                inModel.setYear(pdOuter.getYear());

                oTest.addInnerData(inModel);

                if(!outerList.contains(oTest)){
                    outerList.add(oTest);
                }
            }
        }

        return outerList;

    }

    public static String getRealTimeDate(String data) throws JSONException {
        JSONObject object=new JSONObject(data);
        if(object.has("date")){
            return object.getString("date");
        }
        return null;
    }


   /* public static String getRealTimeDate(String data) throws JSONException {

        JSONObject object=new JSONObject(data);
        if(object.has("date")){
            return object.getString("date");
        }

        return null;

    }*/
}