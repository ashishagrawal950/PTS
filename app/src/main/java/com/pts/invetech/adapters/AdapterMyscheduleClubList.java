package com.pts.invetech.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.pts.invetech.R;
import com.pts.model.myscheduling.ClubViewData;

import java.util.ArrayList;

/**
 * Created by Ashish Karn on 20-03-2018.
 */

public class AdapterMyscheduleClubList extends BaseAdapter {

    private Context context;
    private ArrayList<ClubViewData> data;

    private String[] colArr={"#515151","#000000"};

    public AdapterMyscheduleClubList(Context context,
                                     ArrayList<ClubViewData> _data) {
        this.context = context;
        data = _data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public ClubViewData getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).hashCode();
    }

    public View getView(int position, View view, ViewGroup parent) {

        ClubViewHolder viewHolder;

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout
                            .listitem_club_myschedulelist,
                    null, true);

            viewHolder = new ClubViewHolder(view);
            view.setTag(viewHolder);
        }else{
            viewHolder= (ClubViewHolder) view.getTag();
        }

        ClubViewData vData=getItem(position);

        viewHolder.sNo.setText(vData.getsNo());
        viewHolder.region.setText(vData.getRegion());
        viewHolder.buyer.setText(vData.getBuyer());
        viewHolder.seller.setText(vData.getSeller());
        viewHolder.approval.setText(vData.getApproval());
        viewHolder.qtm.setText(vData.getQtm());
        viewHolder.diff.setText(vData.getDifference());
        viewHolder.club_rev.setText(vData.getRevision());


        if(vData.getIsClubbed()==1){
            viewHolder.club_holder.setBackgroundColor(Color.parseColor(colArr[0]));
            changeColor(viewHolder,Color.WHITE);
        }else if(vData.getIsClubbed()==2){
            viewHolder.club_holder.setBackgroundColor(Color.parseColor(colArr[1]));
            changeColor(viewHolder,Color.WHITE);
        }else if(vData.getIsClubbed()==3){
            viewHolder.club_holder.setBackgroundColor(Color.parseColor(colArr[0]));
            changeColor(viewHolder,Color.RED);
        }

        return view;
    }

    private void changeColor(ClubViewHolder viewHolder,int colCode){
        viewHolder.sNo.setTextColor(colCode);
        viewHolder.region.setTextColor(colCode);
        viewHolder.buyer.setTextColor(colCode);
        viewHolder.seller.setTextColor(colCode);
        viewHolder.approval.setTextColor(colCode);
        viewHolder.qtm.setTextColor(colCode);
        viewHolder.diff.setTextColor(colCode);
        viewHolder.club_rev.setTextColor(colCode);
    }

    private class ClubViewHolder {
        LinearLayout club_holder;
        TextView sNo, region, buyer, seller, approval, qtm, diff,club_rev;

        ClubViewHolder(View view) {
            club_holder= (LinearLayout) view.findViewById(R.id.club_holder);

            sNo = (TextView) view.findViewById(R.id.club_sno);
            region = (TextView) view.findViewById(R.id.club_region);
            buyer = (TextView) view.findViewById(R.id.club_buyer);
            seller = (TextView) view.findViewById(R.id.club_seller);
            approval = (TextView) view.findViewById(R.id.club_approval);
            qtm = (TextView) view.findViewById(R.id.club_qtm);
            diff = (TextView) view.findViewById(R.id.club_diff);
            club_rev= (TextView) view.findViewById(R.id.club_rev);
        }
    }

}