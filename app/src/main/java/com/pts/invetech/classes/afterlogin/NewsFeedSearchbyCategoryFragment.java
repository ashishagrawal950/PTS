package com.pts.invetech.classes.afterlogin;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.AlertDialogManager;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.GPSTracker;
import com.pts.invetech.R;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.activity.NewsFeedActivitySearchWebView;
import com.pts.invetech.customlist.EnergyNewfeedCustomList;
import com.pts.invetech.customscrollview.widget.NonScrollListView;
import com.pts.invetech.dashboardupdate.views.DashBoardUpdate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class NewsFeedSearchbyCategoryFragment extends Fragment {
    private final Context context = getActivity();
    // alert dialog manager
    private AlertDialogManager alert = new AlertDialogManager();
    // Internet detector
    private ConnectionDetector cd;
    private Dialog prgDialog;
    private Editor editor;
    private LinearLayout openLayout;
    private String descinvno, descriptiondata, uniprice, ammount, disccount, quantity,
            dusername, dpassword;
    private Boolean isInternetPresent = false;
    private SharedPreferences pref;
    private SQLiteDatabase db;
    private String device_id;
    private String access_key, status, value, message, category, companyName,
            client_id;
    private NonScrollListView energylist;
    private String icontypefrom, dbacess, displaydate, displaytime;
    private String[] icontypearray;
    private String[] array;
    private String[] arraydispalydate;
    private String[] arraydispalytime;
    private Integer[] elementiconarray;

    private int width;
    private HorizontalScrollView hsview;
    private LinearLayout llhsview, lltoplogin, lltoplogout;
    private String loginconfig;
    private JSONObject mainObject;
    private ArrayList<String> key_element = new ArrayList<String>();
    private ArrayList<Integer> elementiconarray_element = new ArrayList<Integer>();
    private ArrayList<String> id_key_element = new ArrayList<String>();
    private ArrayList<String> title_key_element = new ArrayList<String>();
    private ArrayList<String> type_key_element = new ArrayList<String>();
    private ArrayList<String> link_key_element = new ArrayList<String>();
    private ArrayList<String> comment_key_element = new ArrayList<String>();
    private ArrayList<String> pubdate_key_element = new ArrayList<String>();
    private ArrayList<String> time_key_element = new ArrayList<String>();
    private ArrayList<String> source_key_element = new ArrayList<String>();

    private ArrayList<String> date_Array = new ArrayList<String>();
    private ArrayList<String> min_Array = new ArrayList<String>();
    private ArrayList<String> max_Array = new ArrayList<String>();
    private ArrayList<String> avg_Array = new ArrayList<String>();

    private ArrayList<String> lastyearmarketprice_Arrayone = new ArrayList<String>();
    private ArrayList<String> lastyearmarketprice_Arraytwo = new ArrayList<String>();
    private ArrayList<String> lastyearmarketprice_Arraythree = new ArrayList<String>();

    private TextView tvcategoryname, tvlogintext, tvlogouttext, buttonClick;
    private String sprevision_send, spdate_send, saveResponse;
    private LinearLayout layout_mcp;

    private GPSTracker gps;
    private double latitude, longitude;
    private ArrayList<String> categories = new ArrayList<String>();
    private ArrayList<String> state_id_Array = new ArrayList<String>();
    private ArrayList<String> state_name_Array = new ArrayList<String>();
    private ArrayAdapter<String> adapter_state;
    private Spinner dropdwn, spbidtype;
    private String state_id_array_fromdb, state_name_name_fromdb, state_id_fromdb;
    private String state_id_send, bidtype_id_send, domain;
    private boolean isTrue = true;
    private View rootView;
    private EditText inputSearch;
    private EnergyNewfeedCustomList newsfeedadapter;
    private String searchelement;
    private boolean isFirst = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_allnewsfeed_catrgorywise, container,
                false);
		/*prgDialog = new ProgressDialog(getActivity());
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        db = getActivity().openOrCreateDatabase("deviceDBSecond",
                Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientid(clientid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationnewsfeed(newMessage TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS gpsloaction(latitude VARCHAR, longitude VARCHAR, state_id VARCHAR, state_serial VARCHAR);");

        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
        if(!isFirst){
            domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        }
        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NewFeedMainActivity) getActivity()).open();
            }
        });

        tvcategoryname = (TextView) rootView.findViewById(R.id.tvcategoryname);
        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        energylist = (NonScrollListView) rootView.findViewById(R.id.energylist);

        Bundle bundle = this.getArguments();
        searchelement = bundle.getString("Key");
        tvcategoryname.setText(searchelement);
        //searchelement = "Coal";
        //Toast.makeText(getActivity(), "Frag: "+searchelement, Toast.LENGTH_LONG).show();

        cd = new ConnectionDetector(getActivity());
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getActivity(), "Please Check your network.",
                    Toast.LENGTH_LONG).show();
        } else {
            Cursor newfeedsavec = db
                    .rawQuery("select * from newfeedsave", null);
            if (newfeedsavec.getCount() == 0) {
                // new HttpAsyncTaskmla().execute("http://" + ipaddressfromdb +
                // Constant.ELECTIONGetMlaInfo);
                // new
                // HttpAsyncTaskgetnewsfeedinfo().execute("http://www.mittalpower.com/mobile/pxs_app/service/getnewsfeedinfo.php");
            } else {
                newfeedsavec.moveToFirst();
                do {
                    saveResponse = newfeedsavec.getString(newfeedsavec
                            .getColumnIndex("response"));
                } while (newfeedsavec.moveToNext());
                fatchdataofnewfeed();
            }
        }

        Intent in = getActivity().getIntent();
        loginconfig = in.getStringExtra("LOGINCONFIG");
        // sprevision_send = in.getStringExtra("revision");
        // spdate_send = in.getStringExtra("date");
        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            // String companyname = loginconfig.substring(0, 6);
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        lltoplogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // db = openOrCreateDatabase("deviceDBSecond",
                // android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                // db.execSQL("DROP TABLE IF EXISTS baby");
                // db.execSQL("DROP TABLE IF EXISTS logininfo");
                // db.execSQL("DROP TABLE IF EXISTS companyname");
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
                // overridePendingTransition(R.anim.animation,R.anim.animation2);
            }
        });

        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // db = openOrCreateDatabase("deviceDBSecond",
                // android.content.Context.MODE_PRIVATE, null);
                // db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationUnRead(notificationunread VARCHAR);");
                // db.execSQL("DROP TABLE IF EXISTS baby");
                // db.execSQL("DROP TABLE IF EXISTS logininfo");
                // db.execSQL("DROP TABLE IF EXISTS companyname");
                Intent i = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation,
                        R.anim.animation2);
                getActivity().finish();
            }
        });
        return rootView;
    }

    public void fatchdataofnewfeed() {
        Cursor newfeedsavec = db.rawQuery("select * from newfeedsave", null);
        newfeedsavec.moveToFirst();
        do {
            saveResponse = newfeedsavec.getString(newfeedsavec
                    .getColumnIndex("response"));
            // String notificationdate =
            // newfeedsavec.getString(newfeedsavec.getColumnIndex("notificationdate"));
            // Toast.makeText(getActivity(), saveResponse,
            // Toast.LENGTH_LONG).show();
        } while (newfeedsavec.moveToNext());
        try {
            mainObject = new JSONObject(saveResponse);
            key_element.clear();
            Iterator<String> iter = mainObject.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                key_element.add(key);
            }
            try {
                elementiconarray_element.clear();
                id_key_element.clear();
                title_key_element.clear();
                type_key_element.clear();
                link_key_element.clear();
                comment_key_element.clear();
                pubdate_key_element.clear();
                time_key_element.clear();
                source_key_element.clear();

                category = key_element.get(0);
                JSONArray keyResult = mainObject.getJSONArray(searchelement);
                for (int i = 0; i < keyResult.length(); i++) {
                    JSONObject keyResulttDetails = keyResult.getJSONObject(i);
                    if (keyResulttDetails.has("type")) {
                        String type = keyResulttDetails.getString("type");
                        if (type.contains(searchelement)) {
                            if (keyResulttDetails.has("id")) {
                                String id = keyResulttDetails.getString("id");
                                id_key_element.add(id);
                                elementiconarray_element
                                        .add(R.drawable.news);
                            }
                            if (keyResulttDetails.has("title")) {
                                String title = keyResulttDetails
                                        .getString("title");
                                title_key_element.add(Html.fromHtml(title)
                                        .toString());
                            }
                            if (keyResulttDetails.has("link")) {
                                String link = keyResulttDetails
                                        .getString("link");
                                link_key_element.add(link);
                            }
                            if (keyResulttDetails.has("comment")) {
                                String comment = keyResulttDetails
                                        .getString("comment");
                                comment_key_element.add(comment);
                            }
                            if (keyResulttDetails.has("pubdate")) {
                                String pubdate = keyResulttDetails
                                        .getString("pubdate");
                                pubdate_key_element.add(pubdate);
                            }
                            if (keyResulttDetails.has("time")) {
                                String time = keyResulttDetails
                                        .getString("time");
                                time_key_element.add(time);
                            }
                            if (keyResulttDetails.has("source")) {
                                String source = keyResulttDetails
                                        .getString("source");
                                source_key_element.add(source);
                            }
                        }
                    }
                }
                newsfeedadapter = new EnergyNewfeedCustomList(
                        getActivity(), elementiconarray_element,
                        title_key_element, pubdate_key_element,
                        time_key_element, source_key_element);
                energylist.setAdapter(newsfeedadapter);
                // }
                energylist
                        .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent,
                                                    View view, final int position, long id) {
                                Intent in = new Intent(getActivity(),
                                        NewsFeedActivitySearchWebView.class);
                                in.putExtra("link_key_element",
                                        link_key_element.get(position));
                                in.putExtra("LOGINCONFIG", loginconfig);
                                startActivity(in);

                            }
                        });

            } catch (JSONException e) {
                // Something went wrong!
            }


        } catch (JSONException e) {
            Toast.makeText(getActivity(),
                    "500 Internal Server Error, Please Check your network.",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashBoardUpdate dashBoardUpdate = new DashBoardUpdate();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, dashBoardUpdate);
                        fragmentTransaction.commit();
                    }
                    return true;
                }
                return false;
            }
        });
    }

}

