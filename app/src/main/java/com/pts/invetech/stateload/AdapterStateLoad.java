package com.pts.invetech.stateload;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.dashboardupdate.model.dashboard.RegionLdc;

import java.util.List;


public class AdapterStateLoad extends BaseAdapter {

    private Context mContext;
    private List<RegionLdc> ldcList;

    public AdapterStateLoad(Context _con, List<RegionLdc> _ldc) {
        mContext = _con;
        ldcList = _ldc;
    }

    public void updateData(List<RegionLdc> _ldc){
        ldcList=_ldc;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return ldcList.size();
    }

    @Override
    public RegionLdc getItem(int i) {
        return ldcList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.listitem_realtimestateload, null);
        }
        RegionLdc ldc = getItem(i);

        TextView state = (TextView) view.findViewById(R.id.loadstate);
        TextView generation = (TextView) view.findViewById(R.id.loadgeneration);
        TextView demand = (TextView) view.findViewById(R.id.loaddemand);
        TextView schedule = (TextView) view.findViewById(R.id.loadschedule);
        TextView actual = (TextView) view.findViewById(R.id.loadactual);
        TextView deviation = (TextView) view.findViewById(R.id.loaddeviation);
        TextView id= (TextView) view.findViewById(R.id.loadid);
        id.setText(""+(i+1));

        state.setText(ldc.getState());
        generation.setText(ldc.getGeneration());
        demand.setText(ldc.getDemand());
        schedule.setText(ldc.getSchDrawal());
        actual.setText(ldc.getActDrawal());
        deviation.setText(ldc.getDeviation());


        return view;
    }
}