package com.pts.invetech.classes.afterlogin;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.AlertDialogManager;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.apiresponse.URSdetailsAPIResponse;
import com.pts.invetech.classes.activity.CurrentAvaiableURSDetailsActivity;
import com.pts.invetech.classes.fragment.MainActivityFragment;
import com.pts.invetech.customlist.URSCustomList;
import com.pts.invetech.marquee.MarqueeViewAdapter;
import com.pts.invetech.marquee.MyLayoutManager;
import com.pts.invetech.pojo.AvaiableURS;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.JSONUtils;
import com.pts.invetech.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;

public class CurrentAvaiableURSFragmentAfterLogin extends Fragment implements AdapterView.OnItemSelectedListener {
    private final Context context = getActivity();
    // alert dialog manager
    private AlertDialogManager alert = new AlertDialogManager();
    // Internet detector
    private ConnectionDetector cd;
    private Dialog prgDialog;
    private ProgressBar pbprogressBar;
    private LinearLayout openLayout;
    private SQLiteDatabase db;
    private String device_id, domain;
    private LinearLayout lltoplogin, lltoplogout;
    private String loginconfig;
    private TextView tvlogintext, tvlogouttext, buttonClick, tvnodatafound, tvnodata;
    private ListView list_view_main;
    private View rootView;
    private ArrayList<String> categories = new ArrayList<String>();
    private Spinner dropdwn;
    private String state_id_array_fromdb, state_name_name_fromdb, state_id_fromdb, region_name = "", from;
    private String NR, WR, SR, ER, NER, station_name, available, typeSend;
    private ArrayAdapter<String> adapter_state;
    private ArrayList<String> region_name_array = new ArrayList<>();
    private ArrayList<String> available_array = new ArrayList<>();
    private ArrayList<String> variable_Array = new ArrayList<>();

    private ArrayList<String> select_array = new ArrayList<>();
    private ArrayList<String> revision_array = new ArrayList<>();
    private ArrayList<String> station_name_Array = new ArrayList<String>();
    private ArrayList<String> available_Array = new ArrayList<String>();
    private ArrayList<String> rate_Array = new ArrayList<String>();
    private TextView tvdate;
    private boolean isprogessbar = true;
    private MainActivityFragment mainActivityFragment = null;
    private boolean isFirstTime = true;
    private MarqueeViewAdapter mAdapter;
    private MyLayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_currentavaiableurs, container,
                false);
		/*prgDialog = new ProgressDialog(getActivity());
		prgDialog.setMessage("Please wait...");
		prgDialog.setCancelable(false);*/
        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        //prgDialog.show();
        prgDialog.setCancelable(false);

        db = getActivity().openOrCreateDatabase("deviceDBSecond",
                Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS baby(access_key VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS logininfo(username VARCHAR, password VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS companyname(companyname VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS clientid(clientid VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS menuTable(menucaption VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS lastbidtimeTable(lastbidtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotification(newMessage VARCHAR, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS pushnotificationnewsfeed(newMessage TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS newfeedsave(response TEXT, notificationdate VARCHAR, notificationtime VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS gpsloaction(latitude VARCHAR, longitude VARCHAR, state_id VARCHAR, state_serial VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS availableurs(region VARCHAR, available VARCHAR, select_yes VARCHAR, revision VARCHAR, region_id VARCHAR);");
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }

        Cursor cfrom = db.rawQuery("SELECT * FROM gpsloaction", null);
        if (cfrom.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        while (cfrom.moveToNext()) {
            state_id_array_fromdb = cfrom.getString(0);
            state_name_name_fromdb = cfrom.getString(1);
            state_id_fromdb = cfrom.getString(2);
            typeSend = "STATE";
        }

        Cursor ursfrom = db.rawQuery("SELECT * FROM availableurs", null);
        if (ursfrom.getCount() == 0) {
            //showMessage("Error", "No records found");
        }
        while (ursfrom.moveToNext()) {
            region_name = ursfrom.getString(0);
            //Toast.makeText(getApplicationContext(), "access_key:" + c.getString(0), Toast.LENGTH_LONG).show();
        }
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");

        openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivityAfterLogin) getActivity()).open();
            }
        });

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        lltoplogin.setVisibility(View.GONE);
        lltoplogout.setVisibility(View.VISIBLE);
        tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
        tvlogouttext.setText("Home");
        lltoplogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(in);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);

            }
        });


        list_view_main = (ListView) rootView.findViewById(R.id.list_view_main);
        tvnodatafound = (TextView) rootView.findViewById(R.id.tvnodatafound);
        tvnodata = (TextView) rootView.findViewById(R.id.tvnodata);
        dropdwn = (Spinner) rootView.findViewById(R.id.dropdwn);
        dropdwn.setOnItemSelectedListener(this);
        tvdate = (TextView) rootView.findViewById(R.id.tvdate);
        pbprogressBar = (ProgressBar) rootView.findViewById(R.id.pbprogressBar);

        ConnectionDetector cd = new ConnectionDetector(getActivity());
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getActivity(), "Please Check your network.",
                    Toast.LENGTH_LONG).show();
        } else {
            if (region_name.length() == 0) {
                new HttpAsyncTaskgetursdetails()
                        .execute(domain + "/mobile/pxs_app/service/getursdetails.php");
            } else {
                from = "dropdown";
                new HttpAsyncTaskgetursdetailsforRegion()
                        .execute(domain + "/mobile/pxs_app/service/getursdetails.php");
            }
        }

        new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
				/*ConnectionDetector cd = new ConnectionDetector(getActivity());
				if (!cd.isConnectingToInternet()) {
					Toast.makeText(getActivity(), "Please Check your network.",
							Toast.LENGTH_LONG).show();
				} else {*/
                if (isVisible()) {
                    Log.e("newsfeed", "in side counter");
                    region_name = dropdwn.getSelectedItem().toString();
                    from = "counter";
                    new HttpAsyncTaskgetursdetailsforRegion()
                            .execute(domain + "/mobile/pxs_app/service/getursdetails.php");
                }
                Log.e("newsfeed", "out side counter");
                //}
            }
        }.start();


        return rootView;
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (isFirstTime) {
            isFirstTime = false;
            return;
        }
        Spinner statetypeid = (Spinner) parent;

        if (statetypeid.getId() == R.id.dropdwn) {
            String pos = region_name_array.get(position);
            typeSend = "REGION";
            region_name = dropdwn.getSelectedItem().toString();
            ConnectionDetector cd = new ConnectionDetector(getActivity());
            if (!cd.isConnectingToInternet()) {
                Toast.makeText(getActivity(), "Please Check your network.", Toast.LENGTH_LONG).show();
            } else {
                isFirstTime = true;
                from = "dropdown";
                new HttpAsyncTaskgetursdetailsforRegion().execute(domain + "/mobile/pxs_app/service/getursdetails.php");
                db.execSQL("UPDATE availableurs SET region='" + region_name + "' WHERE region_id='1'");
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }

    public void setMarquee(ArrayList<String> region_name_array2,
                           ArrayList<String> available_array2,
                           ArrayList<String> revision_array2) {

        RelativeLayout parent = (RelativeLayout) rootView.findViewById(R.id.myrecyclerview);
        RecyclerView mRecyclerView = new RecyclerView(getActivity());

        mRecyclerView.setHasFixedSize(true);
        if (!region_name_array2.isEmpty()) {
            tvnodata.setVisibility(View.VISIBLE);
            tvnodata.setText("Current Available URS of");
            dropdwn.setVisibility(View.VISIBLE);
            tvdate.setVisibility(View.VISIBLE);
            //tvnodatafound.setVisibility(View.GONE);
            mAdapter = new MarqueeViewAdapter(region_name_array2, available_array2, revision_array2);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setOverScrollMode(RecyclerView.SCROLL_STATE_SETTLING);
        } else {
            tvnodata.setVisibility(View.VISIBLE);
            tvnodata.setText("Current Available URS");
            dropdwn.setVisibility(View.GONE);
            tvdate.setVisibility(View.GONE);
            tvnodatafound.setVisibility(View.VISIBLE);
            tvnodatafound.setText("Data not found.");
        }

        mLayoutManager = new MyLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.smoothScrollToPosition(Integer.MAX_VALUE);

        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (!mLayoutManager.smoothScroller.isRunning()) {
                        mLayoutManager.againStart();
                    }
                }
                return true;
            }
        });

        parent.addView(mRecyclerView);

        new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
				/*ConnectionDetector cd = new ConnectionDetector(getActivity());
				if (!cd.isConnectingToInternet()) {
					Toast.makeText(getActivity(), "Please Check your network.",
							Toast.LENGTH_LONG).show();
				} else {*/
                if (isVisible()) {
                    Log.e("newsfeed", "in side counter");
                    region_name = dropdwn.getSelectedItem().toString();
                    from = "counter";
                    new HttpAsyncTaskgetursdetailsforRegion()
                            .execute(domain + "/mobile/pxs_app/service/getursdetails.php");
                }
                Log.e("newsfeed", "out side counter");
                //}
            }
        }.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP
                        && keyCode == KeyEvent.KEYCODE_BACK) {
				    /*mainActivityFragment = new MainActivityFragment();
					// Insert the fragment by replacing any existing fragment
					FragmentManager fragmentManager = getActivity().getFragmentManager();
					fragmentManager.beginTransaction()
							.replace(R.id.content_frame, mainActivityFragment)
							.commit();*/
                    Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                    startActivity(in);
                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);

                    return true;

                }

                return false;
            }
        });
    }

    private class HttpAsyncTaskgetursdetails extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AvaiableURS avaiableURS = new AvaiableURS();
            avaiableURS.setType("REGION");
            avaiableURS.setId("NRLDC");
            avaiableURS.setDevice_id(device_id);
            return URSdetailsAPIResponse.POST(urls[0], avaiableURS);
        }

        @Override
        protected void onPreExecute() {
            prgDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            //Toast.makeText(getBaseContext(), "result" + result, Toast.LENGTH_LONG).show();
            AppLogger.showMsg("tagresult", result);
            //prgDialog.hide();
            prgDialog.hide();
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    station_name_Array.clear();
                    available_Array.clear();
                    variable_Array.clear();
                    region_name_array.clear();
                    available_array.clear();
                    select_array.clear();
                    JSONObject mainObject = new JSONObject(result);

                    JSONObject region = mainObject.getJSONObject("region");

                    if (region.has("NRLDC")) {
                        JSONObject NRLDC = region.getJSONObject("NRLDC");
                        if (NRLDC.has("available")) {
                            String available = NRLDC.getString("available");
                            region_name_array.add("NRLDC");
                            available_array.add(available);
                        }
                        if (NRLDC.has("select")) {
                            String select = NRLDC.getString("select");
                            select_array.add(select);
                        }
                        if (NRLDC.has("revision")) {
                            String revision = NRLDC.getString("revision");
                            revision_array.add(revision);
                        }
                    }

                    if (region.has("WRLDC")) {
                        JSONObject WRLDC = region.getJSONObject("WRLDC");
                        if (WRLDC.has("available")) {
                            String available = WRLDC.getString("available");
                            region_name_array.add("WRLDC");
                            available_array.add(available);
                        }
                        if (WRLDC.has("select")) {
                            String select = WRLDC.getString("select");
                            select_array.add(select);
                        }
                        if (WRLDC.has("revision")) {
                            String revision = WRLDC.getString("revision");
                            revision_array.add(revision);
                        }
                    }

                    if (region.has("SRLDC")) {
                        JSONObject SRLDC = region.getJSONObject("SRLDC");
                        if (SRLDC.has("available")) {
                            String available = SRLDC.getString("available");
                            region_name_array.add("SRLDC");
                            available_array.add(available);
                        }
                        if (SRLDC.has("select")) {
                            String select = SRLDC.getString("select");
                            select_array.add(select);
                        }
                        if (SRLDC.has("revision")) {
                            String revision = SRLDC.getString("revision");
                            revision_array.add(revision);
                        }
                    }

                    if (region.has("ERLDC")) {
                        JSONObject ERLDC = region.getJSONObject("ERLDC");
                        if (ERLDC.has("available")) {
                            String available = ERLDC.getString("available");
                            region_name_array.add("ERLDC");
                            available_array.add(available);
                        }
                        if (ERLDC.has("select")) {
                            String select = ERLDC.getString("select");
                            select_array.add(select);
                        }
                        if (ERLDC.has("revision")) {
                            String revision = ERLDC.getString("revision");
                            revision_array.add(revision);
                        }
                    }

                    if (region.has("NERLDC")) {
                        JSONObject NERLDC = region.getJSONObject("NERLDC");
                        if (NERLDC.has("available")) {
                            String available = NERLDC.getString("available");
                            region_name_array.add("NERLDC");
                            available_array.add(available);
                        }
                        if (NERLDC.has("select")) {
                            String select = NERLDC.getString("select");
                            select_array.add(select);
                        }
                        if (NERLDC.has("revision")) {
                            String revision = NERLDC.getString("revision");
                            revision_array.add(revision);
                        }
                    }

                    JSONArray data = mainObject.getJSONArray("data");
                    if (data.length() == 0) {
                        list_view_main.setVisibility(View.GONE);
                        tvnodatafound.setVisibility(View.VISIBLE);
                        tvnodatafound.setText("Data not found.");
                    } else {
                        list_view_main.setVisibility(View.VISIBLE);
                        tvnodatafound.setVisibility(View.GONE);
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject datadata = data.getJSONObject(i);
                            if (datadata.has("station_name")) {
                                String station_name = datadata.getString("station_name");
                                station_name_Array.add(station_name);
                            }
                            if (datadata.has("available")) {
                                String available = datadata.getString("available");
                                available_Array.add(available);
                            }
                            if (datadata.has("variable")) {
                                String variable = datadata.getString("variable");
                                variable_Array.add(variable);
                            }
                            if (datadata.has("rate")) {
                                String rate = datadata.getString("rate");
                                rate_Array.add(rate);
                            }
                        }
                        URSCustomList adapter = new URSCustomList(getActivity(), station_name_Array, rate_Array, available_Array, variable_Array);
                        list_view_main.setAdapter(adapter);
                        Utility.setListViewHeightBasedOnChildren(list_view_main);
                    }

                    adapter_state = new ArrayAdapter<String>(
                            getActivity(),
                            R.layout.spinner_item_state,
                            region_name_array);
                    adapter_state
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dropdwn.setAdapter(adapter_state);
                    select_array.indexOf("YES");
                    dropdwn.setSelection((select_array.indexOf("YES")));
                    String getvalue = dropdwn.getSelectedItem().toString();

                    db.execSQL("DROP TABLE IF EXISTS availableurs");
                    db.execSQL("CREATE TABLE IF NOT EXISTS availableurs(region VARCHAR, available VARCHAR, select_yes VARCHAR, revision VARCHAR, region_id VARCHAR);");
                    db.execSQL("INSERT INTO availableurs VALUES('" + getvalue + "', '" + getvalue + "', '" + getvalue + "','" + getvalue + "', '1')");


                    list_view_main.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            String station_name_Send = station_name_Array.get(position);
                            String available_Send = available_Array.get(position);
                            Intent in = new Intent(getActivity(), CurrentAvaiableURSDetailsActivity.class);
                            in.putExtra("station_name_Send", station_name_Send);
                            in.putExtra("available_Send", available_Send);
                            startActivity(in);
                        }
                    });

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getActivity(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Invalid JSON: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            //test(region_name_array,available_array, revision_array);
            setMarquee(region_name_array, available_array, revision_array);
        }
    }

    private class HttpAsyncTaskgetursdetailsforRegion extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            AvaiableURS avaiableURS = new AvaiableURS();
            avaiableURS.setType("REGION");
            avaiableURS.setId(region_name);
            avaiableURS.setDevice_id(device_id);
            return URSdetailsAPIResponse.POST(urls[0], avaiableURS);
        }


        @Override
        protected void onPreExecute() {
            if (from.equalsIgnoreCase("counter")) {
                pbprogressBar.setVisibility(View.VISIBLE);
            } else if (from.equalsIgnoreCase("dropdown")) {
                prgDialog.show();
            }
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            if (from.equalsIgnoreCase("counter")) {
                pbprogressBar.setVisibility(View.INVISIBLE);
            } else if (from.equalsIgnoreCase("dropdown")) {
                prgDialog.hide();
            }
            AppLogger.showMsg("tag", result);
            boolean firstStringValid = JSONUtils.isJSONValid(result);
            if (firstStringValid == true) {
                try {
                    station_name_Array.clear();
                    available_Array.clear();
                    variable_Array.clear();
                    region_name_array.clear();
                    available_array.clear();
                    select_array.clear();
                    JSONObject mainObject = new JSONObject(result);

                    JSONObject region = mainObject.getJSONObject("region");

                    if (region.has("NRLDC")) {
                        JSONObject NRLDC = region.getJSONObject("NRLDC");
                        if (NRLDC.has("available")) {
                            String available = NRLDC.getString("available");
                            region_name_array.add("NRLDC");
                            available_array.add(available);
                        }
                        if (NRLDC.has("select")) {
                            String select = NRLDC.getString("select");
                            select_array.add(select);
                        }
                        if (NRLDC.has("revision")) {
                            String revision = NRLDC.getString("revision");
                            revision_array.add(revision);
                        }
                    }

                    if (region.has("WRLDC")) {
                        JSONObject WRLDC = region.getJSONObject("WRLDC");
                        if (WRLDC.has("available")) {
                            String available = WRLDC.getString("available");
                            region_name_array.add("WRLDC");
                            available_array.add(available);
                        }
                        if (WRLDC.has("select")) {
                            String select = WRLDC.getString("select");
                            select_array.add(select);
                        }
                        if (WRLDC.has("revision")) {
                            String revision = WRLDC.getString("revision");
                            revision_array.add(revision);
                        }
                    }

                    if (region.has("SRLDC")) {
                        JSONObject SRLDC = region.getJSONObject("SRLDC");
                        if (SRLDC.has("available")) {
                            String available = SRLDC.getString("available");
                            region_name_array.add("SRLDC");
                            available_array.add(available);
                        }
                        if (SRLDC.has("select")) {
                            String select = SRLDC.getString("select");
                            select_array.add(select);
                        }
                        if (SRLDC.has("revision")) {
                            String revision = SRLDC.getString("revision");
                            revision_array.add(revision);
                        }
                    }

                    if (region.has("ERLDC")) {
                        JSONObject ERLDC = region.getJSONObject("ERLDC");
                        if (ERLDC.has("available")) {
                            String available = ERLDC.getString("available");
                            region_name_array.add("ERLDC");
                            available_array.add(available);
                        }
                        if (ERLDC.has("select")) {
                            String select = ERLDC.getString("select");
                            select_array.add(select);
                        }
                        if (ERLDC.has("revision")) {
                            String revision = ERLDC.getString("revision");
                            revision_array.add(revision);
                        }
                    }

                    if (region.has("NERLDC")) {
                        JSONObject NERLDC = region.getJSONObject("NERLDC");
                        if (NERLDC.has("available")) {
                            String available = NERLDC.getString("available");
                            region_name_array.add("NERLDC");
                            available_array.add(available);
                        }
                        if (NERLDC.has("select")) {
                            String select = NERLDC.getString("select");
                            select_array.add(select);
                        }
                        if (NERLDC.has("revision")) {
                            String revision = NERLDC.getString("revision");
                            revision_array.add(revision);
                        }
                    }

                    JSONArray data = mainObject.getJSONArray("data");
                    if (data.length() == 0) {
                        list_view_main.setVisibility(View.GONE);
                        tvnodatafound.setVisibility(View.VISIBLE);
                        tvnodatafound.setText("Data not found.");
                    } else {
                        list_view_main.setVisibility(View.VISIBLE);
                        tvnodatafound.setVisibility(View.GONE);
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject datadata = data.getJSONObject(i);
                            if (datadata.has("station_name")) {
                                String station_name = datadata.getString("station_name");
                                station_name_Array.add(station_name);
                            }
                            if (datadata.has("available")) {
                                String available = datadata.getString("available");
                                available_Array.add(available);
                            }
                            if (datadata.has("variable")) {
                                String variable = datadata.getString("variable");
                                variable_Array.add(variable);
                            }
                            if (datadata.has("rate")) {
                                String rate = datadata.getString("rate");
                                rate_Array.add(rate);
                            }
                        }
                        URSCustomList adapter = new URSCustomList(getActivity(), station_name_Array, rate_Array, available_Array, variable_Array);
                        list_view_main.setAdapter(adapter);
                        Utility.setListViewHeightBasedOnChildren(list_view_main);
                    }

                    if (mainObject.has("date")) {
                        String datefrom = mainObject.getString("date");
                        tvdate.setText("Dated: " + datefrom);
                        tvdate.setSelected(true);
                    }


                    if (from.equalsIgnoreCase("counter")) {

                    } else if (from.equalsIgnoreCase("dropdown")) {

                        adapter_state = new ArrayAdapter<String>(
                                getActivity(),
                                R.layout.spinner_item_state,
                                region_name_array);
                        adapter_state
                                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        dropdwn.setAdapter(adapter_state);
                        select_array.indexOf("YES");
                        dropdwn.setSelection((select_array.indexOf("YES")));
                    }
                    list_view_main.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                final int position, long id) {
                            String station_name_Send = station_name_Array.get(position);
                            String available_Send = available_Array.get(position);
                            Intent in = new Intent(getActivity(), CurrentAvaiableURSDetailsActivity.class);
                            in.putExtra("station_name_Send", station_name_Send);
                            in.putExtra("available_Send", available_Send);
                            startActivity(in);
                        }
                    });

                } catch (JSONException e) {
                    try {
                        JSONObject mainObject = new JSONObject(result);
                        String errormass = mainObject.getString("message");
                        Toast.makeText(getActivity(), errormass, Toast.LENGTH_LONG).show();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    if (result != null) {
                        //Toast.makeText(getApplicationContext(), "500 Internal Server Error, Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        Document html = Jsoup.parse(result);
                        String title = html.title();
                        Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            } else if (result.contains("<html>")) {
                Document html = Jsoup.parse(result);
                String title = html.title();
                Toast.makeText(getActivity(), "Server Side Issue: " + title + ", Please Contact to Admin.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Invalid JSON: Please Contact to Admin.", Toast.LENGTH_LONG).show();
            }
            //test(region_name_array,available_array, revision_array);

            setMarquee(region_name_array, available_array, revision_array);
        }
    }

}
