package com.pts.invetech.dashboardupdate.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.pts.invetech.R;

import java.util.List;

/**
 * Created by vaibhav on 6/1/17.
 */

public class NewsFragAdapter<T> extends BaseAdapter {

    private List<T> listData;
    private Context mContext;

    public NewsFragAdapter(Context _con,List<T> _data){
        listData=_data;
        mContext=_con;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public T getItem(int i) {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        if(convertView==null){
            convertView=LayoutInflater.from(mContext).inflate(R.layout.new_listitem_newsfeed,null);
        }

        T data=getItem(i);



        return convertView;
    }
}
