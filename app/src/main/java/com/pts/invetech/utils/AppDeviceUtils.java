package com.pts.invetech.utils;

import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;


/**
 * Created by vaibhav on 18/10/16.
 */

public class AppDeviceUtils {

    public static String getDeviceId(Context context){

        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static String getTelephonyId(Context context){
        TelephonyManager tManagerr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return tManagerr.getDeviceId();
    }

    public static String getModel(){
        return android.os.Build.MODEL;
    }

    public static String getPlatform(){
        return "Android";
    }

    public static String getVersion(){
        return android.os.Build.VERSION.RELEASE;
    }

    public static String getColorDepth(){
        return "";
    }


}