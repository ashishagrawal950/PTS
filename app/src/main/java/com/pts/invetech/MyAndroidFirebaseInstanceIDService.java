package com.pts.invetech;


import android.content.Context;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.pts.invetech.utils.AppLogger;
import com.pts.model.firebase.RegisterFirebase;
import com.pts.retrofit.ApiClient;
import com.pts.retrofit.ApiInterface;
import com.pts.retrofit.ApiParam;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyAndroidFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyAndroidFCMIIDService";
    public static final String REGISTER = "https://www.mittalpower.com/mobile/gcm_server_php/";
    private static final int MAX_ATTEMPTS = 5;
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    private static final Random random = new Random();

    @Override
    public void onTokenRefresh() {
        String DeviceToken = FirebaseInstanceId.getInstance().getToken();
        AppLogger.showMsg("DeviceToken ==> ", DeviceToken);
    }

    public static void sendData(Context context, String deviceid, String token) {
        AppLogger.showError("tag", deviceid);
        AppLogger.showError("token", token);
        sendRegistrationToServer(context, deviceid, token);
    }

    public static void sendRegistrationToServer(final Context context, String device_id, String token) {
        Map<String,String> param = ApiParam.firebaseAndroid(device_id, token);
        ApiInterface apiService = ApiClient.getAppServiceMethod();
        if (apiService == null) {
            return;
        }
        Call<RegisterFirebase> call = apiService.setNotifyData(param);
        call.enqueue(new Callback<RegisterFirebase>() {
            @Override
            public void onResponse(Call<RegisterFirebase> call, Response<RegisterFirebase> response) {
                if (response.isSuccessful()) {
                    RegisterFirebase data = response.body();
                    AppLogger.show("Firebase API SUCCESS-> " + data.getMessage());
//                    AppLogger.showToastShort(context, data.getMessage());
                    AppLogger.printPostCall(call);
                } else {
                    AppLogger.show("Firebase API FAILURE " + response.toString());
                }
            }

            @Override
            public void onFailure(Call<RegisterFirebase> call, Throwable t) {
                AppLogger.show("Firebase Api ON FAILURE ->  " + t.getMessage());
            }
        });

    }

}