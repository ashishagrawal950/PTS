package com.pts.invetech.NotificationSetting;

import android.view.View;

/**
 * Created by Vaibhav on 26-08-2016.
 */
public interface ToggleDataBridge {

    void onToggleClick(View view, boolean click);
}
