package com.pts.invetech.demoadapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.model.stationmain.Ga;

import java.util.List;

public class DemoListAdapter extends BaseAdapter {

    private Activity context;
    private List<Ga> gaList;

    public DemoListAdapter(Activity context,
                           List<Ga> _gaList) {
        this.context = context;
        gaList = _gaList;
    }
    @Override
    public int getCount() {
        return gaList.size();
    }

    @Override
    public Ga getItem(int position) {
        return gaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if(view==null){
            LayoutInflater inflater = context.getLayoutInflater();
            view= inflater.inflate(R.layout.list_item_main, null, true);

            holder=new ViewHolder();
            holder.txt1= (TextView) view.findViewById(R.id.txt1);
            holder.txt3= (TextView) view.findViewById(R.id.txt3);
            holder.txt4= (TextView) view.findViewById(R.id.txt4);

            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }

        Ga ga=getItem(position);

        holder.txt1.setText(ga.getStationName());
        holder.txt3.setText(ga.getRescheduleToBeneficiary());
        holder.txt4.setText(ga.getUrsRemaining());

        return view;
    }

    class ViewHolder{
        TextView txt1,txt3,txt4;

    }
}