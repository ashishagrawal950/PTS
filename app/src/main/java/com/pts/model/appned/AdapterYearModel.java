package com.pts.model.appned;

/**
 * Created by vaibhav on 1/8/17.
 */

public class AdapterYearModel {

    private String name;
    private String id;
    private boolean isClick=false;

    public AdapterYearModel(String _name,String _id){
        name=_name;
        isClick=false;
        id=_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isClick() {
        return isClick;
    }

    public void setClick(boolean click) {
        isClick = click;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
