package com.pts.invetech.classes.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;
import com.pts.convertor.StateWiseRealConvertor;
import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.handler.NetworkHandlerString;
import com.pts.handler.SharedPrefHandler;
import com.pts.handler.SharedPrefHandlerRealtime;
import com.pts.invetech.APICalling.AppBaseFragment;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.classes.activity.LoginInActivity;
import com.pts.invetech.classes.activity.NewFeedMainActivity;
import com.pts.invetech.classes.activity.PowerDemandGraphActivity;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.customlist.DemandCompareStateAdapter;
import com.pts.invetech.customlist.YearandMontnthCostumAdapter;
import com.pts.invetech.dashboardupdate.background.GpsTrackerUpdate;
import com.pts.invetech.dashboardupdate.model.state.StateList;
import com.pts.invetech.dashboardupdate.views.DashBoardUpdate;
import com.pts.invetech.utils.AppLogger;
import com.pts.model.PowerDataHolder;
import com.pts.model.PowerDemandComparision;
import com.pts.model.PowerDemandOuter;
import com.pts.model.YearandMonthvalue;
import com.pts.model.appned.AdapterStateModel;
import com.pts.model.appned.AdapterYearModel;
import com.pts.model.appned.StateRealTimeModel;
import com.pts.model.testmodel.InnerTableData;
import com.pts.model.testmodel.OuterTableData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Ashish on 26-07-2017.
 */

public class PowerDemandComparisionFragment extends AppBaseFragment implements
        AdapterView.OnItemSelectedListener, View.OnClickListener {

    private final String URL_DEMANDCOMPARISION = "https://www.mittalpower.com/mobile/pxs_app/service/scheduling/demandcomparision.php";
    private final String URL_STATELIST = "https://www.mittalpower.com/mobile/pxs_app/service/getstatenamefromgeo.php";
    private final String URL_YEAR_MONTH = "https://www.mittalpower.com/mobile/pxs_app/service/scheduling/getyearandmonth.php";
    private final int CONSTANT_DEMANDCOMPARISION = 1;
    private final int CONSTANT_STATE = 2;
    private final int CONSTANT_MONTH_YEAR = 3;
    static TextView txt_fromdate;
    static TextView txt_todate;
    private Spinner spn_comparisiontype;
    private LinearLayout txt_selectviewtext;
    private TextView txt_selectstate;
    private ArrayList<String> where = new ArrayList<String>();
    private ArrayList<String> whereshow = new ArrayList<String>();
    private ArrayList<String> stateid = new ArrayList<String>();
    private ArrayList<String> statename = new ArrayList<String>();
    private TextView txt_selecttext;
    private String typeSend;
    private String statevalueset = "", yearandmonthvalueset = "";
    private String typeSendpre;
    private ConnectionDetector cd;
    private Dialog prgDialog;
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private View rootView;
    private SQLiteDatabase db;
    private String device_id;
    private BarChart graph_linechart;
    private LinearLayout formLayout, graphLayout, editBtnLayout;
    private Button btn_go, btn_edit;
    private ListView dialog_ListView;
    private LinearLayout rldata;
    private YearandMontnthCostumAdapter yearandMontnthCostumAdapter;
    private DemandCompareStateAdapter stateListAdapter;
    private RelativeLayout tableholder;
    private TextView tabDemand, tabPeak;
    private int TAB_MODE = 0;
    private int[] GRAPH_COLOR = {Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW, Color.MAGENTA, Color.BLACK, Color.CYAN};
    private ArrayList<PowerDemandOuter> powerDemandComparisiondata;
    private ArrayList<OuterTableData> outerTestList;
    private ArrayList<AdapterStateModel> stateDataList;
    private TextView click_row_opt;
    private ArrayList<String> stateIdList = null;
    private String domain;
    private boolean isFirst = true;
    /*
       Network callbacks
    */
    private NetworkCallBack callBack = new NetworkCallBack() {
        @Override
        public void onResultObject(Object data, int id) {
            if (data == null) {
//                AppLogger.showToastShort(getContext(), "Some Error Occurred in id - " + id);
                return;
            }
            switch (id) {
                case CONSTANT_STATE:

                    StateList stateList = (StateList) data;

                    if (getView() != null) {
                        statevalueset = "";
                        setSpinnerRegion(getView(), stateList);
                        try {
                            SharedPrefHandler.saveStateList(getContext(), stateList.getStates());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }

        @Override
        public void onResultString(String data, int id) {
            if (data == null) {
//                AppLogger.showToastShort(getContext(), "Some Error Occurred in News");
                return;
            }
        }
    };
    private View.OnClickListener tableListner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String data = (String) view.getTag();
            String[] sData = data.split(":");

            if (isDateValid(sData[1])) {
                send2StateWiseRealTime(sData[0], sData[1]);
            }
        }
    };
    private View.OnClickListener tabListner = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (powerDemandComparisiondata == null || powerDemandComparisiondata.isEmpty()) {
                AppLogger.showToastShort(getContext(), getString(R.string.nodata));
                return;
            }

            if (v.getId() == R.id.tab_demand) {
                TAB_MODE = 0;

                tabDemand.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tabDemand.setTextColor(Color.WHITE);
                tabPeak.setTextColor(Color.BLACK);
                tabPeak.setBackgroundColor(Color.WHITE);
            } else if (v.getId() == R.id.tab_peak) {
                TAB_MODE = 1;
                tabPeak.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tabPeak.setTextColor(Color.WHITE);
                tabDemand.setTextColor(Color.BLACK);
                tabDemand.setBackgroundColor(Color.WHITE);
            }

            drawDemandGraph(powerDemandComparisiondata);
            drawTableNew(outerTestList);
            hideViewSelector();
        }
    };
    private NetworkCallBack demandcomparisionCallback = new NetworkCallBack() {
        @Override
        public void onResultString(String data, int id) {
            if (data == null) {
//                AppLogger.showToastShort(getContext(), "Some Error Occurred in id - " + id);
                return;
            }
            switch (id) {
                case CONSTANT_DEMANDCOMPARISION:
                    try {

                        if (typeSend.equalsIgnoreCase("DAY")) {
                            click_row_opt.setVisibility(View.VISIBLE);
                        } else {
                            click_row_opt.setVisibility(View.GONE);
                        }
                        JSONObject object = new JSONObject(data);
                        if (object.has("status")) {
                            String status = object.getString("status");
                            if (status.equalsIgnoreCase("TRUE")) {
                                powerDemandComparisiondata = StateWiseRealConvertor.getPowerDemandComparisiondata(data);
                                if (powerDemandComparisiondata == null || powerDemandComparisiondata.isEmpty()) {
                                    AppLogger.showToastShort(getActivity(), getString(R.string.nodata));
                                }
                                drawDemandGraph(powerDemandComparisiondata);
                                outerTestList = StateWiseRealConvertor.getDataForTable(powerDemandComparisiondata);
                                drawTableNew(outerTestList);
                                hideViewSelector();
                            } else if (status.equalsIgnoreCase("ERR")) {
                                AppLogger.showToastShort(getActivity(), object.getString("message"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        AppLogger.showToastShort(getActivity(), getString(R.string.nodata));
                    }
                    break;

                case CONSTANT_MONTH_YEAR:
                    try {
                        ArrayList<YearandMonthvalue> yearandMonthvalue =
                                StateWiseRealConvertor.getYearandMonthdata(data);
                        if (yearandMonthvalue.isEmpty()) {
                            AppLogger.showToastShort(getContext(), getString(R.string.nodata));
                        } else {
                            setYearandMonth(yearandMonthvalue);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }

    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = getActivity().openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
        if(!isFirst){
            domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_powerdemandcomparision, container, false);
        cd = new ConnectionDetector(getContext());
        click_row_opt = (TextView) rootView.findViewById(R.id.click_row_opt);

        LinearLayout openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NewFeedMainActivity) getActivity()).open();
            }
        });
        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        prgDialog.setCancelable(false);
        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        Intent in = getActivity().getIntent();
        String loginconfig = in.getStringExtra("LOGINCONFIG");
        if (loginconfig.equalsIgnoreCase("LOGIN")) {
            lltoplogin.setVisibility(View.VISIBLE);
            tvlogintext.setPaintFlags(tvlogintext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogintext.setText("Login");
        } else {
            loginconfig.length();
            lltoplogout.setVisibility(View.VISIBLE);
            tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                    | Paint.UNDERLINE_TEXT_FLAG);
            tvlogouttext.setText("Home");
        }

        lltoplogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginInActivity.class);
                i.putExtra("revision", "revision");
                i.putExtra("date", "date");
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                getActivity().finish();
            }
        });

        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                getActivity().finish();
            }
        });

        spn_comparisiontype = (Spinner) rootView.findViewById(R.id.spn_comparisiontype);
        graph_linechart = (BarChart) rootView.findViewById(R.id.graph_linechart);
        graph_linechart.setDescription("");
        graph_linechart.setOnChartGestureListener(new OnChartGestureListener() {
            @Override
            public void onChartGestureStart(MotionEvent motionEvent, ChartTouchListener.ChartGesture chartGesture) {

            }

            @Override
            public void onChartGestureEnd(MotionEvent motionEvent, ChartTouchListener.ChartGesture chartGesture) {

            }

            @Override
            public void onChartLongPressed(MotionEvent motionEvent) {

            }

            @Override
            public void onChartDoubleTapped(MotionEvent motionEvent) {
                move2FullGraph();
            }

            @Override
            public void onChartSingleTapped(MotionEvent motionEvent) {

            }

            @Override
            public void onChartFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {

            }

            @Override
            public void onChartScale(MotionEvent motionEvent, float v, float v1) {

            }

            @Override
            public void onChartTranslate(MotionEvent motionEvent, float v, float v1) {

            }
        });
        ImageView ivgraphclick = (ImageView) rootView.findViewById(R.id.ivgraphclick);
        //ivgraphclick.setOnClickListener(v -> move2FullGraph());
        ivgraphclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                move2FullGraph();
            }
        });
        formLayout = (LinearLayout) rootView.findViewById(R.id.formLayout);
        graphLayout = (LinearLayout) rootView.findViewById(R.id.graphLayout);
        editBtnLayout = (LinearLayout) rootView.findViewById(R.id.editBtnLayout);
        btn_go = (Button) rootView.findViewById(R.id.btn_go);
        btn_go.setOnClickListener(this);
        btn_edit = (Button) rootView.findViewById(R.id.btn_edit);
        btn_edit.setOnClickListener(this);
        txt_selectviewtext = (LinearLayout) rootView.findViewById(R.id.txt_selectviewtext);

        txt_selecttext = (TextView) rootView.findViewById(R.id.txt_selecttext);
        txt_selecttext.setOnClickListener(this);

//        txt_multipleyear = (TextView) rootView.findViewById(R.id.txt_multipleyear);
//        txt_multipleyear.setOnClickListener(this);
        rldata = (LinearLayout) rootView.findViewById(R.id.rldata);
        txt_fromdate = (TextView) rootView.findViewById(R.id.txt_fromdate);
        txt_fromdate.setOnClickListener(this);

        txt_todate = (TextView) rootView.findViewById(R.id.txt_todate);
        txt_todate.setOnClickListener(this);

        txt_selectstate = (TextView) rootView.findViewById(R.id.txt_selectstate);
        txt_selectstate.setOnClickListener(this);

        spinnerComparisionType();

        tabDemand = (TextView) rootView.findViewById(R.id.tab_demand);
        tabDemand.setOnClickListener(tabListner);

        tabPeak = (TextView) rootView.findViewById(R.id.tab_peak);
        tabPeak.setOnClickListener(tabListner);

        tableholder = (RelativeLayout) rootView.findViewById(R.id.tableholder);

        String data = SharedPrefHandlerRealtime.getPowerDemandData(getContext());
        typeSendpre = SharedPrefHandlerRealtime.getTypesend(getContext());

        if (data != null) {
            PowerDataHolder powerData = new Gson().fromJson(data, PowerDataHolder.class);
            powerDemandComparisiondata = powerData.getPowerDemandComparisiondata();
            outerTestList = powerData.getOuterTestList();
            stateDataList = powerData.getStateDataList();

            if (powerData.getFrmDate() != null && powerData.getToDate() != null) {
                txt_fromdate.setText(powerData.getFrmDate());
                txt_todate.setText(powerData.getToDate());
            } else {
                txt_selecttext.setText(powerData.getYear());
            }

            txt_selectstate.setText(powerData.getStates());
            spn_comparisiontype.setSelection(powerData.getPosition());
            stateIdList = powerData.getIdLists();

            drawDemandGraph(powerDemandComparisiondata);
            drawTableNew(outerTestList);
            click_row_opt.setVisibility(View.VISIBLE);
            hideViewSelector();

            SharedPrefHandlerRealtime.removePowerDemand(getContext());
        }


        return rootView;
    }

    private void move2FullGraph() {
        if (powerDemandComparisiondata == null || powerDemandComparisiondata.isEmpty()) {
            AppLogger.showToastShort(getActivity(), getString(R.string.nodata));
            return;
        }
        Intent intent = new Intent(getActivity(), PowerDemandGraphActivity.class);
        intent.putParcelableArrayListExtra("graphdata", powerDemandComparisiondata);
        intent.putParcelableArrayListExtra("tableData", outerTestList);

        startActivity(intent);
    }

    private void spinnerComparisionType() {

        List<String> categories = new ArrayList<String>();
        categories.add("DAY WISE");
        categories.add("YEAR WISE");
        categories.add("MONTH WISE");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spn_comparisiontype.setAdapter(dataAdapter);

        spn_comparisiontype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                typeSend = adapterView.getSelectedItem().toString();
                //AppLogger.showToastLong(getContext(), typeSend);
                if (typeSend.equalsIgnoreCase("DAY WISE")) {
                    rldata.setVisibility(View.VISIBLE);
                    txt_selectviewtext.setVisibility(View.GONE);
                    txt_selecttext.setVisibility(View.GONE);
//                    txt_multipleyear.setVisibility(View.GONE);
                } else {
                    rldata.setVisibility(View.GONE);
                    txt_selectviewtext.setVisibility(View.VISIBLE);
                    txt_selecttext.setVisibility(View.VISIBLE);
//                    txt_multipleyear.setVisibility(View.VISIBLE);
                    yearandmonthvalueset = "";
                    getMonthandYear(typeSend, device_id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getMonthandYear(String typeSend, String device_id) {
        if (typeSend.equalsIgnoreCase("DAY WISE")) {
            typeSend = "DAY";
        } else if (typeSend.equalsIgnoreCase("YEAR WISE")) {
            typeSend = "YEAR";
        } else if (typeSend.equalsIgnoreCase("MONTH WISE")) {
            typeSend = "MONTH";
        }
        JSONObject obj = new JSONObject();
        try {
            obj.put("type", typeSend);
            obj.put("device_id", device_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            if(isFirst){
                new NetworkHandlerString(getContext(), demandcomparisionCallback, CONSTANT_MONTH_YEAR)
                        .execute(URL_YEAR_MONTH, obj.toString());
            }else{
                new NetworkHandlerString(getContext(), demandcomparisionCallback, CONSTANT_MONTH_YEAR)
                        .execute(domain + "/mobile/pxs_app/service/scheduling/getyearandmonth.php", obj.toString());
            }

        }
    }

    private void getStatesWoLoc(int stateId, String type) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("state_id", stateId);
            obj.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            if(isFirst){
                new NetworkHandlerModel(getContext(), callBack, StateList.class, CONSTANT_STATE).execute(URL_STATELIST, obj.toString());
            }
            else{
                new NetworkHandlerModel(getContext(), callBack, StateList.class, CONSTANT_STATE).execute(domain + "/mobile/pxs_app/service/getstatenamefromgeo.php", obj.toString());
            }

        }
    }

    private void getStateList(Location loc) throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("latitude", loc.getLatitude());
        obj.put("logtitude", loc.getLongitude());
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            if(isFirst){
                new NetworkHandlerModel(getContext(), callBack, StateList.class, CONSTANT_STATE).execute(URL_STATELIST, obj.toString());
            }else{
                new NetworkHandlerModel(getContext(), callBack, StateList.class, CONSTANT_STATE).execute(domain + "/mobile/pxs_app/service/getstatenamefromgeo.php", obj.toString());
            }

        }
    }

    private void setSpinnerRegion(View view, StateList stateLists) {

        stateid.clear();
        statename.clear();

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.dialog_state);
        dialog.setTitle("");
        dialog.show();

        dialog_ListView = (ListView) dialog.findViewById(R.id.lv_dialog_state);

        stateDataList = new ArrayList<>();

        for (int i = 0; i < stateLists.getStates().size(); i++) {
            stateDataList.add(new AdapterStateModel(stateLists.getStates().get(i).getStateName(),
                    stateLists.getStates().get(i).getId(),
                    stateLists.getStates().get(i).getRegion()));
        }

        stateListAdapter = new DemandCompareStateAdapter(getContext(), stateDataList);
        dialog_ListView.setAdapter(stateListAdapter);

        Button declineButton = (Button) dialog.findViewById(R.id.ok);

        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sName = stateListAdapter.getAllStateName();
                txt_selectstate.setText(sName);
                dialog.dismiss();
            }
        });

    }

    private void getPowerDemandComparision(String day, ArrayList<String> stateid,
                                           ArrayList<String> where, String device_id) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("type", day);
            obj.put("stateid", new JSONArray(stateid));
            obj.put("where", new JSONArray(where));
            obj.put("device_id", device_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            if(isFirst){
                new NetworkHandlerString(getContext(), demandcomparisionCallback, CONSTANT_DEMANDCOMPARISION)
                        .execute(URL_DEMANDCOMPARISION, obj.toString());
            }else{
                new NetworkHandlerString(getContext(), demandcomparisionCallback, CONSTANT_DEMANDCOMPARISION)
                        .execute(domain + "/mobile/pxs_app/service/scheduling/demandcomparision.php", obj.toString());
            }

        }
    }

    private void drawTableNew(ArrayList<OuterTableData> outerTestList) {

        TableLayout tableLay = (TableLayout) tableholder.findViewById(R.id.tablelayout);
        tableLay.removeAllViews();

        TableRow.LayoutParams mParams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 1f);

//        float paddingPx = 4f;
//        int paddingDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
//                paddingPx, getResources().getDisplayMetrics());

        TableRow headerRow = new TableRow(getContext());

        TextView outtt = new TextView(getContext());
        if (typeSend == null) {
            outtt.setText("State / " + "DAY");
        } else {
            outtt.setText("State / " + typeSend);
        }
        outtt.setMaxLines(1);
        outtt.setBackgroundResource(R.drawable.valuecellborder);
//      outtt.setPadding(0,paddingDp,0,paddingDp);
        outtt.setTextColor(Color.WHITE);
        outtt.setGravity(Gravity.CENTER);
        headerRow.addView(outtt, mParams);
        ArrayList<InnerTableData> tmpInnList = outerTestList.get(0).getInnerModelList();
        for (InnerTableData inMod : tmpInnList) {
            TextView testVu = new TextView(getContext());
            testVu.setMaxLines(1);
            testVu.setTypeface(null, Typeface.BOLD);
            testVu.setBackgroundResource(R.drawable.valuecellborder);
            testVu.setTextColor(Color.WHITE);
            testVu.setGravity(Gravity.CENTER);
//            testVu.setPadding(0,paddingDp,0,paddingDp);
            testVu.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);
            testVu.setText(String.valueOf(inMod.getYear()));
            headerRow.addView(testVu, mParams);
        }

        tableLay.addView(headerRow);
        for (OuterTableData oTest : outerTestList) {
            TableRow innRow = new TableRow(getContext());

            TextView stateName = new TextView(getContext());
            stateName.setText(oTest.getStateName());
            stateName.setMaxLines(1);

            stateName.setTypeface(null, Typeface.BOLD);
            stateName.setBackgroundResource(R.drawable.valuecellborder);
//           stateName.setPadding(0,paddingDp,0,paddingDp);
            stateName.setTextColor(Color.WHITE);
            stateName.setGravity(Gravity.CENTER);

            innRow.addView(stateName, mParams);

            ArrayList<InnerTableData> innMod = oTest.getInnerModelList();

            for (InnerTableData tmpInn : innMod) {

                TextView innDmd = new TextView(getContext());

                if (TAB_MODE == 0) {
                    innDmd.setText(tmpInn.getDemand());
                } else {
                    innDmd.setText(tmpInn.getPeak());
                }
                innDmd.setMaxLines(1);
                innDmd.setGravity(Gravity.CENTER);
                innDmd.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);
                innDmd.setBackgroundResource(R.drawable.valuecellborder_white);
                innDmd.setTag(oTest.getStateId() + ":" + tmpInn.getYear());
                innDmd.setOnClickListener(tableListner);
//                innDmd.setPadding(0,paddingDp,0,paddingDp);

                innRow.addView(innDmd, mParams);
            }

            tableLay.addView(innRow);
        }

    }

    private boolean isDateValid(String sDate) {
        String sdf = "yyyy-MM-dd";
        SimpleDateFormat format = new SimpleDateFormat(sdf);

        try {
            Date dd = format.parse(sDate);
            return true;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void setYearandMonth(ArrayList<YearandMonthvalue> yearandMonthvalue) {
        where.clear();
        whereshow.clear();
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(R.layout.dialog_year);
        dialog.show();
        dialog_ListView = (ListView) dialog.findViewById(R.id.listview_dialog);
        ArrayList<AdapterYearModel> adapterList = new ArrayList<>();

        for (int i = 0; i < yearandMonthvalue.size(); i++) {
            adapterList.add(new AdapterYearModel(yearandMonthvalue.get(i).getData(),
                    yearandMonthvalue.get(i).getVal()));
        }

        yearandMontnthCostumAdapter = new YearandMontnthCostumAdapter(getContext(), adapterList);

        dialog_ListView.setAdapter(yearandMontnthCostumAdapter);

        Button declineButton = (Button) dialog.findViewById(R.id.ok);
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_selectviewtext.setVisibility(View.VISIBLE);
                txt_selecttext.setVisibility(View.VISIBLE);
                if (yearandMontnthCostumAdapter != null) {
                    String str = yearandMontnthCostumAdapter.getAllYearsNames();
                    txt_selecttext.setText(str);
                }
                dialog.dismiss();

            }
        });
    }

    private void drawDemandGraph(ArrayList<PowerDemandOuter> powerDemandComparisiondata) {

        ArrayList<String> labelsStates = new ArrayList<String>();

        ArrayList<ArrayList<BarEntry>> entryHolder = new ArrayList<>();

        for (int index = 0; index < powerDemandComparisiondata.size(); index++) {
            PowerDemandOuter oTst = powerDemandComparisiondata.get(index);
            ArrayList<BarEntry> tmpArr = new ArrayList<>();
            ArrayList<PowerDemandComparision> iModl = oTst.getPdCompareList();

            for (int y = 0; y < iModl.size(); y++) {
                PowerDemandComparision iiiMod = iModl.get(y);
                if (TAB_MODE == 0) {
                    tmpArr.add(new BarEntry(Float.valueOf(iiiMod.getDemand()), y));
                } else {
                    tmpArr.add(new BarEntry(Float.valueOf(iiiMod.getPeak()), y));
                }
                if (!labelsStates.contains(iiiMod.getState())) {
                    labelsStates.add(iiiMod.getState());
                }
            }
            entryHolder.add(tmpArr);
        }

        ArrayList<BarDataSet> aLineDataList = new ArrayList<>();

        for (int index = 0; index < entryHolder.size(); index++) {
            int cCode = GRAPH_COLOR[index % GRAPH_COLOR.length];
            BarDataSet dataset_first = new BarDataSet(entryHolder.get(index), powerDemandComparisiondata.get(index).getYear());
            //dataset_first.setCircleColor(cCode);
            dataset_first.setDrawValues(false);
            dataset_first.setColor(cCode);
            aLineDataList.add(dataset_first);
        }

        BarData data_weekly = new BarData(labelsStates, aLineDataList);
        data_weekly.setValueTextSize(10);

        graph_linechart.setData(data_weekly);
        graph_linechart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry entry, int i, Highlight highlight) {
                if (entry != null) {
                    AppLogger.showToastShort(getContext(), String.valueOf(entry.getVal()));
                }
            }

            @Override
            public void onNothingSelected() {

            }
        });
        // graph_linechart.getAxisLeft().setEnabled(false);
        graph_linechart.getAxisRight().setEnabled(false);
        graph_linechart.getLegend().setEnabled(true);
        graph_linechart.setDrawGridBackground(false);
        graph_linechart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        graph_linechart.invalidate();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void hideViewSelector() {
        editBtnLayout.setVisibility(View.VISIBLE);
        formLayout.setVisibility(View.GONE);
        graphLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        if (v == btn_go) {
            typeSend = spn_comparisiontype.getSelectedItem().toString();
            if (typeSend.equalsIgnoreCase("DAY WISE")) {
                typeSend = "DAY";
            } else if (typeSend.equalsIgnoreCase("YEAR WISE")) {
                typeSend = "YEAR";
            } else if (typeSend.equalsIgnoreCase("MONTH WISE")) {
                typeSend = "MONTH";
            }
            if (typeSend.equalsIgnoreCase("DAY")) {
                where.clear();
                String from = txt_fromdate.getText().toString();
                if (from.equalsIgnoreCase("")) {
                } else {
                    String[] fromparts = from.split("-");
                    String frompart1 = fromparts[0];
                    String frompart2 = fromparts[1];
                    String frompart3 = fromparts[2];
                    where.add(frompart3 + "-" + frompart2 + "-" + frompart1);
                }
                String to = txt_todate.getText().toString();
                if (to.equalsIgnoreCase("")) {
                } else {
                    String[] toparts = to.split("-");
                    String topart1 = toparts[0];
                    String topart2 = toparts[1];
                    String topart3 = toparts[2];
                    where.add(topart3 + "-" + topart2 + "-" + topart1);
                }

                if (stateListAdapter == null && stateIdList == null) {
                    AppLogger.showToastShort(getContext(), getString(R.string.select_all));
                    return;
                }

                if (stateListAdapter != null) {
                    stateIdList = stateListAdapter.getStateIds();
                }

                List<Date> dates = new ArrayList<Date>();
                ArrayList<String> date_array = new ArrayList<>();

                String str_date = "", end_date = "";
                Date startDate = null, endDate = null;
                if ((from.equalsIgnoreCase(""))) {
                    //AppLogger.showToastLong(getContext(), getString(R.string.select_all));
                } else if ((to.equalsIgnoreCase(""))) {
                    //AppLogger.showToastLong(getContext(), getString(R.string.select_all));
                } else {
                    str_date = from;
                    end_date = to;
                    DateFormat formatter;
                    formatter = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        startDate = (Date) formatter.parse(str_date);
                        endDate = (Date) formatter.parse(end_date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
                    long endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
                    long curTime = startDate.getTime();
                    while (curTime <= endTime) {
                        dates.add(new Date(curTime));
                        curTime += interval;
                    }
                    for (int i = 0; i < dates.size(); i++) {
                        Date lDate = (Date) dates.get(i);
                        String ds = formatter.format(lDate);
                        System.out.println(" Date is ..." + ds);
                        date_array.add(ds);
                    }
                }

                if (stateIdList.isEmpty() || where.size() == 0 || where.size() == 1) {
                    AppLogger.showToastShort(getContext(), getString(R.string.select_all));
                } else if (date_array.size() == 0) {
                    AppLogger.showToastShort(getContext(), "To date should be greater than or equal to From Date.");
                } else {

                    getPowerDemandComparision(typeSend, stateIdList, where, device_id);
                }
            } else {
                if ((yearandMontnthCostumAdapter == null) || (stateListAdapter == null && stateIdList == null)) {
                    AppLogger.showToastShort(getContext(), getString(R.string.select_all));
                    return;
                }
                ArrayList<String> year = yearandMontnthCostumAdapter.getYrlist();
                ArrayList<String> state;
                if (stateListAdapter != null) {
                    state = stateListAdapter.getStateIds();
                } else if (stateIdList != null) {
                    state = stateIdList;
                } else {
                    state = new ArrayList<>();
                }


                if (year.isEmpty() || state.isEmpty()) {
                    AppLogger.showToastShort(getContext(), getString(R.string.select_all));
                } else {
                    getPowerDemandComparision(typeSend, state, year, device_id);
                }
            }
        } else if (v == txt_selecttext) {
            yearandmonthvalueset = "";
            typeSend = spn_comparisiontype.getSelectedItem().toString();
            getMonthandYear(typeSend, device_id);
        } else if (v == btn_edit) {
            editBtnLayout.setVisibility(View.GONE);
            formLayout.setVisibility(View.VISIBLE);
            graphLayout.setVisibility(View.GONE);
        } else if (v == txt_fromdate) {
            DialogFragment newFragment = new SelectFromDateFragment();
            newFragment.show(getFragmentManager(), "DatePicker");
        } else if (v == txt_todate) {
            DialogFragment newFragment = new SelectToDateFragment();
            newFragment.show(getFragmentManager(), "DatePicker");
        } else if (v == txt_selectstate) {
            int stateId = SharedPrefHandler.getIntData(getActivity().getApplicationContext(), getString(R.string.db_stateid));
            if (stateId < 0) {
                Location location = GpsTrackerUpdate.getLastLocation(getActivity());
                if (location == null) {
                    SharedPrefHandler.saveInt(getActivity().getApplicationContext(), getString(R.string.db_stateid), 1);
                    getStatesWoLoc(1, "IEX");
                } else {
                    try {
                        getStateList(location);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                getStatesWoLoc(stateId, "IEX");
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    DashBoardUpdate dashBoardUpdate = new DashBoardUpdate();
                    if (dashBoardUpdate != null) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.content_frame, dashBoardUpdate);
                        fragmentTransaction.commit();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    public void send2StateWiseRealTime(String stateId, String date) {

        PowerDataHolder dataHolder = new PowerDataHolder();
        dataHolder.setOuterTestList(outerTestList);
        dataHolder.setPowerDemandComparisiondata(powerDemandComparisiondata);
        dataHolder.setStateDataList(stateDataList);
        int pos = spn_comparisiontype.getSelectedItemPosition();

        if (pos == 0) {
            String start = txt_fromdate.getText().toString();
            String end = txt_todate.getText().toString();

            dataHolder.setFrmDate(start);
            dataHolder.setToDate(end);
        } else {
            String year = txt_selecttext.getText().toString();
            dataHolder.setYear(year);

        }
        String stateList = txt_selectstate.getText().toString();
        dataHolder.setStates(stateList);
        dataHolder.setIdLists(stateIdList);

        String data = dataHolder.getDataInJson();

        SharedPrefHandlerRealtime.savePowerDemandData(getContext(), data, typeSend);

        String region = getRegion(stateId);
        StateRealTimeModel stateRealTimeModel = new StateRealTimeModel();
        stateRealTimeModel.setStateId(stateId);
        stateRealTimeModel.setDate(date);
        stateRealTimeModel.setRegion(region);

        Bundle bundle = new Bundle();
        bundle.putParcelable("powerdata", stateRealTimeModel);
        bundle.putString("from", "Powerdemandcomparision");

        StatewiseRealtimeSchedulingFragment fragobj = new StatewiseRealtimeSchedulingFragment();
        fragobj.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragobj);
        fragmentTransaction.commit();
    }

    private String getRegion(String id) {
        for (AdapterStateModel stateModel : stateDataList) {
            if (stateModel.getId().equalsIgnoreCase(id)) {
                return stateModel.getRegion();
            }
        }
        return null;
    }

    public static class SelectFromDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getContext(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                txt_fromdate.setText(dayplace + "-" + monthplace + "-" + year);
            } else if (month <= 9) {
                String monthplace = "0" + month;
                txt_fromdate.setText(day + "-" + monthplace + "-" + year);
            } else if (day < 10) {
                // String dayplace = "0" + day;
                txt_fromdate.setText(day + "-" + month + "-" + year);
            } else {
                txt_fromdate.setText(day + "-" + month + "-" + year);
            }
            // fromDateEtxt.setText(month + "/" + day + "/" + year);
        }

    }

    public static class SelectToDateFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getContext(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            if ((month <= 9) && (day < 10)) {
                String monthplace = "0" + month;
                String dayplace = "0" + day;
                txt_todate.setText(dayplace + "-" + monthplace + "-" + year);
            } else if (month <= 9) {
                String monthplace = "0" + month;
                txt_todate.setText(day + "-" + monthplace + "-" + year);
            } else if (day < 10) {
                txt_todate.setText(day + "-" + month + "-" + year);
            } else {
                txt_todate.setText(day + "-" + month + "-" + year);
            }
        }
    }

}