package com.pts.invetech.apiresponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.util.Log;

import com.pts.invetech.pojo.AddBidSubmit;
import com.pts.invetech.utils.AppLogger;

public class AddBidRECGetallAPIResponse {
	public static String POST(String url, AddBidSubmit addBidSubmit){
        InputStream inputStream = null;
        String result = "";
        try {
 
            // 1. create HttpClient
            @SuppressWarnings("deprecation")
			HttpClient httpclient = new DefaultHttpClient();
 
            // 2. make POST request to the given URL
            @SuppressWarnings("deprecation")
			HttpPost httpPost = new HttpPost(url);
            AppLogger.showError("url",url);
            String json = "";
 
            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("type", addBidSubmit.getType());
            jsonObject.accumulate("ordernature", addBidSubmit.getBidtype());
            jsonObject.accumulate("date", addBidSubmit.getBiddate());
            jsonObject.accumulate("access_key", addBidSubmit.getAddbidsaveaccesskey());
     
            // 4. convert JSONObject to JSON to String
            json = jsonObject.toString();
            AppLogger.showError("params",json);
            // ** Alternative way to convert Person object to JSON string usin Jackson Lib 
            // ObjectMapper mapper = new ObjectMapper();
            // json = mapper.writeValueAsString(person); 
 
            // 5. set json to StringEntity
		    StringEntity se = new StringEntity("data="+json);
		    
		    // 6. set httpPost Entity
		    httpPost.setEntity(se);
		    
		    // 7. Set some headers to inform server about the type of the content   
		    httpPost.setHeader("Accept", "*/*");
		    httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
 
            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);
 
            // 9. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();
 
            // 10. convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
 
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
 
        // 11. return result
        return result;
    }
	
	 private static String convertInputStreamToString(InputStream inputStream) throws IOException{
	        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	        String line = "";
	        String result = "";
	        while((line = bufferedReader.readLine()) != null)
	            result += line;
	 
	        inputStream.close();
	        return result;
	    }  
}
