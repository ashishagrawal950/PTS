package com.pts.invetech.module.myschedule_delete;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

/**
 * Created by vaibhav on 30/3/18.
 */

public class AlertDFragment<T>  extends DialogFragment {

    ICommunicatorBundle<T> iBundle;
    private T data;

    public AlertDFragment(){

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if(getArguments()!=null){
            iBundle=getArguments().getParcelable("key");
            data=getArguments().getParcelable("data");
        }else{
            return null;
        }

        return new AlertDialog.Builder(getActivity())
                .setTitle("Delete Filer")
                .setMessage("Are you sure to delete Filter ?")
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                if(iBundle!=null){
                                    iBundle.onDataSend(data);
                                }
                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,	int which) {
                        if(iBundle!=null){

                        }
                    }
                }).create();
    }
}