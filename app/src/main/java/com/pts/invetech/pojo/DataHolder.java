package com.pts.invetech.pojo;

import java.util.ArrayList;

/**
 * Created by Vaibhav on 22-08-2016.
 */
public class DataHolder {

    private String date="";
    private String region="";
    private String revision="";
    private String importType="";
    private ArrayList<String> revisionList=new ArrayList<>();


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public String getImportType() {
        return importType;
    }

    public void setImportType(String importType) {
        this.importType = importType;
    }

    public ArrayList<String> getRevisionList() {
        return revisionList;
    }

    public void setRevisionList(ArrayList<String> revisionList) {
        this.revisionList = revisionList;
    }
}
