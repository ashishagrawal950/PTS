package com.pts.invetech.dashboardupdate.model.dashboard;

/**
 * Created by vaibhav on 12/1/17.
 */

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pts.invetech.utils.AppLogger;

public class Stateload  implements Parcelable {

    @SerializedName("WRLDC")
    @Expose
    private List<RegionLdc> wRLDC = null;

    @SerializedName("NRLDC")
    @Expose
    private List<RegionLdc> nRLDC = null;

    public List<RegionLdc> getWRLDC() {
        return wRLDC;
    }

    public void setWRLDC(List<RegionLdc> wRLDC) {
        this.wRLDC = wRLDC;
    }

    public List<RegionLdc> getNRLDC() {
        return nRLDC;
    }

    public void setNRLDC(List<RegionLdc> nRLDC) {
        this.nRLDC = nRLDC;
    }

    public String getLdcDate(){
      return wRLDC.get(0).getTime();
    }

   /* public String getLdcTime(){
        String[] stamp=wRLDC.get(0).getDate().split(" ");
        AppLogger.showMsgWithoutTag(wRLDC.get(0).getDate());
        return stamp[1];
    }*/

    protected Stateload(Parcel in) {
        if (in.readByte() == 0x01) {
            wRLDC = new ArrayList<RegionLdc>();
            in.readList(wRLDC, RegionLdc.class.getClassLoader());
        } else {
            wRLDC = null;
        }
        if (in.readByte() == 0x01) {
            nRLDC = new ArrayList<RegionLdc>();
            in.readList(nRLDC, RegionLdc.class.getClassLoader());
        } else {
            nRLDC = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (wRLDC == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(wRLDC);
        }
        if (nRLDC == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(nRLDC);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Stateload> CREATOR = new Parcelable.Creator<Stateload>() {
        @Override
        public Stateload createFromParcel(Parcel in) {
            return new Stateload(in);
        }

        @Override
        public Stateload[] newArray(int size) {
            return new Stateload[size];
        }
    };
}
