package com.pts.invetech.dashboardupdate.model.graphs;

import android.graphics.Color;

import com.github.mikephil.charting.data.LineData;

/**
 * Created by vaibhav on 12/1/17.
 */

public class GraphLine {

    private String title;
    private LineData lineData;
    private boolean[] gHolder = {true, true};
    private int[] colorArr={Color.WHITE,Color.WHITE};


    public void setColorArr(int[] _arr){
        this.colorArr=_arr;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LineData getLineData() {
        return lineData;
    }

    public void setLineData(LineData lineData) {
        this.lineData = lineData;
    }

    public LineData getLineDataForGraph() {

        if (gHolder[0] && gHolder[1]) {
            return lineData;
        }

        if (gHolder[0]) {
            return new LineData(lineData.getXVals(), lineData.getDataSetByIndex(0));
        }

        if (gHolder[1]) {
            return new LineData(lineData.getXVals(), lineData.getDataSetByIndex(1));
        }

        return null;
    }

    public void updateArr(int val) {

        if (!gHolder[0] && !gHolder[1]) {
            gHolder[0] = true;
            gHolder[1] = true;
            return;
        }

        if (val == 1) {
            if(!gHolder[1]){
                gHolder[0] = true;
                gHolder[1] = true;
                return;
            }
            gHolder[0] = !gHolder[0];
            return;
        }

        if (val == 2) {
            if(!gHolder[0]){
                gHolder[0] = true;
                gHolder[1] = true;
                return;
            }
            gHolder[1] = !gHolder[1];
            return;
        }
    }

    public int getDemandColor(){
        if(gHolder[0]){
            return colorArr[0];
        }
        return Color.GRAY;
    }

    public int getSchColor(){
        if(gHolder[1]){
            return colorArr[1];
        }
        return Color.GRAY;
    }
}
