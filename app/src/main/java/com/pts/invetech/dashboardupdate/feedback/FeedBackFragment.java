package com.pts.invetech.dashboardupdate.feedback;

/**
 * Created by vaibhav on 12/1/17.
 */
import android.app.Fragment;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.invetech.R;
import com.pts.invetech.utils.AppDeviceUtils;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.AppStringUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class FeedBackFragment extends Fragment implements View.OnClickListener{

    private EditText mName,mMail,mNumber,mData;
    private final String URL_FEEDBACK="https://www.mittalpower.com/mobile/pxs_app/service/submitfeedback.php";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_feedback, container, false);

        mName = (EditText) view.findViewById(R.id.feedback_name);
        mMail= (EditText) view.findViewById(R.id.feedback_mail);
        mNumber= (EditText) view.findViewById(R.id.feedback_number);
        mData= (EditText) view.findViewById(R.id.feedback_data);

        Button send= (Button) view.findViewById(R.id.feedback_send);
        send.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.feedback_send){
            verifyFeedback();
        }
    }

    private void verifyFeedback() {
        String name=mName.getText().toString();
        String email=mMail.getText().toString();
        String number=mNumber.getText().toString();
        String data=mData.getText().toString();

        if(AppStringUtils.isTextEmpty(name)){
            AppLogger.showToastLong(getActivity(),getString(R.string.error_empty_name));
            return;
        }

        if(!AppStringUtils.isValidPhoneNumber(number)){
            AppLogger.showToastLong(getActivity(),getString(R.string.error_empty_number));
            return;
        }

        if(!AppStringUtils.isEmailValid(email)){
            AppLogger.showToastLong(getActivity(),getString(R.string.error_empty_mail));
            return;
        }

        if(AppStringUtils.isTextLess(data)){
            AppLogger.showToastLong(getActivity(),getString(R.string.error_empty_data));
            return;
        }

        try {
            sendFeedback(name,email,number,data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void sendFeedback(String name, String email, String number, String data) throws JSONException {
        JSONObject object=new JSONObject();

        object.put("device_ip", AppDeviceUtils.getTelephonyId(getActivity()));
        object.put("name",name);
        object.put("mobile",number);
        object.put("email",email);
        object.put("feedback",data);

        new NetworkHandlerModel(getActivity(), callBack, FeedbackData.class, 1).execute(URL_FEEDBACK, object.toString());
    }

    private NetworkCallBack callBack=new NetworkCallBack(){

        @Override
        public void onResultObject(Object data, int id) {
            if(data==null){
                AppLogger.showToastLong(getActivity(),"Some Error occurred while sending mail.");
                return;
            }

            FeedbackData feedback= (FeedbackData) data;

            AppLogger.showToastLong(getActivity(),feedback.getMessage());
        }
    };
}