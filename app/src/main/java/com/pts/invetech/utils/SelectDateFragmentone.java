package com.pts.invetech.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Ashish on 21-02-2017.
 */

public class SelectDateFragmentone extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        populateSetDate(yy, mm + 1, dd);
    }

    public void populateSetDate(int year, int month, int day) {
        if ((month <= 9) && (day < 10)) {
            String monthplace = "0" + month;
            String dayplace = "0" + day;
            //tv_datepickerone.setText(dayplace + "-" + monthplace + "-" + year);
            //dateone = (year + "-" + monthplace + "-" + dayplace);
            // Toast.makeText(getActivity(), "DownloadResult" +
            // as,Toast.LENGTH_LONG).show();
        }
        else if (month <= 9) {
            String monthplace = "0" + month;
            //tv_datepickerone.setText(day + "-" + monthplace + "-" + year);
           // dateone = (year + "-" + monthplace + "-" + day);
        }
        else if (day < 10) {
            String dayplace = "0" + day;
           // tv_datepickerone.setText(dayplace + "-" + month + "-" + year);
          //  dateone = (year + "-" + month + "-" + dayplace);
        }
        else {
          //  tv_datepickerone.setText(day + "-" + month + "-" + year);
          //  dateone =  (year + "-" + month + "-" + day);
        }
    }
}