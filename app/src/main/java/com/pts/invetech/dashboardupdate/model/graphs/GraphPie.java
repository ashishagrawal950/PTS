package com.pts.invetech.dashboardupdate.model.graphs;

import com.github.mikephil.charting.data.PieData;

/**
 * Created by vaibhav on 12/1/17.
 */

public class GraphPie {

    private String title;
    private PieData data;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public PieData getData() {
        return data;
    }

    public void setData(PieData data) {
        this.data = data;
    }
}
