package com.pts.invetech.pojo;

/**
 * Created by Ashish on 24-07-2017.
 */

public class StatewiseRealtimeScheduling {

    private String region;
    private String stateid;
    private String deviceid;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getStateid() {
        return stateid;
    }

    public void setStateid(String stateid) {
        this.stateid = stateid;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }
}
