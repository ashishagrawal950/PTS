package com.pts.invetech.pojo;

public class MarketPricePXILDetails {

	public String date;
	public double max;
	public double min;
	public double avg;
	
	public String date1;
	public double max1;
	public double min1;
	public double avg1;
	
	public String date2;
	public double max2;
	public double min2;
	public double avg2;
	
	public String date3;
	public double max3;
	public double min3;
	public double avg3;
	
	public String date4;
	public double max4;
	public double min4;
	public double avg4;
	
	public String date5;
	public double max5;
	public double min5;
	public double avg5;
	
	public String date6;
	public double max6;
	public double min6;
	public double avg6;

	public MarketPricePXILDetails(String date, double max, double min, double avg,
			String date1, double max1, double min1, double avg1,
			String date2, double max2, double min2, double avg2,
			String date3, double max3, double min3, double avg3,
			String date4, double max4, double min4, double avg4,
			String date5, double max5, double min5, double avg5,
			String date6, double max6, double min6, double avg6) {
		this.date = date;
		this.max = max;
		this.min = min;
		this.avg = avg;
		
		this.date1 = date1;
		this.max1 = max1;
		this.min1 = min1;
		this.avg1 = avg1;
		
		this.date2 = date2;
		this.max2 = max2;
		this.min2 = min2;
		this.avg2 = avg2;
		
		this.date3 = date3;
		this.max3 = max3;
		this.min3 = min3;
		this.avg3 = avg3;
		
		this.date4 = date4;
		this.max4 = max4;
		this.min4 = min4;
		this.avg4 = avg4;
		
		this.date5 = date5;
		this.max5 = max5;
		this.min5 = min5;
		this.avg5 = avg5;
		
		this.date6 = date6;
		this.max6 = max6;
		this.min6 = min6;
		this.avg6 = avg6;
		

	}
	
	public String getDate() {
		return date;
	}




	public void setDate(String date) {
		this.date = date;
	}




	public double getMax() {
		return max;
	}




	public void setMax(double max) {
		this.max = max;
	}




	public double getMin() {
		return min;
	}




	public void setMin(double min) {
		this.min = min;
	}




	public double getAvg() {
		return avg;
	}




	public void setAvg(double avg) {
		this.avg = avg;
	}



	public String getDate1() {
		return date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	public double getMax1() {
		return max1;
	}

	public void setMax1(double max1) {
		this.max1 = max1;
	}

	public double getMin1() {
		return min1;
	}

	public void setMin1(double min1) {
		this.min1 = min1;
	}

	public double getAvg1() {
		return avg1;
	}

	public void setAvg1(double avg1) {
		this.avg1 = avg1;
	}

	public String getDate2() {
		return date2;
	}

	public void setDate2(String date2) {
		this.date2 = date2;
	}

	public double getMax2() {
		return max2;
	}

	public void setMax2(double max2) {
		this.max2 = max2;
	}

	public double getMin2() {
		return min2;
	}

	public void setMin2(double min2) {
		this.min2 = min2;
	}

	public double getAvg2() {
		return avg2;
	}

	public void setAvg2(double avg2) {
		this.avg2 = avg2;
	}

	public String getDate3() {
		return date3;
	}

	public void setDate3(String date3) {
		this.date3 = date3;
	}

	public double getMax3() {
		return max3;
	}

	public void setMax3(double max3) {
		this.max3 = max3;
	}

	public double getMin3() {
		return min3;
	}

	public void setMin3(double min3) {
		this.min3 = min3;
	}

	public double getAvg3() {
		return avg3;
	}

	public void setAvg3(double avg3) {
		this.avg3 = avg3;
	}

	public String getDate4() {
		return date4;
	}

	public void setDate4(String date4) {
		this.date4 = date4;
	}

	public double getMax4() {
		return max4;
	}

	public void setMax4(double max4) {
		this.max4 = max4;
	}

	public double getMin4() {
		return min4;
	}

	public void setMin4(double min4) {
		this.min4 = min4;
	}

	public double getAvg4() {
		return avg4;
	}

	public void setAvg4(double avg4) {
		this.avg4 = avg4;
	}

	public String getDate5() {
		return date5;
	}

	public void setDate5(String date5) {
		this.date5 = date5;
	}

	public double getMax5() {
		return max5;
	}

	public void setMax5(double max5) {
		this.max5 = max5;
	}

	public double getMin5() {
		return min5;
	}

	public void setMin5(double min5) {
		this.min5 = min5;
	}

	public double getAvg5() {
		return avg5;
	}

	public void setAvg5(double avg5) {
		this.avg5 = avg5;
	}

	public String getDate6() {
		return date6;
	}

	public void setDate6(String date6) {
		this.date6 = date6;
	}

	public double getMax6() {
		return max6;
	}

	public void setMax6(double max6) {
		this.max6 = max6;
	}

	public double getMin6() {
		return min6;
	}

	public void setMin6(double min6) {
		this.min6 = min6;
	}

	public double getAvg6() {
		return avg6;
	}

	public void setAvg6(double avg6) {
		this.avg6 = avg6;
	}

	@Override
    public String toString() {
        return "MarketPricePXILDetails[DATE=" + date + ", max=" + max + ", min=" + min + ", avg=" + avg + "]";
    }

}
