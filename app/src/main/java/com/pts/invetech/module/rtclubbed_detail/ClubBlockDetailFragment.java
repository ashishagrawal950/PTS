package com.pts.invetech.module.rtclubbed_detail;


import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pts.api.ApiParamConvertor;
import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.invetech.APICalling.AppBaseFragment;
import com.pts.invetech.classes.afterlogin.MainActivityAfterLogin;
import com.pts.invetech.R;
import com.pts.invetech.module.rtclubbed_detail.model.ClubDetailData;
import com.pts.invetech.module.rtclubbed_detail.model.First;
import com.pts.invetech.utils.AppLogger;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClubBlockDetailFragment extends AppBaseFragment {

    private final String BASE_URL="https://mittalpower.com/mobile/pxs_app/service/schedule_track/myscheduleclubbedview/getblocksdataclubbedview.php";

    private ListView list_realtime_detail;

    private TextView prev_rev,cur_rev;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_club_block_detail,
                container, false);

        prev_rev= (TextView) view.findViewById(R.id.prev_rev);
        cur_rev= (TextView) view.findViewById(R.id.cur_rev);

        LinearLayout openLayout = (LinearLayout) view.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(onClickListener);

        list_realtime_detail= (ListView) view.findViewById(R.id.list_realtime_detail);


        Bundle args=getArguments();
        if(args.containsKey("id") && args.containsKey("prevId")){
            String id=args.getString("id");
            String prevId=args.getString("prevId");

            getData("wazmrogqbjqhbkeddvelefdttxpeesabtznkoubqdrxfcbigwnrasvtlsjqxcqyv",id,prevId);

        }else{
            AppLogger.showToastShort(getContext(),getString(R.string.error_occurred));
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP
                        && keyCode == KeyEvent.KEYCODE_BACK) {
                    Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                    startActivity(in);
                    getActivity().overridePendingTransition(R.anim.animation,
                            R.anim.animation2);
                    getActivity().finish();
                    return true;
                }
                return false;
            }
        });
    }


    private View.OnClickListener onClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.openLayout:
                    openDrawer();
                    break;
            }
        }
    };

    private void openDrawer() {
        ((MainActivityAfterLogin) getActivity()).open();
    }

    private void logOut(){
        Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
        startActivity(in);
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
    }

    private void getData(String accessKey,String id,String prevId){
        String param= ApiParamConvertor.getBlockDataClubbedParam(accessKey,
                id,prevId);

        new NetworkHandlerModel(getActivity(),networkCallBack,
                ClubDetailData.class,1).execute(BASE_URL,param);
    }

    private NetworkCallBack networkCallBack=new NetworkCallBack(){

        @Override
        public void onResultObject(Object data, int id) {
            if(data!=null){
                ClubDetailData detailData= (ClubDetailData) data;

                String prev=String.valueOf((getString(R.string.prev_rev))+" (R"+detailData.getPrevRev()+")");
                String cur=String.valueOf((getString(R.string.cur_rev))+" (R"+detailData.getCurrentRev()+")");

                prev_rev.setText(prev);
                cur_rev.setText(cur);

                List<First> fData=detailData.getBlockdata().getFirst();
                fData.addAll(detailData.getBlockdata().getSecond());

                ClubBlockDetailAdapter bAdapter=new ClubBlockDetailAdapter(getActivity(),fData);
                list_realtime_detail.setAdapter(bAdapter);
            }
        }
    };

}
