package com.pts.invetech.classes.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.widget.Button;
import android.widget.DatePicker;

import com.pts.invetech.R;

import java.util.Calendar;

/**
 * Created by Ashish Karn on 24-08-2016.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        Button et_search_previous_bid = (Button) getActivity().findViewById(R.id.et_search_previous_bid);
        et_search_previous_bid.setText(view.getDayOfMonth() + "/" + view.getMonth() + "/" + view.getYear());

    }
}