
package com.pts.invetech.dashboardupdate.model.state;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StateList {

    @SerializedName("states")
    @Expose
    private List<State> states = null;

    @SerializedName("selected")
    @Expose
    private String selected;

    public List<State> getStates() {
        return states;
    }

    public void setStates(List<State> states) {
        this.states = states;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public int getStateIdByStateName(String stateName){
        if(stateName.length()<2){
            return 1;
        }
        for(State state:states){
            if(state.getStateName().equalsIgnoreCase(stateName)){
                return Integer.parseInt(state.getId());
            }
        }
        return 1;
    }

    public int getStateIdSelected(){
        if(selected.length()<2){
            return 1;
        }
        for(State state:states){
            if(state.getStateName().equalsIgnoreCase(selected)){
                return Integer.parseInt(state.getId());
            }
        }
        return 1;
    }

    public int getStateIndexSelected(int id){
        int index=0;
        for(State state:states){
            int tmpId=Integer.parseInt(state.getId());
            if(tmpId==id){
                return index;
            }
            index++;
        }
        return 0;
    }

    public int getregionIndexSelected(int id){
        int index=0;
        for(State state:states){
            int tmpId=Integer.parseInt(state.getIexregion());
            if(tmpId==id){
                return index;
            }
            index++;
        }
        return 0;
    }

    public String getiexregionSelected(int id) {
        int index = 0;
        String iexregion = "" ;
        for (State state : states) {
            int tmpId = Integer.parseInt(state.getId());
            if (tmpId == id) {
                iexregion = (state.getIexregion());
                //return index;
                break;
            }
            index++;
        }
        return iexregion;

    }
}
