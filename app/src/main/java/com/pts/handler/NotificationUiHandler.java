package com.pts.handler;

import android.view.View;

import com.pts.invetech.NotificationSetting.SettingsToggle;
import com.pts.invetech.R;
import com.pts.invetech.pojo.NotificationData;


public class NotificationUiHandler {

    public static void setAllNotification(View view,NotificationData data){
        SettingsToggle toggle = (SettingsToggle) view.findViewById( R.id.settingallnotif);

        if(getValue(data)){
            toggle.toggleOn();
        }else{
            toggle.toggleOff();
        }
    }

    public static void setUiCurtailment(View view, NotificationData data){
        SettingsToggle toggle = (SettingsToggle) view.findViewById( R.id.settingscurtailnotif);

        if(data.getPrecurtailment().equalsIgnoreCase("YES") || data.getNLDCTOIEX().equalsIgnoreCase("YES")
                ||  data.getREALTIME().equalsIgnoreCase("YES") // || data.getNOTAFFECTED().equalsIgnoreCase("YES")
                || data.getRESTORE().equalsIgnoreCase("YES")) {

            toggle.toggleOn();
        }else{
            toggle.toggleOff();
        }

    }

    public static void setUiNewsNotification(View view,NotificationData data){
        SettingsToggle toggle = (SettingsToggle) view.findViewById( R.id.settingsnewsnotif);

        if(data.getNewsfeedactivity().equalsIgnoreCase("YES")){
            toggle.toggleOn();
        }else{
            toggle.toggleOff();
        }

    }

    public static void setUiBidNotification(View view,NotificationData data){

        SettingsToggle toggle = (SettingsToggle) view.findViewById( R.id.settingsbidnotif);

        if(data.getMenunobid().equalsIgnoreCase("YES") || data.getRecplacenewbid().equalsIgnoreCase("YES") ||
                data.getRecnobid().equalsIgnoreCase("YES") || data.getMenunewbid().equalsIgnoreCase("YES")){

            toggle.toggleOn();
        }else{
            toggle.toggleOff();
        }
    }

    public static void setUiNewsLetter(View view,NotificationData data){
        SettingsToggle toggle = (SettingsToggle) view.findViewById( R.id.settingsnewsletter);

        if(data.getNEWSLETTER().equalsIgnoreCase("YES")){
            toggle.toggleOn();
        }else{
            toggle.toggleOff();
        }
    }

    private static boolean getValue(NotificationData data){
        if(data.getPrecurtailment().equalsIgnoreCase("YES") && data.getNLDCTOIEX().equalsIgnoreCase("YES")
              //  && data.getNOTAFFECTED().equalsIgnoreCase("YES")
                &&  data.getREALTIME().equalsIgnoreCase("YES")
                && data.getRESTORE().equalsIgnoreCase("YES") && data.getNewsfeedactivity().equalsIgnoreCase("YES")
                && data.getMenunobid().equalsIgnoreCase("YES") && data.getRecplacenewbid().equalsIgnoreCase("YES")
                && data.getRecnobid().equalsIgnoreCase("YES") && data.getMenunewbid().equalsIgnoreCase("YES")
                && data.getNEWSLETTER().equalsIgnoreCase("YES") && data.getMCP().equalsIgnoreCase("YES")
                && data.getMenuhome().equalsIgnoreCase("YES") && data.getMenumarketprice().equalsIgnoreCase("YES")
                && data.getMenudownload().equalsIgnoreCase("YES") && data.getMenureport().equalsIgnoreCase("YES")){

            return Boolean.TRUE;
        }else{
            return Boolean.FALSE;
        }
    }

    private static boolean getBooleanFromString(String str){
        if(str.equalsIgnoreCase("YES")){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

}
