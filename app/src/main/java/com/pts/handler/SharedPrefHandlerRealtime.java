package com.pts.handler;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

/**
 * Created by Vaibhav on 16-08-2016.
 */
public class SharedPrefHandlerRealtime {

    private static final String DB_SHAREDPREF="com.ptssharedpref";
    private static final String DB_ISFIRSTTIME="com.ptssharedpref.isfirsttime";
    private static final String DB_FIRSTMAP="com.ptssharedpref.firstmap";

    private static final String POWERDEMAND="com.pts.power.demand";
    private static final String TYPESEND = "com.pts.power.typesend";

    private static final String TYPE="type";
    private static final String FROM="from";
    private static final String TO="to";
    private static final String STATE="state";


    public static boolean checkFirstTime(Context context){

        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandlerRealtime.DB_SHAREDPREF, Context.MODE_PRIVATE);

        return sharedPref.getBoolean(DB_ISFIRSTTIME,true);
    }

    public static void setFirstTime(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandlerRealtime.DB_SHAREDPREF, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(SharedPrefHandlerRealtime.DB_ISFIRSTTIME, false);
        editor.commit();
    }



    public static boolean checkFirstTimeMap(Context context){

        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandlerRealtime.DB_SHAREDPREF, Context.MODE_PRIVATE);

        return sharedPref.getBoolean(DB_FIRSTMAP,true);
    }

    public static void setFirstTimeMap(Context context){

        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandlerRealtime.DB_SHAREDPREF, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(SharedPrefHandlerRealtime.DB_FIRSTMAP, false);
        editor.commit();
    }

    public static void savePowerDemandData(Context context,String data, String typeSend){
        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandlerRealtime.DB_SHAREDPREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(POWERDEMAND,data);
        editor.putString(TYPESEND,typeSend);
        editor.apply();
    }

    public static void removePowerDemand(Context context){
        SharedPreferences preferences = context.getSharedPreferences(SharedPrefHandlerRealtime.DB_SHAREDPREF, Context.MODE_PRIVATE);
        preferences.edit().remove(POWERDEMAND).apply();
        preferences.edit().remove(TYPESEND).apply();
    }

    public static String getPowerDemandData(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandlerRealtime.DB_SHAREDPREF, Context.MODE_PRIVATE);
        return sharedPref.getString(POWERDEMAND,null);
    }

    public static String getTypesend(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(
                SharedPrefHandlerRealtime.DB_SHAREDPREF, Context.MODE_PRIVATE);
        return sharedPref.getString(TYPESEND,null);
    }







}