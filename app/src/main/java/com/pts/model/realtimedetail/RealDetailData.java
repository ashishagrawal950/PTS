
package com.pts.model.realtimedetail;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class RealDetailData {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("rldc")
    @Expose
    private String rldc;
    @SerializedName("periphery")
    @Expose
    private String periphery;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The rldc
     */
    public String getRldc() {
        return rldc;
    }

    /**
     * 
     * @param rldc
     *     The rldc
     */
    public void setRldc(String rldc) {
        this.rldc = rldc;
    }

    /**
     * 
     * @return
     *     The periphery
     */
    public String getPeriphery() {
        return periphery;
    }

    /**
     * 
     * @param periphery
     *     The periphery
     */
    public void setPeriphery(String periphery) {
        this.periphery = periphery;
    }

    /**
     * 
     * @return
     *     The data
     */
    public List<Datum> getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    public void setData(List<Datum> data) {
        this.data = data;
    }

}
