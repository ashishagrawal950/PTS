package com.pts.invetech.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

import android.widget.DatePicker;
import android.widget.TextView;

import com.pts.invetech.R;

import java.util.Calendar;

/**
 * Created by Ashish on 21-02-2017.
 */

public class DatePickerFragmentFour extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        populateSetDate(yy, mm + 1, dd);
    }

    public void populateSetDate(int year, int month, int day) {
        TextView tv_datepickerfour= (TextView) getActivity().findViewById(R.id.tv_datepickerfour);
        if ((month <= 9) && (day < 10)) {
            String monthplace = "0" + month;
            String dayplace = "0" + day;
            tv_datepickerfour.setText(dayplace + "-" + monthplace + "-" + year);
        }
        else if (month <= 9) {
            String monthplace = "0" + month;
            tv_datepickerfour.setText(day + "-" + monthplace + "-" + year);
        }
        else if (day < 10) {
            String dayplace = "0" + day;
            tv_datepickerfour.setText(dayplace + "-" + month + "-" + year);
        }
        else {
            tv_datepickerfour.setText(day + "-" + month + "-" + year);
        }
    }
}



















/*  extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        populateSetDate(yy, mm + 1, dd);
    }

    public void populateSetDate(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        TextView tv_datepickerfour= (TextView) getActivity().findViewById(R.id.tv_datepickerfour);
       // tv_datepickerfour.setText(view.getDayOfMonth()+"/"+view.getMonth()+"/"+view.getYear());
        if ((month <= 9) && (day < 10)) {
            String monthplace = "0" + month;
            String dayplace = "0" + day;
            tv_datepickerfour.setText(dayplace + "-" + monthplace + "-" + year);
            //etxt_fromdate_set.setText(year + "-" + monthplace + "-" + dayplace);
            //as = etxt_fromdate_set.getText().toString();
            // Toast.makeText(getActivity(), "DownloadResult" +
            // as,Toast.LENGTH_LONG).show();
        }
        else if (month <= 9) {
            String monthplace = "0" + month;
            tv_datepickerfour.setText(day + "-" + monthplace + "-" + year);
            //etxt_fromdate_set.setText(year + "-" + monthplace + "-" + day);
            //as = etxt_fromdate_set.getText().toString();
        }
        else if (day < 10) {
            String dayplace = "0" + day;
            tv_datepickerfour.setText(dayplace + "-" + month + "-" + year);
            //etxt_fromdate_set.setText(year + "-" + month + "-" + dayplace);
            // as = etxt_fromdate_set.getText().toString();
        }
        else {
            tv_datepickerfour.setText(day + "-" + month + "-" + year);
            // etxt_fromdate_set.setText(year + "-" + month + "-" + day);
            // as = etxt_fromdate_set.getText().toString();
        }
    }
}*/