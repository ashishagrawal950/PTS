package com.pts.invetech.customlist;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.invetech.R.color;

public class NotificationCustomList extends ArrayAdapter<String> {

	private  Activity context;
	static ArrayList<Integer> icon_array = new ArrayList<Integer>();
	static ArrayList<String> message_array = new ArrayList<String>();
	static ArrayList<String> datestamp_array = new ArrayList<String>();
	static ArrayList<String> timestamp_array = new ArrayList<String>();
	static ArrayList<String> status_array = new ArrayList<String>();
	private static LayoutInflater inflater = null;
	private int lastPosition = -1;
	
	public NotificationCustomList(Activity context, ArrayList<Integer> icon_array,
			ArrayList<String> message_array
			   , ArrayList<String> datestamp_array
			   , ArrayList<String> timestamp_array
			   , ArrayList<String> status_array) {
		// TODO Auto-generated constructor stub
		super(context, R.layout.notification_list, message_array);
		this.context = context;
		
		this.icon_array = icon_array;
		this.message_array = message_array;
		this.datestamp_array = datestamp_array;
		this.timestamp_array = timestamp_array;
		this.status_array = status_array;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater.inflate(R.layout.notification_list, null, true);
		
		LinearLayout llnotificationbg = (LinearLayout) rowView.findViewById(R.id.llnotificationbg);
		TextView aboveline = (TextView) rowView.findViewById(R.id.aboveline);
		LinearLayout notificationheader = (LinearLayout) rowView.findViewById(R.id.notificationheader);
		LinearLayout noticationicon = (LinearLayout) rowView.findViewById(R.id.noticationicon);
		TextView elementicon = (TextView) rowView.findViewById(R.id.elementicon);
		TextView txt1 = (TextView) rowView.findViewById(R.id.txt1);
		TextView tvdate= (TextView) rowView.findViewById(R.id.tvdate);
		TextView tvtime= (TextView) rowView.findViewById(R.id.tvtime);
		TextView belowline = (TextView) rowView.findViewById(R.id.belowline);
		if(message_array.get(position).isEmpty()){
			aboveline.setVisibility(view.GONE);
			notificationheader.setVisibility(view.INVISIBLE);
			noticationicon.setVisibility(view.INVISIBLE);
			txt1.setVisibility(view.INVISIBLE);
			tvdate.setVisibility(view.INVISIBLE);
			tvtime.setVisibility(view.INVISIBLE);
			belowline.setVisibility(view.GONE);
		}
		else{
		aboveline.setVisibility(view.VISIBLE);
		notificationheader.setVisibility(view.VISIBLE);
		noticationicon.setVisibility(view.VISIBLE);
		txt1.setVisibility(view.VISIBLE);
		tvdate.setVisibility(view.VISIBLE);
		tvtime.setVisibility(view.VISIBLE);
		belowline.setVisibility(view.VISIBLE);
		if(status_array.get(position).equalsIgnoreCase("0")){
		elementicon.setBackgroundResource(icon_array.get(position));
		txt1.setText(message_array.get(position));
		tvdate.setText(datestamp_array.get(position));
		tvtime.setText(timestamp_array.get(position));
		llnotificationbg.setBackgroundColor(Color.GRAY);
		}else{
		elementicon.setBackgroundResource(icon_array.get(position));
		txt1.setText(message_array.get(position));
		tvdate.setText(datestamp_array.get(position));
		tvtime.setText(timestamp_array.get(position));	
		//llnotificationbg.setBackgroundColor(color.grapphbackground);
		//llnotificationbg.setBackgroundResource(R.drawable.F);
		llnotificationbg.setBackgroundColor(Color.DKGRAY);
		}
		}
		Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
		rowView.startAnimation(animation);
		lastPosition = position;
		return rowView;
	}
}
