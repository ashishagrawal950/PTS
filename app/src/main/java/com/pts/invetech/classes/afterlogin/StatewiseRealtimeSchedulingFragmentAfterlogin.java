package com.pts.invetech.classes.afterlogin;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.pts.convertor.StateWiseRealConvertor;
import com.pts.handler.NetworkCallBack;
import com.pts.handler.NetworkHandlerModel;
import com.pts.handler.NetworkHandlerString;
import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseFragment;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.classes.activity.RldcGridClickActivity;
import com.pts.invetech.classes.fragment.StatewiseRealtimeSchedulingFragment;
import com.pts.invetech.customlist.StateWiseCustomGrid;
import com.pts.invetech.dashboardupdate.adapters.StateAdapter;
import com.pts.invetech.dashboardupdate.background.GpsTrackerUpdate;
import com.pts.invetech.dashboardupdate.model.state.State;
import com.pts.invetech.dashboardupdate.model.state.StateList;
import com.pts.invetech.utils.ColorTemplates;
import com.pts.invetech.utils.MyGridView;
import com.pts.model.appned.StateRealTimeModel;
import com.pts.model.statewise.StateRealTimeValue;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ashish on 09-08-2017.
 */

public class StatewiseRealtimeSchedulingFragmentAfterlogin extends AppBaseFragment {

    private final String URL_STATELIST = "https://www.mittalpower.com/mobile/pxs_app/service/getstatenamefromgeo.php";
    private final String URL_STATEWISE = "https://www.mittalpower.com/mobile/pxs_app/service/scheduling/statewise.php";
    private final int CONSTANT_STATE = 2;
    private final int CONSTANT_STATEWISE = 1;
    private Dialog prgDialog;
    private LinearLayout lltoplogin, lltoplogout;
    private TextView tvlogintext, tvlogouttext;
    private View rootView;
    private SQLiteDatabase db;
    private String device_id, domain;
    private PieChart pieChart;
    private MyGridView statewisegridview;
    private boolean boolState = false;
    private int currentStateID = 1;
    private String regionname;

    private StateRealTimeModel stateRealTimeModel = null;
    private TextView current_date;
    private String from = "";
    private TextView tvnull, tvnulltext;
    private ConnectionDetector cd;
    private StatewiseRealtimeSchedulingFragment.IStateWiseDataPasser stateWiseDataPasser = new StatewiseRealtimeSchedulingFragment.IStateWiseDataPasser() {
        @Override
        public void sendImportType(String importType, String revision) {
            if (currentStateID > 0 && regionname != null) {
                // AppLogger.showMsg("state id -- region name",currentStateID+"  --  "+regionname);
                Intent intent = new Intent(getActivity(), RldcGridClickActivity.class);
                intent.putExtra("region", regionname);
                intent.putExtra("stateid", String.valueOf(currentStateID));
                intent.putExtra("importtype", importType);
                intent.putExtra("revision", revision);
                startActivity(intent);

            }
        }
    };
    private NetworkCallBack realTimeCallback = new NetworkCallBack() {
        @Override
        public void onResultString(String data, int id) {
            if (data == null) {
                return;
            }

            try {
                ArrayList<StateRealTimeValue> stateRealTimeValues = StateWiseRealConvertor.getRealTimeData(data);
                String curDate = StateWiseRealConvertor.getRealTimeDate(data);
                String message = StateWiseRealConvertor.getmessage(data);
                if (stateRealTimeValues.size() == 0) {
                    pieChart.setVisibility(View.GONE);
                    statewisegridview.setVisibility(View.GONE);
                    current_date.setVisibility(View.GONE);
                    tvnull.setBackgroundResource(R.drawable.sad);
                    tvnulltext.setText(R.string.nodata);
                    tvnull.setVisibility(View.VISIBLE);
                    tvnulltext.setVisibility(View.VISIBLE);
                } else {
                    pieChart.setVisibility(View.VISIBLE);
                    statewisegridview.setVisibility(View.VISIBLE);
                    tvnull.setVisibility(View.GONE);
                    tvnulltext.setVisibility(View.GONE);
                    drawchart(stateRealTimeValues);
                    recyclerOne(stateRealTimeValues, message);
                    if (curDate != null) {
                        current_date.setText("Schedule For Dated : " + curDate);
                        current_date.setVisibility(View.VISIBLE);
                    } else {
                        current_date.setVisibility(View.GONE);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    /*
       Network callbacks
    */
    private NetworkCallBack callBack = new NetworkCallBack() {
        @Override
        public void onResultObject(Object data, int id) {
            if (data == null) {
//                AppLogger.showToastLong(getActivity(), "Some Error Occurred in id - " + id);
                return;
            }
            switch (id) {
                case CONSTANT_STATE:
                    StateList stateList = (StateList) data;
                    if (getView() != null) {
                        setSpinnerRegion(getView(), stateList);
                        try {
                            SharedPrefHandler.saveStateList(getActivity(), stateList.getStates());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }

        @Override
        public void onResultString(String data, int id) {
            if (data == null) {
//                AppLogger.showToastLong(getActivity(), "Some Error Occurred in News");
                return;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = getActivity().openOrCreateDatabase("deviceDBSecond", Context.MODE_PRIVATE, null);
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getActivity(), "domain");
//        domain = getArguments().getString("domain");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_statewiserealtimescheduling, container, false);

        cd = new ConnectionDetector(getContext());
        current_date = (TextView) rootView.findViewById(R.id.current_date);

        LinearLayout openLayout = (LinearLayout) rootView.findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivityAfterLogin) getActivity()).open();
            }
        });
        prgDialog = new Dialog(getActivity());
        prgDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        prgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        prgDialog.setContentView(R.layout.progessdialog);
        ImageView image = (ImageView) prgDialog.findViewById(R.id.img);
        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom);
        image.startAnimation(animation);
        prgDialog.setCancelable(false);
        pieChart = (PieChart) rootView.findViewById(R.id.stateRealTime);
        statewisegridview = (MyGridView) rootView.findViewById(R.id.statewisegridview);
        tvnull = (TextView) rootView.findViewById(R.id.tvnull);
        tvnulltext = (TextView) rootView.findViewById(R.id.tvnulltext);

        lltoplogin = (LinearLayout) rootView.findViewById(R.id.lltoplogin);
        lltoplogout = (LinearLayout) rootView.findViewById(R.id.lltoplogout);
        tvlogintext = (TextView) rootView.findViewById(R.id.tvlogintext);
        tvlogouttext = (TextView) rootView.findViewById(R.id.tvlogouttext);
        lltoplogin.setVisibility(View.GONE);
        lltoplogout.setVisibility(View.VISIBLE);
        tvlogouttext.setPaintFlags(tvlogouttext.getPaintFlags()
                | Paint.UNDERLINE_TEXT_FLAG);
        tvlogouttext.setText("Home");

        lltoplogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                startActivity(in);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);

            }
        });

        int stateId = -1;

        Bundle argsBundle = getArguments();

        if (argsBundle != null && argsBundle.containsKey("powerdata")) {
            stateRealTimeModel = argsBundle.getParcelable("powerdata");
            from = argsBundle.getString("from");
        }

        if (stateRealTimeModel != null) {
            stateId = Integer.parseInt(stateRealTimeModel.getStateId());
            current_date.setVisibility(View.VISIBLE);
        } else {
            stateId = SharedPrefHandler.getIntData(getActivity().getApplicationContext(), getString(R.string.db_stateid));
            current_date.setVisibility(View.GONE);
        }

        if (stateId < 0) {

            Location location = GpsTrackerUpdate.getLastLocation(getActivity());
            if (location == null) {
                SharedPrefHandler.saveInt(getActivity().getApplicationContext(), getString(R.string.db_stateid), 1);
                getStatesWoLoc(1, "IEX");
            } else {
                try {
                    getStateList(location);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            getStatesWoLoc(stateId, "IEX");
        }

        return rootView;
    }

    private void getStatesWoLoc(int stateId, String type) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("state_id", stateId);
            obj.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            new NetworkHandlerModel(getActivity(), callBack, StateList.class, CONSTANT_STATE)
                    .execute(domain + "/mobile/pxs_app/service/getstatenamefromgeo.php", obj.toString());
        }
    }

    private void getStateList(Location loc) throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("latitude", loc.getLatitude());
        obj.put("logtitude", loc.getLongitude());
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
        } else {
            new NetworkHandlerModel(getActivity(), callBack, StateList.class, CONSTANT_STATE)
                    .execute(domain + "/mobile/pxs_app/service/getstatenamefromgeo.php", obj.toString());
        }
    }

    private void setSpinnerRegion(View view, StateList data) {

        Spinner spinner_region = (Spinner) view.findViewById(R.id.spinner_region);
        spinner_region.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!boolState) {
                    boolState = true;
                    return;
                }
                currentStateID = Integer.parseInt(((State) adapterView.getSelectedItem()).getId());
                regionname = ((State) adapterView.getSelectedItem()).getRegion();
                SharedPrefHandler.saveInt(getActivity().getApplicationContext(), getString(R.string.db_stateid), currentStateID);
                SharedPrefHandler.saveString(getActivity().getApplicationContext(), getString(R.string.db_region), ((State) adapterView.getSelectedItem()).getRegion());
                getStatewise(regionname,
                        ((State) adapterView.getSelectedItem()).getId(), device_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        StateAdapter dataAdapter1 = new StateAdapter(getActivity(), data);
        spinner_region.setAdapter(dataAdapter1);

        if (stateRealTimeModel == null) {
            currentStateID = SharedPrefHandler.getIntData(getActivity().getApplicationContext(), getString(R.string.db_stateid));

            regionname = SharedPrefHandler.getString(getActivity().getApplicationContext(), getString(R.string.db_region));
        } else {
            currentStateID = Integer.parseInt(stateRealTimeModel.getStateId());
            regionname = stateRealTimeModel.getRegion();
        }

        spinner_region.setSelection(data.getStateIndexSelected(currentStateID));
        getStatewise(regionname, Integer.toString(currentStateID), device_id);

    }

    private void getStatewise(String region, String stateid, String device_id) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("region", region);
            obj.put("stateid", stateid);
            obj.put("device_id", device_id);
            if (stateRealTimeModel != null) {
                obj.put("date", stateRealTimeModel.getDate());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        new NetworkHandlerString(getContext(), realTimeCallback, CONSTANT_STATEWISE)
                .execute(domain + "/mobile/pxs_app/service/scheduling/statewise.php", obj.toString());
    }

    private void drawchart(ArrayList<StateRealTimeValue> stateRealTimeValues) {
        pieChart.setUsePercentValues(true);
        ArrayList<Entry> yvalues = new ArrayList<Entry>();
        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < stateRealTimeValues.size(); i++) {
            yvalues.add(new Entry(Float.parseFloat(stateRealTimeValues.get(i).getPer()), i));
            xVals.add("");
        }
        PieDataSet dataSet = new PieDataSet(yvalues, "");
        /*for(int i=0; i<stateRealTimeValues.size(); i++ ){
            xVals.add(stateRealTimeValues.get(i).getName());
        }*/
        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        pieChart.setData(data);
        pieChart.setDescription("");
        pieChart.setDrawHoleEnabled(true);
        pieChart.setTransparentCircleRadius(5f);
        pieChart.setHoleRadius(20f);
        pieChart.getLegend().setEnabled(false);

        pieChart.getIndexForAngle(55);

        dataSet.setColors(ColorTemplates.MY_COLORS);
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.DKGRAY);
        pieChart.animateY(3000);
        pieChart.invalidate();
    }

    private void recyclerOne(ArrayList<StateRealTimeValue> stateRealTimeValues, String message) {
        StateWiseCustomGrid adapter = new StateWiseCustomGrid(getContext(), stateRealTimeValues, ColorTemplates.MY_COLORS, message, stateWiseDataPasser);
        statewisegridview.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (from.equalsIgnoreCase("Powerdemandcomparision")) {
                        PowerDemandComparisionFragmentAfterlogin powerDemandComparisionFragmentAfterlogin = new PowerDemandComparisionFragmentAfterlogin();
                        if (powerDemandComparisionFragmentAfterlogin != null) {
                            FragmentManager fragmentManager = getFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.content_frame, powerDemandComparisionFragmentAfterlogin);
                            fragmentTransaction.commit();
                        }
                    } else {
                        Intent in = new Intent(getActivity(), MainActivityAfterLogin.class);
                        startActivity(in);
                        getActivity().overridePendingTransition(R.anim.animation, R.anim.animation2);
                        getActivity().finish();
                        return true;
                    }
                    return true;
                }
                return false;
            }
        });
    }


    public interface IStateWiseDataPasser {
        void sendImportType(String importType, String revision);
    }


}
