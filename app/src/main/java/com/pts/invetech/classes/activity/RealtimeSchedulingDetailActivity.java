package com.pts.invetech.classes.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pts.handler.SharedPrefHandler;
import com.pts.invetech.APICalling.AppBaseActivity;
import com.pts.invetech.ConnectionDetector;
import com.pts.invetech.R;
import com.pts.invetech.customlist.RealtimeSchedulingCustom_Adapter;
import com.pts.invetech.pojo.DataHolder;
import com.pts.invetech.pojo.MySorter;
import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.AppStringUtils;
import com.pts.invetech.utils.Constant;
import com.pts.model.realtimedetail.Datum;
import com.pts.model.realtimedetail.RealDetailData;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RealtimeSchedulingDetailActivity extends AppBaseActivity implements AdapterView.OnItemSelectedListener {

    private final String URL_REALDETAIL = "https://www.mittalpower.com/mobile/pxs_app/service/scheduling/getqtm_rldcwise.php";
    private RealtimeSchedulingCustom_Adapter realtimeSchedulingCustom_Adapter;
    private RealDetailData currentData;
    private boolean[] incDecArr = {false, false, false, false, false};
    private Button[] btnHolderView = new Button[5];
    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btntrader:
                    sortTrader();
                    break;
                case R.id.btnbuyer:
                    sortBuyer();
                    break;
                case R.id.btnseller:
                    sortSeller();
                    break;
                case R.id.btnapproval:
                    sortApproval();
                    break;
                case R.id.btnflow:
                    sortTotalqtm();
                    break;
                case R.id.btnsearch:

                    ConnectionDetector cd = new ConnectionDetector(RealtimeSchedulingDetailActivity.this);
                    if (!cd.isConnectingToInternet()) {
                        Toast.makeText(getApplicationContext(), "Please Check your network.", Toast.LENGTH_LONG).show();
                    } else {
                        srchaction();
                    }
                    break;
            }
        }
    };
    private DataHolder dataHolder = new DataHolder();
    private SQLiteDatabase db;
    private String device_id, domain;
    private LinearLayout openLayout;
    private AdapterView.OnItemSelectedListener itemList = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            try {
                apiCall(position);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);// will hide the title not
        setContentView(R.layout.activity_on_chart_selected);

        db = openOrCreateDatabase("deviceDBSecond",
                Context.MODE_PRIVATE, null);
        Cursor cdevice = db.rawQuery("SELECT * FROM device", null);
        if (cdevice.getCount() == 0) {
            // showMessage("Error", "No records found");
        }
        while (cdevice.moveToNext()) {
            device_id = cdevice.getString(0);
        }
        domain = SharedPrefHandler.getDeviceString(getApplicationContext(), "domain");
        openLayout = (LinearLayout) findViewById(R.id.openLayout);
        openLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        try {
            init();
            uiSetter();
            searchFun();
        } catch (Exception e) {
            AppLogger.showMsgWithoutTag(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    private void uiSetter() {
        Button btn = (Button) findViewById(R.id.btnbuyer);
        btn.setOnClickListener(btnClick);
        btnHolderView[0] = btn;

        btn = (Button) findViewById(R.id.btnseller);
        btn.setOnClickListener(btnClick);
        btnHolderView[1] = btn;

        btn = (Button) findViewById(R.id.btnapproval);
        btn.setOnClickListener(btnClick);
        btnHolderView[2] = btn;

        btn = (Button) findViewById(R.id.btnflow);
        btn.setOnClickListener(btnClick);
        btnHolderView[3] = btn;

        btn = (Button) findViewById(R.id.btntrader);
        btn.setOnClickListener(btnClick);
        btnHolderView[4] = btn;
    }

    public void init() throws JSONException, ParseException {

        Intent intent = getIntent();

        if (intent != null) {
            dataHolder.setDate(intent.getStringExtra(Constant.VALUE_DATE));
            dataHolder.setRegion(intent.getStringExtra(Constant.VALUE_REGION));
            dataHolder.setRevision(intent.getStringExtra(Constant.VALUE_MAXREV));
            dataHolder.setImportType(intent.getStringExtra(Constant.VALUE_IMPORTTYPE));
            dataHolder.setRevisionList(intent.getStringArrayListExtra(Constant.VALUE_REVARRAY));

//            apiCall(0);

            setHeader();
        } else {
            AppLogger.showToastShort(this, "Some Error Occurred");
        }
    }

    private void apiCall(int index) throws JSONException, ParseException {
        JSONObject object = new JSONObject();
        object.put(Constant.VALUE_DATE, convertDate(dataHolder.getDate()));
        object.put(Constant.VALUE_REGION, dataHolder.getRegion());
        object.put(Constant.VALUE_DEVICEID, device_id);
        object.put(Constant.VALUE_IMPORTTYPE, dataHolder.getImportType());
        object.put("revision", dataHolder.getRevisionList().get(index));

        AppLogger.showMsgWithoutTag(domain + "/mobile/pxs_app/service/scheduling/getqtm_rldcwise.php" + "   " + object.toString());

        new NetworkManagerActivityGson(this, object, domain + "/mobile/pxs_app/service/scheduling/getqtm_rldcwise.php", RealDetailData.class).execute();
    }

    private void setHeader() {
        LinearLayout layout_mainstrip = (LinearLayout) findViewById(R.id.layout_mainstrip);
        TextView tvheader = (TextView) findViewById(R.id.tvheader);

        tvheader.setText("Real Time Scheduling of " + dataHolder.getRegion() + "    R");

        Spinner spinner = (Spinner) layout_mainstrip.findViewById(R.id.headertext);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item_state, dataHolder.getRevisionList());

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(dataHolder.getRevisionList().size() - 1);
        spinner.setOnItemSelectedListener(itemList);

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.realtime_scheduling_slide_left_to_right);
        layout_mainstrip.startAnimation(animation);
    }

    private String convertDate(String date) throws ParseException {

        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy");

        Date date1 = inputFormat.parse(date);
        String outputDateStr = outputFormat.format(date1);

        return outputDateStr;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        Intent intent = new Intent(RealtimeSchedulingDetailActivity.this, RealtimeSchedulingDetailBlockActivity.class);
        startActivity(intent);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void updateObjectResult(Object object) {
        if (object == null) {
            AppLogger.showToastShort(this, "Some Error Occurred");
            return;
        }
        currentData = (RealDetailData) object;
        updateList();
    }

    private void updateList() {

        realtimeSchedulingCustom_Adapter = new RealtimeSchedulingCustom_Adapter(getApplicationContext(), currentData);

        ListView listView = (ListView) findViewById(R.id.list);
        listView.addHeaderView(new View(this));
        listView.addFooterView(new View(this));
        listView.setAdapter(realtimeSchedulingCustom_Adapter);
        listView.setTextFilterEnabled(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(RealtimeSchedulingDetailActivity.this,
                        RealtimeSchedulingDetailBlockActivity.class);

                if (realtimeSchedulingCustom_Adapter == null) {
                    AppLogger.showToastLong(RealtimeSchedulingDetailActivity.this, "Some Error Occured");
                    return;
                }

                Datum datum = realtimeSchedulingCustom_Adapter.getData().get((int) id);

                intent.putExtra(Constant.VALUE_DATAID, datum.getDataId());
                intent.putExtra(Constant.DATA_APPROVAL, datum.getApprovalNo());
                intent.putExtra(Constant.DATA_REV, datum.getMaxrevision());

                startActivity(intent);
            }
        });
    }

    private void resetTop(int num) {

        for (int index = 0; index < incDecArr.length; index++) {
            if (index != num) {
                incDecArr[index] = false;
                btnHolderView[index].setBackgroundResource(R.drawable.on_click_back_init);
            }
        }
    }

    private void srchaction() {
        String str = ((EditText) findViewById(R.id.searchinput)).getText().toString();
        if (AppStringUtils.isTextEmpty(str)) {
            AppLogger.showToastShort(this, "Please Enter valid String");
            return;
        } else {
            List<Datum> tmp = currentData.getData();
            List<Datum> nList = new ArrayList<>();

            for (int index = 0; index < tmp.size(); index++) {
                Datum dt = tmp.get(index);
                if (dt.isPresent(str) != null) {
                    nList.add(dt);
                }
            }
            realtimeSchedulingCustom_Adapter.updateList(nList);
        }
    }


    private void searchFun() {
        ((Button) findViewById(R.id.btnsearch)).setOnClickListener(btnClick);

        EditText txtVu = (EditText) findViewById(R.id.searchinput);
        txtVu.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count < before) {
                    if (realtimeSchedulingCustom_Adapter == null) {
                        //AppLogger.showToastLong(RealtimeSchedulingDetailActivity.this,"Some Error Occured");
                        return;
                    } else {
                        realtimeSchedulingCustom_Adapter.resetData();
                    }
                    realtimeSchedulingCustom_Adapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void sortTotalqtm() {
        if (currentData == null) {
            AppLogger.showToastShort(this, "Some Error Occurred");
            return;
        }
        incDecArr[3] = !incDecArr[3];
        if (incDecArr[3]) {
            MySorter.sortAssendingOnQtm(currentData.getData());
        } else {
            MySorter.sortDesendingOnQtm(currentData.getData());
        }

        realtimeSchedulingCustom_Adapter.updateList(currentData.getData());
        ((Button) findViewById(R.id.btnflow)).setBackgroundResource(incDecArr[3] ?
                R.drawable.on_click_backk : R.drawable.on_click_back);

        resetTop(3);
    }

    private void sortApproval() {
        if (currentData == null) {
            AppLogger.showToastShort(this, "Some Error Occurred");
            return;
        }
        incDecArr[2] = !incDecArr[2];
        if (incDecArr[2]) {
            MySorter.sortA2ZApproval(currentData.getData());
        } else {
            MySorter.sortZ2AApproval(currentData.getData());
        }

        realtimeSchedulingCustom_Adapter.updateList(currentData.getData());
        ((Button) findViewById(R.id.btnapproval)).setBackgroundResource(incDecArr[2] ?
                R.drawable.on_click_backk : R.drawable.on_click_back);

        resetTop(2);
    }

    private void sortSeller() {
        if (currentData == null) {
            AppLogger.showToastShort(this, "Some Error Occurred");
            return;
        }
        incDecArr[1] = !incDecArr[1];
        if (incDecArr[1]) {
            MySorter.sortA2ZSeller(currentData.getData());
        } else {
            MySorter.sortZ2ASeller(currentData.getData());
        }

        realtimeSchedulingCustom_Adapter.updateList(currentData.getData());

        ((Button) findViewById(R.id.btnseller)).setBackgroundResource(incDecArr[1] ?
                R.drawable.on_click_backk : R.drawable.on_click_back);

        resetTop(1);
    }

    private void sortBuyer() {
        if (currentData == null) {
            AppLogger.showToastShort(this, "Some Error Occurred");
            return;
        }
        incDecArr[0] = !incDecArr[0];
        if (incDecArr[0]) {
            MySorter.sortA2ZBuyer(currentData.getData());
        } else {
            MySorter.sortZ2ABuyer(currentData.getData());
        }

        realtimeSchedulingCustom_Adapter.updateList(currentData.getData());

        ((Button) findViewById(R.id.btnbuyer)).setBackgroundResource(incDecArr[0] ?
                R.drawable.on_click_backk : R.drawable.on_click_back);

        resetTop(0);
    }

    private void sortTrader() {
        if (currentData == null) {
            AppLogger.showToastShort(this, "Some Error Occurred");
            return;
        }
        incDecArr[4] = !incDecArr[4];

        if (incDecArr[4]) {
            MySorter.sortA2ZTrader(currentData.getData());
        } else {
            MySorter.sortZ2ATrader(currentData.getData());
        }

        realtimeSchedulingCustom_Adapter.updateList(currentData.getData());

        ((Button) findViewById(R.id.btntrader)).setBackgroundResource(incDecArr[4] ?
                R.drawable.on_click_backk : R.drawable.on_click_back);

        resetTop(4);
    }
}