package com.pts.invetech.customlist;

/**
 * Created by Ashish on 05-10-2016.
 */

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.pts.invetech.classes.fragment.InterRegionalFragment;


public class PagerAdapterMonthlyATC extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterMonthlyATC(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                InterRegionalFragment tab1 = new InterRegionalFragment();
                return tab1;
            /*case 1:
                InterCountryFragment tab2 = new InterCountryFragment();
                return tab2;
            case 2:
                InterCountryFragment tab3 = new TabFragment3();
                return tab3;*/
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}