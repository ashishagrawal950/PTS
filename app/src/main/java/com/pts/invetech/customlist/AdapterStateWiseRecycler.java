package com.pts.invetech.customlist;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pts.invetech.R;
import com.pts.model.statewise.StateRealTimeValue;

import java.util.ArrayList;

/**
 * Created by Ashish Karn on 24-07-2017.
 */

public class AdapterStateWiseRecycler extends RecyclerView.Adapter<AdapterStateWiseRecycler.ViewHolder> {

    ArrayList<StateRealTimeValue> stateRealTimeValues;
    Context context;


    public AdapterStateWiseRecycler(Context context, ArrayList<StateRealTimeValue> stateRealTimeValues) {
        super();
        this.context = context;
        this.stateRealTimeValues = stateRealTimeValues;

    }

    @Override
    public AdapterStateWiseRecycler.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycleritem_statewise, viewGroup, false);
        AdapterStateWiseRecycler.ViewHolder viewHolder = new AdapterStateWiseRecycler.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterStateWiseRecycler.ViewHolder viewHolder, int i) {
        viewHolder.txt_lable.setText(stateRealTimeValues.get(i).getName());
        viewHolder.txt_sum.setText(stateRealTimeValues.get(i).getSum());
        viewHolder.txt_avg.setText(stateRealTimeValues.get(i).getAvg());
    }

    @Override
    public int getItemCount() {
        return stateRealTimeValues.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txt_lable;
        public TextView txt_sum;
        public TextView txt_avg;


        public ViewHolder(View itemView) {
            super(itemView);
            txt_lable = (TextView) itemView.findViewById(R.id.txt_lable);
            txt_sum = (TextView) itemView.findViewById(R.id.txt_sum);
            txt_avg = (TextView) itemView.findViewById(R.id.txt_avg);
        }
    }

}

