package com.pts.retrofit;

import com.google.gson.JsonObject;
import com.pts.model.firebase.RegisterFirebase;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("register.php")
    Call<RegisterFirebase> setNotifyData(@FieldMap Map<String, String> data);
}
