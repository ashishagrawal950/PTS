package com.pts.handler;

import android.app.IntentService;
import android.content.Intent;

import com.pts.invetech.utils.AppLogger;
import com.pts.invetech.utils.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class NetworkIntentService extends IntentService {

    public NetworkIntentService() {
        super("NetworkIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String url=intent.getStringExtra(Constant.INTENT_SERVICE_URL);
        String param=intent.getStringExtra(Constant.INTENT_SERVICE_DATA);

        String data=networkCall(url,param);

        try {
            sendData(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendData(String data) throws JSONException {

        AppLogger.showMsgWithoutTag(data);

        JSONObject object=new JSONObject(data);
        if(object.getString("type").equalsIgnoreCase("TRUE")){
//            AppLogger.showShortToast(getApplicationContext(),"Data");
            AppLogger.showMsgWithoutTag("data");
        }else{
//            AppLogger.showShortToast(getApplicationContext()," Some Error Occurred ");
            AppLogger.showMsgWithoutTag("Some Error Occurred.");
        }
    }

    private String networkCall(String currentUrl, String jsonParam) {

        String contentAsString="";
        HttpURLConnection conn=null;

        try {
            URL url = new URL(currentUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(25000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);

            DataOutputStream dStream = new DataOutputStream(conn.getOutputStream());

            if (jsonParam != null) {
                dStream.writeBytes("data=" + jsonParam);
            }

            dStream.flush();
            dStream.close();

            conn.connect();
            int response = conn.getResponseCode();

            if(response== HttpURLConnection.HTTP_OK) {

                InputStream inputStream = conn.getInputStream();
                contentAsString = convertStreamToString(inputStream);

                inputStream.close();
            }else{
                contentAsString="";
            }

        } catch (Exception e) {
            AppLogger.showMsg("Error in Network Intent Service.", e.getMessage());
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return contentAsString;
    }

    private String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
