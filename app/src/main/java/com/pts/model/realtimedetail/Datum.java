
package com.pts.model.realtimedetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {


    @SerializedName("buyer")
    @Expose
    private String buyer;
    @SerializedName("seller")
    @Expose
    private String seller;
    @SerializedName("approval_no")
    @Expose
    private String approvalNo;
    @SerializedName("trader")
    @Expose
    private String trader;
    @SerializedName("maxrevision")
    @Expose
    private String maxrevision;
    @SerializedName("data_id")
    @Expose
    private String dataId;
    @SerializedName("totalqtm")
    @Expose
    private String totalqtm;
    @SerializedName("track")
    @Expose
    private Integer track;

    /**
     *
     * @return
     *     The buyer
     */
    public String getBuyer() {
        return buyer;
    }

    /**
     *
     * @param buyer
     *     The buyer
     */
    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    /**
     *
     * @return
     *     The seller
     */
    public String getSeller() {
        return seller;
    }

    /**
     *
     * @param seller
     *     The seller
     */
    public void setSeller(String seller) {
        this.seller = seller;
    }

    /**
     *
     * @return
     *     The approvalNo
     */
    public String getApprovalNo() {
        return approvalNo;
    }

    /**
     *
     * @param approvalNo
     *     The approval_no
     */
    public void setApprovalNo(String approvalNo) {
        this.approvalNo = approvalNo;
    }

    /**
     *
     * @return
     *     The trader
     */
    public String getTrader() {
        return trader;
    }

    /**
     *
     * @param trader
     *     The trader
     */
    public void setTrader(String trader) {
        this.trader = trader;
    }

    /**
     *
     * @return
     *     The maxrevision
     */
    public String getMaxrevision() {
        return maxrevision;
    }

    /**
     *
     * @param maxrevision
     *     The maxrevision
     */
    public void setMaxrevision(String maxrevision) {
        this.maxrevision = maxrevision;
    }

    /**
     *
     * @return
     *     The dataId
     */
    public String getDataId() {
        return dataId;
    }

    /**
     *
     * @param dataId
     *     The data_id
     */
    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    /**
     *
     * @return
     *     The totalqtm
     */
    public String getTotalqtm() {
        return totalqtm;
    }

    /**
     *
     * @param totalqtm
     *     The totalqtm
     */
    public void setTotalqtm(String totalqtm) {
        this.totalqtm = totalqtm;
    }

    /**
     *
     * @return
     *     The track
     */
    public Integer getTrack() {
        return track;
    }

    /**
     *
     * @param track
     *     The track
     */
    public void setTrack(Integer track) {
        this.track = track;
    }

    public Datum isPresent(String str){
        if(getBuyer().toLowerCase().contains(str.toLowerCase()) ||
                getSeller().toLowerCase().contains(str.toLowerCase()) ||
                getTrader().toLowerCase().contains(str.toLowerCase())){
            return this;
        }
        return null;
    }


}